# DM_LATAM

A friend in need is a friend indeed, but a friend with weed is better...

If you want to smoke bring your own supply, I never thought you were a joker smoker

# Folder Structure

"Be sure you put your feet in the right place, then stand firm" Abraham Lincoln

The folder structure is designed with simplicity in mind:


```
{dm_latam}
 ├── artifacts
 │   ├── (one folder by country, in the order of the migration schedule)
 │   └── 01.PAN
 │       ├── (a folder by VO Group)
 │       ├── Vendors
 │       |   ├── (the workbook for that VO Group at the root, plus support files)
 │       |   ├── TKE_PAN_FD_04_Vendors.xlsm
 │       |   └── scripts
 │       |      └── (all associated scripts, for inspection, loading stage tables, and others)
 │       |   └── entrysets
 │       |      └── (files with which users interact)
 │       |   └── datasets
 │       |      └── (all generated datasets, zipped)
 │       |   └── support
 │       |      └── (help, implementation notes, any other information for developers)
 ....  
 └── commons
 │   |
 │   ├── modules // (common modules, class definitions)
 │   |   └── (VB files to be shared across workbooks)
 |   └── prototype  
 │       └── (one or more prototypes, following same folder structure as the VO workbooks above)
 └── support
     ├── (backlog, changelog, documentation in general)
     └── backlog.xls (Product backlog)
```

# Terminology

## Prototype

Prototypes are few, ideally ONLY ONE.

The prototype does not follow any flat file specification, nor does it need to be connected to an external datasource.

It is used exclusively by the developers.  Users will never see it.

This is where new features should be first tested.

Changes to the prototype should be concentrated on Commons modules, which can be re-used across all other Workbooks

## VO Template Workbook

There is ONE VO template:
- per VO Group (Vendors, Customers, Inventory, Projects)
- per country

The VO template follows closely its flat file specification, but it's not yet associated to any particular datasource.

```
TKE_<ISO Country Code>_IDL#_Timestamp_Short Description.xslsm

(e.g. TKE_PAN_FD_04_Vendors.xlsm)

```
A VO Template Wookbook sits at the root of the VO Group folder:

```
dm_latam\artifacts\01.PAN\Vendors\TKE_PAN_FD_04_Vendors.xlsm)

```
They are used exclusively by the developers.

## Entrysets

A developer transforms a VO Template into an Entryset by:

- loading data into each proper sheet with parameters obtained from the functional team
- making comments for later tracking (reasons, which script was used, time of first load)
- making sure all locks are in place before handing the workbook to key users

There can be many EntrySets per each VO Template.

Entrysets are always associated to a particular datasource, or loading routine.

Key users interact with Entrysets by updating values, and setting cleansed flags.

## Datasets

There can be many datasets per Entryset.   Datasets are collections of flat files.

A dataset represents the current state of the cleansing process.   As users cleanse more, and more records, new datasets may be generated.

Each dataset is time-stamped and follows a naming pattern:

```
TKE_<ISO Country Code>_IDL#_Timestamp_Short Description.txt

For example:
TKE_PAN_FD_03_201711301030_VendorHeader.txt                                  
TKE_PAN_FD_03_201711301030_VendorSite.txt
TKE_PAN_FD_03_201711301030_VendorSiteContact.txt
TKE_PAN_FD_03_201711301030_VendorBankAccounts.txt

```



# References

[Markdown reference](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
