VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IniFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private objError  As Object
Private strValue As String
Private Const strFileName As String = "Config.ini"

Public Function INIOpen(strSection As String, strKey As String) As Boolean
    On Error GoTo INIOpen_E
    
    Dim strPath As String
    Dim strReturnValue As String
    Dim lngCodeReturn As Long
    
    INIOpen = False
    
    strPath = App.Path & "\" & strFileName
    
    strReturnValue = String(255, 0)
    lngCodeReturn = GetPrivateProfileString(strSection, strKey, vbNullString, strReturnValue, Len(strReturnValue), strPath)
    
    Select Case lngCodeReturn
    Case 0
        LetValue = ""
    Case Else
        LetValue = Left(strReturnValue, InStr(1, strReturnValue, Chr(0)) - 1)
    End Select
    
    INIOpen = True
    
    GoTo DestroyObjects

INIOpen_E:
   objError = Err

DestroyObjects:
   
End Function

Public Property Get GetValue() As String
   GetValue = strValue
End Property

Private Property Let LetValue(ByVal vNewValue As String)
   strValue = vNewValue
End Property

Public Property Get GetError() As Object
   GetError = objError
End Property
