VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Load"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Enum enumValidation
   enConfig = 1
   enRoutes = 2
   enConnection = 3
   enPermisson = 4
   enTags = 5
End Enum

Public Function loadValidation(lstValidation As Object) As Boolean
   On Error GoTo loadValidation_E
   
   Dim objLst As ListItem
   
   loadValidation = False
   
   lstValidation.ListItems.Clear
      
   Set objLst = lstValidation.ListItems.Add(, "K_" & enumValidation.enConfig, "Config INI", , "RedSign")
   objLst.Selected = True
   DoEvents
      
   Set objLst = lstValidation.ListItems.Add(, "K_" & enumValidation.enRoutes, "Routes", , "RedSign")
   objLst.Selected = True
   DoEvents
   
   Set objLst = lstValidation.ListItems.Add(, "K_" & enumValidation.enConnection, "Connection Data", , "RedSign")
   objLst.Selected = True
   DoEvents
   
   Set objLst = lstValidation.ListItems.Add(, "K_" & enumValidation.enPermisson, "Permisson", , "RedSign")
   objLst.Selected = True
   DoEvents
   
   Set objLst = lstValidation.ListItems.Add(, "K_" & enumValidation.enTags, "Tags", , "RedSign")
   objLst.Selected = True
   DoEvents
   
   loadValidation = True
   
   GoTo DestroyObjects

loadValidation_E:
   MsgBox "Error Number: " & Err.Number & vbNewLine & _
          "Error Description:  " & Err.Description, vbCritical
          
DestroyObjects:
   Set objLst = Nothing
End Function

Public Function loadConfigFile(lstValidation As Object) As Boolean
   On Error GoTo loadConfigFile_E
   
   Dim objLst As ListItem
   
   loadConfigFile = False
   
   lstValidation.ListItems.Item("K_" & enumValidation.enConfig).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "YelowSign"
   
   If Len(Trim(Dir(App.Path & "\Config.ini", vbArchive))) > 0 Then
      lstValidation.ListItems.Item("K_" & enumValidation.enConfig).Selected = True
      Set objLst = lstValidation.SelectedItem
      objLst.SmallIcon = "GreenSign"
   End If
   
   loadConfigFile = True
   
   GoTo DestroyObjects

loadConfigFile_E:
   MsgBox "Error Number: " & Err.Number & vbNewLine & _
       "Error Description:  " & Err.Description, vbCritical

DestroyObjects:
   Set objLst = Nothing
End Function

Public Function loadRoutes(lstValidation As Object) As Boolean
   On Error GoTo loadRoutes_E
   
   Dim objLst As ListItem
   Dim clsINIFile As IniFile
   
   loadRoutes = False
   
   lstValidation.ListItems.Item("K_" & enumValidation.enRoutes).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "YelowSign"
   
   Set clsINIFile = New IniFile
   
   With clsINIFile
      If Not .INIOpen("PATH", "ICONS") Then
         GoTo DestroyObjects
      Else
         PathIcons = .GetValue
      End If
   
      If Not .INIOpen("PATH", "OBJECTS") Then
         GoTo DestroyObjects
      Else
         PathObjects = .GetValue
      End If
         
      If Not .INIOpen("CONFIG", "TIME(nn)") Then
         GoTo DestroyObjects
      Else
         intTime = .GetValue
      End If
   End With
   
   lstValidation.ListItems.Item("K_" & enumValidation.enRoutes).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "GreenSign"
   
   loadRoutes = True
   
   GoTo DestroyObjects
   
loadRoutes_E:
   MsgBox "Error Number: " & Err.Number & vbNewLine & _
          "Error Description:  " & Err.Description, vbCritical
          
DestroyObjects:
   Set objLst = Nothing
   Set clsINIFile = Nothing
End Function

Public Function loadDataBase(lstValidation As Object) As Boolean
   On Error GoTo loadDataBase_E
   
   Dim objLst As ListItem
   Dim clsINIFile As IniFile
   
   loadDataBase = False
   
   lstValidation.ListItems.Item("K_" & enumValidation.enConnection).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "YelowSign"
   
   Set clsINIFile = New IniFile
   
   With clsINIFile
      If Not .INIOpen("DATABASE", "PROVIDER") Then
         GoTo DestroyObjects
      Else
         Provider = .GetValue
      End If
   
      If Not .INIOpen("DATABASE", "DATASOURCE") Then
         GoTo DestroyObjects
      Else
         DataSource = .GetValue
      End If
   
      If Not .INIOpen("DATABASE", "USER_ID") Then
         GoTo DestroyObjects
      Else
         User = .GetValue
      End If
      
      If Not .INIOpen("DATABASE", "PASSWORD") Then
         GoTo DestroyObjects
      Else
         Password = .GetValue
      End If
      
      If Not .INIOpen("DATABASE", "INITIAL_CATALOG") Then
         GoTo DestroyObjects
      Else
         InitialCatalog = .GetValue
      End If
   End With
   
   lstValidation.ListItems.Item("K_" & enumValidation.enConnection).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "GreenSign"
   
   loadDataBase = True
   
   GoTo DestroyObjects
   
loadDataBase_E:
   MsgBox "Error Number: " & Err.Number & vbNewLine & _
          "Error Description:  " & Err.Description, vbCritical
          
DestroyObjects:
   Set objLst = Nothing
   Set clsINIFile = Nothing
End Function

Public Function loadPermission(lstValidation As Object) As Boolean
   On Error GoTo loadPermission_E
   
   Dim fso As Scripting.FileSystemObject
   Dim ts As Scripting.TextStream
   Dim objLst As ListItem
   
   loadPermission = False
   
   lstValidation.ListItems.Item("K_" & enumValidation.enPermisson).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "YelowSign"
   
   Set fso = New Scripting.FileSystemObject
   
   On Error Resume Next
   Set ts = fso.CreateTextFile(PathObjects & "\test.txt", ForReading, True)
   
   If ts Is Nothing Then GoTo DestroyObjects
      
   Set ts = fso.OpenTextFile(PathObjects & "\test.txt", ForWriting, False)
   ts.Write "Laundry"
   ts.Close
   
   fso.DeleteFile PathObjects & "\test.txt", True
   
   lstValidation.ListItems.Item("K_" & enumValidation.enPermisson).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "GreenSign"
   
   loadPermission = True
   
   GoTo DestroyObjects
   
loadPermission_E:
   MsgBox "Error Number: " & Err.Number & vbNewLine & _
          "Error Description:  " & Err.Description, vbCritical
              
DestroyObjects:
   Set fso = Nothing
   Set ts = Nothing
   Set objLst = Nothing
End Function

Public Function loadTags(lstValidation As Object, lstTags As Object) As Boolean
   On Error GoTo loadTags_E
   
   Dim strTags As String
   Dim objLst As ListItem
   Dim clsINIFile As IniFile
   Dim intSeparator As Integer
   Dim strObject As String
    
   Set clsINIFile = New IniFile
   
   loadTags = False
   
   lstValidation.ListItems.Item("K_" & enumValidation.enTags).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "YelowSign"
   
   With clsINIFile
      If Not .INIOpen("OBJECTS", "TAGS") Then
         GoTo DestroyObjects
      Else
         strTags = .GetValue
      End If
   End With
   
   lstTags.ListItems.Clear
   
   Do While Len(Trim(strTags)) > 0
      intSeparator = InStr(1, strTags, ";", vbBinaryCompare)
      
      If intSeparator = 0 Then intSeparator = Len(Trim(strTags))
      strObject = Mid(strTags, 1, intSeparator)
      strTags = Replace(strTags, strObject, "")
      strObject = Replace(strObject, ";", "")

      Set objLst = lstTags.ListItems.Add(, "K_" & strObject, strObject, , "RedSign")
      objLst.SubItems(1) = "Not Verified"
      objLst.Selected = True
      DoEvents
   Loop
   
   lstValidation.ListItems.Item("K_" & enumValidation.enTags).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "GreenSign"
   
   loadTags = True
   
   GoTo DestroyObjects
   
loadTags_E:
   MsgBox "Error Number: " & Err.Number & vbNewLine & _
          "Error Description:  " & Err.Description, vbCritical

DestroyObjects:
   Set objLst = Nothing
   Set clsINIFile = Nothing
End Function
