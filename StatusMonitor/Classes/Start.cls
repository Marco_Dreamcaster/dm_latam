VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Start"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Function startTags(lstTags As Object, lstObjects As Object, lstTasks As Object, pgbTasks As ProgressBar, pgbLines As ProgressBar) As Boolean
   On Error GoTo startTags_E
   
   Dim lngCountLst As Long
   Dim objLstTags As ListItem
   Dim objLstObjects As ListItem
   
   startTags = False
   
   lngCountLst = 0
   pgbTasks.Max = lstTags.ListItems.Count
   
   For Each objLstTags In lstTags.ListItems
      lngCountLst = lngCountLst + 1
      objLstTags.SmallIcon = "YelowSign"
      
      pgbTasks.Value = lngCountLst
      
      For Each objLstObjects In lstObjects.ListItems
         If UCase(Right(objLstObjects.Text, 3)) = "CSV" Then
            If InStr(1, objLstObjects.Text, objLstTags.Text) > 0 Then
               If Not startTasks(PathObjects & "\" & objLstObjects.Text, lstTasks, pgbLines, objLstTags.Text) Then GoTo DestroyObjects
               objLstObjects.SubItems(1) = "Processed"
               objLstObjects.SmallIcon = "GreenSign"
            End If
         End If
      Next
      
      objLstTags.SubItems(1) = "Processed"
      objLstTags.SmallIcon = "GreenSign"
      
      DoEvents
   Next
   
   For Each objLstObjects In lstObjects.ListItems
      If objLstObjects.SubItems(1) = "Not Verified" Then
         objLstObjects.SubItems(1) = "Invalid"
         objLstObjects.SmallIcon = "RedSign"
         objLstObjects.Selected = True
      End If
   Next
   
   startTags = True

   GoTo DestroyObjects
   
startTags_E:
   MsgBox "Error Number: " & Err.Number & vbNewLine & _
          "Error Description:  " & Err.Description, vbCritical
          
DestroyObjects:
   Set objLstTags = Nothing
   Set objLstObjects = Nothing
End Function

Public Function startObjects(lstObjects As Object, pgbObjects As ProgressBar) As Boolean
   On Error GoTo startObjects_E
   
   Dim lngLstObjects As Long
   Dim objLst As ListItem
      
   Dim objFSO As Scripting.FileSystemObject
   Dim objFolders As Object
   Dim objExistingFiles As Object
   Dim objFile As Object
   
   startObjects = False
         
   lstObjects.ListItems.Clear
   
   lngLstObjects = 0
   Set objFSO = New Scripting.FileSystemObject
   Set objFolders = objFSO.GetFolder(PathObjects)
   Set objExistingFiles = objFolders.Files
   
   If objExistingFiles.Count > 0 Then
      pgbObjects.Min = 0
      pgbObjects.Max = objExistingFiles.Count
      
      For Each objFile In objExistingFiles
         lngLstObjects = lngLstObjects + 1
         
         Set objLst = lstObjects.ListItems.Add(, "K_" & lngLstObjects, objFile.Name, , "RedSign")
         objLst.SubItems(1) = "Not Verified"
         objLst.Selected = True
         
         pgbObjects.Value = lngLstObjects
         
         DoEvents
      Next
   End If
   startObjects = True
   
   GoTo DestroyObjects
   
startObjects_E:
   MsgBox "Error Number: " & Err.Number & vbNewLine & _
          "Error Description:  " & Err.Description, vbCritical
          
DestroyObjects:
   Set objLst = Nothing
   Set objFSO = Nothing
   Set objFolders = Nothing
   Set objExistingFiles = Nothing
   Set objFile = Nothing
End Function

Private Function startTasks(File As String, lstTasks As Object, pgbLines As ProgressBar, Tag As String) As Boolean
   On Error GoTo startTasks_E
   
   Dim intField As Integer
   Dim curLine As Currency
   Dim strValue As String
   Dim varEnt As String
   Dim arrEnt() As String
   
   Dim strCountry As String
   Dim strObject As String
   Dim strTarger As String
   Dim strDone As String
   
   Dim curFileSize As Currency
   Dim objLstTask As ListItem
   
   Dim strSQL As String
   Dim datNow As String
   
   'Cada processamento de arquivo
   'ira utilizar uma conexao propria
   Dim ADOCon As ADODB.Connection
   
   startTasks = False
   
   Set ADOCon = New ADODB.Connection
   
   ADOCon.Provider = Provider
   ADOCon.Properties("User ID").Value = User
   ADOCon.Properties("Password").Value = Password
   ADOCon.Properties("Data Source").Value = DataSource
   ADOCon.Properties("Initial Catalog").Value = InitialCatalog
   ADOCon.Open
   DoEvents

   'Conta o numero de linhas do arq
   Open File For Input As #1
   Do While Not EOF(1)
      Line Input #1, varEnt
      curFileSize = curFileSize + 1
   Loop
   Close #1
   
   pgbLines.Min = 0
   pgbLines.Max = curFileSize
   
   Open File For Input As #1
   Do While Not EOF(1)
      curLine = curLine + 1
      Line Input #1, varEnt
      If Not curLine = 1 Then
         arrEnt = Split(varEnt, ";")
         
         For intField = 0 To 3
            strValue = arrEnt(intField)
            
            Select Case intField
            Case 0   'Country
               strCountry = strValue
            Case 1   'Object
               strObject = strValue
            Case 2   'Count Target
               strTarger = strValue
            Case 3   'Count Done
               strDone = strValue
            End Select
         Next intField
         
         If Len(Trim(strValue)) > 0 Then
            strSQL = ""
            strSQL = strSQL & " INSERT INTO DM_LATAM.DBO.COM_AUDIT_LOGS ("
            strSQL = strSQL & "     COUNTRY, USER_ID, COUNT, EVENT, STAMP, TYPE, "
            strSQL = strSQL & "     SNAPSHOT_DATE, CUTOFF_DATE, GROUP_REF_CODE, AMOUNT, "
            strSQL = strSQL & "     TARGET, DONE "
            strSQL = strSQL & " ) VALUES ( "
            strSQL = strSQL & "'" & strCountry & "',"
            strSQL = strSQL & "'Monitor',"
            strSQL = strSQL & " NULL,"
            strSQL = strSQL & "'" & Tag & "',"
            strSQL = strSQL & "'" & datNow & "',"
            strSQL = strSQL & "'" & strObject & "',"
            strSQL = strSQL & "dbo.udf_EncodeSnapshotDate('" & strCountry & "'),"
            strSQL = strSQL & "NULL,"
            strSQL = strSQL & "NULL,"
            strSQL = strSQL & "NULL,"
            strSQL = strSQL & strTarger & ","
            strSQL = strSQL & strDone
            strSQL = strSQL & ")"
                       
            ADOCon.Execute strSQL
         End If
         
         Set objLstTask = lstTasks.ListItems.Add(, "K_" & curLine - 1, File, , "GreenSign")
         objLstTask.SubItems(1) = strObject
         objLstTask.SubItems(2) = strTarger
         objLstTask.SubItems(3) = strDone
         objLstTask.SubItems(4) = strCountry
         objLstTask.SubItems(5) = datNow
         objLstTask.Selected = True
      End If
      
      pgbLines.Value = curLine
      DoEvents
   Loop
   Close #1
   
   FileCopy File, Environ("TEMP") & "\" & Tag & "_" & Format(Now, "yyyyMMdd_hhnnss") & ".csv" 'copia o arquivo para o temp
   Kill File 'apaga o arquivo original
   startTasks = True
   
   GoTo DestroyObjects
   
startTasks_E:
   MsgBox "Error Number: " & Err.Number & vbNewLine & _
          "Error Description:  " & Err.Description, vbCritical
   
DestroyObjects:
   Set objLstTask = Nothing
End Function
