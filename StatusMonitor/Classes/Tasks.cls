VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Tasks"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Enum enumValidation
   enConfig = 1
   enRoutes = 2
   enConnection = 3
   enPermisson = 4
   enTags = 5
End Enum

Private strError As String

Public Property Get GetError() As String
   GetError = strError
End Property

Public Function Validation(lstValidation As Object) As Boolean
   On Error GoTo Validation_E
   
   Dim objLst As ListItem
   
   Validation = False
   
   lstValidation.ListItems.Clear
      
   Set objLst = lstValidation.ListItems.Add(, "K_" & enumValidation.enConfig, "Config INI", , "RedSign")
   objLst.Selected = True
   DoEvents
      
   Set objLst = lstValidation.ListItems.Add(, "K_" & enumValidation.enRoutes, "Routes", , "RedSign")
   objLst.Selected = True
   DoEvents
   
   Set objLst = lstValidation.ListItems.Add(, "K_" & enumValidation.enConnection, "Connection Data", , "RedSign")
   objLst.Selected = True
   DoEvents
   
   Set objLst = lstValidation.ListItems.Add(, "K_" & enumValidation.enPermisson, "Permisson", , "RedSign")
   objLst.Selected = True
   DoEvents
   
   Set objLst = lstValidation.ListItems.Add(, "K_" & enumValidation.enTags, "Tags", , "RedSign")
   objLst.Selected = True
   DoEvents
   
   Validation = True
   
   GoTo DestroyObjects

Validation_E:
   strError = "Error Number: " & Err.Number & vbNewLine & _
              "Error Description: " & Err.Description

DestroyObjects:
   Set objLst = Nothing
End Function

Public Function CheckConfigFile(lstValidation As Object) As Boolean
   On Error GoTo CheckConfigFile_E
   
   Dim objLst As ListItem
   
   CheckConfigFile = False
   
   lstValidation.ListItems.Item("K_" & enumValidation.enConfig).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "YelowSign"
   
   If Len(Trim(Dir(App.Path & "\Config.ini", vbArchive))) > 0 Then
      lstValidation.ListItems.Item("K_" & enumValidation.enConfig).Selected = True
      Set objLst = lstValidation.SelectedItem
      objLst.SmallIcon = "GreenSign"
   End If
   
   CheckConfigFile = True
   
   GoTo DestroyObjects

CheckConfigFile_E:
   strError = "Error Number: " & Err.Number & vbNewLine & _
              "Error Description: " & Err.Description
   
DestroyObjects:
   Set objLst = Nothing
End Function

Public Function CheckRoutes(lstValidation As Object) As Boolean
   On Error GoTo CheckRoutes_E
   
   Dim intTime As Integer
   Dim objLst As ListItem
   Dim clsINIFile As IniFile
   
   CheckRoutes = False
   
   lstValidation.ListItems.Item("K_" & enumValidation.enConfig).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "YelowSign"
   
   Set clsINIFile = New IniFile
   
   With clsINIFile
      If Not .INIOpen("PATH", "ICONS") Then
         strError = .GetError
         GoTo DestroyObjects
      Else
         PathIcons = .GetValue
      End If
   
      If Not .INIOpen("PATH", "OBJECTS") Then
         strError = .GetError
         GoTo DestroyObjects
      Else
         PathObjects = .GetValue
      End If
         
      If Not .INIOpen("CONFIG", "TIME(nn)") Then
         strError = .GetError
         GoTo DestroyObjects
      Else
         intTime = .GetValue
      End If
   End With
   
   lstValidation.ListItems.Item("K_" & enumValidation.enRoutes).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "GreenSign"
   
   CheckRoutes = True
   
   GoTo DestroyObjects
   
CheckRoutes_E:
   strError = "Error Number: " & Err.Number & vbNewLine & _
              "Error Description: " & Err.Description
   
DestroyObjects:
   Set objLst = Nothing
   Set clsINIFile = Nothing
End Function

Public Function CheckDataBase(lstValidation As Object) As Boolean
   On Error GoTo CheckDataBase_E
   
   Dim objLst As ListItem
   Dim clsINIFile As IniFile
   
   CheckDataBase = False
   
   lstValidation.ListItems.Item("K_" & enumValidation.enConfig).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "YelowSign"
   
   Set clsINIFile = New IniFile
   
   With clsINIFile
      If Not .INIOpen("DATABASE", "PROVIDER") Then
         strError = .GetError
         GoTo DestroyObjects
      Else
         Provider = .GetValue
      End If
   
      If Not .INIOpen("DATABASE", "DATASOURCE") Then
         strError = .GetError
         GoTo DestroyObjects
      Else
         DataSource = .GetValue
      End If
   
      If Not .INIOpen("DATABASE", "USER_ID") Then
         strError = .GetError
         GoTo DestroyObjects
      Else
         User = .GetValue
      End If
      
      If Not .INIOpen("DATABASE", "PASSWORD") Then
         strError = .GetError
         GoTo DestroyObjects
      Else
         Password = .GetValue
      End If
      
      If Not .INIOpen("DATABASE", "INITIAL_CATALOG") Then
         strError = .GetError
         GoTo DestroyObjects
      Else
         InitialCatalog = .GetValue
      End If
   End With
   
   lstValidation.ListItems.Item("K_" & enumValidation.enConnection).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "GreenSign"
   
   CheckDataBase = True
   
   GoTo DestroyObjects
   
CheckDataBase_E:
   strError = "Error Number: " & Err.Number & vbNewLine & _
              "Error Description: " & Err.Description

DestroyObjects:
   Set objLst = Nothing
   Set clsINIFile = Nothing
End Function

Public Function CheckPermission(lstValidation As Object) As Boolean
   On Error GoTo CheckPermission_E
   
   Dim fso As Scripting.FileSystemObject
   Dim ts As Scripting.TextStream
   Dim objLst As ListItem
   
   CheckPermission = False
   
   lstValidation.ListItems.Item("K_" & enumValidation.enConfig).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "YelowSign"
   
   Set fso = New Scripting.FileSystemObject
   
   On Error Resume Next
   Set ts = fso.CreateTextFile(PathObjects & "\test.txt", ForReading, True)
   
   If ts Is Nothing Then
      strError = "No permission to create file!"
      GoTo DestroyObjects
   End If
   
   Set ts = fso.OpenTextFile(PathObjects & "\test.txt", ForWriting, False)
   ts.Write "Laundry"
   ts.Close
   
   fso.DeleteFile PathObjects & "\test.txt", True
   
   lstValidation.ListItems.Item("K_" & enumValidation.enPermisson).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "GreenSign"
   
   CheckPermission = True
   
   GoTo DestroyObjects
   
CheckPermission_E:
   strError = "Error Number: " & Err.Number & vbNewLine & _
              "Error Description: " & Err.Description
              
DestroyObjects:
   Set fso = Nothing
   Set ts = Nothing
   Set objLst = Nothing
End Function

Public Function CheckTags(lstValidation As Object, lstTags As Object) As Boolean
   On Error GoTo CheckTags_E
   
   Dim strTags As String
   Dim objLst As ListItem
   Dim clsINIFile As IniFile
   Dim intSeparator As Integer
   Dim strObject As String
    
   Set clsINIFile = New IniFile
   
   CheckTags = False
   
   lstValidation.ListItems.Item("K_" & enumValidation.enConfig).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "YelowSign"
   
   With clsINIFile
      If Not .INIOpen("OBJECTS", "TAGS") Then
         strError = .GetError
         GoTo DestroyObjects
      Else
         strTags = .GetValue
      End If
   End With
   
   lstValidation.ListItems.Item("K_" & enumValidation.enTags).Selected = True
   Set objLst = lstValidation.SelectedItem
   objLst.SmallIcon = "GreenSign"
   
   lstTags.ListItems.Clear
   
   Do While Len(Trim(strTags)) > 0
      intSeparator = InStr(1, strTags, ";", vbBinaryCompare)
      
      If intSeparator = 0 Then intSeparator = Len(Trim(strTags))
      strObject = Mid(strTags, 1, intSeparator)
      strTags = Replace(strTags, strObject, "")
      strObject = Replace(strObject, ";", "")

      Set objLst = lstTags.ListItems.Add(, "K_" & strObject, strObject, , "YelowSign")
      objLst.SubItems(1) = "Not Verified"
      objLst.Selected = True
      DoEvents
   Loop
   
   CheckTags = True
   
   GoTo DestroyObjects
   
CheckTags_E:
   strError = "Error Number: " & Err.Number & vbNewLine & _
              "Error Description: " & Err.Description

DestroyObjects:
   Set objLst = Nothing
   Set clsINIFile = Nothing
End Function
