VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmStatusMonitor 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Status Monitor"
   ClientHeight    =   8520
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   13005
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Consolas"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "StatusMonitor.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8520
   ScaleWidth      =   13005
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ProgressBar pgbValidation 
      Height          =   225
      Left            =   45
      TabIndex        =   4
      Top             =   2025
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   397
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
   End
   Begin VB.Timer Timer1 
      Interval        =   10000
      Left            =   4875
      Top             =   645
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   5280
      Top             =   1350
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "StatusMonitor.frx":000C
            Key             =   "ServerOffline"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "StatusMonitor.frx":05A6
            Key             =   "ServerOnline"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "StatusMonitor.frx":0B40
            Key             =   "GreenSign"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "StatusMonitor.frx":10DA
            Key             =   "RedSign"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "StatusMonitor.frx":1674
            Key             =   "YelowSign"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lstValidation 
      Height          =   1935
      Left            =   45
      TabIndex        =   0
      Top             =   75
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   3413
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ColHdrIcons     =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Consolas"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Validation"
         Object.Width           =   7831
      EndProperty
   End
   Begin MSComctlLib.ListView lstTags 
      Height          =   2235
      Left            =   45
      TabIndex        =   1
      Top             =   2265
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   3942
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ColHdrIcons     =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Consolas"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Tags"
         Object.Width           =   3882
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "File"
         Object.Width           =   3881
      EndProperty
   End
   Begin MSComctlLib.ListView lstObjects 
      Height          =   1935
      Left            =   4920
      TabIndex        =   2
      Top             =   75
      Width           =   8010
      _ExtentX        =   14129
      _ExtentY        =   3413
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ColHdrIcons     =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Consolas"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Objects"
         Object.Width           =   9527
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Status"
         Object.Width           =   3951
      EndProperty
   End
   Begin MSComctlLib.ListView lstMonitor 
      Height          =   2235
      Left            =   4920
      TabIndex        =   3
      Top             =   2265
      Width           =   8025
      _ExtentX        =   14155
      _ExtentY        =   3942
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ColHdrIcons     =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Consolas"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Service Initialization"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Last Service Startup"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Next Service Startup"
         Object.Width           =   4410
      EndProperty
   End
   Begin MSComctlLib.ProgressBar pgbObjects 
      Height          =   225
      Left            =   4920
      TabIndex        =   5
      Top             =   2025
      Width           =   8010
      _ExtentX        =   14129
      _ExtentY        =   397
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
   End
   Begin MSComctlLib.ProgressBar pgbTask 
      Height          =   225
      Left            =   45
      TabIndex        =   6
      Top             =   4515
      Width           =   12900
      _ExtentX        =   22754
      _ExtentY        =   397
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
   End
   Begin MSComctlLib.ListView lstTask 
      Height          =   2235
      Left            =   60
      TabIndex        =   7
      Top             =   4995
      Width           =   12885
      _ExtentX        =   22728
      _ExtentY        =   3942
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ColHdrIcons     =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Consolas"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   6
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "File Name"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Object"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Count_Taget"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Count_Done"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Country"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Date and time of registration"
         Object.Width           =   4939
      EndProperty
   End
   Begin MSComctlLib.ProgressBar pgbFileLines 
      Height          =   225
      Left            =   45
      TabIndex        =   8
      Top             =   4755
      Width           =   12900
      _ExtentX        =   22754
      _ExtentY        =   397
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
   End
   Begin MSComctlLib.ListView lstError 
      Height          =   915
      Left            =   60
      TabIndex        =   9
      Top             =   7275
      Width           =   12885
      _ExtentX        =   22728
      _ExtentY        =   1614
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      Icons           =   "ImageList1"
      SmallIcons      =   "ImageList1"
      ColHdrIcons     =   "ImageList1"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Consolas"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Error Number"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Error Description"
         Object.Width           =   16757
      EndProperty
   End
   Begin VB.Image imgClose 
      Height          =   240
      Left            =   12675
      Picture         =   "StatusMonitor.frx":1C0E
      Stretch         =   -1  'True
      Top             =   8235
      Width           =   240
   End
   Begin VB.Image ImgStatus 
      Height          =   240
      Left            =   75
      Picture         =   "StatusMonitor.frx":2050
      Top             =   8235
      Width           =   240
   End
End
Attribute VB_Name = "frmStatusMonitor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private lngCountMonitor As Long

Private strTags As String

Private Function LoadSystem() As Boolean
   On Error GoTo LoadSystem_E
   
   Dim clsLoad As New Load
   
   LoadSystem = False
   
   'Configura o progress bar validation
   Me.pgbValidation.Min = 0
   Me.pgbValidation.Max = 5
   
   With clsLoad
      'Carrega a lista da validacao com os itens
      If Not .loadValidation(Me.lstValidation) Then GoTo TimerOff
      
      'Verifica se o arquivo de configura��o existe
      If Not .loadConfigFile(Me.lstValidation) Then GoTo TimerOff
      
      Me.pgbValidation.Value = 1
      DoEvents
      
      'Verifica e carrega os caminhos dos arquivos e objetos
      If Not .loadRoutes(Me.lstValidation) Then GoTo TimerOff
      
      Me.ImgStatus.Picture = LoadPicture(PathIcons & "\ServerOnline.ico")
      Me.pgbValidation.Value = 2
      DoEvents
      
      'Verifica e carrega os dados de conex�o com o banco do Laundry
      If Not .loadDataBase(Me.lstValidation) Then GoTo TimerOff
            
      Me.pgbValidation.Value = 3
      DoEvents
      
      'Verifica permissao na pasta
      If Not .loadPermission(Me.lstValidation) Then GoTo TimerOff
      
      Me.pgbValidation.Value = 4
      DoEvents
      
      'Verifica as tarefas
      If Not .loadTags(Me.lstValidation, Me.lstTags) Then GoTo TimerOff
      
      Me.pgbValidation.Value = 5
      DoEvents
   End With
   
   LoadSystem = True
   GoTo DestroyObjects
   
LoadSystem_E:
   MsgBox "Error Number: " & Err.Number & vbNewLine & _
          "Error Description:  " & Err.Description, vbCritical
   
TimerOff:
   Me.Timer1.Enabled = False
   Me.ImgStatus.Picture = LoadPicture(PathIcons & "\ServerOffline.ico")
   
DestroyObjects:
   Set clsLoad = Nothing
End Function

Private Function StartSystem() As Boolean
   On Error GoTo StartSystem_E
   
   Dim clsStart As Start
   Dim objLstTags As ListItem
   
   StartSystem = False
   
   Set clsStart = New Start
   With clsStart
      'Organizado tudo para o processamento
      
      Me.lstObjects.ListItems.Clear
      Me.lstTask.ListItems.Clear
      Me.pgbFileLines.Value = 0
      Me.pgbObjects.Value = 0
      Me.pgbTask.Value = 0
      
      'Tags
      For Each objLstTags In Me.lstTags.ListItems
         objLstTags.SubItems(1) = "Not Verified"
         objLstTags.SmallIcon = "RedSign"
         objLstTags.Selected = True
      Next
      
      'Verifica e carrega todos os arquivos no sistema
      If Not .startObjects(Me.lstObjects, Me.pgbObjects) Then GoTo TimerOff
         
      'Verifica cada arquivo apartir da tag, ap�s realiza o processamento do mesmo
      If Not .startTags(Me.lstTags, Me.lstObjects, Me.lstTask, Me.pgbTask, Me.pgbFileLines) Then GoTo TimerOff
      
   End With
   
   StartSystem = True
   
   GoTo DestroyObjects
   
StartSystem_E:
   MsgBox "Error Number: " & Err.Number & vbNewLine & _
          "Error Description:  " & Err.Description, vbCritical
          
TimerOff:
   Me.Timer1.Enabled = False
   Me.ImgStatus.Picture = LoadPicture(PathIcons & "\ServerOffline.ico")
   
DestroyObjects:
   Set clsStart = Nothing
End Function

Private Sub Image1_Click()

End Sub

Private Sub imgClose_Click()
   Me.Timer1.Enabled = False
   End
End Sub

Private Sub ImgStatus_Click()
   If Me.lstValidation.ListItems.Count > 0 And Me.lstMonitor.ListItems.Count > 0 Then
      If Me.Timer1.Enabled Then
         Me.Timer1.Enabled = False
         Me.ImgStatus.Picture = LoadPicture(PathIcons & "\ServerOffline.ico")
      Else
         Me.Timer1.Enabled = True
         Me.ImgStatus.Picture = LoadPicture(PathIcons & "\ServerOnline.ico")
      End If
   End If
End Sub

Private Sub Timer1_Timer()
   Dim datLast As Date
   Dim objLstMonitor As ListItem
   
   If Not LoadSystem() Then Me.Timer1.Enabled = False
   
   If Me.lstMonitor.ListItems.Count = 0 Then
      
      lngCountMonitor = lngCountMonitor + 1
      
      Set objLstMonitor = Me.lstMonitor.ListItems.Add(, "K_" & lngCountMonitor, Now, , "GreenSign")
      objLstMonitor.SubItems(1) = Now
      objLstMonitor.SubItems(2) = DateAdd("n", intTime, Now)
      objLstMonitor.Selected = True
      
      If Not StartSystem() Then
         Me.Timer1.Enabled = False
         Me.ImgStatus.Picture = LoadPicture(PathIcons & "\ServerOffline.ico")
         Exit Sub
      End If
   Else
      Set objLstMonitor = Me.lstMonitor.ListItems.Item("K_" & lngCountMonitor)
      If CDate(objLstMonitor.SubItems(2)) < CDate(Now) Then
         
         datLast = objLstMonitor.SubItems(2)
         lngCountMonitor = lngCountMonitor + 1
         
         Set objLstMonitor = Me.lstMonitor.ListItems.Add(, "K_" & lngCountMonitor, Now, , "GreenSign")
         objLstMonitor.SubItems(1) = datLast
         objLstMonitor.SubItems(2) = DateAdd("n", intTime, Now)
         objLstMonitor.Selected = True
         
         If Not StartSystem() Then
            Me.Timer1.Enabled = False
            Me.ImgStatus.Picture = LoadPicture(PathIcons & "\ServerOffline.ico")
            Exit Sub
         End If
      End If
   End If
End Sub
