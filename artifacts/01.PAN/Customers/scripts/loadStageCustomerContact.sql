-- 1. clean up previous work
--
IF OBJECT_ID('tempdb.dbo.#TMP_FUNCTIONAL', 'U') IS NOT NULL
  DROP TABLE dbo.#TMP_FUNCTIONAL;

IF OBJECT_ID('tempdb.dbo.#TMP_UNIQUE_FUNCTIONAL', 'U') IS NOT NULL
  DROP TABLE dbo.#TMP_UNIQUE_FUNCTIONAL;

-- 2. filters source with functional parameters
--
-- (TBD @see example from VENDORS)

-- 3. avoids duplicates on filtered group
--
-- (TBD @see example from VENDORS)

-- 4. truncates staging table
--
TRUNCATE TABLE STG_CUSTOMER_CONTACT;


-- 5. loads data into stage table
-- CLEANSED state is always D (dirty)

INSERT INTO STG_CUSTOMER_CONTACT WITH (TABLOCK)
	(
	VALIDATED,
	CLEANSED,
	RECOMMENDED,
	ORIG_SYSTEM_CUSTOMER_REF,
	ORIG_SYSTEM_ADRESS_REFERENCE,
	ORIG_SYSTEM_CONTACT_REF,
	ORIG_SYSTEM_TELEPHONE_REF,
	CONTACT_FIRST_NAME,
	CONTACT_LAST_NAME,
	TELEPHONE_TYPE,
	TELEPHONE,
	EMAIL_ADDRESS,
	ORG_NAME
	)
SELECT
	'R',
	'D',
	'Y',
	'PA' + RTRIM(LTRIM(CAST(CLIENTES.CLIENTE AS varchar(30)))) AS ORIG_SYSTEM_CUSTOMER_REF,
	'PA' + RTRIM(LTRIM(CAST(CLIENTES.CLIENTE AS varchar(30)))) + '-CA' AS ORIG_SYSTEM_ADRESS_REFERENCE,
	'PA' + RTRIM(LTRIM(CAST(CLIENTES.CLIENTE AS varchar(30)))) + '-CC' AS ORIG_SYSTEM_ADRESS_REFERENCE,
	'PA' + RTRIM(LTRIM(CAST(CLIENTES.CLIENTE AS varchar(30)))) + '-CT' AS ORIG_SYSTEM_ADRESS_REFERENCE,
	UPPER(LEFT(SUBSTRING(CONTACTO, 1, CHARINDEX(' ', CONTACTO, 1)), 60)),
	UPPER(LEFT(SUBSTRING(CONTACTO, CHARINDEX(' ', CONTACTO, 1)+1, LEN(CONTACTO) - CHARINDEX(CONTACTO, ' ', 1)), 60)),
	'PHONE',
	ISNULL(LEFT(CLIENTES.TELEFONO,25), '<unknown'),
	CLIENTES.EMAIL,
	'THYSSENKRUPP ELEVADORES S.A. (PANAMA)' AS ORG_NAME
FROM
CLIENTES
ORDER BY CLIENTES.CLIENTE

-- 6.  UPDATING recommended flag
--
-- (TBD @see example from VENDORS)
 
-- 7. do some sanity-checks
--
SELECT COUNT(*) AS ALL_CLIENTES_COUNT FROM CLIENTES

SELECT COUNT(*) AS ALL_STG_CUSTOMERS_COUNT FROM STG_CUSTOMER_HEADER

SELECT COUNT(*) AS ALL_STG_COUNT FROM STG_CUSTOMER_CONTACT
SELECT COUNT(*) AS ALL_STG_RECOMMENDED FROM STG_CUSTOMER_CONTACT WHERE RECOMMENDED = 'Y'



