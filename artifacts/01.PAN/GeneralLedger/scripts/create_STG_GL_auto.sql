IF OBJECT_ID('dbo.STG_GL', 'U') IS NOT NULL
  DROP TABLE dbo.STG_GL;

CREATE TABLE [dbo].[STG_GL](
    [FLEXFIELD_SEGMENTS] [VARCHAR](150) NOT NULL,
    [ENTERED_DR] NUMERIC(30,15) NULL,
    [ENTERED_CR] NUMERIC(30,15) NULL,
    [ACCOUNTING_DATE] [VARCHAR](11) NOT NULL,
    [REFERENCE1_BATCH_NAME] [VARCHAR](100) NOT NULL,
    [REFERENCE2_BATCH_DESCRIPTION] [VARCHAR](240) NULL,
    [REFERENCE4_JOURNAL_NAME] [VARCHAR](100) NOT NULL,
    [REFERENCE5_JOUNAL_DESCRIPTION] [VARCHAR](240) NULL,
    [REFERENCE10_JOURNAL_LINE_DESCRIPTION] [VARCHAR](240) NULL,
    [CURRENCY_CODE] [VARCHAR](15) NOT NULL,
    [ORG_NAME] [VARCHAR](60) NOT NULL

)
