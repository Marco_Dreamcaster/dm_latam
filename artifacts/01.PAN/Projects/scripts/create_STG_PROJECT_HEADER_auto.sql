IF OBJECT_ID('dbo.STG_PROJECT_HEADER', 'U') IS NOT NULL
  DROP TABLE dbo.STG_PROJECT_HEADER;

CREATE TABLE [dbo].[STG_PROJECT_HEADER](
    [VALIDATED] [VARCHAR](1) NOT NULL,
    [RECOMMENDED] [VARCHAR](1) NOT NULL,
    [CLEANSED] [VARCHAR](1) NOT NULL,
    [PM_PROJECT_REFERENCE] [VARCHAR](25) NOT NULL,
    [PROJECT_NAME] [VARCHAR](30) NOT NULL,
    [PROJECT_TYPE] [VARCHAR](20) NOT NULL,
    [PROJECT_STATUS] [VARCHAR](30) NOT NULL,
    [TEMPLATE_NAME] [VARCHAR](30) NOT NULL,
    [START_DATE] DATE NOT NULL,
    [COMPLETION_DATE] DATE NOT NULL,
    [DESCRIPTION] [VARCHAR](250) NULL,
    [ORGANIZATION_CODE] [VARCHAR](30) NOT NULL,
    [CUSTOMER_NUMBER] [VARCHAR](30) NOT NULL,
    [BILL_TO_ADDRESS_REF] [VARCHAR](240) NOT NULL,
    [SHIP_TO_ADDRESS_REF] [VARCHAR](240) NOT NULL,
    [CONTACT_REF] [VARCHAR](240) NULL,
    [PROJECT_MANAGER_REF] [VARCHAR](30) NOT NULL,
    [CREDIT_RECEIVER_REF] [VARCHAR](30) NULL,
    [SALES_REPRESENTATIVE_REF] [VARCHAR](30) NULL,
    [DUMMY_KEY_MEMBER_REF] [VARCHAR](30) NULL,
    [ATTRIBUTE2] [VARCHAR](150) NULL,
    [ATTRIBUTE6] NUMERIC(10,5) NULL,
    [ATTRIBUTE8] NUMERIC(10,5) NULL,
    [BOOKING_PKG_RCVD] DATE NULL,
    [COMPLETE_PCKG_RCVD_DATE] DATE NULL,
    [CONTRACT_BOOKED_DATE] DATE NULL,
    [CONTRACT_DATE] DATE NULL,
    [PACKAGE_AND_PO_SALES] DATE NULL,
    [EXECUTION_AND_RETURN_DATE] DATE NULL,
    [CONTRACT_TYPE_TKE] [VARCHAR](1) NULL,
    [CUSTOMER_PO_NUMBER] [VARCHAR](30) NULL,
    [OUTPUT_TAX_CODE] [VARCHAR](30) NULL,
    [ORG_NAME] [VARCHAR](240) NOT NULL

PRIMARY KEY CLUSTERED
(
    [PM_PROJECT_REFERENCE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
