# DM_LATAM

Data Migration LATAM repository Artifacts

# Folder Naming Convention

Each folder is named with a 2-digit number indicating the order of migration schedule, followed by 3-letter country


# Prototype

Folder structure within each country is described under **commons/proptotype/README.ME**
