Attribute VB_Name = "App"
Option Explicit

Public Const COMMONS_VERSION = "0.8.5.a"

' important index definitions

Public Const CONFIG_VALUES_COLUMN = 2

Public Const HEADER_ROW = 1
Public Const FIRST_DATA_ROW = 2

Public Const CONFIG_STAGE_TABLE_NAME = 1
Public Const CONFIG_FLAT_FILE_TEMPLATE_NAME = 2
Public Const CONFIG_OUTPUT_MULTIPLIER = 3
Public Const CONFIG_LOADING_PROCEDURE = 4

Public Const CONFIG_DB_CONNECTOR_STRING = 1
Public Const CONFIG_FLATFILE_OUTPUT_PATH = 2
Public Const CONFIG_DDL_OUTPUT_PATH = 3
Public Const CONFIG_COMMONS_VERSION = 9
Public Const CONFIG_COMMONS_PATH = 4
Public Const CONFIG_RECOMMENDED_TAGS = 12
Public Const CONFIG_VALIDATION_TAGS = 14
Public Const CONFIG_CLEANSED_TAGS = 13

' app - level configurations
'
Public szConfigDbConnector As String
Public szFlatFileOutputPat As String
Public szDDLOutputPath As String
Public szCommonsFilePath As String

' important columns on Worksheet
'
Public Const VALIDATED = 1
Public Const Action = 2
Public Const RECOMMENDED = 3
Public Const CLEANSED = 4

' important columns on Staging Tables
'
Public Const STG_VALIDATED_COL = 0
Public Const STG_RECOMMENDED_COL = 1
Public Const STG_CLEANSED_COL = 2

Public Const STG_ADJUSTED_INDEX = 2 'indexes on stage table and worksheet are shifted by 2

' App Errors
'
Public Const ERROR_NONE = 26 + 513
Public Const ERROR_GENERIC = 29 + 513

Public Const ERROR_MISSING_MANDATORY = 1 + 513     '513 is VBA's lower bound for custom errors (so much for no magic numbers)
Public Const ERROR_DIRTY_RECORD = 2 + 513
Public Const ERROR_MISSING_CLEANSED = 3 + 513
Public Const ERROR_MAX_LENGTH_EXCEEDED = 4 + 513
Public Const ERROR_MISSING_CONFIGURATIONS = 10 + 513
Public Const ERROR_DATASHEET = 11 + 513
Public Const ERROR_NOT_A_DATE = 12 + 513
Public Const ERROR_NOT_A_NUMBER = 13 + 513
Public Const ERROR_CONSTANT_MAX_LENGTH_EXCEEDED = 14 + 513
Public Const ERROR_UNSUPPORTED_LANGUAGE_PACK = 15 + 513
Public Const ERROR_MAL_FORMED_FK = 16 + 513
Public Const ERROR_NOT_IMPLEMENTED = 17 + 513
Public Const ERROR_MISSING_TABLE_FK = 18 + 513
Public Const ERROR_MISSING_FIELD_FK = 19 + 513
Public Const ERROR_MISSING_VALUE_FK = 20 + 513
Public Const ERROR_INVALID_PARENT_FK = 21 + 513
Public Const ERROR_MORE_THAN_ONE_PARENT_FK = 22 + 513
Public Const ERROR_INVALID_TELEPHONE_TYPE = 23 + 513
Public Const ERROR_MISSING_TELEPHONE_FOR_TELEPHONE_OR_FAX = 24 + 513
Public Const ERROR_MISSING_EMAIL = 25 + 513
Public Const ERROR_INVALID_LOCATOR = 27 + 513
Public Const ERROR_INVALID_SCC = 28 + 513
Public Const ERROR_GEOGRAPHY_CODE = 30 + 513
Public Const ERROR_MULTIPLE_PRIMARY_SITE_USE_FLAG = 31 + 513
Public Const ERROR_NOT_IN_LOV = 32 + 513
Public Const ERROR_UNIQUENESS_VIOLATION = 33 + 513


' errors for CustomValidation

Public CustomValidationErrorCode As Integer
Public CustomValidationMessage As String


' controls when validations are triggered
'
Public DisabledValidations As Boolean
Public IgnoreNextValueChange As Boolean

Public configKeys As Collection
Public dataKeys As Collection
Public transactionalKeys As Collection

Dim globalRibbon As IRibbonUI
Dim DM_LATAM_Tag As String

' metrics dictionary
Public dicMetrics As Scripting.Dictionary

' services (wired as Singletons)
'
Public svcXLConfigurator As New XLConfiguratorService
Public svcSynchronizer As New SynchronizerService
Public svcGenerator As New GeneratorService
Public svcValidator As New ValidatorService

Public Sub CleansedFirst(Optional Control As IRibbonControl, Optional AskConfirmation As Boolean = True)
    
    On Error GoTo eh
    Err.Clear
    
    Dim goAhead As Integer
    
    If (AskConfirmation) Then
        goAhead = MsgBox("Normalization sorts by clenased rows, removes filters.  Are you sure?", vbYesNo + vbQuestion, "Please be patient")
    End If
    
    If goAhead = vbYes Or Not AskConfirmation Then
        AutoConfigure
        
        Set dicMetrics = New Scripting.Dictionary
        dicMetrics.RemoveAll
        
        Dim szShortName As Variant
        For Each szShortName In svcValidator.DescriptorsByShortName.Keys
            Application.StatusBar = "Moving cleansed to top on : " & szShortName

            Dim descriptor As CleansingDescriptorVO
            Set descriptor = svcValidator.DescriptorsByShortName(szShortName)
            
            svcXLConfigurator.NormalizeDataView descriptor, dicMetrics
                                    
        Next szShortName
        
        
        MsgBox BuildMetrics
        
        ResetView
    End If
    
    Exit Sub

eh:
    Debug.Print Err
    
End Sub


Private Function BuildMetrics()
    Dim infoJSON As String
    infoJSON = "{" & ActiveWorkbook.Name & " :{" & vbCrLf
    
    Dim dataKey As Variant
    For Each dataKey In dicMetrics.Keys
         infoJSON = infoJSON & dataKey & ": " & dicMetrics(dataKey) & "," & vbCrLf
    Next
    infoJSON = Left(infoJSON, Len(infoJSON) - 1)
    infoJSON = infoJSON & "}}"

    BuildMetrics = infoJSON
    
End Function



Public Sub TrimDirty(Optional Control As IRibbonControl, Optional AskConfirmation As Boolean = True)
    
    On Error GoTo eh
    Err.Clear
    
    Dim goAhead As Integer
    
    If (AskConfirmation) Then
        goAhead = MsgBox("Trimming will remove dirty rows from sheet. Cleansed rows are not afected.  Are you sure?", vbYesNo + vbQuestion, "Please be patient")
    End If
    
    If (AskConfirmation And goAhead = vbYes) Then
        goAhead = MsgBox("Restoring dirty rows is only possible again by running an incremental load.  Show we proceed with trimming?", vbYesNo + vbQuestion, "Please be patient")
    End If
    
    If goAhead = vbYes Or Not AskConfirmation Then
        AutoConfigure
        
        Set dicMetrics = New Scripting.Dictionary
        dicMetrics.RemoveAll
        
        Dim szShortName As Variant
        For Each szShortName In svcValidator.DescriptorsByShortName.Keys
            Application.StatusBar = "Trimming Dirty from : " & szShortName
            
            Dim descriptor As CleansingDescriptorVO
            Set descriptor = svcValidator.DescriptorsByShortName(szShortName)
            
            svcXLConfigurator.TrimDataView descriptor, dicMetrics
                                             
        Next szShortName
        
        MsgBox BuildMetrics
        
        ResetView
    End If
    
    Exit Sub

eh:
    Debug.Print Err
    
End Sub


' entrypoint to validations
' to be used by Worksheet_OnUpdate of Data Sheets
' automatically performs initialization and global validation (if needed)
'
Public Sub ApplyFieldValidationsFor(ByVal VOName As String, ByVal Target As Range)
    ' it's safe to ignore changes
    ' for bulk operations or post-processing
    '
    If (Not DisabledValidations) And (Not IgnoreNextValueChange) Then
    
        If (App.svcValidator.DescriptorsByShortName Is Nothing) Then
            AutoConfigure
        End If
        
        ' only validators that have data sheets named correctly are added
        '
        If (App.svcValidator.DescriptorsByShortName.Exists(VOName)) Then
        
            Dim VODescriptor As CleansingDescriptorVO
            Set VODescriptor = App.svcValidator.DescriptorsByShortName(VOName)
        
            Dim FieldDescriptorsByColIdx As Scripting.Dictionary
            Set FieldDescriptorsByColIdx = VODescriptor.DescriptorsByColIdx
            
            ' performs validation only when cleansed state or a monitored, non-header value has been changed
            If (Target.Column = CLEANSED) And (Target.row <> HEADER_ROW) Or (FieldDescriptorsByColIdx.Exists(Target.Column)) Then
                App.svcValidator.CheckEntireRow VODescriptor.DataSheetIdx, Target.row, FieldDescriptorsByColIdx
                
            End If
        Else
            Err.Raise ERROR_MISSING_CONFIGURATIONS, "App module", "missing configurations for data sheet " & VOName
        
        End If
    
    End If
    
    IgnoreNextValueChange = False
End Sub

' full validation is still problematic in terms of speed
'
Public Sub ValidateAll(Optional Control As IRibbonControl, Optional AskConfirmation As Boolean = True)
    On Error GoTo eh
    Err.Clear
    
    Dim goAhead As Integer
    
    If (AskConfirmation) Then
        goAhead = MsgBox("Full validation may take a few minutes.  Are you sure?", vbYesNo + vbQuestion, "Please be patient")
    End If
    
    If goAhead = vbYes Or Not AskConfirmation Then
        AutoConfigure
        
        Dim szShortName As Variant
        For Each szShortName In svcValidator.DescriptorsByShortName.Keys
            Dim descriptor As CleansingDescriptorVO
            Set descriptor = svcValidator.DescriptorsByShortName(szShortName)
            
            svcValidator.CheckAllRows descriptor.DataSheetIdx, descriptor.DescriptorsByColIdx
                                    
        Next szShortName
        
        ResetView
    End If
    
    Exit Sub

eh:
    MsgBox Err.Number & " : " & Err.Description
    Err.Clear

End Sub


' validates a single row
'
Public Sub ValidateRow(Optional Control As IRibbonControl)
    On Error GoTo eh
    Err.Clear
    
    If (App.svcValidator.DescriptorsBySheetIdx Is Nothing) Then
        AutoConfigure
    End If
    
    
    If (ActiveCell.row <> HEADER_ROW) And (svcValidator.DescriptorsBySheetIdx.Exists(ActiveSheet.Index)) Then
        Dim VODescriptor As CleansingDescriptorVO
        Set VODescriptor = svcValidator.DescriptorsBySheetIdx(ActiveSheet.Index)
        
        svcValidator.CheckEntireRow ActiveSheet.Index, _
                                    ActiveCell.row, _
                                    VODescriptor.DescriptorsByColIdx
        
        
        Application.StatusBar = "validated single row " & ActiveCell.row & " of " & VODescriptor.shortName
        
    End If
    
    
    Exit Sub

eh:
    MsgBox Err.Number & " : " & Err.Description
    Err.Clear

End Sub


' validates and triggers DDL validation
' with reminder to user
Public Sub TriggerDDL(Control As IRibbonControl)
    On Err GoTo eh
    Err.Clear

    GenerateAllDDL

    ResetView
    
    MsgBox "Success! Generated " & svcValidator.DescriptorsByShortName.count & " DDL files" & vbNewLine & "Look at folder " & szDDLOutputPath
    Exit Sub
    
eh:
    MsgBox "there was an error " & Err.Number & " - " & Err.Description
End Sub

'convenience method to generate all flat files without user intervention
'
Public Sub TriggerFlatFileSilent()
    App.GenerateAllFlatFiles True, False
    
End Sub

'convenience method to generate all flat files with user intervention
'
Public Sub TriggerFlatFile(Control As IRibbonControl)
    On Err GoTo eh
    Err.Clear

    Dim ValidateBefore As Integer
    ValidateBefore = MsgBox("It is recommended you do a full validation before generating files, but this might take a while.  Proceed with validation?", vbYesNo + vbQuestion, "Please be patient")

    App.GenerateAllFlatFiles IIf(ValidateBefore = vbYes, True, False)

    MsgBox "Success! Generated " & svcValidator.DescriptorsByShortName.count & " flat files" & vbNewLine & "Look at folder " & App.szFlatFileOutputPat
    
    ResetView
    
    Exit Sub

eh:
    MsgBox "there was an error : " & Err.Number & " - " & Err.Description
End Sub




'convenience method to PULL from latest version of Commons
'
Public Sub FromCommons(Optional Control As IRibbonControl, Optional ShowConfirmation As Boolean = True)
    On Err GoTo eh
    Err.Clear
    
    VBAConfigurator.FromCommons
    
    ResetView
    
    If (ShowConfirmation) Then
        MsgBox "imported Commons " + App.COMMONS_VERSION + " modules from " & App.szCommonsFilePath
    End If

    Exit Sub

eh:
    MsgBox "there was an error : " & Err.Number & " - " & Err.Description
End Sub

Public Sub FromCommonsSilent()
    FromCommons , False
End Sub

'convenience method to PUSH to Commons as latest
'
Public Sub ToCommons(Optional Control As IRibbonControl, Optional ShowConfirmation As Boolean = True)
    On Err GoTo eh
    Err.Clear
    
    VBAConfigurator.ToCommons
    
    ResetView
    
    If (ShowConfirmation) Then
        MsgBox "exported monitored " + App.COMMONS_VERSION + " modules to " & App.szCommonsFilePath
    End If

    Exit Sub

eh:
    MsgBox "there was an error : " & Err.Number & " - " & Err.Description
End Sub


' performs a full load operation if the user confirms
'
Public Sub PerformFullLoad(Control As IRibbonControl)
    Dim goAhead As Integer
    goAhead = MsgBox("Full load destroys work done by USER.  Are you sure?", vbYesNo + vbQuestion, "Please be careful")
    
    If (goAhead = vbYes) Then
        goAhead = MsgBox("All records will be reloaded from staging tables.  Current state will be lost.  Proceed ?", vbYesNo + vbQuestion, "Last warning")
       
        If (goAhead = vbYes) Then
            svcSynchronizer.FullLoad
        
            ResetView
        End If
    End If

End Sub


' performs an incremental load
'
Public Sub PerformIncrementalLoad(Control As IRibbonControl)
    Dim goAhead As Integer
    goAhead = MsgBox("Incremental load needs to connect to the database, and might take some time.  Are you sure?", vbYesNo + vbQuestion, "Please be patient")
    
    If (goAhead = vbYes) Then
        Dim InvolvedStagingTableNames As Collection
        Dim NotificationMessage As String
        
        NotificationMessage = "Cleansing work stage has been recorded at staging tables : "
        
        Set InvolvedStagingTableNames = svcSynchronizer.IncrementalLoad()
        
        Dim TableName As Variant
        
        For Each TableName In InvolvedStagingTableNames
            NotificationMessage = NotificationMessage + TableName + " ,"
        Next TableName
        
        MsgBox (Left(NotificationMessage, Len(NotificationMessage) - 1))
    
        ResetView
    End If

End Sub

Public Sub PerformKPILoad()
    svcSynchronizer.KPILoad
End Sub


' hides undesirable sheets and columns from USER
'
Public Sub SwitchToUserMode(Control As IRibbonControl)
    If (configKeys Is Nothing) Then
        AutoConfigure
    End If
    
    Sheets(TextHandler.VERSION).visible = xlSheetHidden
    Sheets(TextHandler.CONFIG_COMMONS).visible = xlSheetHidden
    
    Dim configKey As Variant
    For Each configKey In configKeys
        Dim wksConfig As Worksheet
        
        Set wksConfig = Sheets(configKey)
        wksConfig.visible = xlSheetHidden
        
    Next configKey
    
    RefreshRibbon Tag:="DM_LATAM_USER"
    
    MsgBox "Welcome, USER!  This is the kyosk mode!"

End Sub


Public Sub SwitchToDEVConfig()
    Dim wksConfigCommons As Worksheet
    Set wksConfigCommons = Sheets(TextHandler.CONFIG_COMMONS)
    
    On Error GoTo eh
    Err.Clear
    
    Dim wksPROD As Worksheet
    Set wksPROD = Sheets(TextHandler.CONFIG_COMMONS_PROD)
    
    Exit Sub
    
eh:
    
    Dim wksDEV As Worksheet
    Set wksDEV = Sheets(TextHandler.CONFIG_COMMONS_DEV)

    wksConfigCommons.Name = TextHandler.CONFIG_COMMONS_PROD
    wksDEV.Name = TextHandler.CONFIG_COMMONS

End Sub

Public Sub SwitchToPRODConfig()
    Dim wksConfigCommons As Worksheet
    Set wksConfigCommons = Sheets(TextHandler.CONFIG_COMMONS)
    
    On Error GoTo eh
    Err.Clear
    
    Dim wksDEV As Worksheet
    Set wksDEV = Sheets(TextHandler.CONFIG_COMMONS_DEV)
    
    Exit Sub
    
eh:
    
    Dim wksPROD As Worksheet
    Set wksPROD = Sheets(TextHandler.CONFIG_COMMONS_PROD)
    
    wksConfigCommons.Name = TextHandler.CONFIG_COMMONS_DEV
    wksPROD.Name = TextHandler.CONFIG_COMMONS

End Sub




' shows all sheets and columns to DEV
'
Public Sub SwitchToDevMode(Control As IRibbonControl)
    Dim ConfirmationSentence As String
    
    ConfirmationSentence = InputBox(Prompt:="Backstage mode is only for developers!", Title:="Off limits to USERS")
    
    If LCase(ConfirmationSentence) <> "qwerty" Then
        Exit Sub
    End If


    If (configKeys Is Nothing) Then
        AutoConfigure
    End If
    
    Sheets(TextHandler.VERSION).visible = xlSheetVisible
    Sheets(TextHandler.CONFIG_COMMONS).visible = xlSheetVisible
    
    Dim configKey As Variant
    For Each configKey In configKeys
        Dim wksConfig As Worksheet
        
        Set wksConfig = Sheets(configKey)
        wksConfig.visible = xlSheetVisible
        
    Next configKey
    
    RefreshRibbon Tag:=""

    MsgBox "Welcome, DEV!  This is the backstage mode!"


End Sub

' performs auto configuration
'
Public Sub AutoConfigure()

    ' 1. auto-detection of sheets based on naming rules
    
    Set configKeys = New Collection
    Set dataKeys = New Collection
    Set transactionalKeys = New Collection
    
    Dim S As Worksheet
    For Each S In ThisWorkbook.Sheets
        If (TextHandler.isVOConfig(S.Name)) Then
            configKeys.Add S.Name
            
        ElseIf (TextHandler.isVOData(S.Name)) Then
            dataKeys.Add S.Name
            
        ElseIf (TextHandler.isTransactionalData(S.Name)) Then
            transactionalKeys.Add S.Name
            
        End If
    Next S
        
    ' 2. loads App-wide parameters
    '
    szConfigDbConnector = ThisWorkbook.Sheets(TextHandler.CONFIG_COMMONS).UsedRange.Cells(CONFIG_DB_CONNECTOR_STRING, CONFIG_VALUES_COLUMN)
    szFlatFileOutputPat = ThisWorkbook.Sheets(TextHandler.CONFIG_COMMONS).UsedRange.Cells(CONFIG_FLATFILE_OUTPUT_PATH, CONFIG_VALUES_COLUMN)
    szDDLOutputPath = ThisWorkbook.Sheets(TextHandler.CONFIG_COMMONS).UsedRange.Cells(CONFIG_DDL_OUTPUT_PATH, CONFIG_VALUES_COLUMN)
    szCommonsFilePath = ThisWorkbook.Sheets(TextHandler.CONFIG_COMMONS).UsedRange.Cells(CONFIG_COMMONS_PATH, CONFIG_VALUES_COLUMN)
    
    ThisWorkbook.Sheets(TextHandler.CONFIG_COMMONS).UsedRange.Cells(CONFIG_COMMONS_VERSION, CONFIG_VALUES_COLUMN).value = App.COMMONS_VERSION
    
    ' 3. loads cleansing descriptors at VO level
    '
    svcValidator.LoadDescriptorsAtVOLevel dataKeys, configKeys, transactionalKeys
    
    
End Sub

Public Sub LoadStageAndGenerateTransactionalFlatFile(Optional Control As IRibbonControl)
    svcSynchronizer.TransactionalLoad

End Sub

Public Sub GenerateTransactionalFlatFile(Optional Control As IRibbonControl)
    Debug.Print "Generate Only"

End Sub


Public Sub ConfigureXL(Optional Control As IRibbonControl)
    AutoConfigure
    
    Application.ScreenUpdating = False
    
    svcXLConfigurator.FormatDataTables dataKeys
    svcXLConfigurator.ResetConditionalFormatting dataKeys
    
    Application.ScreenUpdating = True
    
    ResetView


End Sub

' Keeping critical actions away from USER reach

Public Sub LoadRibbon(Ribbon As IRibbonUI)
    DM_LATAM_Tag = "DM_LATAM_USER"
    Set globalRibbon = Ribbon
End Sub

Sub GetVisible(Control As IRibbonControl, ByRef visible)
    If DM_LATAM_Tag = "" Then
        visible = True
    Else
        If Control.Tag Like DM_LATAM_Tag Then
            visible = True
        Else
            visible = False
        End If
    End If
End Sub

Sub RefreshRibbon(Tag As String)
    DM_LATAM_Tag = Tag
    If globalRibbon Is Nothing Then
        MsgBox "Error, Save/Restart your workbook"
    Else
        globalRibbon.Invalidate
    End If
End Sub

' generates Flat files for all detected VO specifications on the workbook
' checks all rows before generating flat file, only validated records remain in the output
'
Public Sub GenerateAllFlatFiles(Optional ValidateBefore As Boolean = True, Optional UserFeedback As Boolean = True)
    If (svcValidator.DescriptorsByShortName Is Nothing) Then
        AutoConfigure
    End If
    
    Dim szShortName As Variant
    For Each szShortName In svcValidator.DescriptorsByShortName.Keys
        Dim descriptor As CleansingDescriptorVO
        Set descriptor = svcValidator.DescriptorsByShortName(szShortName)
    
        If (ValidateBefore) Then
            svcValidator.CheckAllRows descriptor.DataSheetIdx, descriptor.DescriptorsByColIdx, UserFeedback
        End If
    
        Dim absolutePath As String
        absolutePath = TextHandler.buildFlatFilePathFor(descriptor.shortName, descriptor.FlatFileTemplateName, szFlatFileOutputPat)
        
        svcGenerator.GenerateFlatFile absolutePath, Worksheets(descriptor.DataSheetIdx).UsedRange, descriptor.DescriptorsByColIdx, descriptor.OutputMultiplier, UserFeedback
                                
    Next szShortName
End Sub

' generates DDL for all detected VO specifications on the workbook
'
Private Sub GenerateAllDDL()
    AutoConfigure
    
    Dim szShortName As Variant
    For Each szShortName In svcValidator.DescriptorsByShortName.Keys
        Dim descriptor As CleansingDescriptorVO
        Set descriptor = svcValidator.DescriptorsByShortName(szShortName)
    
        svcGenerator.GenerateDDL descriptor.stgTableName, _
                                TextHandler.buildDDLPathFor(descriptor.stgTableName, szDDLOutputPath), _
                                descriptor.DescriptorsByColIdx, _
                                descriptor.GetPrimaryKeys, _
                                descriptor.IsAnyPrimaryDefined, _
                                descriptor.Transactional
                                
    Next szShortName
    
End Sub

Private Sub ResetView()
    Worksheets(svcValidator.MinRegisteredDataIdx).Activate
End Sub















