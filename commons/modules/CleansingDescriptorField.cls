VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CleansingDescriptorField"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' version 0.7

' Holds cleansing parameters on VO field-level
'
Private pFieldName As String
Private pColumnNumber As Integer
Private pMandatory As Boolean
Private pCustomValidation As String
Private pPostProcessing As String
Private pFormat As String
Private pPrimary As Boolean
Private pLocked As Boolean
Private pHidden As Boolean
Private pConstant As String
Private pLOV As String
Private pFK As String
Private pIsOutputValue As Boolean
Private pOutputPos As Integer
Private pORAType As String

' calculated based on ORAType
'
Private pIsVarchar As Boolean
Private pIsNumber As Boolean
Private pIsDate As Boolean
Private pMaxLength As Integer




' ORA_TYPE has priority over any maxLength definition
'
Public Function MaxLengthNeedsFixing() As Boolean
    MaxLengthNeedsFixing = False

    If InStr(pORAType, "VARCHAR2") <> 0 Then
        Dim iLength As Integer
        iLength = extractLength(pORAType)
        
        If (iLength <> pMaxLength) Then
            pMaxLength = iLength
            MaxLengthNeedsFixing = True
        End If
    End If

End Function

' translates the current field to Transact-SQL DDL
'
Public Property Get SSQL_DDL() As String
    Dim result As String

    If IsVarchar Then
        result = "[" & pFieldName & "] [VARCHAR](" & pMaxLength & ") "
    
    ElseIf InStr(pORAType, "RUC") Then
        result = "[" & pFieldName & "] [VARCHAR](20) "
    
    ElseIf IsNumber Then
        result = "[" & pFieldName & "] NUMERIC(30,15) "
        
    ElseIf IsDate Then
        result = "[" & pFieldName & "] DATE "
    End If
    
    SSQL_DDL = result & IIf(pMandatory, "NOT NULL", "NULL")
End Property

Public Property Get IsNumber() As Boolean
    IsNumber = pIsNumber
End Property

Public Property Get IsDate() As Boolean
    IsDate = pIsDate
End Property

Public Property Get IsVarchar() As Boolean
    IsVarchar = pIsVarchar
End Property


Public Property Get Primary() As String
    Primary = pPrimary
End Property

Public Property Let Primary(value As String)
    pPrimary = value
End Property

Public Property Get LOV() As String
    LOV = pLOV
End Property

Public Property Let LOV(value As String)
    pLOV = value
End Property

Public Property Get FK() As String
    FK = pFK
End Property

Public Property Let FK(value As String)
    pFK = value
End Property

Public Property Get ORAType() As String
    ORAType = pORAType
End Property

Public Property Let ORAType(value As String)
    If InStr(value, "VARCHAR2") <> 0 Then
        pIsVarchar = True
        pIsDate = False
        pIsNumber = False
    
    ElseIf InStr(value, "RUC") Then
        pIsVarchar = True
        pIsDate = False
        pIsNumber = False
    
    ElseIf InStr(value, "NUMBER") Then
        pIsVarchar = False
        pIsDate = False
        pIsNumber = True
    
    ElseIf InStr(value, "DATE") Then
        pIsVarchar = False
        pIsDate = True
        pIsNumeric = False
    
    End If
    
    pORAType = value
End Property

Public Property Get Locked() As Boolean
    Locked = pLocked
End Property

Public Property Let Locked(value As Boolean)
    pLocked = value
End Property

Public Property Get Hidden() As Boolean
    Hidden = pHidden
End Property

Public Property Let Hidden(value As Boolean)
    pHidden = value
End Property

Public Property Get Constant() As String
    Constant = pConstant
End Property

Public Property Let Constant(value As String)
    pConstant = value
End Property


Public Property Get Format() As String
    Format = pFormat
End Property

Public Property Let Format(value As String)
    pFormat = value
End Property

Public Property Get fieldName() As String
    fieldName = pFieldName
End Property

Public Property Let fieldName(value As String)
    pFieldName = value
End Property

Public Property Get ColumnNumber() As Integer
    ColumnNumber = pColumnNumber
End Property

Public Property Let ColumnNumber(value As Integer)
    pColumnNumber = value
End Property

Public Property Get Mandatory() As Boolean
    Mandatory = pMandatory
End Property

Public Property Let Mandatory(value As Boolean)
    pMandatory = value
End Property

Public Property Get MaxLength() As Integer
    MaxLength = pMaxLength
End Property

Public Property Let MaxLength(value As Integer)
    pMaxLength = value
End Property

Public Property Get CustomValidation() As String
    CustomValidation = pCustomValidation
End Property

Public Property Let CustomValidation(value As String)
    pCustomValidation = value
End Property

Public Property Get PostProcessing() As String
    PostProcessing = pPostProcessing
End Property

Public Property Let PostProcessing(value As String)
    pPostProcessing = value
End Property

Public Property Get IsOutputValue() As Boolean
    IsOutputValue = pIsOutputValue
End Property

Public Property Let IsOutputValue(value As Boolean)
    pIsOutputValue = value
End Property

Public Property Get OutputPos() As Integer
    OutputPos = pIsOutputPos
End Property

Public Property Let OutputPos(value As Integer)
    pOutputPos = value
End Property

Private Function extractLength(value As String) As Integer
        Dim chLBound, chUBound As Integer
        
        chLBound = InStr(value, "(")
        chUBound = InStr(value, ")")
        
        Dim sLen As String
        
        sLen = Mid(value, chLBound + 1, chUBound - chLBound - 1)
        
        If IsNumeric(sLen) Then
            extractLength = CInt(sLen)
        Else
            extractLength = 0
        End If

End Function
