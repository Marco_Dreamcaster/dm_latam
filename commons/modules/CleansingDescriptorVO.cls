VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CleansingDescriptorVO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'v.0.8.0

' Holds cleansing parameters on VO-level
'
Private pDataSheetIdx As Integer
Private pStgTableName As String
Private pShortName As String
Private pFlatFileTemplateName As String
Private pOutputMultiplier As String
Private pLoadingProcedure As String
Private pTransactional As Boolean

Private pDescriptorsByColIdx As Scripting.Dictionary
Private pDescriptorsByColName As Scripting.Dictionary

Public Function MaxRegisteredColum() As Integer
    MaxRegisteredColum = 1
    
    For Each descriptor In pDescriptorsByColIdx.Items
        If (descriptor.ColumnNumber > MaxRegisteredColum) Then
            MaxRegisteredColum = descriptor.ColumnNumber
        End If
    
    Next descriptor

End Function


Public Function IsAnyPrimaryDefined() As Boolean

    IsAnyPrimaryDefined = False

    For Each descriptor In pDescriptorsByColIdx.Items
        If (descriptor.Primary) Then
            IsAnyPrimaryDefined = True
            Exit Function
        End If
    
    Next descriptor

End Function

Public Function GetPrimaryKeys() As Collection
    Dim result As New Collection
    For Each descriptor In pDescriptorsByColIdx.Items
        If (descriptor.Primary) Then
            result.Add descriptor
        End If
    Next descriptor
    
    Set GetPrimaryKeys = result

End Function

Public Function DataName() As String
    DataName = TextHandler.DATA_MARKER & pShortName
End Function

Public Function ConfigSheetName() As String
    ConfigSheetName = TextHandler.CONFIG_MARKER & pShortName
End Function

Public Property Get shortName() As String
    shortName = pShortName
End Property

Public Property Let shortName(value As String)
    pShortName = value
End Property

Public Property Get DataSheetIdx() As Integer
    DataSheetIdx = pDataSheetIdx
End Property

Public Property Let DataSheetIdx(value As Integer)
    pDataSheetIdx = value
End Property

Public Property Get FlatFileTemplateName() As String
    FlatFileTemplateName = pFlatFileTemplateName
End Property

Public Property Let FlatFileTemplateName(value As String)
    pFlatFileTemplateName = value
End Property

Public Property Get stgTableName() As String
    stgTableName = pStgTableName
End Property

Public Property Let stgTableName(value As String)
    pStgTableName = value
End Property

Public Property Get DescriptorsByColIdx() As Scripting.Dictionary
    Set DescriptorsByColIdx = pDescriptorsByColIdx
End Property

Public Property Let DescriptorsByColIdx(value As Scripting.Dictionary)
    Set pDescriptorsByColIdx = value
End Property

Public Property Get DescriptorsByColName() As Scripting.Dictionary
    Set DescriptorsByColName = pDescriptorsByColName
End Property

Public Property Let DescriptorsByColName(value As Scripting.Dictionary)
    Set pDescriptorsByColName = value
End Property

Public Property Get OutputMultiplier() As String
    OutputMultiplier = pOutputMultiplier
End Property

Public Property Let OutputMultiplier(value As String)
    pOutputMultiplier = value
End Property

Public Property Get LoadingProcedure() As String
    LoadingProcedure = pLoadingProcedure
End Property

Public Property Let LoadingProcedure(value As String)
    pLoadingProcedure = value
End Property

Public Property Get Transactional() As Boolean
    Transactional = pTransactional
End Property

Public Property Let Transactional(value As Boolean)
    pTransactional = value
End Property

