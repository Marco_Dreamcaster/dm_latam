Attribute VB_Name = "Commons"
' Excel Language codes magic values
Public Const ENGLISH_PACK = 1033
Public Const PORTUGUESE_PACK = 1046
Public Const SPANISH_PACK = 3082


Public Const CUSTOMER_CONTACT_TELEPHONE_COL = 14
Public Const CUSTOMER_CONTACT_EMAIL_COL = 16

Public Const PROJECT_SALES_REPRESENTATIVE_REF = 20

Public ManufacturerIdTable As Dictionary
Public ManufacturerDescriptionTable As Dictionary

Public OracleCountryCodesTable As Dictionary
Public ProjectClassificationsCategoryTable As Dictionary

Public OracleCountriesTable As Dictionary
Public OracleProvincesTable As Dictionary
Public OracleProvinceCodesTable As Dictionary

Public CustomValidationError As Boolean
Public FormattedValue As String

Public dicKeysCounter As Dictionary
Public dicKeysRefresh As Date


Public Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As LongPtr)


Public Sub LoadRecordInto(ByVal wksTarget As Worksheet, _
                                ByVal row As Integer, _
                                ByVal Record As ADODB.Recordset, _
                                ByVal VODescriptor As CleansingDescriptorVO)


    wksTarget.Cells(row, App.RECOMMENDED).value = Record.Fields(App.STG_RECOMMENDED_COL)
    wksTarget.Cells(row, App.CLEANSED).value = "D"
    
    Dim iVOFieldCol As Variant
    For Each iVOFieldCol In VODescriptor.DescriptorsByColIdx.Keys
        Dim VOFieldDescriptor As CleansingDescriptorField
        Set VOFieldDescriptor = VODescriptor.DescriptorsByColIdx(iVOFieldCol)
        
        wksTarget.Cells(row, VOFieldDescriptor.ColumnNumber).value = Record.Fields(VOFieldDescriptor.ColumnNumber - App.STG_ADJUSTED_INDEX)
    
    
    Next iVOFieldCol

 End Sub


' post-processor only changes values if really neeeded
' watch out for circular calls to Worksheet_Change
'
Public Sub toUpper(cell As Range)
    If cell.value <> UCase(cell.value) Then
        App.IgnoreNextValueChange = True
        cell.value = UCase(cell.value)
    End If

End Sub

Public Function validateRUC(RUC As String) As Boolean
    validateRUC = True
    
End Function

Public Function convertToDDMMYYY(ByVal original As String) As String
    If IsDate(original) Then
        FormattedValue = Format(original, "DD-MM-YYYY")
    End If
End Function

Public Sub EnforceORAGeographyCodesForCustomers(ByVal original As Range)
    EnforceOracleGeographyCodes original, "CustomerAddress"
End Sub

Public Sub EnforceORAGeographyCodesForVendors(ByVal original As Range)
    EnforceOracleGeographyCodes original, "VendorSite"
End Sub

Public Sub EnforceOracleGeographyCodes(ByVal original As Range, ByVal shortName As String)
    'loads Oracle Geography codes into dictionaries
    Set OracleCountriesTable = New Dictionary
    OracleCountriesTable.RemoveAll
    
    Set OracleProvincesTable = New Dictionary
    OracleProvincesTable.RemoveAll
    
    Set OracleProvinceCodesTable = New Dictionary
    OracleProvinceCodesTable.RemoveAll
    
    Dim wksOracleCodes As Worksheet
    Set wksOracleCodes = Sheets("Oracle Geography Codes")
    
    Dim rowCount As String
    rowCount = wksOracleCodes.UsedRange.Rows.count
    
    For i = App.FIRST_DATA_ROW To rowCount
        Dim Country As String
        Country = wksOracleCodes.Cells(i, 2)
    
        Dim Province As String
        Province = wksOracleCodes.Cells(i, 3)
        
        Dim ProvinceCode As String
        ProvinceCode = wksOracleCodes.Cells(i, 4)
        
        Dim City As String
        City = wksOracleCodes.Cells(i, 5)
    
        If (OracleCountriesTable.Exists(Country)) Then
            If Not (TextHandler.ExistsIn(Province, OracleCountriesTable(Country))) Then
                OracleCountriesTable(Country).Add Province
            End If
            
        Else
            Dim ProvincesByCountry As Collection
            Set ProvincesByCountry = New Collection
            
            ProvincesByCountry.Add Province
            OracleCountriesTable.Add Country, ProvincesByCountry
        End If
        
        
        If (OracleProvincesTable.Exists(Province)) Then
            OracleProvincesTable(Province) = OracleProvincesTable(Province) + "," + City
        Else
            OracleProvincesTable.Add Province, City
            OracleProvinceCodesTable.Add Province, ProvinceCode
        End If
    Next i
    
       
    'build the LOV for COUNTRY
    Dim CountryLOV As String
    For Each p In OracleCountriesTable.Keys
        If CountryLOV = "" Then
            CountryLOV = p
        Else
            CountryLOV = CountryLOV + "," + p
        End If
    
    Next p
    
    Dim wksData As Worksheet
    Set wksData = Sheets(TextHandler.DATA_MARKER + shortName)
    
    Dim descriptorVO As CleansingDescriptorVO
    Set descriptorVO = App.svcValidator.DescriptorsByShortName(shortName)
    
    Dim descriptorField As CleansingDescriptorField
    Set descriptorField = descriptorVO.DescriptorsByColName("COUNTRY")
        
    Dim dataRange As Range
    Set dataRange = wksData.Range(wksData.Cells(original.row, descriptorField.ColumnNumber), wksData.Cells(original.row, descriptorField.ColumnNumber))

    dataRange.Validation.Delete
    dataRange.Validation.Add Type:=xlValidateList, Formula1:=CountryLOV
    
    If Not (dataRange.Validation.value) Then
        App.CustomValidationErrorCode = App.ERROR_GEOGRAPHY_CODE
        App.CustomValidationMessage = "COUNTRY " + dataRange.Value2 + " is not registered, check Oracle Geography Codes " & original
        Exit Sub
    End If
    
    Country = dataRange.Value2
    
    If (shortName = "VendorSite") Then
        'Apply geographical rules for VENDORs
        Set descriptorField = descriptorVO.DescriptorsByColName("PROVINCE")
        Set dataRange = wksData.Range(wksData.Cells(original.row, descriptorField.ColumnNumber), wksData.Cells(original.row, descriptorField.ColumnNumber))
        
        Province = dataRange.Value2
        
        dataRange.Validation.Delete
        If (OracleCountriesTable.Exists(Country)) Then
            Dim ProvincesLOV As String
            For Each p In OracleCountriesTable(Country)
                If ProvincesLOV = "" Then
                    ProvincesLOV = p
                Else
                    ProvincesLOV = ProvincesLOV + "," + p
                End If
            
            Next p
        
            dataRange.Validation.Add Type:=xlValidateList, Formula1:=ProvincesLOV
            
            If Not (dataRange.Validation.value) And Country = "PA" Then
                App.CustomValidationErrorCode = App.ERROR_GEOGRAPHY_CODE
                App.CustomValidationMessage = "STATE/PROVINCE " + dataRange.Value2 + " is not registered, check Oracle Geography Codes " & original
                Exit Sub
            End If
            
            Set descriptorField = descriptorVO.DescriptorsByColName("STATE")
            Set stateDataRange = wksData.Range(wksData.Cells(original.row, descriptorField.ColumnNumber), wksData.Cells(original.row, descriptorField.ColumnNumber))
            
            If Not (stateDataRange.Value2 = dataRange.Value2) And Country = "PA" Then
                App.CustomValidationErrorCode = App.ERROR_GEOGRAPHY_CODE
                App.CustomValidationMessage = "STATE AND PROVINCE must be the same"
                Exit Sub
            End If
            
            Set descriptorField = descriptorVO.DescriptorsByColName("COUNTY")
            Set stateDataRange = wksData.Range(wksData.Cells(original.row, descriptorField.ColumnNumber), wksData.Cells(original.row, descriptorField.ColumnNumber))
            
            If (OracleProvinceCodesTable(Province) <> stateDataRange.Value2) And Country = "PA" Then
                App.CustomValidationErrorCode = App.ERROR_GEOGRAPHY_CODE
                App.CustomValidationMessage = "COUNTY " + stateDataRange.Value2 + " is not associated to PROVINCE " & Province
                Exit Sub
            End If
     End If
    
    Else
        'Apply geographical rules for CUSTOMERs
    
        Set descriptorField = descriptorVO.DescriptorsByColName("PROVINCE")
        Set dataRange = wksData.Range(wksData.Cells(original.row, descriptorField.ColumnNumber), wksData.Cells(original.row, descriptorField.ColumnNumber))
        
        Province = dataRange.Value2
        
        dataRange.Validation.Delete
        If (OracleCountriesTable.Exists(Country)) Then
            For Each p In OracleCountriesTable(Country)
                If ProvincesLOV = "" Then
                    ProvincesLOV = p
                Else
                    ProvincesLOV = ProvincesLOV + "," + p
                End If
            
            Next p
            dataRange.Validation.Add Type:=xlValidateList, Formula1:=ProvincesLOV
            
            If Not (dataRange.Validation.value) And Country = "PA" Then
                App.CustomValidationErrorCode = App.ERROR_GEOGRAPHY_CODE
                App.CustomValidationMessage = "PROVINCE " + dataRange.Value2 + " is not registered, check Oracle Geography Codes " & original
                Exit Sub
            End If
            
            Set descriptorField = descriptorVO.DescriptorsByColName("COUNTY")
            Set stateDataRange = wksData.Range(wksData.Cells(original.row, descriptorField.ColumnNumber), wksData.Cells(original.row, descriptorField.ColumnNumber))
            
            If (OracleProvinceCodesTable(Province) <> stateDataRange.Value2) And Country = "PA" Then
                App.CustomValidationErrorCode = App.ERROR_GEOGRAPHY_CODE
                App.CustomValidationMessage = "COUNTY " + stateDataRange.Value2 + " is not associated to PROVINCE " & Province
                Exit Sub
            End If
        
            Set descriptorField = descriptorVO.DescriptorsByColName("STATE")
            Set stateDataRange = wksData.Range(wksData.Cells(original.row, descriptorField.ColumnNumber), wksData.Cells(original.row, descriptorField.ColumnNumber))
            
            If (OracleProvinceCodesTable(Province) <> stateDataRange.Value2) And Country = "PA" Then
                App.CustomValidationErrorCode = App.ERROR_GEOGRAPHY_CODE
                App.CustomValidationMessage = "STATE " + stateDataRange.Value2 + " must be the same as COUNTY " & OracleProvinceCodesTable(Province)
                Exit Sub
            End If
            
        
        
        End If
            
                
    End If
    
    Province = dataRange.Value2
    
    If Trim(Province) = "" And Country = "PA" Then
        App.CustomValidationErrorCode = App.ERROR_MISSING_MANDATORY
        App.CustomValidationMessage = "PROVINCE is mandatory for PANAMA addresses "
        Exit Sub
    End If
    
    If Not (dataRange.Validation.value) And Country = "PA" Then
        App.CustomValidationErrorCode = App.ERROR_GEOGRAPHY_CODE
        App.CustomValidationMessage = "PROVINCE " + dataRange.Value2 + " is not registered, check Oracle Geography Codes " & original
        Exit Sub
    End If
    
    
        
    'set the LOV for CITY
    Set descriptorField = descriptorVO.DescriptorsByColName("CITY")
    Set dataRange = wksData.Range(wksData.Cells(original.row, descriptorField.ColumnNumber), wksData.Cells(original.row, descriptorField.ColumnNumber))
    
    dataRange.Validation.Delete
    If (OracleProvincesTable.Exists(Province)) Then
        dataRange.Validation.Add Type:=xlValidateList, Formula1:=OracleProvincesTable(Province)
    Else
        
    End If
   
    If Not (dataRange.Validation.value) And Country = "PA" Then
        App.CustomValidationErrorCode = App.ERROR_GEOGRAPHY_CODE
        App.CustomValidationMessage = "CITY " + dataRange.Value2 + " is not associated to " & original
    End If
    
End Sub

Public Sub CustomCheckCustomerContact(ByVal original As Range)
    Dim wksData As Worksheet
    Set wksData = Sheets("Data - CustomerContact")
    
    Dim VODescriptor As CleansingDescriptorVO
    Set VODescriptor = App.svcValidator.DescriptorsByShortName("CustomerContact")
    
    Dim FieldDescriptor As CleansingDescriptorField
    
    Dim CompositeKey As String
    
    Set dicKeysCounter = New Dictionary
    dicKeysCounter.RemoveAll
    
    For row = App.FIRST_DATA_ROW To wksData.UsedRange.Rows.count
        CompositeKey = wksData.Cells(row, VODescriptor.DescriptorsByColName("ORIG_SYSTEM_CUSTOMER_REF").ColumnNumber) + _
                        wksData.Cells(row, VODescriptor.DescriptorsByColName("ORIG_SYSTEM_ADRESS_REFERENCE").ColumnNumber) + _
                        wksData.Cells(row, VODescriptor.DescriptorsByColName("ORIG_SYSTEM_CONTACT_REF").ColumnNumber) + _
                        wksData.Cells(row, VODescriptor.DescriptorsByColName("ORIG_SYSTEM_TELEPHONE_REF").ColumnNumber)
         
        If Not (dicKeysCounter.Exists(CompositeKey)) Then
            Dim counters As CustomerContactCounters
            Set counters = New CustomerContactCounters
            
            counters.count = 1
            
            dicKeysCounter.Add CompositeKey, counters
        
        Else
            dicKeysCounter(CompositeKey).count = dicKeysCounter(CompositeKey).count + 1
            
        End If
    Next row
    
    CompositeKey = wksData.Cells(original.row, VODescriptor.DescriptorsByColName("ORIG_SYSTEM_CUSTOMER_REF").ColumnNumber) + _
                    wksData.Cells(original.row, VODescriptor.DescriptorsByColName("ORIG_SYSTEM_ADRESS_REFERENCE").ColumnNumber) + _
                    wksData.Cells(original.row, VODescriptor.DescriptorsByColName("ORIG_SYSTEM_CONTACT_REF").ColumnNumber) + _
                    wksData.Cells(original.row, VODescriptor.DescriptorsByColName("ORIG_SYSTEM_TELEPHONE_REF").ColumnNumber)
    
    If (dicKeysCounter(CompositeKey).count > 1) Then
        App.CustomValidationErrorCode = App.ERROR_UNIQUENESS_VIOLATION
        App.CustomValidationMessage = "ORIG_SYSTEM_TELEPHONE_REF " + wksData.Cells(original.row, VODescriptor.DescriptorsByColName("ORIG_SYSTEM_TELEPHONE_REF").ColumnNumber) + " must be unique"
         
    End If
    
End Sub

Public Sub CustomCheckItemHeader(ByVal original As Range)
    CustomCheckInventoryDuplicates original, "ItemHeader"
    
End Sub

Public Sub CustomCheckInventoryDuplicates(ByVal original As Range, VOShortName As String)
    Dim wksData As Worksheet
    Set wksData = Sheets("Data - " + VOShortName)
    
    Dim VODescriptor As CleansingDescriptorVO
    Set VODescriptor = App.svcValidator.DescriptorsByShortName(VOShortName)
    
    Dim FieldDescriptor As CleansingDescriptorField
    
    Dim MaxThresholdDate As Date
    MaxThresholdDate = DateAdd("s", -5, Date)

    
    If (dicKeysRefresh = CDate(0)) Or (dicKeysRefresh < MaxThresholdDate) Then
        dicKeysRefresh = Date
        
        Set dicKeysCounter = New Dictionary
        dicKeysCounter.RemoveAll
        
        Dim itemNumber As String
        
        For row = App.FIRST_DATA_ROW To wksData.UsedRange.Rows.count
            itemNumber = wksData.Cells(row, VODescriptor.DescriptorsByColName("ITEM_NUMBER").ColumnNumber)
            
            dicKeysCounter(itemNumber) = dicKeysCounter(itemNumber) + 1
        Next row
        
        itemNumber = wksData.Cells(original.row, VODescriptor.DescriptorsByColName("ITEM_NUMBER").ColumnNumber)
        
       
    End If
    
    If (dicKeysCounter(itemNumber) > 1) Then
        App.CustomValidationErrorCode = App.ERROR_UNIQUENESS_VIOLATION
        App.CustomValidationMessage = itemNumber + " is not unique"
    End If
    
    
    
End Sub


Public Sub CustomCheckCustomerAddress(ByVal original As Range)
    Dim wksData As Worksheet
    Set wksData = Sheets("Data - CustomerAddress")
    
    Dim VODescriptor As CleansingDescriptorVO
    Set VODescriptor = App.svcValidator.DescriptorsByShortName("CustomerAddress")
    
    Dim FieldDescriptor As CleansingDescriptorField
    
    Dim dicKeysCounter As Dictionary
    Dim siteUseCode As String
    Dim CompositeKey As String
    Dim row As Integer
    
    Set dicKeysCounter = New Dictionary
    dicKeysCounter.RemoveAll
    
    For row = App.FIRST_DATA_ROW To wksData.UsedRange.Rows.count
        CompositeKey = wksData.Cells(row, VODescriptor.DescriptorsByColName("ORIG_SYSTEM_CUSTOMER_REF").ColumnNumber) + _
                        wksData.Cells(row, VODescriptor.DescriptorsByColName("ORIG_SYSTEM_ADRESS_REFERENCE").ColumnNumber)
         
        If Not (dicKeysCounter.Exists(CompositeKey)) Then
            Dim counters As CustomAddressCounters
            Set counters = New CustomAddressCounters
            
            siteUseCode = wksData.Cells(row, VODescriptor.DescriptorsByColName("SITE_USE_CODE").ColumnNumber)
            
            Dim dic As Dictionary
            Set dic = counters.dicSiteUseCode
            dic(siteUseCode) = 1
            Set counters.dicSiteUseCode = dic
            
            If (wksData.Cells(row, VODescriptor.DescriptorsByColName("PRIMARY_SITE_USE_FLAG").ColumnNumber) = "Y") Then
                counters.countPrimary = 1
            End If
            
            dicKeysCounter.Add CompositeKey, counters
        Else
            siteUseCode = wksData.Cells(row, VODescriptor.DescriptorsByColName("SITE_USE_CODE").ColumnNumber)
            If (dicKeysCounter(CompositeKey).dicSiteUseCode.Exists(siteUseCode)) Then
                dicKeysCounter(CompositeKey).dicSiteUseCode(siteUseCode) = dicKeysCounter(CompositeKey).dicSiteUseCode(siteUseCode) + 1
                
            End If
        
            If (wksData.Cells(row, VODescriptor.DescriptorsByColName("PRIMARY_SITE_USE_FLAG").ColumnNumber) = "Y") Then
                dicKeysCounter(CompositeKey).countPrimary = dicKeysCounter(CompositeKey).countPrimary + 1
            End If
        End If
    Next row
    
    CompositeKey = wksData.Cells(original.row, VODescriptor.DescriptorsByColName("ORIG_SYSTEM_CUSTOMER_REF").ColumnNumber) + _
                wksData.Cells(original.row, VODescriptor.DescriptorsByColName("ORIG_SYSTEM_ADRESS_REFERENCE").ColumnNumber)
  
    Dim countersForThisRow As CustomAddressCounters
    Set countersForThisRow = dicKeysCounter(CompositeKey)
    
    Dim OrigSystemAddressRef As String
    OrigSystemAddressRef = wksData.Cells(original.row, VODescriptor.DescriptorsByColName("ORIG_SYSTEM_ADRESS_REFERENCE").ColumnNumber)
    
    
    If (countersForThisRow.countPrimary > 1) Then
        App.CustomValidationErrorCode = App.ERROR_MULTIPLE_PRIMARY_SITE_USE_FLAG
        App.CustomValidationMessage = OrigSystemAddressRef + " at most one PRIMARY_SITE_USE_FLAG"
         
    ElseIf (countersForThisRow.dicSiteUseCode("BOTH") > 1) Then
        App.CustomValidationErrorCode = App.ERROR_UNIQUENESS_VIOLATION
        App.CustomValidationMessage = OrigSystemAddressRef + " must be unique, watch BOTH setup"
         
    ElseIf (countersForThisRow.dicSiteUseCode("BOTH") = 1) And ((countersForThisRow.dicSiteUseCode("SHIP_TO") <> 0) Or (countersForThisRow.dicSiteUseCode("BILL_TO") <> 0)) Then
        App.CustomValidationErrorCode = App.ERROR_UNIQUENESS_VIOLATION
        App.CustomValidationMessage = OrigSystemAddressRef + " must be unique, watch BOTH + SHIP_TO + BILL_TO setup"
         
    ElseIf (countersForThisRow.dicSiteUseCode("SHIP_TO") > 1) Then
        App.CustomValidationErrorCode = App.ERROR_UNIQUENESS_VIOLATION
        App.CustomValidationMessage = OrigSystemAddressRef + " must be unique, watch SHIP_TO setup"
         
    ElseIf (countersForThisRow.dicSiteUseCode("BILL_TO") > 1) Then
        App.CustomValidationErrorCode = App.ERROR_UNIQUENESS_VIOLATION
        App.CustomValidationMessage = OrigSystemAddressRef + "must be unique, watch BILL_TO setup"
         
    End If



End Sub


Private Function MatchPrimaryKey(ByVal oRng As Range, ByVal key As Variant)
    On Error GoTo eh
    Err.Clear
    
    Debug.Print oRng.Rows.count
    
    Dim foundKeyCellPos As Double
    foundKeyCellPos = -1
    foundKeyCellPos = Application.Match(key, oRng, 0)
    
    
    MatchPrimaryKey = foundKeyCellPos
    Exit Function

eh:
    Err.Clear
    MatchPrimaryKey = -1

End Function

Public Sub AppendTrailingDotZeroToSCC(ByVal original As String)
    FormattedValue = original & ".0"
End Sub

Public Sub ToOracleCountryCode(ByVal original As String)
    If (OracleCountryCodesTable Is Nothing) Then
        Set OracleCountryCodesTable = New Dictionary
        
        OracleCountryCodesTable("PANAMA") = "PA"
        OracleCountryCodesTable("CHINA") = "CN"
        OracleCountryCodesTable("ESPA�A") = "ES"
        OracleCountryCodesTable("ALEMANIA") = "DE"
        OracleCountryCodesTable("PERU") = "PE"
        OracleCountryCodesTable("URUGUAY") = "UY"
        OracleCountryCodesTable("BRASIL") = "BR"
        OracleCountryCodesTable("ARGENTINA") = "AR"
        OracleCountryCodesTable("GUATEMALA") = "GU"
        OracleCountryCodesTable("CHILE") = "CH"
        OracleCountryCodesTable("MEXICO") = "MX"
        
    End If
    
    If OracleCountryCodesTable.Exists(original) Then
        FormattedValue = OracleCountryCodesTable(original)
    Else
        FormattedValue = "PA"
    End If
    
End Sub

Public Sub MatchLocatorExpression(ByVal original As String)
    Dim locatorPattern As String: locatorPattern = "^.+\..+\..+"
    Dim regEx As New RegExp
    
    With regEx
        .Global = True
        .MultiLine = True
        .IgnoreCase = False
        .Pattern = locatorPattern
    End With
    
    If Not regEx.Test(original) Then
        App.CustomValidationErrorCode = App.ERROR_INVALID_LOCATOR
        App.CustomValidationMessage = "locator " & original & " is not in the form ROW.RACK.BIN"
    End If
    
End Sub

Public Sub MarkMissingSCCAsError(ByVal original As String)
    Dim locatorPattern As String: locatorPattern = "^missing.*"
    Dim regEx As New RegExp
    
    With regEx
        .Global = True
        .MultiLine = True
        .IgnoreCase = False
        .Pattern = locatorPattern
    End With
    
    If regEx.Test(original) Then
        App.CustomValidationErrorCode = App.ERROR_INVALID_SCC
        App.CustomValidationMessage = "missing valid SCC.0 code"
    End If

End Sub


Public Sub ConditionalContactPhoneValidation(ByVal TelephoneType As Range)
    Dim CustomerContactSheet As Worksheet
    Set CustomerContactSheet = Worksheets("Data - CustomerContact")

    App.CustomValidationErrorCode = App.ERROR_NONE
    App.CustomValidationMessage = ""

    If ((TelephoneType = "PHONE") Or (TelephoneType = "FAX")) Then
        Dim rngPhone As Range
        Set rngPhone = CustomerContactSheet.Cells(TelephoneType.row, CUSTOMER_CONTACT_TELEPHONE_COL)
        
        If (Len(rngPhone.Text) = 0) Then
            App.CustomValidationErrorCode = App.ERROR_MISSING_TELEPHONE_FOR_TELEPHONE_OR_FAX
            App.CustomValidationMessage = "mandatory value missing on field TELEPHONE"
        End If
        
    ElseIf (TelephoneType = "EMAIL") Then
        Dim rngEmail As Range
        Set rngEmail = CustomerContactSheet.Cells(TelephoneType.row, CUSTOMER_CONTACT_EMAIL_COL)
        
        If (Len(rngEmail.Text) = 0) Then
            App.CustomValidationErrorCode = App.ERROR_MISSING_EMAIL
            App.CustomValidationMessage = "mandatory value missing on field EMAIL"
        End If
        
    Else
         
         
        App.CustomValidationErrorCode = App.ERROR_INVALID_TELEPHONE_TYPE
        App.CustomValidationMessage = "invalid telephone type"
    
    End If
    
        

End Sub


Public Sub ConditionalOpenProjects(rng As Range)
    Debug.Print "unimplemented Conditional sales representative ref"
End Sub

Public Sub ConditionalRetention(rng As Range)
    Debug.Print "mutually exclusive retention rule"
End Sub

Public Sub ConditionalTopTasks(rng As Range)
    Debug.Print "mandatory fields only for top task"

End Sub

Public Sub TriggerInventoryIdGenerator(rng As Range)
    Debug.Print "when user selects ITEM_TEMPLATE " & rng.value
    
    Dim ManufacturerArray As Variant
    Dim ManufacturerDescriptionArray As Variant
    Dim ManufacturerKey As String
    Dim ItemId As Integer
    Dim maxItemRow As Integer
    Dim NextItemId As Integer
    
    Dim ItemIdRange As Range
    Dim FoundItemIdRange As Range
    
    Dim NewInventoryKey As String
    Dim OldInventoryKey As String
    
    ManufacturerKey = TextHandler.ExtractManufacturerKey(rng.value)
    
    If ManufacturerIdTable Is Nothing Then
        ManufacturerArray = Sheets(TextHandler.CONFIG_COMMONS).Range("$C$17:$C$75").Value2
    
        Set ManufacturerIdTable = New Dictionary
    
        For Each vTemplate In ManufacturerArray
            ManufacturerKey = TextHandler.ExtractManufacturerKey(vTemplate)
            
            If Not ManufacturerIdTable.Exists(ManufacturerKey) Then
                ManufacturerIdTable.Add ManufacturerKey, 0
            End If
            
        Next
        
        maxItemRow = Sheets("Data - ItemHeader").UsedRange.Rows.count

        Dim ItemIdArray As Variant
        ItemIdArray = Sheets("Data - ItemHeader").Range("$F$2:$F$" & maxItemRow).Value2

        For Each vItem In ItemIdArray
            ManufacturerKey = TextHandler.ExtractManufacturerKey(vItem)
            
            If ManufacturerIdTable.Exists(ManufacturerKey) Then
                ItemId = TextHandler.ExtractManufacturerId(vItem)
                
                If (ManufacturerIdTable(ManufacturerKey) < ItemId) Then
                    ManufacturerIdTable(ManufacturerKey) = ItemId
                End If
                
            End If
        Next

        'ManufacturerIdTable is built
        
    End If
    
    If (ManufacturerDescriptionTable Is Nothing) Then
        Set ManufacturerDescriptionTable = New Scripting.Dictionary
        
        ManufacturerArray = Sheets(TextHandler.CONFIG_COMMONS).Range("$D$17:$D$33").Value2
        ManufacturerDescriptionArray = Sheets(TextHandler.CONFIG_COMMONS).Range("$E$17:$E$33").Value2
        
        For i = 1 To UBound(ManufacturerArray)
            Dim key As Variant
            Dim Val As Variant
            
            key = ManufacturerArray(i, 1)
            Val = ManufacturerDescriptionArray(i, 1)
        
            ManufacturerDescriptionTable.Add key, Val
        Next i
    End If
    
    OldInventoryKey = Sheets("Data - ItemHeader").Range("$F$" & rng.row)
    ManufacturerKey = TextHandler.ExtractManufacturerKey(rng.value)
    maxItemRow = Sheets("Data - ItemHeader").UsedRange.Rows.count
    
    If (ManufacturerKey = "LA98") Then
        Set ItemIdRange = Sheets("Data - ItemMfgPartNum").Range("$I$2:$I$" & maxItemRow)
        Set FoundItemIdRange = ItemIdRange.Find(OldInventoryKey)
        
        If Not (FoundItemIdRange Is Nothing) Then
            ManufacturerKey = Mid(ManufacturerKey, 3, 2)
            
            App.DisabledValidations = True
            Sheets("Data - ItemMfgPartNum").Range("E" & FoundItemIdRange.row).Value2 = ManufacturerKey
            Sheets("Data - ItemMfgPartNum").Range("F" & FoundItemIdRange.row).Value2 = ManufacturerDescriptionTable(ManufacturerKey)
            App.DisabledValidations = False
            
        End If
        
    Else
        If TextHandler.ExtractManufacturerKey(OldInventoryKey) <> ManufacturerKey Then
          
            Debug.Assert ManufacturerIdTable.Exists(ManufacturerKey)
            NextItemId = ManufacturerIdTable(ManufacturerKey) + 1
            
            ManufacturerIdTable(ManufacturerKey) = NextItemId
            
            Debug.Print OldInventoryKey
            
            NewInventoryKey = rng.Value2 & Format(NextItemId, "000000")
            
            Debug.Print NewInventoryKey
            Debug.Print Len(NewInventoryKey)
            
            App.DisabledValidations = True
            
            Set ItemIdRange = Sheets("Data - ItemHeader").Range("$F$2:$F$" & maxItemRow)
            Set FoundItemIdRange = ItemIdRange.Find(OldInventoryKey)
            FoundItemIdRange.Value2 = NewInventoryKey
           
            Set ItemIdRange = Sheets("Data - ItemCrossRef").Range("$F$2:$F$" & maxItemRow)
            Set FoundItemIdRange = ItemIdRange.Find(OldInventoryKey)
            FoundItemIdRange.Value2 = NewInventoryKey
        
            Set ItemIdRange = Sheets("Data - ItemCategory").Range("$F$2:$F$" & maxItemRow)
            Set FoundItemIdRange = ItemIdRange.Find(OldInventoryKey)
            FoundItemIdRange.Value2 = NewInventoryKey
        
            Set ItemIdRange = Sheets("Data - ItemMfgPartNum").Range("$I$2:$I$" & maxItemRow)
            Set FoundItemIdRange = ItemIdRange.Find(OldInventoryKey)
            FoundItemIdRange.Value2 = NewInventoryKey
            
            ManufacturerKey = Mid(ManufacturerKey, 3, 2)
            Sheets("Data - ItemMfgPartNum").Range("E" & FoundItemIdRange.row).Value2 = ManufacturerKey
            Sheets("Data - ItemMfgPartNum").Range("F" & FoundItemIdRange.row).Value2 = ManufacturerDescriptionTable(ManufacturerKey)
            
            Set ItemIdRange = Sheets("Data - ItemTransDefSubInv").Range("$F$2:$F$" & maxItemRow)
            Set FoundItemIdRange = ItemIdRange.Find(OldInventoryKey)
            FoundItemIdRange.Value2 = NewInventoryKey
        
            Set ItemIdRange = Sheets("Data - ItemTransDefLoc").Range("$F$2:$F$" & maxItemRow)
            Set FoundItemIdRange = ItemIdRange.Find(OldInventoryKey)
            FoundItemIdRange.Value2 = NewInventoryKey
            
            App.DisabledValidations = False
        End If
    
    End If

End Sub

Public Sub ProjectClassificationsLinkedLOV(rng As Range)
    Debug.Print "adjust CLASS_CODE according to CLASS_CATEGORY : " & rng.Value2
    
    Dim linkedLOVFormula As String
    
    Select Case rng.Value2
        Case "EXISTING BUILDING"
            linkedLOVFormula = "D20:D21"

        Case "LINE OF BUSINESS"
            linkedLOVFormula = "E20:E23"

        Case "BUILDING TYPE"
            linkedLOVFormula = "C20:C70"
    
        Case "BUILDING CLASSIFICATION"
            linkedLOVFormula = "B20:B62"
    
        Case Else
            Debug.Print "funny value " & rng.Value2
            Err.Raise App.ERROR_MISSING_CONFIGURATIONS, "allowed values are EXISTING BUILDING, LINE OF BUSINESS, BUILDING TYPE, BUILDING CLASSIFICATION"

    End Select

    Dim descriptorVO As CleansingDescriptorVO
    Set descriptorVO = App.svcValidator.DescriptorsByShortName("ProjectClassifications")
    
    Dim descriptorField As CleansingDescriptorField
    Set descriptorField = descriptorVO.DescriptorsByColName("CLASS_CODE")
    
    Dim linkedClassificationCodeCol As Integer
    linkedClassificationCodeCol = descriptorField.ColumnNumber
    
    Dim wksData As Worksheet
    Set wksData = Sheets("Data - ProjectClassifications")
    
    Dim lastRow As Integer
    lastRow = wksData.UsedRange.SpecialCells(xlCellTypeLastCell).row
    
    Dim dataRange As Range
    Set dataRange = wksData.Range(wksData.Cells(rng.row, linkedClassificationCodeCol), wksData.Cells(rng.row, linkedClassificationCodeCol))

    dataRange.Validation.Delete
    
    Debug.Print linkedLOVFormula
    
    dataRange.Validation.Delete
    dataRange.Validation.Add xlValidateList, Formula1:="='Config - Commons'!" & linkedLOVFormula
    
End Sub





Public Function Localized_AND() As String
    Dim currentLanguage As Integer
    currentLanguage = Application.LanguageSettings.LanguageID(msoLanguageIDUI)
    
    Select Case currentLanguage
        Case ENGLISH_PACK
            Localized_AND = "AND"
        Case PORTUGUESE_PACK
            Localized_AND = "E"
        Case SPANISH_PACK
            Localized_AND = "Y"
        Case Else
            Err.Raise App.ERROR_UNSUPPORTED_LANGUAGE_PACK, "App", "unsupported language pack id " & currentLanguage
            
    End Select


End Function

Public Function Localized_FUNCTION_SEPARATOR() As String
    Dim currentLanguage As Integer
    currentLanguage = Application.LanguageSettings.LanguageID(msoLanguageIDUI)
    
    Select Case currentLanguage
        Case ENGLISH_PACK
            Localized_FUNCTION_SEPARATOR = ";"
        Case PORTUGUESE_PACK
            Localized_FUNCTION_SEPARATOR = ";"
        Case SPANISH_PACK
            Localized_FUNCTION_SEPARATOR = ";"
        Case Else
            Err.Raise App.ERROR_UNSUPPORTED_LANGUAGE_PACK, "App", "unsupported language pack id " & currentLanguage
            
    End Select


End Function

