VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "GeneratorService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' version 0.8.0
' The Generator module is responsible for managing textual outputs
'
Private Const SEPARATOR = "|"

' generates Data Definition Language (DDL) scripts for staging tables
'
Public Sub GenerateDDL(stgTableName As String, ddlFullPath As String, validations As Scripting.Dictionary, colPrimaryKeys As Collection, hasPrimary As Boolean, isTransactional As Boolean)
    On Error GoTo eh
    Err.Clear
    
    Dim FSO As FileSystemObject
    Dim oFile As TextStream
    
    Set FSO = CreateObject("Scripting.FileSystemObject")
    Set oFile = FSO.CreateTextFile(ddlFullPath)
    
    oFile.WriteLine Replace("IF OBJECT_ID('dbo.%1', 'U') IS NOT NULL", "%1", stgTableName)
    oFile.WriteLine Replace("  DROP TABLE dbo.%1;", "%1", stgTableName)
    oFile.WriteLine
    oFile.WriteLine Replace("CREATE TABLE [dbo].[%1](", "%1", stgTableName)
    
    If Not (isTransactional) Then
        oFile.WriteLine "    [VALIDATED] [VARCHAR](1) NOT NULL,"
        oFile.WriteLine "    [RECOMMENDED] [VARCHAR](1) NOT NULL,"
        oFile.WriteLine "    [CLEANSED] [VARCHAR](1) NOT NULL,"
    End If
    
    Dim rowCount As Integer
    rowCount = 0
    
    For Each item In validations.Items
        Dim strLine As String
        strLine = "    "
            strLine = strLine + item.SSQL_DDL
    
        rowCount = rowCount + 1
        If (rowCount < validations.count) Then
            strLine = strLine + ","
        End If
        
        oFile.WriteLine strLine
   
    Next item
    
    oFile.WriteLine
    
    ' the PRIMARY definition will only be appended if any field descriptor is marked as primary key
    '
    If Not hasPrimary Then
        oFile.WriteLine ")"
    
    Else
        oFile.WriteLine "PRIMARY KEY CLUSTERED"
        oFile.WriteLine "("
        
        Dim primaryDDL As String
        primaryDDL = Replace("    [%1] ASC", "%1", colPrimaryKeys(1).fieldName)
        
        Dim addKeyIdx As Integer
        For addKeyIdx = 2 To colPrimaryKeys.count
            primaryDDL = primaryDDL & Replace(", [%1] ASC", "%1", colPrimaryKeys(addKeyIdx).fieldName)
        
        Next addKeyIdx
    
    
        oFile.WriteLine primaryDDL
    
        oFile.WriteLine ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]"
        oFile.WriteLine ") ON [PRIMARY]"
    
    End If
    
eh:
    If (Err.Number <> 0) Then
        Debug.Print Err.Number & " - " & Err.Description
    
    End If
    
    If Not (oFile Is Nothing) Then
        oFile.Close
    End If
    
    Set FSO = Nothing
    Set oFile = Nothing


End Sub





' generate Flat Files
' Flat files are separated by pipes defined @SEPARATOR

Public Sub GenerateFlatFile(flatFilePath As String, rawData As Range, validations As Scripting.Dictionary, multipleOutput As String, Optional UserFeedback As Boolean = True)
    On Error GoTo eh
    Err.Clear
    
    Dim fsT As Object
    Dim rw As Range
    Dim count, current As Integer

    Set fsT = CreateObject("ADODB.Stream")
    fsT.Type = 2 'Specify stream type - we want To save text/string data.
    fsT.Charset = "utf-8" 'Specify charset For the source text data.
    fsT.Open 'Open the stream And write binary data To the object
    
    
    Application.Calculation = xlCalculationManual
    
    current = 0
    count = rawData.Rows.count - 1
    
    If (UserFeedback) Then
        ValidationProgress.Show vbModeless
        ValidationProgress.UpdateAction flatFilePath
        ValidationProgress.UpdateProgress 1, count
    End If
    
    For currRow = 2 To rawData.Rows.count
        If rawData.Cells(currRow, VALIDATED).value = "A" Then
            Dim line As String
            Dim idx As Integer
            
            line = ""
            idx = CLEANSED + 1
            
            Do
                If (validations.Exists(idx)) Then
                    Dim descriptor As CleansingDescriptorField
                    Set descriptor = validations.item(idx)
                    
                    
                    If (descriptor.IsOutputValue()) Then
                        Dim outputtedValue As Variant
                        outputtedValue = rawData.Cells(currRow, idx).value
                
                        If (descriptor.Format <> "") And (outputtedValue <> "") Then
                            Application.Run descriptor.Format, outputtedValue
                            outputtedValue = Commons.FormattedValue
                        End If
                        
                        If (outputttedValue = "*NULL*") Then
                            outputtedValue = ""
                        End If
                        
                        line = line & outputtedValue
                        If (idx < rawData.Columns.count) Then
                            line = line & SEPARATOR
                        End If
                    
                    End If
                End If
                idx = idx + 1
            
            Loop Until idx > rawData.Columns.count
            
            If (Right(line, 1) = "|") Then
                line = Left(line, Len(line) - 1)
            End If
            
            
            Select Case multipleOutput
                Case "none":
                    fsT.WriteText line & vbNewLine
                
                Case "dualUseAddress":
                    Debug.Print line
                
                    If InStr(1, line, "BOTH") <> 0 Then
                        Dim lineShipTo, lineBillTo As String
                        If InStr(1, line, "|BOTH|Y|") <> 0 Then
                            lineShipTo = Replace(line, "|BOTH|Y|", "|SHIP_TO|N|")
                            lineBillTo = Replace(line, "|BOTH|Y|", "|BILL_TO|Y|")
                        Else
                            lineShipTo = Replace(line, "|BOTH|N|", "|SHIP_TO|N|")
                            lineBillTo = Replace(line, "|BOTH|N|", "|BILL_TO|N|")
                        End If
                        
                        fsT.WriteText lineShipTo & vbNewLine
                        fsT.WriteText lineBillTo & vbNewLine
                        
                    Else
                        fsT.WriteText line & vbNewLine
                        
                    End If
                    
                    
                    
                Case "multiOrgCodes":
                    Dim lineLPA, lineCPA, lineTKE As String
                    
                    lineCPA = Replace(line, "|LPA|", "|CPA|")
                    lineTKE = Replace(line, "|LPA|", "|TKE|")
                
                    fsT.WriteText lineTKE & vbNewLine
                    fsT.WriteText lineCPA & vbNewLine
                    fsT.WriteText line & vbNewLine
                
                Case Else:
                    Err.Raise ERROR_NOT_IMPLEMENTED, 0, "multiple output of type " + multipleOutput + " has not been implemented."
                    
            End Select
            
        End If
        
        If (UserFeedback) Then
            ValidationProgress.UpdateProgress currRow, count
        End If
    Next currRow
    
    fsT.SaveToFile flatFilePath, 2


eh:
    Application.Calculation = xlCalculationAutomatic
    
    If (UserFeedback) Then
        ValidationProgress.Hide
        Unload ValidationProgress
    End If

    If (Err.Number <> 0) Then
        Debug.Print Err.Number & " - " & Err.Description
    
    End If
    
    If Not (fsT Is Nothing) Then
        fsT.Close
    End If
    
    Set fsT = Nothing
End Sub


