VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "LoadFull"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements LoadStrategy

Private NextInsertionRow As Integer

Private Sub LoadStrategy_Setup(ByVal wksTarget As Worksheet)
    wksTarget.UsedRange.Clear
    NextInsertionRow = App.FIRST_DATA_ROW
    
End Sub


Private Sub LoadStrategy_ProcessStagingRecord(ByVal wksTarget As Worksheet, _
                                                    ByVal Record As ADODB.Recordset, _
                                                    ByVal VODescriptor As CleansingDescriptorVO)
                                   
    Commons.LoadRecordInto wksTarget, NextInsertionRow, Record, VODescriptor
    
    NextInsertionRow = NextInsertionRow + 1

End Sub



