VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "LoadKPI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements LoadStrategy

Const VALIDATED_FIELD = 0
Const CLEANSED_FIELD = 2

Dim NextInsertionRow As Integer

Private Sub LoadStrategy_Setup(ByVal wksTarget As Worksheet)
    NextInsertionRow = wksTarget.UsedRange.Rows.count + 1
End Sub

Private Sub LoadStrategy_ProcessStagingRecord(ByVal wksTarget As Worksheet, _
                                                ByVal Record As ADODB.Recordset, _
                                                ByVal VODescriptor As CleansingDescriptorVO)
    
    Debug.Assert VODescriptor.IsAnyPrimaryDefined
    
    Dim colPrimKeys As Collection
    Set colPrimKeys = VODescriptor.GetPrimaryKeys
    
    Dim firstPrimaryKey As CleansingDescriptorField
    Set firstPrimaryKey = colPrimKeys(1)
    
    Dim keyValue As Variant
    keyValue = Trim(Record.Fields(firstPrimaryKey.ColumnNumber - App.STG_ADJUSTED_INDEX).value)
    
    Dim foundKeyCell As Range
    
    Dim firstKeyRange As Range
    Set firstKeyRange = wksTarget.Range( _
                            wksTarget.Cells(App.FIRST_DATA_ROW, firstPrimaryKey.ColumnNumber), _
                            wksTarget.Cells(NextInsertionRow, firstPrimaryKey.ColumnNumber))
                                            
    Dim KeyVariant As Variant
    KeyVariant = keyValue
    
    Dim foundKeyCellPos As Double
    foundKeyCellPos = MatchPrimaryKey(firstKeyRange, keyValue) + 1
    
    If (foundKeyCellPos = 0) Then
        Exit Sub
        
    Else
        If colPrimKeys.count = 1 Then
            'we have a match
            UpdateOnlyKPIForRecord wksTarget, foundKeyCellPos, Record, VODescriptor
            Exit Sub
        
        Else
            Do
                'we have a candidate for a composite (multi-key) VO
                If CheckCompositeEquals(wksTarget, foundKeyCellPos, Record, VODescriptor) Then
                    ' we have a match
                    UpdateOnlyKPIForRecord wksTarget, foundKeyCellPos, Record, VODescriptor
                    Exit Sub
                    
                Else
                    Dim nextCell As Range
                    Set nextCell = wksTarget.Cells(foundKeyCellPos + 1, foundKeyCell.Column)
                    
                    Set firstKeyRange = wksTarget.Range( _
                            wksTarget.Cells(nextCell.row, nextCell.Column), _
                            wksTarget.Cells(NextInsertionRow, firstPrimaryKey.ColumnNumber))
                    
                    'look for another candidate
                    Set foundKeyCell = firstKeyRange.Find(What:=keyValue, LookIn:=xlValues, SearchDirection:=xlNext)
                    
                End If
            
            Loop Until foundKeyCell Is Nothing
        
        End If
    
    End If
    
    

End Sub

Private Function MatchPrimaryKey(ByVal oRng As Range, ByVal key As Variant)
    On Error GoTo eh
    Err.Clear
    
    Dim foundKeyCellPos As Double
    foundKeyCellPos = -1
    foundKeyCellPos = Application.Match(key, oRng, 0)
    
eh:
    Err.Clear


    MatchPrimaryKey = foundKeyCellPos

End Function



Private Sub UpdateOnlyKPIForRecord(ByVal wksTarget As Worksheet, _
                                    ByVal row As Integer, _
                                    ByVal Record As ADODB.Recordset, _
                                    ByVal VODescriptor As CleansingDescriptorVO)
    
    Dim CleansedStateOnWorksheet As String
    Dim ValidatedStateOnWorksheet As String
    
    CleansedStateOnWorksheet = wksTarget.Cells(row, App.CLEANSED).value
    ValidatedStateOnWorksheet = wksTarget.Cells(row, App.VALIDATED).value
                          
    'this is the only information that flows back to the staging tables
    
    If CleansedStateOnWorksheet = "C" Or CleansedStateOnWorksheet = "D" Then
        Record.Fields(CLEANSED_FIELD) = CleansedStateOnWorksheet
    End If
     
    If ValidatedStateOnWorksheet = "A" Or ValidatedStateOnWorksheet = "R" Then
        Record.Fields(VALIDATED_FIELD) = ValidatedStateOnWorksheet
    End If
    
    Record.Update
    
                          
                          
End Sub

Private Function CheckCompositeEquals(ByVal wksTarget As Worksheet, _
                          ByVal row As Integer, _
                          ByVal Record As ADODB.Recordset, _
                          ByVal VODescriptor As CleansingDescriptorVO) As Boolean
                          
    Dim PKList As Collection
    Set PKList = VODescriptor.GetPrimaryKeys
                          
    Dim PK As Variant
    
    For Each PK In PKList
        Dim PKDescriptor As CleansingDescriptorField
        Set PKDescriptor = PK
        
        If (Record.Fields(PKDescriptor.ColumnNumber - App.STG_ADJUSTED_INDEX).value <> wksTarget.Cells(row, PKDescriptor.ColumnNumber).value) Then
            CheckCompositeEquals = False
            Exit Function
        
        End If
        
    Next PK
                          
    CheckCompositeEquals = True
                          
                          
End Function




