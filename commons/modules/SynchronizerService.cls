VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "SynchronizerService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' v.0.8.0
'
Option Explicit

Private Cnxn As ADODB.Connection
Private RstRecords As ADODB.Recordset
Private Cmd As ADODB.Command

Private StrategyLoadFull As New LoadFull
Private StrategyLoadMergeSimple As New LoadMergeSimple
Private StrategyLoadKPI As New LoadKPI

Private NextInsertionRow As Integer

' A Full Load mimics the "Obtain from External Data Source"  built-in feature of Excel
'
Public Function FullLoad() As Collection
    Set FullLoad = TraverseStagingTables(StrategyLoadFull)
    
End Function


' An Incremental Load takes into account the cleansing state before deciding what to do with a record
'
Public Function IncrementalLoad() As Collection
    Set IncrementalLoad = TraverseStagingTables(StrategyLoadMergeSimple)

End Function

' load only fields related to KPI Key Performance Indexes
'
Public Function KPILoad() As Collection
    Set KPILoad = TraverseStagingTables(StrategyLoadKPI, UserFeedback:=False)
    
End Function



Public Function TransactionalLoad() As Collection
    On Error GoTo eh
    Err.Clear

    App.AutoConfigure
    
    Set Cnxn = New ADODB.Connection
    Cnxn.Open szConfigDbConnector
    
    Dim szVOShortName As Variant
    For Each szVOShortName In App.svcValidator.DescriptorsByShortName.Keys
        Dim VODescriptor As CleansingDescriptorVO
        Set VODescriptor = App.svcValidator.DescriptorsByShortName(szVOShortName)
        
        Set Cmd = New ADODB.Command
        
        With Cmd
            .ActiveConnection = Cnxn
            .CommandType = adCmdStoredProc
            .CommandText = VODescriptor.LoadingProcedure
        End With
        
        Cmd.Execute , , adAsyncExecute
        
        Dim start As Date
        start = Now
        
        Do While Cmd.State = adStateExecuting
            Dim current As Date
            current = Now
        
            Application.StatusBar = "executing: " & VODescriptor.LoadingProcedure + " " + Format(CStr((current - start) * 100000), "#0.00") + "s"
        
            DoEvents
        Loop
        
        Application.StatusBar = "executed: " & VODescriptor.LoadingProcedure + ", rejoice!"
    
    Next szVOShortName
    

eh:
    If (Err.Number <> 0) Then
        Debug.Print Err.Number & " - " & Err.Description
    
    End If
    
    If Not Cnxn Is Nothing Then
        If Cnxn.State = adStateOpen Then Cnxn.Close
    End If
    
    Set Cnxn = Nothing
    Set Cmd = Nothing

End Function


Private Function TraverseStagingTables(ByVal Strategy As LoadStrategy, Optional UserFeedback As Boolean = True) As Collection
    Dim iVOCount As Integer
    Dim VOFieldDescriptor As Variant
    Dim InvolvedStagingTableNames As New Collection

    On Error GoTo eh
    Err.Clear
    
    Application.ScreenUpdating = False
    Application.Calculation = xlCalculationManual
    App.DisabledValidations = True
    
    If (UserFeedback) Then
        ValidationProgress.Show vbModeless
    End If

    App.AutoConfigure
    
    Set Cnxn = New ADODB.Connection
    Cnxn.Open szConfigDbConnector
    
    Dim szVOShortName As Variant
    For Each szVOShortName In App.svcValidator.DescriptorsByShortName.Keys
        Dim VODescriptor As CleansingDescriptorVO
        Set VODescriptor = App.svcValidator.DescriptorsByShortName(szVOShortName)

        If (UserFeedback) Then
            ValidationProgress.UpdateAction "Loading " + VODescriptor.shortName
        End If
        
        Set RstRecords = New ADODB.Recordset
        RstRecords.Open VODescriptor.stgTableName, Cnxn, adOpenStatic, adLockPessimistic, adCmdTable
        
        InvolvedStagingTableNames.Add VODescriptor.stgTableName
        
        Dim wksTarget As Worksheet
        Set wksTarget = Worksheets(VODescriptor.DataName)
        
        wksTarget.Unprotect
        Strategy.Setup wksTarget
        
        Dim count As Integer
        count = 0
        
        Dim totalCount As Integer
        totalCount = RstRecords.recordCount
        
        Do Until RstRecords.EOF
            Strategy.ProcessStagingRecord wksTarget, RstRecords, VODescriptor
                        
            If (UserFeedback) Then
                ValidationProgress.UpdateProgress count, totalCount
            End If

            count = count + 1
            RstRecords.MoveNext
        Loop
        
        If RstRecords.State = adStateOpen Then RstRecords.Close
        
    Next szVOShortName
    
    If (UserFeedback) Then
        ValidationProgress.Hide
    End If
    
eh:
    If (Err.Number <> 0) Then
        Debug.Print Err.Number & " - " & Err.Description
    
    End If
    
    If Not Cnxn Is Nothing Then
        If Cnxn.State = adStateOpen Then Cnxn.Close
    End If
    
    If Not RstRecords Is Nothing Then
        If RstRecords.State = adStateOpen Then RstRecords.Close
    End If

    Set Cnxn = Nothing
    Set RstRecords = Nothing

    If (UserFeedback) Then
        ValidationProgress.Hide
        Unload ValidationProgress
    End If
    
    App.DisabledValidations = False
    Application.Calculation = xlCalculationAutomatic
    Application.ScreenUpdating = True

    Set TraverseStagingTables = InvolvedStagingTableNames

End Function
