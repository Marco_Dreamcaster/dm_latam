Attribute VB_Name = "TextHandler"
' TextHandler module extends VBA string handling features
'
' v 0.8.0 - handles transactional data
'

Public Const VERSION = "Version"

Public Const CONFIG_COMMONS = "Config - Commons"
Public Const CONFIG_COMMONS_PROD = "PROD - Config - Commons"
Public Const CONFIG_COMMONS_DEV = "DEV - Config - Commons"

Public Const CONFIG_MARKER = "Config - "




Public Const DATA_MARKER = "Data - "
Public Const TRANSACTIONAL_MARKER = "Transactional - "

Public Const FK_SEPARATOR = "."

Private Const ERROR_UNRECOGNIZABLE_SHEET_NAME = 20 + 513

Public Function isVOConfig(sheetName As String) As Boolean
    isVOConfig = (InStr(1, sheetName, CONFIG_MARKER) <> 0) And (InStr(1, sheetName, CONFIG_COMMONS) = 0)
End Function

Public Function isVOData(sheetName As String) As Boolean
    isVOData = InStr(1, sheetName, DATA_MARKER) <> 0
End Function

Public Function isTransactionalData(sheetName As String) As Boolean
    isTransactionalData = InStr(1, sheetName, TRANSACTIONAL_MARKER) <> 0
End Function


Public Function extractShortName(sheetName As String) As String
    If (InStr(1, sheetName, CONFIG_MARKER) <> 0) And (InStr(1, sheetName, CONFIG_COMMONS) = 0) Then
        extractShortName = Trim(Mid(sheetName, Len(CONFIG_MARKER), Len(sheetName) - Len(CONFIG_MARKER) + 1))
        Exit Function
        
    ElseIf (InStr(1, sheetName, DATA_MARKER) <> 0) Then
        extractShortName = Trim(Mid(sheetName, Len(DATA_MARKER), Len(sheetName) - Len(DATA_MARKER) + 1))
        Exit Function
        
    ElseIf (InStr(1, sheetName, TRANSACTIONAL_MARKER) <> 0) Then
        extractShortName = Trim(Mid(sheetName, Len(TRANSACTIONAL_MARKER), Len(sheetName) - Len(TRANSACTIONAL_MARKER) + 1))
        Exit Function
        
    End If
    
    Err.Raise ERROR_UNRECOGNIZABLE_SHEET_NAME, "Could not extract a VO short name from " & sheetName
    
End Function

Public Function fromConfigToDataName(configName As String) As String
    Dim shortName As String
    shortName = extractShortName(configName)
    
    fromConfigToDataName = DATA_MARKER & shortName

End Function

Public Function fromDataToConfigName(DataName As String) As String
    Dim shortName As String
    shortName = extractShortName(DataName)
    
    fromDataToConfigName = CONFIG_MARKER & shortName

End Function


Public Function buildDDLPathFor(stgTableName As String, ddlOutputPath As String)
    buildDDLPathFor = ddlOutputPath & "\create_" & stgTableName & "_auto.sql"

End Function

Public Function buildFlatFilePathFor(shortDescription As String, template As String, flatFileOutputOutputPath As String)
    Dim result As String
    
    result = Replace(template, "<%short_description%>", shortDescription)
    result = Replace(result, "<%timestamp%>", Format(Now(), "YYYYMMDDHHMMSS"))
    
    
    result = flatFileOutputOutputPath & "\" & result
    
    buildFlatFilePathFor = result

End Function

Public Function ExtractManufacturerKey(ByVal ID As String)
    Dim result As String
    result = ""
    
    result = Trim(Left(ID, 4))
    
    ExtractManufacturerKey = result
End Function


Public Function ExtractManufacturerId(ByVal ID As String)
    Dim result As String
    result = ""
    
    result = Trim(Right(ID, 6))
    
    If (IsNumeric(result)) Then
        If (result < 99999) Then
            ExtractManufacturerId = CInt(result)
        Else
            ExtractManufacturerId = 0
        End If
        
    Else
        ExtractManufacturerId = 0
    End If
    
    
    
End Function

Public Function ExtractStgTableNameFromFK(ByVal FK As String)
    Dim result As String
    result = ""
    
    If (CountCharacter(FK, FK_SEPARATOR) <> 1) Then
        Err.Raise App.ERROR_MAL_FORMED_FK, 0, "could not parse FK " & FK
    End If
    
    result = Trim(Mid(FK, 1, InStr(1, FK, FK_SEPARATOR) - 1))
    
    ExtractStgTableNameFromFK = result

End Function

Public Function ExtractFieldNameFromFK(ByVal FK As String)
    Dim result As String
    result = ""
    
    If (CountCharacter(FK, FK_SEPARATOR) <> 1) Then
        Err.Raise App.ERROR_MAL_FORMED_FK, 0, "could not parse FK " & FK
    End If
    
    result = Trim(Mid(FK, InStr(1, FK, FK_SEPARATOR) + 1, Len(FK) - InStr(1, FK, FK_SEPARATOR) + 1))
    
    ExtractFieldNameFromFK = result

End Function



' lame collections in VBA
Public Function ExistsIn(item As Variant, lots As Collection) As Boolean
    Dim e As Variant
    ExistsIn = False
    For Each e In lots
        If item = e Then
            ExistsIn = True
            Exit For
        End If
    Next
End Function

' lame everything in VBA
Public Function CountCharacter(ByVal value As String, ByVal ch As String) As Integer
    Dim cnt, startAt As Integer
    cnt = 0
    startAt = 1
  
    Do While InStr(startAt, value, ch) <> 0
        startAt = InStr(startAt, value, ch) + 1
        cnt = cnt + 1
    Loop
    
    CountCharacter = cnt
End Function

