Attribute VB_Name = "UnitTests"
' stress test validation progress
'
Public Sub TestValidationProgress()
    On Error GoTo eh
    
    'this can take some time
    Application.Calculation = xlCalculationManual
    
    Dim iMaster, iCount, iCurrent As Integer
    
    For iMaster = 1 To 5
        iCurrent = 0
        iCount = 1000
        
        ValidationProgress.Show
        ValidationProgress.UpdateAction "testing " & iMaster
        
        For iCurrent = 1 To iCount
            ValidationProgress.UpdateProgress iCurrent, iCount
            
            Sleep 10
        Next iCurrent
        
        ValidationProgress.Hide
    
    Next iMaster
    
    
eh:
    Unload ValidationProgress
    
    Application.Calculation = xlCalculationAutomatic

    If (Err.Number <> 0) Then
        Debug.Print Err.Number & " - " & Err.Description
    
    End If
    
End Sub


Public Sub TestParseManufacturerId()
    Debug.Assert (TextHandler.ExtractManufacturerKey("LA02ESM-00000001") = "LA02")
    Debug.Assert (TextHandler.ExtractManufacturerId("LA02ESM-00000001") = 1)

End Sub


Public Sub QuickPatchForAutomaticItemNumberGenerations()
    App.DisabledValidations = True

    Dim maxItemRow As Integer
    maxItemRow = Sheets("Data - ItemHeader").UsedRange.Rows.count
    
    Dim ItemIdRange As Range
    Set ItemIdRange = Sheets("Data - ItemHeader").Range("$F$2:$F$" & maxItemRow)

    For Each vCell In ItemIdRange
        Debug.Print vCell.row
        If (InStr(1, vCell.Value2, "LA") = 1) Then
            vCell.Value2 = Mid(vCell.Value2, 3, Len(vCell.Value2))
        End If
    Next

    Set ItemIdRange = Sheets("Data - ItemHeader").Range("$E$2:$E$" & maxItemRow)

    For Each vCell In ItemIdRange
        Debug.Print vCell.row
        If (InStr(1, vCell.Value2, "LA") <> 1) Then
            vCell.Value2 = "LA" + vCell.Value2
        End If
    Next
    
    
    Set ItemIdRange = Sheets("Data - ItemCrossRef").Range("$F$2:$F$" & maxItemRow)

    For Each vCell In ItemIdRange
        Debug.Print vCell.row
        If (InStr(1, vCell.Value2, "LA") = 1) Then
            vCell.Value2 = Mid(vCell.Value2, 3, Len(vCell.Value2))
        End If
    Next

    Set ItemIdRange = Sheets("Data - ItemCrossRef").Range("$E$2:$E$" & maxItemRow)

    For Each vCell In ItemIdRange
        Debug.Print vCell.row
        If (InStr(1, vCell.Value2, "LA") <> 1) Then
            vCell.Value2 = "LA" + vCell.Value2
        End If
    Next

    Set ItemIdRange = Sheets("Data - ItemCategory").Range("$F$2:$F$" & maxItemRow)

    For Each vCell In ItemIdRange
        Debug.Print vCell.row
        If (InStr(1, vCell.Value2, "LA") = 1) Then
            vCell.Value2 = Mid(vCell.Value2, 3, Len(vCell.Value2))
        End If
    Next

    Set ItemIdRange = Sheets("Data - ItemCategory").Range("$E$2:$E$" & maxItemRow)

    For Each vCell In ItemIdRange
        Debug.Print vCell.row
        If (InStr(1, vCell.Value2, "LA") <> 1) Then
            vCell.Value2 = "LA" + vCell.Value2
        End If
    Next

    Set ItemIdRange = Sheets("Data - ItemMfgPartNum").Range("$I$2:$I$" & maxItemRow)

    For Each vCell In ItemIdRange
        Debug.Print vCell.row
        If (InStr(1, vCell.Value2, "LA") = 1) Then
            vCell.Value2 = Mid(vCell.Value2, 3, Len(vCell.Value2))
        End If
    Next

    Set ItemIdRange = Sheets("Data - ItemMfgPartNum").Range("$H$2:$H$" & maxItemRow)

    For Each vCell In ItemIdRange
        Debug.Print vCell.row
        If (InStr(1, vCell.Value2, "LA") <> 1) Then
            vCell.Value2 = "LA" + vCell.Value2
        End If
    Next


    Set ItemIdRange = Sheets("Data - ItemTransDefSubInv").Range("$F$2:$F$" & maxItemRow)

    For Each vCell In ItemIdRange
        Debug.Print vCell.row
        If (InStr(1, vCell.Value2, "LA") = 1) Then
            vCell.Value2 = Mid(vCell.Value2, 3, Len(vCell.Value2))
        End If
    Next

    Set ItemIdRange = Sheets("Data - ItemTransDefSubInv").Range("$E$2:$E$" & maxItemRow)

    For Each vCell In ItemIdRange
        Debug.Print vCell.row
        If (InStr(1, vCell.Value2, "LA") <> 1) Then
            vCell.Value2 = "LA" + vCell.Value2
        End If
    Next

    Set ItemIdRange = Sheets("Data - ItemTransDefLoc").Range("$F$2:$F$" & maxItemRow)

    For Each vCell In ItemIdRange
        Debug.Print vCell.row
        If (InStr(1, vCell.Value2, "LA") = 1) Then
            vCell.Value2 = Mid(vCell.Value2, 3, Len(vCell.Value2))
        End If
    Next

    Set ItemIdRange = Sheets("Data - ItemTransDefLoc").Range("$E$2:$E$" & maxItemRow)

    For Each vCell In ItemIdRange
        Debug.Print vCell.row
        If (InStr(1, vCell.Value2, "LA") <> 1) Then
            vCell.Value2 = "LA" + vCell.Value2
        End If
    Next


    App.DisabledValidations = False

End Sub

