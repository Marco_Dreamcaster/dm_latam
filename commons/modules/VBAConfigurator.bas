Attribute VB_Name = "VBAConfigurator"
' manages code to and from the repository Commons folder
'
' Commons are components shared among all templates
'

' exports modules to Commons
'
Public Sub ToCommons()
    Dim wkbSource As Excel.Workbook
    Dim szSourceWorkbook As String
    Dim szFileName As String
    Dim lstMonitoredComponents As Collection
    
    Dim FSO As Scripting.FileSystemObject
    Set FSO = CreateObject("Scripting.FileSystemObject")
    
    App.AutoConfigure
    
    Set lstMonitoredComponents = monitoredComponents
    szSourceWorkbook = ActiveWorkbook.Name
    
    Set wkbSource = Application.Workbooks(szSourceWorkbook)
    
    ' 1. traverse the component list
    '
    For Each cmpComponent In wkbSource.VBProject.VBComponents
        szFileName = cmpComponent.Name
    
        Select Case cmpComponent.Type
            Case vbext_ct_ClassModule
                szFileName = szFileName & ".cls"
            Case vbext_ct_MSForm
                szFileName = szFileName & ".frm"
            Case vbext_ct_StdModule
                szFileName = szFileName & ".bas"
        End Select
    
        ' 2. exports monitored modules
        '
        If TextHandler.ExistsIn(szFileName, lstMonitoredComponents) Then
            Dim cmpPath As String
            cmpPath = App.szCommonsFilePath & "\" & szFileName
        
            If (FSO.FileExists(cmpPath)) Then
                FSO.DeleteFile cmpPath, True
            End If
        
            cmpComponent.Export cmpPath
        
        End If
        
    Next cmpComponent
    

End Sub

' imports modules from Commons
'
Public Sub FromCommons()
    Dim wkbSource As Excel.Workbook
    Dim szSourceWorkbook As String
    Dim szFileName As String
    Dim lstMonitoredComponents As Collection
    
    Dim FSO As Scripting.FileSystemObject
    Set FSO = CreateObject("Scripting.FileSystemObject")
    
    App.AutoConfigure
    
    Set lstMonitoredComponents = monitoredComponents
    szSourceWorkbook = ActiveWorkbook.Name
    
    Set wkbSource = Application.Workbooks(szSourceWorkbook)
    
    ' 1. traverses the component list
    '
    For Each cmpComponent In wkbSource.VBProject.VBComponents
        szFileName = cmpComponent.Name
    
        Select Case cmpComponent.Type
            Case vbext_ct_ClassModule
                szFileName = szFileName & ".cls"
            Case vbext_ct_MSForm
                szFileName = szFileName & ".frm"
            Case vbext_ct_StdModule
                szFileName = szFileName & ".bas"
        End Select
    
        If TextHandler.ExistsIn(szFileName, lstMonitoredComponents) Then
            ' 2. renames component to avoid duplications
            '
            cmpComponent.Name = "OLD_" & cmpComponent.Name
            
            Dim cmpPath As String
            cmpPath = App.szCommonsFilePath & "\" & szFileName
        
            ' 3. adds with the proper name
            '
            wkbSource.VBProject.VBComponents.Import cmpPath
            
            
            ' 4. removes 'OLD_' version
            wkbSource.VBProject.VBComponents.Remove cmpComponent
        
        End If
        
    Next cmpComponent
    
    ThisWorkbook.Sheets(TextHandler.CONFIG_COMMONS).UsedRange.Cells(App.CONFIG_COMMONS_VERSION, CONFIG_VALUES_COLUMN).value = App.COMMONS_VERSION
    
 

End Sub

' monitored components are collectively referred as Commons
'
Private Function monitoredComponents() As Collection
    Dim result As Collection
    Set result = New Collection
    
    result.Add "App.bas"
    result.Add "Commons.bas"
    result.Add "VBAConfigurator.bas"
    result.Add "Generator.bas"
    result.Add "TextHandler.bas"
    result.Add "ValidationProgress.frm"
    result.Add "UnitTests.bas"
    
    result.Add "CleansingDescriptorField.cls"
    result.Add "CleansingDescriptorVO.cls"
    result.Add "CommonsConfiguratorService.cls"
    result.Add "GeneratorService.cls"
    result.Add "SynchronizerService.cls"
    result.Add "ValidatorService.cls"
    result.Add "XLConfiguratorService.cls"
    result.Add "LoadFull.cls"
    result.Add "LoadMergeSimple.cls"
    result.Add "LoadKPI.cls"
    result.Add "LoadStrategy.cls"
    
    Set monitoredComponents = result

End Function

