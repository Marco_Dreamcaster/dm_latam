VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} ValidationProgress 
   Caption         =   "Data Migration Validation"
   ClientHeight    =   2415
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   4560
   OleObjectBlob   =   "ValidationProgress.frx":0000
   ShowModal       =   0   'False
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "ValidationProgress"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False





















Option Explicit

Public iDisplayCount As Integer

Private sPctCompl As Single

Const NOTIFICATION_STEP = 2

Public Sub UpdateAction(ByVal szAction As String, Optional ByVal bHidePercent As Boolean = False)
    Action.Caption = szAction
    If (bHidePercent) Then
        Progress.Caption = ""
    End If

End Sub

Public Sub UpdateProgress(ByVal iCurrent As Integer, ByVal iCount As Long)
    sPctCompl = (iCurrent / iCount) * 100
    
    Dim iPctComplete, iDisplayComplete As Long
    iPctComplete = CInt(sPctCompl)
    
    If ((iPctComplete = 0) Or ((iPctComplete <> iDisplayCount) And (iPctComplete Mod NOTIFICATION_STEP = 0))) Then
        iDisplayCount = iPctComplete
        
        Progress.Caption = Format(iPctComplete, "#,##") & "% completed"
        Bar.Width = sPctCompl * 2
        DoEvents
        Repaint
        
    End If
    
    Application.StatusBar = "Progress: " & iCurrent & " of " & iCount & " : " & Format(iCurrent / iCount, "0%")
    
End Sub
