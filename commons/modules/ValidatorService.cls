VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ValidatorService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' user interaction fields
'
Private rngValidated As Range
Private rngAction As Range
Private rngCleansed As Range

' metadata columns
'
Private Const FIELD_NAME = 1
Private Const COLUMN_NUMBER = 2
Private Const MANDAT = 3
Private Const MAX_LENGTH = 4
Private Const CUSTOM_VALIDATION = 5
Private Const POST_PROCESSING = 6
Private Const FORMAT_STR = 7
Private Const ORA_TYPE = 8
Private Const PRIMARY_COL = 9
Private Const HIDDEN_COL = 10
Private Const LOCKED_COL = 11
Private Const CONSTANT_COL = 12
Private Const LOV_COL = 13
Private Const FK_COL = 14
Private Const ISOUTPUT_COL = 15
Private Const OUPUTPOS_COL = 16


' VO level
Private pVODescriptorsBySheetIdx As Scripting.Dictionary
Private pVODescriptorsByShortName As Scripting.Dictionary
Private pVODescriptorsByStgTableName As Scripting.Dictionary

' field-level
Private pFieldDescriptorsByColIndex As Scripting.Dictionary
Private pFieldDescriptorsByColName As Scripting.Dictionary

' main row-level validation routine
' performs validations according to parameters on CleansingDescriptorField
'
Public Sub CheckEntireRow(ByVal idxWorksheet As Integer, ByVal idxRow As Integer, ByVal validations As Scripting.Dictionary)

    Dim wksData As Worksheet

    Err.Clear
    On Error GoTo eh

    ' set up user interaction fields
    
    Set wksData = Worksheets(idxWorksheet)
    
    Set rngValidated = wksData.UsedRange.Cells(idxRow, VALIDATED)
    Set rngAction = wksData.UsedRange.Cells(idxRow, Action)
    Set rngCleansed = wksData.UsedRange.Cells(idxRow, CLEANSED)
    
    
    Dim rowValues As Range
    Set rowValues = Worksheets(idxWorksheet).UsedRange.Rows(idxRow)
    
    wksData.Unprotect
    
    ' 1. for each column, check if there is a validation registered
    '
    Dim cel As Range
    For Each cel In rowValues.Cells
        If (validations.Exists(cel.Column)) Then
        
            ' 1. Trim spaces
            ' Excel lets user be creative, and insert spaces within cell values
            '
            If Len(cel.value) <> Len(Trim(cel.value)) Then
                IgnoreNextValueChange = True
                cel.value = Trim(cel.value)
            End If
            
            Dim descriptor As CleansingDescriptorField
            Set descriptor = validations(cel.Column)
            
            ' first failed validation will throw exception, short-circuit the remaining validations
            '
            
            ' 2. check cleansed state
            '
            If (rngCleansed.value = "") Or ((rngCleansed.value <> "D") And (rngCleansed.value <> "C")) Then
                Err.Raise App.ERROR_MISSING_CLEANSED, cel.row, "missing valid cleansed state"
            End If
            
            If (rngCleansed.value = "D") Then
                Err.Raise App.ERROR_DIRTY_RECORD, cel.row, "dirty record has not been reviewed yet"
            End If
            
            ' 3. apply constant value
            '
            If (descriptor.Constant <> "") Then
                IgnoreNextValueChange = True
                cel.value = descriptor.Constant
            
                If (Len(descriptor.Constant) > descriptor.MaxLength) Then
                    Err.Raise App.ERROR_CONSTANT_MAX_LENGTH_EXCEEDED, cel.row, descriptor.fieldName & " -> max length of constant exceeds " & descriptor.MaxLength
                End If
                
            End If
            
            ' 4. check mandatory
            '
            If (descriptor.Mandatory) And (Len(cel.Text) = 0) Then
                Err.Raise App.ERROR_MISSING_MANDATORY, cel.row, "missing mandatory " & descriptor.fieldName
            End If
            
            ' 5. type validations
            '
            If (Len(cel.Text) > 0) And (descriptor.IsDate) And Not (IsDate(cel.Text)) Then
                Err.Raise App.ERROR_NOT_A_DATE, cel.row, descriptor.fieldName & " does not contain a valid date"
            End If
            
            If (Len(cel.Text) > 0) And (descriptor.IsNumber) And Not (IsNumeric(cel.Text)) Then
                Err.Raise App.ERROR_NOT_A_NUMBER, cel.row, descriptor.fieldName & " does not contain a valid number"
            End If
            
            If (descriptor.IsVarchar) And (Len(cel.Text) > descriptor.MaxLength) Then
                Err.Raise App.ERROR_MAX_LENGTH_EXCEEDED, cel.row, descriptor.fieldName & " -> max length of " & descriptor.MaxLength & " exceeded"
            End If
            
            
            ' 6. custom validations
            '
            If (descriptor.CustomValidation <> "") Then
                App.CustomValidationErrorCode = App.ERROR_NONE
                App.CustomValidationMessage = ""
            
                Application.Run descriptor.CustomValidation, cel
                
                If (App.CustomValidationErrorCode <> App.ERROR_NONE) Then
                    Err.Raise App.CustomValidationErrorCode, cel.row, App.CustomValidationMessage
                End If
                
            End If
            
            ' 7. custom post-processing
            '
            If (descriptor.PostProcessing <> "") Then
                Application.Run descriptor.PostProcessing, cel
            End If
            
            ' 8.dataset-level validation
            '
            If (descriptor.FK <> "") Then
                CheckFK descriptor.FK, cel.Text
            End If
            
            ' 9. LOV value enforce
            If (descriptor.LOV <> "") Then
                If (cel.Validation.value <> True) Then
                    Err.Raise App.ERROR_NOT_IN_LOV, cel.row, cel.value & " not in LOV for " & descriptor.fieldName
                End If
            End If
        
        End If
    
    
    Next cel
    
    ' 2. congratulations, this row will survive
    
    DisabledValidations = True
    rngValidated.Locked = False
    rngAction.Locked = False
    
    rngValidated.value = "A"
    rngAction.value = ""
    
    rngValidated.Locked = False
    rngAction.Locked = False
    DisabledValidations = False
    
  
    Exit Sub

eh:
    'catch the exception and
    'let the formatting rules from the sheet take care of color coding
    
    wksData.Unprotect
    
    DisabledValidations = True
    rngValidated.Locked = False
    rngAction.Locked = False
    
    rngValidated.value = "R"
    rngAction.value = Err.Description
    
    rngValidated.Locked = True
    rngAction.Locked = True
    DisabledValidations = False
    
    Err.Clear
    
End Sub



Public Sub Class_Terminate()
    Set pVODescriptorsBySheetIdx = Nothing
    Set pVODescriptorsByShortName = Nothing
    Set pVODescriptorsByStgTableName = Nothing
    Set pFieldDescriptorsByColIndex = Nothing
    Set pFieldDescriptorsByColName = Nothing
    
End Sub

Public Property Get DescriptorsBySheetIdx() As Scripting.Dictionary
    Set DescriptorsBySheetIdx = pVODescriptorsBySheetIdx
End Property

Public Property Let DescriptorsBySheetIdx(ByVal value As Scripting.Dictionary)
    Set pVODescriptorsBySheetIdx = value
End Property

Public Property Get DescriptorsByShortName() As Scripting.Dictionary
    Set DescriptorsByShortName = pVODescriptorsByShortName
End Property

Public Property Let DescriptorsByShortName(ByVal value As Scripting.Dictionary)
    Set pVODescriptorsByShortName = value
End Property

Public Property Get DescriptorsByStgTableName() As Scripting.Dictionary
    Set DescriptorsByStgTableName = pVODescriptorsByStgTableName
End Property

Public Property Let DescriptorsByStgTableName(ByVal value As Scripting.Dictionary)
    Set pVODescriptorsByStgTableName = value
End Property

Public Function MinRegisteredDataIdx() As Integer
    MinRegisteredDataIdx = 0
    
    Dim szShortName As Variant
    For Each szShortName In pVODescriptorsByShortName.Keys
        Dim descriptor As CleansingDescriptorVO
        Set descriptor = pVODescriptorsByShortName(szShortName)
    
        If (descriptor.DataSheetIdx < MinRegisteredDataIdx) Or (MinRegisteredDataIdx = 0) Then
            MinRegisteredDataIdx = descriptor.DataSheetIdx
        End If
    
    Next szShortName
    
End Function

' loads VO cleansing descriptors at VO-level
'
Public Sub LoadDescriptorsAtVOLevel(ByVal foundData As Collection, ByVal foundConfig As Collection, ByVal foundTransactional As Collection)
    Set pVODescriptorsBySheetIdx = New Scripting.Dictionary
    Set pVODescriptorsByShortName = New Scripting.Dictionary
    Set pVODescriptorsByStgTableName = New Scripting.Dictionary
    
    Dim k As Variant
    
    For Each k In foundConfig
        ' if there is a corresponding data sheet
        Dim dataSheetName As String
        dataSheetName = TextHandler.fromConfigToDataName(ThisWorkbook.Sheets(k).Name)
        If (TextHandler.ExistsIn(dataSheetName, foundData)) Then
            LoadCleansingDescriptorVO k, dataSheetName
        
        Else
            MsgBox "could not find associated data for " & k
            
        End If
        
    Next k
    
    ' Deals with Transactional VOs
    For Each k In foundTransactional
        LoadCleansingDescriptorVO k
         
    Next k

    
End Sub

Private Sub LoadCleansingDescriptorVO(ByVal configSheet As String, Optional dataSheetName As String = "")
    ' 3. index descriptor by config sheet name
    ' the config sheet name will be used in each Worksheet_Activate to determine if validations will be triggered
    '
    
    Dim shortName As String
    shortName = TextHandler.extractShortName(ThisWorkbook.Sheets(configSheet).Name)
    
    Dim VODescriptor As CleansingDescriptorVO
    Set VODescriptor = New CleansingDescriptorVO
    
    VODescriptor.shortName = shortName
    
    If (dataSheetName <> "") Then
        VODescriptor.DataSheetIdx = ThisWorkbook.Sheets(dataSheetName).Index
        VODescriptor.Transactional = False
    Else
        VODescriptor.DataSheetIdx = 2 'second sheet, after version, only for transactional workbooks
        VODescriptor.Transactional = True
    End If
    
    VODescriptor.stgTableName = ThisWorkbook.Sheets(configSheet).UsedRange.Cells(CONFIG_STAGE_TABLE_NAME, CONFIG_VALUES_COLUMN)
    VODescriptor.FlatFileTemplateName = ThisWorkbook.Sheets(configSheet).UsedRange.Cells(CONFIG_FLAT_FILE_TEMPLATE_NAME, CONFIG_VALUES_COLUMN)
    VODescriptor.OutputMultiplier = ThisWorkbook.Sheets(configSheet).UsedRange.Cells(CONFIG_OUTPUT_MULTIPLIER, CONFIG_VALUES_COLUMN)
    VODescriptor.LoadingProcedure = ThisWorkbook.Sheets(configSheet).UsedRange.Cells(CONFIG_LOADING_PROCEDURE, CONFIG_VALUES_COLUMN)
    
    
    'loads pFieldDescriptorsByColIndex and pFieldDescriptorsByColName
    LoadValidatorsAtFieldLevel (ThisWorkbook.Sheets(configSheet).ListObjects(1))
    VODescriptor.DescriptorsByColIdx = pFieldDescriptorsByColIndex
    VODescriptor.DescriptorsByColName = pFieldDescriptorsByColName
    
    pVODescriptorsBySheetIdx.Add VODescriptor.DataSheetIdx, VODescriptor
    pVODescriptorsByShortName.Add shortName, VODescriptor
    pVODescriptorsByStgTableName.Add VODescriptor.stgTableName, VODescriptor             ' to be used by FK validation

End Sub

' performs row-level checks for all rows in a data worksheet
'
Public Sub CheckAllRows(ByVal idxWorksheet As Integer, ByVal validations As Scripting.Dictionary, Optional UserFeedback As Boolean = True)
    Dim rw, allRows As Range
    Dim count, current As Long
    Dim Progress As Single
    
    Set allRows = Sheets(idxWorksheet).UsedRange.Rows
    
    On Error GoTo eh
    
    'this can take some time
    Application.ScreenUpdating = False
    Application.Calculation = xlCalculationManual
    
    'give some feedback to the user
    current = 0
    count = allRows.count - 1
    
    If (UserFeedback) Then
        ValidationProgress.Show vbModeless
        ValidationProgress.UpdateAction "validating " & Worksheets(idxWorksheet).Name
        ValidationProgress.UpdateProgress 0, count
    End If
    
    For Each rw In allRows
        If (rw.row <> 1) Then
            CheckEntireRow idxWorksheet, rw.row, validations
            
            current = current + 1
            
            If (UserFeedback) Then
                ValidationProgress.UpdateProgress current, count
            End If
        
        End If
    Next rw
    
eh:
    If (UserFeedback) Then
        ValidationProgress.Hide
        Unload ValidationProgress
    End If
    
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic

    If (Err.Number <> 0) Then
        Debug.Print Err.Number & " - " & Err.Description
    
    End If
    
End Sub


Private Sub CheckFK(ByVal FK As String, ByVal key As String)
    Dim stgTableName As String
    stgTableName = TextHandler.ExtractStgTableNameFromFK(FK)
    
    If (pVODescriptorsByStgTableName.Exists(stgTableName)) Then
        Dim VODescriptor As CleansingDescriptorVO
        Set VODescriptor = pVODescriptorsByStgTableName(stgTableName)
        
        Dim fieldName As String
        fieldName = TextHandler.ExtractFieldNameFromFK(FK)
        
        If VODescriptor.DescriptorsByColName.Exists(fieldName) Then
            Dim FieldDescriptor As CleansingDescriptorField
            Set FieldDescriptor = VODescriptor.DescriptorsByColName(fieldName)
            
            Dim DataSheet As Worksheet
            Set DataSheet = Worksheets(VODescriptor.DataSheetIdx)
            
            Dim ForeignKeyValues As Range
            Set ForeignKeyValues = DataSheet.Range( _
                                    DataSheet.Cells(App.FIRST_DATA_ROW, FieldDescriptor.ColumnNumber), _
                                    DataSheet.Cells(DataSheet.UsedRange.Rows.count, FieldDescriptor.ColumnNumber))
                                    
            Dim rngKeyFound As Range
            Set rngKeyFound = ForeignKeyValues.Find(What:=key, LookIn:=xlValues, SearchOrder:=xlByRows, SearchDirection:=xlNext)
            
            If (rngKeyFound Is Nothing) Then
                Err.Raise App.ERROR_MISSING_VALUE_FK, key, "missing key " & key & " on " & FK
            End If
            
            Dim rngKeyValidated As Range
            
            Set rngKeyValidated = DataSheet.Cells(rngKeyFound.row, VALIDATED)
            
            If (rngKeyValidated.Text = "R") Then
                Err.Raise App.ERROR_INVALID_PARENT_FK, key, "invalidated parent key " & key & " on " & FK
            End If
            
            ' if record got here unchecked, congratulations, it won't be blocked
        
        Else
            Err.Raise App.ERROR_MISSING_FIELD_FK, FK, "missing field named " & fieldName
        
        End If
    Else
        Err.Raise App.ERROR_NOT_IMPLEMENTED, FK, "unimplemented validation for non-local table " & stgTableName
    
    End If
    
    
    
    
    
    
    
End Sub




' loads VO cleansing descriptors at Field-level
'
Private Sub LoadValidatorsAtFieldLevel(ByVal configData As ListObject)

    Set pFieldDescriptorsByColIndex = New Scripting.Dictionary
    Set pFieldDescriptorsByColName = New Scripting.Dictionary
   
    Dim x As Long
    For x = 1 To configData.DataBodyRange.Rows.count
        Dim descriptor As CleansingDescriptorField
        Set descriptor = New CleansingDescriptorField
        
        descriptor.fieldName = configData.DataBodyRange(x, FIELD_NAME).value
        descriptor.ColumnNumber = configData.DataBodyRange(x, COLUMN_NUMBER).value
        descriptor.Mandatory = IIf(configData.DataBodyRange(x, MANDAT).value = "Y", True, False)
        descriptor.CustomValidation = configData.DataBodyRange(x, CUSTOM_VALIDATION).value
        descriptor.PostProcessing = configData.DataBodyRange(x, POST_PROCESSING).value
        descriptor.Format = configData.DataBodyRange(x, FORMAT_STR)
        
        ' fix incompatible declarations of max length
        
        descriptor.ORAType = configData.DataBodyRange(x, ORA_TYPE).value
        descriptor.MaxLength = configData.DataBodyRange(x, MAX_LENGTH).value
        
        If (descriptor.MaxLengthNeedsFixing) Then
            configData.DataBodyRange(x, MAX_LENGTH).value = descriptor.MaxLength
        End If
        
        descriptor.Primary = IIf(configData.DataBodyRange(x, PRIMARY_COL).value = "Y", True, False)
        descriptor.Locked = IIf(configData.DataBodyRange(x, LOCKED_COL).value = "Y", True, False)
        descriptor.Hidden = IIf(configData.DataBodyRange(x, HIDDEN_COL).value = "Y", True, False)
        descriptor.Constant = configData.DataBodyRange(x, CONSTANT_COL)
        descriptor.LOV = configData.DataBodyRange(x, LOV_COL)
        descriptor.FK = configData.DataBodyRange(x, FK_COL)
        descriptor.IsOutputValue = IIf(configData.DataBodyRange(x, ISOUTPUT_COL).value = "Y", True, False)
        descriptor.OutputPos = configData.DataBodyRange(x, OUPUTPOS_COL)
        
        pFieldDescriptorsByColIndex.Add descriptor.ColumnNumber, descriptor
        pFieldDescriptorsByColName.Add descriptor.fieldName, descriptor
    
    Next x

End Sub


