VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "XLConfiguratorService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' XLFormatter specializes in customizations of the Excel UI

'
' On DataSheets it controls color coding, enforces LOV, hides columns
'
'

Option Explicit

Private pWHITE As Long
Private pHIGHLIGHT As Long
Private pDEFAULT As Long
Private pLIGHTER As Long

Private pVALID As Long
Private pOVERRIDE As Long
Private pREJECTED As Long
Private pDIRTY As Long
Private pIGNORE As Long

Private Const YES_NO_COLUMN_WIDTH = 6
Private Const ACTION_COLUMN_WIDTH = 45
Private Const FIRST_FREEZE_CELL = 5

Public Function TrimDataView(ByVal descriptor As CleansingDescriptorVO, ByRef metrics As Dictionary)
    On Error GoTo eh
    Err.Clear
    
    Dim loData As ListObject
    Set loData = NormalizeDataView(descriptor, metrics)
    
    Debug.Print loData.Name
    
    Dim wksData As Worksheet
    Set wksData = Sheets(descriptor.DataSheetIdx)

    Dim foundFirstDirtyCellPos As Double
    foundFirstDirtyCellPos = -1
    foundFirstDirtyCellPos = Application.Match("D", wksData.Range("$D$1:$D" & loData.DataBodyRange.Rows.count + 1), 0)
    
    App.DisabledValidations = True

    Dim DirtyRowRange As String
    DirtyRowRange = foundFirstDirtyCellPos & ":" & loData.DataBodyRange.Rows.count + 1
  
    loData.Range.Rows(DirtyRowRange).Delete
    
    App.DisabledValidations = False
    
    Dim infoJSON As String
    infoJSON = "{cleansed:" & Format(foundFirstDirtyCellPos - 1, "000000") & ", " & _
               "dirty:" & Format(0, "000000") & ", " & _
               "total:" & Format(foundFirstDirtyCellPos - 1, "000000") & "}"

    
    
    metrics(descriptor.shortName) = infoJSON

    
    
    Set TrimDataView = loData
    Exit Function
    
eh:
    Debug.Print Err.Number & "-" & Err.Description
    Err.Clear
    
    Set TrimDataView = loData
    
End Function


Public Function NormalizeDataView(ByVal descriptor As CleansingDescriptorVO, ByRef metrics As Dictionary)
    On Error GoTo eh
    Err.Clear
    
    Dim wksData As Worksheet
    Set wksData = Sheets(descriptor.DataSheetIdx)
    
    Dim loData As ListObject
    Set loData = wksData.ListObjects(1)
    
    loData.Name = descriptor.DataName
    
    loData.AutoFilter.ShowAllData
    
    With loData.Sort
        With .SortFields
            .Clear
            .Add wksData.Range("$D1")
            .Add wksData.Range("$C1"), Order:=xlDescending
        End With
        .Apply
    End With
    
    Dim foundFirstDirtyCellPos As Double
    foundFirstDirtyCellPos = -1
    foundFirstDirtyCellPos = Application.Match("D", wksData.Range("$D$1:$D" & loData.DataBodyRange.Rows.count + 1), 0)
    
    Dim cleansedCount As Integer
    cleansedCount = foundFirstDirtyCellPos - 2
    
    Dim infoJSON As String
    infoJSON = "{cleansed:" & Format(cleansedCount, "000000") & ", " & _
               "dirty:" & Format(loData.DataBodyRange.Rows.count - cleansedCount, "000000") & ", " & _
               "total:" & Format(loData.DataBodyRange.Rows.count, "000000") & "}"

    
    
    metrics(descriptor.shortName) = infoJSON
    
    Set NormalizeDataView = loData
    
    Exit Function
    
eh:
    Debug.Print Err.Number & "-" & Err.Description
    Err.Clear


End Function

Private Function MatchPrimaryKey(ByVal oRng As Range, ByVal key As Variant)
    On Error GoTo eh
    Err.Clear
    
    Dim foundKeyCellPos As Double
    foundKeyCellPos = -1
    foundKeyCellPos = Application.Match(key, oRng, 0)
    
eh:
    Err.Clear


    MatchPrimaryKey = foundKeyCellPos

End Function


' color coding
Private Sub Class_Initialize()
    pWHITE = RGB(255, 255, 255)

   'greyish
    pDEFAULT = RGB(100, 120, 100)
    
    'red
    pHIGHLIGHT = RGB(120, 30, 30)

   'cleansed, recommended, and validated ( pale green )
    pVALID = RGB(148, 237, 148)
    
    'cleansed, unrecommended, and validated ( darker green )
    pOVERRIDE = RGB(70, 200, 70)
    
    'cleansed and rejected  (yellow background and red foreground)
    pREJECTED = RGB(237, 237, 148)
    
    'dirty and recommended (light grey)
    pDIRTY = RGB(220, 220, 220)
    
    'dirty and unrecommended (darker grey)
    pIGNORE = RGB(180, 180, 180)
    
    'lighter
    pLIGHTER = RGB(150, 150, 150)

End Sub




' adds user interaction columns
'
Public Sub FormatDataTables(foundData As Collection)
    Dim wksData As Worksheet
    Dim rngHeaderRow As Range
    Dim rngValuesCol As Range
    Dim rngAllowedValues As Range
    Dim lastRow As Long
    Dim ds, fieldKey As Object

    App.DisabledValidations = True

    For Each ds In foundData
        Set wksData = Worksheets(ds)
        wksData.Unprotect
        
        ' 1. all shall be free, except the Locked ones
        '
        wksData.Cells.ClearFormats
        wksData.Cells.Locked = False
        wksData.Cells.Validation.Delete
        
        lastRow = wksData.UsedRange.SpecialCells(xlCellTypeLastCell).row
        
        Set rngHeaderRow = wksData.Rows(App.HEADER_ROW)
        rngHeaderRow.ClearContents
        
        
        ' 2. creates the table based on VODescriptor and UsedRange
        '
        Dim VODescriptor As CleansingDescriptorVO
        Dim shortName As String
        
        shortName = TextHandler.extractShortName(wksData.Name)
        
        Set VODescriptor = App.svcValidator.DescriptorsByShortName(shortName)
        
        ' removes previous values
        
        If wksData.ListObjects.count <> 0 Then
            wksData.ListObjects(1).Unlist
        End If
        
        Dim tableRange As Range
        Set tableRange = wksData.Range(wksData.Cells(App.HEADER_ROW, App.VALIDATED), wksData.Cells(lastRow, VODescriptor.MaxRegisteredColum))
        
        Dim lo As ListObject
        
        Set lo = wksData.ListObjects.Add( _
            SourceType:=xlSrcRange, _
            Source:=tableRange, _
            XlListObjectHasHeaders:=xlYes)
            
        lo.Name = VODescriptor.DataName
        lo.TableStyle = "TableStyleMedium4"
        
        ' 3. add fields headers from flat-file spec
        '
        Dim k As Variant
        For Each k In VODescriptor.DescriptorsByColIdx.Keys
        
            Dim FieldDescriptor As CleansingDescriptorField
            Set FieldDescriptor = VODescriptor.DescriptorsByColIdx(k)
            
            With rngHeaderRow.Columns(FieldDescriptor.ColumnNumber)
                .value = FieldDescriptor.fieldName
                .Orientation = xlHorizontal
                .HorizontalAlignment = xlCenter
                .VerticalAlignment = xlCenter
                .Font.Bold = True
            End With
            
            If (FieldDescriptor.Mandatory) Then
                rngHeaderRow.Columns(FieldDescriptor.ColumnNumber).Font.Color = pHIGHLIGHT
                rngHeaderRow.Columns(FieldDescriptor.ColumnNumber).value = rngHeaderRow.Columns(FieldDescriptor.ColumnNumber) & "(*)"
            End If
            
            Dim dataRange As Range
            Set dataRange = wksData.Range(wksData.Cells(App.FIRST_DATA_ROW, FieldDescriptor.ColumnNumber), wksData.Cells(lastRow, FieldDescriptor.ColumnNumber))
            
            If (FieldDescriptor.IsVarchar) Then
                dataRange.NumberFormat = "@"
            End If
            
            If (FieldDescriptor.IsDate) Then
                dataRange.NumberFormat = "YYYY-MM-DD"
                dataRange.HorizontalAlignment = xlRight
            End If
            
            dataRange.Locked = FieldDescriptor.Locked
            
            If (FieldDescriptor.LOV <> "") Then
               dataRange.Validation.Delete
               
               If InStr(1, FieldDescriptor.LOV, "Commons") <> 0 Then
                    Dim formula As String
                    formula = "=" & Replace(FieldDescriptor.LOV, "Commons", "'Config - Commons'")
                    dataRange.Validation.Add Type:=xlValidateList, Formula1:=formula
               
               Else
                    dataRange.Validation.Add Type:=xlValidateList, Formula1:=FieldDescriptor.LOV
               
               End If
               
            End If
            
            
            If (FieldDescriptor.Hidden) Then
                wksData.Columns(FieldDescriptor.ColumnNumber).Hidden = True

            Else
                wksData.Columns(FieldDescriptor.ColumnNumber).AutoFit
            
            End If
            
            
            
        Next k
        
        ' configure fixed headers
        '
        rngHeaderRow.RowHeight = 128
        
        ' fixed header CLEANSED
        With rngHeaderRow.Columns(App.CLEANSED)
            .value = "CLEANSED" & vbCrLf & "RECORD"
            .Interior.Color = pLIGHTER
            .Orientation = xlUpward
            .HorizontalAlignment = xlCenter
            .VerticalAlignment = xlCenter
            .Font.Bold = True
            .Locked = True
        End With
                
        wksData.Columns(App.CLEANSED).ColumnWidth = YES_NO_COLUMN_WIDTH
        
        Set rngAllowedValues = Worksheets(TextHandler.CONFIG_COMMONS).Cells(App.CONFIG_CLEANSED_TAGS, App.CONFIG_VALUES_COLUMN)
        Set rngValuesCol = wksData.Range(wksData.Cells(App.FIRST_DATA_ROW, App.CLEANSED), wksData.Cells(lastRow, App.CLEANSED))
        
        With rngValuesCol
            .Validation.Delete
            .Validation.Add Type:=xlValidateList, AlertStyle:=xlValidAlertStop, _
                    Operator:=xlBetween, Formula1:=rngAllowedValues.Text
            .Locked = False
            .HorizontalAlignment = xlCenter
            .VerticalAlignment = xlCenter
        End With
        
        ' fixed header VALIDATED
        '
        With rngHeaderRow.Columns(App.VALIDATED)
            .value = "VALIDATED" & vbCrLf & "RECORD"
            .Interior.Color = pVALID
            .Orientation = xlUpward
            .HorizontalAlignment = xlCenter
            .VerticalAlignment = xlCenter
            .Font.Bold = False
            .Locked = True
        End With
        
        wksData.Columns(App.VALIDATED).ColumnWidth = YES_NO_COLUMN_WIDTH
                
        Set rngAllowedValues = Worksheets(TextHandler.CONFIG_COMMONS).Cells(CONFIG_VALIDATION_TAGS, CONFIG_VALUES_COLUMN)
        
        Set rngValuesCol = wksData.Range(wksData.Cells(App.FIRST_DATA_ROW, App.VALIDATED), wksData.Cells(lastRow, App.VALIDATED))
        rngValuesCol.Locked = True
        
        With rngValuesCol
            .Validation.Delete
            .Validation.Add Type:=xlValidateList, AlertStyle:=xlValidAlertStop, _
                    Operator:=xlBetween, Formula1:=rngAllowedValues.Text
            .Locked = True
            .HorizontalAlignment = xlCenter
            .VerticalAlignment = xlCenter
        End With
        
        'fixed header ACTION
        '
        With rngHeaderRow.Columns(App.Action)
            .value = "CURRENT" & vbCrLf & "ACTION"
            .Interior.Color = pLIGHTER
            .Orientation = xlHorizontal
            .HorizontalAlignment = xlCenter
            .VerticalAlignment = xlCenter
            .Font.Color = pWHITE
            .Font.Bold = False
            .Locked = True
        End With
        
        wksData.Columns(App.Action).ColumnWidth = ACTION_COLUMN_WIDTH
        
        Set rngValuesCol = wksData.Range(wksData.Cells(FIRST_DATA_ROW, App.Action), wksData.Cells(lastRow, App.Action))
        rngValuesCol.Locked = True
        
        ' fixed header RECOMMENDED
        '
        With rngHeaderRow.Columns(App.RECOMMENDED)
            .value = "RECOMMENDED " & vbCrLf & "FOR MIGRATION"
            .Interior.Color = pLIGHTER
            .Orientation = xlUpward
            .HorizontalAlignment = xlCenter
            .VerticalAlignment = xlCenter
            .Font.Bold = False
            .Locked = True
        End With
        
        wksData.Columns(App.RECOMMENDED).ColumnWidth = YES_NO_COLUMN_WIDTH
        
        Set rngAllowedValues = Worksheets(TextHandler.CONFIG_COMMONS).Cells(App.CONFIG_RECOMMENDED_TAGS, App.CONFIG_VALUES_COLUMN)
        
        Set rngValuesCol = wksData.Range(wksData.Cells(App.FIRST_DATA_ROW, App.RECOMMENDED), wksData.Cells(lastRow, App.RECOMMENDED))
        
        With rngValuesCol
            'Validation.Delete
            'Validation.Add Type:=xlValidateList, AlertStyle:=xlValidAlertStop, _
                    Operator:=xlBetween, Formula1:=rngAllowedValues.Text
            .HorizontalAlignment = xlCenter
            .VerticalAlignment = xlCenter
            .Locked = True
        End With
        
        
        ' freezes panes and protect changes
        
        wksData.Activate
        
        ActiveWindow.FreezePanes = False
        
        Dim rngFreeze As Range
        Set rngFreeze = wksData.Cells(App.FIRST_DATA_ROW, FIRST_FREEZE_CELL)
        rngFreeze.Select
        
        ActiveWindow.FreezePanes = True
        
        ' wksData.Protect
    Next ds

    App.DisabledValidations = False

End Sub


' reset Conditional Formatting
'
Public Sub ResetConditionalFormatting(foundData As Collection)
    Dim wksData As Worksheet
    
    Dim AND_ As String
    Dim SEP_ As String
    
    Dim ds As Variant
    
    AND_ = Commons.Localized_AND
    SEP_ = Commons.Localized_FUNCTION_SEPARATOR
    
    For Each ds In foundData
        Set wksData = Worksheets(ds)
        wksData.Unprotect
        
        'wipe out all format conditions
        wksData.Cells.FormatConditions.Delete

        Dim descriptor As CleansingDescriptorVO
        Set descriptor = App.svcValidator.DescriptorsByShortName(TextHandler.extractShortName(CStr(ds)))
        
        If (descriptor.IsAnyPrimaryDefined) Then
            If (descriptor.GetPrimaryKeys.count = 1) Then
                Dim descriptorPrimary As CleansingDescriptorField
                Set descriptorPrimary = descriptor.GetPrimaryKeys(1)
            
                Debug.Print descriptorPrimary.fieldName & "-" & descriptorPrimary.ColumnNumber
                
                Dim primaryRange As Range
                Set primaryRange = wksData.Range(wksData.Cells(App.FIRST_DATA_ROW, descriptorPrimary.ColumnNumber), _
                                                    wksData.Cells(wksData.Rows.count, descriptorPrimary.ColumnNumber))
                                                    
                With primaryRange.FormatConditions.AddUniqueValues
                    .DupeUnique = xlDuplicate
                    .Interior.Color = pREJECTED
                End With
            
            End If

        End If


        'cleansed, recommended, and validated ( pale green )
        With wksData.UsedRange.FormatConditions.Add(Type:=xlExpression, Formula1:="=" & AND_ & "($A1=""A""" & SEP_ & "$D1=""C""" & SEP_ & "$C1=""Y""" & ")")
            .Font.Color = pDEFAULT
            .Font.Bold = False
            .Interior.Color = pVALID
            .StopIfTrue = False
        End With
         
        'cleansed, unrecommended, and validated ( darker green )
        With wksData.UsedRange.FormatConditions.Add(Type:=xlExpression, Formula1:="=" & AND_ & "($A1=""A""" & SEP_ & "$D1=""C""" & SEP_ & "$C1=""N""" & ")")
            .Font.Color = pDEFAULT
            .Font.Bold = False
            .Interior.Color = pOVERRIDE
            .StopIfTrue = False
        End With
         
        'cleansed and rejected  (yellow background and red foreground)
        With wksData.UsedRange.FormatConditions.Add(Type:=xlExpression, Formula1:="=" & AND_ & "($A1=""R""" & SEP_ & "$D1=""C"")")
            .Font.Bold = True
            .Font.Color = pDEFAULT
            .Interior.Color = pREJECTED
            .StopIfTrue = False
        End With
        
        'dirty and recommended (light grey)
        With wksData.UsedRange.FormatConditions.Add(Type:=xlExpression, Formula1:="=" & AND_ & "($D1=""D""" & SEP_ & "$C1=""N""" & ")")
            .Font.Color = pDEFAULT
            .Font.Bold = False
            .Interior.Color = pDIRTY
            .StopIfTrue = False
        End With
        
        'dirty and recommended (darker grey)
        With wksData.UsedRange.FormatConditions.Add(Type:=xlExpression, Formula1:="=" & AND_ & "($D1=""D""" & SEP_ & "$C1=""Y""" & ")")
            .Font.Color = pDEFAULT
            .Font.Bold = False
            .Interior.Color = pIGNORE
            .StopIfTrue = False
        End With
        
        
        
        
    
    Next ds

End Sub


Public Property Let Default(value As Long)
    pDEFAULT = value
End Property

Public Property Get Default() As Long
    Default = pDEFAULT
End Property
         
Public Property Let Valid(value As Long)
    pVALID = value
End Property

Public Property Get Valid() As Long
    Valid = pVALID
End Property
         
Public Property Let Override(value As Long)
    pOVERRIDE = value
End Property

Public Property Get Override() As Long
    Override = pOVERRIDE
End Property
         
Public Property Let Rejected(value As Long)
    pREJECTED = value
End Property

Public Property Get Rejected() As Long
    Rejected = pREJECTED
End Property

Public Property Let Dirty(value As Long)
    pDIRTY = value
End Property

Public Property Get Dirty() As Long
    Dirty = pDIRTY
End Property

Public Property Let Ignore(value As Long)
    pIGNORE = value
End Property

Public Property Get Ignore() As Long
    Ignore = pIGNORE
End Property

Public Property Let Lighter(value As Long)
    pLIGHTER = value
End Property

Public Property Get Lighter() As Long
    Lighter = pLIGHTER
End Property
         

