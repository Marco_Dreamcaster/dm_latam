# Datasets

Datasets consists of groups of flat files following a naming convention:


TKE_PAN_201711301030_VendorHeader.txt
TKE_PAN_201711301030_VendorSite.txt
TKE_PAN_201711301030_VendorSiteContact.txt
TKE_PAN_201711301030_VendorBankAccounts.txt

Additionally there is a report file which brings information about the datasets generated for PANama at 30-11-2017 10:30AM

TKE_PAN_201711301030_report.txt

Which datasets we keep on the repository will depend on circumstances.

But all related files should be easily grouped under the same prefix.  Groups could also be zipped (to save space).
