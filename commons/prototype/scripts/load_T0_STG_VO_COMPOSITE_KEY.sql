TRUNCATE TABLE STG_VO_COMPOSITE_KEY;

INSERT INTO STG_VO_COMPOSITE_KEY(
    VALIDATED,
    RECOMMENDED,
    CLEANSED,
    ID1,
    ID2,
    USER_STORY,
    MANDATORY_FIELD)
VALUES
('R', 'Y', 'D', 1, 1, 'valid (1,1) tuple', 'mandatory placeholder'),
('R', 'Y', 'D', 2, 1, 'valid (2,1) tuple', 'erase for missing value action'),
('R', 'Y', 'D', 2, 2, 'valid (2,2) tuple', 'erase for missing value action'),
('R', 'Y', 'D', 2, 3, 'valid (2,3) tuple', 'erase for missing value action'),
('R', 'Y', 'D', 3, 2, 'valid (3,2) tuple', 'mandatory placeholder'),
('R', 'Y', 'D', 4, 2, 'valid (4,2) tuple', 'erase for missing value action');
    

