TRUNCATE TABLE STG_VO_LOCAL_DETAIL;

INSERT INTO STG_VO_LOCAL_DETAIL(
    VALIDATED,
    RECOMMENDED,
    CLEANSED,
    ID,
    ID_MASTER,
    USER_STORY,
    MANDATORY_FIELD)
VALUES
('R', 'Y', 'D', 1, 1, 'valid local parent', 'mandatory placeholder'),
('R', 'Y', 'D', 2, 1, 'valid local parent', 'erase for missing value action'),
('R', 'Y', 'D', 3, 2, 'invalid local parent', 'mandatory placeholder'),
('R', 'Y', 'D', 4, 2, 'invalid local parent', 'erase for missing value action'),
('R', 'Y', 'D', 1, 2, 'bizarre case #1', 'erase for missing value action');
    

