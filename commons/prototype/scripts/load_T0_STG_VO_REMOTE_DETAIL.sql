TRUNCATE TABLE STG_VO_REMOTE_DETAIL;

INSERT INTO STG_VO_REMOTE_DETAIL(
    VALIDATED,
    RECOMMENDED,
    CLEANSED,
    ID,
    ID_MASTER,
    USER_STORY,
    MANDATORY_FIELD)
VALUES
('R', 'Y', 'D', 1, 1, 'valid remote parent', 'mandatory placeholder'),
('R', 'Y', 'D', 2, 1, 'valid remote parent', 'erase for missing value action'),
('R', 'Y', 'D', 3, 2, 'invalid remote parent', 'mandatory placeholder'),
('R', 'Y', 'D', 4, 2, 'invalid remote parent', 'erase for missing value action');
    

