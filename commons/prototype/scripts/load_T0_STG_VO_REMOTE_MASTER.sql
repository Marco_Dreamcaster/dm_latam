TRUNCATE TABLE STG_VO_REMOTE_MASTER;

INSERT INTO STG_VO_REMOTE_MASTER(
    VALIDATED,
    RECOMMENDED,
    CLEANSED,
    ID,
    USERSTORY,
    MANDATORY_FIELD
)
VALUES
('R', 'Y', 'D', 1, 'Happy Flow User Story', 'mandatory'),
('R', 'Y', 'D', 2, 'Unhappy User Story #2', 'remove');
    

