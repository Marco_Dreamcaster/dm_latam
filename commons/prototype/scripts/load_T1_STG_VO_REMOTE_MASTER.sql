TRUNCATE TABLE STG_VO_REMOTE_MASTER;

INSERT INTO STG_VO_REMOTE_MASTER(
    VALIDATED,
    RECOMMENDED,
    CLEANSED,
    ID,
    USERSTORY,
    MANDATORY_FIELD
)
VALUES
('R', 'Y', 'D', 1, 'Happy Flow User Story', 'mandatory'),
('R', 'Y', 'D', 2, 'Unhappy User Story', 'remove'),
('R', 'Y', 'D', 3, 'Happy Flow #2', 'mandatory'),
('R', 'Y', 'D', 4, 'Unhappy #2', 'remove'),
('R', 'Y', 'D', 5, 'Unhappy #3', 'remove')
;
    

