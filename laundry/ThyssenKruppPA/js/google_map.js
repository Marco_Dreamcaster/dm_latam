/*
 * Sistema de Roteirizacao - Versao 1.0
 * Desenvolvido por Fabiano Santos para ThyssenKrupp elevadores.
 * Julho/2017
 *
 * MVC/PHP 5.7
 *
 * Este sistema utiliza partes adaptadas de Codigos de James Tolley e Geir K. Engdahl <geir.engdahl@gmail.com> 
 * sob licença livre 
 *
 * ######################### OBSERVACOES ################################################################################
 *
 * ESTE CODIGO TEM UMA MELHOR PERFORMANCE NOS NAVEGADORES GOOGLE CHROME (1a Opcao ) E FIREFOX (2a Opcao), 
 * ALGUMAS FUNCIONALIDADES NAO FORAM REVISADAS PARA IE E TRAVAM O PROCESSAMENTO NESTE NAVEGADOR.
 * 
 *
 *
*/


(function ($) {

            //---------------------------------------------------------------------------------------------------------------
            // Define as variaveis do programa
            //---------------------------------------------------------------------------------------------------------------
            var tsp;                                                        // Cria um Objeto para os metodos do  TSP Solver computation.
            var mode;
            var markers = new Array();                                      // Cria um Array para alocar os pontos do mapa
            var bounds = new google.maps.LatLngBounds();
            var dirRenderer;                                                // Need pointer to path to clean up.
            var zoomLevel = 11;                                             // Define o Zoom (altitude ) para a visao do Mapa
            var center = new google.maps.LatLng(-23.6492, -46.6600);        // Define o centro do mapa
            var color = '#f9bd00';                                          // Define a Cor par o tracado da Rota
            var customerArr = [];                                           // Cria um Array para alocar a requisicao de Clientes.
            var zone_number = 0;                                            // Define a variavel para a selecao de Zona 
            var TravelMode = google.maps.DirectionsTravelMode.WALKING;      // Define o meio de deslocamento padrao a Pe' (WALKING)
            var server = 'localhost'   // '10.92.1.124'                                     // Especifica o servidor para as requisicoes de GET/POST
            
            //---------------------------------------------------------------------------------------------------------------
            // Para processo de tranferencia de equipamentos entre Zonas/Setores
            //---------------------------------------------------------------------------------------------------------------
            counter = 0;
            transfer = [];
            unidade = '';
            transfer_zone   = 0;
            transfer_sector = 0;

            // Instancia a tabela de registros de equipamentos para tranferencia (Modal)
            tb = $('#tbl1').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                bLengthChange: false,
                buttons: [
                {extend: 'excel', title: 'TransferirEquipamentos'},
                {extend: 'pdfHtml5',orientation: 'landscape',pageSize: 'LEGAL'},
                {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit'); }
                },

                ]
            });

            // Instancia a tabela de registros de equipamentos
            tb2 = $('#tbl2').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                bLengthChange: false,
                buttons: [
                {extend: 'excel', title: 'Equipamentos'},
                {extend: 'pdfHtml5',orientation: 'landscape',pageSize: 'LEGAL'},
                {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit'); }
                },

                ]
            });

            //---------------------------------------------------------------------------------------------------------------
            // Define as configuracoes e Visual do mapa
            //---------------------------------------------------------------------------------------------------------------
            
            var myOptions = {
                zoom: zoomLevel,                                        // Define o nivel de zoom pela variavel zoomLevel
                mapTypeId: google.maps.MapTypeId.ROADMAP,               // Tipo do mapa
                center: center,                                         // Define o centro do mapa pela coordenada armazanada na variavel center
                disableDefaultUI: true,                                 // Desativa os controles do Default do mapa
                zoomControl: true,                                      // Ativa o controle de zoom do mapa
                zoomControlOptions: {                                   // Define a posicao do controle de zoom
                    position: google.maps.ControlPosition.LEFT_CENTER  
                },
                styles: [                                               // Configuracoes css do mapa 
                            { "stylers": [{ "saturation": -100 },{ "gamma": 1 }] }, 
                            { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "off" }] }, 
                            { "featureType": "poi.business", "elementType": "labels.text", "stylers": [{ "visibility": "off" }] }, 
                            { "featureType": "poi.business", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, 
                            { "featureType": "poi.place_of_worship", "elementType": "labels.text", "stylers": [{ "visibility": "off" }] }, 
                            { "featureType": "poi.place_of_worship", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, 
                            { "featureType": "road", "elementType": "geometry", "stylers": [{ "visibility": "simplified" }] }, 
                            { "featureType": "water", "stylers": [{ "visibility": "on" }, { "saturation": 50 }, { "gamma": 0 }, { "hue": "#50a5d1" }] }, 
                            { "featureType": "administrative.neighborhood", "elementType": "labels.text.fill", "stylers": [{ "color": "#333333" }] },{ "featureType": "road.local", "elementType": "labels.text", "stylers": [{ "weight": 0.5 }, { "color": "#333333" }] }, 
                            { "featureType": "transit.station", "elementType": "labels.icon", "stylers": [{ "gamma": 1 }, { "saturation": 50 }] }]
            };


            console.log('Variaveis definidas....')

    //---------------------------------------------------------------------------------------------------------------
    // Inicializa o mapa
    //---------------------------------------------------------------------------------------------------------------

    init();

    function init(){



            div = document.getElementById("google-map");
            myMap = new google.maps.Map(div,myOptions);
            // Instancia do programa de roteiro.    
            tsp = new BpTspSolver(myMap, div);
            // Define a unidade de medida
            tsp.setDirectionUnits("m");

            //---------------------------------------------------------------------------------------------------------------
            //Gera o Selectbox com as Zonas do usuario
            //---------------------------------------------------------------------------------------------------------------
                $('.chosen-select').chosen({width: "100%"});
                // Carrega a lista de unidades de negocio do Banco de dados.
                $.getJSON('http://'+server+'/startup/app/maps/getUnit/', function (unidades) {
                    $('#selectUnidade').append('<option value="">Selecione a Unidade</option>');
                    for (var r = 0; r < unidades.length; r++) {
                        $('#selectUnidade').append('<option value="'+unidades[r].unidade+'">Unidade '+unidades[r].unidade+'</option>');
                        $(".chosen-select").trigger("chosen:updated"); // Atualiza o filtro na tela
                    }
                });
            
                // Espera o usuario alterar a selecao da unidade e
                // atualiza o filtro de Zonas.

                $('#selectUnidade').on('change',function() {
                    // Carrega a lista de ZONAS do Banco de dados.
                    // Atribui a variavel o valor da unidade de negocio selecionada.  (alter. 2/08/17)    
                    unidade = $(this).val();
                    
                    $.getJSON('http://'+server+'/startup/app/maps/getZone/'+unidade, function (zonas) {
                        // Limpa a Sele;cao de Zonas
                        $('#selectZona').html('');
                        $(".chosen-select").trigger("chosen:updated");
                        for (var r = 0; r < zonas.length; r++) {
                            $('#selectZona').append('<option value="'+zonas[r].zona+'">Zona '+zonas[r].zona+'</option>');
                            $(".chosen-select").trigger("chosen:updated"); // Atualiza o filtro na tela
                        }

                        // Carrega a lista de ZONAS do Banco de dados na lista de opcao
                        // para transferenca de equipamentos.
                        
                        $('#selectTransfZona').html('');
                        $(".chosen-select").trigger("chosen:updated");
                        $('#selectTransfZona').append('<option value="">Selecione a Zona de destino dos equipamentos</option>');
                        for (var r = 0; r < zonas.length; r++) {
                            $('#selectTransfZona').append('<option value="'+zonas[r].zona+'">Zona '+zonas[r].zona+'</option>');
                            $(".chosen-select").trigger("chosen:updated"); // Atualiza o filtro na tela
                        }

                    });
                });

                 $('#selectTransfZona').on('change',function() {
                    // Set numero da ZONA destino para transferencia de equipamentos
                    transfer_zone = $(this).val();
                    //--------------------------------------------------------------
                    $.getJSON('http://'+server+'/startup/app/maps/getSetores/'+unidade+'/'+transfer_zone, function (setores) {
                        $('#selectTransfSetor').html('');
                        $(".chosen-select").trigger("chosen:updated");
                        $('#selectTransfSetor').append('<option value="">Selecione o Setor de destino dos equipamentos</option>');
                        for (var r = 0; r < setores.length; r++) {
                            $('#selectTransfSetor').append('<option value="'+setores[r].setor+'">Setor '+setores[r].setor+'</option>');
                            $(".chosen-select").trigger("chosen:updated"); // Atualiza o filtro na tela
                        }
                    });
                 });


                  $('#selectTransfSetor').on('change',function() { 
                    // Set numero da ZONA destino para transferencia de equipamentos
                    transfer_sector =  $(this).val();
                  });
            //---------------------------------------------------------------------------------------------------------------
            //Quando o usuario seleciona uma Zona Armazenar na Variavel zone_number as Zonas selecionadas.
            //---------------------------------------------------------------------------------------------------------------

            $('#selectZona').on('change',function() {
              $('#route_list').addClass('hidden');
              zone_number = $(this).val();
              // Verifica se foi selecionada uma zona no Selectbox.
              if(zone_number!==null && zone_number.length!== 0){
                    getZone(unidade,zone_number);
              };
            });

            //---------------------------------------------------------------------------------------------------------------
            // Gera o Selectbox para processo de tranferencia de equipamentos
            // entre Zonas e Setores.
            //
            // Somente da Unidade Selecionada.
            //---------------------------------------------------------------------------------------------------------------



            //---------------------------------------------------------------------------------------------------------------
            // Define o meio de deslocamento para o processamento do Roteiro...
            //---------------------------------------------------------------------------------------------------------------

            $('#btn_car').click(function (){
                TravelMode = google.maps.DirectionsTravelMode.DRIVING;  // Por automovel
            });

            $('#btn_walk').click(function (){
                TravelMode = google.maps.DirectionsTravelMode.WALKING; // Por caminhada
            });
            
       
            //---------------------------------------------------------------------------------------------------------------
            // Evento para transferir os equipamentos entre Zonas e Setores
            //---------------------------------------------------------------------------------------------------------------
            $('#btn_exec_transfer').click(function() {
                //Verifica se existem equipamentos na lista de transferencia, se vazia exibe mensagem
                if(transfer!==null && transfer.length!== 0){
                    // Verifica se o usuario selecionou a Zona de Destino
                    if(transfer_zone == 0 ){
                        //alert("Selecione a ZONA de Destino para transferencia dos equipamentos.");
                        swal("Ops!!", "Selecione a ZONA de Destino para transferencia dos equipamentos.", "warning")
                    // Verifica se o usuario selecionou o Setor de Destino    
                    } else if(transfer_sector == 0){
                        swal("Ops!!", "Selecione o SETOR de Destino para transferencia dos equipamentos.", "warning")
                    }else{
                        // Processa a transferencia dos equipamentos
                        // 01 registro por vez,  para processo em lote alterar POST e enviar o Array completo
                        //-----------------------------------------------------------------------------------
                        for (var r = 0; r < transfer.length; r++) {
                             $.post( 'http://'+server+'/startup/app/maps/setTransfer/'+
                                transfer[r].contrato+"/"+
                                transfer[r].endereco_formatado+"/"+
                                transfer[r].zona+"/"+
                                transfer[r].setor+"/"+
                                transfer_zone+"/"+
                                transfer_sector, function( data ) {});
                        };
                        // Limpa o Array de tranferencia
                        transfer = [];
                        // Reset contador de transferencia
                        counter = 0;
                        // Limpa tabela de equipamentos pra transferencia
                        tb.clear();
                        tb.draw();
                        // Mensagem de sucesso

                        if(zone_number!==null && zone_number.length!== 0){
                            getZone(unidade,zone_number);
                        };

                        swal("Dados atualizados!", "Atualize o mapa para visualizar as alterações.", "success");


                    };
                }else{
                        swal("Ops!!", "A lista de equipamentos está vazia.", "warning")
                };
            });

    console.log('init() OK....');  // RETIRAR DO CODIGO DE PRD


    };  // End Init()


 //*****************************************************************************************************************************
 //  Funcao getZone - Exibe os pontos (enderecos) no mapa agrupados por Zona ou Setor em Cores diferentes.
 //---------------------------------------------------------------------------------------------------------------
 // Carrega os dados da Zona agrupado por ZONAS e exibe os pontos no mapa. (Cada cor = um setor !)
 //---------------------------------------------------------------------------------------------------------------

    function getZone(unidade,zone_number) {

        // Chamada do servico que gera a lista de clientes da zona, passando como parametro o  numero da Zona.
       /// $.getJSON('http://'+server+'/startup/app/maps/territorio/' + zone_number, function (pontos) {
        $.getJSON('http://'+server+'/startup/app/maps/getTerritorio/'+unidade+'/' + zone_number, function (pontos) {


            var zone = [];
            var id = 0;
            var qty_equip = 0;
            var tempo_previsto_de_mp =0;

            if(zone_number.length>=2){
                var RouteIndex = pontos[0].zona;
                var hidden = 'hidden';
                for (var r = 0; r < pontos.length; r++) {
                    if (pontos[r].zona == RouteIndex) {
                        qty_equip = qty_equip+ Number(pontos[r].qtd_equipamentos);
                        tempo_previsto_de_mp = tempo_previsto_de_mp+Number(pontos[r].tempo_previsto_de_mp);
                    };
                };
                $('#TravelModeMenu').addClass('hidden');
                $('.btn-calc').addClass('hidden');
            }else{
                var RouteIndex = pontos[0].setor;
                var hidden = '';
                for (var r = 0; r < pontos.length; r++) {
                    if (pontos[r].setor == RouteIndex) {
                        qty_equip = qty_equip+ Number(pontos[r].qtd_equipamentos);
                        tempo_previsto_de_mp = tempo_previsto_de_mp+Number(pontos[r].tempo_previsto_de_mp);

                    };
                };
                $('#TravelModeMenu').removeClass('hidden');
                $('.btn-calc').removeClass('hidden');
            };

            // Exibe o setor e quantidade de equipamentos no indice na tela
            $('#zone_name').html("Zona "+ zone_number);
            $('#idx_name').html("Setores");
            $('.lr').removeClass('col-md-6').addClass('col-md-2');

            // Soma a quantidade de eqipamentos


            // Monta o indice na tela
            html    ='<tr>'
                        +'<td class="text-center"><img src="img/markers/'+RouteIndex+'/number_'+RouteIndex+'.png"></td>'
                        +'<td class="text-center"><strong>'+qty_equip+'</strong></td>'
                        +'<td class="text-center"><strong>'+secondsToTime(tempo_previsto_de_mp*60)+'</strong></td>'
                        +'<td><a href="#"  class="btn btn-default btn-sm btn-calc '+hidden+'" data-setor='+RouteIndex+' ><i class="fa fa-cog"></i></a></td>'
                    +'</tr>';



            $('#list_t').html(html);
            $('#zone_list').removeClass('hidden');

            
            $( ".btn-calc").click(function() {
              calcRoute(unidade,zone_number,$(this).data("setor"));
            });
 

            // Limpa o Mapa
            for (var i = 0; i < markers.length; ++i) {
                markers[i].setMap(null);
            }

            //Limpa o Array para alocar os pontos
            markers = [];

            latlngbounds = new google.maps.LatLngBounds();
            var infoWindow = new google.maps.InfoWindow();
            //Limpa o Array para alocar os pontos

            // Monta um Array com os pontos
            for (var r = 0; r < pontos.length; r++) {
                
               qty_equip = 0;
               tempo_previsto_de_mp = 0;

                if(zone_number.length>=2){
                    pt = pontos[r].zona;
                    sqc = pontos[r].zona;
                }else{
                    pt = pontos[r].setor;
                    sqc = pontos[r].sequencia;
                };


                if (RouteIndex != pt) {
                    RouteIndex = pt;
                    for (var r1 = 0; r1 < pontos.length; r1++) {
                        if(zone_number.length>=2){
                            if (pontos[r1].zona == RouteIndex) {
                                qty_equip = qty_equip+ Number(pontos[r1].qtd_equipamentos);
                                tempo_previsto_de_mp = tempo_previsto_de_mp+Number(pontos[r1].tempo_previsto_de_mp);
                            };
                        }else{
                            if (pontos[r1].setor == RouteIndex) {
                                qty_equip = qty_equip+ Number(pontos[r1].qtd_equipamentos);
                                tempo_previsto_de_mp = tempo_previsto_de_mp+Number(pontos[r1].tempo_previsto_de_mp);
                            };
                        };
                    };
                   
                    html   = '<tr>'
                            +'<td class="text-center"><img src="img/markers/'+pt+'/number_'+pt+'.png"></i></td>'
                            +'<td class="text-center"><strong>'+qty_equip+'</strong></td>'
                             +'<td class="text-center"><strong>'+secondsToTime(tempo_previsto_de_mp*60)+'</strong></td>'
                            +'<td><a href="#" id='+pt+' class="btn btn-default btn-sm btn-calc1 '+hidden+'" data-setor='+pt+' ><i class="fa fa-cog "></i></a></td>'
                            +'</tr>';
                    $('#list_t').append(html);

                }


                var icon = new google.maps.MarkerImage("img/markers/"+pt+"/number_"+sqc+".png");
                var position = new google.maps.LatLng(pontos[r].lat, pontos[r].lng);

                var data = pontos[r];

                marker = new google.maps.Marker({
                    id: pontos[r].endereco_formatado,
                    icon:icon,
                    position: position,
                    map: myMap,
                    title: pontos[r].endereco_formatado
                });
                markers.push(marker);
                latlngbounds.extend(marker.position);
                // Automatically center the map fitting all markers on the screen

                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        $.getJSON('http://'+server+'/startup/app/maps/GetCustomerData/' + data.endereco_formatado, function (customer) {
                            var content = '';
                             
                             for (var r = 0; r < customer.length; r++) {
                                var restricao_dia_util ='';
                                if(customer[r].restricao_dia=='S'){
                                    restricao_dia_util = '<p><span class="label label-danger"> Manutenção Preventiva programada aos Sábados </span></p>'
                                };
                                // Monta o Infowindow com os dados de Contrato por Endereco.
                                content = content + '<div>'+restricao_dia_util+
                                                        '<h4><strong>'+customer[r].contrato+' - '+customer[r].nome_obra+'</strong></h4>'+
                                                        '<h4>'+ customer[r].endereco_formatado +'</h4>'+
                                                        '<p>Zona: <strong>' + customer[r].zona + '</strong>'+
                                                            '&emsp;Setor: <strong>' + customer[r].setor + '</strong>'+
                                                            '&emsp;Equipamentos: <strong>' + customer[r].equipos + '</strong>'+
                                                            '&emsp;Tempo prev. de MP: <strong>' + secondsToTime(customer[r].tmp_prev_mp*60) + ' hr(s)</strong>'+
                                                        '</p>'+
                                                        '<p>Técnico: <strong>' + customer[r].nome_tecnico + '</strong></p>'+
                                                        '<p>Tipo de contrato: <strong>' + customer[r].cesc_contrato + '</strong></p>'+
                                                        '<button type="button" data-id='+r+' class="btn btn-sm btn-success btn-setMp"><i class="fa fa fa-wrench"></i> Add./Rem. restricao de MP </button>'+
                                                        '<button type="button" data-id='+r+' class="btn btn-sm btn-warning btn-setAddr m-l-sm"><i class="fa fa fa-road"></i> Endereço incorreto</button>'+
                                                    '</div>'+
                                                    '<hr>'
                             }

                            infoWindow.setContent(content);
                            infoWindow.open(myMap, marker);
                            myMap.panTo(marker.position);

                            $('.btn-setMp').click(function() {
                                $.post( "http://"+server+"/startup/app/maps/setMpSabado/"+customer[$(this).data('id')].endereco_formatado+"/"+customer[$(this).data('id')].contrato, function( data ) {
                                    swal("Successo!", data, "success")
                                });                
                            });


                            $('.btn-setAddr').click(function() {
                                $.post( "http://"+server+"/startup/app/maps/SetAddr/"+customer[$(this).data('id')].endereco_formatado+"/"+customer[$(this).data('id')].contrato, function( data ) {
                                    swal("Successo!", data, "success")
                                });
                            });

                        });
                    });


                    google.maps.event.addListener(marker, "rightclick", function (e) {  

                          // Chamada do servico que devolve os dados de clientes, passando como parametro o numero do contrato.
                          $.getJSON('http://'+server+'/startup/app/maps/GetCustomerData/' + data.endereco_formatado, function (customer) {


                                 // Monta o InfoWindo com os dados do(s) cliente(s), Quando houver mais de um cliente/contrato cadastrados
                                 // no mesmo endereco serao exibidos em sequencia na caixa de informacao.

                                
                                 for (var r = 0; r < customer.length; r++) {

                                     // Desabilita o botao transferir para evidar duplicidade de registros
                                            // Adiciona os registros na Tabela de tranferencia.
                                            tb.row.add( [
                                                customer[r].contrato,
                                                //customer[$(this).data('id')].nome_obra,
                                                customer[r].endereco_formatado,
                                                customer[r].equipos,
                                                customer[r].zona,
                                                customer[r].setor,
                                                ] ).draw( false );
                                            // Armazena os regsitros em uma variavel para o processamento no banco de dados
                                            transfer[counter] = {
                                                "contrato":             customer[r].contrato,
                                                "nome_obra":            customer[r].nome_obra,
                                                "endereco_formatado":   customer[r].endereco_formatado,
                                                "zona":                 customer[r].zona,
                                                "setor":                customer[r].setor     
                                            }

                                }

                                myMap.panTo(marker.position);
                                marker.setAnimation(google.maps.Animation.BOUNCE);



                            }); // end $.getJSON
                      });

                })(marker, data);

            } // End for


            myMap.fitBounds(latlngbounds);

            $( ".btn-calc1").click(function() {
                calcRoute(unidade,zone_number,$(this).data("setor"));
            });


        }); // end get

        console.log('ZONA CARREGADA....');  // RETIRAR DO CODIGO DE PRD

    }

    // Funcao para remover os marcadores da tela.
    function removeMarkers(){
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
        latlngbounds = '';
    }


    //---------------------------------------------------------------------------------------------------------------
    // Funcao para calcular o melhor roteiro para a lista de locais
    //---------------------------------------------------------------------------------------------------------------


    function calcRoute(unidade,zone_number,setor){

        $('.progress').removeClass('hidden');
        $('#zone_list').addClass('hidden');
        $('.lr').removeClass('col-md-2').addClass('col-md-6');

        $.getJSON('http://'+server+'/startup/app/maps/getRota/' + unidade +'/'+ zone_number +'/'+ setor, function (pontos) {
            removeOldMarkers();
            for (var r = 0; r < pontos.length; r++) {
              customerArr[r] = {
                "endereco":     pontos[r].endereco_formatado,
                "sequencia":    pontos[r].sequencia
            };
            var lat = parseFloat(pontos[r].lat);
            var lng = parseFloat(pontos[r].lng);
            var latLng = new google.maps.LatLng(lat, lng);
            tsp.addWaypointWithLabel(latLng,pontos[r].endereco_formatado,addWaypointSuccessCallbackZoom)
                //tsp.addWaypoint(latLng, addWaypointSuccessCallbackZoom);
                //tsp.addAddress(pontos[r].endereco,addAddressSuccessCallbackZoom);
            }

            tsp.setAvoidHighways(false);
            tsp.setAvoidTolls(false);
            tsp.setTravelMode(TravelMode);
            tsp.setOnProgressCallback(onProgressCallback);
            tsp.solveAtoZ(onSolveCallback);

            function addWaypointSuccessCallbackZoom(latlng) {
                if (latlng) {
                    //console.log("addWaypointSuccessCallbackZoom = "+latlng);
                    drawMarkers(true);
                }
            }

            // ##############################################################################################
            // ## Debug  LIMPAR DO CODIGO PARA PRODUCAO !!!!!
            // ##############################################################################################
            
            //    function addAddressSuccessCallbackZoom(address, latlng) {
            //        if (latlng) {
            //          $.post( "http://localhost/startup/app/maps/updateAddr/"+address+"/"+latlng, function( data ) {
            //          });
            //          drawMarkers(true);
            //        } else {
            //          alert('Não foi possivel identificar o endereço no mapa, verifique e tente novamente: ' + address);
            //       }
            //    }

            function drawMarkers(updateViewport) {
                
                removeOldMarkers();
                var waypoints   = tsp.getWaypoints();
                var addresses   = tsp.getAddresses();
                var labels      = tsp.getLabels();

                // ##############################################################################################
                // ## Debug  LIMPAR DO CODIGO PARA PRODUCAO !!!!!
                // ##############################################################################################
                //console.log("updateViewport: "+updateViewport);
                // -----------------------------------------------------------------
                //console.log("waypoints: "+waypoints);
                //console.log("----------------------------------------------------");
                //console.log("addresses: "+addresses);
                //console.log("----------------------------------------------------");
                //console.log("labels: "+labels);


                for (var i = 0; i < waypoints.length; ++i) {
                    drawMarker(waypoints[i], addresses[i], labels[i], i);
                }
                if (updateViewport) {
                    setViewportToCover(waypoints);
                }
            }

            function drawMarker(latlng, addr, label, num) {
                
                icon = new google.maps.MarkerImage("img/markers/marker.png");
    
                var marker = new google.maps.Marker({
                    id:label,
                    position: latlng,
                    icon: icon,
                    map: myMap
                    });
                    markers.push(marker);
            }


            function setViewportToCover(waypoints) {
                var bounds = new google.maps.LatLngBounds();
            
                for (var i = 0; i < waypoints.length; ++i) {
                    bounds.extend(waypoints[i]);
                }
                myMap.fitBounds(bounds);
                google.maps.event.addListenerOnce(myMap, 'bounds_changed', function(event) {
                    if (this.getZoom() > 15) {
                        this.setZoom(17);
                    }
                });
            }

            function removeOldMarkers() {
                for (var i = 0; i < markers.length; ++i) {
                    markers[i].setMap(null);
                }
                markers = new Array();
            }

            // Calculo da Barra de progresso do processamento da Rota
            function onProgressCallback(tsp) {
                $('#p_bar').width(100 * tsp.getNumDirectionsComputed() / tsp.getNumDirectionsNeeded() + '%').html('Calculando a Rota.');;
            }

            // Calculo da distancia total da Rota
            function getTotalDistance(dir) {
                var sum = 0;
                for (var i = 0; i < dir.legs.length; i++) {
                    sum += dir.legs[i].distance.value;
                }
                return sum;
            }

            // Formata o valor das distancias...
            function formatLength(meters) {
                var km = parseInt(meters / 1000);
                meters -= km * 1000;
                var ret = "";
                if (km > 0) 
                    ret += km + " km ";
                if (km < 10)
                    ret += meters + " m";
                return(ret);
            }

            //---------------------------------------------------------------------
            // Apresenta no mapa o Roteiro processado
            //---------------------------------------------------------------------

            function onSolveCallback(myTsp) {

               

                //Oculta a barra de progresso do processamento.
                $('.progress').addClass('hidden');
                //Oculta  Select de Zonas
                $('#select1').addClass('hidden');
                
                var dirRes  = tsp.getGDirections();
                var dir     = dirRes.routes[0];
                var order   = tsp.getOrder();
                var setRoute = new Array();

                //Limpa o mapa
                removeOldMarkers();
                var myPt1 = dir.legs[0].start_location;
                var myIcn1 = new google.maps.MarkerImage("img/markers/marker.png"); 
                // Cria o marcador do ponto de partida. 
                var marker = new google.maps.Marker({
                    position: myPt1,
                    icon: myIcn1,
                    map: myMap,
                    label: {
                        text: '1'
                    }
                });
                markers.push(marker);


                attachInfoWindow(marker, 1, order[0]);
                


                //Oculta a lista de setores da Zona
                $('#zone_list').addClass('hidden');
                //Exibe a lista de clientes do Setor
                $('#route_list').removeClass('hidden');
                //Prepara a lista de clientes
                html = '<tr>'
                        +'<td class="text-center"><span class="label label-success">1</span></td>'
                        +'<td>'+customerArr[0].endereco+'</td>'
                        +'<td> -- Mins. </td>'
                        +'</tr>';
                //Adiciona o 1# cliente na lista
                $('#list').html(html);

                //$.post( "http://"+server+"/startup/app/maps/setRoute/"+customerArr[0].endereco+"/1/0", function( data ) {});

                setRoute[0] = {
                    "c":   customerArr[0].endereco,
                    "s":   1,
                    "t":   0
                };


                for (var i = 0; i < dir.legs.length; ++i) {
                    var route = dir.legs[i];
                    var myPt1 = route.end_location;
                    var myIcn1;
                    var seq = i+2;
                    myIcn1 = new google.maps.MarkerImage("img/markers/marker.png");
                    
                    var marker = new google.maps.Marker({
                        position: myPt1,
                        icon: myIcn1,
                        map: myMap,
                        label: {
                            text: seq.toString()
                        }
                    });
                    markers.push(marker);
                    attachInfoWindow(marker, seq, order[i+1]);

                    html = '<tr>'
                    +'<td class="text-center"><span class="label label-success">'+seq+'</span></td>'
                    +'<td>'+customerArr[order[i+1]].endereco+'</td>'
                    +'<td>'+secondsToTime(dir.legs[i].duration.value)+' hs. </td>'
                    +'</tr>';
                    $(html).appendTo('#list');


                    setRoute[order[i+1]] = {
                        "c":   customerArr[order[i+1]].endereco,
                        "s":   seq,
                        "t":   Math.ceil(dir.legs[i].duration.value / 60 )
                    };



                    //$.post( "http://"+server+"/startup/app/maps/setRoute/"+customerArr[order[i+1]].endereco+"/"+seq+"/"+Math.ceil(dir.legs[i].duration.value / 60 ), function( data ) {});

                } // end fo

                    //envia rota processada para o banco de dados
                    var jsonString = JSON.stringify(setRoute);
                       $.ajax({
                            type: "POST",
                            url: "http://"+server+"/startup/app/maps/setRoutex",
                            data: {data : jsonString}, 
                            cache: false,

                            success: function(){
                                alert("dados enviados");
                            }
                    });


                console.log(setRoute);
                // Clean up old path.
                if (dirRenderer != null) {
                    dirRenderer.setMap(null);
                }

                
                /*-------------------------------------------------------------------------------
                // TESTE !!!  NAO IMPLEMENTADO NO BANCO DE DADOS                                !   
                //-------------------------------------------------------------------------------
                // Grava o resultado jSon do processamento do Google MAPS API no banco de dados;
                // A rota podera' ser visualizada sem a necessidade de consultar a API novamente
                // reduzindo o tempo de processamento e consumo de dados.
                // ------------------------------------------------------------------------------

                                    var d = JSON.stringify(dirRes);
                                    $.ajax({
                                        type: 'POST',
                                        url: 'http://'+server+'/startup/app/maps/setDirRes/',
                                        data: {unidade:unidade,zone:zone_number[0],sector:setor,data:d},
                                        dataType: 'json'
                                    });


                //-------------------------------------------------------------------------------*/

                dirRenderer = new google.maps.DirectionsRenderer({
                    polylineOptions: { strokeColor: color, strokeWeight: 8,strokeOpacity:0.4},
                    directions: dirRes,
                    hideRouteList: true,
                    map: myMap,
                    panel: null,
                    preserveViewport: false,
                    suppressInfoWindows: true,
                    suppressMarkers: true
                });

                $('#distance').html(formatLength(getTotalDistance(dir)));


            } // End onSolveCallback function


            function attachInfoWindow(marker, legIndex, labels) {
                //$.getJSON("http://"+server+"/startup/app/maps/GetCustomerData/" + customerArr[labels].endereco, function (customer) {
            

                    var infowindow = new google.maps.InfoWindow({
                        content: '<div>'+
                        '</br><p><button class="btn btn-sm btn-success init_route" type="button" data-id="'+customerArr[labels].endereco+'" ><i class="fa fa-sort-numeric-asc"></i>&nbsp;&nbsp;<span class="bold">Definir como inicio da Rota</span></button></p>'+
                        '</div>'
                    }); // google.maps.InfoWindow


                    google.maps.event.addListener(marker, 'click', function () {
                        infowindow.close();
                        infowindow.open(myMap, marker);

                        $('.init_route').click(function() {
                            $.post( "http://"+server+"/startup/app/maps/setInitRoute/"+zone_number+"/"+setor+"/"+$(this).data('id')+"/1", function( data ) {
                             tsp.startOver();
                             calcRoute(unidade,zone_number,setor);
                            });
                        });
                    }); // end google.maps.event.addListener
                //}); // end $.getJSON
            } // End attachInfoWindow

        }); // End getJson
    } // End calc Route


            // Formata o tempo de Segundos para o formato horas  (hh:mm:ss)
            function secondsToTime(secs) {
                secs = Math.round(secs);
                var hours = Math.floor(secs / (60 * 60));
                var divisor_for_minutes = secs % (60 * 60);
                var minutes = Math.floor(divisor_for_minutes / 60);
                var divisor_for_seconds = divisor_for_minutes % 60;
                var seconds = Math.ceil(divisor_for_seconds);

                // Fabiano -- 19/01/2009 --  Adiciona o "0" antes do n[uemro quando o valor for menor que 10.
                if (hours   < 10) {hours   = "0"+hours;}
                if (minutes < 10) {minutes = "0"+minutes;}
                if (seconds < 10) {seconds = "0"+seconds;}
                //------

                var t = hours + ":" + minutes + ":" + seconds;
                return t;
            }


})(window.jQuery); // End main function

//---------------------------------------------------------------------
// Funcao para exibir o conteudo de um elemento HTML em Tela cheia
//---------------------------------------------------------------------
function toggleFullScreen(elem) {
      if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
        if (elem.requestFullScreen) {
          elem.requestFullScreen();
      } else if (elem.mozRequestFullScreen) {
          elem.mozRequestFullScreen();
      } else if (elem.webkitRequestFullScreen) {
          elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
      } else if (elem.msRequestFullscreen) {
          elem.msRequestFullscreen();
      }
    } else {
        if (document.cancelFullScreen) {
          document.cancelFullScreen();
      } else if (document.mozCancelFullScreen) {
          document.mozCancelFullScreen();
      } else if (document.webkitCancelFullScreen) {
          document.webkitCancelFullScreen();
      } else if (document.msExitFullscreen) {
          document.msExitFullscreen();
      }
    }
}
//---------------------------------------------------------------------



