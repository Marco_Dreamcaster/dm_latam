<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['Audit/Vendors/(:any)'] = 'IDL/Serializers/generateAuditVendorsReports/$1';
$route['Audit/Customers/(:any)'] = 'IDL/Serializers/generateAuditCustomersReports/$1';
$route['Reports/Customers/(:any)/(:any)'] = 'IDL/Serializers/generateCustomersReports/$1/$2';

$route['NamedQueries/(:any)'] = 'IDL/Serializers/generateNamedQuery/$1';
$route['NamedQueries/(:any)/(:any)'] = 'IDL/Serializers/generateNamedQuery/$1/$2';
$route['NamedQueries/(:any)/(:any)/(:any)'] = 'IDL/Serializers/generateNamedQuery/$1/$2/$3';

// FFGenerator

// refreshes all FFs declared in bundle
// RefreshFF/Projects/CO

$route['RefreshFF/(:any)/(:any)'] = 'FFController/refresh/$1/$2';

// refreshes only TKE_LA_CD_02_PROJECT_HEADER FF
// RefreshFF/Projects/CO/TKE_LA_CD_02_PROJECT_HEADER

$route['RefreshFF/(:any)/(:any)/(:any)'] = 'FFController/refresh/$1/$2/$3';

// displays latest TKE_LA_CD_02_PROJECT_HEADER on screen
// InspectFF/Projects/CO/TKE_LA_CD_02_PROJECT_HEADER

$route['InspectFF/(:any)/(:any)/(:any)'] = 'FFController/inspect/$1/$2/$3';

// downloads all latest FFs declared in bundle as zip file
// DownloadFF/Projects/CO

$route['DownloadFF/(:any)/(:any)'] = 'FFController/download/$1/$2';

// displays FF.xml with customized forms for default placeholders
// ManageFF (displays all bundles)
// ManageFF/Projects (displays all definitions for bundle Project)
// ManageFF/Projects/CO (displays all definitions for bundle Project, customized for CO)
$route['ManageFF'] = 'FFController/manage';
$route['ManageFF/(:any)'] = 'FFController/manage/$1';
$route['ManageFF/(:any)/(:any)'] = 'FFController/manage/$1/$2';




$route['RunSetup/(:any)'] = 'IDL/Serializers/runSetup/$1';
$route['RunSQLCMD/(:any)'] = 'IDL/Serializers/runSQLCMD/$1';
$route['RunSQLCMDVARIABLE/(:any)'] = 'IDL/Serializers/runSQLCMDVARIABLE/$1';
$route['RunSQLCMD/(:any)/(:any)'] = 'IDL/Serializers/runSingleSQLCMD/$1/$2';
$route['InspectSQLCMD/(:any)'] = 'IDL/Serializers/inspectSQLCMD/$1';
$route['Bundled/(:any)/(:any)'] = 'IDL/Serializers/generateBundledNamedQuery/$1/$2';
$route['Inspect/(:any)/(:any)'] = 'IDL/Serializers/generateInspectByTitleFromForm/$1/$2';
$route['Inspect/(:any)/(:any)/(:any)'] = 'IDL/Serializers/generateInspectByTitleAndCountryFromForm/$1/$2/$3';

$route['Experimental/auditCSVOutput'] = 'Experimental/Main/auditCSVOutput';
$route['Experimental/createTestExcelSpreadsheet'] = 'MY_Controller/createTestExcelSpreadsheet';
$route['Experimental/createTestExcelReport'] = 'MY_Controller/createTestExcelReport';

$route['Experimental/triggerSQLCMD/(:any)/(:any)/(:any)'] = 'IDL/Serializers/triggerSQLCMD/$1/$2/$3';
$route['Experimental/triggerBulkValidation/(:any)/(:any)'] = 'Admin/Main/triggerBulkValidation/$1/$2';
$route['Experimental/triggerBulkValidation/(:any)/(:any)/0'] = 'Admin/Main/triggerBulkValidation/$1/$2/0';


$route['Debug/display/doc/(:any)'] = 'Debug/displayDoc/$1';
$route['Debug/display/doc'] = 'Debug/displayDoc';
$route['Debug/explode/(:any)/(:any)/(:any)'] = 'Debug/explodeForCountryGroupIDL/$1/$2/$3';
$route['Debug/(:any)/(:any)/(:any)'] = 'Debug/debugForCountryGroupIDL/$1/$2/$3';
$route['Debug'] = 'Debug';

$route['Search'] = 'Search';

$route['TBD'] = 'pages/view/TBD';

$route['Gallery/download'] = 'IDL/Serializers/generateGalleryZipFile';


$route['Maps/ORAGeoCodes/list'] = 'MAPS/ORAGeoCodes/index';
$route['Maps/ORAGeoCodes/list/(:any)'] = 'MAPS/ORAGeoCodes/index/$1';
$route['Maps/ORAGeoCodes/TestJSON'] = 'MAPS/ORAGeoCodes/testJSON';
$route['Maps/TKEFamilyLines/testJSON'] = 'Maps/TKEFamilyLines/testJSON';
$route['Maps/TKEFamilyLines/getSubFamilies'] = 'Maps/TKEFamilyLines/getSubFamilies';
$route['Maps/TKEFamilyLines/getLines'] = 'Maps/TKEFamilyLines/getLines';

$route['Maps/ORAGeoCodes/create'] = 'MAPS/ORAGeoCodes/create';
$route['Maps/ORAGeoCodes/new'] = 'MAPS/ORAGeoCodes/addNew';
$route['Maps/ORAGeoCodes/remove/(:any)'] = 'MAPS/ORAGeoCodes/remove/$1';
$route['Maps/ORAGeoCodes/getStates'] = 'Maps/ORAGeoCodes/getStates';
$route['Maps/ORAGeoCodes/getProvinces'] = 'Maps/ORAGeoCodes/getProvinces';
$route['Maps/ORAGeoCodes/getCities'] = 'Maps/ORAGeoCodes/getCities';

$route['Projects/Maps/removePFF/(:any)/(:any)/(:any)'] = 'MAPS/PFF/removePFF/$1/$2/$3';
$route['Projects/Maps/createPFF'] = 'MAPS/PFF/createPFF';
$route['Projects/Maps/newPFF'] = 'MAPS/PFF/newPFF';
$route['Projects/Maps/PFF'] = 'MAPS/PFF/index';

$route['ProjectTasks/discard/(:any)'] = 'ProjectTasks/Main/discard/$1';
$route['ProjectTasks/validate/(:any)'] = 'ProjectTasks/Main/validate/$1';
$route['ProjectTasks/customUpdateCleansed/(:any)/(:any)'] = 'ProjectTasks/Main/customUpdateCleansed/$1/$2';
$route['ProjectTasks/customEditCleansed/(:any)/(:any)'] = 'ProjectTasks/Main/customEditCleansed/$1/$2';
$route['ProjectTasks/updateCleansed/(:any)'] = 'ProjectTasks/Main/updateCleansed/$1';
$route['ProjectTasks/editCleansed/(:any)'] = 'ProjectTasks/Main/editCleansed/$1';
$route['ProjectTasks/createNew/(:any)'] = 'ProjectTasks/Main/createNew/$1';
$route['ProjectTasks/addNew/(:any)'] = 'ProjectTasks/Main/addNew/$1';


$route['Repairs/validate/(:any)'] = 'Repairs/Main/validate/$1';
$route['Repairs/toggle/(:any)'] = 'Repairs/Main/toggle/$1';
$route['Repairs/customUpdateCleansed/(:any)/(:any)'] = 'Repairs/Main/customUpdateCleansed/$1/$2';
$route['Repairs/customEditCleansed/(:any)/(:any)'] = 'Repairs/Main/customEditCleansed/$1/$2';
$route['Repairs/discardCleansed/(:any)'] = 'Repairs/Main/discardCleansed/$1';
$route['Repairs/updateCleansed/(:any)'] = 'Repairs/Main/updateCleansed/$1';
$route['Repairs/editCleansed/(:any)'] = 'Repairs/Main/editCleansed/$1';
$route['Repairs/cleanseDirty/(:any)'] = 'Repairs/Main/cleanseDirty/$1';
$route['Repairs/editDirty/(:any)'] = 'Repairs/Main/editDirty/$1';
$route['Repairs/dirty/(:any)/(:any)'] = 'Repairs/Main/listDirty/$1/$2';
$route['Repairs/dirty/(:any)'] = 'Repairs/Main/listDirty/$1/0';
$route['Repairs/cleansed/(:any)/(:any)/(:any)/(:any)'] = 'Repairs/Main/listCleansed/$1/$2/$3/$4';
$route['Repairs/cleansed/(:any)'] = 'Repairs/Main/listCleansed/$1/0';
$route['Repairs/(:any)'] = 'Repairs/Main/listCleansed/$1/0';

$route['Repairs'] = 'Repairs/Main/listCleansed/CO/0';


$route['Services/validate/(:any)'] = 'Services/Main/validate/$1';
$route['Services/toggle/(:any)'] = 'Services/Main/toggle/$1';
$route['Services/customUpdateCleansed/(:any)/(:any)'] = 'Services/Main/customUpdateCleansed/$1/$2';
$route['Services/customEditCleansed/(:any)/(:any)'] = 'Services/Main/customEditCleansed/$1/$2';
$route['Services/discardCleansed/(:any)'] = 'Services/Main/discardCleansed/$1';
$route['Services/updateCleansed/(:any)'] = 'Services/Main/updateCleansed/$1';
$route['Services/editCleansed/(:any)'] = 'Services/Main/editCleansed/$1';
$route['Services/cleanseDirty/(:any)'] = 'Services/Main/cleanseDirty/$1';
$route['Services/editDirty/(:any)'] = 'Services/Main/editDirty/$1';
$route['Services/dirty/(:any)/(:any)'] = 'Services/Main/listDirty/$1/$2';
$route['Services/dirty/(:any)'] = 'Services/Main/listDirty/$1/0';
$route['Services/cleansed/(:any)/(:any)/(:any)/(:any)'] = 'Services/Main/listCleansed/$1/$2/$3/$4';
$route['Services/cleansed/(:any)'] = 'Services/Main/listCleansed/$1/0';
$route['Services/(:any)'] = 'Services/Main/listCleansed/$1/0';

$route['Services'] = 'Services/Main/listCleansed/CO/0';

$route['Projects/validate/(:any)'] = 'Projects/Main/validate/$1';
$route['Projects/toggle/(:any)'] = 'Projects/Main/toggle/$1';
$route['Projects/customUpdateCleansed/(:any)/(:any)'] = 'Projects/Main/customUpdateCleansed/$1/$2';
$route['Projects/customEditCleansed/(:any)/(:any)'] = 'Projects/Main/customEditCleansed/$1/$2';
$route['Projects/discardCleansed/(:any)'] = 'Projects/Main/discardCleansed/$1';
$route['Projects/updateCleansed/(:any)'] = 'Projects/Main/updateCleansed/$1';
$route['Projects/editCleansed/(:any)'] = 'Projects/Main/editCleansed/$1';
$route['Projects/cleanseDirty/(:any)'] = 'Projects/Main/cleanseDirty/$1';
$route['Projects/editDirty/(:any)'] = 'Projects/Main/editDirty/$1';
$route['Projects/dirty/(:any)/(:any)'] = 'Projects/Main/listDirty/$1/$2';
$route['Projects/dirty/(:any)'] = 'Projects/Main/listDirty/$1/0';
$route['Projects/cleansed/(:any)/(:any)/(:any)/(:any)'] = 'Projects/Main/listCleansed/$1/$2/$3/$4';
$route['Projects/cleansed/(:any)'] = 'Projects/Main/listCleansed/$1/0';
$route['Projects/(:any)'] = 'Projects/Main/listCleansed/$1/0';

$route['Projects'] = 'Projects/Main/listCleansed/CO/0';

$route['CustomerContact/createNew/(:any)'] = 'CustomerContact/Main/createNew/$1';
$route['CustomerContact/addNew/(:any)'] = 'CustomerContact/Main/addNew/$1';
$route['CustomerContact/toggle/(:any)'] = 'CustomerContact/Main/toggle/$1';

$route['CustomerContact/validate/(:any)'] = 'CustomerContact/Main/validate/$1';
$route['CustomerContact/customUpdateCleansed/(:any)/(:any)'] = 'CustomerContact/Main/customUpdateCleansed/$1/$2';
$route['CustomerContact/customEditCleansed/(:any)/(:any)'] = 'CustomerContact/Main/customEditCleansed/$1/$2';
$route['CustomerContact/updateCleansed/(:any)'] = 'CustomerContact/Main/updateCleansed/$1';
$route['CustomerContact/editCleansed/(:any)'] = 'CustomerContact/Main/editCleansed/$1';

$route['CA/addNew/(:any)'] = 'CustomerAddress/Main/addNew/$1';
$route['CA/createNew/(:any)'] = 'CustomerAddress/Main/createNew/$1';
$route['CA/toggle/(:any)'] = 'CustomerAddress/Main/toggle/$1';
$route['CA/validate/(:any)'] = 'CustomerAddress/Main/validate/$1';

$route['CA/customUpdateCleansed/(:any)/(:any)'] = 'CustomerAddress/Main/customUpdateCleansed/$1/$2';
$route['CA/customEditCleansed/(:any)/(:any)'] = 'CustomerAddress/Main/customEditCleansed/$1/$2';
$route['CA/updateCleansed/(:any)'] = 'CustomerAddress/Main/updateCleansed/$1';
$route['CA/editCleansed/(:any)'] = 'CustomerAddress/Main/editCleansed/$1';



$route['Customers/createNew/(:any)'] = 'pages/view/TBD';
$route['Customers/toggle/(:any)'] = 'Customers/Main/toggle/$1';
$route['Customers/validate/(:any)'] = 'Customers/Main/validate/$1';

$route['Customers/discard/(:any)'] = 'Customers/Main/discard/$1';
$route['Customers/cleanseDirty/(:any)'] = 'Customers/Main/cleanseDirty/$1';
$route['Customers/editDirty/(:any)'] = 'Customers/Main/editDirty/$1';
$route['Customers/customUpdateCleansed/(:any)/(:any)'] = 'Customers/Main/customUpdateCleansed/$1/$2';
$route['Customers/customEditCleansed/(:any)/(:any)'] = 'Customers/Main/customEditCleansed/$1/$2';
$route['Customers/updateCleansed/(:any)'] = 'Customers/Main/updateCleansed/$1';
$route['Customers/editCleansed/(:any)'] = 'Customers/Main/editCleansed/$1';
$route['Customers/cleansed/(:any)/(:any)/(:any)/(:any)'] = 'Customers/Main/listCleansed/$1/$2/$3/$4';
$route['Customers/dirty/(:any)/(:any)'] = 'Customers/Main/listDirty/$1/$2';
$route['Customers/dirty/(:any)'] = 'Customers/Main/listDirty/$1/0';

$route['Customers/cleansed/(:any)'] = 'Customers/Main/listCleansed/$1/0';
$route['Customers/(:any)'] = 'Customers/Main/listCleansed/$1/0';

$route['VendorBankAccount/createNew/(:any)'] = 'VendorBankAccount/Main/createNew/$1';
$route['VendorBankAccount/addNew/(:any)'] = 'VendorBankAccount/Main/addNew/$1';
$route['VendorBankAccount/toggle/(:any)'] = 'VendorBankAccount/Main/toggle/$1';
$route['VendorBankAccount/validate/(:any)'] = 'VendorBankAccount/Main/validate/$1';

$route['VendorBankAccount/customUpdateCleansed/(:any)/(:any)'] = 'VendorBankAccount/Main/customUpdateCleansed/$1/$2';
$route['VendorBankAccount/customEditCleansed/(:any)/(:any)'] = 'VendorBankAccount/Main/customEditCleansed/$1/$2';
$route['VendorBankAccount/updateCleansed/(:any)'] = 'VendorBankAccount/Main/updateCleansed/$1';
$route['VendorBankAccount/editCleansed/(:any)'] = 'VendorBankAccount/Main/editCleansed/$1';

$route['VendorSiteContact/createNew/(:any)'] = 'VendorSiteContact/Main/createNew/$1';
$route['VendorSiteContact/addNew/(:any)'] = 'VendorSiteContact/Main/addNew/$1';
$route['VendorSiteContact/toggle/(:any)'] = 'VendorSiteContact/Main/toggle/$1';
$route['VendorSiteContact/validate/(:any)'] = 'VendorSiteContact/Main/validate/$1';

$route['VendorSiteContact/customUpdateCleansed/(:any)/(:any)'] = 'VendorSiteContact/Main/customUpdateCleansed/$1/$2';
$route['VendorSiteContact/customEditCleansed/(:any)/(:any)'] = 'VendorSiteContact/Main/customEditCleansed/$1/$2';
$route['VendorSiteContact/updateCleansed/(:any)'] = 'VendorSiteContact/Main/updateCleansed/$1';
$route['VendorSiteContact/editCleansed/(:any)'] = 'VendorSiteContact/Main/editCleansed/$1';

$route['VendorSite/createNew/(:any)'] = 'VendorSite/Main/createNew/$1';
$route['VendorSite/addNew/(:any)'] = 'VendorSite/Main/addNew/$1';
$route['VendorSite/toggle/(:any)'] = 'VendorSite/Main/toggle/$1';
$route['VendorSite/validate/(:any)'] = 'VendorSite/Main/validate/$1';

$route['VendorSite/customUpdateCleansed/(:any)/(:any)'] = 'VendorSite/Main/customUpdateCleansed/$1/$2';
$route['VendorSite/customEditCleansed/(:any)/(:any)'] = 'VendorSite/Main/customEditCleansed/$1/$2';
$route['VendorSite/updateCleansed/(:any)'] = 'VendorSite/Main/updateCleansed/$1';
$route['VendorSite/editCleansed/(:any)'] = 'VendorSite/Main/editCleansed/$1';


$route['Vendors/validate/(:any)'] = 'Vendors/Main/validate/$1';
$route['Vendors/toggle/(:any)'] = 'Vendors/Main/toggle/$1';
$route['Vendors'] = 'Vendors/Main/listCleansed/CO';
$route['Vendors/discardCleansed/(:any)'] = 'Vendors/Main/discardCleansed/$1';
$route['Vendors/cleanseDirty/(:any)'] = 'Vendors/Main/cleanseDirty/$1';
$route['Vendors/editDirty/(:any)'] = 'Vendors/Main/editDirty/$1';
$route['Vendors/customUpdateCleansed/(:any)/(:any)'] = 'Vendors/Main/customUpdateCleansed/$1/$2';
$route['Vendors/customEditCleansed/(:any)/(:any)'] = 'Vendors/Main/customEditCleansed/$1/$2';
$route['Vendors/updateCleansed/(:any)'] = 'Vendors/Main/updateCleansed/$1';
$route['Vendors/editCleansed/(:any)'] = 'Vendors/Main/editCleansed/$1';
$route['Vendors/cleansed/(:any)/(:any)/(:any)/(:any)'] = 'Vendors/Main/listCleansed/$1/$2/$3/$4';
$route['Vendors/cleansed/(:any)'] = 'Vendors/Main/listCleansed/$1/0';
$route['Vendors/dirty/(:any)/(:any)'] = 'Vendors/Main/listDirty/$1/$2';
$route['Vendors/dirty/(:any)'] = 'Vendors/Main/listDirty/$1/0';

$route['Vendors/getPersonTypePE'] = 'Vendors/Main/getPersonTypePE';
$route['Vendors/getDocumentTypePE'] = 'Vendors/Main/getDocumentTypePE';



$route['Vendors/cleansed/(:any)'] = 'Vendors/Main/listCleansed/$1/0';
$route['Vendors/(:any)'] = 'Vendors/Main/listCleansed/$1/0';

$route['ItemTransDefLoc/toggle/(:any)'] = 'ItemTransDefLoc/Main/toggle/$1';
$route['ItemTransDefLoc/customUpdateCleansed/(:any)/(:any)'] = 'ItemTransDefLoc/Main/customUpdateCleansed/$1/$2';
$route['ItemTransDefLoc/customEditCleansed/(:any)/(:any)'] = 'ItemTransDefLoc/Main/customEditCleansed/$1/$2';

$route['ItemCrossRef/searchAssociationDirtyToCleansed/(:any)'] = 'ItemCrossRef/Main/searchAssociationForDirty/$1';

$route['ItemCrossRef/searchAssociationDirtyToCleansed/(:any)'] = 'ItemCrossRef/Main/searchAssociationForDirty/$1';
$route['ItemCrossRef/searchAssociationCleansedToCleansed/(:any)'] = 'ItemCrossRef/Main/searchAssociationForCleansed/$1';

$route['ItemCrossRef/confirmAssociationDirtyToCleansed/(:any)/(:any)'] = 'ItemCrossRef/Main/confirmAssociationDirtyToCleansed/$1/$2';
$route['ItemCrossRef/confirmAssociationCleansedToCleansed/(:any)/(:any)'] = 'ItemCrossRef/Main/confirmAssociationCleansedToCleansed/$1/$2';

$route['Inventory/uploadResource/(:any)'] = 'Inventory/Main/uploadResource/$1';
$route['Inventory/saveResource/(:any)'] = 'Inventory/Main/saveResource/$1';
$route['Inventory/removeResource/(:any)/(:any)'] = 'Inventory/Main/removeResource/$1/$2';

$route['Inventory/associateDirtyToCleansed/(:any)/(:any)'] = 'Inventory/Main/associateDirtyToCleansed/$1/$2';
$route['Inventory/associateCleansedToCleansed/(:any)/(:any)'] = 'Inventory/Main/associateCleansedToCleansed/$1/$2';

$route['ItemMfgPartNum/toggle/(:any)'] = 'ItemMfgPartNum/Main/toggle/$1';
$route['ItemMfgPartNum/addNew/(:any)'] = 'ItemMfgPartNum/Main/addNew/$1';
$route['ItemMfgPartNum/createNew/(:any)'] = 'ItemMfgPartNum/Main/createNew/$1';

$route['ItemMfgPartNum/customEditCleansed/(:any)/(:any)'] = 'ItemMfgPartNum/Main/customEditCleansed/$1/$2';
$route['ItemMfgPartNum/customUpdateCleansed/(:any)/(:any)'] = 'ItemMfgPartNum/Main/customUpdateCleansed/$1/$2';

$route['Inventory/validate/(:any)'] = 'Inventory/Main/validate/$1';
$route['Inventory/discard/(:any)'] = 'Inventory/Main/discardCleansed/$1';
$route['Inventory/cleanseDirty/(:any)'] = 'Inventory/Main/cleanseDirty/$1';
$route['Inventory/toggle/(:any)'] = 'Inventory/Main/toggle/$1';
$route['Inventory/editDirty/(:any)'] = 'Inventory/Main/editDirty/$1';
$route['Inventory/customUpdateCleansed/(:any)/(:any)'] = 'Inventory/Main/customUpdateCleansed/$1/$2';
$route['Inventory/customEditCleansed/(:any)/(:any)'] = 'Inventory/Main/customEditCleansed/$1/$2';
$route['Inventory/updateCleansed/(:any)'] = 'Inventory/Main/updateCleansed/$1';
$route['Inventory/editCleansed/(:any)'] = 'Inventory/Main/editCleansed/$1';
$route['Inventory/cleansed/(:any)/(:any)/(:any)/(:any)'] = 'Inventory/Main/listCleansed/$1/$2/$3/$4';
$route['Inventory/cleansed/(:any)'] = 'Inventory/Main/listCleansed/$1/0';
$route['Inventory/dirty/(:any)/(:any)'] = 'Inventory/Main/listDirty/$1/$2';
$route['Inventory/dirty/(:any)'] = 'Inventory/Main/listDirty/$1/0';
$route['Inventory/(:any)'] = 'Inventory/Main/listCleansed/$1';

$route['Admin/executeProcedure'] = "Admin/Main/executeProcedure";
$route['Admin/validate/(:any)'] = "Admin/Main/validate/$1";

$route['Admin/displayPost/(:any)'] = "Admin/Main/displayPost/$1";
$route['Admin/posts'] = "Admin/Main/posts";
$route['Admin/navigateByCombo'] = "Admin/Main/navigateByCombo";

$route['Admin/login'] = 'Admin/Main/login';
$route['Admin/authenticate'] = 'Admin/Main/authenticate';
$route['Admin/logout'] = 'Admin/Main/logout';

$route['IDL/groups/downloadAllTemplates/(:any)/(:any)'] = 'IDL/Templates/downloadAllTemplates/$1/$2';
$route['IDL/groups/clone/(:any)'] = 'IDL/IDL/cloneGroupTo/$1';
$route['IDL/groups/create'] = 'IDL/IDL/createGroup';
$route['IDL/groups/new'] = 'IDL/IDL/newGroup';
$route['IDL/groups/edit/(:any)'] = 'IDL/IDL/editGroup/$1';
$route['IDL/groups/update/(:any)'] = 'IDL/IDL/updateGroup/$1';
$route['IDL/groups/delete/(:any)'] = 'IDL/IDL/deleteGroup/$1';
$route['IDL/groups'] = 'IDL/IDL/groups';

$route['IDL/descriptors/create/(:any)'] = 'IDL/IDL/createDescriptor/$1';
$route['IDL/descriptors/new/(:any)'] = 'IDL/IDL/newDescriptor/$1';
$route['IDL/descriptors/edit/(:any)'] = 'IDL/IDL/editDescriptor/$1';
$route['IDL/descriptors/update/(:any)'] = 'IDL/IDL/updateDescriptor/$1';
$route['IDL/descriptors/delete/(:any)'] = 'IDL/IDL/deleteDescriptor/$1';
$route['IDL/descriptors'] = 'IDL/IDL/descriptors';

$route['IDL/fields/cloneToMissingCountries/(:any)'] = 'IDL/IDL/cloneToMissingCountries/$1';
$route['IDL/fields/create/(:any)'] = 'IDL/IDL/createField/$1';
$route['IDL/fields/new/(:any)'] = 'IDL/IDL/newField/$1';
$route['IDL/fields/edit/(:any)'] = 'IDL/IDL/editField/$1';
$route['IDL/fields/update/(:any)'] = 'IDL/IDL/updateField/$1';
$route['IDL/fields/delete/(:any)'] = 'IDL/IDL/deleteField/$1';
$route['IDL/fields'] = 'IDL/IDL/fields';

$route['IDL/Stats/grabRankingFrequency/(:any)/(:any)/(:any)'] = 'IDL/Stats/grabRankingFrequency/$1/$2/$3';
$route['IDL/Stats/grabLatestErrorFrequency/(:any)/(:any)/(:any)'] = 'IDL/Stats/grabLatestErrorFrequency/$1/$2/$3';
$route['IDL/Stats/grabTimeSeriesSampleKPI/(:any)/(:any)'] = 'IDL/Stats/grabTimeSeriesSampleKPI/$1/$2';
$route['IDL/Stats/grabLatestSampleKPI/(:any)/(:any)'] = 'IDL/Stats/grabLatestSampleKPI/$1/$2';

$route['IDL/Navigate/navigateGroup/(:any)/(:any)'] = 'IDL/Navigation/navigateGroup/$1/$2';
$route['IDL/Navigate/navigate/(:any)/(:any)/(:any)'] = 'IDL/Navigation/navigate/$1/$2/$3';
$route['IDL/Serializers'] = 'IDL/Serializers';

$route['IDL/Templates/PHP_Base/(:any)/(:any)/(:any)'] = 'IDL/Templates/generatePHP_Base/$1/$2/$3';
$route['IDL/Templates/PHP_ORA/(:any)/(:any)/(:any)'] = 'IDL/Templates/generatePHP_ORA/$1/$2/$3';
$route['IDL/Templates/PHP_RAW/(:any)/(:any)/(:any)'] = 'IDL/Templates/generatePHP_RAW/$1/$2/$3';
$route['IDL/Templates/HeaderController/(:any)/(:any)/(:any)'] = 'IDL/Templates/generateHeaderController/$1/$2/$3';
$route['IDL/Templates/TransactionalController/(:any)/(:any)/(:any)'] = 'IDL/Templates/generateDefaultController/$1/$2/$3';
$route['IDL/Templates/HeaderController/(:any)/(:any)/(:any)'] = 'IDL/Templates/generateHeaderController/$1/$2/$3';
$route['IDL/Templates/LineController/(:any)/(:any)/(:any)'] = 'IDL/Templates/generateLineController/$1/$2/$3';
$route['IDL/Templates/TransactionalController/(:any)/(:any)/(:any)'] = 'IDL/Templates/generateTransactionalController/$1/$2/$3';
$route['IDL/Templates/DefaultRoutes/(:any)/(:any)/(:any)'] = 'IDL/Templates/generateDefaultRoutes/$1/$2/$3';
$route['IDL/Templates/AutoOutput/(:any)/(:any)/(:any)'] = 'IDL/Templates/generateAutoOutputTemplate/$1/$2/$3';
$route['IDL/Templates/FormBase/(:any)/(:any)/(:any)'] = 'IDL/Templates/generateFormForBase/$1/$2/$3';
$route['IDL/Templates/FormCustom/(:any)/(:any)/(:any)'] = 'IDL/Templates/generateFormForCustom/$1/$2/$3';
$route['IDL/Templates/FormMandatory/(:any)/(:any)/(:any)'] = 'IDL/Templates/generateFormForMandatory/$1/$2/$3';
$route['IDL/Templates'] = 'IDL/Templates';

$route['IDL'] = 'IDL/IDL';

$route['Serializers'] = 'Serializers';

$route['KPI/(:any)'] = 'Landing/index/$1';
$route['KPI/(:any)/(:any)'] = 'Landing/showStats/$1/$2';

$route['Transactional/(:any)'] = 'Transactional/index/$1';

$route['default_controller'] = 'Landing/dashboard';
$route['(:any)'] = 'pages/view/$1';

/* GL Params */
$route['Setup/(:any)'] = 'Setup/index/$1';
$route['GLMenu/(:any)'] = 'Setup/indexMenu/$1';
$route['GLParams/(:any)'] = 'Setup/indexParams/$1';
$route['GLParams/(:any)/(:any)/(:any)'] = 'Setup/indexParams/$1/$2/$3';
$route['GLTables/(:any)'] = 'Setup/indexTables/$1';
$route['GLTables/(:any)/(:any)/(:any)'] = 'Setup/indexTables/$1/$2/$3';
$route['GLDiag/(:any)'] = 'Setup/indexDiag/$1';



$route['GLParams/addGLCostCenter/(:any)'] = 'Setup/addGLCostCenter/$1';
$route['GLParams/removeGLCostCenter/(:any)'] = 'Setup/removeGLCostCenter/$1';
$route['GLParams/addGLCostCenterFixed/(:any)'] = 'Setup/addGLCostCenterFixed/$1';
$route['GLParams/removeGLCostCenterFixed/(:any)'] = 'Setup/removeGLCostCenterFixed/$1';

$route['GLParams/addGLExclusionCuentas/(:any)'] = 'Setup/addGLExclusionCuentas/$1';
$route['GLParams/removeGLExclusionCuentas/(:any)'] = 'Setup/removeGLExclusionCuentas/$1';
$route['GLParams/addGLPLDePara/(:any)'] = 'Setup/addGLPLDePara/$1';
$route['GLParams/removeGLPLDePara/(:any)'] = 'Setup/removeGLPLDePara/$1';
$route['GLParams/addGLPL/(:any)'] = 'Setup/addGLPL/$1';
$route['GLParams/removeGLPL/(:any)'] = 'Setup/removeGLPL/$1';
$route['GLParams/addGLPLFixed/(:any)'] = 'Setup/addGLPLFixed/$1';
$route['GLParams/removeGLPLFixed/(:any)'] = 'Setup/removeGLPLFixed/$1';
$route['GLParams/addGLIC/(:any)'] = 'Setup/addGLIC/$1';
$route['GLParams/removeGLIC/(:any)'] = 'Setup/removeGLIC/$1';
$route['GLParams/addGLLOB/(:any)'] = 'Setup/addGLLOB/$1';
$route['GLParams/removeGLLOB/(:any)'] = 'Setup/removeGLLOB/$1';
$route['GLParams/addGLLOBFixed/(:any)'] = 'Setup/addGLLOBFixed/$1';
$route['GLParams/removeGLLOBFixed/(:any)'] = 'Setup/removeGLLOBFixed/$1';

$route['GLParams/addGLCheckRules/(:any)'] = 'Setup/addGLCheckRules/$1';
$route['GLParams/removeGLCheckRules/(:any)'] = 'Setup/removeGLCheckRules/$1';

$route['GLParams/updateGLCuentas/(:any)'] = 'Setup/updateGLCuentas/$1';
$route['GLParams/clearGLCuentas/(:any)/(:any)'] = 'Setup/clearGLCuentas/$1/$2';
$route['GLParams/updateGLLocales/(:any)'] = 'Setup/updateGLLocales/$1';
$route['GLParams/clearGLLocales/(:any)/(:any)'] = 'Setup/clearGLLocales/$1/$2';
$route['GLParams/updateGLTipCos/(:any)'] = 'Setup/updateGLTipCos/$1';
$route['GLParams/clearGLTipCos/(:any)/(:any)'] = 'Setup/clearGLTipCos/$1/$2';
$route['GLParams/updateGLTipoCont/(:any)'] = 'Setup/updateGLTipoCont/$1';
$route['GLParams/clearGLTipoCont/(:any)/(:any)'] = 'Setup/clearGLTipoCont/$1/$2';
$route['GLParams/updateGLDiarios/(:any)'] = 'Setup/updateGLDiarios/$1';
$route['GLParams/clearGLDiarios/(:any)/(:any)'] = 'Setup/clearGLDiarios/$1/$2';
$route['GLParams/updateGLConfigEstandar/(:any)'] = 'Setup/updateGLConfigEstandar/$1';

$route['GLParams/updateGLMonedas/(:any)'] = 'Setup/updateGLMonedas/$1';
$route['GLParams/updateGLCostoLocal/(:any)'] = 'Setup/updateGLCostoLocal/$1';
$route['GLParams/clearGLCostoLocal/(:any)/(:any)'] = 'Setup/clearGLCostoLocal/$1/$2';

$route['GLParams/searchAccount/(:any)'] = 'Setup/searchAccount/$1';


/* PO Params */
$route['POParams'] = 'POParams';  // default to PA
$route['POParams/(:any)'] = 'POParams/index/$1';

/* marked for deletion */
$route['IDL/Serializers/flatfile/(:any)/(:any)/(:any)'] = 'IDL/Serializers/generateFlatFiles/$1/$2/$3';


