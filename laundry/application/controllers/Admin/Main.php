<?php
include_once APPPATH . 'controllers/MY_Controller.php';

ini_set('max_execution_time', 0);
ini_set('memory_limit','2048M');

Use Doctrine\ORM\Query;

class Main extends MY_Controller
{
    protected $selCountry;
    protected $selGroup;
    protected $selEntity;
    protected $selOrder;
    protected $selAsc;

    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->getEntityManager();
        $this->load->helper('url_helper');
        $this->load->library('session');
        $this->load->library('markdown');

        $this->load->helper('form');
        $this->load->library('form_validation');

    }

    public function index()
    {
        $data['countries'] = $this->em->createQuery('SELECT COUNT(DISTINCT o.country) FROM Commons\\IDL\\IDLGroupDescriptor o')->getSingleScalarResult();
        $data['groups'] = $this->em->createQuery('SELECT COUNT(o) FROM Commons\\IDL\\IDLGroupDescriptor o')->getSingleScalarResult();
        $data['descriptors'] = $this->em->createQuery('SELECT COUNT(o) FROM Commons\\IDL\\IDLDescriptor o')->getSingleScalarResult();
        $data['fields'] = $this->em->createQuery('SELECT COUNT(o) FROM Commons\\IDL\\IDLFieldDescriptor o')->getSingleScalarResult();

        $data['title'] = 'IDL stats';
        $data = $this->loadSessionNavigation($data);

        $this->load->view('templates/header', $data);
        $this->load->view('IDL/index', $data);
        $this->load->view('templates/footer', $data);
    }

    public function login(){
        $data['title'] = 'Laundry';
        $data = $this->loadSessionNavigation($data);
        $data['redirectTo'] = "Setup/CO";

        $this->smarty->view('Admin/login.tpl', $data, true);
    }

    public function authenticate(){
        $data['title'] = 'Laundry';
        $data = $this->loadSessionNavigation($data);

        $this->form_validation->set_rules('user', 'user', 'required|exact_length[8]');
        $this->form_validation->set_rules('redirectTo', 'redirectTo', 'required');
        $this->form_validation->set_rules('password', 'password', 'required|max_length[16]');



        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['errorUser'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('user') ));
            $data['errorPassword'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('password') ));
            $data['errorRedirectTo'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('redirectTo') ));

            $this->smarty->view('Admin/login.tpl', $data, true);

        } else {
            $passwords=[md5('laundry')=>0, md5('eagleeyes')=>1, md5('masterblaster')=>2];

            $md5Pass = md5($this->input->post('password'));

            if (isset($passwords[$md5Pass])){
                $this->session->user8id=$this->input->post('user');
                $this->session->clearance=$passwords[$md5Pass];

                redirect(base_url() . $this->input->post('redirectTo'));

            }else{
                $data['redirectTo'] = "Setup/CO";
                $this->smarty->view('Admin/login.tpl', $data, true);

            }


        }


    }

    public function logout(){
        $data['title'] = 'Laundry';
        $this->session->user8id=null;

        $data = $this->loadSessionNavigation($data);
        $data['redirectTo'] = "KPI/CO";
        $this->smarty->view('Admin/login.tpl', $data, true);
    }

    public function validate($groupId)
    {
        $group = $this->em->find('Commons\\IDL\\IDLGroupDescriptor', $groupId);




        redirect(base_url() . "IDL/groups/");
    }

    protected function getFileList($dir)
    {
        // array to hold return value
        $retval = [];

        // add trailing slash if missing
        if(substr($dir, -1) != "/") $dir .= "/";

        // open directory for reading
        $d = new DirectoryIterator($dir) or die("getFileList: Failed opening directory $dir for reading");
        foreach($d as $fileinfo) {
            // skip hidden files
            if($fileinfo->isDot()) continue;
            if($fileinfo->getExtension()!='md') continue;

            preg_match('/\d\d\d\d-\d\d-\d\d_.*\.md/', $fileinfo, $postPattern);
            if (sizeof($postPattern)!=1) continue;

            preg_match('/\d\d\d\d-\d\d-\d\d/', $postPattern[0], $date);

            $retval[] = [
                'title' => substr($postPattern[0], strlen($date[0])+1, strlen($postPattern[0])-strlen($date[0])-4),
                'date' => $date[0],
                'hash' => md5($postPattern[0]),
                'name' => "{$fileinfo}",
                'type' => ($fileinfo->getType() == "dir") ? "dir" : mime_content_type($fileinfo->getRealPath()),
                'size' => $fileinfo->getSize(),
                'lastmod' => $fileinfo->getMTime()
            ];
        }

        usort($retval, function ($item1, $item2) {
            if ($item1['date'] == $item2['date']) return 0;
            return $item1['date'] > $item2['date'] ? -1 : 1;
        });

        return $retval;
    }



    public function navigateByCombo(){
        $this->load->helper('form');
        $this->resetSessionNavigation();

        $group = ucfirst($this->input->post('group'));
        $country = strtoupper($this->input->post('country_code'));
        $sortCol = $this->input->post('sortCol');
        $sortAsc = $this->input->post('sortAsc')?true:false;

        $target = $this->input->post("page_code");

        $this->session->set_userdata(array('selGroup' => $group));
        $this->session->set_userdata(array('selCountry' => $country));
        $this->session->set_userdata(array('selCol' => $sortCol));
        $this->session->set_userdata(array('selAsc' => $sortAsc));

        if (($group=='Transactional')||($group=='GLMenu')||($group== 'Setup')||($group=='GLDiag')||($group=='GLTables')){
            $url = base_url().$group."/".$country;

        }else if (($group=='Transactional')||($group=='POParams')) {
            $url = base_url() . $group . "/" . $country;

        }else if ($target=='dirty'){
            $url = base_url().$group."/dirty/".$country;

        }else{
            $url = base_url().$group."/cleansed/".$country.'/0/'.$sortCol."/".($sortAsc?1:0);

        }

        redirect($url);


    }

    protected $posts = [  ['title'=>'KPI Sampling', 'file'=>ADMIN_POSTS.'2018-05-20_KPI_Sampling.md'],
                          ['title'=>'Vendors & Customer Bugs', 'file'=>ADMIN_POSTS.'2018-05-22_VendorsAndCustomerBugs.md']];

    public function posts(){

        $data['title'] = 'Development posts';
        $data = $this->loadSessionNavigation($data);

        $data['posts'] = $this->getFileList(ADMIN_POSTS);
        $this->load->view('templates/header', $data);
        $this->smarty->view('Admin/posts.tpl', $data, true);
        $this->load->view('templates/footer', $data);


    }

    public function displayPost($id){

        $postFile = null;

        // open directory for reading
        $d = new DirectoryIterator(ADMIN_POSTS) or die("getFileList: Failed opening directory ADMIN_POSTS for reading");
        foreach($d as $fileinfo) {
            // skip hidden files
            if($fileinfo->isDot()) continue;
            if($fileinfo->getExtension()!='md') continue;

            preg_match('/\d\d\d\d-\d\d-\d\d_.*\.md/', $fileinfo, $postPattern);
            if (sizeof($postPattern)!=1) continue;

            if ($id==md5($postPattern[0])){
                $postFile = $fileinfo;
                break;

            }

        }


        if (isset($postFile)){
            $data['title'] = $postFile->getFilename();
            $data = $this->loadSessionNavigation($data);

            $this->load->view('templates/header', $data);
            $this->output->append_output($this->markdown->parse_file($postFile->getPathname()));
            $this->load->view('templates/footer', $data);

        }else{
            redirect( base_url().'Admin/posts');
        }

    }


    public function triggerBulkValidation($group, $country, $flushOnlyOnChange=true){
        $result = array();

        switch ($group){
            case "Projects":
                $result['cmd'] = 'start /B php.exe ' . APPPATH . '..\prototypes\bulkValidations\validate_Projects.php ' . $country;
                break;

            default:
                $result['error'] = 'group not configured';
                break;

        }

        if (!isset($result['error'])){
            if (!$flushOnlyOnChange){
                $result['cmd'] = $result['cmd'] . ' --force';
            }

            $descriptorspec = array(
                0 => array("pipe", "r"),  // stdin for worker
                1 => array("pipe", "w"),  // stdout for worker
            );

            $now = new \DateTime();
            $now->setTimezone(new DateTimeZone('America/Sao_Paulo'));
            $result['triggeredAt'] = $now->format("Y-m-d H:i:sP");

            $result['cmd'] = $result['cmd'] . ' > NUL';

            $worker = proc_open($result['cmd'], $descriptorspec, $pipes);

            stream_set_blocking($pipes[0], 0);
            stream_set_blocking($pipes[1], 0);

            session_write_close();

            /**

            $result['completionDetected'] = false;
            if ($worker) {
                while (!feof($pipes[1])) {
                    if (strstr(stream_get_contents($pipes[1]), 'bulk validation completed')){
                        $result['completionDetected'] = true;

                    }
                }
                proc_close($worker);
            }
            **/

        }


        //add the header here
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        echo json_encode( $result );
    }

}