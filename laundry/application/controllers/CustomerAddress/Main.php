<?php
include_once APPPATH . 'controllers/MY_Controller.php';

/**
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/query-builder.html
 *
 * http://www.doctrine-project.org/api/orm/2.5/class-Doctrine.ORM.QueryBuilder.html
 *
 * CustomerAddress Controller (detail style)
 *
 */

Use Doctrine\ORM\Query;


class Main extends MY_Controller
{
    const VO_CLASS_NAME = 'Commons\\ORA\\CustomerAddress';
    const VO_PRE_CLASS_NAME = 'Commons\\RAW\\PreCustomerAddress';

    const HEADER1_CLASS_NAME = 'Commons\\ORA\\CustomerHeader';
    const HEADER1_KEY_COLUMNS = array('origSystemCustomerRef');
    const CANCEL1 = "Customers/customEditCleansed/";

    const CHILD1_CLASS_NAME = 'Commons\\ORA\\CustomerContact';
    const CHILD1_PRE_CLASS_NAME = 'Commons\\RAW\\PreCustomerContact';
    const CHILD1_KEY_COLUMNS = array('origSystemAdressReference');

    const ACTION_BASE = 'CA/updateCleansed/';
    const ACTION_CUSTOM = 'CA/customUpdateCleansed/';
    const ACTION_LABEL = 'Update CustomerAddress';

    const ADD_PA = [
        'frm' => array(
            'ORA/CustomerAddress/formCustom.tpl'
        )
    ];

    const ADD_ALL = [
        'frm' => array(
            'ORA/CustomerAddress/formCustomALL.tpl'
        )
    ];

    const VIEW_CO = [
        'frm' => array(
            'ORA/CustomerAddress/cleansedTopBar.tpl',
            'ORA/CustomerAddress/formCustomALL.tpl'
        )
    ];

    const VIEW_ALL = [
        'frm' => array(
            'ORA/CustomerAddress/cleansedTopBar.tpl',
            'ORA/CustomerAddress/formCustomALL.tpl'
        )
    ];

    const ADD_LAYOUTS = array(
        "PA" => self::ADD_PA,
        "CO" => self::ADD_ALL,
        "CR" => self::ADD_ALL,
        "HN" => self::ADD_ALL,
        "GT" => self::ADD_ALL,
        "MX" => self::ADD_ALL,
        "NI" => self::ADD_ALL,
        "SV" => self::ADD_ALL,
        "UY" => self::ADD_ALL,
        "PY" => self::ADD_ALL,
        "AR" => self::ADD_ALL,
        "PE" => self::ADD_ALL,
        "EC" => self::ADD_ALL,
        "CL" => self::ADD_ALL
    );


    const VIEW_LAYOUTS = array(
        "PA" => self::VIEW_ALL,
        "CO" => self::VIEW_ALL,
        "CR" => self::VIEW_ALL,
        "HN" => self::VIEW_ALL,
        "GT" => self::VIEW_ALL,
        "MX" => self::VIEW_ALL,
        "NI" => self::VIEW_ALL,
        "SV" => self::VIEW_ALL,
        "UY" => self::VIEW_ALL,
        "PY" => self::VIEW_ALL,
        "PE" => self::VIEW_ALL,
        "AR" => self::VIEW_ALL,
        "EC" => self::VIEW_ALL,
        "CL" => self::VIEW_ALL
    );


    public function __construct()
    {
        parent::__construct();

    }

    public function editCleansed($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence(null, $data);
    }

    public function customEditCleansed($country, $id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence($country, $data);
    }

    public function toggle($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->setDismissed(!$vo->getDismissed());
        $this->em->merge($vo);
        $this->em->flush();

        $this->toggleChildren(self::CHILD1_CLASS_NAME,self::CHILD1_KEY_COLUMNS,  array($vo->getOrigSystemAdressReference()),$vo->getDismissed());

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($vo->getOrigSystemCustomerRef()))->getSingleResult();

        $header->validate($this->em);
        $this->em->flush();

        $header->validateCrossRef($this->em);
        $this->em->flush();

        redirect(base_url() . self::CANCEL1 . $header->getCountry(). '/'.$header->getId());
    }

    public function validate($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->validate($this->em);
        $this->em->flush();

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($vo->getOrigSystemCustomerRef()))->getSingleResult();

        $header->validateCrossRef($this->em);
        $this->em->flush();

        redirect(base_url() . self::CANCEL1 . $header->getCountry(). '/'.$header->getId());
    }

    private function startEditSequence($country, $data)
    {
        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($data['origSystemCustomerRef']))->getSingleResult();

        $actionCustomByCountry = self::ACTION_CUSTOM . $country.'/' . $data['id'];
        $cancelAction = self::CANCEL1 . $country.'/' . $header->getId();

        $data = $this->loadOraGeoCodes($data);

        // CustomerContact children
        $data['contacts'] = $this->getChildQuery(self::CHILD1_CLASS_NAME,
            self::CHILD1_KEY_COLUMNS,
            array($data['origSystemAdressReference']))->getResult(Query::HYDRATE_ARRAY);

        $data = $this->loadCommonData(
            $data,
            'Edit cleansed CustomerAddress #' . $data['id'],
            $actionCustomByCountry,
            self::ACTION_LABEL,
            '',
            $cancelAction
        );

        $this->displayStandardLayout(self::VIEW_LAYOUTS[$country]['frm'], $data);
    }

    public function updateCleansed($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->loadValidationRules(self::VO_CLASS_NAME, $id, $this->form_validation);

        if ($this->form_validation->run() === FALSE) {
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence(null, $data);

        } else {
            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);

            if (($vo->getProvince()!='')&&($vo->getCountry()=='PA')){
                $vo->setState($vo->getProvince());
                $vo->setCounty($vo->getProvince());
            }

            if (($vo->getState()!='')&&($vo->getCountry()=='CO')){
                $vo->setProvince($vo->getState());
                $vo->setCounty($vo->getState());
            }

            $this->em->merge($vo);

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getorigSystemCustomerRef()))->getSingleResult();

            $header->validate($this->em);
            $this->em->flush();

            $header->validateCrossRef($this->em);
            $this->em->flush();

            redirect(base_url() . self::CANCEL1 . $header->getCountry(). '/'.$header->getId());
        }
    }

    public function customUpdateCleansed($country, $id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->loadDefaultRulesByCountry($country);

        if ($this->form_validation->run() === FALSE) {
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence($country, $data);

        } else {

            $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);

            $oldAddressCountry = $vo->getAddressCountry();
            $oldState = $vo->getState();
            $oldProvince = $vo->getProvince();
            $oldCounty = $vo->getCounty();
            $oldCity = $vo->getCity();

            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);

            if ($this->input->post('changeGeoCodes')==null){
                $vo->setAddressCountry($oldAddressCountry);
                $vo->setState($oldState);
                $vo->setProvince($oldProvince);
                $vo->setCounty($oldCounty);
                $vo->setCity($oldCity);
            }

            $vo->setTimeZone($this->input->post('timeZone'));
            $this->em->flush();

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getorigSystemCustomerRef()))->getSingleResult();

            $header->validate($this->em);
            $this->em->flush();

            $header->validateCrossRef($this->em);
            $this->em->flush();

            redirect(base_url() . self::CANCEL1 . $header->getCountry(). '/'.$header->getId());
        }
    }

    public function createNew($id,  $data=null)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME, array('id'), array($id))->getSingleResult();
        $cancelAction = self::CANCEL1.$header->getCountry().'/'.$header->getId();

        if (!isset($data)){
            $data = $this->resetDataAddress($header);
        }

        $data = $this->loadOraGeoCodes($data);

        $actionCreateNewAddress = 'CA/addNew/' . $header->getId() ;

        $data = $this->loadCommonData(
            $data,
            'Add a new Address to CustomerHeader #' . $header->getId(),
            $actionCreateNewAddress,
            'Create new Address to Customer ' . $header->getOrigSystemCustomerRef(),
            '',
            $cancelAction
        );

        $this->displayStandardLayout(self::ADD_LAYOUTS[$header->getCountry()]['frm'], $data);
    }

    public function addNew($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME, array('id'),  array($id))->getSingleResult();

        $this->loadDefaultRulesByCountry($header->getCountry());

        if ($this->form_validation->run() === FALSE){
            $data = $this->resetDataAddress($header);

            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data['timeZone'] = ($this->input->post('timeZone'));

            $data = $this->loadFormErrorsForClassNameAndCountry(self::VO_CLASS_NAME, $data['country'], $data, $this->form_validation);
            $this->createNew($id, $data);

        }else{
            $vo = $this->createInstanceWithFormValues(self::VO_CLASS_NAME, $header->getCountry(), $this->input);

            $vo->setCustomerName($header->getCustomerName());
            $vo->setOrigSystemCustomerRef($header->getOrigSystemCustomerRef());

            $vo->setTimeZone($this->input->post('timeZone'));

            $query = $this->em->createQuery("SELECT COUNT(cc) FROM Commons\\ORA\CustomerAddress cc WHERE cc.origSystemCustomerRef=?1");
            $query->setParameter(1, $header->getOrigSystemCustomerRef());
            $vsCount = $query->getSingleScalarResult();

            $vo->setOrigSystemAdressReference(str_replace('-CC','',$header->getOrigSystemCustomerRef()).'.'.($vsCount+1).'-CA');

            $vo->setCountry($header->getCountry());
            $vo->setOrgName($header->getOrgName());
            $vo->setDismissed(false);

            $this->em->persist($vo);
            $this->em->flush();

            $header->validate($this->em);
            $this->em->flush();

            $header->validateCrossRef($this->em);
            $this->em->flush();

            redirect(base_url() . self::CANCEL1 . $header->getCountry().'/'.$header->getId());
        }


    }

    /**
     * @param $country
     */
    private function loadDefaultRulesByCountry($country)
    {
// uncomment rules that fit the custom CustomerAddress form
        switch ($country) {
            case 'PA':
                //$this->form_validation->set_rules('customerName', 'customerName', 'required|max_length[340]');
                //$this->form_validation->set_rules('origSystemCustomerRef', 'origSystemCustomerRef', 'required|max_length[240]');
                //$this->form_validation->set_rules('origSystemAdressReference', 'origSystemAdressReference', 'required|max_length[30]');
                $this->form_validation->set_rules('siteUseCode', 'siteUseCode', 'required|max_length[30]|in_list[SHIP_TO,BILL_TO,BOTH,DUN,ALL]');
                $this->form_validation->set_rules('location', 'location', 'max_length[40]');
                $this->form_validation->set_rules('addressLine1', 'addressLine1', 'required|max_length[240]');
                //$this->form_validation->set_rules('addressLine2', 'addressLine2', 'max_length[240]');
                //$this->form_validation->set_rules('addressLine3', 'addressLine3', 'max_length[240]');
                //$this->form_validation->set_rules('addressLine4', 'addressLine4', 'max_length[240]');
                //$this->form_validation->set_rules('addressLinesPhonetic', 'addressLinesPhonetic', 'max_length[560]');
                $this->form_validation->set_rules('city', 'city', 'required|max_length[60]');
                $this->form_validation->set_rules('province', 'province', 'required|max_length[60]');
                //$this->form_validation->set_rules('state', 'state', 'max_length[60]');
                //$this->form_validation->set_rules('county', 'county', 'max_length[60]');
                //$this->form_validation->set_rules('postalCode', 'postalCode', 'max_length[60]');
                $this->form_validation->set_rules('addressCountry', 'addressCountry', 'required|max_length[60]');
                //$this->form_validation->set_rules('recevablesAccount', 'recevablesAccount', 'max_length[150]');
                //$this->form_validation->set_rules('revenueAccount', 'revenueAccount', 'max_length[150]');
                //$this->form_validation->set_rules('taxAccount', 'taxAccount', 'max_length[150]');
                //$this->form_validation->set_rules('freightAccount', 'freightAccount', 'max_length[150]');
                //$this->form_validation->set_rules('clearingAccount', 'clearingAccount', 'max_length[150]');
                //$this->form_validation->set_rules('unbilledReceivablesAccount', 'unbilledReceivablesAccount', 'max_length[150]');
                //$this->form_validation->set_rules('unearnedRevenueAccount', 'unearnedRevenueAccount', 'max_length[150]');
                //$this->form_validation->set_rules('paymentMethodName', 'paymentMethodName', 'max_length[30]');
                $this->form_validation->set_rules('customerProfile', 'customerProfile', 'required|max_length[30]');
                //$this->form_validation->set_rules('siteUseTaxReference', 'siteUseTaxReference', 'max_length[50]');
                $this->form_validation->set_rules('timeZone', 'timeZone', 'required|max_length[50]');
                $this->form_validation->set_rules('language', 'language', 'required|max_length[3]');
                $this->form_validation->set_rules('gdfAddressAttribute8', 'gdfAddressAttribute8', 'max_length[30]');
                //$this->form_validation->set_rules('gdfSiteUseAttribute9', 'gdfSiteUseAttribute9', 'max_length[150]');
                //$this->form_validation->set_rules('orgName', 'orgName', 'required|max_length[60]');
                //$this->form_validation->set_rules('gdfAddressAttribute9', 'gdfAddressAttribute9', 'max_length[150]');
                //$this->form_validation->set_rules('gdfSiteUseAttribute8', 'gdfSiteUseAttribute8', 'max_length[30]');
                //$this->form_validation->set_rules('economicActivity', 'economicActivity', 'max_length[15]');

                break;

            case 'CO':
                //$this->form_validation->set_rules('customerName', 'customerName', 'required|max_length[340]');
                //$this->form_validation->set_rules('origSystemCustomerRef', 'origSystemCustomerRef', 'required|max_length[240]');
                //$this->form_validation->set_rules('origSystemAdressReference', 'origSystemAdressReference', 'required|max_length[30]');
                $this->form_validation->set_rules('siteUseCode', 'siteUseCode', 'required|max_length[30]|in_list[SHIP_TO,BILL_TO,BOTH,DUN,ALL]');
                $this->form_validation->set_rules('location', 'location', 'max_length[40]');
                $this->form_validation->set_rules('addressLine1', 'addressLine1', 'required|max_length[240]');
                //$this->form_validation->set_rules('addressLine2', 'addressLine2', 'max_length[240]');
                //$this->form_validation->set_rules('addressLine3', 'addressLine3', 'max_length[240]');
                //$this->form_validation->set_rules('addressLine4', 'addressLine4', 'max_length[240]');
                //$this->form_validation->set_rules('addressLinesPhonetic', 'addressLinesPhonetic', 'required|max_length[560]');
                $this->form_validation->set_rules('city', 'city', 'required|max_length[60]');
                //$this->form_validation->set_rules('province', 'province', 'max_length[60]');
                $this->form_validation->set_rules('state', 'state', 'required|max_length[60]');
                //$this->form_validation->set_rules('county', 'county', 'max_length[60]');
                //$this->form_validation->set_rules('postalCode', 'postalCode', 'max_length[60]');
                $this->form_validation->set_rules('addressCountry', 'addressCountry', 'required|max_length[60]');
                //$this->form_validation->set_rules('recevablesAccount', 'recevablesAccount', 'max_length[150]');
                //$this->form_validation->set_rules('revenueAccount', 'revenueAccount', 'max_length[150]');
                //$this->form_validation->set_rules('taxAccount', 'taxAccount', 'max_length[150]');
                //$this->form_validation->set_rules('freightAccount', 'freightAccount', 'max_length[150]');
                //$this->form_validation->set_rules('clearingAccount', 'clearingAccount', 'max_length[150]');
                //$this->form_validation->set_rules('unbilledReceivablesAccount', 'unbilledReceivablesAccount', 'max_length[150]');
                //$this->form_validation->set_rules('unearnedRevenueAccount', 'unearnedRevenueAccount', 'max_length[150]');
                //$this->form_validation->set_rules('paymentMethodName', 'paymentMethodName', 'max_length[30]');
                $this->form_validation->set_rules('customerProfile', 'customerProfile', 'required|max_length[30]');
                //$this->form_validation->set_rules('siteUseTaxReference', 'siteUseTaxReference', 'max_length[50]');
                $this->form_validation->set_rules('timeZone', 'timeZone', 'required|max_length[50]');
                $this->form_validation->set_rules('language', 'language', 'required|max_length[3]');
                $this->form_validation->set_rules('gdfAddressAttribute8', 'gdfAddressAttribute8', 'max_length[30]');
                //$this->form_validation->set_rules('gdfSiteUseAttribute9', 'gdfSiteUseAttribute9', 'max_length[150]');
                //$this->form_validation->set_rules('orgName', 'orgName', 'required|max_length[60]');
                //$this->form_validation->set_rules('gdfAddressAttribute9', 'gdfAddressAttribute9', 'max_length[150]');
                //$this->form_validation->set_rules('gdfSiteUseAttribute8', 'gdfSiteUseAttribute8', 'max_length[30]');
                $this->form_validation->set_rules('economicActivity', 'economicActivity', 'required|max_length[15]');

                break;

            case 'MX':
            case 'NI':
            case 'HN':
            case 'CR':
            case 'SV':
            case 'GT':
            default:
                //$this->form_validation->set_rules('customerName', 'customerName', 'required|max_length[340]');
                //$this->form_validation->set_rules('origSystemCustomerRef', 'origSystemCustomerRef', 'required|max_length[240]');
                //$this->form_validation->set_rules('origSystemAdressReference', 'origSystemAdressReference', 'required|max_length[30]');
                //$this->form_validation->set_rules('siteUseCode', 'siteUseCode', 'required|max_length[30]|in_list[SHIP_TO,BILL_TO,DUN,ALL]');
                //$this->form_validation->set_rules('location', 'location', 'max_length[40]');
                //$this->form_validation->set_rules('addressLine1', 'addressLine1', 'required|max_length[240]');
                //$this->form_validation->set_rules('addressLine2', 'addressLine2', 'max_length[240]');
                //$this->form_validation->set_rules('addressLine3', 'addressLine3', 'max_length[240]');
                //$this->form_validation->set_rules('addressLine4', 'addressLine4', 'max_length[240]');
                //$this->form_validation->set_rules('addressLinesPhonetic', 'addressLinesPhonetic', 'max_length[560]');
                $this->form_validation->set_rules('city', 'city', 'max_length[60]');
                //$this->form_validation->set_rules('province', 'province', 'max_length[60]');
                //$this->form_validation->set_rules('state', 'state', 'max_length[60]');
                //$this->form_validation->set_rules('county', 'county', 'max_length[60]');
                //$this->form_validation->set_rules('postalCode', 'postalCode', 'max_length[60]');
                $this->form_validation->set_rules('addressCountry', 'addressCountry', 'required|max_length[60]');
                //$this->form_validation->set_rules('recevablesAccount', 'recevablesAccount', 'max_length[150]');
                //$this->form_validation->set_rules('revenueAccount', 'revenueAccount', 'max_length[150]');
                //$this->form_validation->set_rules('taxAccount', 'taxAccount', 'max_length[150]');
                //$this->form_validation->set_rules('freightAccount', 'freightAccount', 'max_length[150]');
                //$this->form_validation->set_rules('clearingAccount', 'clearingAccount', 'max_length[150]');
                //$this->form_validation->set_rules('unbilledReceivablesAccount', 'unbilledReceivablesAccount', 'max_length[150]');
                //$this->form_validation->set_rules('unearnedRevenueAccount', 'unearnedRevenueAccount', 'max_length[150]');
                //$this->form_validation->set_rules('paymentMethodName', 'paymentMethodName', 'max_length[30]');
                //$this->form_validation->set_rules('customerProfile', 'customerProfile', 'required|max_length[30]');
                //$this->form_validation->set_rules('siteUseTaxReference', 'siteUseTaxReference', 'max_length[50]');
                //$this->form_validation->set_rules('timeZone', 'timeZone', 'required|max_length[50]');
                //$this->form_validation->set_rules('language', 'language', 'required|max_length[3]');
                //$this->form_validation->set_rules('gdfAddressAttribute8', 'gdfAddressAttribute8', 'max_length[30]');
                //$this->form_validation->set_rules('gdfSiteUseAttribute9', 'gdfSiteUseAttribute9', 'max_length[150]');
                //$this->form_validation->set_rules('orgName', 'orgName', 'required|max_length[60]');
                //$this->form_validation->set_rules('gdfAddressAttribute9', 'gdfAddressAttribute9', 'max_length[150]');
                //$this->form_validation->set_rules('gdfSiteUseAttribute8', 'gdfSiteUseAttribute8', 'max_length[30]');
                //$this->form_validation->set_rules('economicActivity', 'economicActivity', 'max_length[15]');

                break;
        }
    }

    private function resetDataAddress($header){
        $data = array();
        $data['country'] = $header->getCountry();
        $data['origSystemPartyRef'] = $header->getOrigSystemPartyRef();
        $data['origSystemCustomerRef'] = $header->getOrigSystemCustomerRef();
        $data['addressCountry'] = '';
        $data['province']='';
        $data['state']='';
        $data['city']='';
        $data['origSystemAddressRef'] = 'NEW';
        $data['id'] = 'NEW';

        return $data;
    }

}

