<?php
include_once APPPATH . 'controllers/MY_Controller.php';

/**
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/query-builder.html
 *
 * http://www.doctrine-project.org/api/orm/2.5/class-Doctrine.ORM.QueryBuilder.html
 *
 * CustomerContact Controller (detail style)
 *
 */

Use Doctrine\ORM\Query;


class Main extends MY_Controller {

    const VO_CLASS_NAME = 'Commons\\ORA\\CustomerContact';
    const VO_PRE_CLASS_NAME = 'Commons\\RAW\\PreCustomerContact';

    const HEADER1_CLASS_NAME = 'Commons\\ORA\\CustomerHeader';
    const HEADER1_KEY_COLUMNS = array('origSystemCustomerRef');
    const CANCEL1 = "Customers/customEditCleansed/";
    const HEADER2_CLASS_NAME = 'Commons\\ORA\\CustomerAddress';
    const HEADER2_KEY_COLUMNS = array('origSystemAdressReference');
    const CANCEL2 = "CA/customEditCleansed/";


    const ACTION_BASE = 'CustomerContact/updateCleansed/';
    const ACTION_CUSTOM = 'CustomerContact/customUpdateCleansed/';
    const ACTION_LABEL = 'Update CustomerContact';

    const LAYOUT_BASE = array(
        'ORA/CustomerContact/cleansedTopBar.tpl',
        'ORA/CustomerContact/formBase.tpl'
    );

    const LAYOUT_CUSTOM = array(
        'ORA/CustomerContact/cleansedTopBar.tpl',
        'ORA/CustomerContact/formCustom.tpl'
    );

    const LAYOUT_CUSTOM_CO = array(
        'ORA/CustomerContact/cleansedTopBar.tpl',
        'ORA/CustomerContact/formCustomCO.tpl'
    );

    const LAYOUT_ADD = array(
        'ORA/CustomerContact/formCustom.tpl'
    );

    const LAYOUT_ERROR = array(
        'ORA/CustomerContact/error.tpl'
    );

    public function __construct()
    {
        parent::__construct();

    }

    public function editCleansed($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence(null, $data);
    }

    public function customEditCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence($country, $data);
    }


    private function startEditSequence($country, $data){

        try{
            $header = $this->getHeaderQuery(self::HEADER2_CLASS_NAME,
                self::HEADER2_KEY_COLUMNS,
                array($data['origSystemAdressReference']))->getSingleResult();

            $actionCustomByCountry = self::ACTION_CUSTOM . $country.'/' . $data['id'];

            switch($country){
                case 'PA':
                    $layoutByCountry = self::LAYOUT_CUSTOM;
                    $cancelAction = self::CANCEL2.'PA/'.$header->getId();
                    break;

                default:
                    $layoutByCountry = self::LAYOUT_CUSTOM_CO;
                    $cancelAction = self::CANCEL2.$data['country'].'/'.$header->getId();
                    break;
            }
            $data = $this->loadCommonData(
                $data,
                'Edit cleansed CustomerContact #' . $data['id'],
                $actionCustomByCountry,
                self::ACTION_LABEL,
                '',
                $cancelAction
            );

            $this->displayStandardLayout($layoutByCountry, $data);

        }catch(\Doctrine\ORM\NonUniqueResultException $e){

            $header = $this->getHeaderQuery(self::HEADER2_CLASS_NAME,
                self::HEADER2_KEY_COLUMNS,
                array($data['origSystemAdressReference']))->getResult();

            $data = $this->loadCommonData(
                $data,
                'Data has problems - Multiple CustomerAddress for user #' . $data['id'],
                '',
                self::ACTION_LABEL,
                '',
                '');

            $data['message']['EN'] = 'MORE THAN ONE CustomerAddress with ref '.$data['origSystemAdressReference']. '.  Please dismiss records until ONLY ONE CustomerAddress is active.';
            $data['message']['PT'] = 'MAIS DE UM CustomerAddress com ref '.$data['origSystemAdressReference']. '.  Por favor, use o dismiss para que APENAS UM CustomerAddress fique ativo.';

            $data['headerIds'] = array();

            foreach ($header as $h){
                array_push($data['headerIds'], $h->getId());
            }

            $this->displayStandardLayout(self::LAYOUT_ERROR, $data);

        }

    }

    public function toggle($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->setDismissed(!$vo->getDismissed());
        $this->em->merge($vo);
        $this->em->flush();

        $header = $this->getHeaderQuery(self::HEADER2_CLASS_NAME,
            self::HEADER2_KEY_COLUMNS,
            array($vo->getorigSystemAdressReference()))->getSingleResult();

        $header->validateCrossRef($this->em);
        $this->em->flush();

        redirect(base_url() . self::CANCEL2 . $header->getCountry(). '/'.$header->getId());
    }

    public function createNew($id,  $data=null){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $header = $this->getHeaderQuery(self::HEADER2_CLASS_NAME, array('id'),  array($id))->getSingleResult();

        switch($header->getCountry()){
            case 'PA':
                $layoutByCountry = self::LAYOUT_ADD;
                $cancelAction = self::CANCEL2.'PA/'.$header->getId();
                break;

            case 'CO':
            case 'MX':
            case 'NI':
            case 'HN':
            case 'CR':
            case 'SV':

                $layoutByCountry = self::LAYOUT_ADD;
                $cancelAction = self::CANCEL2.$header->getCountry().'/'.$header->getId();
                break;

            default:
                $layoutByCountry = self::LAYOUT_ADD;
                $cancelAction = self::CANCEL2.$header->getCountry().'/'.$header->getId();
                break;
        }

        $actionCreateNewContact = 'CustomerContact/addNew/' . $header->getId() ;

        if (!isset($data)){
            $data = $this->resetDataContact($header);
        }

        $data = $this->loadCommonData(
            $data,
            'Add a new Contact to CustomerAddress #' . $header->getId(),
            $actionCreateNewContact,
            'Create new Contact to CustomerAddress ' . $header->getOrigSystemAdressReference(),
            '',
            $cancelAction
        );

        $this->displayStandardLayout($layoutByCountry, $data);


    }

    public function addNew($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $header = $this->getHeaderQuery(self::HEADER2_CLASS_NAME, array('id'),  array($id))->getSingleResult();

        switch($header->getCountry()) {
            case 'PA':
                $this->form_validation->set_rules('contactFirstName', 'contactFirstName', 'required|max_length[60]');
                $this->form_validation->set_rules('contactLastName', 'contactLastName', 'required|max_length[60]');
                $this->form_validation->set_rules('telephoneType', 'telephoneType', 'required|max_length[30]|in_list[PHONE,FAX,EMAIL]');
                $this->form_validation->set_rules('phoneCountryCode', 'phoneCountryCode', 'max_length[10]');
                $this->form_validation->set_rules('telephoneAreaCode', 'telephoneAreaCode', 'max_length[10]');
                $this->form_validation->set_rules('telephone', 'telephone', 'max_length[25]');
                $this->form_validation->set_rules('telephoneExtension', 'telephoneExtension', 'max_length[20]');
                $this->form_validation->set_rules('emailAddress', 'emailAddress', 'max_length[240]');
                $this->form_validation->set_rules('jobTitle', 'jobTitle', 'max_length[30]');

                break;

            case 'CO':
            case 'MX':
            case 'NI':
            case 'HN':
            case 'CR':
            case 'SV':

                $this->form_validation->set_rules('contactFirstName', 'contactFirstName', 'required|max_length[60]');
                $this->form_validation->set_rules('contactLastName', 'contactLastName', 'required|max_length[60]');
                $this->form_validation->set_rules('telephoneType', 'telephoneType', 'required|max_length[30]|in_list[PHONE,FAX,EMAIL]');
                $this->form_validation->set_rules('phoneCountryCode', 'phoneCountryCode', 'max_length[10]');
                $this->form_validation->set_rules('telephoneAreaCode', 'telephoneAreaCode', 'max_length[10]');
                $this->form_validation->set_rules('telephone', 'telephone', 'max_length[25]');
                $this->form_validation->set_rules('telephoneExtension', 'telephoneExtension', 'max_length[20]');
                $this->form_validation->set_rules('emailAddress', 'emailAddress', 'max_length[240]');
                $this->form_validation->set_rules('jobTitle', 'jobTitle', 'max_length[30]');

                break;

            default:
                $this->form_validation->set_rules('contactFirstName', 'contactFirstName', 'required|max_length[60]');
                $this->form_validation->set_rules('contactLastName', 'contactLastName', 'required|max_length[60]');
                $this->form_validation->set_rules('telephoneType', 'telephoneType', 'required|max_length[30]|in_list[PHONE,FAX,EMAIL]');
                $this->form_validation->set_rules('phoneCountryCode', 'phoneCountryCode', 'max_length[10]');
                $this->form_validation->set_rules('telephoneAreaCode', 'telephoneAreaCode', 'max_length[10]');
                $this->form_validation->set_rules('telephone', 'telephone', 'max_length[25]');
                $this->form_validation->set_rules('telephoneExtension', 'telephoneExtension', 'max_length[20]');
                $this->form_validation->set_rules('emailAddress', 'emailAddress', 'max_length[240]');
                $this->form_validation->set_rules('jobTitle', 'jobTitle', 'max_length[30]');

                break;
        }

        //Todos los pa�ses
        $telephoneType = $this->input->post('telephoneType');

        switch($telephoneType){
            case 'PHONE':
                $this->form_validation->set_rules('phoneCountryCode', 'phoneCountryCode', 'required|max_length[10]');
                $this->form_validation->set_rules('telephoneAreaCode', 'telephoneAreaCode', 'required|max_length[10]');
                $this->form_validation->set_rules('telephone', 'telephone', 'required|max_length[25]');
                break;

            case 'FAX':

                $this->form_validation->set_rules('phoneCountryCode', 'phoneCountryCode', 'required|max_length[10]');
                $this->form_validation->set_rules('telephoneAreaCode', 'telephoneAreaCode', 'required|max_length[10]');
                $this->form_validation->set_rules('telephone', 'telephone', 'required|max_length[25]');
                break;

            case 'EMAIL':
                $this->form_validation->set_rules('emailAddress', 'emailAddress', 'required|max_length[240]');
                break;

        }

        if ($this->form_validation->run() === FALSE){
            $data = $this->resetDataContact($header);

            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrorsForClassNameAndCountry(self::VO_CLASS_NAME, $data['country'], $data, $this->form_validation);
            $this->createNew($id, $data);

        }else{
            $vo = $this->createInstanceWithFormValues(self::VO_CLASS_NAME, $header->getCountry(), $this->input);

            $vo->setOrigSystemCustomerRef($header->getOrigSystemCustomerRef());
            $vo->setOrigSystemAdressReference($header->getOrigSystemAdressReference());

            $query = $this->em->createQuery("SELECT COUNT(cc) FROM Commons\\ORA\CustomerContact cc WHERE cc.origSystemAdressReference=?1");
            $query->setParameter(1, $header->getOrigSystemAdressReference());
            $vsCount = $query->getSingleScalarResult();

            $vo->setOrigSystemContactRef(str_replace('-CA','',$header->getOrigSystemAdressReference()).'.'.($vsCount).'-CC');
            $vo->setOrigSystemTelephoneRef(str_replace('-CA','',$header->getOrigSystemAdressReference()).'.'.($vsCount).'-CT');

            $vo->setCountry($header->getCountry());
            $vo->setOrgName($header->getOrgName());
            $vo->setDismissed(false);

            $this->em->persist($vo);
            $this->em->flush();

            $header->validateCrossRef($this->em);
            $this->em->flush();

            redirect(base_url() . self::CANCEL2 . $header->getCountry().'/'.$header->getId());
        }


    }

    private function resetDataContact($header){
        $data = array();
        $data['country'] = $header->getCountry();
        $data['origSystemCustomerRef'] = $header->getOrigSystemCustomerRef();
        $data['origSystemAdressReference'] = $header->getOrigSystemAdressReference();
        $data['origSystemContactRef'] = 'NEW';
        $data['origSystemTelephoneRef'] = 'NEW';
        $data['id'] = 'NEW';

        return $data;

    }


    public function updateCleansed($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->loadValidationRules(self::VO_CLASS_NAME, $id, $this->form_validation);

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence(null, $data);

        }else{
            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);

            $this->em->merge($vo);

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getorigSystemCustomerRef()))->getSingleResult();

            $header->validateCrossRef($this->em);
            $header = $this->getHeaderQuery(self::HEADER2_CLASS_NAME,
                self::HEADER2_KEY_COLUMNS,
                array($vo->getorigSystemAdressReference()))->getSingleResult();

            $header->validateCrossRef($this->em);


            $this->em->flush();

            redirect(base_url() . self::CANCEL2 . $header->getCountry(). '/'.$header->getId());
        }
    }

    public function validate($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->validate($this->em);
        $this->em->merge($vo);
        $this->em->flush();

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($vo->getOrigSystemCustomerRef()))->getSingleResult();

        $header->validateCrossRef($this->em);

        redirect(base_url() . self::CANCEL1 . $header->getCountry(). '/'.$header->getId());
    }

    public function customUpdateCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        // uncomment rules that fit the custom CustomerContact form

        switch($country){
            case 'PA':
                //$this->form_validation->set_rules('origSystemCustomerRef', 'origSystemCustomerRef', 'required|max_length[240]');
                //$this->form_validation->set_rules('origSystemAdressReference', 'origSystemAdressReference', 'required|max_length[30]');
                //$this->form_validation->set_rules('origSystemContactRef', 'origSystemContactRef', 'required|max_length[30]');
                //$this->form_validation->set_rules('origSystemTelephoneRef', 'origSystemTelephoneRef', 'required|max_length[240]');
                $this->form_validation->set_rules('contactFirstName', 'contactFirstName', 'required|max_length[60]');
                $this->form_validation->set_rules('contactLastName', 'contactLastName', 'required|max_length[60]');
                //$this->form_validation->set_rules('telephoneType', 'telephoneType', 'required|max_length[30]|in_list[PHONE,FAX,EMAIL]');
                //$this->form_validation->set_rules('phoneCountryCode', 'phoneCountryCode', 'max_length[10]');
                //$this->form_validation->set_rules('telephoneAreaCode', 'telephoneAreaCode', 'max_length[10]');
                //$this->form_validation->set_rules('telephone', 'telephone', 'max_length[25]');
                //$this->form_validation->set_rules('telephoneExtension', 'telephoneExtension', 'max_length[20]');
                //$this->form_validation->set_rules('emailAddress', 'emailAddress', 'max_length[240]');
                //$this->form_validation->set_rules('jobTitle', 'jobTitle', 'max_length[30]');
                //$this->form_validation->set_rules('contactAttribute1', 'contactAttribute1', 'max_length[10]|in_list[EMAIL,PRINT]');
                //$this->form_validation->set_rules('orgName', 'orgName', 'required|max_length[60]');

                break;

            case 'CO':
            case 'MX':
            case 'NI':
            case 'HN':
            case 'CR':
            case 'SV':

                //$this->form_validation->set_rules('origSystemCustomerRef', 'origSystemCustomerRef', 'required|max_length[240]');
                //$this->form_validation->set_rules('origSystemAdressReference', 'origSystemAdressReference', 'required|max_length[30]');
                //$this->form_validation->set_rules('origSystemContactRef', 'origSystemContactRef', 'required|max_length[30]');
                //$this->form_validation->set_rules('origSystemTelephoneRef', 'origSystemTelephoneRef', 'required|max_length[240]');
                $this->form_validation->set_rules('contactFirstName', 'contactFirstName', 'required|max_length[60]');
                $this->form_validation->set_rules('contactLastName', 'contactLastName', 'required|max_length[60]');
                //$this->form_validation->set_rules('telephoneType', 'telephoneType', 'required|max_length[30]|in_list[PHONE,FAX,EMAIL]');
                //$this->form_validation->set_rules('phoneCountryCode', 'phoneCountryCode', 'max_length[10]');
                //$this->form_validation->set_rules('telephoneAreaCode', 'telephoneAreaCode', 'max_length[10]');
                //$this->form_validation->set_rules('telephone', 'telephone', 'max_length[25]');
                //$this->form_validation->set_rules('telephoneExtension', 'telephoneExtension', 'max_length[20]');
                //$this->form_validation->set_rules('emailAddress', 'emailAddress', 'max_length[240]');
                //$this->form_validation->set_rules('jobTitle', 'jobTitle', 'max_length[30]');
                //$this->form_validation->set_rules('contactAttribute1', 'contactAttribute1', 'max_length[10]|in_list[EMAIL,PRINT]');
                //$this->form_validation->set_rules('orgName', 'orgName', 'required|max_length[60]');

                break;

            default:
                //$this->form_validation->set_rules('origSystemCustomerRef', 'origSystemCustomerRef', 'required|max_length[240]');
                //$this->form_validation->set_rules('origSystemAdressReference', 'origSystemAdressReference', 'required|max_length[30]');
                //$this->form_validation->set_rules('origSystemContactRef', 'origSystemContactRef', 'required|max_length[30]');
                //$this->form_validation->set_rules('origSystemTelephoneRef', 'origSystemTelephoneRef', 'required|max_length[240]');
                $this->form_validation->set_rules('contactFirstName', 'contactFirstName', 'required|max_length[60]');
                $this->form_validation->set_rules('contactLastName', 'contactLastName', 'required|max_length[60]');
                //$this->form_validation->set_rules('telephoneType', 'telephoneType', 'required|max_length[30]|in_list[PHONE,FAX,EMAIL]');
                //$this->form_validation->set_rules('phoneCountryCode', 'phoneCountryCode', 'max_length[10]');
                //$this->form_validation->set_rules('telephoneAreaCode', 'telephoneAreaCode', 'max_length[10]');
                //$this->form_validation->set_rules('telephone', 'telephone', 'max_length[25]');
                //$this->form_validation->set_rules('telephoneExtension', 'telephoneExtension', 'max_length[20]');
                //$this->form_validation->set_rules('emailAddress', 'emailAddress', 'max_length[240]');
                //$this->form_validation->set_rules('jobTitle', 'jobTitle', 'max_length[30]');
                //$this->form_validation->set_rules('contactAttribute1', 'contactAttribute1', 'max_length[10]|in_list[EMAIL,PRINT]');
                //$this->form_validation->set_rules('orgName', 'orgName', 'required|max_length[60]');

                break;
        }

        // Todos los paises
        $telephoneType = $this->input->post('telephoneType');

        switch($telephoneType){

            case 'PHONE':
                $this->form_validation->set_rules('phoneCountryCode', 'phoneCountryCode', 'required|max_length[10]');
                $this->form_validation->set_rules('telephoneAreaCode', 'telephoneAreaCode', 'required|max_length[10]');
                $this->form_validation->set_rules('telephone', 'telephone', 'required|max_length[25]');
                break;
            case 'FAX':
                $this->form_validation->set_rules('phoneCountryCode', 'phoneCountryCode', 'required|max_length[10]');
                $this->form_validation->set_rules('telephoneAreaCode', 'telephoneAreaCode', 'required|max_length[10]');
                $this->form_validation->set_rules('telephone', 'telephone', 'required|max_length[25]');
                break;

            case 'EMAIL':
                $this->form_validation->set_rules('emailAddress', 'emailAddress', 'required|max_length[240]|valid_email');
                break;

            default:


        }

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence($country, $data);

        }else{

            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);
            $this->em->merge($vo);
            $this->em->flush();

            //Customers Header
            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getorigSystemCustomerRef()))->getSingleResult();
            $header->validateCrossRef($this->em);
            //*******//


            //Customers Address
            $header = $this->getHeaderQuery(self::HEADER2_CLASS_NAME,
                self::HEADER2_KEY_COLUMNS,
                array($vo->getorigSystemAdressReference()))->getSingleResult();

            $header->validateCrossRef($this->em);
            //*******//

            $this->em->flush();

            redirect(base_url() . self::CANCEL2 . $header->getCountry(). '/'.$header->getId());
        }
    }
}

