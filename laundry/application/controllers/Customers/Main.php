<?php
include_once APPPATH . 'controllers/MY_Controller.php';

/**
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/query-builder.html
 *
 * http://www.doctrine-project.org/api/orm/2.5/class-Doctrine.ORM.QueryBuilder.html
 *
 * Customers Header Controller
 *
 */

Use Doctrine\ORM\Query;


class Main extends MY_Controller {

    const VO_CLASS_NAME = 'Commons\\ORA\\CustomerHeader';
    const VO_PRE_CLASS_NAME = 'Commons\\RAW\\PreCustomerHeader';

    // Customer Address is both header and detail
    //
    const CHILD1_CLASS_NAME = 'Commons\\ORA\\CustomerAddress';
    const CHILD1_PRE_CLASS_NAME = 'Commons\\RAW\\PreCustomerAddress';
    const CHILD1_KEY_COLUMNS = array('origSystemCustomerRef');

    const CHILD2_CLASS_NAME = 'Commons\\ORA\\CustomerContact';
    const CHILD2_PRE_CLASS_NAME = 'Commons\\RAW\\PreCustomerContact';
    const CHILD2_KEY_COLUMNS = array('origSystemCustomerRef');

    const ACTION_CLEANSE = 'Customers/cleanseDirty/';
    const ACTION_CLEANSE_LABEL = 'Mark as Cleansed';

    const ACTION_BASE = 'Customers/updateCleansed/';
    const ACTION_CUSTOM = 'Customers/customUpdateCleansed/';
    const ACTION_LABEL = 'Update Customer';

    const CANCEL = "Customers/cleansed/";
    const CANCEL_DIRTY = "Customers/dirty/";
    const DISCARD = 'Customers/discardCleansed/';

    const LAYOUT_CLEANSED = array(
        'ORA/CustomerHeader/lstCleansed.tpl'
    );

    const LAYOUT_DIRTY = array(
        'ORA/CustomerHeader/lstDirty.tpl'
    );

    const LAYOUT_BASE = array(
        'ORA/CustomerHeader/topBarCleansed.tpl',
        'ORA/CustomerHeader/frmBase.tpl'
    );

    const LAYOUT_EDIT_DIRTY = array(
        'ORA/CustomerHeader/frmBase.tpl'
    );

    const LAYOUT_CUSTOM = array(
        'ORA/CustomerHeader/topBarCleansed.tpl',
        'ORA/CustomerHeader/frmCustom.tpl'
    );

    const LAYOUT_CUSTOM_CO = array(
        'ORA/CustomerHeader/topBarCleansed.tpl',
        'ORA/CustomerHeader/frmCustomCO.tpl'
    );

    const LAYOUT_CUSTOM_MX = array(
        'ORA/CustomerHeader/topBarCleansed.tpl',
        'ORA/CustomerHeader/frmCustomMX.tpl'
    );

    const DEFAULT_PA = [
        'frm' => array(
            'ORA/CustomerHeader/topBarCleansed.tpl',
            'ORA/CustomerHeader/frmCustom.tpl')
    ];

    const DEFAULT_CO = [
        'frm' => array(
            'ORA/CustomerHeader/topBarCleansed.tpl',
            'ORA/CustomerHeader/frmCustomCO.tpl')
    ];

    const DEFAULT_ALL = [
        'frm' => array(
            'ORA/CustomerHeader/topBarCleansed.tpl',
            'ORA/CustomerHeader/frmCustomALL.tpl')
    ];


    const VIEW_LAYOUTS = array(
        "PA" => self::DEFAULT_PA,
        "CO" => self::DEFAULT_ALL,
        "CR" => self::DEFAULT_ALL,
        "HN" => self::DEFAULT_ALL,
        "GT" => self::DEFAULT_ALL,
        "MX" => self::DEFAULT_ALL,
        "NI" => self::DEFAULT_ALL,
        "SV" => self::DEFAULT_ALL,
        "UY" => self::DEFAULT_ALL,
        "PE" => self::DEFAULT_ALL,
        "PY" => self::DEFAULT_ALL,
        "AR" => self::DEFAULT_ALL,
        "EC" => self::DEFAULT_ALL,
        "CL" => self::DEFAULT_ALL
    );



    public function __construct()
    {
        parent::__construct();

    }

    /**
     * @param $country
     *
     * lists all cleansed CustomerHeaders for $country
     *
     */

    public function listCleansed($country, $page=0, $sortCol=null, $sortAsc=null){
        if ($this->checkPreConditions($country)) {
            show_error('please contact your system administrator');
        }

        if (!isset($sortCol)){
            $sortCol = $this->session->selCol;
        }

        if (!isset($sortAsc)){
            $sortAsc = $this->session->selAsc;
        }

        switch($sortCol){
            case 'name':
                $data['cleansedVOs'] = $this->getCleansedHeader(self::VO_CLASS_NAME, $country, $page, 'customerName', $sortAsc==1)->getResult(Query::HYDRATE_ARRAY);
                break;
            case 'lastUpdated':
                $sortAsc = false;
                $this->session->selAsc = false;
                $data['cleansedVOs'] = $this->getCleansedHeader(self::VO_CLASS_NAME, $country, $page, 'lastUpdated', false)->getResult(Query::HYDRATE_ARRAY);
                break;
            case 'error':
                $this->session->selAsc = true;
                $sortAsc = true;
                $data['cleansedVOs'] = $this->getCleansedHeader(self::VO_CLASS_NAME, $country, $page, 'crossRefValidated', $sortAsc==1)->getResult(Query::HYDRATE_ARRAY);
                break;
            case 'id':
                $data['cleansedVOs'] = $this->getCleansedHeader(self::VO_CLASS_NAME, $country, $page, 'origSystemPartyRef', $sortAsc==1)->getResult(Query::HYDRATE_ARRAY);
                break;

            default:
                $data['cleansedVOs'] = $this->getCleansedHeader(self::VO_CLASS_NAME, $country, $page, 'lastUpdated', $sortAsc==1)->getResult(Query::HYDRATE_ARRAY);
                break;
        }

        $data['country'] = $country;
        $data['sortOrder'] = $sortCol;
        $data['sortDirection'] = $sortAsc==1?'ASC':'DESC';

        $this->session->selGroup='Customers';
        $this->session->selCountry=strtolower($country);

        $data = $this->loadCommonData($data,$country.' Cleansed Customers ', '', '', '' , '');

        $data = $this->addCleansedStats($data, self::VO_CLASS_NAME, $country, true);
        $data = $this->addPaginationMetadata($data, $country, $page, self::CANCEL);

        $this->displayStandardLayout(self::LAYOUT_CLEANSED, $data);
    }

    /**
     * @param $country
     *
     * lists all dirty CustomerHeaders for $country
     *
     */

    public function listDirty($country, $page = 0){
        if ($this->checkPreConditions($country)) {
            show_error('please contact your system administrator');
        }


        $data['dirtyVOs'] = $this->getDirtyHeader(self::VO_PRE_CLASS_NAME, $country, $page, 'customerName')->getResult(Query::HYDRATE_ARRAY);

        $data['country'] = $country;

        $this->session->selGroup='Customers';
        $this->session->selCountry=strtolower($country);

        $data = $this->loadCommonData($data,$country.' Dirty Customers ', '', '', '' , '');

        $data = $this->loadStats($data, self::VO_PRE_CLASS_NAME, self::VO_CLASS_NAME, $country);

        $data['total'] = $data['totalPreSTG'];
        $data = $this->addPaginationMetadata($data, $country, $page, self::CANCEL_DIRTY);

        $this->displayStandardLayout(self::LAYOUT_DIRTY, $data);
    }

    public function validate($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->validate($this->em);
        $this->em->flush();

        $this->validateChildren( self::CHILD1_CLASS_NAME, self::CHILD1_KEY_COLUMNS, array($vo->getOrigSystemCustomerRef()) );
        $this->validateChildren( self::CHILD2_CLASS_NAME, self::CHILD2_KEY_COLUMNS, array($vo->getOrigSystemCustomerRef()) );
        $this->em->flush();

        $vo->validateCrossRef($this->em);

        $this->em->flush();

        $this->customEditCleansed($vo->getCountry(), $id);
    }


    /**
     * @param $id
     *
     * opens the edit form for dirty CustomerHeader with $id
     *
     * all changes are ignored
     *
     * form submit is cleanseDirty($id)
     */

    public function editDirty($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }
        $data = $this->getVOHidratedByID(self::VO_PRE_CLASS_NAME,$id);

        $data = $this->loadCommonData(
            $data,
            'Edit dirty ' . $data['country'] . ' Customer #' . $id,
            self::ACTION_CLEANSE . $data['id'],
            self::ACTION_CLEANSE_LABEL,
            '',
            self::CANCEL_DIRTY.$data['country']
        );

        $this->displayStandardLayout(self::LAYOUT_EDIT_DIRTY, $data);
    }

    /**
     * @param $country
     * @param $id
     *
     * copies CustomerHeader from pre-stage to stage table
     *
     *
     */

    public function cleanseDirty($id){

        try{
            $qb = $this->em->createQueryBuilder();
            $qb->select('pch')
                ->from(self::VO_PRE_CLASS_NAME, 'pch')
                ->where($qb->expr()->eq('pch.id', '?1'))
                ->setParameter(1, $id);

            $pre = $qb->getQuery()->getSingleResult();

            if (!$this->checkPreConditionsForCleansingCustomer($pre)) {
                show_error('please check if there is already a Customer with ID '.$pre->getOrigSystemCustomerRef());
                return;
            }

            /**
             * cleanse parent - clone header from pre-stage to stage table
             *
             */

            $stg = self::VO_CLASS_NAME;
            $stg = new $stg;
            $stg = $this->cloneByReflection($pre, $stg);
            $stg->setDismissed(false);
            $stg->setCleansed(true);

            $this->em->persist($stg);
            $this->em->flush();

            $pre->setCleansed(true);
            $this->em->persist($pre);
            $this->em->flush();

            /**
             * cleanse children - clones dependencies from pre-stage to stage table
             *
             */

            $this->moveChildrenToStage( self::CHILD1_PRE_CLASS_NAME, self::CHILD1_CLASS_NAME, self::CHILD1_KEY_COLUMNS, array($stg->getOrigSystemCustomerRef()) );
            $this->moveChildrenToStage( self::CHILD2_PRE_CLASS_NAME, self::CHILD2_CLASS_NAME, self::CHILD2_KEY_COLUMNS, array($stg->getOrigSystemCustomerRef()) );

            $stg->validateCrossRef($this->em);
            $this->em->persist($stg);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "cleansing CustomerHeader ".$id." - ".$stg->getCustomerName(). " - ".$stg->getOrigSystemCustomerRef(), null, $stg->getCountry());

            $this->session->selGroup='Customers';
            $this->session->selCountry=strtolower($stg->getCountry());
            $this->session->selCol = "lastUpdated";
            $this->session->selAsc = false;

            $this->customEditCleansed($stg->getCountry(), $stg->getId());


        }catch(\Exception $e){
            show_error('something went wrong, please try to go back home' );

        }



    }

    public function toggle($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->setDismissed(!$vo->getDismissed());
        $this->em->merge($vo);
        $this->em->flush();

        $this->toggleChildren(self::CHILD1_CLASS_NAME,self::CHILD1_KEY_COLUMNS,  array($vo->getOrigSystemCustomerRef()),$vo->getDismissed());
        $this->toggleChildren(self::CHILD2_CLASS_NAME,self::CHILD2_KEY_COLUMNS,  array($vo->getOrigSystemCustomerRef()),$vo->getDismissed());

        $this->auditor->log("dummy", "toggling to". $vo->getDismissed() .  " CustomerHeader ".$id." - ".$vo->getCustomerName(). " - ".$vo->getOrigSystemCustomerRef(), null, $vo->getCountry());

        $this->session->selGroup='Customers';
        $this->session->selCountry=strtolower($vo->getCountry());
        $this->session->selCol = "lastUpdated";
        $this->session->selAsc = false;

        $this->customEditCleansed($vo->getCountry(), $vo->getId());
    }


    /**
     * @param $id
     *
     * edits cleansed CustomerHeader with ID  (formBase)
     *
     * editCleansed is paired with updateCleansed
     *
     */

    public function editCleansed($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence(null, $data);
    }

    public function customEditCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence($country, $data);
    }

    private function startEditSequence($country, $data){
        $actionCustomByCountry = self::ACTION_CUSTOM . $country.'/'. $data['id'];

        $data['addresses'] = $this->getChildQuery(self::CHILD1_CLASS_NAME,
            self::CHILD1_KEY_COLUMNS,
            array($data['origSystemCustomerRef']))->getResult(Query::HYDRATE_ARRAY);

        $data = $this->loadCommonData(
            $data,
            'Edit cleansed Customer Header #' . $data['id'],
            $actionCustomByCountry,
            self::ACTION_LABEL,
            self::DISCARD . $data['id'],
            self::CANCEL . $data['country']
        );

        $this->displayStandardLayout(self::VIEW_LAYOUTS[$country]['frm'], $data);
    }


    /**
     * @param $id
     *
     * updates cleansed CustomerHeader with ID (formBase)
     *
     * updateCleansed is paired with editCleansed
     *
     */
    public function updateCleansed($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->loadValidationRules(self::VO_CLASS_NAME, $id, $this->form_validation);

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence(null, $data);

        }else{

            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);

            $this->em->merge($vo);
            $this->em->flush();

            redirect(base_url() . self::CANCEL . $vo->getCountry());
        }
    }

    public function customUpdateCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        if ($data['dismissed']){
            $this->form_validation->set_rules('billToMigratedRef', 'billToMigratedRef', 'required|max_length[80]');
            $this->form_validation->set_rules('shipToMigratedRef', 'shipToMigratedRef', 'required|max_length[80]');

        }else{
            $this->form_validation->set_rules('customerName', 'customerName', 'required');

        }

        if ($this->form_validation->run() === FALSE){
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence($country, $data);

        }else{
            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);

            $vo->setOriginAll($this->input->post('originAll'));
            $vo->setNatureAll($this->input->post('natureAll'));
            $vo->setPersonCategoryPE($this->input->post('personCategoryPE'));

            if ($data['dismissed']){
                $vo->setBillToMigratedRef($this->input->post('billToMigratedRef'));
                $vo->setShipToMigratedRef($this->input->post('shipToMigratedRef'));
            }else{
                $vo->setCustomerName($this->input->post('customerName'));
            }

            $this->em->merge($vo);
            $this->em->flush();

            $vo->validateCrossRef($this->em, false);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "cleansing CustomerHeader ".$id." - ".$vo->getCustomerName(). " - ".$vo->getOrigSystemCustomerRef(), null, $vo->getCountry());

            $this->session->selGroup='Customers';
            $this->session->selCountry=strtolower($country);
            $this->session->selCol = "lastUpdated";
            $this->session->selAsc = false;


            redirect(base_url() . self::CANCEL . $vo->getCountry());
        }
    }

    /**
     * @param $id
     *
     * discards cleansed CustomerHeader with $id
     *
     */

    public function discard($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $stg = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $pre = $this->getPreVO(self::VO_PRE_CLASS_NAME, array('origSystemCustomerRef'), array($stg->getOrigSystemCustomerRef()));

        $this->em->remove($stg);
        $pre->setCleansed(false);
        $this->em->persist($pre);
        $this->em->flush();

        /**
         *
         * also discard children
         *
         */
        $this->discardChildren(self::CHILD1_PRE_CLASS_NAME, self::CHILD1_CLASS_NAME,self::CHILD1_KEY_COLUMNS, array($stg->getOrigSystemCustomerRef()));
        $this->discardChildren(self::CHILD2_PRE_CLASS_NAME, self::CHILD2_CLASS_NAME,self::CHILD2_KEY_COLUMNS, array($stg->getOrigSystemCustomerRef()));

        $this->auditor->log("dummy", "discarding Customer ".$id." - ".$stg->getOrigSystemCustomerRef(), null, $stg->getCountry());


        redirect(base_url() . 'Customers/cleansed/' . $stg->getCountry());

    }



}