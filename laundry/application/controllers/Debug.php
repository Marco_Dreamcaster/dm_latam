<?php
include_once APPPATH . 'controllers/MY_Controller.php';

Use Doctrine\ORM\Query;
Use Commons\AuditLog;

class Debug extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->getEntityManager();
        $this->auditor->setEntityManager($this->em);
        $this->load->helper('url_helper');
        $this->load->library('session');

    }


    public function displayDoc($locale='en')
    {
        $data = array();

        $data['locale'] = $locale;

        if (strtolower($locale)=='pt'){
            $this->load->view('docs/docs_pt', $data);
            return;
        }

        $this->load->view('docs/docs_en', $data);

    }

    public function index()
    {
        $queryBuilder = $this->em->createQueryBuilder();

        $queryBuilder->select('log')
            ->from('Commons\\AuditLog', 'log')
            ->orderBy('log.stamp', 'DESC')
            ->setMaxResults(100);

        $data['logs'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);

        $data['title'] = 'Audit, Debug, and Setup';
        $data = $this->loadSessionNavigation($data);
        $data['show'] = 'logs';

        $data['action'] = 'Debug/explode';

        $data['countries'] = array_keys(self::COUNTRY3_FOLDERS);

        $this->load->view('templates/header', $data);
        $this->smarty->view('debug.tpl', $data, true );
        $this->load->view('templates/footer', $data);
    }

    public function explode(){
        $data['title'] = 'Developer Debugging and Audting';
        $data = $this->loadSessionNavigation($data);

        $data['originalLine'] = $this->input->post('flatfileLine');

        $data['action'] = 'Debug/explode';
        $data['values'] = explode('|', $this->input->post('flatfileLine'));

        $this->load->view('templates/header', $data);
        $this->smarty->view('debugAnalyseFlatFileLine.tpl', $data, true );
        $this->load->view('templates/footer', $data);

    }

    public function debugForCountryGroupIDL($country, $group, $idl){
        $data['title'] = 'Developer Debugging and Audting';
        $data = $this->loadSessionNavigation($data);

        $data['originalLine'] = $this->input->post('flatfileLine');

        $queryBuilder = $this->em->createQueryBuilder();

        $queryBuilder->select('log')
            ->from('Commons\\AuditLog', 'log')
            ->andWhere( $queryBuilder->expr()->eq('log.country', '?1'))
            ->setParameter(1, $country)
            ->orderBy('log.stamp', 'DESC')
            ->setMaxResults(100);

        $data['logs'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);
        $data['title'] = 'Developer Debugging and Audting';
        $data['action'] = 'Debug/explode/'.$country."/".$group."/".$idl;
        $data['missingFields'] = false;
        $data['show'] = 'explode';

        $this->load->view('templates/header', $data);
        $this->smarty->view('debug.tpl', $data, true );
        $this->load->view('templates/footer', $data);

    }

    public function explodeForCountryGroupIDL($country, $groupShortName, $idlShortName){
        $data['title'] = 'Developer Debugging and Audting';
        $data = $this->loadSessionNavigation($data);

        $data['originalLine'] = $this->input->post('flatfileLine');

        $values = explode('|', $this->input->post('flatfileLine'));

        $idlGroupRepository = $this->em->getRepository('Commons\\IDL\IDLGroupDescriptor');
        $idlGroup = $idlGroupRepository->findOneBy(array('country' => $country, 'shortName' => $groupShortName));

        $idlRepository = $this->em->getRepository('Commons\\IDL\IDLDescriptor');
        $idl = $idlRepository->findOneBy(array('shortName' => $idlShortName, 'group' => $idlGroup->getId()));

        $queryBuilder = $this->em->createQueryBuilder();

        $queryBuilder->select('fd')
            ->from('Commons\\IDL\\IDLFieldDescriptor', 'fd')
            ->innerJoin('fd.descriptor', 'idl', 'WITH', 'idl.id = :idlID')
            ->where("fd.isOutputValue=true")
            ->orderBy("fd.outputPos", "ASC")
            ->setParameter("idlID", $idl->getId());

        $fields = $queryBuilder->getQuery()->getResult();

        $exploded = array();

        $index = 0;
        $missingfields = false;
        foreach($fields as $f){
            if (array_key_exists ($index, $values)){
                if ($values[$index]!=''){
                    $exploded[$f->getShortName()]= $values[$index];
                }else{
                    $exploded[$f->getShortName()]= '(empty)';
                }
            }else{
                $exploded[$f->getShortName()]= '(not present)';
                $missingfields = true;
            }
            $index = $index + 1;
        }

        $data['values'] = $exploded;
        $data['action'] = 'Debug/explode/'.$country."/".$groupShortName."/".$idlShortName;
        $data['missingFields'] = $missingfields;

        $this->load->view('templates/header', $data);
        $this->smarty->view('debugAnalyseFlatFileLine.tpl', $data, true );
        $this->load->view('templates/footer', $data);

    }


}