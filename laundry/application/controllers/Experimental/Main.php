<?php
include_once APPPATH . 'controllers/MY_Controller.php';

ini_set('max_execution_time', 0);
ini_set('memory_limit','2048M');

Use Doctrine\ORM\Query;

class Main extends MY_Controller
{
    protected $selCountry;
    protected $selGroup;
    protected $selEntity;
    protected $selOrder;
    protected $selAsc;

    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->getEntityManager();
        $this->load->helper('url_helper');
        $this->load->library('session');
        $this->load->library('markdown');
        $this->load->library('zip');
        $this->load->database();


    }

    public function index()
    {

    }

    public function auditCSVOutput(){
        $queryArray = array();

        $queryArray['auditVendors'] = "
            SELECT
                a.SEGMENT1 AS MIGRATION_CODE,
                a.CLEANSED AS STAGED,
                b.DISMISSED AS DISMISSED_RECORD,
                b.CROSSREF_VALIDATED AS VALIDATED,
                a.RECOMMENDED AS RFM,
                a.VENDOR_NAME AS VENDOR_NAME_SIGLA,
                b.VENDOR_NAME AS VENDOR_NAME_ORACLE,
                b.ALIASES,
                b.OBSERVATION,
                b.VAT_REGISTRATION_NUM,
                b.*
            FROM O_PRE_VENDOR_HEADER a
                LEFT JOIN O_STG_VENDOR_HEADER b ON a.SEGMENT1=b.SEGMENT1
            WHERE
                a.COUNTRY='PA'
            ORDER BY
                STAGED DESC,
                DISMISSED_RECORD ASC,
                VALIDATED DESC,
                RFM DESC,
                VENDOR_NAME_ORACLE ASC
	              ";

        $queryArray['auditVendorSites'] = "
            SELECT
                a.SEGMENT1 AS MIGRATION_CODE,
                a.CLEANSED AS STAGED,
                b.DISMISSED AS DISMISSED_RECORD,
                b.CROSSREF_VALIDATED AS VALIDATED,
                a.RECOMMENDED AS RFM,
                a.VENDOR_NAME AS VENDOR_NAME_SIGLA,
                b.VENDOR_NAME AS VENDOR_NAME_ORACLE,
                c.*
            FROM O_PRE_VENDOR_HEADER a
                LEFT JOIN O_STG_VENDOR_HEADER b ON a.SEGMENT1=b.SEGMENT1
                LEFT JOIN O_STG_VENDOR_SITE c ON a.SEGMENT1=c.VENDOR_NUMBER
            WHERE
                a.COUNTRY='PA'
            ORDER BY
                STAGED DESC,
                DISMISSED_RECORD ASC,
                VALIDATED DESC,
                RFM DESC,
                VENDOR_NAME_ORACLE ASC
	              ";

        $queryArray['auditVendorSiteContact'] = "
            SELECT
                a.SEGMENT1 AS MIGRATION_CODE,
                a.CLEANSED AS STAGED,
                b.DISMISSED AS DISMISSED_RECORD,
                b.CROSSREF_VALIDATED AS VALIDATED,
                a.RECOMMENDED AS RFM,
                a.VENDOR_NAME AS VENDOR_NAME_SIGLA,
                b.VENDOR_NAME AS VENDOR_NAME_ORACLE,
                c.*
            FROM O_PRE_VENDOR_HEADER a
                LEFT JOIN O_STG_VENDOR_HEADER b ON a.SEGMENT1=b.SEGMENT1
                LEFT JOIN O_STG_VENDOR_SITE_CONTACT c ON a.SEGMENT1=c.VENDOR_NUMBER
            WHERE
                a.COUNTRY='PA'
            ORDER BY
                STAGED DESC,
                DISMISSED_RECORD ASC,
                VALIDATED DESC,
                RFM DESC,
                VENDOR_NAME_ORACLE ASC
		              ";

        $this->serializeArray($queryArray, 'reports/audit', 'auditVendors');
    }

    private function serializeArray($queryArray, $outputFolder, $zipName){
        date_default_timezone_set('America/Sao_Paulo');
        $date = date('YmdHi', time());

        $timeStampedoutputFolder = IDL_TEMPLATES_AUTO_OUTPUT.$outputFolder.'/'.$date;

        if (!file_exists(IDL_TEMPLATES_AUTO_OUTPUT.$outputFolder)){
            mkdir(IDL_TEMPLATES_AUTO_OUTPUT.$outputFolder);

        }
        if (!file_exists($timeStampedoutputFolder)){
            mkdir($timeStampedoutputFolder);

        }

        $generatedPaths = array();

        foreach($queryArray as $file => $sql){
            $fileTemplate = $file."_%timestamp%".".csv";
            $fileTemplate = $timeStampedoutputFolder.'/'.str_replace("%timestamp%", $date, $fileTemplate);

            $resultArray = $this->db->query($sql)->result_array();

            if (sizeof($resultArray)>=1){
                $fp = fopen($fileTemplate, 'w');

                $header = array();
                foreach(array_keys($resultArray[0]) as $key) {
                    array_push($header, $key);
                }

                fputcsv($fp, $header);

                foreach($resultArray as $fields){
                    fputcsv($fp, $fields);

                }
                fclose($fp);

                array_push($generatedPaths, $fileTemplate);

            }

        }

        foreach($generatedPaths as $path){
            $this->zip->read_file($path);
        }

        $this->zip->download('TKE_LA_'.$zipName.'_'.$date.'.zip');
    }

}