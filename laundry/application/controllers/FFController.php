<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
include_once APPPATH . 'controllers/MY_Controller.php';

/**
*
 *  Serializers use Smarty templates to generate output
 *
 * https://www.codeigniter.com/userguide3/libraries/output.html
 *
 */

class FFController extends MY_Controller {

    const LAYOUT_INSPECT_TRIGGERED = array(
        'triggeredSQLCMD.tpl'
    );

    const LAYOUT_MANAGE_FF = array(
        'manageFF.tpl'
    );

    const LAYOUT_MANAGE_FF_BUNDLE_COUNTRY = array(
        'manageFFBundleCountry.tpl'
    );

    private $FF = array();
    private $placeholders = array();
    private $selected;


    public function __construct()
    {
        parent::__construct();

        $this->em = $this->doctrine->getEntityManager();
        $this->load->library('zip');
        $this->load->helper('xml');
        $this->load->helper('form');
        $this->load->database();

        $this->load->helper('url_helper');

        $this->auditor->setEntityManager($this->em);

        if (file_exists(APPPATH.'/config/FF.xml')) {
            $this->FF = json_decode(json_encode(simplexml_load_file(APPPATH.'/config/FF.xml')),true);
        }

        $this->placeholders = array('country', 'timestamp','cutoffDate','execType','bAccList');
    }


    /**
     * @param $bundle
     * @param $country
     * @param null $flatfile
     *
     * refereshes flat files declared under $bundle, $country
     *
     * alternatively refreshes only a single flatfile
     */

    public function refresh($bundle, $country, $flatfile = null){
        if (isset($flatfile)){
            // a flat file was specified
            $this->selectSingleWithPrecedenceRule($bundle, $country, $flatfile);

        }else{
            $this->selectMultiWithPrecedenceRule($bundle, $country);

        }

        // set PlaceHolder variables
        $data = $this->loadDataWithDefaultPlaceholders($bundle, $country);

        // perform refresh according to the strategy
        $data = $this->refreshBundles($data);

        // display debugInfo
        $data = $this->loadCommonData(
            $data,
            'Refresh of bundle ' . $data['bundle'],
            '',
            '',
            '',
            ''
        );

        $this->displayStandardLayout(self::LAYOUT_INSPECT_TRIGGERED, $data);

    }

    /**
     * @param $bundle
     * @param $country
     * @param $flatfile
     *
     * displays latest $bundle/$country/$flatfile
     *
     * also trims O4.Output folder, removes all older versions of the same flatfile
     *
     */

    public function inspect($bundle, $country, $flatfile){
        $this->selectSingleWithPrecedenceRule($bundle, $country, $flatfile);
        $data = $this->loadDataWithDefaultPlaceholders($bundle, $country);

        $latestFlatfilePaths = $this->matchLatestFlatfilePaths($data);

        $this->output->set_content_type('text/plain','UTF-8');

        if (sizeof($latestFlatfilePaths)==1){
            $handle = fopen($latestFlatfilePaths[0], "r");
            if ($handle) {
                while (($line = fgets($handle)) !== false) {
                    $this->output->append_output($line);
                }

                fclose($handle);
            }else{
                $this->output->append_output('handle to ' . $latestFlatfilePaths . " not available \r\n");

            }
        }else{
            $this->output->append_output('could not find a file for ' . $bundle . " ". $country ." ". $flatfile ." \r\n");
            $this->output->append_output('try to generate one with ' . base_url() . "RefreshFF/". $bundle. "/". $country ."/". $flatfile ." \r\n");

        }

    }

    /**
     * @param $bundle
     * @param $country
     *
     * downloads all flatfiles from $bundle/$country
     *
     */

    public function download($bundle,$country){
        $this->selectMultiWithPrecedenceRule($bundle, $country);
        $data = $this->loadDataWithDefaultPlaceholders($bundle, $country);

        $latestFlatFilePaths = $this->matchLatestFlatfilePaths($data);

        foreach($latestFlatFilePaths as $ff){
            $this->zip->read_file($ff);
        }

        date_default_timezone_set('America/Sao_Paulo');
        $this->zip->download('TKE_LA_'.$country.'_'.date('YmdHi', time()).'_'.$bundle.'.zip');


    }


    /**
     * @param null $bundle
     * @param null $country
     *
     * manages bundles
     *
     * ManageFF (displays all bundles)
     * ManageFF/Projects (displays all definitions for bundle Project)
     * ManageFF/Projects/CO (displays all definitions for bundle Project, customized for CO)
     */
    public function manage($bundle=null, $country=null)
    {

        $data = $this->loadDataWithDefaultPlaceholders($bundle,$country);

        if (($bundle == null)&&($country == null)) {
            $this->selectAllBundles();
            $data['bundles'] = $this->selected;
            $data['disabledCountries'] = [''];
            $data['disabledBundles'] = ['Repairs','Services'];

            $data = $this->loadCommonData(
                $data,
                'Manage all bundles ',
                '',
                '',
                '',
                ''
            );

            $this->displayStandardLayout(self::LAYOUT_MANAGE_FF, $data);

        }elseif (($bundle!=null) && ($country!=null)){
            $this->selectMultiWithPrecedenceRule($bundle, $country);
            $data['bundles'] = $this->selected;

            $data = $this->loadCommonData(
                $data,
                'Manage bundle '.$country.' '.$bundle,
                '',
                '',
                '',
                ''
            );

            $this->displayStandardLayout(self::LAYOUT_MANAGE_FF_BUNDLE_COUNTRY, $data);

        }



    }


    // end of public interface

    private function matchLatestFlatfilePaths($data){
        $latestFlatfilePaths = array();
        foreach($this->selected as $flatfileName=>$strategy) {

            reset($strategy);
            $first_key = key($strategy);

            if (isset($strategy[$first_key]['output'])) {
                $outputPath = $this->getScriptBaseLocation($strategy[$first_key]['output']['location']);

                $allFlatFiles = scandir($outputPath, 1);

                $regexFF = $this->replaceRegexPlaceHolders($strategy[$first_key]['output']['fileNameTemplate'], $data);
                $matchingFlatFilePaths = array();
                foreach($allFlatFiles as $ff){
                    if (preg_match($regexFF,$ff)){
                        array_push($matchingFlatFilePaths, $ff);
                    }
                }
                rsort($matchingFlatFilePaths);

                if (sizeof($matchingFlatFilePaths)>0){
                    for ($i=1;$i<sizeof($matchingFlatFilePaths);$i++){
                        unlink($outputPath.'/'.$matchingFlatFilePaths[$i]);
                    }

                    array_push($latestFlatfilePaths, $outputPath.'/'.$matchingFlatFilePaths[0]);
                }
            }

        }

        return $latestFlatfilePaths;

    }


    private function loadDataWithDefaultPlaceholders($bundle, $country){
        $data = array();
        date_default_timezone_set('America/Sao_Paulo');

        $qb = $this->em->createQueryBuilder();

        if ($country==null){
            $data['cutoffDate'] = '31-12-2018';

        }else{
            $qb->select('c')
                ->from('Commons\\MAPS\\GLConfigEstandar', 'c')
                ->where($qb->expr()->eq('c.country', '?1'))
                ->setParameter(1, $country);

            $config = $qb->getQuery()->getSingleResult();
            $data['cutoffDate'] = date_format($config->getCutoffDate(),"d-m-Y");
            $data['bAccList'] = $config->getBAccList();
            $data['rAccList'] = $config->getRAccList();

        }

        $data['bundle'] = $bundle;
        $data['country'] = $country;
        $data['country3'] = self::COUNTRY3_FOLDERS[$country];
        $data['timestamp'] = date('YmdHi', time());
        $data['execType'] = 1;
        $data['countries'] = array_keys(self::COUNTRY3_FOLDERS);

        if (isset($this->input)){
            $data = $this->loadPlaceholderValuesFromPost($this->input, $data);

        }

        return $data;

    }

    private function selectSingleWithPrecedenceRule($bundle, $country, $flatfile){
        $this->selected = array();

        if (isset($this->FF[$bundle][$country][$flatfile])){
            // gets bundle for specific country
            $this->mergeFlatfileName($this->FF[$bundle][$country][$flatfile], $flatfile);

        }else if (isset($this->FF[$bundle]['ALL'][$flatfile])) {
            // specific bundle not found, get generic 'ALL'
            $this->mergeFlatfileName($this->FF[$bundle]['ALL'][$flatfile], $flatfile);

        }
    }

    private function selectAllBundles()
    {
        $this->selected = $this->FF;
    }

    private function selectMultiWithPrecedenceRule($bundle, $country){
        $this->selected = array();

        if (isset($this->FF[$bundle]['ALL'])){
            // apply precedence rule

            foreach(array_keys($this->FF[$bundle]['ALL']) as $flatfile) {
                // if a bundle has been defined for a country, it gets loaded instead of generic 'ALL'

                if (isset($this->FF[$bundle][$country][$flatfile])){
                    // gets bundle for specific country
                    $this->mergeFlatfileName($this->FF[$bundle][$country][$flatfile], $flatfile);

                }elseif (isset($this->FF[$bundle]['ALL'][$flatfile])) {
                    // specific country bundle not found, get generic 'ALL'
                    $this->mergeFlatfileName($this->FF[$bundle]['ALL'][$flatfile], $flatfile);

                }
            }
        }


    }

    private function mergeFlatfileName($bundle, $flatfile){
        $selectedBundle = $bundle;
        reset($selectedBundle);
        $first_key = key($selectedBundle);

        $selectedBundle[$first_key]['flatfileName'] = $flatfile;
        array_push($this->selected, $selectedBundle);

    }


    /**
     * @param $data
     * @return mixed
     *
     * major switch for Refresh strategies
     *
     */

    private function refreshBundles($data){

        $data['flatfiles'] = array();

        foreach($this->selected as $bundle){
            $debugInfo = array();

            foreach(array_keys($bundle) as $strategy) {
                $debugInfo['strategy'] = $strategy;
                $debugInfo['flatfileName'] = $bundle[$strategy]['flatfileName'];

                switch ($strategy){
                    case 'syncWithTemplate':
                        $debugInfo = $this->generateSyncWithSmartyTemplate($bundle[$strategy], $data, $debugInfo);
                        break;

                    case 'asyncCMD':
                        $debugInfo = $this->generateAsyncCMD($bundle[$strategy], $data, $debugInfo);
                        break;

                    case 'asyncSQLCMD':
                        $debugInfo = $this->generateAsyncSQLCMD($bundle[$strategy], $data, $debugInfo);
                        break;

                    case 'asyncPrompt':
                        $debugInfo = $this->runAsyncPrompt($bundle[$strategy], $data, $debugInfo);
                        break;

                }

                array_push($data['flatfiles'], $debugInfo);
            }
        }

        return $data;
    }

    private function updateContextWithBundle($bundle, $data, $context){
        $context['outputLocation'] = $bundle['output']['location'];
        $context['outputFileName'] = $this->replaceStandardPlaceholders($bundle['output']['fileNameTemplate'], $data);
        $context['outputFileFullPath'] = $this->getScriptBaseLocation($context['outputLocation'], $data) . '/' . $context['outputFileName'];

        $context['cwd'] =  $this->getScriptBaseLocation('');

        if (isset($bundle['description'])){
            $context['description'] = $bundle['description'];
        }

        if (isset($bundle['output']['encodeAs'])){
            $context['encodeAs'] = $bundle['output']['encodeAs'];
        }

        if (isset($bundle['input'])){
            $context['inputLocation'] =  $this->replaceStandardPlaceholders($bundle['input']['location'], $data);
            $context['cwd'] =  $this->getScriptBaseLocation($context['inputLocation'], $data);
            $context['inputScriptFullPath'] = $context['cwd'] . '/' . $bundle['input']['script'];
            $context['sqlcmd'] =  $this->getBaseSQLCMD( $context['inputScriptFullPath']);

            if ($this->doctrine->getBaseDatos()=='DOCKER'){
                $context['sqlcmd'] = '/opt/mssql-tools/bin/'.$context['sqlcmd'];

                $varTemplate='export X=Y && ';

                foreach($this->placeholders as $phKey){
                    $varParam = str_replace('Y', $data[$phKey], str_replace('X', $phKey, $varTemplate));
                    $context['sqlcmd'] = $varParam.$context['sqlcmd'];
                }


            }else{
                foreach($this->placeholders as $phKey){
                    if ($phKey=='bAccList'){
                        $varTemplate=' -v X="Y"';

                    }else{
                        $varTemplate=' -v X=Y';

                    }

                    $varParam = str_replace('Y', $data[$phKey], str_replace('X', $phKey, $varTemplate));
                    $context['sqlcmd'] = $context['sqlcmd'].$varParam;
                }

            }


        }else if (isset($bundle['cmd'])){

            if ($this->doctrine->getBaseDatos()=='DOCKER'){
                $context['sqlcmd'] =  '/opt/mssql-tools/bin/'.$this->getBaseSQLCMD( null);

            }else{
                $context['sqlcmd'] =  $this->getBaseSQLCMD( null);

            }

            $context['sqlcmd'] = $context['sqlcmd'] . ' -Q "'. $context['cmd'] .'"';

        }else{
            throwException(new \Exception('bad configuration of FF.xml - missing input script or raw SQL command CMD'));
        }

        $context['sqlcmd'] = $context['sqlcmd'] . ' -o "'. $context['outputFileFullPath'] .'"';

        $context =  $this->encodeOutput($context);

        $context['cmdLine'] = $context['sqlcmd'];

        return $context;
    }

    private function spawnNewProcess($context){
        $descriptorspec = array(
            0 => array("pipe", "r"),  // stdin for worker
            1 => array("pipe", "w"),  // stdout for worker
        );

        $worker = proc_open($context['cmdLine'], $descriptorspec, $pipes, $context['cwd']);

        stream_set_blocking($pipes[0], 0);
        stream_set_blocking($pipes[1], 0);

        return $context;

    }

    private function encodeOutput($context){
        if ($this->doctrine->getBaseDatos()=='DOCKER') {
            if (!isset($context['encodeAs'])) {
                $context['sqlcmd'] = $context['sqlcmd'] . ' -h-1 -s"|" -W ';

            } else if ($context['encodeAs'] == 'CSV') {
                $context['sqlcmd'] = $context['sqlcmd'] . ' -s"|" -W ';

            } else {
                throwException(new \Exception('bad configuration of FF.xml - missing input script or raw SQL command CMD'));
            }
        }else{
            if (!isset($context['encodeAs'])) {
                $context['sqlcmd'] = $context['sqlcmd'] . ' -h-1 -s"|" -W -f 65001 ';

            } else if ($context['encodeAs'] == 'CSV') {
                $context['sqlcmd'] = $context['sqlcmd'] . ' -s"|" -W -f 65001 ';

            } else {
                throwException(new \Exception('bad configuration of FF.xml - missing input script or raw SQL command CMD'));
            }

        }

        return $context;

    }

    private function runAsyncPrompt($asyncBundle, $data, $context){
        $context = $this->updateContextWithBundle($asyncBundle, $data, $context);

        return $this->spawnNewProcess($context);

    }

    private function generateAsyncSQLCMD($asyncBundle, $data, $context){
        $context = $this->updateContextWithBundle($asyncBundle, $data, $context);

        return $this->spawnNewProcess($context);

    }


    private function generateAsyncCMD($asyncBundle, $data, $context){
        $context['cmd'] = $this->replaceStandardPlaceholders($asyncBundle['cmd'], $data);

        $context = $this->updateContextWithBundle($asyncBundle, $data, $context);

        return $this->spawnNewProcess($context);
    }


    private function generateSyncWithSmartyTemplate($smartyBundle, $data, $debugInfo){
        $debugInfo['template'] = $smartyBundle['template'];
        $debugInfo['outputLocation'] = $smartyBundle['output']['location'];

        // only cmd style is allowed for syncWIthSmartyTemplate
        //
        $debugInfo['cmd'] = $this->replaceStandardPlaceholders($smartyBundle['cmd'], $data);

        $stmt = $this->em->getConnection()->prepare($debugInfo['cmd']);
        $stmt->execute();

        // loads into memory
        $data['VOs'] = $stmt->fetchAll();

        $debugInfo['templateFullPath'] = $this->getTemplateFullPath($debugInfo['template']) ;

        $this->smarty->assign('rn', "\r\n");
        $content = $this->smarty->fetch($debugInfo['templateFullPath'], $data);
        $content = substr($content, 0, strlen($content) - 2);

        $debugInfo['outputFileName'] = $this->replaceStandardPlaceholders($smartyBundle['output']['fileNameTemplate'], $data);
        $debugInfo['outputFileFullPath'] = $this->getScriptBaseLocation($debugInfo['outputLocation'], $data) . '/' . $debugInfo['outputFileName'];

        $outputfile = fopen($debugInfo['outputFileFullPath'], "w") or die("Unable to open file!");
        fwrite($outputfile, utf8_encode($content));
        fclose($outputfile);

        return $debugInfo;
    }

    private function loadPlaceholderValuesFromPost($input, $data){
        if ($input->post('cutoffDate')!=null) {
            $data['cutoffDate'] = $input->post('cutoffDate');
        }

        if ($input->post('execType')!=null) {
            $data['execType'] = $input->post('execType');
        }

        return $data;
    }



    private function replaceStandardPlaceholders($var, $data){
        $result = $var;
        $result = str_replace('%country%',  $data['country'], $result);
        $result = str_replace('%country3%',  $data['country3'], $result);
        $result = str_replace('%timestamp%', $data['timestamp'], $result);
        $result = str_replace('%cutoffDate%', $data['cutoffDate'], $result);
        $result = str_replace('%execType%', $data['execType'], $result);

        return $result;
    }

    private function replaceRegexPlaceHolders($var, $data){
        $result = '/^'.$var;
        $result = str_replace('%country%', $data['country'], $result);
        $result = str_replace('%timestamp%', '[0-9]{12}', $result);
        $result = $result.'$/';

        return $result;
    }

    private function getScriptBaseLocation($location){
        $result = $this->doctrine->getScriptBaseTemplate();
        $result =  str_replace('%location%', $location, $result);

        return $result;
    }

    private function getTemplateFullPath($template){
        $result = IDL_TEMPLATES.trim(str_replace('\n','', $template));

        return $result;
    }

    private function  getBaseSQLCMD($scriptLocation){
        $result = $this->doctrine->getSqlCmdTemplate();
        if (isset($scriptLocation)){
            $result =  str_replace('%script%', $scriptLocation, $result);

        }else{
            $result =  str_replace('-i %script%', '', $result);

        }

        return $result;
    }

}
