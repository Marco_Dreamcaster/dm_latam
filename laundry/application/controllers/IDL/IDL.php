<?php
include_once APPPATH . 'controllers/MY_Controller.php';

Use Doctrine\ORM\Query;
Use Commons\IDL\IDLGroupDescriptor;
Use Commons\IDL\IDLDescriptor;
Use Commons\IDL\IDLFieldDescriptor;

class IDL extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->getEntityManager();
        $this->load->helper('url_helper');
        $this->load->library('session');

        if (!isset($this->session->sessionCountry)){
            $this->session->set_userdata(array('sessionCountry' => 'PA'));
        }

        if (!isset($this->session->sessionGroup)){
            $this->session->set_userdata(array('sessionGroup' => 'Customers'));
        }


    }

    public function index()
    {
        $data['title'] = 'IDL stats';
        $data = $this->loadSessionNavigation($data);

        $data['countries'] = $this->em->createQuery('SELECT COUNT(DISTINCT o.country) FROM Commons\\IDL\\IDLGroupDescriptor o')->getSingleScalarResult();
        $data['groups'] = $this->em->createQuery('SELECT COUNT(o) FROM Commons\\IDL\\IDLGroupDescriptor o')->getSingleScalarResult();
        $data['descriptors'] = $this->em->createQuery('SELECT COUNT(o) FROM Commons\\IDL\\IDLDescriptor o')->getSingleScalarResult();
        $data['fields'] = $this->em->createQuery('SELECT COUNT(o) FROM Commons\\IDL\\IDLFieldDescriptor o')->getSingleScalarResult();

        $this->load->view('templates/header', $data);
        $this->load->view('IDL/index', $data);
        $this->load->view('templates/footer', $data);
    }


    public function groups()
    {
        $data['title'] = 'IDL Groups';
        $data = $this->loadSessionNavigation($data);

        $qb = $this->em->createQueryBuilder();

        $qb->select('g')
            ->from('Commons\\IDL\\IDLGroupDescriptor', 'g')
            ->orderBy('g.country', 'ASC');

        $data['groups'] = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

        $data['display'] = array();
        foreach($data['groups'] as $group){
            if (!isset($data['display'][$group['shortName']])){
                $data['display'][$group['shortName']] = array();
            }

            if (!isset($data['display'][$group['shortName']][$group['country']])){
                $data['display'][$group['shortName']][$group['country']] = $group['id'];
            }

        }

        $this->load->view('templates/header', $data);
        $this->smarty->view('IDL/groups.tpl', $data, true );
        $this->load->view('templates/footer', $data);
    }

    public function descriptors()
    {
        $data['title'] = 'IDL Descriptors';
        $data = $this->loadSessionNavigation($data);

        $qb = $this->em->createQueryBuilder();

        $qb->select('d', 'ownerGroup')
            ->from('Commons\\IDL\\IDLDescriptor', 'd')
            ->leftJoin('d.group', 'ownerGroup')
            ->orderBy('ownerGroup.country', 'ASC')
            ->orderBy('ownerGroup.shortName', 'ASC');

        $data['descriptors'] = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

        $this->load->view('templates/header', $data);
        $this->smarty->view('IDL/descriptors.tpl', $data, true );
        $this->load->view('templates/footer', $data);
    }

    public function fields()
    {
        $data['title'] = 'IDL Field Descriptors';
        $data = $this->loadSessionNavigation($data);

        $qb = $this->em->createQueryBuilder();

        $qb->select('f', 'od', 'og')
            ->from('Commons\\IDL\\IDLFieldDescriptor', 'f')
            ->leftJoin('f.descriptor', 'od')
            ->leftJoin('od.group', 'og')
            ->orderBy('og.country', 'ASC')
            ->orderBy('od.shortName', 'ASC');


        $data['fields'] = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);


        $this->load->view('templates/header', $data);
        $this->smarty->view('IDL/fields.tpl', $data, true );
        $this->load->view('templates/footer', $data);
    }


    /**
     *
     *  creates
     *
     */

    public function createGroup()
    {
        $data['title'] = 'Create an IDL Group';
        $data = $this->loadSessionNavigation($data);

        $data['action'] = 'IDL/groups/new';
        $data['actionLabel'] = 'Create';

        $this->load->view('templates/header', $data);
        $this->smarty->view('IDL/Group/frmCustom.tpl', $data, true );
        $this->load->view('templates/footer', $data);
    }

    public function createDescriptor($groupId)
    {
        $data['title'] = 'Create a new IDL for Group # '.$groupId;
        $data = $this->loadSessionNavigation($data);

        $data['action'] = 'IDL/descriptors/new/'.$groupId;
        $data['actionLabel'] = 'Create';

        $data['group']['id'] = $groupId;

        $this->load->view('templates/header', $data);
        $this->smarty->view('IDL/Descriptor/frmCustom.tpl', $data, true );
        $this->load->view('templates/footer', $data);
    }



    public function createField($descriptorId)
    {
        $data['title'] = 'Create an IDL Field for Group # '.$descriptorId;
        $data = $this->loadSessionNavigation($data);

        $data['action'] = 'IDL/fields/new/'.$descriptorId;
        $data['actionLabel'] = 'Create';

        $data['field']['descriptor']['id'] = $descriptorId;

        $this->load->view('templates/header', $data);
        $this->smarty->view('IDL/Field/frmCustom.tpl', $data, true );
        $this->load->view('templates/footer', $data);
    }

    public function editGroup($id = null)
    {
        $data = $this->findGroupById($id);

        $data['title'] = 'Edit IDL Group #'.$id." ".$data['country']." ".$data['shortName'];
        $data = $this->loadSessionNavigation($data);

        $data['action'] = 'IDL/groups/update'."/".$data['id'];
        $data['actionLabel'] = 'Save Changes';

        $data['sessionCountry'] = strtolower($this->session->sessionCountry);
        $data['sessionGroup'] = ucfirst($this->session->sessionGroup);

        $data['cloneGroupTo'] = 'IDL/groups/clone/'.strtoupper($data['sessionCountry'])."/".$data['id'];
        $data['downloadAllTemplates'] = 'IDL/groups/downloadAllTemplates/'.strtoupper($data['country'])."/".$data['id'];

        $this->load->view('templates/header', $data);
        $this->smarty->view('IDL/Group/frmCustom.tpl', $data, true );
        $this->load->view('templates/footer', $data);
    }

    public function cloneGroupTo($groupId)
    {
        $country = null;

        foreach($_POST as $key=>$value){
            if (strpos($key, '_code') !== false) {
                $country = strtoupper($_POST[$key]);
                break;
            }
        }

        if (!isset($country)) {
            redirect(base_url() . "IDL/groups/");

        }else{

            $srcGroup = $this->findGroupById($groupId);
            // check if a group already exists at target country
            $qb = $this->em->createQueryBuilder();
            $qb->select('g')
                ->from('Commons\\IDL\\IDLGroupDescriptor', 'g')
                ->andWhere( $qb->expr()->eq('g.country', '?1'), $qb->expr()->eq('g.shortName',  '?2'))
                ->setParameter(1, $country)
                ->setParameter(2, $srcGroup['shortName']);


            $singleGroupFound = true;
            try{
                $targetGroup = $qb->getQuery()->getSingleResult();

            }catch(Exception $e){
                $singleGroupFound = false;
            }

            if ($singleGroupFound==true){
                redirect(base_url()."IDL/groups/edit/".$targetGroup->getId());

            }else{
                $qb = $this->em->createQueryBuilder();
                $qb->select('g')
                    ->from('Commons\\IDL\\IDLGroupDescriptor', 'g')
                    ->andWhere( $qb->expr()->eq('g.id', '?1'))
                    ->setParameter(1, $groupId);


                $srcGroup = $qb->getQuery()->getSingleResult();
                $newGroup = new IDLGroupDescriptor();

                $newGroup = $this->cloneByReflection($srcGroup,$newGroup);
                $newGroup->setCountry($country);

                $this->em->persist($newGroup);
                $this->em->flush();

                $srcDescriptors = $srcGroup->getDescriptors();

                foreach($srcDescriptors as $descriptor){
                    $newDescriptor = new IDLDescriptor();
                    $newDescriptor = $this->cloneByReflection($descriptor,$newDescriptor);
                    $newDescriptor->setGroup($newGroup);

                    $this->em->persist($newDescriptor);
                    $this->em->flush();

                    $srcFields = $descriptor->getFields();

                    foreach($srcFields as $field){
                        $newField = new IDLFieldDescriptor();
                        $newField = $this->cloneByReflection($field,$newField);
                        $newField->setDescriptor($newDescriptor);

                        $this->em->persist($newField);
                    }
                    $this->em->flush();

                }

                redirect(base_url()."IDL/groups/edit/".$newGroup->getId());


            }

        }

    }

    public function cloneToMissingCountries($id){
        $qb = $this->em->createQueryBuilder();

        $qb->select('f', 'od', 'og')
            ->from('Commons\\IDL\\IDLFieldDescriptor', 'f')
            ->leftJoin('f.descriptor', 'od')
            ->leftJoin('od.group', 'og')
            ->where( $qb->expr()->eq('f.id', $id))
            ->orderBy('og.country', 'ASC')
            ->orderBy('od.shortName', 'ASC');

        $data['templateField'] = $qb->getQuery()->getSingleResult(Query::HYDRATE_ARRAY);

        $qb = $this->em->createQueryBuilder();

        $qb->select('od', 'og')
            ->from('Commons\\IDL\\IDLDescriptor', 'od')
            ->leftJoin('od.group', 'og')
            ->where( $qb->expr()->eq('od.shortName', '?1'))
            ->andWhere( $qb->expr()->neq('og.country', '?2'))
            ->setParameter(1, $data['templateField']['descriptor']['shortName'])
            ->setParameter(2, $data['templateField']['descriptor']['group']['country'])
            ->orderBy('og.country', 'ASC');

        $data['similarDescriptors'] = $qb->getQuery()->getResult();

        foreach($data['similarDescriptors'] as $similarDescriptor){
            $fields = $similarDescriptor->getFields();

            $found = false;
            foreach($fields as $f){
                if ($f->getShortName()==$data['templateField']['shortName']){
                    $found = true;
                    break;
                }
            }

            if (!$found){
                $newField = new IDLFieldDescriptor();
                $newField->setColName($data['templateField']['colName']);
                $newField->setShortName($data['templateField']['shortName']);
                $newField->setORA_TYPE($data['templateField']['ORA_TYPE']);
                $newField->setDescription($data['templateField']['description']);
                $newField->setOutputPos($data['templateField']['outputPos']);
                $newField->setConstant($data['templateField']['constant']);
                $newField->setLOV($data['templateField']['LOV']);
                $newField->setIsMandatory($data['templateField']['isMandatory']);
                $newField->setIsOutputValue($data['templateField']['isOutputValue']);
                $newField->setDescriptor($similarDescriptor);

                $this->em->persist($newField);
                $this->em->flush();

            }


        }

        redirect(base_url()."IDL/fields/edit/".$data['templateField']['id']);



    }


    public function editDescriptor($id)
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select('d', 'og', 'fields')
            ->from('Commons\\IDL\\IDLDescriptor', 'd')
            ->leftJoin('d.group', 'og')
            ->leftJoin('d.fields', 'fields')
            ->where( $qb->expr()->eq('d.id', $id))
            ->addOrderBy('fields.isOutputValue','DESC')
            ->addOrderBy('fields.outputPos','ASC');


        $data = $qb->getQuery()->getSingleResult(Query::HYDRATE_ARRAY);

        $descriptor = $this->em->find('Commons\\IDL\\IDLDescriptor', $id);

        $data['country'] = $data['group']['country'];

        $data['stageTableName'] = $descriptor->getStageTableName();
        $data['preStageTableName'] = $descriptor->getPreStageTableName();

        $data['title'] = 'Edit Descriptor '.$data['group']['shortName']." - ".$data['shortName'];
        $data = $this->loadSessionNavigation($data);

        $data['action'] = 'IDL/descriptors/update'."/".$data['id'];
        $data['actionLabel'] = 'Save Changes';

        $this->load->view('templates/header', $data);
        $this->smarty->view('IDL/Descriptor/frmCustom.tpl', $data, true );
        $this->load->view('templates/footer', $data);
    }

    public function editField($id)
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select('f', 'od', 'og')
            ->from('Commons\\IDL\\IDLFieldDescriptor', 'f')
            ->leftJoin('f.descriptor', 'od')
            ->leftJoin('od.group', 'og')
            ->where( $qb->expr()->eq('f.id', $id))
            ->orderBy('og.country', 'ASC')
            ->orderBy('od.shortName', 'ASC');


        $data['field'] = $qb->getQuery()->getSingleResult(Query::HYDRATE_ARRAY);

        $data['country'] = $data['field']['descriptor']['group']['country'];

        $field = $this->em->find('Commons\\IDL\\IDLFieldDescriptor', $data['field']['id']);
        $data['field']['suggestedShortName'] = $field->getSuggestedShortName();

        $data['title'] = 'Edit IDL Field #'.$id.' - '.$data['field']['descriptor']['group']['shortName'].' - '.$data['field']['descriptor']['shortName'].' - '.$data['field']['shortName'];
        $data = $this->loadSessionNavigation($data);

        $data['action'] = 'IDL/fields/update'."/".$data['field']['id'];
        $data['actionLabel'] = 'Save Changes';

        $qb = $this->em->createQueryBuilder();

        $qb->select('f', 'od', 'og')
            ->from('Commons\\IDL\\IDLFieldDescriptor', 'f')
            ->leftJoin('f.descriptor', 'od')
            ->leftJoin('od.group', 'og')
            ->where( $qb->expr()->eq('od.shortName', '?1'))
            ->andWhere( $qb->expr()->eq('f.shortName', '?2'))
            ->andWhere( $qb->expr()->neq('og.country', '?3'))
            ->setParameter(1, $data['field']['descriptor']['shortName'])
            ->setParameter(2, $data['field']['shortName'])
            ->setParameter(3, $data['country'])
            ->orderBy('og.country', 'ASC');

        $data['similar'] = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

        $this->load->view('templates/header', $data);
        $this->smarty->view('IDL/Field/frmCustom.tpl', $data, true );
        $this->load->view('templates/footer', $data);
    }


    /**
     *
     *  new instance
     *
     */
    public function newGroup()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('country', 'Country', 'trim|required|in_list[NP,PA,UY,AR,PE,EC,PY,CH,BO,VE,GY,CO,MX,SV,NI,HN,GT,CR]');
        $this->form_validation->set_rules('shortName', 'Short Name', 'required');
        $this->form_validation->set_rules('outputPath', 'Output Path', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $data = $this->input->post(array('country', 'shortName'));

            $data['errorCountry'] = $this->form_validation->error('country',' ', ' ');
            $data['errorShortName'] = $this->form_validation->error('shortName');
            $data['errorOutputPath'] = $this->form_validation->error('outputPath');

            $data['title'] = 'Try again to create an IDL Group';
            $data = $this->loadSessionNavigation($data);

            $data['action'] = 'IDL/groups/new';
            $data['actionLabel'] = 'Create';

            $this->load->view('templates/header', $data);
            $this->smarty->view('IDL/Group/topBar.tpl', $data, true );
            $this->smarty->view('IDL/Group/frmCustom.tpl', $data, true );
            $this->load->view('templates/footer', $data);

        }
        else
        {
            $group = new IDLGroupDescriptor();
            $group->setCountry($this->input->post('country'));
            $group->setShortName($this->input->post('shortName'));
            $group->setOutputPath($this->input->post('outputPath'));

            $this->em->persist($group);
            $this->em->flush();

            redirect(base_url()."IDL/groups");
        }

    }

    public function newDescriptor($groupId)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('tableName', 'tableName', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $data = $this->input->post(array('shortName', 'tableName', 'outputPath', 'templateName', 'isTransactional', 'isHeader'));

            $data['errorTableName'] = $this->form_validation->error('tableName');

            $data['title'] = 'Try again to create an IDL Descriptor';
            $data = $this->loadSessionNavigation($data);

            $data['action'] = 'IDL/descriptors/new/'.$groupId;
            $data['actionLabel'] = 'Create';

            $data['group']['id'] = $groupId;

            $this->load->view('templates/header', $data);
            $this->smarty->view('IDL/Descriptor/frmCustom.tpl', $data, true );
            $this->load->view('templates/footer', $data);

        }
        else
        {
            $idl = new IDLDescriptor();
            $idl->setTableName($this->input->post('tableName'));

            if ($this->input->post('shortName')==''){
                $idl->setShortName($idl->getSuggestedShortName());
            }else{
                $idl->setShortName($this->input->post('shortName'));
            }

            if ($this->input->post('templateName')==''){
                $idl->setTemplateName($idl->getSuggestedTemplateName());
            }else{
                $idl->setTemplateName($this->input->post('templateName'));
            }

            $isHeader = $this->input->post('isHeader');
            if (isset($isHeader)) {
                $idl->setIsHeader(true);
            } else {
                $idl->setIsHeader(false);
            }

            $isTransactional = $this->input->post('isTransactional');
            if (isset($isTransactional)) {
                $idl->setIsTransactional(true);
            } else {
                $idl->setIsTransactional(false);
            }

            $qb = $this->em->createQueryBuilder();
            $qb->select('g')
                ->from('Commons\\IDL\\IDLGroupDescriptor', 'g')
                ->where($qb->expr()->eq('g.id', '?1'))
                ->setParameter(1, $groupId);

            $owner = $qb->getQuery()->getSingleResult();
            $idl->setGroup($owner);

            $this->em->persist($idl);
            $this->em->flush();

            redirect(base_url()."IDL/groups/edit/".$groupId);
        }

    }

    public function newField($idlId)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('colName', 'Column Name', 'required|max_length[50]');
        $this->form_validation->set_rules('shortName', 'Short Name', 'required|max_length[50]');
        $this->form_validation->set_rules('ORA_TYPE', 'ORA_TYPE', 'required|max_length[50]');
        $this->form_validation->set_rules('description', 'Description', 'max_length[2048]');
        $this->form_validation->set_rules('outputPos', 'Output Pos', 'is_natural');

        if ($this->form_validation->run() === FALSE)
        {
            $data = $this->input->post(array('shortName', 'colName', 'ORA_TYPE', 'outpuPos', 'isOutputValue', 'isMandatory', 'LOV', 'description'));

            $data['errorColName'] = $this->form_validation->error('colName');
            $data['errorORA_TYPE'] = $this->form_validation->error('ORA_TYPE');
            $data['errorDescription'] = $this->form_validation->error('description');
            $data['outputPos'] = $this->form_validation->error('outputPos');

            $data['title'] = 'Try again to create an IDL Group';
            $data = $this->loadSessionNavigation($data);

            $data['action'] = 'IDL/fields/new/'.$idlId;
            $data['actionLabel'] = 'Create';

            $data['field']['descriptor']['id'] = $idlId;

            $this->load->view('templates/header', $data);
            $this->smarty->view('IDL/Field/frmCustom.tpl', $data, true );
            $this->load->view('templates/footer', $data);

        }
        else
        {
            $field = new IDLFieldDescriptor();
            $field->setColName($this->input->post('colName'));
            $field->setORA_TYPE($this->input->post('ORA_TYPE'));
            $field->setShortName($this->input->post('shortName'));
            $field->setDescription($this->input->post('description'));
            $field->setOutputPos($this->input->post('outputPos'));

            $encodedValue =  trim(mb_strtoupper(mb_convert_encoding($this->input->post('constant'), 'ISO-8859-1', 'UTF-8'), 'ISO-8859-1'));
            $field->setConstant($encodedValue);
            $field->setLOV($this->input->post('LOV'));
            $field->setShortName($this->input->post('shortName'));

            $isMandatory = $this->input->post('isMandatory');
            if (isset($isMandatory)) {
                $field->setIsMandatory(true);
            } else {
                $field->setIsMandatory(false);
            }

            $isOutputValue = $this->input->post('isOutputValue');
            if (isset($isOutputValue)) {
                $field->setIsOutputValue(true);
            } else {
                $field->setIsOutputValue(false);
            }

            $qb = $this->em->createQueryBuilder();
            $qb->select('d')
                ->from('Commons\\IDL\\IDLDescriptor', 'd')
                ->where($qb->expr()->eq('d.id', '?1'))
                ->setParameter(1, $idlId);

            $owner = $qb->getQuery()->getSingleResult();
            $field->setDescriptor($owner);

            $field->calculateFormRules();


            $this->em->persist($field);
            $this->em->flush();

            redirect(base_url()."IDL/descriptors/edit/".$idlId);
        }

    }


    /**
     *  updates
     *
     */

    public function updateGroup($id = null)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('country', 'Country', 'trim|required|in_list[NP,PA,UY,AR,PE,EC,PY,CH,BO,VE,GY,CO,MX,SV,NI,GT,CR,HN]');
        $this->form_validation->set_rules('shortName', 'Short Name', 'required');
        $this->form_validation->set_rules('outputPath', 'Output Path', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $data = $this->input->post(array('country', 'shortName', 'outputPath'));

            $data['title'] = 'Try again to update an IDL Group';
            $data = $this->loadSessionNavigation($data);

            $data['action'] = 'IDL/groups/update/'.$id;
            $data['actionLabel'] = 'Save Changes';

            $data['errorCountry'] = $this->form_validation->error('country',' ', ' ');
            $data['errorShortName'] = $this->form_validation->error('shortName');
            $data['errorOutputPath'] = $this->form_validation->error('outputPath');

            $this->load->view('templates/header', $data);
            $this->smarty->view('IDL/Group/topBar.tpl', $data, true );
            $this->smarty->view('IDL/Group/frmCustom.tpl', $data, true );
            $this->load->view('templates/footer', $data);

        }
        else
        {
            $group = $this->em->find('Commons\\IDL\\IDLGroupDescriptor', $id);
            $group->setCountry($this->input->post('country'));
            $group->setShortName($this->input->post('shortName'));
            $group->setOutputPath($this->input->post('outputPath'));

            $this->em->merge($group);
            $this->em->flush();

            redirect(base_url()."IDL/groups");
        }
    }


    public function updateField($id = null)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('shortName', 'Short Name', 'required|max_length[50]');
        $this->form_validation->set_rules('colName', 'Column Name', 'required|max_length[50]');
        $this->form_validation->set_rules('ORA_TYPE', 'ORA_TYPE', 'required|max_length[50]');
        $this->form_validation->set_rules('description', 'Description', 'max_length[2048]');
        $this->form_validation->set_rules('outputPos', 'Output Pos', 'is_natural');

        if ($this->form_validation->run() === FALSE)
        {
            $data['field'] = $this->input->post(array('shortName', 'colName', 'isMandatory', 'ORA_TYPE', 'description', 'isOutputValue', 'LOV'));

            $data['title'] = 'Try again to update an IDL Field';
            $data = $this->loadSessionNavigation($data);

            $data['action'] = 'IDL/fields/update/'.$id;
            $data['actionLabel'] = 'Save Changes';

            $data['errorShortName'] = $this->form_validation->error('shortName');
            $data['errorColName'] = $this->form_validation->error('colName');
            $data['errorORA_TYPE'] = $this->form_validation->error('ORA_TYPE');
            $data['errorDescription'] = $this->form_validation->error('description');
            $data['outputPos'] = $this->form_validation->error('outputPos');

            $this->load->view('templates/header', $data);
            $this->smarty->view('IDL/Field/topBar.tpl', $data, true );
            $this->smarty->view('IDL/Field/frmCustom.tpl', $data, true );
            $this->load->view('templates/footer', $data);

        }
        else
        {
            $field = $this->em->find('Commons\\IDL\\IDLFieldDescriptor', $id);

            $field->setShortName($this->input->post('shortName'));
            $field->setColName($this->input->post('colName'));
            $field->setORA_TYPE(str_replace(' ', '', $this->input->post('ORA_TYPE')));
            $field->setLOV($this->input->post('LOV'));
            $field->setDescription($this->input->post('description'));
            $encodedValue =  trim(mb_convert_encoding($this->input->post('constant'), 'ISO-8859-1', 'UTF-8'), 'ISO-8859-1');
            $field->setConstant($encodedValue);
            $isOutputValue = $this->input->post('isOutputValue');
            if (isset($isOutputValue)){
                $field->setIsOutputValue(true);

            }else{
                $field->setIsOutputValue(false);

            }
            $field->setOutputPos($this->input->post('outputPos'));
            $isMandatory = $this->input->post('isMandatory');
            if (isset($isMandatory)){
                $field->setIsMandatory(true);

            }else{
                $field->setIsMandatory(false);

            }

            $isQueryable = $this->input->post('isQueryable');
            if (isset($isQueryable)){
                $field->setIsQueryable(true);

            }else{
                $field->setIsQueryable(false);

            }

            $isSortable = $this->input->post('isSortable');
            if (isset($isSortable)){
                $field->setIsSortable(true);

            }else{
                $field->setIsSortable(false);

            }

            $field->calculateFormRules();

            $this->em->merge($field);
            $this->em->flush();

            redirect(base_url()."IDL/descriptors/edit/".$field->getDescriptor()->getId());
        }
    }


    public function updateDescriptor($id = null)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('shortName', 'Short Name', 'required|max_length[50]');
        $this->form_validation->set_rules('tableName', 'Table Name', 'required|max_length[50]');
        $this->form_validation->set_rules('templateName', 'Template Name', 'required|max_length[50]');

        if ($this->form_validation->run() === FALSE)
        {
            $data['field'] = $this->input->post(array('shortName', 'tableName', 'templateName', 'isTransactional', 'isHeader'));

            $data['title'] = 'Try again to update an IDL';
            $data = $this->loadSessionNavigation($data);

            $data['action'] = 'IDL/descriptors/update/'.$id;
            $data['actionLabel'] = 'Save Changes';

            $data['errorShortName'] = $this->form_validation->error('shortName');
            $data['errorTableName'] = $this->form_validation->error('tableName');
            $data['errorTemplateName'] = $this->form_validation->error('templateName');

            $this->load->view('templates/header', $data);
            $this->smarty->view('IDL/Descriptor/topBar.tpl', $data, true );
            $this->smarty->view('IDL/Descriptor/frmCustom.tpl', $data, true );
            $this->load->view('templates/footer', $data);

        }
        else
        {
            $idl = $this->em->find('Commons\\IDL\\IDLDescriptor', $id);
            $idl->setShortName($this->input->post('shortName'));
            $idl->setTemplateName($this->input->post('templateName'));
            $idl->setTableName($this->input->post('tableName'));

            $isTransactional = $this->input->post('isTransactional');
            $idl->setIsTransactional((isset($isTransactional)));

            $isHeader = $this->input->post('isHeader');
            $idl->setIsHeader((isset($isHeader)));

            $this->em->merge($idl);
            $this->em->flush();


            redirect(base_url()."IDL/groups/edit/".$idl->getGroup()->getId());
        }
    }

    /**
     *
     * deletes
     *
     */

    public function deleteGroup($id = null)
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select('g', 'descriptors')
            ->from('Commons\\IDL\\IDLGroupDescriptor', 'g')
            ->leftJoin('g.descriptors', 'descriptors')
            ->where('g.id = ?1')
            ->setParameter(1, $id);

        $group = $qb->getQuery()->getSingleResult();
        if (isset($group)){
            foreach($group->getDescriptors() as $descriptor){
                foreach($descriptor->getFields() as $field){
                    $this->em->remove($field);
                    $this->em->flush();
                }

                $this->em->remove($descriptor);
                $this->em->flush();

            }
            $this->em->remove($group);
            $this->em->flush();
        }

        redirect(base_url()."IDL/groups");
    }

    public function deleteField($id = null)
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select('f', 'od', 'og')
            ->from('Commons\\IDL\\IDLFieldDescriptor', 'f')
            ->leftJoin('f.descriptor', 'od')
            ->leftJoin('od.group', 'og')
            ->where( $qb->expr()->eq('f.id', $id))
            ->orderBy('og.country', 'ASC')
            ->orderBy('od.shortName', 'ASC');


        $field = $qb->getQuery()->getSingleResult();

        $this->em->remove($field);
        $this->em->flush();

        redirect(base_url()."IDL/descriptors/edit/".$field->getDescriptor()->getId());
    }

    private function findGroupById($id = null){
        if ($id==null){
            show_404();
        }

        $queryBuilder = $this->em->createQueryBuilder();

        $queryBuilder->select('g', 'descriptors')
            ->from('Commons\\IDL\\IDLGroupDescriptor', 'g')
            ->leftJoin('g.descriptors', 'descriptors')
            ->where('g.id = ?1')
            ->orderBy('g.shortName', 'ASC')
            ->setParameter(1, $id);

        $data = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY)[0];

        if (empty($data)){
            show_404();
        }

        return $data;
    }


}