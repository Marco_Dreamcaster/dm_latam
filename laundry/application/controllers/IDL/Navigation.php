<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
include_once APPPATH . 'controllers/MY_Controller.php';
/**
*
 *  Serializers use Smarty templates to generate output
 *
 * https://www.codeigniter.com/userguide3/libraries/output.html
 *
 */
Use Doctrine\ORM\Query;
Use Commons\IDL\IDLGroupDescriptor;

class Navigation extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->em = $this->doctrine->getEntityManager();
        $this->load->library('zip');
        $this->load->helper('url_helper');
   }

    public function navigateGroup($country, $groupShortName){
        $idlGroupRepository = $this->em->getRepository('Commons\\IDL\\IDLGroupDescriptor');
        $idlGroup = $idlGroupRepository->findOneBy(array('country' => $country, 'shortName' => $groupShortName));

        redirect(base_url()."IDL/groups/edit/".$idlGroup->getId());
    }


    public function navigate($country, $groupShortName, $idlShortName){
        $idlGroupRepository = $this->em->getRepository('Commons\\IDL\\IDLGroupDescriptor');
        $idlGroup = $idlGroupRepository->findOneBy(array('country' => $country, 'shortName' => $groupShortName));

        $idlRepository = $this->em->getRepository('Commons\\IDL\\IDLDescriptor');
        $idl = $idlRepository->findOneBy(array('shortName' => $idlShortName, 'group' => $idlGroup->getId()));


        redirect(base_url()."IDL/descriptors/edit/".$idl->getId());
    }

    public function getJsonSampleKPI($country){

        $collectionKPI = ['CustomerHeader'];

        foreach($collectionKPI as $kpi){
            $qb = $this->em->createQueryBuilder();
            $qb->resetDQLPart('where');

            $qb->select('*')
                ->from('Commons\\AuditLog', 'log')
                ->andWhere(
                    $qb->expr()->eq('log.country', '?1'),
                    $qb->expr()->orX(
                        $qb->expr()->like('log.event', '?2')
                    )
                )
                ->setParameter(1, $country)
                ->setParameter(2, 'sampleKPI %'.$kpi.'%')
                ->addOrderBy('log.stamp','DESC')
                ->setMaxResults(1);


            try{
                $data = $qb->getQuery()->getSingleResult(Query::HYDRATE_ARRAY);

            }catch (Exception $e){
                $data = ['error'=>'no KPIs found for '.$kpi];
            }

        }

        //add the header here
        header('Content-Type: application/json');
        echo json_encode( $data );







    }


}
