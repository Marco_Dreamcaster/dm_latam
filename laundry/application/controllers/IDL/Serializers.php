<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
include_once APPPATH . 'controllers/MY_Controller.php';

/**
*
 *  Serializers use Smarty templates to generate output
 *
 * https://www.codeigniter.com/userguide3/libraries/output.html
 *
 */
Use Doctrine\ORM\Query;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
Use Commons\IDL\IDLGroupDescriptor;

class Serializers extends MY_Controller {


    const LAYOUT_INSPECT = array(
        'inspectSQLCMD.tpl'
    );

    const LAYOUT_INSPECT_TRIGGERED = array(
        'triggeredSQLCMD.tpl'
    );

    private $setupQueries = array();
    private $scriptLocations = array();

    private $selectsByCountry = array();
    private $bundledQueries = array();

    private $outputPaths =[
        'PA'=>'01.PAN',
        'CO'=>'02.COL',
        'MX'=>'03.MEX',
        'SV'=>'04.SVL',
        'NI'=>'NI.SVL',
        'HV'=>'06.HND',
        'GT'=>'07.GTM',
        'CR'=>'08.CRI'
    ];

    private $scriptLocationFiles =[
        'loadCustomer'=>'/config/namedQueries/load/loadCustomer.xml',
        'loadInventory'=>'/config/namedQueries/load/loadInventory.xml',
        'setupInventory'=>'/config/namedQueries/setup/setupInventory.xml',
        'setupServices'=>'/config/namedQueries/setup/setupServices.xml',
        'setupProjects'=>'/config/namedQueries/setup/setupProjects.xml',
        'setupRepairs'=>'/config/namedQueries/setup/setupRepairs.xml',
        'setupCustomers'=>'/config/namedQueries/setup/setupCustomers.xml',
        'setupVendors'=>'/config/namedQueries/setup/setupVendors.xml',
        'setupAP'=>'/config/namedQueries/setup/setupAP.xml',
        'setupAR'=>'/config/namedQueries/setup/setupAR.xml',
        'setupPO'=>'/config/namedQueries/setup/setupPO.xml',
        'setupGL'=>'/config/namedQueries/setup/setupGL.xml'
    ];

    private $setupFiles =[
        'Projects'=>'/config/namedQueries/setup/setupProjects.xml',
        'Services'=>'/config/namedQueries/setup/setupServices.xml',
        'Customers'=>'/config/namedQueries/setup/setupCustomers.xml',
        'Vendors'=>'/config/namedQueries/setup/setupVendors.xml',
        'Inventory'=>'/config/namedQueries/setup/setupInventory.xml',

    ];

    public function __construct()
    {
        parent::__construct();

        $this->em = $this->doctrine->getEntityManager();
        $this->load->library('zip');
        $this->load->helper('xml');
        $this->load->helper('form');
        $this->load->database();

        $this->load->helper('url_helper');

        $this->auditor->setEntityManager($this->em);


        if (file_exists(APPPATH.'/config/namedQueries/selectsByCountry.xml')) {
            $this->selectsByCountry = json_decode(json_encode(simplexml_load_file(APPPATH.'/config/namedQueries/selectsByCountry.xml')),true);
        }

        if (file_exists(APPPATH.'/config/namedQueries/bundledQueries.xml')) {
            $this->bundledQueries = json_decode(json_encode(simplexml_load_file(APPPATH.'/config/namedQueries/bundledQueries.xml')),true);
        }

        foreach ($this->setupFiles as $key=>$value){
            $this->parseSetupFile($key,$value);
        }

        foreach ($this->scriptLocationFiles as $key=>$value){
            $this->parseScriptLocationFile($key,$value);
        }

    }

    private function parseSetupFile($key, $file){
        if (file_exists(APPPATH.$file)) {
            $setup = json_decode(json_encode(simplexml_load_file(APPPATH.$file, null, LIBXML_NOCDATA)),true);
            $this->setupQueries[$key] = $setup;
        }

    }

    private function parseScriptLocationFile($key, $file){
        if (file_exists(APPPATH.$file)) {
            $load = json_decode(json_encode(simplexml_load_file(APPPATH.$file, null, LIBXML_NOCDATA)),true);
            $this->scriptLocations[$key] = $load;
        }

    }

    public function index()
    {
        // Some example data
        $data['title'] = "The Smarty parser works!";
        $data['body']  = "This is body text to show that the Smarty Parser works!  By default, the output is generated directly to the response.";

        $this->smarty->assign('rn', "\r\n");
        $this->smarty->assign('data', $data);

        $this->smarty->view('info.tpl',$data , false);
    }

    private function getFlatFileContent($country, $groupShortName, $idlShortName){
        $idlGroupRepository = $this->em->getRepository('Commons\\IDL\\IDLGroupDescriptor');
        $idlGroup = $idlGroupRepository->findOneBy(array('country' => $country, 'shortName' => $groupShortName));

        $idlRepository = $this->em->getRepository('Commons\\IDL\\IDLDescriptor');
        $idl = $idlRepository->findOneBy(array('shortName' => $idlShortName, 'group' => $idlGroup->getId()));

        $voFullClassName = "Commons\\ORA\\".$idlShortName;

        $queryBuilder = $this->em->createQueryBuilder();

        $queryBuilder->select('VO')
            ->from($voFullClassName, 'VO')
            ->andWhere(
                $queryBuilder->expr()->eq('VO.dismissed', 'false'),
                $queryBuilder->expr()->eq('VO.locallyValidated', 'true'),
                $queryBuilder->expr()->eq('VO.crossRefValidated', 'true'),
                $queryBuilder->expr()->eq('VO.country', '?1')
                )
            ->setParameter(1, $country);

        $data['VOs'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);

        $templateFullPath = IDL_TEMPLATES.'flatfiles/'.$country.'/minified/'.$idlShortName.'.min';

        $this->smarty->assign('rn', "\r\n");
        $content =  $this->smarty->fetch($templateFullPath, $data);
        $content = substr($content, 0, strlen($content)-2);

        return $content;

    }

    /** marked for deletion */
    public function generateFlatFiles($country = null, $groupShortName = null, $idlShortName = null, $direct = true)
    {
        $messages = array("generating flat files for ".$country."/".$groupShortName."/".$idlShortName." @ ".IDL_TEMPLATES_AUTO_OUTPUT );

        $content = $this->getFlatFileContent($country, $groupShortName, $idlShortName);

        date_default_timezone_set('America/Sao_Paulo');
        $date = date('Ymd_His', time());

        $fileTemplate = $idlShortName."_%timestamp%_".".txt";
        $fileTemplate = str_replace("%timestamp%", $date, $fileTemplate);

        $outputPath = IDL_TEMPLATES_AUTO_OUTPUT.$fileTemplate;

        $outputfile = fopen($outputPath, "w") or die("Unable to open file!");

        fwrite($outputfile, utf8_encode ($content));
        fclose($outputfile);

        if ($direct){
            $this->output
                ->set_content_type('text/plain','ANSI')
                ->set_output($content);

        }else {
            return $messages;
        }
    }


    public function generateNamedQuery($title, $country ='PA', $alsoGenerateExcel=false){
        if (isset($this->selectsByCountry[$title])){
            foreach($this->selectsByCountry[$title] as $file=>$query){
                $this->selectsByCountry[$title][$file]['params']= array($country);
            }
            $this->serializeNamedQueriesToZippedBundle($this->selectsByCountry[$title], 'reports/'.$title, $title.'_'.$country, $country, $alsoGenerateExcel);
        }
    }

    /** BRAVE NEW WORLD **/

    public function generateGalleryZipFile(){
        $resourcesFolder = $this->doctrine->getItemFilesBaseTemplate() . '/resources/';

        date_default_timezone_set('America/Sao_Paulo');
        $date = date('YmdHi', time());

        $destination = 'TKE_LA_Gallery_'.$date.'.zip';

        $this->zip->read_dir($resourcesFolder,FALSE);
        ob_end_clean();
        $this->zip->download($destination);

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "generate Gallery zip for download", null, 'ZZ');
    }

    public function runSetup($title, $country=null){
        if (isset($this->setupQueries[$title])){
            foreach($this->setupQueries[$title] as $key=>$sequence) {
                foreach($sequence['step'] as $cmd){
                    $stmt = $this->em->getConnection()->prepare($cmd);
                    $stmt->execute();

                }

                $this->auditor->log($_SERVER['REMOTE_ADDR'], "running setup for ".$title.' - '.$key, null, $country==null?$country:'ALL');
            }

        }
        redirect(base_url() . "Debug");

    }

    public function runSQLCMD($title, $country='ZZ'){
        if (isset($this->scriptLocations[$title])){
            foreach($this->scriptLocations[$title] as $key=>$locationInfo) {
                $this->auditor->log($_SERVER['REMOTE_ADDR'], "running setup for ".$title.' - '.$key, null, $country);
                $this->executeSQLCMD($locationInfo);
            }

        }
        redirect(base_url() . "Debug");

    }

    public function runSingleSQLCMD($title, $key){
        if (isset($this->scriptLocations[$title][$key])){
            $this->auditor->log($_SERVER['REMOTE_ADDR'], "running setup for ".$title.' - '.$key, null, 'ZZ');
            $this->executeSQLCMD($this->scriptLocations[$title][$key]);
        }
        redirect(base_url() . "Debug");

    }

    private function executeSQLCMD($locationInfo){
        $cwd =  str_replace('%location%', $locationInfo['location'], $this->doctrine->getScriptBaseTemplate());
        $cmd =  str_replace('%script%', $locationInfo['script'], $this->doctrine->getSqlCmdTemplate());

        $oldCWD = getcwd();

        chdir($cwd);
        $output = shell_exec($cmd);

        chdir($oldCWD);
    }

    public function inspectSQLCMD($title, $country='ZZ'){
        if (isset($this->scriptLocations[$title])){
            $data=array();
            $data['group']=$title;
            $data['scriptLocations']=$this->scriptLocations[$title];

            $data = $this->loadCommonData(
                $data,
                'Inspect scripts for ' . $title,
                '',
                '',
                '',
                ''
            );

            $this->displayStandardLayout(self::LAYOUT_INSPECT, $data);

        }else{
            redirect(base_url() . "Debug");
        }

    }


    public function triggerSQLCMD($title, $action, $country){
        if (isset($this->scriptLocations[$title][$action][$country])){
            $this->auditor->log($_SERVER['REMOTE_ADDR'], "SQLCMD invoked ".$title.'.'.$action.'.'.$country , null, $country);

            $cwd =  str_replace('%location%', $this->scriptLocations[$title][$action][$country]['location'], $this->doctrine->getScriptBaseTemplate());
            $cmd =  str_replace('%script%', $this->scriptLocations[$title][$action][$country]['script'], $this->doctrine->getSqlCmdTemplate());

            $descriptorspec = array(
                0 => array("pipe", "r"),  // stdin for worker
                1 => array("pipe", "w"),  // stdout for worker
            );

            $worker = proc_open($cmd, $descriptorspec, $pipes, $cwd);

            stream_set_blocking($pipes[0], 0);
            stream_set_blocking($pipes[1], 0);

            session_write_close();
        }

        redirect(base_url() . "Debug");

    }

    public function generateInspectByTitleFromForm($title, $flatfile){
        if (isset($this->bundledQueries[$title][$flatfile])){
            $cutoffDate = $this->input->post('cutoffDate');
            $country = $this->input->post('country_code');

            if (!isset($country)){
                $country = 'PA';
            }
            if (!isset($cutoffDate)){
                $cutoffDate = $this->defaultCutoffDatesByCountry[$country];
            }
            $this->serializeInspectView($this->bundledQueries[$title][$flatfile], $country, $cutoffDate);

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "inpected dataset ".$title." - ".$country. " - ".$flatfile ." with cutoff ".$cutoffDate, null, $country);
        }
    }

    public function generateInspectByTitleAndCountryFromForm($title, $country, $flatfile){
        $cutoffDate = $this->input->post('cutoffDate');
        if (!isset($cutoffDate)){
            $cutoffDate = $this->defaultCutoffDatesByCountry[$country];
        }

        $bundle = null;
        if (isset($this->bundledQueries[$title][$country][$flatfile])){
            $bundle = $this->bundledQueries[$title][$country][$flatfile];

        }else if (isset($this->bundledQueries[$title]['ALL'][$flatfile])){
            $bundle = $this->bundledQueries[$title]['ALL'][$flatfile];

        }

        if (isset($bundle)){
            $this->serializeInspectView($title, $bundle, $country, $cutoffDate);

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "inpected dataset ".$title." - ".$country. " - ".$flatfile ." with cutoff ".$cutoffDate, null, $country);

        }



    }

    private function replaceParams($sql, $country, $cutoffDate){
        $sql = str_replace('%country%', $country, $sql);
        $sql = str_replace('%cutoffDate%', $cutoffDate,$sql);

        return $sql;
    }


    private function serializeInspectView($title, $steps, $country=null, $cutoffDate = null){
        if (isset($steps['setup'])){
            $stmt = $this->em->getConnection()->prepare($this->replaceParams($steps['setup'], $country, $cutoffDate));
            $stmt->execute();
        }

        $content = '';
        if (isset($steps['useSmartyTemplate'])){
            // do the Smarty thing
            $stmt = $this->em->getConnection()->prepare($this->replaceParams($steps['recordSet'], $country, $cutoffDate));
            $stmt->execute();
            $data['VOs'] = $stmt->fetchAll();

            $templateFullPath = IDL_TEMPLATES.trim(str_replace('\n','', $steps['useSmartyTemplate']));

            $this->smarty->assign('rn', "\r\n");
            $content =  $this->smarty->fetch($templateFullPath, $data);
            $content = substr($content, 0, strlen($content)-2);

        }else if (isset($steps['useSQLCMDScript'])){
            $outputPath =  str_replace('%location%', $steps['output']['location'], $this->doctrine->getScriptBaseTemplate());

            date_default_timezone_set('America/Sao_Paulo');
            $timestamp = date('YmdHi', time());

            $outputFile =  str_replace('%country%', $country, str_replace('%timestamp%', $timestamp, $steps['output']['fileNameTemplate']));

            $cwd =  str_replace('%location%', $steps['useSQLCMDScript']['location'], $this->doctrine->getScriptBaseTemplate());
            $cmd =  str_replace('%script%', $steps['useSQLCMDScript']['script'], $this->doctrine->getSqlCmdTemplate());
            $cmd = $cmd.' -h-1 -s"|" -W -f 65001 ';
            $cmd = $cmd.' -o "'.$outputPath.'/'.$outputFile.'"';
            $cmd = $cmd.' -v '.str_replace('%country%', $country,'targetCountryCode="%country%"');
            $cmd = $cmd.' -v '.str_replace('%cutoffDate%', $cutoffDate,'cutoffDate="%cutoffDate%"');

            $descriptorspec = array(
                0 => array("pipe", "r"),  // stdin for worker
                1 => array("pipe", "w"),  // stdout for worker
            );

            $worker = proc_open($cmd, $descriptorspec, $pipes, $cwd);

            stream_set_blocking($pipes[0], 0);
            stream_set_blocking($pipes[1], 0);

            $data=array();
            $data['bundleTitle']=$title;
            $data['country']=$country;
            $data['outputFile']=$outputFile;
            $data['location']=$steps['useSQLCMDScript']['location'];
            $data['script']=$steps['useSQLCMDScript']['script'];

            $data['debugCWD']=$cwd;
            $data['debugCMD']=$cmd;

            $data = $this->loadCommonData(
                $data,
                'Requested generation of file ' . $data['outputFile'],
                '',
                '',
                '',
                ''
            );

            $this->displayStandardLayout(self::LAYOUT_INSPECT_TRIGGERED, $data);

        } else {
            // default serialization without template
            //
            $cmd = $this->replaceParams($steps['recordSet'], $country, $cutoffDate);

            $query = $this->db->query($cmd);
            $resultArray = $query->result_array();
            if (sizeof($resultArray) >= 1) {
                $this->output->set_content_type('text/plain','ANSI');

                foreach ($resultArray as $fields) {
                    $this->output->append_output(implode($fields, '|') . "\r\n");

                   // $content = $content . implode($fields, '|') . "\r\n";
                }
            }
        }


    }



    public function generateBundledNamedQuery($title, $country){
        $cutoffDate = $this->input->post('cutoffDate');
        if (!isset($cutoffDate)){
            $cutoffDate = $this->defaultCutoffDatesByCountry[$country];
        }


        $bundle = null;

        if (isset($this->bundledQueries[$title][$country])){
            $bundle = $this->bundledQueries[$title][$country];

        }else if (isset($this->bundledQueries[$title]['ALL'])){
            $bundle = $this->bundledQueries[$title]['ALL'];

        }

        if (isset($bundle)){
            reset($bundle);
            $first_key = key($bundle);

            if (isset ($bundle[$first_key]['output'])){
                // new async process SQLCMD style
                $this->downloadExistingFlatFilesToZip($bundle,  $country, $title.'_'.$country);

            }else{
                // old sync style
                $this->serializeBundledNamedQueriesToZip($title, $country, $cutoffDate, $bundle, 'transactional/'.$title, $title.'_'.$country);

            }


            $this->auditor->log($_SERVER['REMOTE_ADDR'], "generated bundle ".$title." - ".$country, null, $country);

        }
    }

    private function downloadExistingFlatFilesToZip($flatFileArray, $country, $zipName){
        date_default_timezone_set('America/Sao_Paulo');
        $date = date('YmdHi', time());

        $latestFlatFilePaths = array();

        foreach($flatFileArray as $fileKey=>$steps) {
            if (isset($steps['output'])) {
                $outputPath =  str_replace('%location%', $steps['output']['location'], $this->doctrine->getScriptBaseTemplate());

                $allFlatFiles = scandir($outputPath, 1);

                $countryFlatFilePaths = array();

                foreach($allFlatFiles as $ff){
                    if (strstr($ff, $country)){
                        array_push($countryFlatFilePaths, $ff);
                    }
                }

                rsort($countryFlatFilePaths);

                if (sizeof($countryFlatFilePaths)>0){
                    for ($i=1;$i<sizeof($countryFlatFilePaths);$i++){
                        unlink($outputPath.'/'.$countryFlatFilePaths[$i]);
                    }

                    array_push($latestFlatFilePaths, $outputPath.'/'.$countryFlatFilePaths[0]);
                }
            }

        }

        foreach($latestFlatFilePaths as $ff){
            $this->zip->read_file($ff);
        }

        $this->zip->download('TKE_LA_'.$zipName.'_'.$date.'.zip');


    }

    private function serializeBundledNamedQueriesToZip($title, $country, $cutoffDate, $flatFileArray, $outputFolder, $zipName){
        date_default_timezone_set('America/Sao_Paulo');
        $date = date('YmdHi', time());

        $timeStampedoutputFolder = IDL_TEMPLATES_AUTO_OUTPUT.$outputFolder.'/'.$date;

        if (!file_exists(IDL_TEMPLATES_AUTO_OUTPUT.$outputFolder)){
            mkdir(IDL_TEMPLATES_AUTO_OUTPUT.$outputFolder);

        }
        if (!file_exists($timeStampedoutputFolder)){
            mkdir($timeStampedoutputFolder);

        }

        $generatedPaths = array();

        foreach($flatFileArray as $fileKey=>$steps) {
            $fileTemplate = $timeStampedoutputFolder . '/' . str_replace('%country%', $country, str_replace("%timestamp%", $date, $steps['fileNameTemplate']));

            if (isset($steps['setup'])) {
                $query = $this->db->query($steps['setup']);
                $resultArray = $query->result_array();
                assert($resultArray != null);
            }

            if (isset($steps['useSmartyTemplate'])) {
                // do the Smarty thing
                $stmt = $this->em->getConnection()->prepare($this->replaceParams($steps['recordSet'], $country, $cutoffDate));
                $stmt->execute();

                $data['VOs'] = $stmt->fetchAll();

                $templateFullPath = IDL_TEMPLATES . trim(str_replace('\n', '', $steps['useSmartyTemplate']));

                $this->smarty->assign('rn', "\r\n");
                $content = $this->smarty->fetch($templateFullPath, $data);
                $content = substr($content, 0, strlen($content) - 2);

                $outputfile = fopen($fileTemplate, "w") or die("Unable to open file!");
                fwrite($outputfile, utf8_encode($content));
                fclose($outputfile);

                array_push($generatedPaths, $fileTemplate);

            } else {
                // default serialization without template
                //
                $query = $this->db->query($this->replaceParams($steps['recordSet'], $country, $cutoffDate));
                $resultArray = $query->result_array();

                if (sizeof($resultArray) >= 1) {
                    $fp = fopen($fileTemplate, 'w');
                    fwrite($fp, pack("CCC",0xef,0xbb,0xbf));

                    if (!strstr($fileTemplate, '.csv' )){
                        foreach ($resultArray as $fields) {
                            fputs($fp, implode($fields, '|'));
                            fputs($fp, "\r\n");
                        }

                    }else{
                        $header = array();
                        foreach(array_keys($resultArray[0]) as $key) {
                            array_push($header, $key);
                        }

                        fputcsv($fp, $header, ',', '"');

                        foreach ($resultArray as $fields) {
                            fputcsv($fp, $fields, ',', '"');
                        }

                    }

                    fclose($fp);

                    array_push($generatedPaths, $fileTemplate);
                }
            }
        }
        foreach($generatedPaths as $reportPath){
            $this->zip->read_file($reportPath);
        }

        $this->zip->download('TKE_LA_'.$zipName.'_'.$date.'.zip');
    }






    private function serializeNamedQueriesToZippedBundle($queryArray, $outputFolder, $zipName, $country, $alsoGenerateExcel = false){
        date_default_timezone_set('America/Sao_Paulo');
        $date = date('YmdHi', time());

        $timeStampedoutputFolder = IDL_TEMPLATES_AUTO_OUTPUT.$outputFolder.'/'.$date;

        if (!file_exists(IDL_TEMPLATES_AUTO_OUTPUT.$outputFolder)){
            mkdir(IDL_TEMPLATES_AUTO_OUTPUT.$outputFolder);

        }
        if (!file_exists($timeStampedoutputFolder)){
            mkdir($timeStampedoutputFolder);

        }

        $generatedPaths = array();

        foreach($queryArray as $file => $query){
            $fileTemplate = $file."_%country_%timestamp%".".csv";
            $fileTemplate = $timeStampedoutputFolder.'/'.str_replace('%country',$country, str_replace("%timestamp%", $date, $fileTemplate));

            $query = $this->db->query($query['sql'], $query['params']);

            $resultArray = $query->result_array();

            if (sizeof($resultArray)>=1){
                $fp = fopen($fileTemplate, 'w');

                $header = array();
                foreach(array_keys($resultArray[0]) as $key) {
                    array_push($header, $key);
                }

                fputcsv($fp, $header);

                foreach($resultArray as $fields){
                    fputcsv($fp, $fields);

                }
                fclose($fp);

                array_push($generatedPaths, $fileTemplate);

            }

        }

        $pathsToZip = array();

        if ($alsoGenerateExcel){

            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();

            $reader->setDelimiter(',');
            $reader->setEnclosure('"');
            $reader->setSheetIndex(0);

            foreach($generatedPaths as $csvPath){
                $spreadsheet = $this->phpSpreadsheet->createNewSpreadsheet();

                /* Load a CSV file and save as a XLS */
                $spreadsheet = $reader->load($csvPath);

                $xlPath = str_replace(".csv",'.xlsx', $csvPath);
                $writer = new Xlsx($spreadsheet);
                $writer->setPreCalculateFormulas(false);
                $writer->save($xlPath);

                $spreadsheet->disconnectWorksheets();
                unset($spreadsheet);

                array_push($pathsToZip, $csvPath);
                array_push($pathsToZip, $xlPath);
            }


        }else{
            $pathsToZip = $generatedPaths;
        }


        foreach($pathsToZip as $reportPath){
            $this->zip->read_file($reportPath);
        }

        $this->zip->download('TKE_LA_'.$zipName.'_'.$date.'.zip');
    }




}
