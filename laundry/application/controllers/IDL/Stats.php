<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
include_once APPPATH . 'controllers/MY_Controller.php';
/**
*
 *  Serializers use Smarty templates to generate output
 *
 * https://www.codeigniter.com/userguide3/libraries/output.html
 *
 */
Use Doctrine\ORM\Query;
Use Commons\IDL\IDLGroupDescriptor;

class Stats extends MY_Controller {

    const COLLECTION_KPI =[
        'Inventory' => [
            'ItemHeader' => ['label' => 'Item staged','color' => 'rgb(39, 144, 114)'],
            'ItemHeader - target' => ['label' => 'Item RFM (current target)','color' => 'rgb(110, 165, 153)'],
            'ItemHeader - dirtyRecommended AKA unstagedRecommended' => ['label' => 'Item unstaged and recommended','color' => 'rgb(2550, 0, 0)']
        ],
        'Customers' => [
            'CustomerHeader' => ['label' => 'Customer', 'color' => 'rgb(0, 160, 245)'],
            'CustomerHeader - target' => ['label' => 'Customer target', 'color' => 'rgb(0, 201, 245)'],
        ],
        'Vendors' => [
            'VendorHeader' => ['label' => 'Vendor','color' => 'rgb(133, 0, 0)'],
            'VendorHeader - target' => ['label' => 'Vendor target','color' => 'rgb(250, 0, 0)'],
        ],
        'Projects' => [
            'ProjectHeader' => ['label' => 'Project','color' => 'rgb(64, 123, 0)'],
            'ProjectHeader - target' => ['label' => 'Project target','color' => 'rgb(133, 206, 0)'],
        ],
    ];

    public function __construct()
    {
        parent::__construct();

        $this->em = $this->doctrine->getEntityManager();
        $this->load->library('zip');
        $this->load->helper('url_helper');
   }

    public function grabLatestErrorFrequency($country, $group, $idl){
        $qb = $this->getErrorFrequency($country,$group,$idl);
        $this->serializeErrorFrequencyResult($country, $qb);
    }

    public function grabRankingFrequency($country, $group, $idl){
        $this->serializeRankingResult($country, $this->getRankingFrequency($country,$group,$idl), $this->getCleansedRFMByRanking($country,$group,$idl));
    }


    public function grabLatestSampleKPI($country, $group){
        $qb = $this->getKPIBuilder($country);
        $qb->setMaxResults(1);
        $this->serializeKPIQueryResult($country, $group, $qb);
    }


    public function grabTimeSeriesSampleKPI($group, $country){
        $qb = $this->getKPIBuilder($country);
        $this->serializeKPIQueryResult($group, $country, $qb);
    }


    private function serializeRankingResult($country, $dataRFMQuery, $dataCleansedRFMQuery){
        $data['country'] = $country;
        $data['data'] =  array();

        $data['data']['labels'] = array();
        $data['data']['dataRFM'] = array();
        $data['data']['dataCleansedRFM'] = array();

        $result = $dataRFMQuery->getQuery()->getResult(Query::HYDRATE_ARRAY);

        foreach($result as $r){
            array_push( $data['data']['labels'], utf8_encode($r['ranking']).($r['ranking']<5?" - NOT RFM":""));

            array_push( $data['data']['dataRFM'], $r['rankingCount']);

            $qb = $dataCleansedRFMQuery;
            $qb->setParameter('ranking',$r['ranking']);

            $countForRanking = $qb->getQuery()->getSingleResult();

            array_push( $data['data']['dataCleansedRFM'], $countForRanking['rankingCount']);
        }

        //add the header here
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        echo json_encode( $data );

    }

    private function serializeErrorFrequencyResult($country, $qb){
        $data['country'] = $country;
        $data['data'] =  array();

        $data['data']['labels'] = array();
        $data['data']['datasets']['data'] = array();

        $result = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

        foreach($result as $r){
            array_push( $data['data']['labels'], utf8_encode($r['action']));
            array_push( $data['data']['datasets']['data'], $r['errorCount']);
        }

        //add the header here
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        echo json_encode( $data );

    }


    private function serializeKPIQueryResult($group, $country, $qb){
        $data['country'] = $country;
        $data['data'] =  array();

        $data['data']['labels'] = Stats::COLLECTION_KPI;
        $data['data']['datasets'] = array();

        foreach(Stats::COLLECTION_KPI[$group] as $key => $value){
            $qb->setParameter(2, 'sampleKPI - '.$key);
            try{
                $result = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

                $kpiNode['label'] = $value['label'];
                $kpiNode['fill'] = false;
                $kpiNode['pointRadius'] = 2;
                $kpiNode['lineTension'] = 0;
                $kpiNode['borderWidth'] = 2;
                $kpiNode['backgroundColor'] = $value['color'];
                $kpiNode['borderColor'] = $value['color'];

                $dataResults = array();
                foreach($result as $r){
                    $dataPoint = array();
                    $dataPoint['t'] = $r['stamp']->format(DateTime::ATOM);
                    $dataPoint['y'] = $r['count'];
                    array_push($dataResults, $dataPoint);
                }
                $kpiNode['data'] = $dataResults;

                array_push($data['data']['datasets'], $kpiNode);
            }catch (Exception $e){
                $data = ['error'=>'no KPIs found for '.$key];
            }

        }
        //add the header here
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        echo json_encode( $data );

    }

    private function getKPIBuilder($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('log')
            ->from('Commons\\AuditLog', 'log')
            ->andWhere(
                $qb->expr()->eq('log.country', '?1'),
                $qb->expr()->orX(
                    $qb->expr()->like('log.event', '?2')
                )
            )
            ->setParameter(1, $country)
            ->addOrderBy('log.stamp','DESC');
        return $qb;

    }

    private function getRankingFrequency($country, $group, $idl){
        if (stristr($idl,'Pre')) {
            $classPrefix = "RAW";
        }else{
            $classPrefix = "ORA";
        }

        $qb = $this->em->createQueryBuilder();
        $qb->select('vo.ranking, count(vo.ranking) as rankingCount')
            ->from('Commons\\'.$classPrefix.'\\'.$idl, 'vo')
            ->andWhere(
                $qb->expr()->eq('vo.country', '?1')
            )
            ->setParameter(1, $country)
            ->groupBy('vo.ranking')
            ->orderBy('vo.ranking','DESC');
        return $qb;

    }

    private function getCleansedRFMByRanking($country, $group, $idl){
        if (stristr($idl,'Pre')) {
            $classPrefix = "RAW";
        }else{
            $classPrefix = "ORA";
        }

        $qb = $this->em->createQueryBuilder();
        $qb->select('count(vo.ranking) as rankingCount')
            ->from('Commons\\'.$classPrefix.'\\'.$idl, 'vo')
            ->andWhere(
                $qb->expr()->eq('vo.ranking', ':ranking'),
                $qb->expr()->eq('vo.country', '?1'),
                $qb->expr()->eq('vo.cleansed', '?2')
            )
            ->setParameter(1, $country)
            ->setParameter(2, true);

        return $qb;

    }



    private function getErrorFrequency($country, $group, $idl){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo.action, count(vo.action) as errorCount')
            ->from('Commons\\ORA\\'.$idl, 'vo')
            ->andWhere(
                $qb->expr()->neq('vo.action', '?1'),
                $qb->expr()->eq('vo.dismissed', '?2'),
                $qb->expr()->eq('vo.country', '?3'),
                $qb->expr()->orX(
                    $qb->expr()->eq('vo.crossRefValidated', '?4'),
                    $qb->expr()->eq('vo.locallyValidated', '?5')
                )
            )
            ->setParameter(1, '')
            ->setParameter(2, false)
            ->setParameter(3, $country)
            ->setParameter(4, false)
            ->setParameter(5, false)
            ->groupBy('vo.action');
        return $qb;

    }


}
