<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

Use Doctrine\ORM\Query;

class Templates extends CI_Controller
{

    private $em;

    public function __construct()
    {
        parent::__construct();

        $this->em = $this->doctrine->getEntityManager();

        $this->smarty->assign('rn', "\r\n");

        $this->load->library('zip');
        $this->load->helper('url_helper');
    }

    public function index()
    {
        $data['title'] = "The Smarty parser works!";
        $data['body'] = "This is body text to show that the Smarty Parser works!  By default, the output is generated directly to the response.";

        $this->smarty->assign('data', $data);
        $this->smarty->view('info.tpl', $data, false);
    }

    public function downloadAllTemplates($country = null, $groupId = null)
    {
        $idlGroupRepository = $this->em->getRepository('Commons\\IDL\\IDLGroupDescriptor');
        $idlGroup = $idlGroupRepository->findOneBy(array('id' => $groupId));

        $messages = "generating zipped templates for " . $country . "/" . $idlGroup->getShortName() . " @ " . IDL_TEMPLATES_AUTO_OUTPUT;

        date_default_timezone_set('America/Sao_Paulo');
        $date = date('Ymd_His', time());

        $outputFolder = $idlGroup->getOutputPath() . '/' . $idlGroup->getShortName() . '_' . $date;
        mkdir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder);
        mkdir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/base');
        mkdir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/ORA');
        mkdir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/RAW');
        mkdir( IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/headerController');
        mkdir( IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/lineController');
        mkdir( IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/transactionalController');
        mkdir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/formBase');
        mkdir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/formCustom');
        mkdir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/routes');
        mkdir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/output');

        foreach ($idlGroup->getDescriptors() as $descriptor) {
            $idlShortName = $descriptor->getShortName();
            $this->generateContent($country, $idlGroup->getShortName(), $idlShortName, 'auto/phpBase.tpl', $outputFolder.'/base', false);
            $this->generateContent($country, $idlGroup->getShortName(), $idlShortName, 'auto/phpORA.tpl', $outputFolder.'/ORA', false);
            $this->generateContent($country, $idlGroup->getShortName(), $idlShortName, 'auto/phpRAW.tpl', $outputFolder.'/RAW', false);
            $this->generateContent($country, $idlGroup->getShortName(), $idlShortName, 'auto/defaultRoutes.tpl', $outputFolder.'/routes', false);
            $this->generateContent($country, $idlGroup->getShortName(), $idlShortName, 'auto/headerController.tpl', $outputFolder.'/headerController', false, true);
            $this->generateContent($country, $idlGroup->getShortName(), $idlShortName, 'auto/lineController.tpl', $outputFolder.'/lineController', false, true);
            $this->generateContent($country, $idlGroup->getShortName(), $idlShortName, 'auto/transactionalController.tpl', $outputFolder.'/transactionalController', false, true);
            $this->generateContent($country, $idlGroup->getShortName(), $idlShortName, 'auto/formBase.tpl', $outputFolder.'/formBase', false, true);
            $this->generateContent($country, $idlGroup->getShortName(), $idlShortName, 'auto/formCustom.tpl', $outputFolder.'/formCustom', false, true);
            $this->generateContent($country, $idlGroup->getShortName(), $idlShortName, 'auto/idlOutput.tpl', $outputFolder.'/output', true);
        }

        $this->zip->read_dir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/base', false);
        $this->zip->read_dir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/ORA', false);
        $this->zip->read_dir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/RAW', false);
        $this->zip->read_dir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/formBase', false);
        $this->zip->read_dir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/formCustom', false);
        $this->zip->read_dir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/headerController', false);
        $this->zip->read_dir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/lineController', false);
        $this->zip->read_dir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/transactionalController', false);
        $this->zip->read_dir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/routes', false);
        $this->zip->read_dir(IDL_TEMPLATES_AUTO_OUTPUT .$outputFolder.'/output', false);

        $this->zip->download($country . '_' . $idlGroup->getShortName() . '_' . $date . '_templates.zip');
    }


    public function generateFormForBase($country = null, $groupShortName = null, $idlShortName = null, $direct = true)
    {
        $messages = array("generating form for Base Class " . $country . "/" . $groupShortName . "/" . $idlShortName . " @ " . IDL_TEMPLATES_AUTO_OUTPUT);

        $content = $this->generateContent($country, $groupShortName, $idlShortName, 'auto/formBase.tpl', '/formBase', false);
        if ($direct) {
            $this->output
                ->set_content_type('text/plain')
                ->set_output($content);

        } else {
            return $messages;

        }

    }

    public function generateFormForCustom($country = null, $groupShortName = null, $idlShortName = null, $direct = true)
    {
        $messages = array("generating customized form for Base Class " . $country . "/" . $groupShortName . "/" . $idlShortName . " @ " . IDL_TEMPLATES_AUTO_OUTPUT);

        $content = $this->generateContent($country, $groupShortName, $idlShortName, 'auto/formCustom.tpl', '/formCustom', false, true);
        if ($direct) {
            $this->output
                ->set_content_type('text/plain')
                ->set_output($content);

        } else {
            return $messages;

        }

    }

    public function generateFormForMandatory($country = null, $groupShortName = null, $idlShortName = null, $direct = true)
    {
        $messages = array("generating customized mandatory-only form for Base Class " . $country . "/" . $groupShortName . "/" . $idlShortName . " @ " . IDL_TEMPLATES_AUTO_OUTPUT);

        $content = $this->generateContent($country, $groupShortName, $idlShortName, 'auto/formMandatory.tpl', '/formMandatory', false, true);
        if ($direct) {
            $this->output
                ->set_content_type('text/plain')
                ->set_output($content);

        } else {
            return $messages;

        }

    }

    /**
     * creates automatic templates based on country, group, and idl
     *
     */
    public function generateAutoOutputTemplate($country = null, $groupShortName = null, $idlShortName = null, $direct = true)
    {
        $messages = array("generating automatic output template for " . $country . "/" . $groupShortName . "/" . $idlShortName . " @ " . IDL_TEMPLATES_AUTO_OUTPUT);

        $content = $this->generateContent($country, $groupShortName, $idlShortName, 'auto/idlOutput.tpl', '/output', true, true);
        if ($direct) {
            $this->output
                ->set_content_type('text/plain')
                ->set_output($content);

        } else {
            return $messages;

        }

    }


    public function generateHeaderController($country = null, $groupShortName = null, $idlShortName = null, $direct = true)
    {
        $messages = array("generating automatic header controller for " . $country . "/" . $groupShortName . "/" . $idlShortName . " @ " . IDL_TEMPLATES_AUTO_OUTPUT);

        $content = $this->generateContent($country, $groupShortName, $idlShortName, 'auto/headerController.tpl', '/default', false);
        if ($direct) {
            $this->output
                ->set_content_type('text/plain')
                ->set_output($content);

        } else {
            return $messages;

        }

    }

    public function generateLineController($country = null, $groupShortName = null, $idlShortName = null, $direct = true)
    {
        $messages = array("generating automatic line controller for " . $country . "/" . $groupShortName . "/" . $idlShortName . " @ " . IDL_TEMPLATES_AUTO_OUTPUT);

        $content = $this->generateContent($country, $groupShortName, $idlShortName, 'auto/lineController.tpl', '/default', false);
        if ($direct) {
            $this->output
                ->set_content_type('text/plain')
                ->set_output($content);

        } else {
            return $messages;

        }

    }

    public function generateTransactionalController($country = null, $groupShortName = null, $idlShortName = null, $direct = true)
    {
        $messages = array("generating automatic transactional controller for " . $country . "/" . $groupShortName . "/" . $idlShortName . " @ " . IDL_TEMPLATES_AUTO_OUTPUT);

        $content = $this->generateContent($country, $groupShortName, $idlShortName, 'auto/transactionalController.tpl', '/default', false);
        if ($direct) {
            $this->output
                ->set_content_type('text/plain')
                ->set_output($content);

        } else {
            return $messages;

        }

    }


    public function generatePHP_Base($country = null, $groupShortName = null, $idlShortName = null, $direct = true)
    {
        $messages = array("generating automatic PHP Base for " . $country . "/" . $groupShortName . "/" . $idlShortName . " @ " . IDL_TEMPLATES_AUTO_OUTPUT);

        $content = $this->generateContent($country, $groupShortName, $idlShortName, 'auto/phpBase.tpl', 'base', false);
        if ($direct) {
            $this->output
                ->set_content_type('text/plain')
                ->set_output($content);

        } else {
            return $messages;

        }

    }

    public function generatePHP_ORA($country = null, $groupShortName = null, $idlShortName = null, $direct = true)
    {
        $messages = array("generating automatic PHP ORA for " . $country . "/" . $groupShortName . "/" . $idlShortName . " @ " . IDL_TEMPLATES_AUTO_OUTPUT);

        $content = $this->generateContent($country, $groupShortName, $idlShortName, '/auto/phpORA.tpl', '/ORA', false);
        if ($direct) {
            $this->output
                ->set_content_type('text/plain')
                ->set_output($content);

        } else {
            return $messages;

        }

    }

    public function generatePHP_RAW($country = null, $groupShortName = null, $idlShortName = null, $direct = true)
    {
        $messages = array("generating automatic PHP Base for " . $country . "/" . $groupShortName . "/" . $idlShortName . " @ " . IDL_TEMPLATES_AUTO_OUTPUT);

        $content = $this->generateContent($country, $groupShortName, $idlShortName, '/auto/phpRAW.tpl', '/RAW', false);
        if ($direct) {
            $this->output
                ->set_content_type('text/plain')
                ->set_output($content);
        } else {
            return $messages;

        }

    }

    public function generateDefaultRoutes($country = null, $groupShortName = null, $idlShortName = null, $direct = true)
    {
        $messages = array("generating automatic Default routes for " . $country . "/" . $groupShortName . "/" . $idlShortName . " @ " . IDL_TEMPLATES_AUTO_OUTPUT . '/routes');

        $content = $this->generateContent($country, $groupShortName, $idlShortName, '/auto/defaultRoutes.tpl', '/routes', false);
        if ($direct) {
            $this->output
                ->set_content_type('text/plain')
                ->set_output($content);

        } else {
            return $messages;

        }

    }


    /**
     * @param $country
     * @param $groupShortName
     * @param $idlShortName
     * @return mixed
     */
    private function getIDLFor($country, $groupShortName, $idlShortName)
    {
        $idlGroupRepository = $this->em->getRepository('Commons\\IDL\\IDLGroupDescriptor');
        $idlGroup = $idlGroupRepository->findOneBy(array('country' => $country, 'shortName' => $groupShortName));

        $idlRepository = $this->em->getRepository('Commons\\IDL\\IDLDescriptor');
        $idl = $idlRepository->findOneBy(array('shortName' => $idlShortName, 'group' => $idlGroup->getId()));
        return $idl;
    }

    private function getAllDataFields($country, $groupShortName, $idlShortName)
    {

        $idlGroupRepository = $this->em->getRepository('Commons\\IDL\\IDLGroupDescriptor');
        $idlGroup = $idlGroupRepository->findOneBy(array('country' => $country, 'shortName' => $groupShortName));

        $idlRepository = $this->em->getRepository('Commons\\IDL\\IDLDescriptor');
        $idl = $idlRepository->findOneBy(array('shortName' => $idlShortName, 'group' => $idlGroup->getId()));

        $queryBuilder = $this->em->createQueryBuilder();


        $queryBuilder->select('f', 'od', 'og')
            ->from('Commons\\IDL\\IDLFieldDescriptor', 'f')
            ->leftJoin('f.descriptor', 'od')
            ->leftJoin('od.group', 'og')
            ->andWhere($queryBuilder->expr()->eq('f.descriptor', '?1'))
            ->setParameter(1, $idl->getId())
            ->orderBy('og.country', 'ASC')
            ->orderBy('od.shortName', 'ASC');


        $data['fields'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);

        return $data;
    }

    private function getChildrenFieldsFor($country, $idlShortName){
        $qb = $this->em->createQueryBuilder();
        $qb->select('f', 'children', 'd', 'g')
            ->from('Commons\\IDL\\IDLFieldDescriptor', 'f')
            ->leftJoin('f.descriptor', 'd')
            ->leftJoin('d.group', 'g')
            ->leftJoin('f.children', 'children')
            ->andWhere(
                $qb->expr()->eq('g.country', '?1'),
                $qb->expr()->eq('d.shortName',  '?2')
            );

        $childrenQuery = $qb->getQuery();
        $childrenQuery->setParameter(1, $country)
            ->setParameter(2, $idlShortName);

        $fields = $childrenQuery->getResult();

        $childrenFields = array();

        foreach($fields as $f){
            if (sizeof($f->getChildren())!=0){
                $children = $f->getChildren();
                foreach($children as $child){
                    $name = $child->getDescriptor()->getShortName();

                    $childrenFields[]=$child;

                }
            }
        }

        return $childrenFields;
    }

    private function getHeaderFieldsFor($country, $idlShortName){
        $qb = $this->em->createQueryBuilder();
        $qb->select('f', 'children', 'd', 'g')
            ->from('Commons\\IDL\\IDLFieldDescriptor', 'f')
            ->leftJoin('f.descriptor', 'd')
            ->leftJoin('d.group', 'g')
            ->leftJoin('f.children', 'children')
            ->andWhere(
                $qb->expr()->eq('g.country', '?1'),
                $qb->expr()->eq('d.shortName',  '?2')
            );

        $headerQuery = $qb->getQuery();
        $headerQuery->setParameter(1, $country)
            ->setParameter(2, $idlShortName);

        $fields = $headerQuery->getResult();

        $headerFields = array();

        foreach($fields as $f){
            if ($f->getHeader()!=null){
                $headerFields[]=$f;
            }
        }

        return $headerFields;
    }


    private function getOutputDataFields($country, $groupShortName, $idlShortName)
    {
        $idlGroupRepository = $this->em->getRepository('Commons\\IDL\\IDLGroupDescriptor');
        $idlGroup = $idlGroupRepository->findOneBy(array('country' => $country, 'shortName' => $groupShortName));

        $idlRepository = $this->em->getRepository('Commons\\IDL\\IDLDescriptor');
        $idl = $idlRepository->findOneBy(array('shortName' => $idlShortName, 'group' => $idlGroup->getId()));

        $queryBuilder = $this->em->createQueryBuilder();

        $queryBuilder->select('fd')
            ->from('Commons\\IDL\\IDLFieldDescriptor', 'fd')
            ->andWhere($queryBuilder->expr()->eq('fd.descriptor', '?1'), $queryBuilder->expr()->eq('fd.isOutputValue', 'true'))
            ->orderBy('fd.outputPos', 'ASC')
            ->setParameter(1, $idl->getId());

        $data['fields'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);

        return $data;
    }

    /**
     * @param $country
     * @param $idl
     * @param $content
     */
    private function saveToCache($content, $fileTemplate, $country, $idlShortName)
    {

        $fileTemplate = str_replace('%country%', $country, $fileTemplate);
        $fileTemplate = str_replace('%idlShortName%', $idlShortName, $fileTemplate);

        date_default_timezone_set('America/Sao_Paulo');
        $date = date('Ymd_His', time());
        $fileTemplate = str_replace("%timestamp%", $date, $fileTemplate);

        $outputPath = IDL_TEMPLATES_AUTO_OUTPUT . $fileTemplate;

        $outputfile = fopen($outputPath, "w") or die("Unable to open file!");

        fwrite($outputfile, utf8_encode($content));
        fclose($outputfile);
    }

    private function generateContent($country, $groupShortName, $idlShortName, $templateName, $outputPath, $isOutputFieldsOnly = false, $explodeLOV = false)
    {
        $idl = $this->getIDLFor($country, $groupShortName, $idlShortName);

        if ($isOutputFieldsOnly) {
            $data = $this->getOutputDataFields($country, $groupShortName, $idlShortName);

        } else {
            $data = $this->getAllDataFields($country, $groupShortName, $idlShortName);

        }

        if ($explodeLOV) {
            for ($i = 0; $i < sizeof($data['fields']); $i++) {
                if (strlen($data['fields'][$i]['LOV']) > 0) {
                    $data['fields'][$i]['selectForLOV'] = true;
                    $data['fields'][$i]['selectValues'] = explode(",", $data['fields'][$i]['LOV']);
                } else {
                    $data['fields'][$i]['selectForLOV'] = false;
                }

            }
        }

        $data['headerFields'] = $this->getHeaderFieldsFor($country, $idlShortName);
        $data['childrenFields'] = $this->getChildrenFieldsFor($country, $idlShortName);

        $this->smarty->assign('country', $country);
        $this->smarty->assign('groupShortName', $groupShortName);
        $this->smarty->assign('idlShortName', $idlShortName);
        $this->smarty->assign('preStageTableName', $idl->getPreStageTableName());
        $this->smarty->assign('stageTableName', $idl->getStageTableName());

        $content = $this->smarty->fetch(IDL_TEMPLATES . $templateName, $data);

        $this->saveToCache($content, $outputPath . '/%idlShortName%.txt', $country, $idlShortName);
        return $content;
    }


}
