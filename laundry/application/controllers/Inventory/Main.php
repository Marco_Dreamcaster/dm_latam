<?php
include_once APPPATH . 'controllers/MY_Controller.php';

/**
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/query-builder.html
 *
 * http://www.doctrine-project.org/api/orm/2.5/class-Doctrine.ORM.QueryBuilder.html
 *
 * ItemHeader Controller (header style)
 *
 */

Use Doctrine\ORM\Query;


class Main extends MY_Controller
{

    const KEY_ITEM_CATEGORY = array('origItemNumber');
    const CH_ITEM_CATEGORY = 'Commons\\ORA\\ItemCategory';
    const CH_PRE_ITEM_CATEGORY = 'Commons\\RAW\\PreItemCategory';

    const KEY_ITEM_CROSS_REF = array('origItemNumber');
    const CH_ITEM_CROSS_REF = 'Commons\\ORA\\ItemCrossRef';
    const CH_PRE_ITEM_CROSS_REF = 'Commons\\RAW\\PreItemCrossRef';

    const KEY_ITEM_MFG_PART_NUM = array('origItemNumber');
    const CH_ITEM_MFG_PART_NUM = 'Commons\\ORA\\ItemMFGPartNum';
    const CH_PRE_ITEM_MFG_PART_NUM = 'Commons\\RAW\\PreItemMFGPartNum';

    const KEY_ITEM_TRANS_DEF_LOC = array('origItemNumber');
    const CH_ITEM_TRANS_DEF_LOC = 'Commons\\ORA\\ItemTransDefLoc';
    const CH_PRE_ITEM_TRANS_DEF_LOC = 'Commons\\RAW\\PreItemTransDefLoc';

    const KEY_ITEM_TRANS_DEF_SUB_INV = array('origItemNumber');
    const CH_ITEM_TRANS_DEF_SUB_INV = 'Commons\\ORA\\ItemTransDefSubInv';
    const CH_PRE_ITEM_TRANS_DEF_SUB_INV = 'Commons\\RAW\\PreItemTransDefSubInv';

    const ACTION_CLEANSE = 'Inventory/cleanseDirty/';
    const ACTION_CLEANSE_LABEL = 'NEW Cleansed Record';

    const ACTION_BASE = 'Inventory/updateCleansed/';
    const ACTION_CUSTOM = 'Inventory/customUpdateCleansed/';
    const ACTION_LABEL = 'Update ItemHeader';

    const ACTION_LIST = "Inventory/cleansed/";

    const CANCEL = "Inventory/cleansed/";
    const CANCEL_DIRTY = "Inventory/dirty/";
    const DISCARD = 'Inventory/discard/';

    const DEFAULT_LAYOUTS = [
        'base' => array(
            'ORA/ItemHeader/topBarCleansed.tpl',
            'ORA/ItemHeader/frmBase.tpl'),

        'blocked' => array(
            'ORA/ItemHeader/frmBlocked.tpl'),

        'customCleansed' => array(
            'ORA/ItemHeader/topBarCleansed.tpl',
            'ORA/ItemHeader/frmCustomCleansed.tpl'),

        'dirty' => array(
            'ORA/ItemHeader/frmDirty.tpl'),

        'listDirty' => array(
            'ORA/ItemHeader/lstDirty.tpl'),

        'listCleansed' => array(
            'ORA/ItemHeader/lstCleansed.tpl'),

        'error' => array(
            'ORA/ItemHeader/error.tpl'),

        'mergedCleansed' => array(
            'ORA/ItemHeader/topBarCleansed.tpl',
            'ORA/ItemHeader/frmMergedCleansed.tpl'),

        'uploadResource' => array(
            'ORA/ItemHeader/frmUploadResource.tpl')

    ];

    const VIEW_LAYOUTS = array(
        "PA" => self::DEFAULT_LAYOUTS,
        "CO" => self::DEFAULT_LAYOUTS,
        "CR" => self::DEFAULT_LAYOUTS,
        "HN" => self::DEFAULT_LAYOUTS,
        "GT" => self::DEFAULT_LAYOUTS,
        "MX" => self::DEFAULT_LAYOUTS,
        "NI" => self::DEFAULT_LAYOUTS,
        "SV" => self::DEFAULT_LAYOUTS,
        "UY" => self::DEFAULT_LAYOUTS,
        "PE" => self::DEFAULT_LAYOUTS,
        "PY" => self::DEFAULT_LAYOUTS,
        "AR" => self::DEFAULT_LAYOUTS,
        "EC" => self::DEFAULT_LAYOUTS,
        "CL" => self::DEFAULT_LAYOUTS
    );

    public function __construct()
    {
        parent::__construct();

    }

    public function listCleansed($country, $page = 0, $sortCol = null, $sortAsc = null)
    {
        if ($this->checkPreConditions($country)) {
            show_error('please contact your system administrator');
        }

        if (!isset($sortCol)) {
            $sortCol = $this->session->selCol;
        }

        if (!isset($sortAsc)) {
            $sortAsc = $this->session->selAsc;
        }

        switch ($sortCol) {
            case 'name':
                $data['cleansedVOs'] = $this->getCleansedHeader(self::VO_ITEM_HEADER, $country, $page, 'descriptionEsa', $sortAsc == 1)->getResult(Query::HYDRATE_ARRAY);
                break;
            case 'lastUpdated':
                $data['cleansedVOs'] = $this->getCleansedHeader(self::VO_ITEM_HEADER, $country, $page, 'lastUpdated', $sortAsc == 1)->getResult(Query::HYDRATE_ARRAY);
                break;
            case 'id':
                $data['cleansedVOs'] = $this->getCleansedHeader(self::VO_ITEM_HEADER, $country, $page, 'itemNumber', $sortAsc == 1)->getResult(Query::HYDRATE_ARRAY);
                break;
            case 'error':
                $data['cleansedVOs'] = $this->getCleansedHeader(self::VO_ITEM_HEADER, $country, $page, 'crossRefValidated', $sortAsc == 1)->getResult(Query::HYDRATE_ARRAY);
                break;
            case 'ranking':
                $data['cleansedVOs'] = $this->getCleansedHeader(self::VO_ITEM_HEADER, $country, $page, 'ranking', $sortAsc == 1)->getResult(Query::HYDRATE_ARRAY);
                break;

            default:
                $data['cleansedVOs'] = $this->getCleansedHeader(self::VO_ITEM_HEADER, $country, $page, 'itemNumber', $sortAsc == 1)->getResult(Query::HYDRATE_ARRAY);
                break;
        }

        $data['country'] = $country;
        $data['sortOrder'] = $sortCol;
        $data['sortDirection'] = $sortAsc == 1 ? 'ASC' : 'DESC';

        $this->session->selGroup='Inventory';
        $this->session->selCountry=strtolower($country);

        $data = $this->loadCommonData($data, $country . ' Cleansed Inventory Items ', '', '', '', '');

        $data = $this->addCleansedStats($data, self::VO_ITEM_HEADER, $country, true);
        $data = $this->addPaginationMetadata($data, $country, $page, self::ACTION_LIST);

        $this->displayStandardLayout(self::VIEW_LAYOUTS[$country]['listCleansed'], $data);
    }

    public function listDirty($country, $page = 0)
    {
        if ($this->checkPreConditions($country)) {
            show_error('please contact your system administrator');
        }

        $data['dirtyVOs'] = $this->getDirtyHeader(self::VO_PRE_ITEM_HEADER, $country, $page, 'ranking')->getResult(Query::HYDRATE_ARRAY, false);

        $data['country'] = $country;
        $data = $this->loadSessionNavigation($data);

        $this->session->selGroup='Inventory';
        $this->session->selCountry=strtolower($country);

        $data = $this->loadCommonData($data, $country . ' Dirty Inventory Items ', '', '', '', '');

        $data = $this->loadStats($data, self::VO_PRE_ITEM_HEADER, self::VO_ITEM_HEADER, $country);

        $data['total'] = $data['totalPreSTG'];
        $data = $this->addPaginationMetadata($data, $country, $page, self::CANCEL_DIRTY);

        $this->displayStandardLayout(self::VIEW_LAYOUTS[$country]['listDirty'], $data);
    }


    public function toggle($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_ITEM_HEADER, $id);
        $vo->setDismissed(!$vo->getDismissed());
        $this->em->flush();

        $this->toggleChildren(self::CH_ITEM_CATEGORY, self::KEY_ITEM_CATEGORY, array($vo->getOrigItemNumber()), $vo->getDismissed());
        $this->toggleChildren(self::CH_ITEM_CROSS_REF, self::KEY_ITEM_CROSS_REF, array($vo->getOrigItemNumber()), $vo->getDismissed());
        $this->toggleChildren(self::CH_ITEM_MFG_PART_NUM, self::KEY_ITEM_MFG_PART_NUM, array($vo->getOrigItemNumber()), $vo->getDismissed());
        $this->toggleChildren(self::CH_ITEM_TRANS_DEF_LOC, self::KEY_ITEM_TRANS_DEF_LOC, array($vo->getOrigItemNumber()), $vo->getDismissed());
        $this->toggleChildren(self::CH_ITEM_TRANS_DEF_SUB_INV, self::KEY_ITEM_TRANS_DEF_SUB_INV, array($vo->getOrigItemNumber()), $vo->getDismissed());
        $this->em->flush();

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "toggling to" . $vo->getDismissed() . " ItemHeader " . $id . " - " . $vo->getOrigItemNumber(), null, $vo->getCountry());

        $vo->validateCrossRef($this->em);
        $this->em->flush();

        redirect(base_url() . "Inventory/customEditCleansed/" . $vo->getCountry().'/'. $vo->getId());
    }

    public function validate($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_ITEM_HEADER, $id);
        $vo->validate($this->em);
        $this->em->flush();

        $this->validateChildren(self::CH_ITEM_CATEGORY, self::KEY_ITEM_CATEGORY, array($vo->getOrigItemNumber()));
        $this->validateChildren(self::CH_ITEM_CROSS_REF, self::KEY_ITEM_CROSS_REF, array($vo->getOrigItemNumber()));
        $this->validateChildren(self::CH_ITEM_MFG_PART_NUM, self::KEY_ITEM_MFG_PART_NUM, array($vo->getOrigItemNumber()));
        $this->validateChildren(self::CH_ITEM_TRANS_DEF_LOC, self::KEY_ITEM_TRANS_DEF_LOC, array($vo->getOrigItemNumber()));
        $this->validateChildren(self::CH_ITEM_TRANS_DEF_SUB_INV, self::KEY_ITEM_TRANS_DEF_SUB_INV, array($vo->getOrigItemNumber()));
        $this->em->flush();


        $vo->validateCrossRef($this->em);
        $this->em->flush();

        $vo2 = $this->getVOByID(self::VO_ITEM_HEADER, $id);


        $this->customEditCleansed($vo->getCountry(), $id);
    }


    public function associateCleansedToCleansed($parentId, $childId)
    {
        $parent = $this->getVOByID('Commons\\ORA\\ItemHeader', $parentId);

        if ($parent->getMergedId()!=null){
            show_error("A parent cannot be in merged state - check ItemHeader #" . $parent->getId().' - item j� tem seu pr�prio master');

        }

        $child = $this->getVOByID('Commons\\ORA\\ItemHeader', $childId);

        if ($child->getChildless()==false){
            show_error("A child cannot have children of its own - check ItemHeader #" . $child->getId().' - item � master de outro');

        }

        $backup = $this->getVOHidratedByID('Commons\\ORA\\ItemHeader', $childId);

        $child = $this->cloneByReflection($parent, $child);
        $child->setHasPicture($parent->getHasPicture());
        $child->setOrigItemNumber($backup['origItemNumber']);
        $child->setCountry($backup['country']);
        $child->setOrganizationCode($backup['organizationCode']);
        $child->setOrgName($backup['orgName']);
        $child->setMergedId($parent->getId());
        $child->setChildless(true);
        $child->setCleansed(true);

        if ($child->getCountry() == $parent->getCountry()){
            $child->setSameCountryParent(true);
            $child->setDismissed(true);
            $this->em->flush();

            $child->validateCrossRef($this->em);
            $this->em->flush();


            $this->reparentChildren(self::CH_ITEM_CATEGORY, $child->getOrigItemNumber(), $parent->getItemNumber());
            $this->dismissChildren(self::CH_ITEM_CATEGORY, $child->getOrigItemNumber());

            $this->reparentChildren(self::CH_ITEM_MFG_PART_NUM, $child->getOrigItemNumber(), $parent->getItemNumber());
            $this->dismissChildren(self::CH_ITEM_MFG_PART_NUM, $child->getOrigItemNumber());

            $this->reparentChildren(self::CH_ITEM_TRANS_DEF_SUB_INV, $child->getOrigItemNumber(), $parent->getItemNumber());
            $this->dismissChildren(self::CH_ITEM_TRANS_DEF_SUB_INV, $child->getOrigItemNumber());

            $this->reparentChildren(self::CH_ITEM_TRANS_DEF_LOC, $child->getOrigItemNumber(), $parent->getItemNumber());
            $this->dismissChildren(self::CH_ITEM_TRANS_DEF_LOC, $child->getOrigItemNumber());

        }else{
            $child->setSameCountryParent(false);
            $child->setDismissed(false);
            $this->em->flush();

            $child->validateCrossRef($this->em);
            $this->em->flush();

            $this->reparentChildren(self::CH_ITEM_MFG_PART_NUM, $child->getOrigItemNumber(), $parent->getItemNumber());
            $this->dismissChildren(self::CH_ITEM_MFG_PART_NUM, $child->getOrigItemNumber());

            $this->reparentChildren(self::CH_ITEM_TRANS_DEF_SUB_INV, $child->getOrigItemNumber(), $parent->getItemNumber());

            $this->reparentChildren(self::CH_ITEM_CATEGORY, $child->getOrigItemNumber(), $parent->getItemNumber());

            $this->reparentChildren(self::CH_ITEM_TRANS_DEF_LOC, $child->getOrigItemNumber(), $parent->getItemNumber());
            $this->dismissChildren(self::CH_ITEM_TRANS_DEF_LOC, $child->getOrigItemNumber());

        }

        $this->reparentChildren(self::CH_ITEM_CROSS_REF, $child->getOrigItemNumber(), $parent->getItemNumber());

        $parent->setChildless(false);
        $this->em->flush();

        $parent->validateCrossRef($this->em);
        $this->em->flush();

        // fix item family
        $qb = $this->em->createQueryBuilder();

        $qb->select('parent')
            ->from(self::CH_ITEM_CATEGORY, 'parent')
            ->andWhere(
                $qb->expr()->eq('parent.origItemNumber', '?1')
            )
            ->setParameter(1, $parent->getOrigItemNumber());

        $parentItemCategories = $qb->getQuery()->getResult();

        foreach($parentItemCategories as $parentItemCategory){
            $qb = $this->em->createQueryBuilder();

            $qb->select('child')
                ->from(self::CH_ITEM_CATEGORY, 'child')
                ->andWhere(
                    $qb->expr()->eq('child.itemNumber', '?1'),
                    $qb->expr()->eq('child.categorySetName', '?2'),
                    $qb->expr()->isNull('child.mergedId')

                )
                ->setParameter(1, $parent->getItemNumber())
                ->setParameter(2, $parentItemCategory->getCategorySetName());

            $childrenItemCategories = $qb->getQuery()->getResult();

            foreach($childrenItemCategories as $childItemCategory) {
                $childItemCategory->setConcatCategoryName($parentItemCategory->getConcatCategoryName());
            }

        }

        $this->em->flush();

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "associating cleansed child ItemHeader #" . $child->getId() . " to cleansed parent ItemHeader #" . $parent->getId(), null, $child->getCountry());

        redirect(base_url() . "Inventory/customEditCleansed/" . $child->getCountry() . '/' . $child->getId());
    }


    public function associateDirtyToCleansed($cleansedId, $dirtyId)
    {
        // this kind of association is different from a regular  cleansing
        //

        $qb = $this->em->createQueryBuilder();
        $qb->select('parent')
            ->from(self::VO_ITEM_HEADER, 'parent')
            ->where($qb->expr()->eq('parent.id', '?1'))
            ->setParameter(1, $cleansedId);

        $parent = $qb->getQuery()->getSingleResult();

        if ($parent->getMergedId()!=null){
            show_error("A parent cannot be in merged state - check ItemHeader #" . $parent->getId());

        }

        $qb = $this->em->createQueryBuilder();
        $qb->select('child')
            ->from(self::VO_PRE_ITEM_HEADER, 'child')
            ->where($qb->expr()->eq('child.id', '?1'))
            ->setParameter(1, $dirtyId);

        $pre = $qb->getQuery()->getSingleResult();

        $merged = self::VO_ITEM_HEADER;
        $merged = new $merged;
        $merged = $this->cloneByReflection($parent, $merged);

        $merged->setOrigItemNumber($pre->getOrigItemNumber());
        $merged->setItemNumber($parent->getItemNumber());
        $merged->setMergedId($parent->getId());
        $merged->setHasPicture($parent->getHasPicture());
        $merged->setCountry($pre->getCountry());
        $merged->setOrganizationCode($pre->getOrganizationCode());
        $merged->setOrgName($pre->getOrgName());
        $merged->setDismissed(false);
        $merged->setSameCountryParent($pre->getCountry() == $parent->getCountry());

        $merged->setChildless(true);

        if ($pre->getCountry() == $parent->getCountry()){
            $merged->setSameCountryParent(true);
            $merged->setDismissed(true);
            $this->em->persist($merged);
            $this->em->flush();

            $this->moveChildrenToStage(self::CH_PRE_ITEM_CATEGORY, self::CH_ITEM_CATEGORY, self::KEY_ITEM_CATEGORY, array($pre->getOrigItemNumber()));
            $this->reparentChildren(self::CH_ITEM_CATEGORY, $pre->getOrigItemNumber(), $parent->getItemNumber());
            $this->dismissChildren(self::CH_ITEM_CATEGORY, $pre->getOrigItemNumber());

            $this->moveChildrenToStage(self::CH_PRE_ITEM_MFG_PART_NUM, self::CH_ITEM_MFG_PART_NUM, self::KEY_ITEM_MFG_PART_NUM, array($pre->getOrigItemNumber()));
            $this->reparentChildren(self::CH_ITEM_MFG_PART_NUM, $pre->getOrigItemNumber(), $parent->getItemNumber());
            $this->dismissChildren(self::CH_ITEM_MFG_PART_NUM, $pre->getOrigItemNumber());

            $this->moveChildrenToStage(self::CH_PRE_ITEM_TRANS_DEF_SUB_INV,self::CH_ITEM_TRANS_DEF_SUB_INV, self::KEY_ITEM_TRANS_DEF_SUB_INV, array($pre->getOrigItemNumber()));
            $this->reparentChildren(self::CH_ITEM_TRANS_DEF_SUB_INV, $pre->getOrigItemNumber(), $parent->getItemNumber());
            $this->dismissChildren(self::CH_ITEM_TRANS_DEF_SUB_INV, $pre->getOrigItemNumber());

            $this->moveChildrenToStage(self::CH_PRE_ITEM_TRANS_DEF_LOC, self::CH_ITEM_TRANS_DEF_LOC, self::KEY_ITEM_TRANS_DEF_LOC, array($pre->getOrigItemNumber()));
            $this->reparentChildren(self::CH_ITEM_TRANS_DEF_LOC, $pre->getOrigItemNumber(), $parent->getItemNumber());
            $this->dismissChildren(self::CH_ITEM_TRANS_DEF_LOC, $pre->getOrigItemNumber());

        }else{
            $merged->setSameCountryParent(false);
            $merged->setDismissed(false);
            $this->em->persist($merged);
            $this->em->flush();

            $this->moveChildrenToStage(self::CH_PRE_ITEM_MFG_PART_NUM, self::CH_ITEM_MFG_PART_NUM, self::KEY_ITEM_MFG_PART_NUM, array($pre->getOrigItemNumber()));
            $this->reparentChildren(self::CH_ITEM_MFG_PART_NUM, $pre->getOrigItemNumber(), $parent->getItemNumber());
            $this->dismissChildren(self::CH_ITEM_MFG_PART_NUM, $pre->getOrigItemNumber());

            $this->moveChildrenToStage(self::CH_PRE_ITEM_TRANS_DEF_SUB_INV,self::CH_ITEM_TRANS_DEF_SUB_INV, self::KEY_ITEM_TRANS_DEF_SUB_INV, array($pre->getOrigItemNumber()));
            $this->reparentChildren(self::CH_ITEM_TRANS_DEF_SUB_INV, $pre->getOrigItemNumber(), $parent->getItemNumber());

            $this->moveChildrenToStage(self::CH_PRE_ITEM_CATEGORY, self::CH_ITEM_CATEGORY, self::KEY_ITEM_CATEGORY, array($pre->getOrigItemNumber()));
            $this->reparentChildren(self::CH_ITEM_CATEGORY, $pre->getOrigItemNumber(), $parent->getItemNumber());

            $this->moveChildrenToStage(self::CH_PRE_ITEM_TRANS_DEF_LOC, self::CH_ITEM_TRANS_DEF_LOC, self::KEY_ITEM_TRANS_DEF_LOC, array($pre->getOrigItemNumber()));
            $this->reparentChildren(self::CH_ITEM_TRANS_DEF_LOC, $pre->getOrigItemNumber(), $parent->getItemNumber());
            $this->dismissChildren(self::CH_ITEM_TRANS_DEF_LOC, $pre->getOrigItemNumber());

        }

        $this->moveChildrenToStage(self::CH_PRE_ITEM_CROSS_REF, self::CH_ITEM_CROSS_REF, self::KEY_ITEM_CROSS_REF, array($pre->getOrigItemNumber()));
        $this->reparentChildren(self::CH_ITEM_CROSS_REF, $pre->getOrigItemNumber(), $parent->getItemNumber());

        $pre->setCleansed(true);
        $this->em->flush();

        $parent->setChildless(false);
        $this->em->flush();

        $merged->validateCrossRef($this->em);
        $this->em->flush();

        // fix item family
        $qb = $this->em->createQueryBuilder();

        $qb->select('parent')
            ->from(self::CH_ITEM_CATEGORY, 'parent')
            ->andWhere(
                $qb->expr()->eq('parent.origItemNumber', '?1')
            )
            ->setParameter(1, $parent->getOrigItemNumber());

        $parentItemCategories = $qb->getQuery()->getResult();

        foreach($parentItemCategories as $parentItemCategory){
            $qb = $this->em->createQueryBuilder();

            $qb->select('child')
                ->from(self::CH_ITEM_CATEGORY, 'child')
                ->andWhere(
                    $qb->expr()->eq('child.itemNumber', '?1'),
                    $qb->expr()->eq('child.categorySetName', '?2'),
                    $qb->expr()->isNull('child.mergedId')

                )
                ->setParameter(1, $parent->getItemNumber())
                ->setParameter(2, $parentItemCategory->getCategorySetName());

            $childrenItemCategories = $qb->getQuery()->getResult();

            foreach($childrenItemCategories as $childItemCategory) {
                $childItemCategory->setConcatCategoryName($parentItemCategory->getConcatCategoryName());
            }

        }

        $this->em->flush();

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "associating dirty child ItemHeader #" . $pre->getId() . " to cleansed parent ItemHeader #" . $parent->getId(), null, $merged->getCountry());

        $this->session->selCol = "lastUpdated";
        $this->session->selAsc = false;

        redirect(base_url() . "Inventory/customEditCleansed/" . $merged->getCountry(). '/'.$merged->getId());

    }

    public function cleanseDirty($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        /**
         * cleanse parent - clone header from pre-stage to stage table
         *
         */

        $qb = $this->em->createQueryBuilder();
        $qb->select('pch')
            ->from(self::VO_PRE_ITEM_HEADER, 'pch')
            ->where($qb->expr()->eq('pch.id', '?1'))
            ->setParameter(1, $id);

        $pre = $qb->getQuery()->getSingleResult();

        $stg = self::VO_ITEM_HEADER;
        $stg = new $stg;
        $stg = $this->cloneByReflection($pre, $stg);
        $stg->setMergedId(null);
        $stg->setChildless(true);
        $stg->setSameCountryParent(false);

        $this->em->persist($stg);
        $this->em->flush();
        $stg->setDismissed(false);

        $pre->setCleansed(true);
        $this->em->persist($pre);
        $this->em->flush();

        /**
         * cleanse children - clones dependencies from pre-stage to stage table
         *
         */
        $this->moveChildrenToStage(self::CH_PRE_ITEM_CATEGORY, self::CH_ITEM_CATEGORY, self::KEY_ITEM_CATEGORY, array($stg->getOrigItemNumber()));
        $this->moveChildrenToStage(self::CH_PRE_ITEM_CROSS_REF, self::CH_ITEM_CROSS_REF, self::KEY_ITEM_CROSS_REF, array($stg->getOrigItemNumber()));
        $this->moveChildrenToStage(self::CH_PRE_ITEM_MFG_PART_NUM, self::CH_ITEM_MFG_PART_NUM, self::KEY_ITEM_MFG_PART_NUM, array($stg->getOrigItemNumber()));
        $this->moveChildrenToStage(self::CH_PRE_ITEM_TRANS_DEF_LOC, self::CH_ITEM_TRANS_DEF_LOC, self::KEY_ITEM_TRANS_DEF_LOC, array($stg->getOrigItemNumber()));
        $this->moveChildrenToStage(self::CH_PRE_ITEM_TRANS_DEF_SUB_INV, self::CH_ITEM_TRANS_DEF_SUB_INV, self::KEY_ITEM_TRANS_DEF_SUB_INV, array($stg->getOrigItemNumber()));

        $this->dismissChildren(self::CH_PRE_ITEM_TRANS_DEF_LOC, $stg->getOrigItemNumber());

        $stg->validateCrossRef($this->em);
        $this->em->persist($stg);
        $this->em->flush();


        $this->session->selGroup='Inventory';
        $this->session->selCountry=strtolower($stg->getCountry());
        $this->session->selCol = "lastUpdated";
        $this->session->selAsc = false;

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "cleansing ItemHeader " . $id . " - " . $stg->getOrigItemNumber() . " - " . $stg->getOrigItemNumber(), null, $stg->getCountry());

        redirect(base_url() . "Inventory/customEditCleansed/" . $stg->getCountry() . "/" . $stg->getId());

    }

    public function discardCleansed($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $stg = $this->getVOByID(self::VO_ITEM_HEADER, $id);

        $pre = $this->getPreVO(self::VO_PRE_ITEM_HEADER, self::KEY_ITEM_CATEGORY, array($stg->getOrigItemNumber()));

        $this->em->remove($stg);
        $this->em->flush();

        $pre->setCleansed(false);
        $this->em->merge($pre);
        $this->em->flush();

        $this->discardChildren(self::CH_PRE_ITEM_CATEGORY, self::CH_ITEM_CATEGORY, self::KEY_ITEM_CATEGORY, array($stg->getOrigItemNumber()));
        $this->discardChildren(self::CH_PRE_ITEM_CROSS_REF, self::CH_ITEM_CROSS_REF, self::KEY_ITEM_CROSS_REF, array($stg->getOrigItemNumber()));
        $this->discardChildren(self::CH_PRE_ITEM_MFG_PART_NUM, self::CH_ITEM_MFG_PART_NUM, self::KEY_ITEM_MFG_PART_NUM, array($stg->getOrigItemNumber()));
        $this->discardChildren(self::CH_PRE_ITEM_TRANS_DEF_LOC, self::CH_ITEM_TRANS_DEF_LOC, self::KEY_ITEM_TRANS_DEF_LOC, array($stg->getOrigItemNumber()));
        $this->discardChildren(self::CH_PRE_ITEM_TRANS_DEF_SUB_INV, self::CH_ITEM_TRANS_DEF_SUB_INV, self::KEY_ITEM_TRANS_DEF_SUB_INV, array($stg->getOrigItemNumber()));

        $parent = null;
        if ($stg->getMergedId()!=null){
            $parent = $this->getVOByID(self::VO_ITEM_HEADER, $stg->getMergedId());
            $parent->checkForChildren($this->em);
            $this->em->merge($parent);
            $this->em->flush();

        }

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "discarding ItemHeader " . $id . " - " . $stg->getOrigItemNumber(), null, $stg->getCountry());

        $this->session->selCol = "lastUpdated";
        $this->session->selAsc = false;


        if ($parent==null){
            redirect(base_url() . 'Inventory/cleansed/' . $stg->getCountry());

        }else{
            redirect(base_url() . 'Inventory/cleansed/' . $parent->getCountry());


        }

    }


    public function editDirty($id)
    {
        $data = $this->getVOHidratedByID(self::VO_PRE_ITEM_HEADER, $id);

        $preConditions = $this->checkPreConditionsForPreItem($data);

        if ($preConditions['block']) {

            $data = $this->loadCommonData(
                $data,
                'Blocked (pre-STG) ItemHeader #' . $id,
                'Inventory/editDirty/' . $id,
                'Retry',
                '',
                self::CANCEL_DIRTY . $data['country']
            );

            $data['previousEdit'] = $preConditions['previousEdit'];
            $data['previousEdit']['validUntil'] = date('Y-m-d H:i:s', date_add($data['previousEdit']['stamp'], date_interval_create_from_date_string('5 minutes'))->getTimestamp());
            $data['timeOnServer'] = date('Y-m-d H:i:s', (new \DateTime('now'))->getTimestamp());

            $this->displayStandardLayout(self::VIEW_LAYOUTS[$data['country']]['blocked'], $data);

            return;
        }

        if ($data['cleansed']) {
            $qb = $this->em->createQueryBuilder();
            $qb->select('vo')
                ->from(self::VO_ITEM_HEADER, 'vo')
                ->where($qb->expr()->eq('vo.origItemNumber', '?1'))
                ->setParameter(1, $data['origItemNumber']);

            $stg = $qb->getQuery()->getSingleResult();

            redirect(base_url() . 'Inventory/customEditCleansed/' . $stg->getCountry() . '/' . $stg->getId());

        }

        // criteria: add weight 1 for "same ORIG_ITEM_NUMBER"
        //
        $origItemNumber = trim(substr($data['origItemNumber'], 3));

        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from(self::VO_ITEM_HEADER, 'vo')
            ->where($qb->expr()->like('vo.origItemNumber', '?1'))
            ->setParameter(1, '%' . $origItemNumber . '%');

        $data['relatedOrigItemNumber'] = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

        if (strlen($data['observations']>7)){
            $qb = $this->em->createQueryBuilder();
            $qb->select('vo')
                ->from(self::VO_ITEM_HEADER, 'vo')
                ->where($qb->expr()->like('vo.observations', '?1'))
                ->setParameter(1, '%' . trim(substr($data['observations'], 2)) . '%');

            $data['relatedCodigoInterno'] = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

        }

        $data['startsWithL'] = trim(substr($data['observations'], 0, 3)) == $data['country'] . "L";

        $data = $this->loadCommonData(
            $data,
            'Dirty (pre-stage) ItemHeader #' . $id,
            self::ACTION_CLEANSE . $data['id'],
            self::ACTION_CLEANSE_LABEL,
            '',
            self::CANCEL_DIRTY . $data['country']
        );

        $this->displayStandardLayout(self::VIEW_LAYOUTS[$data['country']]['dirty'], $data);
    }

    public function editCleansed($id)
    {
        $data = $this->getVOHidratedByID(self::VO_ITEM_HEADER, $id);

        if (!$this->checkPreConditionsForItem($data)) {
            show_error('please contact your system administrator');
        }

        $this->startEditSequence(null, $data);
    }

    public function customEditCleansed($country, $id)
    {
        $data = $this->getVOHidratedByID(self::VO_ITEM_HEADER, $id);

        if (!$this->checkPreConditionsForItem($data)) {
            show_error('please contact your system administrator');
        }


        $this->startEditSequence($country, $data);
    }

    public function uploadResource($id)
    {
        $data = $this->getVOHidratedByID(self::VO_ITEM_HEADER, $id);

        if (!$this->checkPreConditionsForItem($data)) {
            show_error('please contact your system administrator');
        }


        $data = $this->loadCommonData(
            $data,
            'Associate file with '.$data['itemNumber'],
            'Inventory/saveResource/'. $data['id'],
            'attach file',
            '',
            'Inventory/customEditCleansed/' . $data['country'].'/'. $data['id']
        );

        $this->displayStandardLayout(self::DEFAULT_LAYOUTS['uploadResource'], $data);
    }

    public function saveResource($id){
        $data = $this->getVOHidratedByID(self::VO_ITEM_HEADER, $id);

        $resourcesFolder = $this->doctrine->getItemFilesBaseTemplate() . '/resources';

        if (!file_exists($resourcesFolder)) {
            mkdir($resourcesFolder, 0777, true);
        }

        $itemResourcesFolder = $resourcesFolder.'/'.trim($data['itemNumber']);

        if (!file_exists($itemResourcesFolder)) {
            mkdir($itemResourcesFolder, 0777, true);
        }

        $config['upload_path']          = $itemResourcesFolder;
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 5120;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('userfile'))
        {
            $data['uploadError']= str_replace("<p>","", str_replace("</p>","",$this->upload->display_errors()));

            $data = $this->loadCommonData(
                $data,
                'Associate file with '.$data['itemNumber'],
                'Inventory/saveResource/'. $data['id'],
                'attach file',
                '',
                'Inventory/customEditCleansed/' . $data['country'].'/'. $data['id']
            );

            $this->displayStandardLayout(self::DEFAULT_LAYOUTS['uploadResource'], $data);

        }
        else
        {
            $data['upload_data']= $this->upload->data();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], 'uploaded resource ' . $data['upload_data']['file_name']. ' to item ' . trim($data['itemNumber']), null, $data['country']);
            redirect(base_url().'Inventory/customEditCleansed/' . $data['country'].'/'. $data['id']);
        }
    }

    public function removeResource($id, $resourceId){
        $data = $this->getVOHidratedByID(self::VO_ITEM_HEADER, $id);

        $resourceFile = $this->doctrine->getItemFilesBaseTemplate() . '/resources'.'/'.trim($data['itemNumber']).'/'.$resourceId;

        if (file_exists($resourceFile)) {
            $this->auditor->log($_SERVER['REMOTE_ADDR'], "removed resource ".$resourceId. " from item " . trim($data['itemNumber']), null, $data['country']);
            unlink($resourceFile);
        }

        redirect(base_url().'Inventory/customEditCleansed/' . $data['country'].'/'. $data['id']);

    }


    private function startEditSequence($country, $data)
    {
        $queryBuilder = $this->em->createQueryBuilder();

        try{
            $queryBuilder->select('ic')
                ->from(self::CH_ITEM_CATEGORY, 'ic')
                ->andWhere($queryBuilder->expr()->eq('ic.origItemNumber', '?1'))
                ->setParameter(1, $data['origItemNumber']);

            $data['itemCategory'] = $queryBuilder->getQuery()->getSingleResult(Query::HYDRATE_ARRAY);

            $query = $this->em->createQuery("SELECT DISTINCT scc.family, scc.familyESA FROM Commons\\MAPS\TKEFamilyLines scc ORDER BY scc.family ASC");
            $data['sccFamilies'] = $query->getResult(Query::HYDRATE_ARRAY);


            $queryBuilder = $this->em->createQueryBuilder();
            $queryBuilder->select('icr')
                ->from(self::CH_ITEM_CROSS_REF, 'icr')
                ->andWhere($queryBuilder->expr()->eq('icr.origItemNumber', '?1'))
                ->setParameter(1, $data['origItemNumber']);

            $data['itemCrossRef'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);


            $queryBuilder = $this->em->createQueryBuilder();
            $queryBuilder->select('impn')
                ->from(self::CH_ITEM_MFG_PART_NUM, 'impn')
                ->andWhere($queryBuilder->expr()->eq('impn.origItemNumber', '?1'))
                ->setParameter(1, $data['origItemNumber']);

            $data['itemMfgPartNum'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);


            $queryBuilder = $this->em->createQueryBuilder();
            $queryBuilder->select('itdsi')
                ->from(self::CH_ITEM_TRANS_DEF_SUB_INV, 'itdsi')
                ->andWhere($queryBuilder->expr()->eq('itdsi.origItemNumber', '?1'))
                ->setParameter(1, $data['origItemNumber'])
                ->orderBy('itdsi.id', 'ASC');

            $data['itemTransDefSubInv'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);

            $queryBuilder = $this->em->createQueryBuilder();
            $queryBuilder->select('itdl')
                ->from(self::CH_ITEM_TRANS_DEF_LOC, 'itdl')
                ->andWhere($queryBuilder->expr()->eq('itdl.origItemNumber', '?1'))
                ->setParameter(1, $data['origItemNumber'])
                ->orderBy('itdl.id', 'ASC');

            $data['itemTransDefLoc'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);


            $queryBuilder = $this->em->createQueryBuilder();
            $queryBuilder->select('child')
                ->from(self::VO_ITEM_HEADER, 'child')
                ->andWhere($queryBuilder->expr()->eq('child.mergedId', '?1'))
                ->setParameter(1, $data['id'])
                ->orderBy('child.id', 'ASC');

            $data['mergedChildren'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);

            if ($country==null){
                $actionCustomByCountry = self::ACTION_BASE . $country .'/'.$data['id'];
                $layoutByCountry = self::VIEW_LAYOUTS['PA']['base'];
                $title = 'Edit all fields of Item Header #' . $data['id'];

            }else{
                $actionCustomByCountry = self::ACTION_CUSTOM . $country .'/'.$data['id'];

                if (isset($data['mergedId'])) {
                    $queryBuilder = $this->em->createQueryBuilder();
                    $queryBuilder->select('parent')
                        ->from(self::VO_ITEM_HEADER, 'parent')
                        ->andWhere($queryBuilder->expr()->eq('parent.id', '?1'))
                        ->setParameter(1, $data['mergedId']);

                    $data['parent'] = $queryBuilder->getQuery()->getSingleResult(Query::HYDRATE_ARRAY);

                    $layoutByCountry = self::VIEW_LAYOUTS[$country]['mergedCleansed'];
                    $title = 'Edit child Item Header #' . $data['id'];

                }else{
                    $layoutByCountry = self::VIEW_LAYOUTS[$country]['customCleansed'];

                    if ($data['childless']){
                        $title = 'Edit Item Header #' . $data['id'];

                    }else{
                        $title = 'Edit Master Header #' . $data['id'];

                    }

                }

            }

            $data = $this->loadCommonData(
                $data,
                $title,
                $actionCustomByCountry,
                self::ACTION_LABEL,
                self::DISCARD . $data['id'],
                self::CANCEL . $data['country']
            );

            $data['associatedFiles'] = [];

            $itemResourceFolder = $this->doctrine->getItemFilesBaseTemplate() . '/resources/'.trim($data['itemNumber']).'/';

            if (is_dir($itemResourceFolder)){
                // open directory for reading
                $d = new DirectoryIterator($itemResourceFolder) or die("getFileList: Failed opening directory $itemResourceFolder for reading");
                foreach($d as $fileinfo) {
                    // skip hidden files
                    if($fileinfo->isDot()) continue;
                    if((strtolower($fileinfo->getExtension())!='jpeg')&&(strtolower($fileinfo->getExtension())!='jpg')&&(strtolower($fileinfo->getExtension())!='gif')&&(strtolower($fileinfo->getExtension())!='png')) continue;

                    $data['associatedFiles'][] = [
                        'name' => "{$fileinfo}",
                        'type' => ($fileinfo->getType() == "dir") ? "dir" : mime_content_type($fileinfo->getRealPath()),
                        'size' => $fileinfo->getSize(),
                        'lastmod' => $fileinfo->getMTime()
                    ];
                }

            }

            $this->displayStandardLayout($layoutByCountry, $data);

        }catch(\Doctrine\ORM\NonUniqueResultException $e){
            $data = $this->loadCommonData(
                $data,
                'Consistency problems '.$e->getMessage(),
                '',
                self::ACTION_LABEL,
                '',
                self::CANCEL . $data['country']
            );


            $data['message']['PT'] = 'problemas com este registro, copie e cole os dados a seguir, envie por email para limpeza manual diretamente no banco';
            $data['message']['EN'] = 'consistency problems with this record, copy and paste the info, send it by email for manual database cleansing';

            $queryBuilder->select('h.id, h.origItemNumber')
                ->from(self::VO_ITEM_HEADER, 'h')
                ->andWhere($queryBuilder->expr()->eq('h.origItemNumber', '?1'))
                ->distinct()
                ->setParameter(1, $data['origItemNumber']);

            $data['headers'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);

            $queryBuilder->select('ic.id, ic.origItemNumber')
                ->from(self::CH_ITEM_CATEGORY, 'i')
                ->andWhere($queryBuilder->expr()->eq('ic.origItemNumber', '?1'))
                ->distinct()
                ->setParameter(1, $data['origItemNumber']);

            $data['itemCategories'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);

            $queryBuilder->select('icr.id, icr.origItemNumber')
                ->from(self::CH_ITEM_CROSS_REF, 'icr')
                ->andWhere($queryBuilder->expr()->eq('icr.origItemNumber', '?1'))
                ->distinct()
                ->setParameter(1, $data['origItemNumber']);

            $data['itemCrossRefs'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);

            $queryBuilder->select('im.id, im.origItemNumber')
                ->from(self::CH_ITEM_MFG_PART_NUM, 'im')
                ->andWhere($queryBuilder->expr()->eq('im.origItemNumber', '?1'))
                ->distinct()
                ->setParameter(1, $data['origItemNumber']);

            $data['itemMfgPartNums'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);

            $queryBuilder->select('il.id, il.origItemNumber')
                ->from(self::CH_ITEM_TRANS_DEF_LOC, 'il')
                ->andWhere($queryBuilder->expr()->eq('il.origItemNumber', '?1'))
                ->distinct()
                ->setParameter(1, $data['origItemNumber']);

            $data['itemTransDefLocs'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);

            $queryBuilder->select('isi.id, isi.origItemNumber')
                ->from(self::CH_ITEM_TRANS_DEF_SUB_INV, 'isi')
                ->andWhere($queryBuilder->expr()->eq('isi.origItemNumber', '?1'))
                ->distinct()
                ->setParameter(1, $data['origItemNumber']);

            $data['itemTransDefSubInvs'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);


            $this->displayStandardLayout(self::VIEW_LAYOUTS[$country]['error'], $data);

        }

    }

    /**
     * @param $id
     *
     * updates cleansed ItemHeader with ID (formBase)
     *
     * updateCleansed is paired with editCleansed
     *
     */
    public function updateCleansed($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->loadValidationRules(self::VO_ITEM_HEADER, $id, $this->form_validation);

        if ($this->form_validation->run() === FALSE) {
            $data = $this->getVOHidratedByID(self::VO_ITEM_HEADER, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data = $this->loadFormErrors(self::VO_ITEM_HEADER, $id, $data, $this->form_validation);

            $this->startEditSequence(null, $data);

        } else {
            $vo = $this->loadInstanceWithFormValues(self::VO_ITEM_HEADER, $id, $this->input);

            $this->em->merge($vo);
            $this->em->flush();


            $this->session->selCol = "lastUpdated";
            $this->session->selAsc = false;

            redirect(base_url() . "Inventory/cleansed/" . $vo->getCountry());
        }
    }

    public function customUpdateCleansed($country, $id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data = $this->getVOHidratedByID(self::VO_ITEM_HEADER, $id);

        switch ($country) {
            default:

                if (isset($data['mergedId'])){
                    $this->form_validation->set_rules('observations', 'observations', 'required|max_length[3000]');

                }else{
                    if ($this->input->post('templateName') == 'LA98') {
                        $this->form_validation->set_rules('itemNumber', 'itemNumber', 'required|max_length[16]');
                    }
                    $this->form_validation->set_rules('description', 'description', 'required|max_length[240]');
                    $this->form_validation->set_rules('descriptionEsa', 'descriptionEsa', 'required|max_length[240]');
                    $this->form_validation->set_rules('templateName', 'templateName', 'max_length[50]');
                    $this->form_validation->set_rules('uomCode', 'uomCode', 'max_length[3]|in_list[AMT,CAD,EUR,GBP,USD,cm,ft,in,m,mm,BWK,HYR,10p,box,dz,E00,E10,ea,PR,DAY,HR,MIN,MTH,QTR,Sec,WK,YR,IGl,l,UGl,g,kg,Lb,Oz]');
                    $this->form_validation->set_rules('weightUomCode', 'weightUomCode', 'max_length[3]|in_list[g,kg,Lb,Oz]');
                    $this->form_validation->set_rules('volumeUomCode', 'volumeUomCode', 'max_length[3]|in_list[IGl,l,UGl]');
                    $this->form_validation->set_rules('longDescription', 'longDescription', 'max_length[4000]');
                    $this->form_validation->set_rules('itemInvApplication', 'itemInvApplication', 'max_length[50]|in_list[AR,INV,PO]');
                    $this->form_validation->set_rules('transactionConditionCode', 'transactionConditionCode', 'max_length[80]');
                    $this->form_validation->set_rules('adjustmentAccount', 'adjustmentAccount', 'max_length[100]');
                    $this->form_validation->set_rules('correctionAccount', 'correctionAccount', 'max_length[100]');
                    $this->form_validation->set_rules('salesCostAccount', 'salesCostAccount', 'max_length[100]');
                    $this->form_validation->set_rules('fourDigitCode', 'fourDigitCode', 'max_length[4]');

                    $this->form_validation->set_rules('listPricePerUnit', 'listPricePerUnit', 'greater_than[0]');
                    $this->form_validation->set_rules('minMinmaxQuantity', 'minMinmaxQuantity', 'greater_than_equal_to[0]');
                    $this->form_validation->set_rules('maxMinmaxQuantity', 'maxMinmaxQuantity', 'greater_than_equal_to[0]');


                }
                break;

        }

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data = $this->loadFormErrors(self::VO_ITEM_HEADER, $id, $data, $this->form_validation);

            $data['errorListPricePerUnit'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('listPricePerUnit') ));
            $data['errorMaxMinmaxQuantity'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('maxMinmaxQuantity') ));
            $data['errorMinMinmaxQuantity'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('minMinmaxQuantity') ));

            $data['buildingType'] = $this->input->post('buildingType');
            $data['errorBuildingType'] = str_replace("</p>", "", str_replace("<p>", "", $this->form_validation->error('buildingType')));

            $data['buildingClassification'] = $this->input->post('buildingClassification');
            $data['errorBuildingClassification'] = str_replace("</p>", "", str_replace("<p>", "", $this->form_validation->error('buildingClassification')));

            $this->startEditSequence($country, $data);

        } else {
            $vo = $this->loadInstanceWithFormValues(self::VO_ITEM_HEADER, $id, $this->input);
            $vo->setFamily($this->input->post('family'));
            $vo->setSubFamily($this->input->post('subFamily'));
            $vo->setLine($this->input->post('line'));

            $this->em->flush();

            $qb = $this->em->createQueryBuilder();
            $qb->select('ic')
                ->from(self::CH_ITEM_CATEGORY, 'ic')
                ->andWhere(
                    $qb->expr()->eq('ic.origItemNumber', '?1'),
                    $qb->expr()->eq('ic.categorySetName', '?2'))
                ->setParameter(1, $vo->getOrigItemNumber())
                ->setParameter(2, 'TKE PO Item Category');

            $itemCategory = $qb->getQuery()->getSingleResult();

            $itemCategory->setConcatCategoryName($this->input->post('itemCategoryName'));
            $this->em->merge($itemCategory);
            $this->em->flush();


            // fix item family
            $qb = $this->em->createQueryBuilder();

            $qb->select('child')
                ->from(self::CH_ITEM_CATEGORY, 'child')
                ->andWhere(
                    $qb->expr()->eq('child.itemNumber', '?1'),
                    $qb->expr()->eq('child.categorySetName', '?2')

                )
                ->setParameter(1, $vo->getItemNumber())
                ->setParameter(2, 'TKE PO Item Category');

            $childrenItemCategories = $qb->getQuery()->getResult();

            foreach($childrenItemCategories as $childItemCategory) {
                $childItemCategory->setConcatCategoryName($itemCategory->getConcatCategoryName());
            }
            $this->em->flush();

            $vo->validateCrossRef($this->em);
            $this->em->flush();


            $this->auditor->log($_SERVER['REMOTE_ADDR'], "edit of ItemHeader " . $id . " - " . $vo->getOrigItemNumber(), null, $vo->getCountry());

            $this->session->selGroup='Inventory';
            $this->session->selCountry=strtolower($country);
            $this->session->selCol = "lastUpdated";
            $this->session->selAsc = false;

            redirect(base_url() . 'Inventory/cleansed/' . $vo->getCountry());
        }
    }


}
