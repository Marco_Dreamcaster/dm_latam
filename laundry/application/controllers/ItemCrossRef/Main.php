<?php
include_once APPPATH . 'controllers/MY_Controller.php';

/**
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/query-builder.html
 *
 * http://www.doctrine-project.org/api/orm/2.5/class-Doctrine.ORM.QueryBuilder.html
 *
 * ItemCrossRef Controller (detail style)
 *
 */

Use Doctrine\ORM\Query;


class Main extends MY_Controller {

    const VO_CLASS_NAME = 'Commons\\ORA\\ItemCrossRef';
    const VO_PRE_CLASS_NAME = 'Commons\\RAW\\PreItemCrossRef';

    const HEADER1_CLASS_NAME = 'Commons\\ORA\\ItemHeader';
    const HEADER1_KEY_COLUMNS = array('origItemNumber');
    const CANCEL1 = "Inventory/customEditCleansed/";

    const ACTION_BASE = 'ItemCrossRef/updateCleansed/';
    const ACTION_CUSTOM = 'ItemCrossRef/customUpdateCleansed/';
    const ACTION_LABEL = 'Update ItemCrossRef';

    const LAYOUT_BASE = array(
        'ORA/ItemCrossRef/cleansedTopBar.tpl',
        'ORA/ItemCrossRef/formBase.tpl'
    );

    const LAYOUT_CUSTOM = array(
        'ORA/ItemCrossRef/cleansedTopBar.tpl',
        'ORA/ItemCrossRef/formCustom.tpl'
    );

    const LAYOUT_CUSTOM_CO = array(
        'ORA/ItemCrossRef/cleansedTopBar.tpl',
        'ORA/ItemCrossRef/formCustomCO.tpl'
    );

    const LAYOUT_CONFIRM = array(
        'ORA/ItemCrossRef/formConfirm.tpl'
    );

    const LAYOUT_SEARCH = array(
        'ORA/ItemCrossRef/formSearch.tpl'
    );


    public function __construct()
    {
        parent::__construct();

        $this->load->helper('form');
        $this->load->library('form_validation');

    }


    /**
     * functions prefixed by 'confirm" display a confirmation step
     *
     */
    public function confirmAssociationCleansedToCleansed($cleansed1Id, $cleansed2Id){
        if ($this->checkPreConditions($cleansed1Id)) {
            show_error('please contact your system administrator');
        }

        $data = array();
        $data['child'] = $this->getVOHidratedByID('Commons\\ORA\\ItemHeader', $cleansed1Id);
        $data['parent'] = $this->getVOHidratedByID('Commons\\ORA\\ItemHeader', $cleansed2Id);

        $data['title'] = "Confirm Association child Item #".$data['child']['id']." to master Item #".$data['parent']['id'];
        $data = $this->loadSessionNavigation($data);

        $data['confirmLabel'] = "confirm merge of items";
        $data['confirmAction'] = "Inventory/associateCleansedToCleansed/".$data['parent']['id']."/".$data['child']['id'];
        $data['cancelAction'] = "Inventory/customEditCleansed/".$data['child']['country'].'/'.$data['child']['id'];


        $this->displayStandardLayout(self::LAYOUT_CONFIRM, $data);

    }

    public function confirmAssociationDirtyToCleansed($dirtyId, $cleansedId){
        if ($this->checkPreConditions($cleansedId)) {
            show_error('please contact your system administrator');
        }

        $data = array();
        $data['parent'] = $this->getVOHidratedByID('Commons\\ORA\\ItemHeader', $cleansedId);
        $data['child'] = $this->getVOHidratedByID('Commons\\RAW\\PreItemHeader', $dirtyId);

        $data['actionPeekParentUrl'] = "Inventory/customEditCleansed/".$data['parent']['country']."/".$data['parent']['id'];
        $data['actionPeekChildUrl'] = "Inventory/editDirty/".$data['child']['id'];

        $data['title'] = "Confirm Association for dirty Item #".$data['child']['id'];
        $data = $this->loadSessionNavigation($data);

        $data['confirmLabel'] = "confirm merge of items";
        $data['confirmAction'] = "Inventory/associateDirtyToCleansed/".$data['parent']['id']."/".$data['child']['id'];
        $data['cancelAction'] = "Inventory/editDirty/".$data['child']['id'];

        $this->displayStandardLayout(self::LAYOUT_CONFIRM, $data);

    }

    /**
     * functions prefixed by 'search" display a search page
     *
     */
    public function searchAssociationForDirty($dirtyId){
        if ($this->checkPreConditions($dirtyId)) {
            show_error('please contact your system administrator');
        }

        $data = array();
        $data['source'] = $this->getVOHidratedByID('Commons\\RAW\\PreItemHeader', $dirtyId);

        $data['title'] = "Search Association for dirty Item #".$data['source']['id'];
        $data['action'] = "ItemCrossRef/searchAssociationDirtyToCleansed/".$data['source']['id'];
        $data['cancelAction'] = "Inventory/editDirty/".$data['source']['id'];

        $data = $this->loadSessionNavigation($data);

        $this->form_validation->set_rules('keywords', 'keywords', 'required|min_length[3]');
        $data['keywords'] = $this->input->post('keywords');

        if ($this->form_validation->run() === FALSE) {
            $data['errorKeywords'] = str_replace('</p>','', str_replace('<p>','', $this->form_validation->error('keywords')));

        }else{
            $data['results'] = array();

            $keyword = trim($this->input->post('keywords'));

            $qb = $this->em->createQueryBuilder();
            $qb->select('target')
                ->from('Commons\ORA\ItemHeader', 'target')
                ->andWhere(
                    $qb->expr()->isNull('target.mergedId'),
                    $qb->expr()->eq('target.dismissed', '?1'),
                    $qb->expr()->orX(
                        $qb->expr()->like('target.origItemNumber', '?2'),
                        $qb->expr()->like('target.itemNumber', '?3'),
                        $qb->expr()->like('target.description', '?4'),
                        $qb->expr()->like('target.descriptionEsa', '?5'),
                        $qb->expr()->like('target.observations', '?6')
                    )
                )
                ->setParameter(1, false)
                ->setParameter(2, '%'.$keyword.'%')
                ->setParameter(3, '%'.$keyword.'%')
                ->setParameter(4, '%'.$keyword.'%')
                ->setParameter(5, '%'.$keyword.'%')
                ->setParameter(6, '%'.$keyword.'%')
                ->addOrderBy('target.recommended',' DESC')
                ->addOrderBy('target.itemNumber','ASC');;

            $rawResults = $qb->getQuery()->getResult();

            foreach($rawResults as $rawResult){
                array_push($data['results'], array( 'country'=>$rawResult->getCountry(),
                                                    'detail'=>$rawResult->getOrigItemNumber().' - '.$rawResult->getItemNumber().' - '.$rawResult->getDescription(),
                                                    'actionUrl'=>'ItemCrossRef/confirmAssociationDirtyToCleansed/'.'/'.$dirtyId.'/'.$rawResult->getId(),
                                                    'actionPeekUrl'=>'Inventory/customEditCleansed/'.$rawResult->getCountry().'/'.$rawResult->getId()));

            }

        }

        $this->displayStandardLayout(self::LAYOUT_SEARCH, $data);

    }

    public function searchAssociationForCleansed($cleansedId){
        if ($this->checkPreConditions($cleansedId)) {
            show_error('please contact your system administrator');
        }

        $data = array();
        $data['source'] = $this->getVOHidratedByID('Commons\\ORA\\ItemHeader', $cleansedId);

        $data['title'] = "Search Association for cleansed Item #".$data['source']['id'];
        $data['action'] = "ItemCrossRef/searchAssociationCleansedToCleansed/".$data['source']['id'];
        $data['cancelAction'] = "Inventory/customEditCleansed/".$data['source']['country'].'/'.$data['source']['id'];

        $data = $this->loadSessionNavigation($data);

        $this->form_validation->set_rules('keywords', 'keywords', 'required|min_length[3]');
        $data['keywords'] = $this->input->post('keywords');

        if ($this->form_validation->run() === FALSE) {
            $data['errorKeywords'] = str_replace('</p>','', str_replace('<p>','', $this->form_validation->error('keywords')));

        }else{
            $data['results'] = array();

            $keyword = trim($this->input->post('keywords'));

            $qb = $this->em->createQueryBuilder();
            $qb->select('target')
                ->from('Commons\ORA\ItemHeader', 'target')
                ->andWhere(
                    $qb->expr()->isNull('target.mergedId'),
                    $qb->expr()->eq('target.dismissed', '?1'),
                    $qb->expr()->orX(
                        $qb->expr()->like('target.origItemNumber', '?2'),
                        $qb->expr()->like('target.itemNumber', '?3'),
                        $qb->expr()->like('target.description', '?4'),
                        $qb->expr()->like('target.descriptionEsa', '?5'),
                        $qb->expr()->like('target.observations', '?6')
                    )
                )
                ->setParameter(1, false)
                ->setParameter(2, '%'.$keyword.'%')
                ->setParameter(3, '%'.$keyword.'%')
                ->setParameter(4, '%'.$keyword.'%')
                ->setParameter(5, '%'.$keyword.'%')
                ->setParameter(6, '%'.$keyword.'%')
                ->addOrderBy('target.recommended',' DESC')
                ->addOrderBy('target.itemNumber','ASC');;

            $rawResults = $qb->getQuery()->getResult();

            foreach($rawResults as $rawResult){
                if ($rawResult->getOrigItemNumber()!= $data['source']['origItemNumber']){
                    array_push($data['results'], array( 'country'=>$rawResult->getCountry(),
                        'detail'=>$rawResult->getOrigItemNumber().' - '.$rawResult->getItemNumber().' - '.$rawResult->getDescription(),
                        'actionUrl'=>'ItemCrossRef/confirmAssociationCleansedToCleansed/'.'/'.$cleansedId.'/'.$rawResult->getId(),
                        'actionPeekUrl'=>'Inventory/customEditCleansed/'.$rawResult->getCountry().'/'.$rawResult->getId()));

                }

            }

        }

        $this->displayStandardLayout(self::LAYOUT_SEARCH, $data);
    }


    public function editCleansed($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence(null, $data);
    }

    public function customEditCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence($country, $data);
    }

    public function toggle($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->setDismissed(!$vo->getDismissed());
        $this->em->merge($vo);
        $this->em->flush();

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($vo->getOrigItemNumber()))->getSingleResult();

        redirect(base_url() . self::CANCEL1 . $header->getCountry(). '/'.$header->getId());
    }



    private function startEditSequence($country, $data){
        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($data['origItemNumber']))->getSingleResult();

        switch($country){
            case 'PA':
                $actionCustomByCountry = self::ACTION_CUSTOM . 'PA/' . $data['id'];
                $layoutByCountry = self::LAYOUT_CUSTOM;
                $cancelAction = self::CANCEL1.'PA/'.$header->getId();
                break;

            default:
                $actionCustomByCountry = self::ACTION_CUSTOM . $country.'/'. $data['id'];
                $layoutByCountry = self::LAYOUT_CUSTOM_CO;
                $cancelAction = self::CANCEL1.$country.'/'.$header->getId();
                break;
        }


        $data = $this->loadCommonData(
            $data,
            'Edit cleansed ItemCrossRef #' . $data['id'],
            $actionCustomByCountry,
            self::ACTION_LABEL,
            '',
            $cancelAction
        );

        $this->displayStandardLayout($layoutByCountry, $data);
    }

    public function updateCleansed($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->loadValidationRules(self::VO_CLASS_NAME, $id, $this->form_validation);

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence(null, $data);

        }else{
            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);

            $this->em->merge($vo);

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getorigItemNumber()))->getSingleResult();

            $header->validateCrossRef($this->em);

            $this->em->flush();

            redirect(base_url() . self::CANCEL1 . $header->getCountry().'/'.$header->getId());
        }
    }

    public function customUpdateCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        // uncomment rules that fit the custom ItemCrossRef form
        switch($country){
            case 'PA':
                //$this->form_validation->set_rules('origItemNumber', 'origItemNumber', 'required|max_length[16]');
                //$this->form_validation->set_rules('itemNumber', 'itemNumber', 'required|max_length[16]');
                //$this->form_validation->set_rules('crossReferenceType', 'crossReferenceType', 'required|max_length[25]');
                //$this->form_validation->set_rules('crossReference', 'crossReference', 'required|max_length[25]');
                //$this->form_validation->set_rules('description', 'description', 'max_length[240]');
                //$this->form_validation->set_rules('organizationCode', 'organizationCode', 'max_length[3]');
                //$this->form_validation->set_rules('orgName', 'orgName', 'max_length[60]');
                break;

            case 'CO':
                //$this->form_validation->set_rules('origItemNumber', 'origItemNumber', 'required|max_length[16]');
                //$this->form_validation->set_rules('itemNumber', 'itemNumber', 'required|max_length[16]');
                //$this->form_validation->set_rules('crossReferenceType', 'crossReferenceType', 'required|max_length[25]');
                //$this->form_validation->set_rules('crossReference', 'crossReference', 'required|max_length[25]');
                //$this->form_validation->set_rules('description', 'description', 'max_length[240]');
                //$this->form_validation->set_rules('organizationCode', 'organizationCode', 'max_length[3]');
                //$this->form_validation->set_rules('orgName', 'orgName', 'max_length[60]');
                break;

            case 'MX':
                //$this->form_validation->set_rules('origItemNumber', 'origItemNumber', 'required|max_length[16]');
                //$this->form_validation->set_rules('itemNumber', 'itemNumber', 'required|max_length[16]');
                //$this->form_validation->set_rules('crossReferenceType', 'crossReferenceType', 'required|max_length[25]');
                //$this->form_validation->set_rules('crossReference', 'crossReference', 'required|max_length[25]');
                //$this->form_validation->set_rules('description', 'description', 'max_length[240]');
                //$this->form_validation->set_rules('organizationCode', 'organizationCode', 'max_length[3]');
                //$this->form_validation->set_rules('orgName', 'orgName', 'max_length[60]');
                break;

            default:
                //$this->form_validation->set_rules('origItemNumber', 'origItemNumber', 'required|max_length[16]');
                //$this->form_validation->set_rules('itemNumber', 'itemNumber', 'required|max_length[16]');
                //$this->form_validation->set_rules('crossReferenceType', 'crossReferenceType', 'required|max_length[25]');
                //$this->form_validation->set_rules('crossReference', 'crossReference', 'required|max_length[25]');
                //$this->form_validation->set_rules('description', 'description', 'max_length[240]');
                //$this->form_validation->set_rules('organizationCode', 'organizationCode', 'max_length[3]');
                //$this->form_validation->set_rules('orgName', 'orgName', 'max_length[60]');
                break;
        }

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence($country, $data);

        }else{

            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);
            $this->em->merge($vo);

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getorigItemNumber()))->getSingleResult();

            $header->validateCrossRef($this->em);

            $this->em->flush();

            redirect(base_url() . self::CANCEL1 . $header->getCountry().'/'.$header->getId());
        }
    }
}