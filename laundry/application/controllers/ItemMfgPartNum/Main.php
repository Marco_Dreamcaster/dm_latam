<?php
include_once APPPATH . 'controllers/MY_Controller.php';

/**
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/query-builder.html
 *
 * http://www.doctrine-project.org/api/orm/2.5/class-Doctrine.ORM.QueryBuilder.html
 *
 * ItemMFGPartNum Controller (detail style)
 *
 */

Use Doctrine\ORM\Query;


class Main extends MY_Controller {

    const VO_CLASS_NAME = 'Commons\\ORA\\ItemMFGPartNum';
    const VO_PRE_CLASS_NAME = 'Commons\\RAW\\PreItemMFGPartNum';

    const HEADER1_CLASS_NAME = 'Commons\\ORA\\ItemHeader';
    const HEADER1_KEY_COLUMNS = array('origItemNumber');
    const CANCEL1 = "Inventory/customEditCleansed/";
    const ACTION_BASE = 'ItemMFGPartNum/updateCleansed/';
    const ACTION_CUSTOM = 'ItemMfgPartNum/customUpdateCleansed/';
    const ACTION_LABEL = 'Update ItemMFGPartNum';

    const LAYOUT_BASE = array(
        'ORA/ItemMFGPartNum/cleansedTopBar.tpl',
        'ORA/ItemMFGPartNum/formBase.tpl'
    );

    const LAYOUT_CUSTOM_DEFAULT = array(
        'ORA/ItemMFGPartNum/cleansedTopBar.tpl',
        'ORA/ItemMFGPartNum/formCustom.tpl'
    );

    const LAYOUT_ADD = array(
        'ORA/ItemMFGPartNum/formCustom.tpl'
    );

    public function __construct()
    {
        parent::__construct();

    }

    public function addNew($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME, array('id'),  array($id))->getSingleResult();

        // uncomment rules that fit the custom ItemMFGPartNum form
        switch($header->getCountry()){
            default:
                $this->form_validation->set_rules('manufacturer', 'manufacturer', 'required|max_length[30]');
                $this->form_validation->set_rules('description', 'description', 'max_length[240]');
                $this->form_validation->set_rules('partNumber', 'partNumber', 'required|max_length[30]');
                break;
        }

        if ($this->form_validation->run() === FALSE){
            $data = $this->generateNewItemMfgPartNumFor($header);

            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrorsForClassNameAndCountry(self::VO_CLASS_NAME, $data['country'], $data, $this->form_validation);
            $this->createNew($id, $data);

        }else{
            $vo = $this->createInstanceWithFormValues(self::VO_CLASS_NAME, $header->getCountry(), $this->input);

            $vo->setOrigItemNumber($header->getOrigItemNumber());
            $vo->setItemNumber($header->getItemNumber());
            $vo->setOrgName($header->getOrgName());
            $vo->setCountry($header->getCountry());
            $vo->setDismissed(false);

            $this->em->persist($vo);
            $this->em->flush();

            $header->validate($this->em);
            $this->em->flush();

            $header->validateCrossRef($this->em);
            $this->em->flush();

            $this->session->selCol = 'lastUpdated';
            $this->session->selAsc = false;

            redirect(base_url() . self::CANCEL1 . $header->getCountry().'/'.$header->getId());
        }


    }


    public function createNew($id,  $data=null)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME, array('id'), array($id))->getSingleResult();

        switch($header->getCountry()){
            default:
                $layoutByCountry = self::LAYOUT_ADD;
                $cancelAction = self::CANCEL1.$header->getCountry().'/'.$header->getId();
                break;
        }

        if (!isset($data)){
            $data = $this->generateNewItemMfgPartNumFor($header);
        }

        $actionCreateNewAddress = 'ItemMfgPartNum/addNew/' . $header->getId() ;

        $data = $this->loadCommonData(
            $data,
            'Add a new PartNum to Item #' . $header->getId(),
            $actionCreateNewAddress,
            'Create new PartNum to Item ' . $header->getOrigItemNumber(),
            '',
            $cancelAction
        );

        $this->displayStandardLayout($layoutByCountry, $data);
    }

    private function generateNewItemMfgPartNumFor($header){
        $data = array();
        $data['country'] = $header->getCountry();
        $data['itemNumber'] = $header->getItemNumber();
        $data['origItemNumber'] = $header->getOrigItemNumber();
        $data['manufacturer'] = '98';
        $data['orgName'] = $header->getOrgName();
        $data['id'] = 'NEW';

        return $data;
    }


    public function editCleansed($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence(null, $data);
    }

    public function customEditCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence($country, $data);
    }

    public function toggle($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->setDismissed(!$vo->getDismissed());
        $this->em->merge($vo);
        $this->em->flush();

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($vo->getOrigItemNumber()))->getSingleResult();

        redirect(base_url() . self::CANCEL1 . $header->getCountry(). '/'.$header->getId());
    }



    private function startEditSequence($country, $data){
        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($data['origItemNumber']))->getSingleResult();

        switch($country){
            default:
                $actionCustomByCountry = self::ACTION_CUSTOM . $country.'/'. $data['id'];
                $layoutByCountry = self::LAYOUT_CUSTOM_DEFAULT;
                $cancelAction = self::CANCEL1.$country.'/'.$header->getId();
                break;
        }


        $data = $this->loadCommonData(
            $data,
            'Edit cleansed ItemMFGPartNum #' . $data['id'],
            $actionCustomByCountry,
            self::ACTION_LABEL,
            '',
            $cancelAction
        );

        $this->displayStandardLayout($layoutByCountry, $data);
    }

    public function updateCleansed($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->loadValidationRules(self::VO_CLASS_NAME, $id, $this->form_validation);

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence(null, $data);

        }else{
            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);

            $this->em->merge($vo);

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getorigItemNumber()))->getSingleResult();

            $header->validateCrossRef($this->em);

            $this->em->flush();

            redirect(base_url() . self::CANCEL1 . $header->getCountry().'/'.$header->getId());
        }
    }

    public function customUpdateCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        // uncomment rules that fit the custom ItemMFGPartNum form
        switch($country){
            default:
                $this->form_validation->set_rules('manufacturer', 'manufacturer', 'required|max_length[30]');
                $this->form_validation->set_rules('description', 'description', 'max_length[240]');
                $this->form_validation->set_rules('partNumber', 'partNumber', 'required|max_length[30]');
                break;
        }

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence($country, $data);

        }else{

            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);
            $this->em->merge($vo);
            $this->em->flush();

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getorigItemNumber()))->getSingleResult();

            $header->validateCrossRef($this->em);
            $this->em->flush();

            redirect(base_url() . self::CANCEL1 . $header->getCountry().'/'.$header->getId());
        }
    }
}

