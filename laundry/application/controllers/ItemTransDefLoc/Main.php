<?php
include_once APPPATH . 'controllers/MY_Controller.php';

/**
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/query-builder.html
 *
 * http://www.doctrine-project.org/api/orm/2.5/class-Doctrine.ORM.QueryBuilder.html
 *
 * ItemTransDefLoc Controller (detail style)
 *
 */

Use Doctrine\ORM\Query;


class Main extends MY_Controller {

    const VO_CLASS_NAME = 'Commons\\ORA\\ItemTransDefLoc';
    const VO_PRE_CLASS_NAME = 'Commons\\RAW\\PreItemTransDefLoc';

    const ACTION_BASE = 'ItemTransDefLoc/updateCleansed/';
    const ACTION_CUSTOM = 'ItemTransDefLoc/customUpdateCleansed/';
    const ACTION_LABEL = 'Update ItemTransDefLoc';

    const HEADER1_CLASS_NAME = 'Commons\\ORA\\ItemHeader';
    const HEADER1_KEY_COLUMNS = array('origItemNumber');
    const CANCEL1 = "Inventory/customEditCleansed/";

    const HEADER2_CLASS_NAME = 'Commons\\ORA\\ItemTransDefSubInv';
    const HEADER2_KEY_COLUMNS = array('origItemNumber');

    const LAYOUT_BASE = array(
        'ORA/ItemTransDefLoc/cleansedTopBar.tpl',
        'ORA/ItemTransDefLoc/formBase.tpl'
    );

    const LAYOUT_CUSTOM = array(
        'ORA/ItemTransDefLoc/cleansedTopBar.tpl',
        'ORA/ItemTransDefLoc/formCustom.tpl'
    );

    const LAYOUT_CUSTOM_CO = array(
        'ORA/ItemTransDefLoc/cleansedTopBar.tpl',
        'ORA/ItemTransDefLoc/formCustom.tpl'
    );

    const LAYOUT_CUSTOM_MX = array(
        'ORA/ItemTransDefLoc/cleansedTopBar.tpl',
        'ORA/ItemTransDefLoc/formCustom.tpl'
    );

    public function __construct()
    {
        parent::__construct();

    }

    public function customEditCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($data['origItemNumber']))->getSingleResult();

        $data['header'] =  $header;

        $this->startEditSequence($country, $data);
    }

    public function toggle($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }


        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($vo->getOrigItemNumber()))->getSingleResult();

        $data['header'] =  $header;

        $vo->setDismissed(!$vo->getDismissed());
        $this->em->flush();

        $header2 = $this->getHeaderQuery(self::HEADER2_CLASS_NAME,
            self::HEADER2_KEY_COLUMNS,
            array($vo->getOrigItemNumber()))->getResult();

        foreach($header2 as $transDefLoc){
            $transDefLoc->validate($this->em);
            $this->em->flush();

        }



        $header->validateCrossRef($this->em);
        $this->em->flush();

        redirect(base_url() . self::CANCEL1 . $header->getCountry().'/'.$header->getId());

    }



    private function startEditSequence($country, $data){
        switch($country){
            default:
                $actionCustomByCountry = self::ACTION_CUSTOM . $country.'/' . $data['id'];
                $layoutByCountry = self::LAYOUT_CUSTOM;
                $cancelAction = self::CANCEL1. $country.'/'.$data['header']->getId();
                break;

        }


        $data = $this->loadCommonData(
            $data,
            'Edit cleansed ItemTransDefLoc #' . $data['id'],
            $actionCustomByCountry,
            self::ACTION_LABEL,
            '',
            $cancelAction
        );

        $this->displayStandardLayout($layoutByCountry, $data);
    }

    public function customUpdateCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($data['origItemNumber']))->getSingleResult();

        $data['header'] =  $header;

        // uncomment rules that fit the custom ItemTransDefLoc form
        switch($country){
            case 'PA':
                $this->form_validation->set_rules('locatorName', 'locatorName', 'required|max_length[100]');
                break;

            case 'CO':
                $this->form_validation->set_rules('locatorName', 'locatorName', 'required|max_length[100]');
                break;

            default:
                $this->form_validation->set_rules('locatorName', 'locatorName', 'required|max_length[100]');
                break;
        }

        if ($this->form_validation->run() === FALSE){
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence($country, $data);

        }else{
            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);
            $this->em->merge($vo);
            $this->em->flush();

            $header->validateCrossRef($this->em);
            $this->em->flush();

            redirect(base_url() . self::CANCEL1 . $header->getCountry().'/'.$header->getId());
        }
    }
}