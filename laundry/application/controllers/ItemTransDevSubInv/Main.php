<?php
include_once APPPATH . 'controllers/MY_Controller.php';

/**
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/query-builder.html
 *
 * http://www.doctrine-project.org/api/orm/2.5/class-Doctrine.ORM.QueryBuilder.html
 *
 * ItemTransDefSubInv Controller (detail style)
 *
 */

Use Doctrine\ORM\Query;


class Main extends MY_Controller {

    const VO_CLASS_NAME = 'Commons\\ORA\\ItemTransDefSubInv';
    const VO_PRE_CLASS_NAME = 'Commons\\RAW\\PreItemTransDefSubInv';

    const ACTION_BASE = 'ItemTransDefSubInv/updateCleansed/';
    const ACTION_CUSTOM = 'ItemTransDefSubInv/customUpdateCleansed/';
    const ACTION_LABEL = 'Update ItemTransDefSubInv';

    const LAYOUT_BASE = array(
        'ORA/ItemTransDefSubInv/cleansedTopBar.tpl',
        'ORA/ItemTransDefSubInv/formBase.tpl'
    );

    const LAYOUT_CUSTOM = array(
        'ORA/ItemTransDefSubInv/cleansedTopBar.tpl',
        'ORA/ItemTransDefSubInv/formCustom.tpl'
    );

    const LAYOUT_CUSTOM_CO = array(
        'ORA/ItemTransDefSubInv/cleansedTopBar.tpl',
        'ORA/ItemTransDefSubInv/formCustomCO.tpl'
    );

    public function __construct()
    {
        parent::__construct();

    }

    public function toggle($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->setDismissed(!$vo->getDismissed());
        $this->em->merge($vo);
        $this->em->flush();

    }

}