<?php
include_once APPPATH . 'controllers/MY_Controller.php';

Use Doctrine\ORM\Query;
Use Commons\AuditLog;

class Landing extends MY_Controller {

    const LAYOUT_WELCOME = array('landing.tpl');
    const LAYOUT_DASHBOARD = array('dashboard.tpl');

    const LAYOUT_STATS = array(
        'inventory' =>  array('Stats/inventory.tpl'),
        'customers' => array('Stats/common.tpl'),
        'vendors' => array('Stats/common.tpl'),
        'projects' => array('Stats/common.tpl')
        );

    const LAYOUT_PA_STATS = array(
        'inventory' =>  array('Stats/inventoryPA.tpl'),
        'customers' => array('Stats/common.tpl'),
        'vendors' => array('Stats/common.tpl'),
        'projects' => array('Stats/common.tpl')
    );

    const LAYOUT_MX_STATS = array(
        'inventory' =>  array('Stats/inventoryMX.tpl'),
        'customers' => array('Stats/common.tpl'),
        'vendors' => array('Stats/common.tpl'),
        'projects' => array('Stats/common.tpl')
    );

    const LAYOUT_SV_STATS = array(
        'inventory' =>  array('Stats/inventorySV.tpl'),
        'customers' => array('Stats/common.tpl'),
        'vendors' => array('Stats/common.tpl'),
        'projects' => array('Stats/common.tpl')
    );

    const LAYOUT_HN_STATS = array(
        'inventory' =>  array('Stats/inventoryHN.tpl'),
        'customers' => array('Stats/common.tpl'),
        'vendors' => array('Stats/common.tpl'),
        'projects' => array('Stats/common.tpl')
    );

    const LAYOUT_NI_STATS = array(
        'inventory' =>  array('Stats/inventoryNI.tpl'),
        'customers' => array('Stats/common.tpl'),
        'vendors' => array('Stats/common.tpl'),
        'projects' => array('Stats/common.tpl')
    );

    const LAYOUT_GT_STATS = array(
        'inventory' =>  array('Stats/inventoryGT.tpl'),
        'customers' => array('Stats/common.tpl'),
        'vendors' => array('Stats/common.tpl'),
        'projects' => array('Stats/common.tpl')
    );

    const LAYOUT_CR_STATS = array(
        'inventory' =>  array('Stats/inventoryCR.tpl'),
        'customers' => array('Stats/common.tpl'),
        'vendors' => array('Stats/common.tpl'),
        'projects' => array('Stats/common.tpl')
    );


    const LAYOUT_STATS_BY_COUNTRY = array(
        'PA' => self::LAYOUT_PA_STATS,
        'CO' => self::LAYOUT_STATS,
        'MX' => self::LAYOUT_MX_STATS,
        'SV' => self::LAYOUT_SV_STATS,
        'HN' => self::LAYOUT_HN_STATS,
        'NI' => self::LAYOUT_NI_STATS,
        'GT' => self::LAYOUT_GT_STATS,
        'CR' => self::LAYOUT_CR_STATS,
        'CL' => self::LAYOUT_STATS,
        'AR' => self::LAYOUT_STATS,
        'PE' => self::LAYOUT_STATS,
        'UY' => self::LAYOUT_STATS,
        'PY' => self::LAYOUT_STATS
    );

    public function __construct()
    {
        parent::__construct();

        $this->benchmark->mark('load_start');

        $this->load->database();

        $this->benchmark->mark('load_finish');

        //echo ' - dbLoad : ';
        //echo $this->benchmark->elapsed_time('load_start', 'load_finish');
        //$this->output->enable_profiler(TRUE);

    }

    public function dashboard(){
        $this->benchmark->mark('code_start');

        $data = array();
        $data = $this->loadCommonData($data,'Laundry Dashboard', '', '', '' , '');

        //$this->benchmark->mark('common');
        //echo ' - common : ';
        //echo $this->benchmark->elapsed_time('code_start', 'common');

        //$data = $this->loadDashboardStats($data);
        //$this->benchmark->mark('load');

        //echo ' - dashboard : ';
        //echo $this->benchmark->elapsed_time('code_start', 'load');

        $data = $this->loadDashboardStatsByWave($data);

        $this->displayStandardLayout(self::LAYOUT_DASHBOARD, $data);

        $this->benchmark->mark('display_4');

        //echo ' - display : ';
        //echo $this->benchmark->elapsed_time('code_start', 'display_4');

    }

    public function index($country='CO')
	{

        $this->session->selCountry = $country;

        $data['country'] = $country;
        $data = $this->loadCommonData($data,$country.' Laundry', '', '', '' , '');

        $projects = array();
        $data['projects'] = $this->loadProjectStats($projects, $country);

        $services = array();
        $data['services'] = $this->loadServiceStats($services, $country);

        $customers = array();
        $data['customers'] = $this->loadStats($customers, self::VO_PRE_CUSTOMER_HEADER, self::VO_CUSTOMER_HEADER, $country);

        $vendors = array();
        $data['vendors'] = $this->loadStats($vendors, self::VO_PRE_VENDOR_HEADER, self::VO_VENDOR_HEADER, $country);

        $inventory = array();
        $data['inventory'] = $this->loadStats($inventory, self::VO_PRE_ITEM_HEADER, self::VO_ITEM_HEADER, $country);

        $qb = $this->em->createQueryBuilder();

        $qb->select('log')
            ->from('Commons\\AuditLog', 'log')
            ->andWhere(
                $qb->expr()->eq('log.country', '?1'),
                $qb->expr()->orX(
                    $qb->expr()->like('log.type', '?2')
                ))
            ->setParameter(1, $country)
            ->setParameter(2, '%INV_SIGLA_AMT%')
            ->orderBy('log.stamp', 'DESC')
            ->setMaxResults(1);

        $amt = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

        if (sizeof($amt)>0){
            $data['inventory']['siglaAmount'] = $amt [0];

        }

        $qb = $this->em->createQueryBuilder();

        $qb->select('log')
            ->from('Commons\\AuditLog', 'log')
            ->andWhere(
                $qb->expr()->eq('log.country', '?1'),
                $qb->expr()->orX(
                    $qb->expr()->like('log.type', '?2')
                ))
            ->setParameter(1, $country)
            ->setParameter(2, '%INV_ORACLE_AMT%')
            ->orderBy('log.stamp', 'DESC')
            ->setMaxResults(1);

        $amt = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

        if (sizeof($amt)>0){
            $data['inventory']['oracleAmount'] = $amt[0];
        }

        $qb = $this->em->createQueryBuilder();

        $qb->select('c')
            ->from('Commons\\MAPS\\GLConfigEstandar', 'c')
            ->andWhere(
                $qb->expr()->eq('c.country', '?1')
            )
            ->setParameter(1, $country)
            ->setMaxResults(1);

        $config = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

        if (sizeof($config)>0){
            $data['inventory']['localCurrency'] = $config[0]['localcurrency'];
        }

        $qb = $this->em->createQueryBuilder();

        $qb->select('c')
            ->from('Commons\\MAPS\\GLMonedas', 'c')
            ->andWhere(
                $qb->expr()->eq('c.country', '?1'),
                $qb->expr()->orX($qb->expr()->like('c.sigla', '?2'), $qb->expr()->like('c.sigla', '?3'))
            )
            ->setParameter(1, $country)
            ->setParameter(2, 'USD')
            ->setParameter(3, 'US$')
            ->setMaxResults(1);

        $dollarExchangeRate = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

        if (sizeof($dollarExchangeRate)>0){
            $data['inventory']['dollarExchangeRate'] = $dollarExchangeRate[0]['tasa'];
        }


        if ((isset($data['inventory']['dollarExchangeRate']))&&($data['inventory']['dollarExchangeRate']!=0)){
            $data['inventory']['siglaAmount']['displayAmount'] = number_format((float)$data['inventory']['siglaAmount']['event'],2,'.',',');
            $data['inventory']['siglaAmount']['dollarAmount'] = number_format(((float)$data['inventory']['siglaAmount']['event'])/((float)$data['inventory']['dollarExchangeRate']),2,'.',',');

            $data['inventory']['oracleAmount']['displayAmount'] = number_format((float)$data['inventory']['oracleAmount']['event'],2,'.',',');
            $data['inventory']['oracleAmount']['dollarAmount'] = number_format(((float)$data['inventory']['oracleAmount']['event'])/((float)$data['inventory']['dollarExchangeRate']),2,'.',',');

            $data['inventory']['siglaToOracleDifference']['displayAmount'] = number_format(((float)$data['inventory']['siglaAmount']['event']-(float)$data['inventory']['oracleAmount']['event']),2,'.',',');
            $data['inventory']['siglaToOracleDifference']['dollarAmount'] = number_format(((float)$data['inventory']['siglaAmount']['event']-(float)$data['inventory']['oracleAmount']['event'])/((float)$data['inventory']['dollarExchangeRate']),2,'.',',');
            $data['inventory']['siglaToOracleDifference']['percentage'] = number_format(((float)$data['inventory']['siglaAmount']['event']-(float)$data['inventory']['oracleAmount']['event'])/((float)$data['inventory']['siglaAmount']['event'])*100,2,'.',',');

        }

        $data['snapshotDate'] = $this->loadSnapshotDate($country);
        $data['cutoffDate'] = $this->loadCutoffDate($country);

        $qb = $this->em->createQueryBuilder();

        $qb->select('log')
            ->from('Commons\\AuditLog', 'log')
            ->andWhere(
                $qb->expr()->eq('log.country', '?1'),
                $qb->expr()->orX(
                    $qb->expr()->like('log.event', '?2'),
                    $qb->expr()->like('log.event', '?3')
                ))
            ->setParameter(1, $country)
            ->setParameter(2, '%Master Data loaded%')
            ->setParameter(3, '%Master Data added recommendations%')
            ->orderBy('log.stamp', 'DESC')
            ->setMaxResults(50);

        $data['logs'] = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

        $this->displayStandardLayout(self::LAYOUT_WELCOME, $data);

	}

	private function loadDashboardStatsByWave($data){
        $data['timestamp'] = date("M-d-Y");


        $data['dashboard'] = array();
        $data['dashboard']['data'] = array();

        $waves = [0,1,2,3];

        foreach($waves as $w){

            $qb = $this->em->createQueryBuilder();
            $qb->select('c.country')
                ->where($qb->expr()->eq('c.wave', '?1'))
                ->setParameter(1, $w)
                ->from('Commons\\MAPS\\GLConfigEstandar', 'c')
                ->orderBy('c.migOrder', 'ASC');

            $countries = $qb->getQuery()->getResult();

            $data['dashboard']['data']['wave'.$w] = array();
            $data['dashboard']['data']['wave'.$w]['countries']=array();

            foreach ($countries as $elArray){
                array_push($data['dashboard']['data']['wave'.$w]['countries'], $elArray['country']);
            }

            $data['dashboard']['data']['wave'.$w]['data']=array();

            $data['dashboard']['data']['wave'.$w]['data']['01.SNAPSHOT'] = array();
            foreach ($data['dashboard']['data']['wave'.$w]['countries'] as $country){
                $stmt = $this->em->getConnection()->prepare('EXEC dbo.udf_GetSnapshotDate \''.$country.'\'');
                $stmt->execute();

                $cellStats = array();
                $result=$stmt->fetchAll();

                $cellStats['date']=str_replace(' 00:00:00.000', '', $result[0]['FECHA']);

                $date = new DateTime($result[0]['FECHA']);
                $now = new DateTime();

                $diff = $date->diff($now);

                if ($diff->m==0){
                    $cellStats['dminus'] = $diff->format("D-%d");

                }else{
                    $cellStats['dminus'] = $diff->format("+%m month");

                }


                $data['dashboard']['data']['wave'.$w]['data']['01.SNAPSHOT'][$country]=$cellStats;
            }

            $data['dashboard']['data']['wave'.$w]['data']['02.CUTOFF'] = array();
            foreach ($data['dashboard']['data']['wave'.$w]['countries'] as $country){
                $qb = $this->em->createQueryBuilder();
                $qb->select('vo')
                    ->from('Commons\\MAPS\\GLConfigEstandar', 'vo')
                    ->where($qb->expr()->eq('vo.country', '?1'))
                    ->setParameter(1, $country);

                $vo = $qb->getQuery()->getSingleResult();

                $cellStats['date']=$vo->getCutoffDate()->format('Y-m-d');

                $date = $vo->getCutoffDate();
                $now = new DateTime();

                $diff = $date->diff($now);

                if ($diff->m==0){
                    if ($diff->d>=0){
                        $cellStats['dminus'] = $diff->format("D+%d");

                    }else{
                        $cellStats['dminus'] = $diff->format("D-%d");

                    }

                }else{
                    $cellStats['dminus'] = $diff->format("+%m month");

                }

                $data['dashboard']['data']['wave'.$w]['data']['02.CUTOFF'][$country]=$cellStats;
            }

            $data['dashboard']['data']['wave'.$w]['data']['03.INVENTORY'] = array();
            foreach ($data['dashboard']['data']['wave'.$w]['countries'] as $country){
                $qb = $this->em->createQueryBuilder();
                $qb->select('log')
                    ->from('Commons\\AuditLog', 'log')
                    ->andWhere(
                        $qb->expr()->eq('log.country', '?1'),
                        $qb->expr()->orX(
                            $qb->expr()->like('log.type', '?2')
                        ))
                    ->setParameter(1, $country)
                    ->setParameter(2, '%INVENTORY%')
                    ->orderBy('log.stamp', 'DESC')
                    ->setMaxResults(1);

                $cellStats = array();
                try {
                    $log = $qb->getQuery()->getSingleResult();

                    $cellStats['target'] = $log->getTarget();
                    $cellStats['done'] = $log->getDone();

                    if ($cellStats['target']>0){
                        $cellStats['percent'] = number_format($cellStats['done']/$cellStats['target']*100, 2).'%';

                        if ($cellStats['percent']>99){
                            $cellStats['status'] = 'nirvana';

                        }elseif ($cellStats['percent']>70){
                            $cellStats['status'] = 'ok';

                        }elseif ($cellStats['percent']>20){
                            $cellStats['status'] = 'warning';
                        }else{
                            $cellStats['status'] = 'critical';
                        }
                    }else{
                        $cellStats['percent'] = '-';
                        $cellStats['status'] = 'critical';
                    }

                }catch(\Exception $e){
                    // no problem, maybe nobody has tried editing this PreItemHeader in the last 10 minutes
                    $cellStats['target'] = '-';
                    $cellStats['done'] = '-';
                    $cellStats['percent'] = '-';
                    $cellStats['status'] = 'critical';
                }

                $data['dashboard']['data']['wave'.$w]['data']['03.INVENTORY'][$country]=$cellStats;



            }

            $data['dashboard']['data']['wave'.$w]['data']['04.CUSTOMER'] = array();
            foreach ($data['dashboard']['data']['wave'.$w]['countries'] as $country){
                $qb = $this->em->createQueryBuilder();
                $qb->select('log')
                    ->from('Commons\\AuditLog', 'log')
                    ->andWhere(
                        $qb->expr()->eq('log.country', '?1'),
                        $qb->expr()->orX(
                            $qb->expr()->like('log.type', '?2')
                        ))
                    ->setParameter(1, $country)
                    ->setParameter(2, '%CUSTOMER%')
                    ->orderBy('log.stamp', 'DESC')
                    ->setMaxResults(1);

                $cellStats = array();
                try {
                    $log = $qb->getQuery()->getSingleResult();

                    $cellStats['target'] = $log->getTarget();
                    $cellStats['done'] = $log->getDone();

                    if ($cellStats['target']>0){
                        $cellStats['percent'] = number_format($cellStats['done']/$cellStats['target']*100, 2).'%';

                        if ($cellStats['percent']>99){
                            $cellStats['status'] = 'nirvana';

                        }elseif ($cellStats['percent']>70){
                            $cellStats['status'] = 'ok';

                        }elseif ($cellStats['percent']>20){
                            $cellStats['status'] = 'warning';
                        }else{
                            $cellStats['status'] = 'critical';
                        }
                    }else{
                        $cellStats['percent'] = '-';
                        $cellStats['status'] = 'critical';
                    }

                }catch(\Exception $e){
                    // no problem, maybe nobody has tried editing this PreItemHeader in the last 10 minutes
                    $cellStats['target'] = '-';
                    $cellStats['done'] = '-';
                    $cellStats['percent'] = '-';
                    $cellStats['status'] = 'critical';
                }

                $data['dashboard']['data']['wave'.$w]['data']['04.CUSTOMER'][$country]=$cellStats;



            }

            $data['dashboard']['data']['wave'.$w]['data']['05.VENDOR'] = array();
            foreach ($data['dashboard']['data']['wave'.$w]['countries'] as $country){
                $qb = $this->em->createQueryBuilder();
                $qb->select('log')
                    ->from('Commons\\AuditLog', 'log')
                    ->andWhere(
                        $qb->expr()->eq('log.country', '?1'),
                        $qb->expr()->orX(
                            $qb->expr()->like('log.type', '?2')
                        ))
                    ->setParameter(1, $country)
                    ->setParameter(2, '%VENDOR%')
                    ->orderBy('log.stamp', 'DESC')
                    ->setMaxResults(1);

                $cellStats = array();
                try {
                    $log = $qb->getQuery()->getSingleResult();

                    $cellStats['target'] = $log->getTarget();
                    $cellStats['done'] = $log->getDone();

                    if ($cellStats['target']>0){
                        $cellStats['percent'] = number_format($cellStats['done']/$cellStats['target']*100, 2).'%';

                        if ($cellStats['percent']>99){
                            $cellStats['status'] = 'nirvana';

                        }elseif ($cellStats['percent']>70){
                            $cellStats['status'] = 'ok';

                        }elseif ($cellStats['percent']>20){
                            $cellStats['status'] = 'warning';
                        }else{
                            $cellStats['status'] = 'critical';
                        }
                    }else{
                        $cellStats['percent'] = '-';
                        $cellStats['status'] = 'critical';
                    }

                }catch(\Exception $e){
                    // no problem, maybe nobody has tried editing this PreItemHeader in the last 10 minutes
                    $cellStats['target'] = '-';
                    $cellStats['done'] = '-';
                    $cellStats['percent'] = '-';
                    $cellStats['status'] = 'critical';
                }

                $data['dashboard']['data']['wave'.$w]['data']['05.VENDOR'][$country]=$cellStats;



            }

        }

        return $data;

    }

 	private function mockDashboardStats($data){
        $data['timestamp'] = date("M-d-Y");
        $data['dashboard'] = [
            "data" => [
                "wave0" => [
                    "countries" => [ "PA"],
                    "data" => [
                        "03.INVENTORY" => [
                            "PA" => ["target"=>"1000","done"=>"1000","percent"=>"100%","status"=>"nirvana"]
                        ],
                        "PROJECTS" => [
                            "PA" => ["target"=>"1000","done"=>"1000","percent"=>"100%","status"=>"nirvana"]
                        ],
                        "04.CUSTOMER" => [
                            "PA" => ["target"=>"1000","done"=>"1000","percent"=>"100%","status"=>"nirvana"]
                        ],
                        "05.VENDOR" => [
                            "PA" => ["target"=>"1000","done"=>"1000","percent"=>"100%","status"=>"nirvana"]
                        ]
                    ],
                ],
                "wave1" => [
                    "countries" => [ "CO"],
                    "data" => [

                        "03.INVENTORY" => [
                            "CO" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"]
                        ],
                        "PROJECTS" => [
                            "CO" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"]
                        ],
                        "04.CUSTOMER" => [
                            "CO" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"]
                        ],
                        "05.VENDOR" => [
                            "CO" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"]
                        ]
                    ],
                ],
                "wave3" => [
                    "countries" => [ "MX", "SV", "HN", "NI", "GT", "CR"],
                    "data" => [
                        "03.INVENTORY" => [
                            "MX" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "SV" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "HN" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "NI" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "GT" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "CR" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"]
                        ],
                        "PROJECTS" => [
                            "MX" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "SV" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "HN" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "NI" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "GT" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "CR" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"]
                        ],
                        "04.CUSTOMER" => [
                            "MX" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "SV" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "HN" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "NI" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "GT" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "CR" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"]
                        ],
                        "05.VENDOR" => [
                            "MX" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "SV" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "HN" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "NI" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "GT" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "CR" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"]
                        ]
                    ],
                ],
                "wave4" => [
                    "countries" => [ "PE", "CL", "AR", "UY", "PY"],
                    "data" => [
                        "03.INVENTORY" => [
                            "PE" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "CL" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "AR" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "UY" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "PY" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"]
                        ],
                        "PROJECTS" => [
                            "PE" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "CL" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "AR" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "UY" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "PY" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"]
                        ],
                        "04.CUSTOMER" => [
                            "PE" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "CL" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "AR" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "UY" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "PY" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"]
                        ],
                        "05.VENDOR" => [
                            "PE" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "CL" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "AR" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "UY" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"],
                            "PY" => ["target"=>"1000","done"=>"900","percent"=>"90%","status"=>"ok"]
                        ]

                    ],
                ]
            ]
        ];

        return $data;
    }

    public function showStats($group='Inventory', $country='PA')
    {
        $data['country'] = $country;

        switch($group){
            case 'Inventory':
                $data = $this->loadCommonData($data,' Inventory Stats ', '', '', '' , '');
                $data = $this->loadStats($data, self::VO_PRE_ITEM_HEADER, $country);
                $this->displayStandardLayout(self::LAYOUT_STATS_BY_COUNTRY[$country]['inventory'], $data);
                return;
                break;

        }

        redirect(redirect(base_url() . 'KPI/' . $country));
    }
}
