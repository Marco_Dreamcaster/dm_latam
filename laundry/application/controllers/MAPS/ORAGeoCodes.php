<?php
include_once APPPATH . 'controllers/MY_Controller.php';
Use Doctrine\ORM\Query;


class ORAGeoCodes extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->getEntityManager();
        $this->auditor->setEntityManager($this->em);
        $this->load->helper('url_helper');
        $this->load->library('session');

    }

    public function index($page=0)
    {
        $data['title'] = 'Oracle Geography Codes';
        $data['addressCountry'] = 'PA';
        $data['province'] = null;
        $data['state'] = null;
        $data['city'] = null;

        $data = $this->loadSessionNavigation($data);
        $data = $this->loadOraGeoCodes($data);

        $extPAGE_SIZE = Auditor::PAGE_SIZE*3;

        $qb = $this->em->createQueryBuilder();
        $qb->select('geo')
            ->from('Commons\\MAPS\ORAGeoCodes', 'geo')
            ->setFirstResult($page*$extPAGE_SIZE)
            ->setMaxResults($extPAGE_SIZE)
            ->addOrderBy("geo.countryCode",'ASC');

        $data['oraGeoCodes'] = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

        $dql = 'SELECT COUNT(geo) FROM Commons\\MAPS\ORAGeoCodes geo';
        $data['total'] = $this->em->createQuery($dql)->getSingleScalarResult();

        if ($data['total']>$extPAGE_SIZE){
            $data['isPaginationNeeded'] = true;
            $data['currentPage'] = $page;
            $data['lastPage']=intval($data['total']/$extPAGE_SIZE);
            $data['listAction'] = '/Maps/ORAGeoCodes/list/';

        }else{
            $data['isPaginationNeeded'] = false;
        }

        $data['countries'] = array_keys(self::COUNTRY3_FOLDERS);

        $this->load->view('templates/header', $data);
        $this->smarty->view('MAPS/ORAGeoCodes/ORAGeoCodes.tpl', $data, true );
        $this->load->view('templates/footer', $data);
    }

    public function testJSON(){
        $data['title'] = 'Oracle Geography Codes JSONified Linked Combos Test';
        $data['addressCountry'] = 'PA';
        $data['province'] = 'PANAM� OESTE';
        $data['state'] = 'PANAM� OESTE';
        $data['county'] = 'PA-10';
        $data['city'] = 'SAN CARLOS';

        $data = $this->loadSessionNavigation($data);
        $data = $this->loadOraGeoCodes($data);

        $this->load->view('templates/header', $data);
        $this->smarty->view('MAPS/ORAGeoCodes/ORAGeoCodesTest.tpl', $data, true );
        $this->load->view('templates/footer', $data);

    }

    public function create()
    {
        $data['title'] = 'Create new Oracle Geo Code';
        $data = $this->loadSessionNavigation($data);

        $data['action'] = 'Maps/ORAGeoCodes/new';
        $data['actionLabel'] = 'Create';

        $this->load->view('templates/header', $data);
        $this->smarty->view('MAPS/ORAGeoCodes/custom.tpl', $data, true );
        $this->load->view('templates/footer', $data);
    }

    public function addNew()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('countryCode', 'countryCode', 'required|alpha|max_length[2]');
        $this->form_validation->set_rules('countryName', 'countryName', 'required|max_length[30]');
        $this->form_validation->set_rules('provinceCode', 'provinceCode', 'trim|required|max_length[10]');
        $this->form_validation->set_rules('provinceName', 'provinceName', 'trim|required|max_length[60]');
        $this->form_validation->set_rules('stateCode', 'stateCode', 'trim|required|max_length[10]');
        $this->form_validation->set_rules('stateName', 'stateName', 'trim|required|max_length[60]');
        $this->form_validation->set_rules('countyCode', 'countyCode', 'trim|required|max_length[10]');
        $this->form_validation->set_rules('countyName', 'countyName', 'trim|required|max_length[60]');
        $this->form_validation->set_rules('cityName', 'cityName', 'trim|required|max_length[60]');

        if ($this->form_validation->run() === FALSE){
            $data = $this->input->post(array('countryCode', 'countryName', 'provinceCode', 'provinceName', 'cityCode', 'cityName'));

            $data['errorCountryCode'] = $this->form_validation->error('countryCode');
            $data['errorCountryName'] = $this->form_validation->error('countryName');
            $data['errorProvinceCode'] = $this->form_validation->error('provinceCode');
            $data['errorProvinceName'] = $this->form_validation->error('provinceName');
            $data['errorStateCode'] = $this->form_validation->error('stateCode');
            $data['errorStateName'] = $this->form_validation->error('stateName');
            $data['errorCountyCode'] = $this->form_validation->error('countyCode');
            $data['errorCountyName'] = $this->form_validation->error('countyName');
            $data['errorCityName'] = $this->form_validation->error('cityName');

            $data['title'] = 'Try again to create new Oracle Geo Code';
            $data['action'] = 'Maps/ORAGeoCodes/new';
            $data['actionLabel'] = 'Create';

            $data['sessionCountry'] = strtolower($this->session->sessionCountry);
            $data['sessionGroup'] = ucfirst($this->session->sessionGroup);

            $this->load->view('templates/header', $data);
            $this->smarty->view('MAPS/ORAGeoCodes/custom.tpl', $data, true );
            $this->load->view('templates/footer', $data);
        }
        else{
            $geoCode = new \Commons\MAPS\OraGeoCodes();
            $geoCode->setCountryCode(strtoupper($this->input->post('countryCode')));
            $geoCode->setCountryName( mb_strtoupper(mb_convert_encoding($this->input->post('countryName'), 'ISO-8859-1', 'UTF-8'), 'ISO-8859-1'));
            $geoCode->setProvinceCode(strtoupper($this->input->post('provinceCode')));
            $geoCode->setProvince(mb_strtoupper(mb_convert_encoding($this->input->post('provinceName'), 'ISO-8859-1', 'UTF-8'), 'ISO-8859-1'));
            $geoCode->setStateCode(strtoupper($this->input->post('stateCode')));
            $geoCode->setState(mb_strtoupper(mb_convert_encoding($this->input->post('stateName'), 'ISO-8859-1', 'UTF-8'), 'ISO-8859-1'));
            $geoCode->setCountyCode(strtoupper($this->input->post('countyCode')));
            $geoCode->setCounty(mb_strtoupper(mb_convert_encoding($this->input->post('countyName'), 'ISO-8859-1', 'UTF-8'), 'ISO-8859-1'));
            $geoCode->setCityCode(substr(mb_strtoupper(str_replace(' ', '' , mb_convert_encoding($this->input->post('cityName'), 'ISO-8859-1', 'UTF-8')), 'ISO-8859-1'),0,10));
            $geoCode->setCity(mb_strtoupper(mb_convert_encoding($this->input->post('cityName'), 'ISO-8859-1', 'UTF-8'), 'ISO-8859-1'));
            date_default_timezone_set('America/Santiago');
            $geoCode->setLastUpdated(new \DateTime("now"));
            $geoCode->setLastSource('user interface');   // get the freaking username when authorization and authentication are available

            $this->em->persist($geoCode);
            $this->em->flush();

            redirect(base_url()."Maps/ORAGeoCodes");
        }

    }

    public function getProvinces(){
        $urlParams =  $this->input->get();
        $country = $urlParams[array_keys($urlParams)[0]];
        $state = utf8_decode($urlParams[array_keys($urlParams)[1]]);

        $query = $this->em->createQuery("SELECT DISTINCT(geo.province) AS PR FROM Commons\\MAPS\ORAGeoCodes geo WHERE geo.countryCode = ?1 AND geo.state = ?2 ORDER BY PR ASC");
        $query->setParameter(1, $country);
        $query->setParameter(2, $state);
        $results = $query->getResult();

        $data = array();

        foreach($results as $result){
            $data[utf8_encode($result['PR'])]=utf8_encode($result['PR']);
        }

        //add the header here
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        echo json_encode( $data );

    }

    public function getCounties(){
        $urlParams =  $this->input->get();
        $country = $urlParams[array_keys($urlParams)[0]];
        $province = utf8_decode($urlParams[array_keys($urlParams)[1]]);

        $query = $this->em->createQuery("SELECT DISTINCT(geo.county) AS CO FROM Commons\\MAPS\ORAGeoCodes geo WHERE geo.countryCode = ?1 AND geo.province = ?2 ORDER BY CO ASC");
        $query->setParameter(1, $country);
        $query->setParameter(2, $province);
        $results = $query->getResult();

        $data = array();

        foreach($results as $result){
            $data[utf8_encode($result['CO'])]=utf8_encode($result['CO']);
        }

        //add the header here
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        echo json_encode( $data );

    }


    public function getStates(){

        $urlParams =  $this->input->get();
        $country = $urlParams[array_keys($urlParams)[0]];

        $query = $this->em->createQuery("SELECT DISTINCT(geo.state) AS ST FROM Commons\\MAPS\ORAGeoCodes geo WHERE geo.countryCode = ?1 ORDER BY ST ASC");
        $query->setParameter(1, $country);
        $results = $query->getResult();

        $data = array();

        foreach($results as $result){
            $data[utf8_encode($result['ST'])]=utf8_encode($result['ST']);
        }

        //add the header here
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        echo json_encode( $data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES );

    }


    public function getCities(){
        $urlParams =  $this->input->get();
        $country = $urlParams[array_keys($urlParams)[0]];
        $county = utf8_decode($urlParams[array_keys($urlParams)[1]]);

        $query = $this->em->createQuery("SELECT DISTINCT(geo.city) AS CI FROM Commons\\MAPS\ORAGeoCodes geo WHERE geo.countryCode = ?1 AND geo.county = ?2 ORDER BY CI ASC");
        $query->setParameter(1, $country);
        $query->setParameter(2, $county);
        $results = $query->getResult();

        $data = array();

        foreach($results as $result){
            $data[utf8_encode($result['CI'])]=utf8_encode($result['CI']);
        }

        //add the header here
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        echo json_encode( $data );

    }


    public function remove($id)
    {
        $geo = $this->em->find('Commons\\MAPS\ORAGeoCodes', $id);
        $this->em->remove($geo);
        $this->em->flush();

        redirect(base_url()."Maps/ORAGeoCodes");
    }



}