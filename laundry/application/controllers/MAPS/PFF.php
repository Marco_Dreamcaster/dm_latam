<?php
include_once APPPATH . 'controllers/MY_Controller.php';

Use Doctrine\ORM\Query;
Use Commons\AuditLog;
Use Commons\MAPS\ProjectFlexFields;


class PFF extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->getEntityManager();
        $this->auditor->setEntityManager($this->em);
        $this->load->helper('url_helper');
        $this->load->library('session');

    }

    public function index()
    {
        $query = $this->em->createQuery("SELECT pff FROM Commons\\MAPS\ProjectFlexFields pff");

        $data['title'] = 'Project Flex Fields';
        $data = $this->loadSessionNavigation($data);

        $data['projectFlexFields'] = $query->getResult(Query::HYDRATE_ARRAY);

        $this->load->view('templates/header', $data);
        $this->smarty->view('MAPS/PFF/pff.tpl', $data, true );
        $this->load->view('templates/footer', $data);
    }

    public function removePFF($type = null, $configuration= null, $line = null)
    {
        $type = urldecode($type);
        $line = urldecode($line);
        $configuration = urldecode($configuration);

        $data = $this->findProjectFFById($type, $line, $configuration);

        $this->em->remove($data);
        $this->em->flush();

        redirect(base_url()."Projects/Maps/PFF");
    }

    public function createPFF()
    {
        $data['title'] = 'Create new Project Flex Field combination';
        $data = $this->loadSessionNavigation($data);

        $data['action'] = 'Projects/Maps/newPFF';
        $data['actionLabel'] = 'Create';

        $this->load->view('templates/header', $data);
        $this->smarty->view('MAPS/PFF/custom.tpl', $data, true );
        $this->load->view('templates/footer', $data);
    }

    public function newPFF()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('type', 'Type', 'trim|required');
        $this->form_validation->set_rules('line', 'Line', 'trim|required');
        $this->form_validation->set_rules('configuration', 'Configuration', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $data = $this->input->post(array('type', 'line', 'configuration'));

            $data['errorType'] = $this->form_validation->error('type',' ', ' ');
            $data['errorLine'] = $this->form_validation->error('line');
            $data['errorConfiguration'] = $this->form_validation->error('configuration');

            $data['sessionCountry'] = strtolower($this->session->sessionCountry);
            $data['sessionGroup'] = ucfirst($this->session->sessionGroup);

            $data['title'] = 'Try again to create an IDL Group';
            $data['action'] = 'PA/MAPS/newPFF';
            $data['actionLabel'] = 'Create';

            $this->load->view('templates/header', $data);
            $this->smarty->view('PA/MAPS/PFF/topBar.tpl', $data, true );
            $this->smarty->view('PA/MAPS/PFF/custom.tpl', $data, true );
            $this->load->view('templates/footer', $data);

        }
        else
        {
            $existingPFF = $this->findProjectFFById($this->input->post('type'), $this->input->post('line'), $this->input->post('configuration'));

            if (sizeof($existingPFF)==0){
                $pff = new ProjectFlexFields();
                $pff->setLine($this->input->post('line'));
                $pff->setType($this->input->post('type'));
                $pff->setConfiguration($this->input->post('configuration'));

                $this->em->persist($pff);
                $this->em->flush();

            }

            redirect(base_url()."Projects/Maps/PFF");
        }

    }


    private function findProjectFFById($type, $line, $configuration){
        if (($type==null)||($line==null)||($configuration==null)){
            show_404();
            return;
        }

        $queryBuilder = $this->em->createQueryBuilder();

        $queryBuilder->select('pff')
            ->from('Commons\\MAPS\\ProjectFlexFields', 'pff')
            ->andWhere(
                $queryBuilder->expr()->eq('pff.type', '?1'),
                $queryBuilder->expr()->eq('pff.line', '?2'),
                $queryBuilder->expr()->eq('pff.configuration', '?3'))
            ->setParameter(1, $type)
            ->setParameter(2, $line)
            ->setParameter(3, $configuration);

        $data = $queryBuilder->getQuery()->getResult();

        return $data[0];

    }


}