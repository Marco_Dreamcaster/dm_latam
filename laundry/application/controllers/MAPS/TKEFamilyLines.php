<?php
include_once APPPATH . 'controllers/MY_Controller.php';
Use Doctrine\ORM\Query;


class TKEFamilyLines extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->getEntityManager();
        $this->auditor->setEntityManager($this->em);
        $this->load->helper('url_helper');
        $this->load->library('session');

    }

    public function testJSON()
    {
        $data['title'] = 'SCC Family Line Codes JSONified Combos Test';
        $data['family'] = '66000000';
        $data['subFamily'] = '66090000';
        $data['line'] = '66099800';

        $data = $this->loadSessionNavigation($data);

        $query = $this->em->createQuery("SELECT DISTINCT scc.family, scc.familyESA FROM Commons\\MAPS\TKEFamilyLines scc ORDER BY scc.family ASC");
        $data['sccFamilies'] = $query->getResult(Query::HYDRATE_ARRAY);

        $this->load->view('templates/header', $data);
        $this->smarty->view('MAPS/TKEFamilyLines/TKEFamilyLinesTest.tpl', $data, true );
        $this->load->view('templates/footer', $data);
    }

    public function getSubFamilies(){
        $urlParams =  $this->input->get();
        $family = utf8_decode($urlParams[array_keys($urlParams)[0]]);

        $query = $this->em->createQuery("SELECT DISTINCT scc.subFamily, scc.subFamilyESA FROM Commons\\MAPS\TKEFamilyLines scc WHERE scc.family = ?1 ORDER BY scc.subFamily ASC");
        $query->setParameter(1, $family);
        $results = $query->getResult();

        $data = array();

        foreach($results as $result){
            $data[utf8_encode($result['subFamily'])]=utf8_encode($result['subFamily'].' - '.$result['subFamilyESA']);
        }

        //add the header here
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        echo json_encode( $data );

    }

    public function getLines(){
        $urlParams =  $this->input->get();
        $subFamily = utf8_decode($urlParams[array_keys($urlParams)[0]]);

        $query = $this->em->createQuery("SELECT DISTINCT scc.line, scc.lineESA FROM Commons\\MAPS\TKEFamilyLines scc WHERE scc.subFamily = ?1 ORDER BY scc.line ASC");
        $query->setParameter(1, $subFamily);
        $results = $query->getResult();

        $data = array();

        foreach($results as $result){
            $data[utf8_encode($result['line'])]=utf8_encode($result['line'].' - '.$result['lineESA']);
        }

        //add the header here
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        echo json_encode( $data );

    }

}