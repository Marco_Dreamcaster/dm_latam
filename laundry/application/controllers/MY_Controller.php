<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use Doctrine\ORM\Query;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class MY_Controller extends CI_Controller
{
    protected $em;

    const VO_ITEM_HEADER = 'Commons\\ORA\\ItemHeader';
    const VO_PRE_ITEM_HEADER = 'Commons\\RAW\\PreItemHeader';

    const VO_CUSTOMER_HEADER = 'Commons\\ORA\\CustomerHeader';
    const VO_PRE_CUSTOMER_HEADER = 'Commons\\RAW\\PreCustomerHeader';

    const VO_VENDOR_HEADER = 'Commons\\ORA\\VendorHeader';
    const VO_PRE_VENDOR_HEADER = 'Commons\\RAW\\PreVendorHeader';

    const VO_PROJECT_HEADER = 'Commons\\ORA\\ProjectHeader';
    const VO_PRE_PROJECT_HEADER = 'Commons\\RAW\\PreProjectHeader';

    const COUNTRY3_FOLDERS = [
        null=>'00.COM',
        'PA'=>'01.PAN',
        'CO'=>'02.COL',
        'MX'=>'03.MEX',
        'SV'=>'04.SLV',
        'NI'=>'05.NIC',
        'HN'=>'06.HND',
        'GT'=>'07.GTM',
        'CR'=>'08.CRI',
        'CL'=>'09.CHL',
        'AR'=>'10.ARG',
        'PE'=>'11.PER',
        'PY'=>'12.PRY',
        'UY'=>'13.URY'
    ];


    protected $defaultCutoffDatesByCountry = [
        null=>'31-12-2018',
        'PA'=>'31-10-2018',
        'CO'=>'31-12-2018',
        'MX'=>'31-12-2018',
        'SV'=>'31-12-2018',
        'NI'=>'31-12-2018',
        'GT'=>'31-12-2018',
        'HN'=>'31-12-2018',
        'AR'=>'31-12-2018',
        'CL'=>'31-12-2018',
        'PY'=>'31-12-2018',
        'UY'=>'31-12-2018',
        'PE'=>'31-12-2018',
        'CR'=>'31-12-2018'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->em = $this->doctrine->getEntityManager();
        $this->basedatos = $this->doctrine->getBaseDatos();
        $this->objectTags = $this->doctrine->getObjectTags();
        $this->auditor->setEntityManager($this->em);
        $this->load->helper('url_helper');
        $this->load->library('session');
        $this->session->basedatos = $this->basedatos;
        $this->load->helper('download');
    }

    public function createTestExcelSpreadsheet(){

        $spreadsheet = $this->phpSpreadsheet->createNewSpreadsheet();
        assert($spreadsheet!=null);

        date_default_timezone_set('America/Sao_Paulo');
        $date = date('Ymd_His', time());

        $sheet = $spreadsheet->setActiveSheetIndex(0);
        $cell = $sheet->getCell('A1');
        $cell->setValue('POC');
        $cell = $sheet->getCell('A2');
        $cell->setValue('Proof of Concept');
        $cell = $sheet->getCell('A3');
        $cell->setValue('Prova de Conceito');
        $sheet->setSelectedCells('A1');
        $cell = $sheet->getCell('A4');
        $cell->setValue("generada en ".$date);
        $spreadsheet->setActiveSheetIndex(0);


        $fileTemplate = 'poc'."_%timestamp%_".".xlsx";
        $fileTemplate = str_replace("%timestamp%", $date, $fileTemplate);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileTemplate.'"');
        header('Cache-Control: max-age=0');

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

    }


    public function createTestExcelReport(){

        $spreadsheet = $this->phpSpreadsheet->createNewSpreadsheet();

        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
        $spreadsheet->getDefaultStyle()->getFont()->setSize(8);


        assert($spreadsheet!=null);

        $dql = "SELECT DISTINCT
                  i.origItemNumber,
                  i.itemNumber,
                  i.description AS partDescription,
                  im.manufacturer,
                  im.description AS manufacturerDescription,
                  im.partNumber
                FROM Commons\ORA\ItemHeader i
                JOIN Commons\ORA\ItemMfgPartNum im
                WHERE i.origItemNumber=im.origItemNumber
                AND i.hasPicture=1";

        $voArray = $this->em->createQuery($dql)->getResult(Query::HYDRATE_ARRAY);

        date_default_timezone_set('America/Sao_Paulo');
        $date = date('Ymd_His', time());

        $spreadsheet->createSheet();
        $sheet = $spreadsheet->setActiveSheetIndex(1);
        $sheet->setTitle('meta report');
        $cell = $sheet->getCell('A1');
        $cell->setValue('Proof of Concept');
        $cell = $sheet->getCell('A2');
        $cell->setValue('Prova de Conceito');
        $sheet->setSelectedCells('A1');
        $cell = $sheet->getCell('A3');
        $cell->setValue("generada en ".$date);

        $sheet = $spreadsheet->setActiveSheetIndex(0);
        $sheet->setTitle('report');

        $cell = $sheet->getCell('A1');
        $cell->setValue('origItemNumber');

        $cell = $sheet->getCell('B1');
        $cell->setValue('itemNumber');

        $cell = $sheet->getCell('C1');
        $cell->setValue('partDescription');

        $cell = $sheet->getCell('D1');
        $cell->setValue('manufacturer');

        $cell = $sheet->getCell('E1');
        $cell->setValue('manufacturerDescription');

        $cell = $sheet->getCell('F1');
        $cell->setValue('partNumber');

        for($i=2; $i<sizeof($voArray); $i++){
            $cell = $sheet->getCell('A'.$i);
            $cell->setValue($voArray[$i-2]['origItemNumber']);

            $cell = $sheet->getCell('B'.$i);
            $cell->setValue($voArray[$i-2]['itemNumber']);

            $cell = $sheet->getCell('C'.$i);
            $cell->setValue($voArray[$i-2]['partDescription']);

            $cell = $sheet->getCell('D'.$i);
            $cell->setValue($voArray[$i-2]['manufacturer']);

            $cell = $sheet->getCell('E'.$i);
            $cell->setValue($voArray[$i-2]['manufacturerDescription']);

            $cell = $sheet->getCell('F'.$i);
            $cell->setValue($voArray[$i-2]['partNumber']);
        }

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);


        $fileTemplate = 'pocReport'."_%timestamp%_".".xlsx";
        $fileTemplate = str_replace("%timestamp%", $date, $fileTemplate);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileTemplate.'"');
        header('Cache-Control: max-age=0');

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

    }


    public function editCleansed($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence(null, $data);
    }

    public function checkPreConditions($id){
        return $id == null;
    }

    public function checkPreConditionsForItem($id){
        return true;
    }

    public function checkPreConditionsForPreItem($data){
        $result = array();

        $result['block'] = true;

        try{
            date_default_timezone_set('America/Sao_Paulo');

            $threshold = new \DateTime("5 minutes ago");

            $qb = $this->em->createQueryBuilder();
            $qb->select('log')
                ->from('Commons\AuditLog', 'log')
                ->andWhere(
                    $qb->expr()->like('log.event', '?1'),
                    $qb->expr()->gt('log.stamp', '?2'))
                ->setParameter(1, '%started edit of PreItemHeader #'.$data['id'].'%')
                ->setParameter(2, $threshold)
                ->setMaxResults( 1 )
                ->orderBy('log.stamp', 'DESC');

            try {
                $result['previousEdit'] = $qb->getQuery()->getSingleResult(Query::HYDRATE_ARRAY);
                $result['block'] = $result['previousEdit']['user']!=$_SERVER['REMOTE_ADDR'];

            }catch(\Exception $e){
                // no problem, maybe nobody has tried editing this PreItemHeader in the last 10 minutes
                $result['block'] = false;
            }

            if (!$result['block']){
                $log = new Commons\AuditLog($data['country'], $_SERVER['REMOTE_ADDR'], 'started edit of PreItemHeader #'.$data['id'], 0);

                $this->em->persist($log);
                $this->em->flush();
            }


        }catch(\Exception $e){
            error_log($e->getMessage());
        }

        return $result;
    }

    public function checkPreConditionsForCleansingCustomer($pre){
        try{
            $qb = $this->em->createQueryBuilder();
            $qb->select('vo')
                ->from('Commons\ORA\CustomerHeader', 'vo')
                ->where($qb->expr()->eq('vo.origSystemCustomerRef', '?1'))
                ->setParameter(1, $pre->getOrigSystemCustomerRef());

            try {
                $vo = $qb->getQuery()->getSingleResult();
                assert($vo!=null);

            }catch(\Exception $e){
                // good, there should be no CustomerHeader with OrigSystemCustomerRef at Stage table, Doctrine has thrown an exception
                return true;
            }

        }catch(\Exception $e){
            error_log($e->getMessage());
        }

        return false;
    }

    public function checkPreConditionsForCleansingVendor($pre){
        try{
            $qb = $this->em->createQueryBuilder();
            $qb->select('vo')
                ->from('Commons\ORA\VendorHeader', 'vo')
                ->where($qb->expr()->eq('vo.segment1', '?1'))
                ->setParameter(1, $pre->getSegment1());

            try {
                $vo = $qb->getQuery()->getSingleResult();
                assert($vo!=null);

            }catch(\Exception $e){
                // good, there should be no VendorHeader with Segment1 at Stage table, Doctrine has thrown an exception
                return true;
            }

        }catch(\Exception $e){
            error_log($e->getMessage());
        }

        return false;
    }


    public function loadCommonData($data, $title, $action, $actionLabel, $dirtyAction, $cancelAction){
        $data['title'] = $title;
        $data = $this->loadSessionNavigation($data);

        if (isset($data['action'])){
            $data['cleansingAction'] = $data['action'];
        }
        $data['action'] = $action;
        $data['dirtyAction'] = $dirtyAction;
        $data['actionLabel'] = $actionLabel;
        $data['cancelAction'] = $cancelAction;

        return $data;
    }

    protected function loadSessionNavigation($data){
        $this->resetSessionNavigation();

        $data['sessionCountry'] = strtolower($this->session->selCountry);
        $data['sessionGroup'] = ucfirst($this->session->selGroup);
        $data['sessionSelCol'] = $this->session->selCol;
        $data['sessionSelAsc'] = $this->session->selAsc;
        $data['basedatos'] = $this->session->basedatos;
        $data['user8id'] = $this->session->user8id;
        $data['clearance'] = $this->session->clearance;
        return $data;

    }

    protected function resetSessionNavigation(){
        if (!isset($this->session->selCountry)){
            $this->session->set_userdata(array('selCountry' => 'PE'));
        }

        if (!isset($this->session->selGroup)){
            $this->session->set_userdata(array('selGroup' => 'Projects'));
        }

        if (!isset($this->session->selCol)){
            $this->session->set_userdata(array('selCol' => 'name'));
        }

        if (!isset($this->session->selAsc)){
            $this->session->set_userdata(array('selAsc' => true));
        }

    }



    public function loadOraGeoCodes($data){
        $query = $this->em->createQuery("SELECT DISTINCT(geo.countryCode) as CC FROM Commons\\MAPS\ORAGeoCodes geo ORDER BY CC ASC");
        $data['countryCodes'] = $query->getResult(Query::HYDRATE_ARRAY);

        for($i=0;$i<sizeof($data['countryCodes']);$i++){
            $data['countryCodes'][$i]['CC']=str_replace(' ','_', $data['countryCodes'][$i]['CC']);
            if ($data['countryCodes'][$i]['CC']==$data['addressCountry']){
                $data['countryCodes'][$i]['selected']='selected';
            }else{
                $data['countryCodes'][$i]['selected']='';
            }

        }


        return $data;

    }

    public function loadOraProjectFF($data){
        $query = $this->em->createQuery("SELECT DISTINCT(pff.type) as TP FROM Commons\\MAPS\ProjectFlexFields pff ORDER BY TP ASC");
        $data['types'] = $query->getResult(Query::HYDRATE_ARRAY);

        for($i=0;$i<sizeof($data['types']);$i++){
            $data['types'][$i]['TP']=str_replace(' ','_', $data['types'][$i]['TP']);
            if ($data['types'][$i]['TP']==str_replace(' ','_',$data['attribute2'])){
                $data['types'][$i]['selected']='selected';
            }else{
                $data['types'][$i]['selected']='';
            }

        }

        $query = $this->em->createQuery("SELECT DISTINCT(pff.type) AS TP, pff.line AS LN FROM Commons\\MAPS\ProjectFlexFields pff ORDER BY LN ASC");
        $data['lines'] = $query->getResult();

        for($i=0;$i<sizeof($data['lines']);$i++){
            $data['lines'][$i]['TP']=str_replace(' ','_', $data['lines'][$i]['TP']);
            $data['lines'][$i]['LN']=str_replace(' ','_', $data['lines'][$i]['LN']);
            if (($data['lines'][$i]['LN']==str_replace(' ','_',$data['attribute7']))&&($data['lines'][$i]['TP']==str_replace(' ','_',$data['attribute2']))){
                $data['lines'][$i]['selected']='selected';
            }else{
                $data['lines'][$i]['selected']='';
            }

        }

        $query = $this->em->createQuery("SELECT DISTINCT(pff.configuration) AS CO, pff.line AS LN FROM Commons\\MAPS\ProjectFlexFields pff ORDER BY CO ASC");
        $data['configurations'] = $query->getResult();

        for($i=0;$i<sizeof($data['configurations']);$i++){
            $data['configurations'][$i]['LN']=str_replace(' ','_', $data['configurations'][$i]['LN']);
            if (($data['configurations'][$i]['LN']==str_replace(' ','_',$data['attribute7']))&&($data['configurations'][$i]['CO']==str_replace(' ','_',$data['attribute3']))){
                $data['configurations'][$i]['selected']='selected';
            }else{
                $data['configurations'][$i]['selected']='';
            }

        }

        return $data;

    }

    public function loadFormErrors($voClassName, $id, $data, $formValidation){
        $vo = $this->getVOByID($voClassName, $id);
        $fields = $this->getFieldsAssociatedToVO($voClassName, $vo);

        foreach($fields as $f) {
            if ($formValidation->error($f->getShortName())!=null){
                $data['error'.ucfirst($f->getShortName())] = str_replace("</p>","", str_replace("<p>", "",$formValidation->error($f->getShortName()) ));
            }
        }

        return $data;

    }

    public function loadFormErrorsForClassNameAndCountry($voClassName, $country, $data, $formValidation){
        $fields = $this->getFieldsAssociatedToClassNameAndCountry($voClassName, $country);

        foreach($fields as $f) {
            $formRules = $f->getFormRules();
            if (!isset($formRules)||($formRules!='')){
                $data['error'.ucfirst($f->getShortName())] = str_replace("</p>","", str_replace("<p>", "",$formValidation->error($f->getShortName()) ));
            }
        }

        return $data;

    }

    /**
     * @param $voClassName
     * @param $id
     * @return mixed
     *
     * can be implemented with Doctrine 2 regular repository call findBy()
     *
     */

    public function getVOByID($voClassName, $id){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from($voClassName, 'vo')
            ->where($qb->expr()->eq('vo.id', '?1'))
            ->setParameter(1, $id);

        $vo = $qb->getQuery()->getSingleResult();

        return $vo;
    }

    public function getVOHidratedByID($voClassName, $id){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from($voClassName, 'vo')
            ->where($qb->expr()->eq('vo.id', '?1'))
            ->setParameter(1, $id);

        $vo = $qb->getQuery()->getSingleResult(Query::HYDRATE_ARRAY);

        return $vo;
    }


    public function displayStandardLayout($contentLayoutArray, $data){
        $this->load->view('templates/header', $data);

        foreach($contentLayoutArray as $layout){
            $this->smarty->view($layout, $data, true);
        }
        $this->load->view('templates/footer', $data);


    }

    public function getCleansedHeader($classHeaderName, $country, $page, $colOrder, $asc = false){
        $qb = $this->em->createQueryBuilder();
        $qb->select('header')
            ->from($classHeaderName, 'header')
            ->andWhere(
                $qb->expr()->eq('header.country', '?1'))
            ->setParameter(1, $country)
            ->setFirstResult($page*Auditor::PAGE_SIZE)
            ->setMaxResults(Auditor::PAGE_SIZE)
            ->addOrderBy('header.dismissed','ASC')
            ->addOrderBy("header.".$colOrder,$asc?'ASC':'DESC');

        return $qb->getQuery();

    }

    public function getDirtyHeader($classHeaderName, $country, $page, $colOrder, $asc = false){
        $qb = $this->em->createQueryBuilder();
        $qb->select('pch')
            ->from($classHeaderName, 'pch')
            ->andWhere(
                $qb->expr()->eq('pch.country', '?1'),
                $qb->expr()->eq('pch.cleansed', '?2')
            )
            ->setParameter(1, $country)
            ->setParameter(2, false)
            ->setFirstResult($page*Auditor::PAGE_SIZE)
            ->setMaxResults(Auditor::PAGE_SIZE)
            ->addOrderBy('pch.recommended','DESC')
            ->addOrderBy('pch.'.$colOrder,$asc?'ASC':'DESC');

        return $qb->getQuery();

    }

    public function addCleansedStats($data, $className, $country, $countAliases = false){
        $dql = 'SELECT COUNT(ch) FROM '.$className.' ch WHERE ch.country=?1';
        $data['total'] = $this->em->createQuery($dql)->setParameter(1, $country)->getSingleScalarResult();

        $dql = 'SELECT COUNT(ch) FROM '.$className.' ch WHERE  ch.country=?1 AND ch.locallyValidated=?2';
        $data['cleansedLocallyValidated'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, 1)->getSingleScalarResult();

        $dql = 'SELECT COUNT(ch) FROM '.$className.' ch WHERE  ch.country=?1 AND ch.crossRefValidated=?2';
        $data['cleansedCrossRefValidated'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, 1)->getSingleScalarResult();

        $dql = 'SELECT COUNT(ch) FROM '.$className.' ch WHERE  ch.country=?1 AND ch.dismissed=?2';
        $data['dismissed'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, 1)->getSingleScalarResult();

        $dql = 'SELECT COUNT(ch) FROM '.$className.' ch WHERE  ch.country=?1 AND ch.dismissed=?2 AND ch.locallyValidated=?3 AND ch.crossRefValidated=?4';
        $data['cleansedValidatedForOutput'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, 0)->setParameter(3, true)->setParameter(4, true)->getSingleScalarResult();

        $dql = 'SELECT COUNT(ch) FROM '.$className.' ch WHERE  ch.country=?1 AND ch.recommended=?2';
        $data['recommendedCleansed'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, 1)->getSingleScalarResult();

        if ($countAliases){
            $dql = 'SELECT COUNT(ch) FROM '.$className.' ch WHERE  ch.country=?1 AND NOT(ch.aliases=?2)';
            $data['cleansedMultipleAliases'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, '')->getSingleScalarResult();
        }

        return $data;

    }

    public function loadStats($data, $preClassName, $stgClassName, $country){



        $dql = 'SELECT COUNT(ch) FROM '.$preClassName.' ch WHERE ch.country=?1';
        $data['totalPreSTG'] = $this->em->createQuery($dql)->setParameter(1, $country)->getSingleScalarResult();

        $dql = 'SELECT COUNT(ch) FROM '.$stgClassName.' ch WHERE ch.country=?1 AND ch.locallyValidated=?2 AND ch.crossRefValidated=?3';
        $data['totalValidated'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, true)->setParameter(3, true)->getSingleScalarResult();

        $dql = 'SELECT COUNT(pch) FROM '.$preClassName.' pch WHERE  pch.country=?1 AND pch.cleansed=?2';

        $data['dirtyDirty'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, false)->getSingleScalarResult();
        $data['cleansedDirty'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, true)->getSingleScalarResult();

        $dql = 'SELECT COUNT(pch) FROM '.$preClassName.' pch WHERE  pch.country=?1 AND pch.recommended=?2';

        $data['recommendedDirty'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, true)->getSingleScalarResult();
        $data['unrecommendedDirty'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, false)->getSingleScalarResult();

        $dql = 'SELECT COUNT(pch) FROM '.$preClassName.' pch WHERE  pch.country=?1 AND pch.cleansed=?2 AND pch.recommended=?3';

        $data['cleansedRecommended'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, true)->setParameter(3, true)->getSingleScalarResult();
        $data['cleansedUnrecommended'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, true)->setParameter(3, false)->getSingleScalarResult();

        $data['dirtyRecommended'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, false)->setParameter(3, true)->getSingleScalarResult();

        return $data;

    }

    public function loadCutoffDate($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('c.cutoffDate')
            ->where($qb->expr()->eq('c.country', '?1'))
            ->setParameter(1, $country)
            ->from('Commons\\MAPS\\GLConfigEstandar', 'c')
            ->orderBy('c.migOrder', 'ASC');

        $cutoff = $qb->getQuery()->getResult();

        return $cutoff[0]['cutoffDate']->format('Y-m-d');
    }


    public function loadSnapshotDate($country){

        $debugInfo = array();
        $debugInfo['cmd'] = "EXEC dbo.udf_GetAveriasDate '".$country."'";

        $stmt = $this->em->getConnection()->prepare($debugInfo['cmd']);
        $stmt->execute();

        // loads into memory
        $data['VOs'] = $stmt->fetchAll();

        return $data['VOs'][0]['FECHA'];

        /**
        $sql = 	"SELECT TOP 1 a.FECHA FROM TK".$country."_LATEST.dbo.AVERIAS a ORDER BY a.idAveria DESC";

        $query = $this->db->query($sql);
        $resultArray = $query->result_array();

        return $resultArray[0]['FECHA'];
        **/


    }


    public function loadProjectStats($data, $country){
        $dql = 'SELECT COUNT(ch) FROM Commons\\ORA\\ProjectHeader ch WHERE ch.country=?1 AND (ch.templateName LIKE \'%DM NI TEMPLATE%\' OR ch.templateName LIKE \'%DM MOD TEMPLATE%\')';
        $data['total'] = $this->em->createQuery($dql)->setParameter(1, $country)->getSingleScalarResult();

        $dql = 'SELECT COUNT(pch) FROM Commons\\RAW\\PreProjectHeader pch WHERE  pch.country=?1 AND pch.cleansed=?2 AND (pch.templateName LIKE \'%DM NI TEMPLATE%\' OR pch.templateName LIKE \'%DM MOD TEMPLATE%\')';

        $data['dirtyDirty'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, false)->getSingleScalarResult();
        $data['cleansedDirty'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, true)->getSingleScalarResult();

        $dql = 'SELECT COUNT(pch) FROM Commons\\RAW\\PreProjectHeader pch WHERE  pch.country=?1 AND pch.recommended=?2 AND (pch.templateName LIKE \'%DM NI TEMPLATE%\' OR pch.templateName LIKE \'%DM MOD TEMPLATE%\')';

        $data['recommendedDirty'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, true)->getSingleScalarResult();
        $data['unrecommendedDirty'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, false)->getSingleScalarResult();

        $dql = 'SELECT COUNT(pch) FROM Commons\\RAW\\PreProjectHeader pch WHERE  pch.country=?1 AND pch.cleansed=?2 AND pch.recommended=?3 AND (pch.templateName LIKE \'%DM NI TEMPLATE%\' OR pch.templateName LIKE \'%DM MOD TEMPLATE%\')';

        $data['cleansedRecommended'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, true)->setParameter(3, true)->getSingleScalarResult();
        $data['cleansedUnrecommended'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, true)->setParameter(3, false)->getSingleScalarResult();

        $data['dirtyRecommended'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, false)->setParameter(3, true)->getSingleScalarResult();

        return $data;

    }


    public function loadServiceStats($data, $country){
        $dql = 'SELECT COUNT(ch) FROM Commons\\ORA\\ProjectHeader ch WHERE ch.country=?1 AND ch.templateName LIKE \'%SERV TEMPLATE%\'';
        $data['total'] = $this->em->createQuery($dql)->setParameter(1, $country)->getSingleScalarResult();

        $dql = 'SELECT COUNT(pch) FROM Commons\\RAW\\PreProjectHeader pch WHERE  pch.country=?1 AND pch.cleansed=?2 AND pch.templateName LIKE \'%SERV TEMPLATE%\'';

        $data['dirtyDirty'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, false)->getSingleScalarResult();
        $data['cleansedDirty'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, true)->getSingleScalarResult();

        $dql = 'SELECT COUNT(pch) FROM Commons\\RAW\\PreProjectHeader pch WHERE  pch.country=?1 AND pch.recommended=?2 AND pch.templateName LIKE \'%SERV TEMPLATE%\'';

        $data['recommendedDirty'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, true)->getSingleScalarResult();
        $data['unrecommendedDirty'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, false)->getSingleScalarResult();

        $dql = 'SELECT COUNT(pch) FROM Commons\\RAW\\PreProjectHeader pch WHERE  pch.country=?1 AND pch.cleansed=?2 AND pch.recommended=?3 AND pch.templateName LIKE \'%SERV TEMPLATE%\'';

        $data['cleansedRecommended'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, true)->setParameter(3, true)->getSingleScalarResult();
        $data['cleansedUnrecommended'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, true)->setParameter(3, false)->getSingleScalarResult();

        $data['dirtyRecommended'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, false)->setParameter(3, true)->getSingleScalarResult();

        return $data;

    }

    public function loadRepairStats($data, $country){
        $dql = 'SELECT COUNT(ch) FROM Commons\\ORA\\ProjectHeader ch WHERE ch.country=?1 AND ch.templateName LIKE \'%CC QR TEMPLATE%\'';
        $data['total'] = $this->em->createQuery($dql)->setParameter(1, $country)->getSingleScalarResult();

        $dql = 'SELECT COUNT(pch) FROM Commons\\RAW\\PreProjectHeader pch WHERE  pch.country=?1 AND pch.cleansed=?2 AND pch.templateName LIKE \'%CC QR TEMPLATE%\'';

        $data['dirtyDirty'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, false)->getSingleScalarResult();
        $data['cleansedDirty'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, true)->getSingleScalarResult();

        $dql = 'SELECT COUNT(pch) FROM Commons\\RAW\\PreProjectHeader pch WHERE  pch.country=?1 AND pch.recommended=?2 AND pch.templateName LIKE \'%CC QR TEMPLATE%\'';

        $data['recommendedDirty'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, true)->getSingleScalarResult();
        $data['unrecommendedDirty'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, false)->getSingleScalarResult();

        $dql = 'SELECT COUNT(pch) FROM Commons\\RAW\\PreProjectHeader pch WHERE  pch.country=?1 AND pch.cleansed=?2 AND pch.recommended=?3 AND pch.templateName LIKE \'%CC QR TEMPLATE%\'';

        $data['cleansedRecommended'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, true)->setParameter(3, true)->getSingleScalarResult();
        $data['cleansedUnrecommended'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, true)->setParameter(3, false)->getSingleScalarResult();

        $data['dirtyRecommended'] = $this->em->createQuery($dql)->setParameter(1, $country)->setParameter(2, false)->setParameter(3, true)->getSingleScalarResult();

        return $data;

    }


    public function addPaginationMetadata($data, $country, $page, $listAction){
        if ($data['total']>Auditor::PAGE_SIZE){
            $data['isPaginationNeeded'] = true;
            $data['currentPage'] = $page;
            $data['lastPage']=intval($data['total']/Auditor::PAGE_SIZE);
            $data['listAction'] = $listAction.$country.'/';

        }else{
            $data['isPaginationNeeded'] = false;
        }

        return $data;

    }

    /**
     * @param $headerClass
     * @param $headerKeyColumn
     * @param $headerKeyValue
     * @return mixed
     *
     * returns an instance of $headerClass where $headerKeyColumn matches $headerKeyValue
     *
     * $headerKeyColumn and $headerKeyValue are arrays
     *
     */
    public function getHeaderQuery($headerClass, $headerKeyColumn, $headerKeyValue){
        assert(sizeof($headerKeyColumn)==sizeof($headerKeyValue));

        $qb = $this->em->createQueryBuilder();
        $qb->select('header')
            ->from($headerClass, 'header');

        $i=1;
        foreach($headerKeyColumn as $colName){
            $qb->where($qb->expr()->eq('header.'.$colName, '?'.$i));
        }

        $i=1;
        foreach($headerKeyValue as $colValue){
            $qb->setParameter($i, $colValue);
        }

        $qb->andWhere('header.dismissed=false');

        return $qb->getQuery();
    }

    public function getChildQuery($childClass, $childKeyColumn, $childKeyValue){
        assert(sizeof($childKeyColumn)==sizeof($childKeyValue));

        $qb = $this->em->createQueryBuilder();
        $qb->select('child')
            ->from($childClass, 'child');

        $i=1;
        foreach($childKeyColumn as $colName){
            $qb->where($qb->expr()->eq('child.'.$colName, '?'.$i));
        }

        $i=1;
        foreach($childKeyValue as $colValue){
            $qb->setParameter($i, $colValue);
        }

        return $qb->getQuery();
    }

    public function getPreVO($preClassName, $headerKeyColumn, $headerKeyValue){
        assert(sizeof($headerKeyColumn)==sizeof($headerKeyValue));

        $qb = $this->em->createQueryBuilder();
        $qb->select('a')
            ->from($preClassName, 'a');

        $i=1;
        foreach($headerKeyColumn as $colName){
            $qb->where($qb->expr()->eq('a.'.$colName, '?'.$i));
        }

        $i=1;
        foreach($headerKeyValue as $colValue){
            $qb->setParameter($i, $colValue);
        }

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @param $source
     * @param $target
     * @return mixed
     *
     * clones $source properties into $target (if they exist)
     *
     */

    public function cloneByReflection($source, $target){
        $srcRfx = new ReflectionClass($source);
        $targetRfx = new ReflectionClass($target);

        foreach($srcRfx->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED) as $prop){
            if (($prop->getName()!="id")&&
                ($prop->getName()!="header")&&
                ($prop->getName()!="recommended")&&
                ($prop->getName()!="action")&&
                ($prop->getName()!="cleansed")&&
                ($prop->getName()!="dismissed")&&
                ($prop->getName()!="ranking")&&
                ($prop->getName()!="locallyValidated")&&
                ($prop->getName()!="crossRefValidated")&&
                ($prop->getName()!="mergedId")&&
                ($prop->getName()!="sameCountryParent")&&
                ($prop->getName()!="childless")&&

                ($targetRfx->hasMethod("set".ucfirst($prop->getName())))){
                    $getter = $srcRfx->getMethod("get".ucfirst($prop->getName()));
                    $setter = $targetRfx->getMethod("set".ucfirst($prop->getName()));

                    $setter->invoke($target, $getter->invoke($source));


            }
        }

        return $target;
    }

    public function validateChildren($childClass, $parentKeyName, $parentKeyValue){
        $qb = $this->em->createQueryBuilder();
        $qb->select('src')
            ->from($childClass, 'src');

        foreach($parentKeyName as $key){
            $qb->where($qb->expr()->eq('src.'.$key, '?1'));
        }

        $i=1;
        foreach($parentKeyValue as $value){
            $qb->setParameter($i, $parentKeyValue);
            $i++;
        }

        $srcArray = $qb->getQuery()->getResult();

        foreach($srcArray as $src){
            $src->validate($this->em);
        }

        $this->em->flush();
    }


    /**
     * @param $childPreClass
     * @param $childClass
     * @param $parentKeyName
     * @param $parentKeyValue
     *
     * moves pre-stage $childPreClass to staging table defined by $childClass
     *
     * $parentKeyName and $parentKeyValue are single values
     *
     */

    public function moveChildrenToStage($childPreClass, $childClass, $parentKeyName, $parentKeyValue){
        $qb = $this->em->createQueryBuilder();
        $qb->select('src')
            ->from($childPreClass, 'src');

        foreach($parentKeyName as $key){
            $qb->where($qb->expr()->eq('src.'.$key, '?1'));
        }

        $i=1;
        foreach($parentKeyValue as $value){
            $qb->setParameter($i, $parentKeyValue);
            $i++;
        }

        $srcArray = $qb->getQuery()->getResult();

        foreach($srcArray as $src){
            $target = new $childClass;
            $target = $this->cloneByReflection($src, $target);
            $target->setDismissed(false);

            $this->em->persist($target);
            $this->em->flush();

            $src->setCleansed(true);
            $this->em->persist($src);
            $this->em->flush();
        }


    }

    public function reparentChildren($childClass, $origItemNumber, $parentItemNumber){
        $qb = $this->em->createQueryBuilder();
        $qb->select('src')
            ->from($childClass, 'src');

        $qb->where($qb->expr()->eq('src.origItemNumber', '?1'));

        $qb->setParameter(1, $origItemNumber);

        $srcArray = $qb->getQuery()->getResult();

        foreach($srcArray as $src){
            $src->setItemNumber($parentItemNumber);
            $this->em->persist($src);
        }
        $this->em->flush();
    }


    public function dismissChildren($childClass, $origItemNumber){
        $qb = $this->em->createQueryBuilder();
        $qb->select('src')
            ->from($childClass, 'src');

        $qb->where($qb->expr()->eq('src.origItemNumber', '?1'));

        $qb->setParameter(1, $origItemNumber);

        $srcArray = $qb->getQuery()->getResult();

        foreach($srcArray as $src){
            $src->setDismissed(true);
            $this->em->persist($src);
        }
        $this->em->flush();
    }


    public function toggleChildren($childClass, $parentKeyName, $parentKeyValue, $toggleValue){
        $qb = $this->em->createQueryBuilder();
        $qb->select('src')
            ->from($childClass, 'src');

        foreach($parentKeyName as $key){
            $qb->where($qb->expr()->eq('src.'.$key, '?1'));
        }

        $i=1;
        foreach($parentKeyValue as $value){
            $qb->setParameter($i, $parentKeyValue);
            $i++;
        }

        $srcArray = $qb->getQuery()->getResult();

        foreach($srcArray as $src){
            $src->setDismissed($toggleValue);
        }

        $this->em->flush();


    }


    /**
     * @param $childPreClass
     * @param $childClass
     * @param $parentKeyColumn
     * @param $parentKeyValue
     *
     * discards matching children elements
     *
     * column ID names are given by array $parentKeyColumn
     * parent ID values are given by $parentKeyValue
     *
     * matching $childClass elements on staging table are removed
     *
     * matching $childPreClass elements on pre-staging tables revert to dirty state
     *
     *
     */
    public function discardChildren($childPreClass, $childClass, $parentKeyColumn, $parentKeyValue){
        assert(sizeof($parentKeyColumn)==sizeof($parentKeyValue));

        $qb = $this->em->createQueryBuilder();
        $qb->select('child')
            ->from($childClass, 'child');

        for($i=0;$i<sizeof($parentKeyColumn);$i++){
            $qb->where($qb->expr()->eq('child.'.$parentKeyColumn[$i], '?'.$i));
        }

        for($i=0;$i<sizeof($parentKeyValue);$i++){
            $qb->setParameter($i, $parentKeyValue[$i]);
        }

        $childArray = $qb->getQuery()->getResult();

        foreach($childArray as $child){
            $this->em->remove($child);
            $this->em->flush();
        }

        $qb = $this->em->createQueryBuilder();
        $qb->select('pre')
            ->from($childPreClass, 'pre');

        for($i=0;$i<sizeof($parentKeyColumn);$i++){
            $qb->where($qb->expr()->eq('pre.'.$parentKeyColumn[$i], '?'.$i));
        }

        for($i=0;$i<sizeof($parentKeyValue);$i++){
            $qb->setParameter($i, $parentKeyValue[$i]);
        }

        $preArray = $qb->getQuery()->getResult();

        foreach($preArray as $pre){
            $pre->setCleansed(false);
            $this->em->persist($pre);
            $this->em->flush();
        }

    }

    /**
     * @param $headerClass
     * @param $headerKeyColumn
     * @param $headerKeyValue
     * @param $redirectAction
     *
     * notifies the header entity to perform cross-reference validations
     *
     * redirects the output to proper header list (e.g Customers/cleansed/PA)
     *
     * to be called after successful updates to children:
     *
     * (e.g. updateCleansed, customUpdateCleansed, cleanseDirty, discardCleansed)
     *
     */
    public function postUpdate($headerClass, $headerKeyColumn, $headerKeyValue, $redirectAction){

        $h = $this->getHeaderQuery($headerClass, $headerKeyColumn, $headerKeyValue)->getSingleResult();

        $h->validateCrossRef($this->em);
        $this->em->merge($h);
        $this->em->flush();

        redirect(base_url().$redirectAction);
    }

    /**
     * @param $voClassName
     * @param $vo
     * @return mixed
     *
     * given an instance of a $vo, get its associated fields (by country)
     *
     * good for setting up Form validations, and loading values for templating
     *
     */
    public function getFieldsAssociatedToVO($voClassName, $vo){
        $qb = $this->em->createQueryBuilder();
        $qb->select('f', 'd', 'g')
            ->from('Commons\\IDL\\IDLFieldDescriptor', 'f')
            ->leftJoin('f.descriptor', 'd')
            ->leftJoin('d.group', 'g')
            ->andWhere(
                $qb->expr()->eq('g.country', '?1'),
                $qb->expr()->eq('d.shortName',  '?2')
            )
            ->setParameter(1, $vo->getCountry())
            ->setParameter(2, $this->extractShortName($voClassName));

        $fields = $qb->getQuery()->getResult();

        return $fields;
    }

    public function getFieldsAssociatedToClassNameAndCountry($voClassName, $country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('f', 'd', 'g')
            ->from('Commons\\IDL\\IDLFieldDescriptor', 'f')
            ->leftJoin('f.descriptor', 'd')
            ->leftJoin('d.group', 'g')
            ->andWhere(
                $qb->expr()->eq('g.country', '?1'),
                $qb->expr()->eq('d.shortName',  '?2')
            )
            ->setParameter(1, $country)
            ->setParameter(2, $this->extractShortName($voClassName));

        $fields = $qb->getQuery()->getResult();

        return $fields;
    }

    /**
     * @param $voClassName
     * @param $id
     * @param $formValidation
     * @return mixed
     *
     *
     */

    public function loadValidationRules($voClassName, $id, $formValidation){
        $vo = $this->getVOByID($voClassName, $id);
        $fields = $this->getFieldsAssociatedToVO($voClassName, $vo);

        foreach($fields as $f) {
            $formRules = $f->getFormRules();
            if (!isset($formRules)||($formRules!='')){
                $formValidation->set_rules($f->getShortName(), $f->getShortName(), $f->getFormRules());
            }
        }

        return $formValidation;
    }




    /**
     * @param $voClassName
     * @param $id
     * @param $input
     * @return mixed
     *
     * applies special parsing rules to load values from HTML forms
     *
     */

    public function loadInstanceWithFormValues($voClassName, $id, $input){
        $vo = $this->getVOByID($voClassName, $id);
        $fields = $this->getFieldsAssociatedToVO($voClassName, $vo);

        $voRfx = new \ReflectionClass($vo);

        foreach($fields as $f){
            assert($voRfx->hasMethod('set'.ucfirst(trim($f->getShortName()))));
            $setter = $voRfx->getMethod('set'.ucfirst(trim($f->getShortName())));

            switch ($f->getORA_TYPE()) {
                // a checkbox
                case 'VARCHAR2(1)':
                    $formValue = $input->post($f->getShortName());
                    if (isset($formValue)) {
                        $setter->invoke($vo, true);
                    }else{
                        $setter->invoke($vo, false);

                    }
                    break;

                // a date-picker
                case 'DATE':
                    $formValue = $input->post($f->getShortName());
                    if (isset($formValue)) {
                        if (DateTime::createFromFormat('d-m-Y',trim($this->input->post($f->getShortName())))){
                            $setter->invoke($vo, DateTime::createFromFormat('d-m-Y',$this->input->post($f->getShortName())));
                        }
                    }
                    break;

                // a numerical value
                case 'NUMBER':
                    $formValue = $input->post($f->getShortName());
                    if (isset($formValue)) {
                        if ($formValue==''){
                            $setter->invoke($vo, null);

                        }else{
                            $setter->invoke($vo, trim($formValue));

                        }

                    }
                    break;

                // default textual field
                default:
                    $formValue = $input->post(trim($f->getShortName()));

                    // a little juggling with encoding is necessary, all forms are UTF-8, but SQL Server storage is ISO-8559-1
                    //
                    if (isset($formValue)) {
                        if ($f->getShortName()!='classification'){
                            $encodedValue =  trim(mb_strtoupper(mb_convert_encoding($formValue, 'ISO-8859-1', 'UTF-8'), 'ISO-8859-1'));

                        }else{
                            $encodedValue =  trim(mb_convert_encoding($formValue, 'ISO-8859-1', 'UTF-8'), 'ISO-8859-1');
                        }

                        $setter->invoke($vo, $encodedValue);
                    }
                    break;
            }

        }

        return $vo;


    }

    public function createInstanceWithFormValues($voClassName, $country, $input){
        $vo = new $voClassName;
        $fields = $this->getFieldsAssociatedToClassNameAndCountry($voClassName, $country);

        $voRfx = new \ReflectionClass($vo);

        foreach($fields as $f){
            assert($voRfx->hasMethod('set'.ucfirst(trim($f->getShortName()))));
            $setter = $voRfx->getMethod('set'.ucfirst(trim($f->getShortName())));

            switch ($f->getORA_TYPE()) {
                // a checkbox
                case 'VARCHAR2(1)':
                    $formValue = $input->post($f->getShortName());
                    if (isset($formValue)) {
                        $setter->invoke($vo, true);
                    }else{
                        $setter->invoke($vo, false);

                    }
                    break;

                // a date-picker
                case 'DATE':
                    $formValue = $input->post($f->getShortName());
                    if (isset($formValue)) {
                        if (DateTime::createFromFormat('d-m-Y',trim($this->input->post($f->getShortName())))){
                            $setter->invoke($vo, DateTime::createFromFormat('d-m-Y',$this->input->post($f->getShortName())));
                        }
                    }
                    break;

                // a numerical value
                case 'NUMBER':
                    $formValue = $input->post($f->getShortName());
                    if (isset($formValue)) {
                        $setter->invoke($vo, trim($formValue));

                    }
                    break;

                // default textual field
                default:
                    $formValue = $input->post(trim($f->getShortName()));

                    // a little juggling with encoding is necessary, all forms are UTF-8, but SQL Server storage is ISO-8559-1
                    //
                    if (isset($formValue)) {
                        if ($f->getShortName()!='classification'){
                            $encodedValue =  trim(mb_strtoupper(mb_convert_encoding($formValue, 'ISO-8859-1', 'UTF-8'), 'ISO-8859-1'));

                        }else{
                            $encodedValue =  trim(mb_convert_encoding($formValue, 'ISO-8859-1', 'UTF-8'), 'ISO-8859-1');
                        }

                        $setter->invoke($vo, $encodedValue);
                    }
                    break;
            }

        }

        return $vo;


    }



    /**
     * @param $voClassName
     * @return mixed
     *
     */

    public function extractShortName($voClassName){
        $voClassNameParts = explode('\\', $voClassName);

        assert(sizeof($voClassNameParts)>1);

        return $voClassNameParts[sizeof($voClassNameParts)-1];
    }




}