<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 31/10/2018
 * Time: 17:37
 */

include_once APPPATH . 'controllers/MY_Controller.php';

Use Doctrine\ORM\Query;


class POParams extends MY_Controller
{
    const LAYOUT_WELCOME = array('POParams.tpl');



    public function __construct()
    {
        parent::__construct();

        $this->load->helper('form');
        $this->load->library('form_validation');
    }

    public function index($country='PA')
    {
        $this->session->selGroup='POParams';
        $this->session->selCountry = $country;

        $data = array();

        $data['TipoCont'] = $this->loadPOTipoCont($country);

        $this->startEditSequence($country, $data);
    }

    private function startEditSequence($country, $data){
        $data['country'] = $country;
        $data = $this->loadCommonData($data,$country.' PO Params', '', '', '' , '');

        $this->displayStandardLayout(self::LAYOUT_WELCOME, $data);
    }

    private function loadPOTipoCont($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLTipoCont', 'vo') // TABELA DO GL
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function updatePOTipoCont($country)
    {
        $this->form_validation->set_rules('id', 'id', 'required');
        $this->form_validation->set_rules('tipo', 'tipo', 'required|max_length[2]');
        $this->form_validation->set_rules('descrip', 'descrip', 'required|max_length[50]');
        $this->form_validation->set_rules('tipoOracle', 'tipoOracle', 'required|max_length[6]');

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['topActive'] = 'tablas';
            $data['bottomActive'] = 'TipoCont';

            $data['errorTipoCont'] = str_replace("</p>", "", str_replace("<p>", "", $this->form_validation->error('tipo')));
            $data['errorDescripTipoCont'] = str_replace("</p>", "", str_replace("<p>", "", $this->form_validation->error('descrip')));
            $data['errorTipoContOracle'] = str_replace("</p>", "", str_replace("<p>", "", $this->form_validation->error('tipoOracle')));

            $this->session->selGroup = 'POParams';
            $this->session->selCountry = $country;

            $data['country'] = $country;
            $data = $this->loadCommonData($data, $country . ' Laundry PO Params', '', '', '', '');

            $this->startEditSequence($country, $data);

        } else {

            $qb = $this->em->createQueryBuilder();
            $qb->select('vo')
                ->from('Commons\\MAPS\\GLTipoCont', 'vo') // TABELA DO GL
                ->where($qb->expr()->eq('vo.id', '?1'))
                ->setParameter(1, $this->input->post('id'));

            $vo = $qb->getQuery()->getSingleResult();
            $vo->settipoOracle($this->input->post('tipoOracle'));

            $this->em->merge($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "update GLTipoCont " . $vo->gettipo() . " - " . $vo->getDescrip() . " - " . $vo->gettipoOracle(), null, $vo->getCountry(), 'POP_CHANGE');

            $this->session->topActive = 'tablas';
            $this->session->bottomActive = 'TipoCont';

            redirect(base_url() . "POParams/" . $country);
        }
    }

}