<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

    public function view($page = 'home')
    {
        $this->load->library('session');

        if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter
        $data['sessionCountry'] = strtolower($this->session->sessionCountry);
        $data['sessionGroup'] = $this->session->sessionGroup;

        $this->load->helper('url_helper');
        $this->load->helper('url');

        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
}