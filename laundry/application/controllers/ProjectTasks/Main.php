<?php
include_once APPPATH . 'controllers/MY_Controller.php';

/**
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/query-builder.html
 *
 * http://www.doctrine-project.org/api/orm/2.5/class-Doctrine.ORM.QueryBuilder.html
 *
 * ProjectTasks Controller (detail style)
 *
 */

Use Doctrine\ORM\Query;


class Main extends MY_Controller {

    const VO_CLASS_NAME = 'Commons\\ORA\\ProjectTasks';
    const VO_PRE_CLASS_NAME = 'Commons\\RAW\\PreProjectTasks';

    const HEADER1_CLASS_NAME = 'Commons\\ORA\\ProjectHeader';
    const HEADER1_KEY_COLUMNS = array('projectName');

    const ACTION_BASE = 'ProjectTasks/updateCleansed/';
    const ACTION_CUSTOM = 'ProjectTasks/customUpdateCleansed/';
    const ACTION_LABEL = 'Update ProjectTasks';

    const LAYOUT_BASE = array(
        'ORA/ProjectTasks/cleansedTopBar.tpl',
        'ORA/ProjectTasks/formBase.tpl'
    );

    const LAYOUT_CUSTOM = array(
        'ORA/ProjectTasks/cleansedTopBar.tpl',
        'ORA/ProjectTasks/formCustom.tpl'
    );

    const LAYOUT_CUSTOM_CO = array(
        'ORA/ProjectTasks/cleansedTopBar.tpl',
        'ORA/ProjectTasks/formCustom.tpl'
    );

    public function __construct()
    {
        parent::__construct();

        $this->load->helper('form');
        $this->load->library('form_validation');

    }

    public function editCleansed($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence(null, $data);
    }

    public function customEditCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence($country, $data);
    }

    public function createNew($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $header = $this->getVOByID('Commons\\ORA\\ProjectHeader', $id);
        $data['id'] = 'NEW';

        $data['projectName']=$header->getProjectName();

        $data['startDate']=$header->getStartDate()->format('d-m-Y');
        $data['finishDate']=$header->getCompletionDate()->format('d-m-Y');

        $data['description']="NEW TASK DESCRIPTION";

        $data['attribute2']='OTHER';
        $data['attribute3']='*NULL*';
        $data['attribute7']='NONE';

        $data['country'] = $header->getCountry();

        $this->startEditSequence($data['country'], $data);
    }


    private function startEditSequence($country, $data){
        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($data['projectName']))->getSingleResult();


        switch($country){
            case 'PA':
                $layoutByCountry = self::LAYOUT_CUSTOM;
                break;

            case 'CO':
            case 'MX':
            case 'NI':
            case 'HN':
            case 'CR':
            case 'SV':
            case 'GT':
            default:
                $layoutByCountry = self::LAYOUT_CUSTOM_CO;
                break;

        }

        if ($data['id']=='NEW'){
            $actionCustomByCountry = "ProjectTasks/addNew/". $header->getId();

        }else{
            $actionCustomByCountry = "ProjectTasks/customUpdateCleansed/". $country.'/'. $data['id'];

        }

        if (strstr($header->getTemplateName(), 'SERV TEMPLATE')) {
            $cancelAction = 'Services/customEditCleansed/' . $country . '/' . $header->getId();

        }else if (strstr($header->getTemplateName(), 'CC QR TEMPLATE')){
            $cancelAction = 'Repairs/customEditCleansed/'.$country.'/'.$header->getId();

        }else{
            $cancelAction = 'Projects/customEditCleansed/'.$country.'/'.$header->getId();

        }

        $data = $this->loadOraProjectFF($data);

        $data = $this->loadCommonData(
            $data,
            'Project Top Task #' . $data['id'],
            $actionCustomByCountry,
            self::ACTION_LABEL,
            '',
            $cancelAction
        );

        $this->displayStandardLayout($layoutByCountry, $data);
    }

    public function updateCleansed($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->loadValidationRules(self::VO_CLASS_NAME, $id, $this->form_validation);

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence(null, $data);

        }else{
            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);
            $this->em->flush();

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getprojectName()))->getSingleResult();

            $header->validateCrossRef($this->em);
            $this->em->flush();

            if (strstr($header->getTemplateName(), 'QR')){
                redirect(base_url() . "Repairs/customEditCleansed/" . $header->getCountry().'/'.$header->getId());

            }else if (strstr($header->getTemplateName(), 'SERV TEMPLATE')){
                redirect(base_url() . "Services/customEditCleansed/" . $header->getCountry().'/'.$header->getId());

            }else{
                redirect(base_url() . "Projects/customEditCleansed/" . $header->getCountry().'/'.$header->getId());

            }



        }
    }

    public function addNew($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->form_validation->set_rules('taskName', 'taskName', 'required|max_length[20]');
        $this->form_validation->set_rules('attribute2', 'attribute2', 'required');
        $this->form_validation->set_rules('attribute3', 'attribute3', 'required');
        $this->form_validation->set_rules('attribute7', 'attribute7', 'required');

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID('Commons\\ORA\\ProjectHeader', $id);
            $data['id'] = 'NEW';

            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data['errorTaskName'] = str_replace("</p>", "", str_replace("<p>", "",$this->form_validation->error('taskName')));
            $data['errorAttribute2'] = str_replace("</p>", "", str_replace("<p>", "",$this->form_validation->error('attribute2')));
            $data['errorAttribute3'] = str_replace("</p>", "", str_replace("<p>", "",$this->form_validation->error('attribute3')));
            $data['errorAttribute7'] = str_replace("</p>", "", str_replace("<p>", "",$this->form_validation->error('attribute7')));

            $this->startEditSequence($data['country'], $data);

        }else{
            $header = $this->getVOByID('Commons\\ORA\\ProjectHeader', $id);

            $vo = $this->createInstanceWithFormValues(self::VO_CLASS_NAME, $header->getCountry(), $this->input);
            $vo->setProjectName($header->getProjectName());
            $vo->setSiglaPrimaryKey($header->getSiglaPrimaryKey());
            $vo->setCountry($header->getCountry());
            $vo->setAttribute5($header->getCompletionDate()->format('d-m-Y'));
            $vo->setOrgName($header->getOrgName());

            if (strstr($header->getTemplateName(),'NI')){
                $vo->setServiceType('NI');

            }elseif (strstr($header->getTemplateName(),'MOD')){
                $vo->setServiceType('MOD');

            }
            $this->em->persist($vo);
            $this->em->flush();


            $header->validateCrossRef($this->em);
            $this->em->flush();

            if (strstr($header->getTemplateName(), 'QR')){
                redirect(base_url() . "Repairs/customEditCleansed/" . $header->getCountry().'/'.$header->getId());

            }else if (strstr($header->getTemplateName(), 'SERV TEMPLATE')){
                redirect(base_url() . "Services/customEditCleansed/" . $header->getCountry().'/'.$header->getId());

            }else{
                redirect(base_url() . "Projects/customEditCleansed/" . $header->getCountry().'/'.$header->getId());

            }



        }

    }


    public function validate($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->validate($this->em);
        $this->em->flush();

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($vo->getProjectName()))->getSingleResult();

        $header->validate($this->em);
        $this->em->flush();

        $header->validateCrossRef($this->em);
        $this->em->flush();

        $this->customEditCleansed($vo->getCountry(), $id);
    }




    public function discard($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        try{
            $stg = $this->getVOByID("Commons\\ORA\\ProjectTasks",$id);
            $this->em->remove($stg);
            $this->em->flush();

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($stg->getprojectName()))->getSingleResult();

            $header->validateCrossRef($this->em);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "discarding ProjectTask ".$id." - ".$stg->getProjectName(), null, $stg->getCountry());

            if (strstr($header->getTemplateName(), 'QR')){
                redirect(base_url() . "Repairs/customEditCleansed/" . $header->getCountry().'/'.$header->getId());

            }else if (strstr($header->getTemplateName(), 'SERV TEMPLATE')){
                redirect(base_url() . "Services/customEditCleansed/" . $header->getCountry().'/'.$header->getId());

            }else{
                redirect(base_url() . "Projects/customEditCleansed/" . $header->getCountry().'/'.$header->getId());

            }

        }catch(\Exception $e){
            $this->em->flush();
            $this->auditor->log($_SERVER['REMOTE_ADDR'], "bad data for ProjectTask discard -->".$id.'-'.$e->getMessage(), null, 'ZZ');
            redirect(base_url() . 'Debug');
        }


    }


    public function customUpdateCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        // uncomment rules that fit the custom ProjectTasks form
        switch($country){
            default:
                $this->form_validation->set_rules('attribute2', 'attribute2', 'required|max_length[40]');
                $this->form_validation->set_rules('attribute3', 'attribute3', 'required|max_length[40]');
                $this->form_validation->set_rules('attribute7', 'attribute7', 'max_length[40]');
                //$this->form_validation->set_rules('projectName', 'projectName', 'required|max_length[30]');
                //$this->form_validation->set_rules('taskNumber', 'taskNumber', 'required|max_length[25]');
                //$this->form_validation->set_rules('taskName', 'taskName', 'required|max_length[20]');
                //$this->form_validation->set_rules('parentTaskNumber', 'parentTaskNumber', 'max_length[25]');
                //$this->form_validation->set_rules('description', 'description', 'max_length[250]');
                //$this->form_validation->set_rules('startDate', 'startDate', 'required');
                //$this->form_validation->set_rules('serviceType', 'serviceType', 'required|max_length[30]');
                //$this->form_validation->set_rules('attribute1', 'attribute1', 'max_length[20]');
                //$this->form_validation->set_rules('attribute4', 'attribute4', 'max_length[35]');
                //$this->form_validation->set_rules('attribute5', 'attribute5', 'required|max_length[11]');
                //$this->form_validation->set_rules('attribute6', 'attribute6', 'required');
                //$this->form_validation->set_rules('attribute8', 'attribute8', 'max_length[40]');
                //$this->form_validation->set_rules('noOfFloors', 'noOfFloors', 'max_length[25]');
                //$this->form_validation->set_rules('speed', 'speed', 'max_length[25]');
                //$this->form_validation->set_rules('capacity', 'capacity', 'max_length[25]');
                //$this->form_validation->set_rules('orgName', 'orgName', 'required|max_length[240]');

                break;
        }

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence($country, $data);

        }else{

            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);

            $vo->setAttribute2(str_replace('_',' ', $vo->getAttribute2()) );
            $vo->setAttribute7(str_replace('_',' ', $vo->getAttribute7()) );
            $vo->setAttribute3(str_replace('_',' ', $vo->getAttribute3()) );
            $this->em->flush();

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getprojectName()))->getSingleResult();

            $header->validateCrossRef($this->em);
            $this->em->flush();

            if (strstr($header->getTemplateName(), 'QR')){
                redirect(base_url() . "Repairs/customEditCleansed/" . $header->getCountry().'/'.$header->getId());

            }else if (strstr($header->getTemplateName(), 'SERV')){
                redirect(base_url() . "Services/customEditCleansed/" . $header->getCountry().'/'.$header->getId());

            }else{
                redirect(base_url() . "Projects/customEditCleansed/" . $header->getCountry().'/'.$header->getId());

            }


        }
    }


}
