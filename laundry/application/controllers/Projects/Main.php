<?php
include_once APPPATH . 'controllers/MY_Controller.php';

/**
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/query-builder.html
 *
 * http://www.doctrine-project.org/api/orm/2.5/class-Doctrine.ORM.QueryBuilder.html
 *
 * ProjectHeader Controller (header style)
 *
 */

Use Doctrine\ORM\Query;


class Main extends MY_Controller {

    const VO_CLASS_NAME = 'Commons\\ORA\\ProjectHeader';
    const VO_PRE_CLASS_NAME = 'Commons\\RAW\\PreProjectHeader';

    const CHILD1_CLASS_NAME = 'Commons\\ORA\\ProjectClassifications';
    const CHILD1_PRE_CLASS_NAME = 'Commons\\RAW\\PreProjectClassifications';
    const CHILD1_KEY_COLUMNS = array('projectName');

    const CHILD2_CLASS_NAME = 'Commons\\ORA\\ProjectTasks';
    const CHILD2_PRE_CLASS_NAME = 'Commons\\RAW\\PreProjectTasks';
    const CHILD2_KEY_COLUMNS = array('projectName');

    const CHILD3_CLASS_NAME = 'Commons\\ORA\\ProjectRetRules';
    const CHILD3_PRE_CLASS_NAME = 'Commons\\RAW\\PreProjectRetRules';
    const CHILD3_KEY_COLUMNS = array('projectName');


    const ACTION_CLEANSE = 'Projects/cleanseDirty/';
    const ACTION_CLEANSE_LABEL = 'Mark as Cleansed';

    const ACTION_BASE = 'Projects/updateCleansed/';
    const ACTION_CUSTOM = 'Projects/customUpdateCleansed/';
    const ACTION_LABEL = 'Update ProjectHeader';

    const CANCEL = "Projects/cleansed/";
    const CANCEL_DIRTY = "Projects/dirty/";
    const DISCARD = 'Projects/discardCleansed/';

    const DEFAULT_ALL = [
        'frm' => array(
            'ORA/ProjectHeader/topBarCleansedProject.tpl',
            'ORA/ProjectHeader/frmCustomALL.tpl')
    ];

    const VIEW_LAYOUTS = array(
        "CO" => self::DEFAULT_ALL,
        "MX" => self::DEFAULT_ALL,
        "SV" => self::DEFAULT_ALL,
        "NI" => self::DEFAULT_ALL,
        "HN" => self::DEFAULT_ALL,
        "GT" => self::DEFAULT_ALL,
        "CR" => self::DEFAULT_ALL,
        "CL" => self::DEFAULT_ALL,
        "AR" => self::DEFAULT_ALL,
        "PE" => self::DEFAULT_ALL,
        "UY" => self::DEFAULT_ALL,
        "PY" => self::DEFAULT_ALL
    );

    const LAYOUT_CLEANSED = array(
        'ORA/ProjectHeader/topBarCleansedProjects.tpl',
        'ORA/ProjectHeader/lstCleansedProjects.tpl'
    );

    const LAYOUT_DIRTY = array(
        'ORA/ProjectHeader/topBarDirtyProjects.tpl',
        'ORA/ProjectHeader/lstDirtyProjects.tpl'
    );

    const LAYOUT_EDIT_DIRTY = array(
        'ORA/ProjectHeader/formBase.tpl'
    );

    const LAYOUT_ERROR = array(
        'ORA/ProjectHeader/error.tpl'
    );

    public function __construct()
    {
        parent::__construct();

    }

    public function listCleansed($country, $page=0, $sortCol=null, $sortAsc=null){
        if ($this->checkPreConditions($country)) {
            show_error('please contact your system administrator');
        }

        if (!isset($sortCol)){
            $sortCol = $this->session->selCol;
        }

        if (!isset($sortAsc)){
            $sortAsc = $this->session->selAsc;
        }


        switch($sortCol){
            case 'id':
                $data['cleansedVOs'] = $this->getCleansedProjects($country, $page, 'pmProjectReference', $sortAsc==1)->getResult(Query::HYDRATE_ARRAY);
                break;

            case 'name':
                $data['cleansedVOs'] = $this->getCleansedProjects($country, $page, 'projectName', $sortAsc==1)->getResult(Query::HYDRATE_ARRAY);
                break;

            case 'error':
                $this->session->selAsc = true;
                $sortAsc = true;
                $data['cleansedVOs'] = $this->getCleansedProjects($country, $page, 'crossRefValidated', $sortAsc==1)->getResult(Query::HYDRATE_ARRAY);
                break;

            default:
            case 'lastUpdated':
                $sortAsc = false;
                $this->session->selAsc = false;

                $data['cleansedVOs'] = $this->getCleansedProjects( $country, $page, 'lastUpdated', false)->getResult(Query::HYDRATE_ARRAY);
                break;
        }

        $data['country'] = $country;
        $data['sortOrder'] = $sortCol;
        $data['sortDirection'] = $sortAsc==1?'ASC':'DESC';

        $this->session->selGroup='Projects';
        $this->session->selCountry=strtolower($country);

        $data = $this->loadCommonData($data,' Cleansed Projects ', '', '', '' , '');

        $data = $this->loadProjectStats($data, $country);
        $data['total'] = $data['total'];

        $data = $this->addPaginationMetadata($data, $country, $page, self::CANCEL);

        $this->displayStandardLayout(self::LAYOUT_CLEANSED, $data);
    }

    private function getCleansedProjects($country, $page, $colOrder, $asc = false){
        $qb = $this->em->createQueryBuilder();
        $qb->select('header')
            ->from(self::VO_CLASS_NAME, 'header')
            ->andWhere(
                $qb->expr()->eq('header.country', '?1'),
                $qb->expr()->orX(
                    $qb->expr()->like('header.templateName', '?2'),
                    $qb->expr()->like('header.templateName', '?3')))
            ->setParameter(1, $country)
            ->setParameter(2, '%DM MOD TEMPLATE%')
            ->setParameter(3, '%DM NI TEMPLATE%')
            ->setFirstResult($page*Auditor::PAGE_SIZE)
            ->setMaxResults(Auditor::PAGE_SIZE)
            ->addOrderBy('header.dismissed','ASC')
            ->addOrderBy("header.".$colOrder,$asc?'ASC':'DESC');

        return $qb->getQuery();

    }

    public function listDirty($country, $page = 0){
        if ($this->checkPreConditions($country)) {
            show_error('please contact your system administrator');
        }

        $data['dirtyVOs'] = $this->getDirtyProjects($country, $page, 'pmProjectReference')->getResult(Query::HYDRATE_ARRAY);

        $data['country'] = $country;

        $this->session->selGroup='Projects';
        $this->session->selCountry=strtolower($country);

        $data = $this->loadCommonData($data,' Dirty Projects ', '', '', '' , '');

        $data = $this->loadProjectStats($data, $country);

        $data['total'] = $data['dirtyDirty'];
        $data = $this->addPaginationMetadata($data, $country, $page, self::CANCEL_DIRTY);

        $this->displayStandardLayout(self::LAYOUT_DIRTY, $data);
    }

    private function getDirtyProjects($country, $page, $colOrder, $asc = false){
        $qb = $this->em->createQueryBuilder();
        $qb->select('header')
            ->from(self::VO_PRE_CLASS_NAME, 'header')
            ->andWhere(
                $qb->expr()->eq('header.country', '?1'),
                $qb->expr()->neq('header.cleansed', '?2'),
                $qb->expr()->orX(
                    $qb->expr()->like('header.templateName', '?3'),
                    $qb->expr()->like('header.templateName', '?4')))
            ->setParameter(1, $country)
            ->setParameter(2, true)
            ->setParameter(3, '%DM MOD TEMPLATE%')
            ->setParameter(4, '%DM NI TEMPLATE%')
            ->setFirstResult($page*Auditor::PAGE_SIZE)
            ->setMaxResults(Auditor::PAGE_SIZE)
            ->addOrderBy('header.dismissed','ASC')
            ->addOrderBy("header.".$colOrder,$asc?'ASC':'DESC');

        return $qb->getQuery();

    }

    public function editDirty($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }
        $data = $this->getVOHidratedByID(self::VO_PRE_CLASS_NAME,$id);

        $data = $this->loadCommonData(
            $data,
            'Edit dirty ' . $data['country'] . ' Project #' . $id,
            self::ACTION_CLEANSE . $data['id'],
            self::ACTION_CLEANSE_LABEL,
            '',
            self::CANCEL_DIRTY.$data['country']
        );

        $this->displayStandardLayout(self::LAYOUT_EDIT_DIRTY, $data);
    }


    public function toggle($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->setDismissed(!$vo->getDismissed());
        $this->em->merge($vo);
        $this->em->flush();

        $this->toggleChildren(self::CHILD1_CLASS_NAME,self::CHILD1_KEY_COLUMNS,  array($vo->getProjectName()),$vo->getDismissed());
        $this->toggleChildren(self::CHILD2_CLASS_NAME,self::CHILD2_KEY_COLUMNS,  array($vo->getProjectName()),$vo->getDismissed());
        $this->toggleChildren(self::CHILD3_CLASS_NAME,self::CHILD3_KEY_COLUMNS,  array($vo->getProjectName()),$vo->getDismissed());

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "toggling to". $vo->getDismissed() .  " ProjectHeader ".$id." - ".$vo->getProjectName(), null, $vo->getCountry());

        redirect(base_url() . self::CANCEL . $vo->getCountry());
    }

    public function validate($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->validate($this->em);

        $this->validateChildren( self::CHILD1_CLASS_NAME, self::CHILD1_KEY_COLUMNS, array($vo->getProjectName()) );
        $this->validateChildren( self::CHILD2_CLASS_NAME, self::CHILD2_KEY_COLUMNS, array($vo->getProjectName()) );
        $this->validateChildren( self::CHILD3_CLASS_NAME, self::CHILD2_KEY_COLUMNS, array($vo->getProjectName()) );

        $vo->validateCrossRef($this->em);
        $this->em->flush();

        $this->customEditCleansed($vo->getCountry(), $id);
    }



    public function cleanseDirty($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        /**
         * cleanse parent - clone header from pre-stage to stage table
         *
         */

        $qb = $this->em->createQueryBuilder();
        $qb->select('pch')
            ->from(self::VO_PRE_CLASS_NAME, 'pch')
            ->where($qb->expr()->eq('pch.id', '?1'))
            ->setParameter(1, $id);

        $pre = $qb->getQuery()->getSingleResult();

        $stg = self::VO_CLASS_NAME;
        $stg = new $stg;
        $stg = $this->cloneByReflection($pre, $stg);
        $stg->setDismissed(false);

        $this->em->persist($stg);
        $this->em->flush();

        $pre->setCleansed(true);
        $this->em->flush();

        /**
         * cleanse children - clones dependencies from pre-stage to stage table
         *
         */
        $this->moveChildrenToStage( self::CHILD1_PRE_CLASS_NAME, self::CHILD1_CLASS_NAME, self::CHILD1_KEY_COLUMNS, array($stg->getProjectName()) );
        $this->moveChildrenToStage( self::CHILD2_PRE_CLASS_NAME, self::CHILD2_CLASS_NAME, self::CHILD2_KEY_COLUMNS, array($stg->getProjectName()) );
        $this->moveChildrenToStage( self::CHILD3_PRE_CLASS_NAME, self::CHILD3_CLASS_NAME, self::CHILD3_KEY_COLUMNS, array($stg->getProjectName()) );

        $stg->validateCrossRef($this->em);
        $this->em->flush();

        $this->session->selGroup='Projects';
        $this->session->selCountry=strtolower($stg->getCountry());
        $this->session->selCol = "lastUpdated";
        $this->session->selAsc = false;


        $this->auditor->log($_SERVER['REMOTE_ADDR'], "cleansing ProjectHeader ".$id." - ".$stg->getProjectName(). " - ".$stg->getPmProjectReference(), null, $stg->getCountry());
        redirect(base_url().self::CANCEL.$stg->getCountry());

    }

    public function discardCleansed($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $stg = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $pre = $this->getPreVO(self::VO_PRE_CLASS_NAME, array('projectName'), array($stg->getProjectName()));

        $this->em->remove($stg);
        $pre->setCleansed(false);
        $this->em->persist($pre);
        $this->em->flush();

        $this->discardChildren( self::CHILD1_PRE_CLASS_NAME,
            self::CHILD1_CLASS_NAME,
            self::CHILD1_KEY_COLUMNS,
            array($stg->getProjectName()));
        $this->discardChildren( self::CHILD2_PRE_CLASS_NAME,
            self::CHILD2_CLASS_NAME,
            self::CHILD2_KEY_COLUMNS,
            array($stg->getProjectName()));
        $this->discardChildren( self::CHILD3_PRE_CLASS_NAME,
            self::CHILD3_CLASS_NAME,
            self::CHILD3_KEY_COLUMNS,
            array($stg->getProjectName()));

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "discarding ProjectHeader ".$id." - ".$stg->getProjectName(), null, $stg->getCountry());

        redirect(base_url().self::CANCEL_DIRTY.$stg->getCountry());

    }

    public function editCleansed($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $queryBuilder = $this->em->createQueryBuilder();
        $queryBuilder->select('pcbt')
            ->from('Commons\\ORA\\ProjectClassifications', 'pcbt')
            ->andWhere($queryBuilder->expr()->eq('pcbt.projectName', '?1'), $queryBuilder->expr()->eq('pcbt.classCategory', '?2'))
            ->setParameter(1, $data['projectName'])
            ->setParameter(2, 'BUILDING TYPE');

        $pcbt = $queryBuilder->getQuery()->getSingleResult();

        $data['buildingType'] = $pcbt->getClassCode();

        $queryBuilder = $this->em->createQueryBuilder();
        $queryBuilder->select('pcbc')
            ->from('Commons\\ORA\\ProjectClassifications', 'pcbc')
            ->andWhere($queryBuilder->expr()->eq('pcbc.projectName', '?1'), $queryBuilder->expr()->eq('pcbc.classCategory', '?2'))
            ->setParameter(1, $data['projectName'])
            ->setParameter(2, 'BUILDING CLASSIFICATION');

        $pcbt = $queryBuilder->getQuery()->getSingleResult();

        $data['buildingClassification'] = $pcbt->getClassCode();


        $this->startEditSequence(null, $data);
    }

    public function customEditCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence($country, $data);
    }


    private function startEditSequence($country, $data){

        try{
            if (!isset($data['buildingType'])){
                $queryBuilder = $this->em->createQueryBuilder();
                $queryBuilder->select('pcbt')
                    ->from('Commons\\ORA\\ProjectClassifications', 'pcbt')
                    ->andWhere($queryBuilder->expr()->eq('pcbt.projectName', '?1'), $queryBuilder->expr()->eq('pcbt.classCategory', '?2'))
                    ->setParameter(1, $data['projectName'])
                    ->setParameter(2, 'BUILDING TYPE');

                $pcbt = $queryBuilder->getQuery()->getSingleResult();

                $data['buildingType'] = $pcbt->getClassCode();
            }

            if (!isset($data['buildingClassification'])) {
                $queryBuilder = $this->em->createQueryBuilder();
                $queryBuilder->select('pcbc')
                    ->from('Commons\\ORA\\ProjectClassifications', 'pcbc')
                    ->andWhere($queryBuilder->expr()->eq('pcbc.projectName', '?1'), $queryBuilder->expr()->eq('pcbc.classCategory', '?2'))
                    ->setParameter(1, $data['projectName'])
                    ->setParameter(2, 'BUILDING CLASSIFICATION');

                $pcbt = $queryBuilder->getQuery()->getSingleResult();
                $data['buildingClassification'] = $pcbt->getClassCode();
            }

            // checks for customer reference
            //
            $queryBuilder = $this->em->createQueryBuilder();
            $queryBuilder->select('customer')
                ->from('Commons\\ORA\\CustomerHeader', 'customer')
                ->andWhere($queryBuilder->expr()->eq('customer.origSystemCustomerRef', '?1'))
                ->setParameter(1, $data['customerNumber']);

            try{
                $customer = $queryBuilder->getQuery()->getSingleResult(Query::HYDRATE_ARRAY);
                $data['customer'] = $customer;

            }catch(\Exception $e){
                $data['errorCustomer'] = 'customer '.$data['customerNumber'].'NOT_FOUND';  // what to do?
            }

            // ProjectTasks children
            $data['tasks'] = $this->getChildQuery(self::CHILD2_CLASS_NAME,
                self::CHILD2_KEY_COLUMNS,
                array($data['projectName']))->getResult(Query::HYDRATE_ARRAY);

            $actionCustomByCountry = self::ACTION_CUSTOM . $country.'/'. $data['id'];

            $data = $this->loadCommonData(
                $data,
                'Edit cleansed Project Header #' . $data['id'],
                $actionCustomByCountry,
                self::ACTION_LABEL,
                self::DISCARD . $data['id'],
                self::CANCEL . $data['country']
            );

            $data['Locales'] = $this->loadOrganizationName($country);

            $data['NITemplateName'] = 'DM NI TEMPLATE ' . ($country);
            $data['MODTemplateName'] = 'DM MOD TEMPLATE ' . ($country);

            $this->displayStandardLayout(self::VIEW_LAYOUTS[$country]['frm'], $data);

        }catch(\Doctrine\ORM\NonUniqueResultException $e){

            $data = $this->loadCommonData(
                $data,
                'Consistency problems '.$e->getMessage(),
                '',
                self::ACTION_LABEL,
                '',
                self::CANCEL . $data['country']
            );


            $data['message']['PT'] = 'problemas com este registro, copie e cole os comandos na caixa azul, envie por email para limpeza manual diretamente no banco';
            $data['message']['EN'] = 'consistency problems with this record, copy and paste the commands in the blue box, send it by email for manual database cleansing';

            $queryBuilder->select('h.id, h.projectName')
                ->from(self::VO_PROJECT_HEADER, 'h')
                ->andWhere($queryBuilder->expr()->eq('h.projectName', '?1'))
                ->distinct()
                ->setParameter(1, $data['projectName']);

            $data['headers'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);

            $queryBuilder->select('t.id, t.projectName')
                ->from(self::CHILD2_CLASS_NAME, 't')
                ->andWhere($queryBuilder->expr()->eq('t.projectName', '?1'))
                ->distinct()
                ->setParameter(1, $data['projectName']);

            $data['tasks'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);


            $queryBuilder->select('c.id, c.projectName')
                ->from(self::CHILD1_CLASS_NAME, 'c')
                ->andWhere($queryBuilder->expr()->eq('c.projectName', '?1'))
                ->distinct()
                ->setParameter(1, $data['projectName']);

            $data['classifications'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);

            $queryBuilder->select('rr.id, rr.projectName')
                ->from(self::CHILD3_CLASS_NAME, 'rr')
                ->andWhere($queryBuilder->expr()->eq('rr.projectName', '?1'))
                ->distinct()
                ->setParameter(1, $data['projectName']);

            $data['retrules'] = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);


            $this->displayStandardLayout(self::LAYOUT_ERROR, $data);

        }

    }

    /**
     * @param $id
     *
     * updates cleansed ProjectHeader with ID (formBase)
     *
     * updateCleansed is paired with editCleansed
     *
     */
    public function updateCleansed($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->loadValidationRules(self::VO_CLASS_NAME, $id, $this->form_validation);

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence(null, $data);

        }else{
            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);
            $this->em->flush();

            $this->validateChildren( self::CHILD1_CLASS_NAME, self::CHILD1_KEY_COLUMNS, array($vo->getProjectName()) );
            $this->validateChildren( self::CHILD2_CLASS_NAME, self::CHILD2_KEY_COLUMNS, array($vo->getProjectName()) );
            $this->validateChildren( self::CHILD3_CLASS_NAME, self::CHILD2_KEY_COLUMNS, array($vo->getProjectName()) );

            $vo->validateCrossRef($this->em);
            $this->em->flush();

            $this->session->selCol = "lastUpdated";
            $this->session->selAsc = false;

            redirect(base_url() . self::CANCEL . $vo->getCountry());
        }
    }

    public function customUpdateCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        switch($country){
            case 'PA':
            case 'CO':
            case 'MX':
            case 'NI':
            case 'GT':
            case 'HN':
            case 'CR':
            case 'CL':
            case 'AR':
            case 'PE':
            case 'UY':
            case 'PY':

            case 'SV':
                $this->form_validation->set_rules('templateName', 'templateName', 'required|max_length[30]');
                $this->form_validation->set_rules('buildingType', 'buildingType', 'max_length[30]');
                $this->form_validation->set_rules('buildingClassification', 'buildingClassification', 'required|max_length[30]');
                $this->form_validation->set_rules('organizationCode', 'organizationCode', 'required|max_length[30]');
                $this->form_validation->set_rules('aliases', 'aliases', 'max_length[3000]');
                $this->form_validation->set_rules('observations', 'observations', 'max_length[3000]');

                break;

            default:
                show_error('country '. $country . ' still not configured');
                return;

        }

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $data['buildingType']= $this->input->post('buildingType');
            $data['errorBuildingType'] = str_replace("</p>","", str_replace("<p>", "", $this->form_validation->error('buildingType') ));

            $data['buildingClassification']= $this->input->post('buildingClassification');
            $data['errorBuildingClassification'] = str_replace("</p>","", str_replace("<p>", "", $this->form_validation->error('buildingClassification') ));

            $this->startEditSequence($country, $data);

        }else{
            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);
            $this->em->flush();

            $qb = $this->em->createQueryBuilder();
            $qb->select('pcbc')
                ->from('Commons\\ORA\\ProjectClassifications', 'pcbc')
                ->andWhere($qb->expr()->eq('pcbc.projectName', '?1'), $qb->expr()->eq('pcbc.classCategory', '?2'))
                ->setParameter(1, $vo->getProjectName())
                ->setParameter(2, 'BUILDING CLASSIFICATION');
            $this->em->flush();

            $pcbt = $qb->getQuery()->getSingleResult();
            $pcbt->setClassCode($this->input->post('buildingClassification'));
            $this->em->flush();

            $qb = $this->em->createQueryBuilder();
            $qb->select('pcbt')
                ->from('Commons\\ORA\\ProjectClassifications', 'pcbt')
                ->andWhere($qb->expr()->eq('pcbt.projectName', '?1'), $qb->expr()->eq('pcbt.classCategory', '?2'))
                ->setParameter(1, $vo->getProjectName())
                ->setParameter(2, 'BUILDING TYPE');
            $this->em->flush();

            $pcbt = $qb->getQuery()->getSingleResult();
            $pcbt->setClassCode($this->input->post('buildingType'));
            $this->em->merge($pcbt);

            $this->validateChildren( self::CHILD1_CLASS_NAME, self::CHILD1_KEY_COLUMNS, array($vo->getProjectName()) );
            $this->validateChildren( self::CHILD2_CLASS_NAME, self::CHILD2_KEY_COLUMNS, array($vo->getProjectName()) );
            $this->validateChildren( self::CHILD3_CLASS_NAME, self::CHILD2_KEY_COLUMNS, array($vo->getProjectName()) );

            $vo->validateCrossRef($this->em);
            $this->em->flush();


            $this->auditor->log($_SERVER['REMOTE_ADDR'], "updating ProjectHeader ".$id." - ".$vo->getProjectName(), null, $vo->getCountry());
            $this->em->flush();

            $this->session->selGroup='Projects';
            $this->session->selCountry=strtolower($country);
            $this->session->selCol = "lastUpdated";
            $this->session->selAsc = false;

            redirect(base_url() . 'Projects/cleansed/' . $vo->getCountry());
        }
    }
    private function loadOrganizationName($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLLocales', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->orderBy('vo.localOracle')
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

}
