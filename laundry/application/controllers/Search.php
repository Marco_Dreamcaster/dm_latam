<?php
include_once APPPATH . 'controllers/MY_Controller.php';
Use Doctrine\ORM\Query;
Use Commons\AuditLog;

class Search extends MY_Controller {

    private $groupSearch;
    private $countrySearch;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('keywords', 'keywords', 'required|min_length[5]');

        if ($this->form_validation->run() === FALSE) {
            $data['title'] = 'DM LATAM Laundry Search';
            $data = $this->loadSessionNavigation($data);

            $this->groupSearch = $this->input->post('groupSearch');
            $this->countrySearch = $this->input->post('country_code');
            $data['keywords'] = $this->input->post('keywords');
            $data['groupSearch'] = $this->groupSearch;
            $data['countrySearch'] = strtolower($this->countrySearch);

            $data['errorKeywords'] = str_replace('</p>','', str_replace('<p>','', $this->form_validation->error('keywords')));

            $this->load->view('templates/header2', $data);
            $this->smarty->view('results.tpl', $data, true );
            $this->load->view('templates/footer2', $data);

        }else{
            $data['title'] = 'DM LATAM Laundry Search';
            $data = $this->loadSessionNavigation($data);

            $this->groupSearch = $this->input->post('groupSearch');
            $this->countrySearch = $this->input->post('country_code');
            $this->loadDefaultParamsFromSession();
            $data['keywords'] = $this->input->post('keywords');
            $data['groupSearch'] = $this->groupSearch;
            $data['countrySearch'] = strtolower($this->countrySearch);

            $data['results'] = array();

            $countrySet = ['PA', 'CO', 'MX', 'SV', 'GT', 'HN', 'CR', 'NI', 'CL', 'PE', 'AR', 'PY', 'UY'];

            foreach($countrySet as $c){
                $targetResults = $this->searchCustomerHeader($this->input->post('keywords'), $c);
                foreach($targetResults as $rawResult){
                    array_push($data['results'], array( 'country'=>$rawResult->getCountry(),
                                                        'type'=>'STG CustomerHeader',
                                                        'detail'=>$rawResult->getOrigSystemCustomerRef().' - '.$rawResult->getCustomerName(),
                                                        'actionUrl'=>'Customers/customEditCleansed/'.$c.'/'.$rawResult->getId() ));

                }

                $targetResults = $this->searchPreCustomerHeader($this->input->post('keywords'), $c);
                foreach($targetResults as $rawResult){
                    array_push($data['results'], array( 'country'=>$rawResult->getCountry(),
                                                        'type'=>'pre-STG CustomerHeader',
                                                        'detail'=>$rawResult->getOrigSystemCustomerRef().' - '.$rawResult->getCustomerName(),
                                                        'actionUrl'=>'Customers/editDirty/'.$rawResult->getId() ));
                }

                $targetResults = $this->searchVendorHeader($this->input->post('keywords'), $c);
                foreach($targetResults as $rawResult){
                    array_push($data['results'], array( 'country'=>$rawResult->getCountry(),
                                                        'type'=>'STG VendorHeader',
                                                        'detail'=>$rawResult->getSegment1().' - '.$rawResult->getVendorName(),
                                                        'actionUrl'=>'Vendors/customEditCleansed/'.$c.'/'.$rawResult->getId() ));

                }

                $targetResults = $this->searchPreVendorHeader($this->input->post('keywords'), $c);
                foreach($targetResults as $rawResult){
                    array_push($data['results'], array( 'country'=>$rawResult->getCountry(),
                                                        'type'=>'pre-STG VendorHeader',
                                                        'detail'=>$rawResult->getSegment1().' - '.$rawResult->getVendorName(),
                                                        'actionUrl'=>'Vendors/editDirty/'.$rawResult->getId() ));

                }

                $targetResults = $this->searchProjectHeader($this->input->post('keywords'), $c);
                foreach($targetResults as $rawResult){
                    if (strpos($rawResult->getTemplateName(), "SERV")!==false){
                        array_push($data['results'], array( 'country'=>$rawResult->getCountry(),
                            'type'=>'STG ServiceHeader',
                            'detail'=>$rawResult->getPmProjectReference().' - '.$rawResult->getProjectName(),
                            'actionUrl'=>'Services/customEditCleansed/'.$c.'/'.$rawResult->getId() ));

                    }elseif (strstr($rawResult->getTemplateName(), "CC QR TEMPLATE")){
                        array_push($data['results'], array( 'country'=>$rawResult->getCountry(),
                            'type'=>'STG RepairHeader',
                            'detail'=>$rawResult->getPmProjectReference().' - '.$rawResult->getProjectName(),
                            'actionUrl'=>'Projects/customEditCleansed/'.$c.'/'.$rawResult->getId() ));
                    }else{
                        array_push($data['results'], array( 'country'=>$rawResult->getCountry(),
                            'type'=>'STG ProjectHeader',
                            'detail'=>$rawResult->getPmProjectReference().' - '.$rawResult->getProjectName(),
                            'actionUrl'=>'Projects/customEditCleansed/'.$c.'/'.$rawResult->getId() ));
                    }


                }

                $targetResults = $this->searchPreProjectHeader($this->input->post('keywords'), $c);
                foreach($targetResults as $rawResult){
                    if (strpos($rawResult->getTemplateName(), "SERV")!==false) {
                        array_push($data['results'], array( 'country'=>$rawResult->getCountry(),
                            'type'=>'pre-STG ServiceHeader',
                            'detail'=>$rawResult->getPmProjectReference().' - '.$rawResult->getProjectName(),
                            'actionUrl'=>'Services/editDirty/'.$rawResult->getId() ));

                    }elseif (strstr($rawResult->getTemplateName(), "CC QR TEMPLATE")){
                        array_push($data['results'], array( 'country'=>$rawResult->getCountry(),
                            'type'=>'pre-STG RepairHeader',
                            'detail'=>$rawResult->getPmProjectReference().' - '.$rawResult->getProjectName(),
                            'actionUrl'=>'Projects/editDirty/'.$c.'/'.$rawResult->getId() ));
                    }else{
                        array_push($data['results'], array( 'country'=>$rawResult->getCountry(),
                            'type'=>'pre-STG ProjectHeader',
                            'detail'=>$rawResult->getPmProjectReference().' - '.$rawResult->getProjectName(),
                            'actionUrl'=>'Projects/editDirty/'.$rawResult->getId() ));


                    }
                }

                $targetResults = $this->searchItemHeader($this->input->post('keywords'), $c);
                foreach($targetResults as $rawResult){
                    array_push($data['results'], array( 'country'=>$rawResult->getCountry(),
                                                        'type'=>'STG ItemHeader',
                                                        'detail'=>$rawResult->getItemNumber().' - '.$rawResult->getDescriptionEsa(),
                                                        'actionUrl'=>'Inventory/customEditCleansed/'.$c.'/'.$rawResult->getId() ));

                }

                $targetResults = $this->searchPreItemHeader($this->input->post('keywords'), $c);
                foreach($targetResults as $rawResult){
                    array_push($data['results'], array( 'country'=>$rawResult->getCountry(),
                                                        'type'=>'pre-STG ItemHeader',
                                                        'detail'=>$rawResult->getItemNumber().' - '.$rawResult->getDescriptionEsa(),
                                                        'actionUrl'=>'Inventory/editDirty/'.$rawResult->getId() ));

                }

                $targetResults = $this->searchItemMfgPartNum($this->input->post('keywords'), $c);
                foreach($targetResults as $rawResult){
                    array_push($data['results'], array( 'country'=>$rawResult->getCountry(),
                        'type'=>'STG ItemHeader (found by partNumber)',
                        'detail'=>$rawResult->getItemNumber().' - '.$rawResult->getDescriptionEsa(),
                        'actionUrl'=>'Inventory/customEditCleansed/'.$c.'/'.$rawResult->getId() ));

                }



            }

            $this->load->view('templates/header', $data);
            $this->smarty->view('results.tpl', $data, true );
            $this->load->view('templates/footer', $data);

        }

    }

    private function searchCustomerHeader($keyword, $country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('target')
            ->from('Commons\ORA\CustomerHeader', 'target')
            ->andWhere(
                $qb->expr()->eq('target.country', '?1'),
                $qb->expr()->orX(
                    $qb->expr()->like('target.customerName', '?2'),
                    $qb->expr()->like('target.origSystemCustomerRef', '?3')
                )
            )
            ->setParameter(1, $country)
            ->setParameter(2, '%'.$keyword.'%')
            ->setParameter(3, '%'.$keyword.'%')
            ->addOrderBy('target.recommended',' DESC')
            ->addOrderBy('target.customerName','ASC');;

        $targetResults = $qb->getQuery()->getResult();

        return $targetResults;
    }

    private function searchPreCustomerHeader($keyword, $country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('target')
            ->from('Commons\RAW\PreCustomerHeader', 'target')
            ->andWhere(
                $qb->expr()->eq('target.country', '?1'),
                $qb->expr()->orX(
                    $qb->expr()->like('target.customerName', '?2'),
                    $qb->expr()->like('target.origSystemCustomerRef', '?3')
                )
            )
            ->setParameter(1, $country)
            ->setParameter(2, '%'.$keyword.'%')
            ->setParameter(3, '%'.$keyword.'%')
            ->addOrderBy('target.recommended',' DESC')
            ->addOrderBy('target.customerName','ASC');;

        $targetResults = $qb->getQuery()->getResult();

        return $targetResults;
    }

    private function searchVendorHeader($keyword, $country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('target')
            ->from('Commons\ORA\VendorHeader', 'target')
            ->andWhere(
                $qb->expr()->eq('target.country', '?1'),
                $qb->expr()->orX(
                    $qb->expr()->like('target.vendorName', '?2'),
                    $qb->expr()->like('target.segment1', '?3')
                )
            )
            ->setParameter(1, $country)
            ->setParameter(2, '%'.$keyword.'%')
            ->setParameter(3, '%'.$keyword.'%')
            ->addOrderBy('target.recommended',' DESC')
            ->addOrderBy('target.segment1','ASC');;

        $targetResults = $qb->getQuery()->getResult();

        return $targetResults;
    }

    private function searchPreVendorHeader($keyword, $country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('target')
            ->from('Commons\RAW\PreVendorHeader', 'target')
            ->andWhere(
                $qb->expr()->eq('target.country', '?1'),
                $qb->expr()->orX(
                    $qb->expr()->like('target.vendorName', '?2'),
                    $qb->expr()->like('target.segment1', '?3')
                )
            )
            ->setParameter(1, $country)
            ->setParameter(2, '%'.$keyword.'%')
            ->setParameter(3, '%'.$keyword.'%')
            ->addOrderBy('target.recommended',' DESC')
            ->addOrderBy('target.segment1','ASC');;

        $targetResults = $qb->getQuery()->getResult();

        return $targetResults;
    }

    private function searchProjectHeader($keyword, $country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('target')
            ->from('Commons\ORA\ProjectHeader', 'target')
            ->andWhere(
                $qb->expr()->eq('target.country', '?1'),
                $qb->expr()->orX(
                    $qb->expr()->like('target.pmProjectReference', '?2'),
                    $qb->expr()->like('target.projectName', '?3')
                )
            )
            ->setParameter(1, $country)
            ->setParameter(2, '%'.$keyword.'%')
            ->setParameter(3, '%'.$keyword.'%')
            ->addOrderBy('target.recommended',' DESC')
            ->addOrderBy('target.projectName','ASC');;

        $targetResults = $qb->getQuery()->getResult();

        return $targetResults;
    }

    private function searchPreProjectHeader($keyword, $country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('target')
            ->from('Commons\RAW\PreProjectHeader', 'target')
            ->andWhere(
                $qb->expr()->eq('target.country', '?1'),
                $qb->expr()->orX(
                    $qb->expr()->like('target.pmProjectReference', '?2'),
                    $qb->expr()->like('target.projectName', '?3')
                )
            )
            ->setParameter(1, $country)
            ->setParameter(2, '%'.$keyword.'%')
            ->setParameter(3, '%'.$keyword.'%')
            ->addOrderBy('target.recommended',' DESC')
            ->addOrderBy('target.projectName','ASC');;

        $targetResults = $qb->getQuery()->getResult();

        return $targetResults;
    }

    private function searchItemMfgPartNum($keyword, $country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('target')
            ->from('Commons\\ORA\\ItemMFGPartNum', 'target')
            ->andWhere(
                $qb->expr()->eq('target.country', '?1'),
                $qb->expr()->orX(
                    $qb->expr()->like('target.partNumber', '?2')
                )
            )
            ->setParameter(1, $country)
            ->setParameter(2, '%'.$keyword.'%')
            ->addOrderBy('target.recommended',' DESC')
            ->addOrderBy('target.itemNumber','ASC');;

        $targetResults = $qb->getQuery()->getResult();

        $parentItemResults = array();

        foreach ($targetResults as $result){
            $qb = $this->em->createQueryBuilder();
            $qb->select('target')
                ->from('Commons\\ORA\\ItemHeader', 'target')
                ->andWhere(
                    $qb->expr()->eq('target.country', '?1'),
                    $qb->expr()->orX(
                        $qb->expr()->eq('target.itemNumber', '?2')
                    )
                )
                ->setParameter(1, $country)
                ->setParameter(2, $result->getItemNumber())
                ->addOrderBy('target.recommended',' DESC')
                ->addOrderBy('target.itemNumber','ASC');;

            $itemResults = $qb->getQuery()->getResult();

            foreach($itemResults as $item){
                array_push($parentItemResults,$item);
            }


        }

        return $parentItemResults;



    }

    private function searchItemHeader($keyword, $country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('target')
            ->from('Commons\ORA\ItemHeader', 'target')
            ->andWhere(
                $qb->expr()->eq('target.country', '?1'),
                $qb->expr()->orX(
                    $qb->expr()->like('target.origItemNumber', '?2'),
                    $qb->expr()->like('target.itemNumber', '?3'),
                    $qb->expr()->like('target.description', '?4'),
                    $qb->expr()->like('target.descriptionEsa', '?5'),
                    $qb->expr()->like('target.observations', '?6')
                )
            )
            ->setParameter(1, $country)
            ->setParameter(2, '%'.$keyword.'%')
            ->setParameter(3, '%'.$keyword.'%')
            ->setParameter(4, '%'.$keyword.'%')
            ->setParameter(5, '%'.$keyword.'%')
            ->setParameter(6, '%'.$keyword.'%')
            ->addOrderBy('target.recommended',' DESC')
            ->addOrderBy('target.itemNumber','ASC');;

        $targetResults = $qb->getQuery()->getResult();

        return $targetResults;
    }

    private function searchPreItemHeader($keyword, $country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('target')
            ->from('Commons\RAW\PreItemHeader', 'target')
            ->andWhere(
                $qb->expr()->eq('target.country', '?1'),
                $qb->expr()->orX(
                    $qb->expr()->like('target.origItemNumber', '?2'),
                    $qb->expr()->like('target.itemNumber', '?3'),
                    $qb->expr()->like('target.description', '?4'),
                    $qb->expr()->like('target.descriptionEsa', '?5'),
                    $qb->expr()->like('target.observations', '?6')
                )
            )
            ->setParameter(1, $country)
            ->setParameter(2, '%'.$keyword.'%')
            ->setParameter(3, '%'.$keyword.'%')
            ->setParameter(4, '%'.$keyword.'%')
            ->setParameter(5, '%'.$keyword.'%')
            ->setParameter(6, '%'.$keyword.'%')
            ->addOrderBy('target.recommended',' DESC')
            ->addOrderBy('target.itemNumber','ASC');;

        $targetResults = $qb->getQuery()->getResult();

        return $targetResults;
    }


    private function loadDefaultParamsFromSession(){
        if ($this->session->sessionCountry==null){
            $this->session->set_userdata(array('sessionCountry' => 'PA'));
        }

        if ($this->session->sessionGroup==null){
            $this->session->set_userdata(array('sessionGroup' => 'Projects'));
        }

        if ($this->groupSearch==null){
            $this->groupSearch = $this->session->sessionGroup;
        }

        if ($this->countrySearch==null){
            $this->countrySearch = $this->session->sessionCountry;
        }

    }


}