<?php
include_once APPPATH . 'controllers/MY_Controller.php';

Use Doctrine\ORM\Query;

class Setup extends MY_Controller
{

    const LAYOUT_MENU = array('GL/GLControlCenter.tpl');
    const LAYOUT_PARAMS = array('GL/GLParams.tpl');
    const LAYOUT_TABLES = array('GL/GLTables.tpl');
    const LAYOUT_MAIN = array('Setup/Setup.tpl');
    const LAYOUT_DIAG = array('GL/GLDiag.tpl');

    public function __construct()
    {
        parent::__construct();

        $this->load->database();

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');


    }

    public function index($country){
        if ($this->session->user8id==null){
            $this->redirectWhenNotIdentified($country);

        }else {
            $this->session->selGroup = 'Setup';
            $this->session->selCountry = $country;

            $data = array();

            $data['country'] = $country;
            $data = $this->loadCommonData($data, ' Migration Setup', '', '', '', '');

            $this->startEditSequenceMain($country, $data);
        }
    }

    public function indexMenu($country='CO')
    {

        if ($this->session->user8id==null){
            $this->redirectWhenNotIdentified($country);

        }else{
            $this->session->selGroup='GLMenu';
            $this->session->selCountry = $country;

            $data = array();

            $data['country'] = $country;
            $data = $this->loadCommonData($data,' GL Control Center', '', '', '' , '');

            $data['snapshotDate'] = $this->loadSnapshotDate($country);

            /**
//            $query = $this->db->query("EXEC dbo.udf_getSnapshotDate '".$country."'");
//            $resultArray = $query->result_array();

//            $data['snapshotDate'] = $resultArray[0]['FECHA'];



            if (sizeof($resultArray)){
                $data['parameterChangeDate'] = $resultArray[0]['STAMP'];
                $data['parameterChangeEvent'] = $resultArray[0]['EVENT'];
                $data['parameterChangeResponsible'] = $resultArray[0]['USER_ID'];

            }else{
                $data['parameterChangeDate'] = 'never';
                $data['parameterChangeEvent'] = 'none';
                $data['parameterChangeResponsible'] = 'no one';

            }
**/

            $this->displayStandardLayout(self::LAYOUT_MENU, $data);

        }
    }


    public function indexParams($country='CO', $topActive=null, $bottomActive=null)
	{
	    if ($this->session->user8id==null){
            $this->redirectWhenNotIdentified($country);

        }else{
            $this->session->selGroup='GLMenu';
            $this->session->selCountry = $country;

            $data = array();

            if (isset($this->session->topActive)){
                $data['topActive'] = $this->session->topActive;
            }else{
                $data['topActive'] = 'params';
            }

            if (isset($this->session->bottomActive)){
                $data['bottomActive'] = $this->session->bottomActive;
            }else{
                $data['bottomActive'] = 'CC';
            }

            $this->startEditSequenceParams($country, $data);


        }
	}

    public function indexTables($country='CO', $topActive=null, $bottomActive=null)
    {
        if ($this->session->user8id==null){
            $this->redirectWhenNotIdentified($country);

        }else{
            $this->session->selGroup='GLMenu';
            $this->session->selCountry = $country;

            $data = array();

            if (($topActive=='tablas')&&($bottomActive=='Locales')){
                $data['topActive'] = 'tablas';
                $data['bottomActive'] = 'Locales';
                $data['country'] = $country;
                $data = $this->loadCommonData($data,$country.' GL Locales', '', '', '' , '');

                $data['Locales'] = $this->loadGLLocales($country);

                $this->displayStandardLayout(self::LAYOUT_TABLES, $data);

            }else{
                if (isset($this->session->topActive)){
                    $data['topActive'] = $this->session->topActive;
                }else{
                    $data['topActive'] = 'tablas';
                }

                if (isset($this->session->bottomActive)){
                    $data['bottomActive'] = $this->session->bottomActive;
                }else{
                    $data['bottomActive'] = 'Cuentas';
                }

                $this->startEditSequenceTables($country, $data);
            }
        }
    }

    public function indexDiag($country='CO')
    {
        if ($this->session->user8id==null){
            $this->redirectWhenNotIdentified($country);

        }else{
            $this->session->selGroup='GLMenu';
            $this->session->selCountry = $country;

            $data = array();

            if (isset($this->session->topActive)){
                $data['topActive'] = $this->session->topActive;
            }else{
                $data['topActive'] = 'diagnostics';
            }

            if (isset($this->session->bottomActive)){
                $data['bottomActive'] = $this->session->bottomActive;
            }else{
                $data['bottomActive'] = 'RAD';
            }

            $data['desde'] = '01-01-2000';
            $data['hasta'] = getdate();

            $this->startEditSequenceDiag($country, $data);

        }

    }


    private function redirectWhenNotIdentified($country){
        $data['title'] = 'Credentials for GL';
        $data = $this->loadSessionNavigation($data);
        $data['redirectTo'] = "Setup/".$country;

        $this->smarty->view('Admin/login.tpl', $data, true);

    }

    private function startEditSequenceMain($country, $data){
        $data['country'] = $country;
        $data = $this->loadCommonData($data,$country.' Migration Setup', '', '', '' , '');

        $data['ConfigEstandar'] =$this->loadGLConfigEstandar($country);
        $data['Locales'] = $this->loadGLLocales($country);

        $this->displayStandardLayout(self::LAYOUT_MAIN, $data);

    }


	private function startEditSequenceParams($country, $data){
        $data['country'] = $country;
        $data = $this->loadCommonData($data,$country.' GL Parameters', '', '', '' , '');

        $data['CCs'] = $this->loadGLCostCenters($country);
        $data['CCFixed'] = $this->loadGLCostCenterFixed($country);
        $data['Exclusion'] = $this->loadGLExclusionCuentas($country);
        $data['PLDePara'] = $this->loadGLPLDePara($country);
        $data['PL'] = $this->loadGLPL($country);
        $data['PLFixed'] = $this->loadGLPLFixed($country);
        $data['IC'] = $this->loadGLIC($country);
        $data['LOB'] = $this->loadGLLOB($country);
        $data['CR'] = $this->loadGLCheckRules($country);
        $data['LOBFixed'] = $this->loadGLLOBFixed($country);

        $this->displayStandardLayout(self::LAYOUT_PARAMS, $data);

    }

    private function startEditSequenceTables($country, $data){
        $data['country'] = $country;
        $data = $this->loadCommonData($data,$country.' GL Table Mappings', '', '', '' , '');

        $data['Cuentas'] = $this->loadGLCuentas($country);
        $data['TipCos'] = $this->loadGLTipCos($country);
        $data['TipoCont'] = $this->loadGLTipoCont($country);
        $data['Diarios'] = $this->loadGLDiarios($country);
        $data['CostoLocal'] = $this->loadGLCostoLocal($country);
        $data['Monedas'] = $this->loadGLMonedas($country);

        $this->displayStandardLayout(self::LAYOUT_TABLES, $data);

    }

    private function startEditSequenceDiag($country, $data){
        $data['country'] = $country;
        $data = $this->loadCommonData($data,$country.' GL Diagnostics', '', '', '' , '');

        $data['MS'] = $this->loadMS($country);
        $data['DiagnosticsB'] = $this->loadDiagnosticsB($country);
        $data['DiagnosticsR'] = $this->loadDiagnosticsR($country);

        $this->displayStandardLayout(self::LAYOUT_DIAG, $data);
    }

    private function loadGLCuentas($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLCuentas', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function updateGLCuentas($country){
        if ($this->session->clearance<1){
            show_error("You're not allowed to change values on this area.  Please talk to Rodrigo!");
        }


        $this->form_validation->set_rules('id', 'id', 'required');

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['topActive'] = 'tablas';
            $data['bottomActive'] = 'Cuentas';

            $data['errorCuentas'] = str_replace("</p>","", str_replace("<p>", "","No items selected" ));

            $this->session->selGroup= 'Setup';
            $this->session->selCountry = $country;

            $data['country'] = $country;
            $data = $this->loadCommonData($data,$country.' Laundry GL Params', '', '', '' , '');

            $this->startEditSequenceTables($country,$data);

        } else {

            $qb = $this->em->createQueryBuilder();
            $qb->select('vo')
                ->from('Commons\\MAPS\\GLCuentas', 'vo')
                ->where($qb->expr()->eq('vo.id', '?1'))
                ->setParameter(1, $this->input->post('id'));

            $vo = $qb->getQuery()->getSingleResult();
            $vo->setCtaOracle($this->input->post('ctaOracle'));

            $this->em->merge($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "update GLCuentas " . $vo->getCta() . " - " . $vo->getDescrip() ." - ". $vo->getCtaOracle(), null, $vo->getCountry(), 'GLP_CHANGE');

            $this->session->topActive = 'tablas';
            $this->session->bottomActive = 'Cuentas';

            redirect(base_url() . "GLTables/" . $country);
        }

    }

    private function loadGLLocales($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLLocales', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function clearGLCuentas($id, $country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLCuentas', 'vo')
            ->where($qb->expr()->eq('vo.id', '?1'))
            ->setParameter(1, $id);

        $vo = $qb->getQuery()->getSingleResult();
        $vo->setCtaOracle(null);

        $this->em->merge($vo);
        $this->em->flush();

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "update GLCuentas " . $vo->getCta() . " - " . $vo->getDescrip() ." - ". $vo->getCtaOracle(), null, $vo->getCountry(), 'GLP_CHANGE');

        $this->session->topActive = 'tablas';
        $this->session->bottomActive = 'Cuentas';

        redirect(base_url() . "GLTables/" . $country);

    }

    public function updateGLLocales($country){
        if ($this->session->user8id==null) {
            $this->redirectWhenNotIdentified($country);

        }

        $this->form_validation->set_rules('id', 'id', 'required');

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['topActive'] = 'tablas';
            $data['bottomActive'] = 'Locales';

            $data['errorLocales'] = str_replace("</p>","", str_replace("<p>", "","No items selected" ));

            $this->session->selGroup= 'Setup';
            $this->session->selCountry = $country;

            $data['country'] = $country;
            $data = $this->loadCommonData($data,$country.' Updated Locales', '', '', '' , '');

            $data['Locales'] = $this->loadGLLocales($country);

            $this->displayStandardLayout(self::LAYOUT_TABLES, $data);

        } else {

            $qb = $this->em->createQueryBuilder();
            $qb->select('vo')
                ->from('Commons\\MAPS\\GLLocales', 'vo')
                ->where($qb->expr()->eq('vo.id', '?1'))
                ->setParameter(1, $this->input->post('id'));

            $vo = $qb->getQuery()->getSingleResult();
            $vo->setlocalOracle($this->input->post('localOracle'));
            $vo->setIdAlmacen($this->input->post('idAlmacen'));
            $vo->setSubInventoryOrgCode($this->input->post('subInventoryOrgCode'));
            $vo->setSubInventoryName($this->input->post('subInventoryName'));

            $qb = $this->em->createQueryBuilder();

            $qb->select('c')
                ->from('Commons\\MAPS\\GLConfigEstandar', 'c')
                ->where($qb->expr()->eq('c.country', '?1'))
                ->setParameter(1, $vo->getCountry());

            $config = $qb->getQuery()->getSingleResult();

            $vo->setSubInventoryDistributionAccount($vo->getLocalOracle().'.'.$config->getInventoryDistAccountPrefix());

            $this->em->merge($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "update GLLocales " . $vo->getlocal() . " - " . $vo->getDescrip() ." - ". $vo->getlocalOracle(), null, $vo->getCountry(), 'GLP_CHANGE');

            redirect(base_url() . "Setup/" . $country);
        }

    }

    private function loadGLTipCos($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLTipCos', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function clearGLLocales($id, $country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLLocales', 'vo')
            ->where($qb->expr()->eq('vo.id', '?1'))
            ->setParameter(1, $id);

        $vo = $qb->getQuery()->getSingleResult();
        $vo->setlocalOracle(null);

        $this->em->merge($vo);
        $this->em->flush();

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "update GLLocales " . $vo->getlocal() . " - " . $vo->getDescrip() ." - ". $vo->getlocalOracle(), null, $vo->getCountry(), 'GLP_CHANGE');

        $this->session->topActive = 'tablas';
        $this->session->bottomActive = 'Locales';

        redirect(base_url() . "GLTables/" . $country);

    }

    public function updateGLTipCos($country)
    {
        $this->form_validation->set_rules('id', 'id', 'required');

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['topActive'] = 'tablas';
            $data['bottomActive'] = 'TipCos';

            $data['errorTipCos'] = str_replace("</p>", "", str_replace("<p>", "",  "No items selected"));

            $this->session->selGroup = 'GLMenu';
            $this->session->selCountry = $country;

            $data['country'] = $country;
            $data = $this->loadCommonData($data, $country . ' Laundry GL Tables', '', '', '', '');

            $this->startEditSequenceTables($country, $data);

        } else {

            $qb = $this->em->createQueryBuilder();
            $qb->select('vo')
                ->from('Commons\\MAPS\\GLTipCos', 'vo')
                ->where($qb->expr()->eq('vo.id', '?1'))
                ->setParameter(1, $this->input->post('id'));

            $vo = $qb->getQuery()->getSingleResult();
            $vo->settipcosOracle($this->input->post('tipcosOracle'));
            $vo->setexpenditure($this->input->post('expenditure'));
            $vo->setclasificacion($this->input->post('clasificacion'));

            $this->em->merge($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "update GLTipCos " . $vo->gettipcos() . " - " . $vo->getDescrip() . " - " . $vo->gettipcosOracle(), null, $vo->getCountry(), 'GLP_CHANGE');

            $this->session->topActive = 'tablas';
            $this->session->bottomActive = 'TipCos';

            redirect(base_url() . "GLTables/" . $country);
        }
    }

    public function clearGLTipCos($id, $country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLTipCos', 'vo')
            ->where($qb->expr()->eq('vo.id', '?1'))
            ->setParameter(1, $id);

        $vo = $qb->getQuery()->getSingleResult();
        $vo->settipcosOracle(null);

        $this->em->merge($vo);
        $this->em->flush();

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "update GLTipCos " . $vo->gettipcos() . " - " . $vo->getDescrip() . " - " . $vo->gettipcosOracle(), null, $vo->getCountry(), 'GLP_CHANGE');

        $this->session->topActive = 'tablas';
        $this->session->bottomActive = 'TipCos';

        redirect(base_url() . "GLTables/" . $country);

    }

    private function loadGLTipoCont($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLTipoCont', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function updateGLTipoCont($country)
    {
        $this->form_validation->set_rules('id', 'id', 'required');

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['topActive'] = 'tablas';
            $data['bottomActive'] = 'TipoCont';

            $data['errorTipoCont'] = str_replace("</p>", "", str_replace("<p>", "", "No items selected"));

            $this->session->selGroup = 'GLMenu';
            $this->session->selCountry = $country;

            $data['country'] = $country;
            $data = $this->loadCommonData($data, $country . ' Laundry GL Tables', '', '', '', '');

            $this->startEditSequenceTables($country, $data);

        } else {

            $qb = $this->em->createQueryBuilder();
            $qb->select('vo')
                ->from('Commons\\MAPS\\GLTipoCont', 'vo')
                ->where($qb->expr()->eq('vo.id', '?1'))
                ->setParameter(1, $this->input->post('id'));

            $vo = $qb->getQuery()->getSingleResult();
            $vo->settipoOracle($this->input->post('tipoOracle'));

            $this->em->merge($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "update GLTipoCont " . $vo->gettipo() . " - " . $vo->getDescrip() . " - " . $vo->gettipoOracle(), null, $vo->getCountry(), 'GLP_CHANGE');

            $this->session->topActive = 'tablas';
            $this->session->bottomActive = 'TipoCont';

            redirect(base_url() . "GLTables/" . $country);
        }
    }

    public function updateGLConfigEstandar($country)
    {
        if ($this->session->user8id==null) {
            $this->redirectWhenNotIdentified($country);

        }

        $this->form_validation->set_rules('id', 'id', 'required');

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data['ConfigEstandar'][$key] = $value;
            }

            $data['topActive'] = 'params';
            $data['bottomActive'] = 'ConfigEstandar';

            $data['errorBranch'] = str_replace("</p>", "", str_replace("<p>", "", $this->form_validation->error('branch')));

            $this->session->selGroup = 'GLMenu';
            $this->session->selCountry = $country;

            $data['country'] = $country;
            $data = $this->loadCommonData($data, $country . ' Laundry GL Tables', '', '', '', '');

            $data['Locales'] = $this->loadGLLocales($country);

            $this->displayStandardLayout(self::LAYOUT_MAIN, $data);

        } else {
            $qb = $this->em->createQueryBuilder();
            $qb->select('vo')
                ->from('Commons\\MAPS\\GLConfigEstandar', 'vo')
                ->where($qb->expr()->eq('vo.id', '?1'))
                ->setParameter(1, $this->input->post('id'));

            $vo = $qb->getQuery()->getSingleResult();

            $vo->setCutoffDate(DateTime::createFromFormat('d-m-Y',$this->input->post('cutoffDate')));
            $vo->setBranch($this->input->post('branch'));
            $vo->setCompany($this->input->post('companyAlt'));
            $vo->setCompanyAlt($this->input->post('companyAlt'));
            $vo->setLocalcurrency($this->input->post('localcurrency'));
            $vo->setRef1($this->input->post('ref1'));
            $vo->setRef2($this->input->post('ref2'));
            $vo->setRef4($this->input->post('ref4'));
            $vo->setRef5($this->input->post('ref5'));
            $vo->setRef10($this->input->post('ref10'));
            $vo->setBAccList(trim($this->input->post('bAccList')));
            $vo->setRAccList(trim($this->input->post('rAccList')));
            $vo->setSmref($this->input->post('Smref'));
            $vo->setPmref($this->input->post('Pmref'));
            $vo->setInventoryDistAccountPrefix($this->input->post('inventoryDistAccountPrefix'));
            $vo->setDefaultLocatorName($this->input->post('defaultLocatorName'));

            $this->em->persist($vo);
            $this->em->flush();

            $this->auditor->log($this->session->user8id, "update GLConfigEstandar " . $vo->getBranch(), null, $vo->getCountry(), 'GLP_CHANGE');

            redirect(base_url() . "Setup/" . $country);
        }
    }

    public function searchAccount($country){

        $sqlCmd = "SELECT TOP 10000 A.CTA As CtaSigla, A.DEBE As Debe, A.HABER As Haber, A.IMPORTE As Importe,
                    A.MONEDA As Moneda, C.DIARIO As Diario, C.DESCRIP as Descrip, B.CONTROL As [Control], 
                    CONVERT(VARCHAR(10), B.FECHA,105)  As Fecha, 
                    CONVERT(VARCHAR(10), B.FECHAREG, 105)  As FechaReg
                  FROM TK%country%_LATEST.dbo.COM A
                  INNER JOIN TK%country%_LATEST.dbo.TITCOM B ON (A.IDCOMP = B.IDCOMP)
                  INNER JOIN TK%country%_LATEST.dbo.DIARIOS C ON (B.DIARIO = C.DIARIO)
                  WHERE A.CTA = ? AND B.FECHA BETWEEN ? AND ?
                  AND A.GLOSA <> 'A N U L A D O'
                  ORDER BY A.CTA";

        $sqlCmd = str_replace('%country%', $country, $sqlCmd);

        $data['ctaSigla'] = $this->input->post('ctaSigla');
        $data['desde'] = $this->input->post('desde');
        $data['hasta'] = $this->input->post('hasta');

        $stmt = $this->em->getConnection()->prepare($sqlCmd);
        $stmt->bindValue(1, $data['ctaSigla']);
        $stmt->bindValue(2, $data['desde']);
        $stmt->bindValue(3, $data['hasta']);

        $stmt->execute();

        $data['AS'] = $stmt->fetchAll();

        $this->auditor->log($this->session->user8id, "search Account ", null, $country, 'ACC_SRC');

        $data['country'] = $country;
        $data = $this->loadCommonData($data,' GL Control Center', '', '', '' , '');

        $data['topActive'] = 'diagnostics';
        $data['bottomActive'] = 'AS';

        $this->displayStandardLayout(self::LAYOUT_DIAG, $data);

    }


    public function clearGLTipoCont($id, $country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLTipoCont', 'vo')
            ->where($qb->expr()->eq('vo.id', '?1'))
            ->setParameter(1, $id);

        $vo = $qb->getQuery()->getSingleResult();
        $vo->settipoOracle(null);

        $this->em->merge($vo);
        $this->em->flush();

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "update GLTipoCont " . $vo->gettipo() . " - " . $vo->getDescrip() . " - " . $vo->gettipoOracle(), null, $vo->getCountry(), 'GLP_CHANGE');

        $this->session->topActive = 'tablas';
        $this->session->bottomActive = 'TipoCont';

        redirect(base_url() . "GLParams/" . $country);

    }

    public function addGLPLDePara($country){

        $this->form_validation->set_rules('codSigla', 'codSigla', 'required|max_length[2]');
        $this->form_validation->set_rules('codOracle', 'codOracle', 'required|max_length[2]');

        if ($this->form_validation->run() === FALSE){
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['errorCodSigla'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('codSigla') ));
            $data['errorCodOracle'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('codOracle') ));

            $data['topActive'] = 'params';
            $data['bottomActive'] = 'PLDePara';

            $this->session->selGroup= 'Setup';
            $this->session->selCountry = $country;

            $this->startEditSequenceParams($country, $data);

        } else {

            $vo = new Commons\MAPS\GLProductLineDePara;
            $vo->setCodSigla($this->input->post('codSigla'));
            $vo->setCodOracle($this->input->post('codOracle'));
            $vo->setCountry($country);

            $this->em->persist($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "adding Product Line De Para " . $vo->getCodSigla() . " - " . $vo->getCodOracle(), null, $vo->getCountry(), 'GLP_CHANGE');

            $this->session->topActive = 'params';
            $this->session->bottomActive = 'PLDePara';

            redirect(base_url() . "GLParams/" . $country);
        }

    }

    public function removeGLPLDePara($id){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLProductLineDePara', 'vo')
            ->where($qb->expr()->eq('vo.id', '?1'))
            ->setParameter(1, $id);

        $vo = $qb->getQuery()->getSingleResult();

        $this->em->remove($vo);
        $this->em->flush();

        $this->session->topActive = 'params';
        $this->session->bottomActive = 'PLDePara';

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "removing Product Line De Para " . $vo->getCodSigla() . " - " . $vo->getCodOracle(), null, $vo->getCountry(), 'GLP_CHANGE');

        redirect(base_url() . "GLParams/" . $vo->getCountry());

    }


    private function loadGLPLDePara($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLProductLineDePara', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function addGLPL($country){

        $this->form_validation->set_rules('ctaOracle', 'ctaOracle', 'required|max_length[8]');
        $this->form_validation->set_rules('desde', 'desde', 'required|max_length[2]');
        $this->form_validation->set_rules('hasta', 'hasta', 'required|max_length[2]');

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['errorCtaOracle'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('ctaOracle') ));
            $data['errorDesde'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('desde') ));
            $data['errorHasta'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('hasta') ));

            $data['topActive'] = 'params';
            $data['bottomActive'] = 'PL';

            $this->session->selGroup= 'Setup';
            $this->session->selCountry = $country;

            $this->startEditSequenceParams($country, $data);

        } else {

            $vo = new Commons\MAPS\GLProductLine;
            $vo->setCtaOracle($this->input->post('ctaOracle'));
            $vo->setDesde($this->input->post('desde'));
            $vo->setHasta($this->input->post('hasta'));
            $vo->setCountry($country);

            $this->em->persist($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "adding Product Line " . $vo->getCtaOracle() . " - " . $vo->getDesde(). " - " . $vo->getHasta(), null, $vo->getCountry(), 'GLP_CHANGE');

            $this->session->topActive = 'params';
            $this->session->bottomActive = 'PL';

            redirect(base_url() . "GLParams/" . $country);
        }

    }

    public function removeGLPL($id){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLProductLine', 'vo')
            ->where($qb->expr()->eq('vo.id', '?1'))
            ->setParameter(1, $id);

        $vo = $qb->getQuery()->getSingleResult();

        $this->em->remove($vo);
        $this->em->flush();

        $this->session->topActive = 'params';
        $this->session->bottomActive = 'PL';

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "removing Product Line " . $vo->getctaOracle() . " - " . $vo->getDesde(). " - " . $vo->getHasta(), null, $vo->getCountry(), 'GLP_CHANGE');

        redirect(base_url() . "GLParams/" . $vo->getCountry());

    }


    private function loadGLPL($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLProductLine', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function addGLIC($country){

        $this->form_validation->set_rules('ctaSigla', 'ctaSigla', 'required|max_length[8]');
        $this->form_validation->set_rules('ICCod', 'ICCod', 'required|max_length[6]');

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['errorctaSigla'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('ctaSigla') ));
            $data['errorICCod'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('ICCod') ));

            $data['topActive'] = 'params';
            $data['bottomActive'] = 'IC';

            $this->session->selGroup= 'Setup';
            $this->session->selCountry = $country;

            $this->startEditSequenceParams($country, $data);

        } else {

            $vo = new Commons\MAPS\GLIntercompany;
            $vo->setCtaSigla($this->input->post('ctaSigla'));
            $vo->setIcCod($this->input->post('ICCod'));

            $vo->setCountry($country);

            $this->em->persist($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "adding Intercompany " . $vo->getctaSigla() . " - " . $vo->getICCod(), null, $vo->getCountry(), 'GLP_CHANGE');

            $this->session->topActive = 'params';
            $this->session->bottomActive = 'IC';

            redirect(base_url() . "GLParams/" . $country);
        }

    }

    public function removeGLIC($id){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLIntercompany', 'vo')
            ->where($qb->expr()->eq('vo.id', '?1'))
            ->setParameter(1, $id);

        $vo = $qb->getQuery()->getSingleResult();

        $this->em->remove($vo);
        $this->em->flush();

        $this->session->topActive = 'params';
        $this->session->bottomActive = 'IC';

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "adding Intercompany " . $vo->getctaSigla() . " - " . $vo->getICCod(), null, $vo->getCountry(), 'GLP_CHANGE');

        redirect(base_url() . "GLParams/" . $vo->getCountry());

    }

    private function loadGLIC($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLIntercompany', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function addGLLOB($country){

        $this->form_validation->set_rules('ctaOracle', 'ctaOracle', 'required|max_length[8]');
        $this->form_validation->set_rules('lobParam1', 'lobParam1', 'required|max_length[2]');
        $this->form_validation->set_rules('lobParam2', 'lobParam2', 'max_length[2]');
        $this->form_validation->set_rules('lobParam3', 'lobParam3', 'max_length[2]');
        $this->form_validation->set_rules('lobParam4', 'lobParam4', 'max_length[2]');
        $this->form_validation->set_rules('lobParam5', 'lobParam5', 'max_length[2]');

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['errorCtaOracle'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('ctaOracle') ));
            $data['errorLobParam1'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('lobParam1') ));

            $data['topActive'] = 'params';
            $data['bottomActive'] = 'LOB';

            $this->session->selGroup= 'Setup';
            $this->session->selCountry = $country;

            $this->startEditSequenceParams($country, $data);

        } else {

            $vo = new Commons\MAPS\GLLineOfBusiness();
            $vo->setCtaOracle($this->input->post('ctaOracle'));
            $vo->setLobParam1($this->input->post('lobParam1'));
            $vo->setLobParam2($this->input->post('lobParam2'));
            $vo->setLobParam3($this->input->post('lobParam3'));
            $vo->setLobParam4($this->input->post('lobParam4'));
            $vo->setLobParam5($this->input->post('lobParam5'));
            $vo->setCountry($country);

            $this->em->persist($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "adding Line of Business " . $vo->getCtaOracle() . " - " . $vo->getLobParam1(). " - " . $vo->getLobParam2()
                . " - " . $vo->getLobParam3(). " - " . $vo->getLobParam4(). " - " . $vo->getLobParam5(), null, $vo->getCountry(), 'GLP_CHANGE');

            $this->session->topActive = 'params';
            $this->session->bottomActive = 'LOB';

            redirect(base_url() . "GLParams/" . $country);
        }

    }

    public function removeGLLOB($id){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLLineOfBusiness', 'vo')
            ->where($qb->expr()->eq('vo.id', '?1'))
            ->setParameter(1, $id);

        $vo = $qb->getQuery()->getSingleResult();

        $this->em->remove($vo);
        $this->em->flush();

        $this->session->topActive = 'params';
        $this->session->bottomActive = 'LOB';

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "adding Line of Business " . $vo->getCtaOracle() . " - " . $vo->getLobParam1(). " - " . $vo->getLobParam2()
            . " - " . $vo->getLobParam3(). " - " . $vo->getLobParam4(). " - " . $vo->getLobParam5(), null, $vo->getCountry(), 'GLP_CHANGE');

        redirect(base_url() . "GLParams/" . $vo->getCountry());
    }

    private function loadGLLOB($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLLineOfBusiness', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function addGLLOBFixed($country){

        $this->form_validation->set_rules('ctaOracle', 'ctaOracle', 'required|max_length[8]');
        $this->form_validation->set_rules('lobParam', 'lobParam', 'required|max_length[2]');

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['errorCtaOracle'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('ctaOracle') ));
            $data['errorLobParam'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('lobParam') ));

            $data['topActive'] = 'params';
            $data['bottomActive'] = 'LOBFixed';

            $this->session->selGroup= 'Setup';
            $this->session->selCountry = $country;

            $this->startEditSequenceParams($country, $data);

        } else {

            $vo = new Commons\MAPS\GLLineOfBusinessFixed;
            $vo->setCtaOracle($this->input->post('ctaOracle'));
            $vo->setLobParam($this->input->post('lobParam'));
            $vo->setCountry($country);

            $this->em->persist($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "adding Line of Business Fixed " . $vo->getCtaOracle() . " - " . $vo->getLobParam(), null, $vo->getCountry(), 'GLP_CHANGE');

            $this->session->topActive = 'params';
            $this->session->bottomActive = 'LOBFixed';

            redirect(base_url() . "GLParams/" . $country);
        }

    }
    public function removeGLLOBFixed($id){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLLineOfBusinessFixed', 'vo')
            ->where($qb->expr()->eq('vo.id', '?1'))
            ->setParameter(1, $id);

        $vo = $qb->getQuery()->getSingleResult();

        $this->em->remove($vo);
        $this->em->flush();

        $this->session->topActive = 'params';
        $this->session->bottomActive = 'LOBFixed';

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "adding Line of Business Fixed " . $vo->getCtaOracle() . " - " . $vo->getLobParam(), null, $vo->getCountry(), 'GLP_CHANGE');

        redirect(base_url() . "GLParams/" . $vo->getCountry());

    }

    private function loadGLLOBFixed($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLLineOfBusinessFixed', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function addGLCostCenter($country){

        $this->form_validation->set_rules('ctaOracle', 'ctaOracle', 'required|max_length[8]');
        $this->form_validation->set_rules('desde', 'desde', 'required|max_length[4]');
        $this->form_validation->set_rules('hasta', 'hasta', 'required|max_length[4]');

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['errorCtaOracle'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('ctaOracle') ));
            $data['errorDesde'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('desde') ));
            $data['errorHasta'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('hasta') ));

            $data['topActive'] = 'params';
            $data['bottomActive'] = 'CC';

            $this->session->selGroup= 'Setup';
            $this->session->selCountry = $country;

            $this->startEditSequenceParams($country, $data);

        } else {

            $vo = new Commons\MAPS\GLCostCenter;
            $vo->setCtaOracle($this->input->post('ctaOracle'));
            $vo->setDesde($this->input->post('desde'));
            $vo->setHasta($this->input->post('hasta'));
            $vo->setCountry($country);

            $this->em->persist($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "adding CostCenter " . $vo->getCtaOracle() . " - " . $vo->getDesde(). " - " . $vo->getHasta(), null, $vo->getCountry(), 'GLP_CHANGE');

            $this->session->topActive = 'params';
            $this->session->bottomActive = 'CC';

            redirect(base_url() . "GLParams/" . $vo->getCountry());

        }

    }

    public function removeGLCostCenter($id){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLCostCenter', 'vo')
            ->where($qb->expr()->eq('vo.id', '?1'))
            ->setParameter(1, $id);

        $vo = $qb->getQuery()->getSingleResult();

        $this->em->remove($vo);
        $this->em->flush();

        $this->session->topActive = 'params';
        $this->session->bottomActive = 'CC';

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "removing CostCenter " . $vo->getCtaOracle() . " - " . $vo->getDesde(). " - " . $vo->getHasta(), null, $vo->getCountry(), 'GLP_CHANGE');

        redirect(base_url() . "GLParams/" . $vo->getCountry());

    }

    private function loadGLCostCenters($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLCostCenter', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function addGLCostCenterFixed($country){

        $this->form_validation->set_rules('ctaOracle', 'ctaOracle', 'required|max_length[8]');
        $this->form_validation->set_rules('costcenter', 'costcenter', 'required|max_length[4]');

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['errorCtaOracle'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('ctaOracle') ));
            $data['errorCostCenter'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('costcenter') ));

            $data['topActive'] = 'params';
            $data['bottomActive'] = 'CCFixed';

            $this->session->selGroup= 'Setup';
            $this->session->selCountry = $country;

            $this->startEditSequenceParams($country, $data);

        } else {

            $vo = new Commons\MAPS\GLCostCenterFixed;
            $vo->setCtaOracle($this->input->post('ctaOracle'));
            $vo->setCostcenter($this->input->post('costcenter'));
            $vo->setLob($this->input->post('lob'));
            $vo->setCountry($country);

            $this->em->persist($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "adding CostCenter Fixed " . $vo->getCtaOracle() . " - " . $vo->getCostcenter(). " - " . $vo->getLob(), null, $vo->getCountry(), 'GLP_CHANGE');

            $this->session->topActive = 'params';
            $this->session->bottomActive = 'CCFixed';

            redirect(base_url() . "GLParams/" . $vo->getCountry());

        }

    }

    public function removeGLCostCenterFixed($id){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLCostCenterFixed', 'vo')
            ->where($qb->expr()->eq('vo.id', '?1'))
            ->setParameter(1, $id);

        $vo = $qb->getQuery()->getSingleResult();

        $this->em->remove($vo);
        $this->em->flush();

        $this->session->topActive = 'params';
        $this->session->bottomActive = 'CCFixed';

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "adding CostCenter Fixed " . $vo->getCtaOracle() . " - " . $vo->getCostcenter(). " - " . $vo->getLob(), null, $vo->getCountry(), 'GLP_CHANGE');

        redirect(base_url() . "GLParams/" . $vo->getCountry());

    }

    private function loadGLCostCenterFixed($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLCostCenterFixed', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }


    public function addGLExclusionCuentas($country){

        $this->form_validation->set_rules('ctaSigla', 'ctaSigla', 'required|max_length[8]');

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['errorCtaSigla'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('ctaSigla') ));

            $data['topActive'] = 'params';
            $data['bottomActive'] = 'Exclusion';

            $this->session->selGroup= 'Setup';
            $this->session->selCountry = $country;

            $this->startEditSequenceParams($country, $data);

        } else {

            $vo = new Commons\MAPS\GLExclusionCuentas;
            $vo->setCtaSigla($this->input->post('ctaSigla'));
            $vo->setCountry($country);

            $this->em->persist($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "adding Exclusion de Cuentas " . $vo->getCtaSigla(), null, $vo->getCountry(), 'GLP_CHANGE');

            $this->session->topActive = 'params';
            $this->session->bottomActive = 'Exclusion';

            redirect(base_url() . "GLParams/" . $vo->getCountry());
        }
    }

    public function removeGLExclusionCuentas($id){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLExclusionCuentas', 'vo')
            ->where($qb->expr()->eq('vo.id', '?1'))
            ->setParameter(1, $id);

        $vo = $qb->getQuery()->getSingleResult();

        $this->em->remove($vo);
        $this->em->flush();

        $this->session->topActive = 'params';
        $this->session->bottomActive = 'Exclusion';

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "removing CostCenter " . $vo->getctaSigla(), null, $vo->getCountry(), 'GLP_CHANGE');

        redirect(base_url() . "GLParams/" . $vo->getCountry());

    }

    private function loadGLExclusionCuentas($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLExclusionCuentas', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }


    private function loadGLDiarios($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLDiarios', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function updateGLDiarios($country)
    {
        $this->form_validation->set_rules('id', 'id', 'required'); // campo obrigatorio

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['topActive'] = 'tablas';
            $data['bottomActive'] = 'Diarios';

            $data['errorDiarios'] = str_replace("</p>", "", str_replace("<p>", "", "No items selected"));

            $this->session->selGroup = 'GLMenu';
            $this->session->selCountry = $country;

            $data['country'] = $country;
            $data = $this->loadCommonData($data, $country . ' Laundry GL Params', '', '', '', '');

            $this->startEditSequenceTables($country, $data);

        } else {

            $qb = $this->em->createQueryBuilder();
            $qb->select('vo')
                ->from('Commons\\MAPS\\GLDiarios', 'vo')
                ->where($qb->expr()->eq('vo.id', '?1'))
                ->setParameter(1, $this->input->post('id'));

            $vo = $qb->getQuery()->getSingleResult();

            if (isset($_POST['active'])){
                $vo->setActive(1);
            } else {
                $vo->setActive(0);
            }

            $this->em->merge($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "update GLDiarios " . $vo->getdiario() . " - " . $vo->getDescrip() . " - " . $vo->getactive(), null, $vo->getCountry(), 'GLP_CHANGE');

            $this->session->topActive = 'tablas';
            $this->session->bottomActive = 'Diarios';

            redirect(base_url() . "GLTables/" . $country);
        }
    }

    public function clearGLDiarios($id, $country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLDiarios', 'vo')
            ->where($qb->expr()->eq('vo.id', '?1'))
            ->setParameter(1, $id);

        $vo = $qb->getQuery()->getSingleResult();
        $vo->setActive(1);

        $this->em->merge($vo);
        $this->em->flush();

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "update GLDiarios " . $vo->getdiario() . " - " . $vo->getDescrip() . " - " . $vo->getactive(), null, $vo->getCountry(), 'GLP_CHANGE');

        $this->session->topActive = 'tablas';
        $this->session->bottomActive = 'Diarios';

        redirect(base_url() . "GLTables/" . $country);

    }

    private function loadGLConfigEstandar($country){
        $this->session->selGroup='GLMenu';
        $this->session->selCountry = $country;

        $data = array();
        $data['country'] = $country;
        $data = $this->loadCommonData($data,' GL Analysis', '', '', '' , '');

        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLConfigEstandar', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getSingleResult(Query::HYDRATE_ARRAY);
    }

    private function loadGLCostoLocal($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLCostoLocal', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function updateGLCostoLocal($country)
    {
        $this->form_validation->set_rules('id', 'id', 'required');

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['topActive'] = 'tablas';
            $data['bottomActive'] = 'CostoLocal';

            $data['errorCostoLocal'] = str_replace("</p>", "", str_replace("<p>", "",  "No items selected"));

            $this->session->selGroup = 'GLMenu';
            $this->session->selCountry = $country;

            $data['country'] = $country;
            $data = $this->loadCommonData($data, $country . ' Laundry GL Tables', '', '', '', '');

            $this->startEditSequenceTables($country, $data);

        } else {

            $qb = $this->em->createQueryBuilder();
            $qb->select('vo')
                ->from('Commons\\MAPS\\GLCostoLocal', 'vo')
                ->where($qb->expr()->eq('vo.id', '?1'))
                ->setParameter(1, $this->input->post('id'));

            $vo = $qb->getQuery()->getSingleResult();
            $vo->setId_sigla($this->input->post('id_sigla'));
            $vo->setExpenditure($this->input->post('expenditure'));
            $vo->setClasificacion($this->input->post('clasificacion'));

            $this->em->merge($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "update GLCostoLocal " . $vo->getId_sigla() . " - " . $vo->getDescrip() . " - " . $vo->getExpenditure() . " - " . $vo->getClasificacion(), null, $vo->getCountry(), 'GLP_CHANGE');

            $this->session->topActive = 'tablas';
            $this->session->bottomActive = 'CostoLocal';

            redirect(base_url() . "GLTables/" . $country);
        }
    }


    private function loadGLMonedas($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLMonedas', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function updateGLMonedas($country)
    {
        $this->form_validation->set_rules('id', 'id', 'required'); // campo obrigatorio
        $this->form_validation->set_rules('tasa', 'tasa', 'required|decimal'); // campo obrigatorio

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $qb = $this->em->createQueryBuilder();
            $qb->select('vo')
                ->from('Commons\\MAPS\\GLMonedas', 'vo')
                ->where($qb->expr()->eq('vo.id', '?1'))
                ->setParameter(1, $this->input->post('id'));

            $vo = $qb->getQuery()->getSingleResult();

            $data['moneda'] = $vo->getMoneda();
            $data['descrip'] = $vo->getDescrip();
            $data['sigla'] = $vo->getSigla();



            $data['topActive'] = 'tablas';
            $data['bottomActive'] = 'Monedas';

            $data['errorMonedas'] = str_replace("</p>", "", str_replace("<p>", "", $this->form_validation->error('tasa')));

            $this->session->selGroup = 'GLMenu';
            $this->session->selCountry = $country;

            $data['country'] = $country;
            $data = $this->loadCommonData($data, $country . ' Laundry GL Params', '', '', '', '');

            $this->startEditSequenceTables($country, $data);

        } else {

            $qb = $this->em->createQueryBuilder();
            $qb->select('vo')
                ->from('Commons\\MAPS\\GLMonedas', 'vo')
                ->where($qb->expr()->eq('vo.id', '?1'))
                ->setParameter(1, $this->input->post('id'));

            $vo = $qb->getQuery()->getSingleResult();
            $vo->setTasa((float)$this->input->post('tasa'));

            $this->em->merge($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "update GLMonedas " . $vo->getMoneda() . " - " . $vo->getDescrip() . " - " . $vo->getTasa(), null, $vo->getCountry(), 'GLP_CHANGE');

            $this->session->topActive = 'tablas';
            $this->session->bottomActive = 'Monedas';

            redirect(base_url() . "GLTables/" . $country);
        }
    }

    private function loadMS($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\MS', 'vo');
            //->where($qb->expr()->eq('vo.country', '?1'))
            //->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    private function loadGLPLFixed($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLProductLineFixed', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }


    public function addGLPLFixed($country){

        $this->form_validation->set_rules('lob', 'lob', 'required|max_length[2]');
        $this->form_validation->set_rules('pl', 'pl', 'required|max_length[2]');

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['errorLob'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('lob') ));
            $data['errorPL'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('pl') ));

            $data['topActive'] = 'params';
            $data['bottomActive'] = 'PLFixed';

            $this->session->selGroup= 'Setup';
            $this->session->selCountry = $country;

            $this->startEditSequenceParams($country, $data);

        } else {

            $vo = new Commons\MAPS\GLProductLineFixed;
            $vo->setLob($this->input->post('lob'));
            $vo->setPl($this->input->post('pl'));
            $vo->setCountry($country);

            $this->em->persist($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "adding Product Line Fixed" . $vo->getlob() . " - " . $vo->getpl(), null, $vo->getCountry(), 'GLP_CHANGE');

            $this->session->topActive = 'params';
            $this->session->bottomActive = 'PLFixed';

            redirect(base_url() . "GLParams/" . $country);
        }
    }

    public function removeGLPLFixed($id){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLProductLineFixed', 'vo')
            ->where($qb->expr()->eq('vo.id', '?1'))
            ->setParameter(1, $id);

        $vo = $qb->getQuery()->getSingleResult();

        $this->em->remove($vo);
        $this->em->flush();

        $this->session->topActive = 'params';
        $this->session->bottomActive = 'PLFixed';

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "removing Product Line Fixed" . $vo->getlob() . " - " . $vo->getpl(), null, $vo->getCountry(), 'GLP_CHANGE');

        redirect(base_url() . "GLParams/" . $vo->getCountry());
    }

    private function loadGLCheckRules($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLCheckRules', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function addGLCheckRules($country){

        $this->form_validation->set_rules('ctaOracle', 'ctaOracle', 'required|max_length[8]');

        if ($this->form_validation->run() === FALSE) {
            $postedValues = $this->input->post(NULL, FALSE);
            foreach ($postedValues as $key => $value) {
                $data[$key] = $value;
            }

            $data['errorctaOracle'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('ctaOracle') ));

            $data['topActive'] = 'params';
            $data['bottomActive'] = 'CR';

            $this->session->selGroup= 'Setup';
            $this->session->selCountry = $country;

            $this->startEditSequenceParams($country, $data);

        } else {

            $vo = new Commons\MAPS\GLCheckRules();
            $vo->setCtaOracle($this->input->post('ctaOracle'));
            $vo->setCc($this->input->post('cc'));
            $vo->setLob($this->input->post('lob'));
            $vo->setPl($this->input->post('pl'));
            $vo->setIc($this->input->post('ic'));

            $vo->setCountry($country);

            $this->em->persist($vo);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "adding Check Rules" .  $vo->getCtaOracle() . " - " . $vo->getcc() . " - " . $vo->getlob() . " - " . $vo->getpl() . " - " . $vo->getic(), null, $vo->getCountry(), 'GLP_CHANGE');

            $this->session->topActive = 'params';
            $this->session->bottomActive = 'CR';

            redirect(base_url() . "GLParams/" . $country);
        }
    }
    public function removeGLCheckRules($id){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLCheckRules', 'vo')
            ->where($qb->expr()->eq('vo.id', '?1'))
            ->setParameter(1, $id);

        $vo = $qb->getQuery()->getSingleResult();

        $this->em->remove($vo);
        $this->em->flush();

        $this->session->topActive = 'params';
        $this->session->bottomActive = 'CR';

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "removing Check Rules" .  $vo->getCtaOracle() . " - " . $vo->getcc() . " - " . $vo->getlob() . " - " . $vo->getpl() . " - " . $vo->getic(), null, $vo->getCountry(), 'GLP_CHANGE');

        redirect(base_url() . "GLParams/" . $vo->getCountry());
    }

    private function loadDiagnosticsB($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLDiagnosticsB', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    private function loadDiagnosticsR($country){
        $qb = $this->em->createQueryBuilder();
        $qb->select('vo')
            ->from('Commons\\MAPS\\GLDiagnosticsR', 'vo')
            ->where($qb->expr()->eq('vo.country', '?1'))
            ->setParameter(1, $country);

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }
}
