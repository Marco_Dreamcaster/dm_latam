<?php
include_once APPPATH . 'controllers/MY_Controller.php';

class Transactional extends MY_Controller {

    const LAYOUT_PA = array('Transactional/transactionalPA.tpl');
    const LAYOUT_ALL = array('Transactional/transactionalALL.tpl');
    const LAYOUT_UNAVAILABLE = array('Transactional/unavailable.tpl');

    const VIEW_LAYOUTS = array(
        "PA" => self::LAYOUT_PA,
        "CO" => self::LAYOUT_ALL,
        "CR" => self::LAYOUT_ALL,
        "HN" => self::LAYOUT_ALL,
        "GT" => self::LAYOUT_ALL,
        "MX" => self::LAYOUT_ALL,
        "NI" => self::LAYOUT_ALL,
        "CL" => self::LAYOUT_ALL,
        "PY" => self::LAYOUT_ALL,
        "UY" => self::LAYOUT_ALL,
        "PE" => self::LAYOUT_ALL,
        "AR" => self::LAYOUT_ALL,
        "SV" => self::LAYOUT_ALL
    );


    public function index($country='CO')
	{
        $this->session->selGroup = 'Transactional';
        $this->session->selCountry = $country;

        $data['country'] = $country;
        $data['cutoffDate'] = $this->defaultCutoffDatesByCountry[$country];
        $data = $this->loadCommonData($data,$country.' Transactional', '', '', '' , '');

        $this->displayStandardLayout(self::VIEW_LAYOUTS[$country], $data);
	}


}
