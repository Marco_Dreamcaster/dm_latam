<?php
include_once APPPATH . 'controllers/MY_Controller.php';

/**
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/query-builder.html
 *
 * http://www.doctrine-project.org/api/orm/2.5/class-Doctrine.ORM.QueryBuilder.html
 *
 * VendorBankAccount Controller (detail style)
 *
 */

Use Doctrine\ORM\Query;


class Main extends MY_Controller {

    const VO_CLASS_NAME = 'Commons\\ORA\\VendorBankAccount';
    const VO_PRE_CLASS_NAME = 'Commons\\RAW\\PreVendorBankAccount';

    const HEADER1_CLASS_NAME = 'Commons\\ORA\\VendorHeader';
    const HEADER1_KEY_COLUMNS = array('segment1');
    const CANCEL1 = "Vendors/customEditCleansed/";


    const ACTION_BASE = 'VendorBankAccount/updateCleansed/';
    const ACTION_CUSTOM = 'VendorBankAccount/customUpdateCleansed/';
    const ACTION_LABEL = 'Update VendorBankAccount';

    const LAYOUT_BASE = array(
        'ORA/VendorBankAccount/topBarCleansed.tpl',
        'ORA/VendorBankAccount/frmBase.tpl'
    );

    const LAYOUT_CUSTOM = array(
        'ORA/VendorBankAccount/topBarCleansed.tpl',
        'ORA/VendorBankAccount/frmCustom.tpl'
    );

    const LAYOUT_CUSTOM_CO = array(
        'ORA/VendorBankAccount/topBarCleansed.tpl',
        'ORA/VendorBankAccount/frmCustomCO.tpl'
    );

    public function __construct()
    {
        parent::__construct();

    }

    public function createNew($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME, array('id'),  array($id))->getSingleResult();

        switch($header->getCountry()){
            case 'PA':
                $layoutByCountry = self::LAYOUT_CUSTOM;
                $cancelAction = self::CANCEL1.'PA/'.$header->getId();
                break;

            case 'CO':
                $layoutByCountry = self::LAYOUT_CUSTOM_CO;
                $cancelAction = self::CANCEL1.'CO/'.$header->getId();
                break;

            default:
                $layoutByCountry = self::LAYOUT_CUSTOM_CO;
                $cancelAction = self::CANCEL1.$header->getCountry().'/'.$header->getId();
                break;
        }

        $actionCreateNewContact = 'VendorBankAccount/addNew/' . $header->getId() ;

        $data = array();

        $query = $this->em->createQuery("SELECT DISTINCT(geo.countryCode) as CC FROM Commons\\MAPS\ORAGeoCodes geo ORDER BY CC ASC");
        $data['countryCodes'] = $query->getResult(Query::HYDRATE_ARRAY);

        for($i=0;$i<sizeof($data['countryCodes']);$i++){
            $data['countryCodes'][$i]['selected']='';
        }

        $data['country'] = $header->getCountry();
        $data['vendorNumber'] = $header->getSegment1();
        $data['id'] = 'NEW';

        $data = $this->loadCommonData(
            $data,
            'Add a new BankAccount to Vendor #' . $header->getId(),
            $actionCreateNewContact,
            'Create new BankAccount for Vendor ' . $header->getSegment1(),
            '',
            $cancelAction
        );

        $this->displayStandardLayout($layoutByCountry, $data);


    }

    public function validate($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->validate($this->em);
        $this->em->flush();

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($vo->getVendorNumber()))->getSingleResult();
        $header->validateCrossRef($this->em);
        $this->em->flush();

        $this->customEditCleansed($vo->getCountry(), $id);
    }


    public function addNew($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME, array('id'), array($id))->getSingleResult();

        switch($header->getCountry()) {
            case 'PA':
                $this->form_validation->set_rules('bankName', 'bankName', 'required|max_length[60]');
                $this->form_validation->set_rules('branchName', 'branchName', 'required|max_length[60]');
                $this->form_validation->set_rules('bankNumber', 'bankNumber', 'required|max_length[30]');
                $this->form_validation->set_rules('branchNumber', 'branchNumber', 'required|max_length[25]');
                $this->form_validation->set_rules('bankHomeCountry', 'bankHomeCountry', 'required|max_length[2]');
                $this->form_validation->set_rules('taxPayerId', 'taxPayerId', 'max_length[20]');
                $this->form_validation->set_rules('bankAccountName', 'bankAccountName', 'required|max_length[80]');
                $this->form_validation->set_rules('bankAccountNum', 'bankAccountNum', 'required|max_length[30]');
                $this->form_validation->set_rules('currencyCode', 'currencyCode', 'required|max_length[15]');
                $this->form_validation->set_rules('iban', 'iban', 'max_length[30]');
                $this->form_validation->set_rules('bic', 'bic', 'max_length[50]');
                $this->form_validation->set_rules('type', 'type', 'max_length[25]');
                $this->form_validation->set_rules('startDate', 'startDate', 'required');
                break;

            case 'CO':
            case 'MX':
            case 'NI':
            case 'HN':
            case 'CR':
            case 'SV':
            default:
                $this->form_validation->set_rules('bankName', 'bankName', 'required|max_length[60]');
                /**
                 * $this->form_validation->set_rules('branchName', 'branchName', 'required|max_length[60]');
                 * $this->form_validation->set_rules('bankNumber', 'bankNumber', 'required|max_length[30]');
                 * $this->form_validation->set_rules('branchNumber', 'branchNumber', 'required|max_length[25]');
                 * $this->form_validation->set_rules('bankHomeCountry', 'bankHomeCountry', 'required|max_length[2]');
                 * $this->form_validation->set_rules('taxPayerId', 'taxPayerId', 'required|max_length[20]');
                 * $this->form_validation->set_rules('bankAccountName', 'bankAccountName', 'required|max_length[80]');
                 * $this->form_validation->set_rules('bankAccountNum', 'bankAccountNum', 'required|max_length[30]');
                 * $this->form_validation->set_rules('currencyCode', 'currencyCode', 'required|max_length[15]');
                 * $this->form_validation->set_rules('iban', 'iban', 'max_length[30]');
                 * $this->form_validation->set_rules('bic', 'bic', 'max_length[50]');
                 * $this->form_validation->set_rules('type', 'type', 'max_length[25]');
                 * $this->form_validation->set_rules('startDate', 'startDate', 'required');
                 * */
                break;
        }

        if ($this->form_validation->run() === FALSE){

            $data = array();

            $data['country'] = $header->getCountry();
            $data['vendorNumber'] = $header->getSegment1();
            $data['id'] = 'NEW';

            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            switch($header->getCountry()){
                case 'PA':
                    $layoutByCountry = self::LAYOUT_CUSTOM;
                    $cancelAction = self::CANCEL1.'PA/'.$header->getId();
                    break;

                case 'CO':
                case 'MX':
                case 'NI':
                case 'HN':
                case 'CR':
                case 'SV':
                    $layoutByCountry = self::LAYOUT_CUSTOM_CO;
                    $cancelAction = self::CANCEL1.'CO/'.$header->getId();
                    break;

                default:
                    $layoutByCountry = self::LAYOUT_BASE;
                    $cancelAction = self::CANCEL1.$header->getCountry().'/'.$header->getId();
                    break;
            }

            $actionCreateNewContact = 'VendorBankAccount/addNew/' . $header->getId() ;

            $data = $this->loadCommonData(
                $data,
                'Add a new Bank Account to Vendor #' . $header->getId(),
                $actionCreateNewContact,
                'Create new Bank Account to Vendor ' . $header->getSegment1(),
                '',
                $cancelAction
            );

            $query = $this->em->createQuery("SELECT DISTINCT(geo.countryCode) as CC FROM Commons\\MAPS\ORAGeoCodes geo ORDER BY CC ASC");
            $data['countryCodes'] = $query->getResult(Query::HYDRATE_ARRAY);

            for($i=0;$i<sizeof($data['countryCodes']);$i++){
                if ($data['countryCodes'][$i]['CC']==$data['bankHomeCountry']){
                    $data['countryCodes'][$i]['selected']='selected';
                }else{
                    $data['countryCodes'][$i]['selected']='';
                }

            }


            $data = $this->loadFormErrorsForClassNameAndCountry(self::VO_CLASS_NAME, $data['country'], $data, $this->form_validation);

            $this->displayStandardLayout($layoutByCountry, $data);

        }else{
            $vo = $this->createInstanceWithFormValues(self::VO_CLASS_NAME, $header->getCountry(), $this->input);

            $vo->setVendorNumber($header->getSegment1());
            $vo->setCountry($header->getCountry());
            $vo->setOrgName($header->getOrgName());
            $vo->setDismissed(false);

            $this->em->persist($vo);
            $this->em->flush();

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getvendorNumber()))->getSingleResult();

            $header->validateCrossRef($this->em);
            $this->em->flush();

            redirect(base_url() . self::CANCEL1 . $header->getCountry().'/'.$header->getId());
        }



    }

    public function editCleansed($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence(null, $data);


    }

    public function customEditCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence($country, $data);
    }

    public function toggle($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->setDismissed(!$vo->getDismissed());
        $this->em->merge($vo);
        $this->em->flush();


        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($vo->getVendorNumber()))->getSingleResult();

        redirect(base_url() . self::CANCEL1 . $header->getCountry(). '/'.$header->getId());

    }



    private function startEditSequence($country, $data){
        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($data['vendorNumber']))->getSingleResult();


        switch($country){
            case 'PA':
                $actionCustomByCountry = self::ACTION_CUSTOM . 'PA/' . $data['id'];
                $layoutByCountry = self::LAYOUT_CUSTOM;
                $cancelAction = self::CANCEL1.'PA/'.$header->getId();
                break;

            case 'CO':
                $actionCustomByCountry = self::ACTION_CUSTOM . 'CO/'. $data['id'];
                $layoutByCountry = self::LAYOUT_CUSTOM_CO;
                $cancelAction = self::CANCEL1.'CO/'.$header->getId();
                break;

            case 'MX':
                $actionCustomByCountry = self::ACTION_CUSTOM . 'MX/'. $data['id'];
                $layoutByCountry = self::LAYOUT_CUSTOM_CO;
                $cancelAction = self::CANCEL1.'MX/'.$header->getId();
                break;

            default:
                $actionCustomByCountry = self::ACTION_CUSTOM .$data['country'].'/'. $data['id'];
                $layoutByCountry = self::LAYOUT_CUSTOM_CO;
                $cancelAction = self::CANCEL1.$data['country'].'/'.$header->getId();
                break;
        }



        $data = $this->loadCommonData(
            $data,
            'Edit cleansed VendorBankAccount #' . $data['id'],
            $actionCustomByCountry,
            self::ACTION_LABEL,
            '',
            $cancelAction
        );

        $query = $this->em->createQuery("SELECT DISTINCT(geo.countryCode) as CC FROM Commons\\MAPS\ORAGeoCodes geo ORDER BY CC ASC");
        $data['countryCodes'] = $query->getResult(Query::HYDRATE_ARRAY);

        for($i=0;$i<sizeof($data['countryCodes']);$i++){
            if ($data['countryCodes'][$i]['CC']==$data['bankHomeCountry']){
                $data['countryCodes'][$i]['selected']='selected';
            }else{
                $data['countryCodes'][$i]['selected']='';
            }

        }

        $this->displayStandardLayout($layoutByCountry, $data);
    }

    public function updateCleansed($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->loadValidationRules(self::VO_CLASS_NAME, $id, $this->form_validation);

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence(null, $data);

        }else{
            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);

            $this->em->merge($vo);

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getvendorNumber()))->getSingleResult();

            $header->validateCrossRef($this->em);


            $this->em->flush();

            redirect(base_url() . self::CANCEL1 . $header->getCountry().'/'.$header->getId());
        }
    }

    public function customUpdateCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        switch($country){
            case 'PA':
                $this->form_validation->set_rules('bankName', 'bankName', 'required|max_length[60]');
                $this->form_validation->set_rules('branchName', 'branchName', 'required|max_length[60]');
                $this->form_validation->set_rules('bankNumber', 'bankNumber', 'required|max_length[30]');
                $this->form_validation->set_rules('branchNumber', 'branchNumber', 'required|max_length[25]');
                $this->form_validation->set_rules('bankHomeCountry', 'bankHomeCountry', 'required|max_length[2]');
                $this->form_validation->set_rules('taxPayerId', 'taxPayerId', 'max_length[20]');
                $this->form_validation->set_rules('bankAccountName', 'bankAccountName', 'required|max_length[80]');
                $this->form_validation->set_rules('bankAccountNum', 'bankAccountNum', 'required|max_length[30]');
                $this->form_validation->set_rules('currencyCode', 'currencyCode', 'required|max_length[15]|in_list[USD,COP,MEP,PAB,MXN]');
                $this->form_validation->set_rules('iban', 'iban', 'max_length[30]');
                $this->form_validation->set_rules('bic', 'bic', 'max_length[50]');
                $this->form_validation->set_rules('type', 'type', 'max_length[25]');
                $this->form_validation->set_rules('startDate', 'startDate', 'required');

                break;

            case 'CO':
            case 'MX':
            case 'NI':
            case 'HN':
            case 'CR':
            case 'SV':
            default:
                $this->form_validation->set_rules('bankName', 'bankName', 'required|max_length[60]');
                $this->form_validation->set_rules('branchName', 'branchName', 'required|max_length[60]');
                $this->form_validation->set_rules('bankNumber', 'bankNumber', 'required|max_length[30]');
                $this->form_validation->set_rules('branchNumber', 'branchNumber', 'required|max_length[25]');
                $this->form_validation->set_rules('bankHomeCountry', 'bankHomeCountry', 'required|max_length[2]');
                $this->form_validation->set_rules('taxPayerId', 'taxPayerId', 'required|max_length[20]');
                $this->form_validation->set_rules('bankAccountName', 'bankAccountName', 'required|max_length[80]');
                $this->form_validation->set_rules('bankAccountNum', 'bankAccountNum', 'required|max_length[30]');
                $this->form_validation->set_rules('currencyCode', 'currencyCode', 'required|max_length[15]');
                $this->form_validation->set_rules('iban', 'iban', 'max_length[30]');
                $this->form_validation->set_rules('bic', 'bic', 'max_length[50]');
                $this->form_validation->set_rules('type', 'type', 'max_length[25]');
                $this->form_validation->set_rules('startDate', 'startDate', 'required');
                break;

        }

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence($country, $data);

        }else{

            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);
            $this->em->flush();

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getvendorNumber()))->getSingleResult();

            $header->validateCrossRef($this->em);
            $this->em->flush();



            redirect(base_url() . self::CANCEL1 . $header->getCountry().'/'.$header->getId());
        }
    }
}

