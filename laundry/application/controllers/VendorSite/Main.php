<?php
include_once APPPATH . 'controllers/MY_Controller.php';

/**
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/query-builder.html
 *
 * http://www.doctrine-project.org/api/orm/2.5/class-Doctrine.ORM.QueryBuilder.html
 *
 * VendorSite Controller (detail style)
 *
 */

Use Doctrine\ORM\Query;


class Main extends MY_Controller {

    const VO_CLASS_NAME = 'Commons\\ORA\\VendorSite';
    const VO_PRE_CLASS_NAME = 'Commons\\RAW\\PreVendorSite';

    const HEADER1_CLASS_NAME = 'Commons\\ORA\\VendorHeader';
    const HEADER1_KEY_COLUMNS = array('segment1');
    const CANCEL1 = "Vendors/customEditCleansed/";

    const CHILD1_CLASS_NAME = 'Commons\\ORA\\VendorSiteContact';
    const CHILD1_PRE_CLASS_NAME = 'Commons\\RAW\\PreVendorSiteContact';
    const CHILD1_KEY_COLUMNS = array('vendorSiteCode');


    const ACTION_BASE = 'VendorSite/updateCleansed/';
    const ACTION_CUSTOM = 'VendorSite/customUpdateCleansed/';
    const ACTION_LABEL = 'Update VendorSite';

    const LAYOUT_BASE = array(
        'ORA/VendorSite/topBarCleansed.tpl',
        'ORA/VendorSite/frmBase.tpl'
    );

    const DEFAULT_PA = [
        'frm' => array(
            'ORA/VendorSite/topBarCleansed.tpl',
            'ORA/VendorSite/frmCustom.tpl')
    ];

    const DEFAULT_CO = [
        'frm' => array(
            'ORA/VendorSite/topBarCleansed.tpl',
            'ORA/VendorSite/frmCustomCO.tpl')
    ];

    const DEFAULT_ALL = [
        'frm' => array(
            'ORA/VendorSite/topBarCleansed.tpl',
            'ORA/VendorSite/frmCustomALL.tpl')
    ];


    const VIEW_LAYOUTS = array(
        "PA" => self::DEFAULT_PA,
        "CO" => self::DEFAULT_CO,
        "CR" => self::DEFAULT_ALL,
        "HN" => self::DEFAULT_ALL,
        "GT" => self::DEFAULT_ALL,
        "MX" => self::DEFAULT_ALL,
        "NI" => self::DEFAULT_ALL,
        "SV" => self::DEFAULT_ALL,
        "UY" => self::DEFAULT_ALL,
        "PY" => self::DEFAULT_ALL,
        "PE" => self::DEFAULT_ALL,
        "AR" => self::DEFAULT_ALL,
        "EC" => self::DEFAULT_ALL,
        "CL" => self::DEFAULT_ALL
    );


    public function __construct()
    {
        parent::__construct();

    }

    public function editCleansed($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence(null, $data);
    }

    public function customEditCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }


        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($data['vendorNumber']))->getSingleResult();

        $data['vendorName'] = $header->getVendorName();

        $this->startEditSequence($country, $data);
    }


    private function startEditSequence($country, $data){
        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($data['vendorNumber']))->getSingleResult();

        $actionCustomByCountry = self::ACTION_CUSTOM . $country. '/' . $data['id'];
        $cancelAction = self::CANCEL1.$country.'/'. $header->getId();

        $data = $this->loadOraGeoCodes($data);

        // VendorSiteContact children
        $data['contacts'] = $this->getChildQuery(self::CHILD1_CLASS_NAME,
        self::CHILD1_KEY_COLUMNS,

        array($data['vendorSiteCode']))->getResult(Query::HYDRATE_ARRAY);

        $data = $this->loadCommonData(
            $data,
            'Edit cleansed VendorSite #' . $data['id'],
            $actionCustomByCountry,
            self::ACTION_LABEL,
            '',
            $cancelAction
        );

        $this->displayStandardLayout(self::VIEW_LAYOUTS[$country]['frm'], $data);
    }

    public function createNew($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME, array('id'),  array($id))->getSingleResult();

        $cancelAction = self::CANCEL1.$header->getCountry().'/'.$header->getId();

        $actionCreateNewContact = 'VendorSite/addNew/' . $header->getId() ;

        $data = array();

        $data['addressCountry'] = '' ;
        $data['state'] = '' ;
        $data['province'] = '' ;
        $data['city'] = '' ;

        $data = $this->loadOraGeoCodes($data);

        $data['country'] = $header->getCountry();
        $data['vendorNumber'] = $header->getSegment1();
        $data['id'] = 'NEW';

        $data = $this->loadCommonData(
            $data,
            'Add a new VendorSite to Vendor #' . $header->getId(),
            $actionCreateNewContact,
            'Create new VendorSite for Vendor ' . $header->getSegment1(),
            '',
            $cancelAction
        );

        $this->displayStandardLayout(self::VIEW_LAYOUTS[$header->getCountry()]['frm'], $data);

    }

    public function addNew($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME, array('id'), array($id))->getSingleResult();

        switch($header->getCountry()) {
            case 'PA':
                //$this->form_validation->set_rules('vendorNumber', 'vendorNumber', 'required|max_length[30]');
                //$this->form_validation->set_rules('vendorSiteCode', 'vendorSiteCode', 'required|max_length[15]');
                //$this->form_validation->set_rules('vendorSiteCodeAlt', 'vendorSiteCodeAlt', 'max_length[320]');
                $this->form_validation->set_rules('addressLine1', 'addressLine1', 'required|max_length[150]');
                //$this->form_validation->set_rules('addressLine2', 'addressLine2', 'max_length[150]');
                //$this->form_validation->set_rules('addressLine3', 'addressLine3', 'max_length[240]');
                //$this->form_validation->set_rules('addressLine4', 'addressLine4', 'max_length[240]');
                //$this->form_validation->set_rules('addressLineAlt', 'addressLineAlt', 'max_length[560]');
                //$this->form_validation->set_rules('city', 'city', 'required|max_length[25]');
                //$this->form_validation->set_rules('county', 'county', 'max_length[150]');
                //$this->form_validation->set_rules('state', 'state', 'required|max_length[50]');
                //$this->form_validation->set_rules('province', 'province', 'max_length[150]');
                //$this->form_validation->set_rules('zip', 'zip', 'max_length[20]');
                //$this->form_validation->set_rules('addressCountry', 'addressCountry', 'required|max_length[25]');
                //$this->form_validation->set_rules('areaCode', 'areaCode', 'max_length[15]');
                //$this->form_validation->set_rules('phone', 'phone', 'max_length[15]');
                //$this->form_validation->set_rules('faxAreaCode', 'faxAreaCode', 'max_length[15]');
                //$this->form_validation->set_rules('fax', 'fax', 'max_length[15]');
                //$this->form_validation->set_rules('paymentMethodLookupCode', 'paymentMethodLookupCode', 'max_length[25]');
                //$this->form_validation->set_rules('vatCode', 'vatCode', 'max_length[20]');
                //$this->form_validation->set_rules('acctsPayAccount', 'acctsPayAccount', 'max_length[150]');
                //$this->form_validation->set_rules('prepayAccount', 'prepayAccount', 'max_length[150]');
                //$this->form_validation->set_rules('payGroupLookupCode', 'payGroupLookupCode', 'max_length[25]');
                //$this->form_validation->set_rules('invoiceCurrencyCode', 'invoiceCurrencyCode', 'max_length[15]');
                //$this->form_validation->set_rules('language', 'language', 'max_length[10]');
                //$this->form_validation->set_rules('termsName', 'termsName', 'max_length[200]|in_list[IMMEDIATE,NET30,NET60]');
                //$this->form_validation->set_rules('emailAddress', 'emailAddress', 'max_length[150]');
                //$this->form_validation->set_rules('vatRegistrationNum', 'vatRegistrationNum', 'max_length[20]');
                //$this->form_validation->set_rules('shipToLocationCode', 'shipToLocationCode', 'max_length[60]');
                //$this->form_validation->set_rules('attributeCategory', 'attributeCategory', 'max_length[30]');
                //$this->form_validation->set_rules('attribute1', 'attribute1', 'max_length[150]');
                //$this->form_validation->set_rules('attribute2', 'attribute2', 'max_length[150]');
                //$this->form_validation->set_rules('attribute3', 'attribute3', 'max_length[150]');
                //$this->form_validation->set_rules('attribute4', 'attribute4', 'max_length[150]');
                //$this->form_validation->set_rules('attribute5', 'attribute5', 'max_length[150]');
                //$this->form_validation->set_rules('attribute6', 'attribute6', 'max_length[150]');
                //$this->form_validation->set_rules('attribute7', 'attribute7', 'max_length[150]');
                //$this->form_validation->set_rules('attribute8', 'attribute8', 'max_length[150]');
                //$this->form_validation->set_rules('attribute9', 'attribute9', 'max_length[150]');
                //$this->form_validation->set_rules('attribute10', 'attribute10', 'max_length[150]');
                //$this->form_validation->set_rules('orgName', 'orgName', 'required|max_length[240]');
                $this->form_validation->set_rules('classificationTypeCode', 'classificationTypeCode', 'required|max_length[30]');
                $this->form_validation->set_rules('classificationCode', 'classificationCode', 'required|max_length[30]');
                $this->form_validation->set_rules('classificationStartDate', 'classificationStartDate', 'required');
                $this->form_validation->set_rules('coExtAttribute1', 'coExtAttribute1', 'max_length[15]');
                break;

            case 'CO':
            case 'MX':
            //$this->form_validation->set_rules('vendorNumber', 'vendorNumber', 'required|max_length[30]');
            //$this->form_validation->set_rules('vendorSiteCode', 'vendorSiteCode', 'required|max_length[15]');
            //$this->form_validation->set_rules('vendorSiteCodeAlt', 'vendorSiteCodeAlt', 'max_length[320]');
            $this->form_validation->set_rules('addressLine1', 'addressLine1', 'required|max_length[150]');
            //$this->form_validation->set_rules('addressLine2', 'addressLine2', 'required|max_length[150]');
            //$this->form_validation->set_rules('addressLine3', 'addressLine3', 'max_length[240]');
            //$this->form_validation->set_rules('addressLine4', 'addressLine4', 'max_length[240]');
            //$this->form_validation->set_rules('addressLineAlt', 'addressLineAlt', 'required|max_length[560]');
            //$this->form_validation->set_rules('city', 'city', 'required|max_length[25]');
            //$this->form_validation->set_rules('county', 'county', 'max_length[150]');
            //$this->form_validation->set_rules('state', 'state', 'required|max_length[50]');
            //$this->form_validation->set_rules('province', 'province', 'max_length[150]');
            //$this->form_validation->set_rules('zip', 'zip', 'max_length[20]');
            //$this->form_validation->set_rules('addressCountry', 'addressCountry', 'required|max_length[25]');
            //$this->form_validation->set_rules('areaCode', 'areaCode', 'max_length[15]');
            //$this->form_validation->set_rules('phone', 'phone', 'max_length[15]');
            //$this->form_validation->set_rules('faxAreaCode', 'faxAreaCode', 'max_length[15]');
            //$this->form_validation->set_rules('fax', 'fax', 'max_length[15]');
            //$this->form_validation->set_rules('paymentMethodLookupCode', 'paymentMethodLookupCode', 'max_length[25]');
            //$this->form_validation->set_rules('vatCode', 'vatCode', 'max_length[20]');
            //$this->form_validation->set_rules('acctsPayAccount', 'acctsPayAccount', 'max_length[150]');
            //$this->form_validation->set_rules('prepayAccount', 'prepayAccount', 'max_length[150]');
            //$this->form_validation->set_rules('payGroupLookupCode', 'payGroupLookupCode', 'max_length[25]');
            //$this->form_validation->set_rules('invoiceCurrencyCode', 'invoiceCurrencyCode', 'max_length[15]');
            //$this->form_validation->set_rules('language', 'language', 'max_length[10]');
            //$this->form_validation->set_rules('termsName', 'termsName', 'max_length[200]|in_list[IMMEDIATE,NET30,NET60]');
            //$this->form_validation->set_rules('emailAddress', 'emailAddress', 'max_length[150]');
            //$this->form_validation->set_rules('vatRegistrationNum', 'vatRegistrationNum', 'max_length[20]');
            //$this->form_validation->set_rules('shipToLocationCode', 'shipToLocationCode', 'max_length[60]');
            //$this->form_validation->set_rules('attributeCategory', 'attributeCategory', 'max_length[30]');
            //$this->form_validation->set_rules('attribute1', 'attribute1', 'max_length[150]');
            //$this->form_validation->set_rules('attribute2', 'attribute2', 'max_length[150]');
            //$this->form_validation->set_rules('attribute3', 'attribute3', 'max_length[150]');
            //$this->form_validation->set_rules('attribute4', 'attribute4', 'max_length[150]');
            //$this->form_validation->set_rules('attribute5', 'attribute5', 'max_length[150]');
            //$this->form_validation->set_rules('attribute6', 'attribute6', 'max_length[150]');
            //$this->form_validation->set_rules('attribute7', 'attribute7', 'max_length[150]');
            //$this->form_validation->set_rules('attribute8', 'attribute8', 'max_length[150]');
            //$this->form_validation->set_rules('attribute9', 'attribute9', 'max_length[150]');
            //$this->form_validation->set_rules('attribute10', 'attribute10', 'max_length[150]');
            //$this->form_validation->set_rules('orgName', 'orgName', 'required|max_length[240]');
            $this->form_validation->set_rules('classificationTypeCode', 'classificationTypeCode', 'max_length[30]');
            $this->form_validation->set_rules('classificationCode', 'classificationCode', 'max_length[30]');
            break;

            case 'NI':
            case 'HN':
            case 'CR':
            case 'SV':
                //$this->form_validation->set_rules('vendorNumber', 'vendorNumber', 'required|max_length[30]');
                //$this->form_validation->set_rules('vendorSiteCode', 'vendorSiteCode', 'required|max_length[15]');
                //$this->form_validation->set_rules('vendorSiteCodeAlt', 'vendorSiteCodeAlt', 'max_length[320]');
                $this->form_validation->set_rules('addressLine1', 'addressLine1', 'required|max_length[150]');
                //$this->form_validation->set_rules('addressLine2', 'addressLine2', 'required|max_length[150]');
                //$this->form_validation->set_rules('addressLine3', 'addressLine3', 'max_length[240]');
                //$this->form_validation->set_rules('addressLine4', 'addressLine4', 'max_length[240]');
                //$this->form_validation->set_rules('addressLineAlt', 'addressLineAlt', 'required|max_length[560]');
                //$this->form_validation->set_rules('city', 'city', 'required|max_length[25]');
                //$this->form_validation->set_rules('county', 'county', 'max_length[150]');
                //$this->form_validation->set_rules('state', 'state', 'required|max_length[50]');
                //$this->form_validation->set_rules('province', 'province', 'max_length[150]');
                //$this->form_validation->set_rules('zip', 'zip', 'max_length[20]');
                //$this->form_validation->set_rules('addressCountry', 'addressCountry', 'required|max_length[25]');
                //$this->form_validation->set_rules('areaCode', 'areaCode', 'max_length[15]');
                //$this->form_validation->set_rules('phone', 'phone', 'max_length[15]');
                //$this->form_validation->set_rules('faxAreaCode', 'faxAreaCode', 'max_length[15]');
                //$this->form_validation->set_rules('fax', 'fax', 'max_length[15]');
                //$this->form_validation->set_rules('paymentMethodLookupCode', 'paymentMethodLookupCode', 'max_length[25]');
                //$this->form_validation->set_rules('vatCode', 'vatCode', 'max_length[20]');
                //$this->form_validation->set_rules('acctsPayAccount', 'acctsPayAccount', 'max_length[150]');
                //$this->form_validation->set_rules('prepayAccount', 'prepayAccount', 'max_length[150]');
                //$this->form_validation->set_rules('payGroupLookupCode', 'payGroupLookupCode', 'max_length[25]');
                //$this->form_validation->set_rules('invoiceCurrencyCode', 'invoiceCurrencyCode', 'max_length[15]');
                //$this->form_validation->set_rules('language', 'language', 'max_length[10]');
                //$this->form_validation->set_rules('termsName', 'termsName', 'max_length[200]|in_list[IMMEDIATE,NET30,NET60]');
                //$this->form_validation->set_rules('emailAddress', 'emailAddress', 'max_length[150]');
                //$this->form_validation->set_rules('vatRegistrationNum', 'vatRegistrationNum', 'max_length[20]');
                //$this->form_validation->set_rules('shipToLocationCode', 'shipToLocationCode', 'max_length[60]');
                //$this->form_validation->set_rules('attributeCategory', 'attributeCategory', 'max_length[30]');
                //$this->form_validation->set_rules('attribute1', 'attribute1', 'max_length[150]');
                //$this->form_validation->set_rules('attribute2', 'attribute2', 'max_length[150]');
                //$this->form_validation->set_rules('attribute3', 'attribute3', 'max_length[150]');
                //$this->form_validation->set_rules('attribute4', 'attribute4', 'max_length[150]');
                //$this->form_validation->set_rules('attribute5', 'attribute5', 'max_length[150]');
                //$this->form_validation->set_rules('attribute6', 'attribute6', 'max_length[150]');
                //$this->form_validation->set_rules('attribute7', 'attribute7', 'max_length[150]');
                //$this->form_validation->set_rules('attribute8', 'attribute8', 'max_length[150]');
                //$this->form_validation->set_rules('attribute9', 'attribute9', 'max_length[150]');
                //$this->form_validation->set_rules('attribute10', 'attribute10', 'max_length[150]');
                //$this->form_validation->set_rules('orgName', 'orgName', 'required|max_length[240]');
                //$this->form_validation->set_rules('classificationTypeCode', 'classificationTypeCode', 'max_length[30]|in_list[TKE_LATAM_CO_CONTRIBUTOR]');
                //$this->form_validation->set_rules('classificationCode', 'classificationCode', 'max_length[30]');
                //$this->form_validation->set_rules('coExtAttribute1', 'coExtAttribute1', 'required|max_length[15]');
                break;

            default:
                $this->form_validation->set_rules('addressLine1', 'addressLine1', 'required|max_length[150]');
                break;
        }

        if ($this->form_validation->run() === FALSE){

            $data = array();

            $data['country'] = $header->getCountry();
            $data['vendorNumber'] = $header->getSegment1();
            $data['id'] = 'NEW';

            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            if (isset($data['state'])){
                $data['province'] = $data['state'];

            }else{
                $data['province'] = '';

            }

            if (!isset($data['city'])){
                $data['city'] = '';

            }

            $cancelAction = self::CANCEL1.$header->getCountry().'/'.$header->getId();

            $actionCreateNewContact = 'VendorSite/addNew/' . $header->getId() ;

            $data = $this->loadCommonData(
                $data,
                'Add a new Bank Account to Vendor #' . $header->getId(),
                $actionCreateNewContact,
                'Create new Bank Account to Vendor ' . $header->getSegment1(),
                '',
                $cancelAction
            );

            $data = $this->loadOraGeoCodes($data);

            $data = $this->loadFormErrorsForClassNameAndCountry(self::VO_CLASS_NAME, $data['country'], $data, $this->form_validation);

            $this->displayStandardLayout(self::VIEW_LAYOUTS[$header->getCountry()]['frm'], $data);

        }else{
            $vo = $this->createInstanceWithFormValues(self::VO_CLASS_NAME, $header->getCountry(), $this->input);

            $vo->setVendorNumber($header->getSegment1());

            $query = $this->em->createQuery("SELECT COUNT(vs) FROM Commons\\ORA\VendorSite vs WHERE vs.vendorNumber=?1");
            $query->setParameter(1, $header->getSegment1());
            $vsCount = $query->getSingleScalarResult();

            $vo->setVendorSiteCode(str_replace('-VE','',$header->getSegment1()).'.'.($vsCount).'-VC');
            $vo->setVendorSiteCodeAlt($vo->getVendorSiteCode());

            $vo->setCountry($header->getCountry());
            $vo->setOrgName($header->getOrgName());
            $vo->setDismissed(false);

            $this->em->persist($vo);
            $this->em->flush();

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getvendorNumber()))->getSingleResult();

            $header->validateCrossRef($this->em);
            $this->em->flush();

            $this->session->selGroup='Vendors';
            $this->session->selCountry=strtolower($vo->getCountry());
            $this->session->selCol = "lastUpdated";
            $this->session->selAsc = false;

            redirect(base_url() . self::CANCEL1 . $header->getCountry().'/'.$header->getId());
        }



    }



    public function toggle($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->setDismissed(!$vo->getDismissed());
        $this->em->merge($vo);
        $this->em->flush();

        $this->toggleChildren(self::CHILD1_CLASS_NAME, self::CHILD1_KEY_COLUMNS,  array($vo->getVendorSiteCode()),$vo->getDismissed());

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($vo->getVendorNumber()))->getSingleResult();

        redirect(base_url() . self::CANCEL1 . $header->getCountry(). '/'.$header->getId());

    }

    public function validate($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->validate($this->em);
        $this->em->flush();

        $this->validateChildren(self::CHILD1_CLASS_NAME, self::CHILD1_KEY_COLUMNS,  array($vo->getVendorSiteCode()));

        $vo->validateCrossRef($this->em);
        $this->em->flush();

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($vo->getVendorNumber()))->getSingleResult();
        $header->validateCrossRef($this->em);
        $this->em->flush();

        $this->customEditCleansed($vo->getCountry(), $id);
    }


    public function updateCleansed($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->loadValidationRules(self::VO_CLASS_NAME, $id, $this->form_validation);

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence(null, $data);

        }else{
            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);

            $this->em->merge($vo);

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getvendorNumber()))->getSingleResult();

            $header->validateCrossRef($this->em);

            $this->em->flush();

            $this->session->selGroup='Vendors';
            $this->session->selCountry=strtolower($vo->getCountry());
            $this->session->selCol = "lastUpdated";
            $this->session->selAsc = false;


            redirect(base_url() . self::CANCEL . $vo->getCountry());
        }
    }

    public function customUpdateCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('vendorSiteCodeAlt', 'vendorSiteCodeAlt', 'max_length[15]');

        switch($country){
            case 'PA':
                $this->form_validation->set_rules('vendorSiteCodeAlt', 'vendorSiteCodeAlt', 'max_length[15]');
                $this->form_validation->set_rules('addressLine1', 'addressLine1', 'required|max_length[150]');
                $this->form_validation->set_rules('addressCountry', 'addressCountry', 'required|max_length[25]');
                $this->form_validation->set_rules('city', 'city', 'required|max_length[30]');
                $this->form_validation->set_rules('zip', 'zip', 'max_length[20]');
                $this->form_validation->set_rules('classificationTypeCode', 'classificationTypeCode', 'required|max_length[30]');
                $this->form_validation->set_rules('classificationCode', 'classificationCode', 'required|max_length[30]');
                $this->form_validation->set_rules('classificationStartDate', 'classificationStartDate', 'required|max_length[30]');
                break;

            case 'CO':
                $this->form_validation->set_rules('vendorSiteCodeAlt', 'vendorSiteCodeAlt', 'max_length[15]');
                $this->form_validation->set_rules('shipToLocationCode', 'shipToLocationCode', 'required|max_length[60]');
                $this->form_validation->set_rules('addressLine1', 'addressLine1', 'required|max_length[150]');
                $this->form_validation->set_rules('addressCountry', 'addressCountry', 'required|max_length[25]');
                $this->form_validation->set_rules('city', 'city', 'required|max_length[30]');
                $this->form_validation->set_rules('zip', 'zip', 'max_length[20]');
                break;


            case 'MX':
            case 'NI':
            case 'HN':
            case 'CR':
            case 'SV':
            case 'GT':
            default:
                $this->form_validation->set_rules('addressLine1', 'addressLine1', 'required|max_length[150]');
                break;
        }

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence($country, $data);

        }else{

            $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);

            $oldAddressCountry = $vo->getAddressCountry();
            $oldState = $vo->getState();
            $oldProvince = $vo->getProvince();
            $oldCounty = $vo->getCounty();
            $oldCity = $vo->getCity();

            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);

            if ($this->input->post('changeGeoCodes')==null){
                $vo->setAddressCountry($oldAddressCountry);
                $vo->setState($oldState);
                $vo->setProvince($oldProvince);
                $vo->setCounty($oldCounty);
                $vo->setCity($oldCity);
            }

            $this->em->merge($vo);
            $this->em->flush();

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getvendorNumber()))->getSingleResult();

            $header->validateCrossRef($this->em);
            $this->em->flush();

            $this->session->selGroup='Vendors';
            $this->session->selCountry=strtolower($vo->getCountry());
            $this->session->selCol = "lastUpdated";
            $this->session->selAsc = false;

            redirect(base_url() . self::CANCEL1 . $header->getCountry().'/'.$header->getId());
        }
    }
}

