<?php
include_once APPPATH . 'controllers/MY_Controller.php';

/**
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/query-builder.html
 *
 * http://www.doctrine-project.org/api/orm/2.5/class-Doctrine.ORM.QueryBuilder.html
 *
 * VendorSiteContact Controller (detail style)
 *
 */

Use Doctrine\ORM\Query;


class Main extends MY_Controller {

    const VO_CLASS_NAME = 'Commons\\ORA\\VendorSiteContact';
    const VO_PRE_CLASS_NAME = 'Commons\\RAW\\PreVendorSiteContact';

    const HEADER1_CLASS_NAME = 'Commons\\ORA\\VendorHeader';
    const HEADER1_KEY_COLUMNS = array('segment1');
    const CANCEL1 = "VendorHeader/customEditCleansed/";
    const HEADER2_CLASS_NAME = 'Commons\\ORA\\VendorSite';
    const HEADER2_KEY_COLUMNS = array('vendorSiteCode');
    const CANCEL2 = "VendorSite/customEditCleansed/";


    const ACTION_BASE = 'VendorSiteContact/updateCleansed/';
    const ACTION_CUSTOM = 'VendorSiteContact/customUpdateCleansed/';
    const ACTION_LABEL = 'Update VendorSiteContact';

    const LAYOUT_BASE = array(
        'ORA/VendorSiteContact/topBarCleansed.tpl',
        'ORA/VendorSiteContact/frmBase.tpl'
    );

    const LAYOUT_CUSTOM = array(
        'ORA/VendorSiteContact/topBarCleansed.tpl',
        'ORA/VendorSiteContact/frmCustom.tpl'
    );

    const LAYOUT_CUSTOM_CO = array(
        'ORA/VendorSiteContact/topBarCleansed.tpl',
        'ORA/VendorSiteContact/frmCustomCO.tpl'
    );

    public function __construct()
    {
        parent::__construct();

    }

    public function editCleansed($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence(null, $data);
    }

    public function customEditCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence($country, $data);
    }

    public function toggle($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->setDismissed(!$vo->getDismissed());
        $this->em->merge($vo);
        $this->em->flush();

        $header = $this->getHeaderQuery(self::HEADER2_CLASS_NAME,
            self::HEADER2_KEY_COLUMNS,
            array($vo->getVendorSiteCode()))->getSingleResult();

        redirect(base_url() . self::CANCEL2 . $header->getCountry(). '/'.$header->getId());

    }



    private function startEditSequence($country, $data){
        $header = $this->getHeaderQuery(self::HEADER2_CLASS_NAME,
            self::HEADER2_KEY_COLUMNS,
            array($data['vendorSiteCode']))->getSingleResult();


        switch($country){
            case 'PA':
                $actionCustomByCountry = self::ACTION_CUSTOM . 'PA/' . $data['id'];
                $layoutByCountry = self::LAYOUT_CUSTOM;
                $cancelAction = self::CANCEL2.'PA/'.$header->getId();
                break;

            case 'CO':
            case 'MX':
            case 'NI':
            case 'HN':
            case 'CR':
            case 'SV':
            case 'GT':
            case 'PE':
            case 'AR':
            case 'CL':
            case 'PY':
            case 'UY':
                $actionCustomByCountry = self::ACTION_CUSTOM . $country.'/'. $data['id'];
                $layoutByCountry = self::LAYOUT_CUSTOM_CO;
                $cancelAction = self::CANCEL2.$data['country'].'/'.$header->getId();
                break;

            default:
                $actionCustomByCountry = self::ACTION_BASE . $data['id'];
                $layoutByCountry = self::LAYOUT_BASE;
                $cancelAction = self::CANCEL2.$data['country'].'/'.$header->getId();
                break;
        }

        $data = $this->loadCommonData(
            $data,
            'Edit cleansed VendorSiteContact #' . $data['id'],
            $actionCustomByCountry,
            self::ACTION_LABEL,
            '',
            $cancelAction
        );

        $this->displayStandardLayout($layoutByCountry, $data);
    }

    public function createNew($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $header = $this->getHeaderQuery(self::HEADER2_CLASS_NAME, array('id'),  array($id))->getSingleResult();

        switch($header->getCountry()){
            case 'PA':
                $layoutByCountry = self::LAYOUT_CUSTOM;
                $cancelAction = self::CANCEL2.'PA/'.$header->getId();
                break;

            case 'CO':
            case 'MX':
            case 'NI':
            case 'HN':
            case 'CR':
            case 'SV':
            case 'GT':
            case 'PE':
            case 'AR':
            case 'CL':
            case 'PY':
            case 'UY':
                $layoutByCountry = self::LAYOUT_CUSTOM_CO;
                $cancelAction = self::CANCEL2.$header->getCountry().'/'.$header->getId();
                break;

            default:
                $layoutByCountry = self::LAYOUT_BASE;
                $cancelAction = self::CANCEL2.$header->getCountry().'/'.$header->getId();
                break;
        }

        $actionCreateNewContact = 'VendorSiteContact/addNew/' . $header->getId() ;

        $data = array();

        $data['country'] = $header->getCountry();
        $data['vendorNumber'] = $header->getVendorNumber();
        $data['vendorSiteCode'] = $header->getVendorSiteCode();
        $data['id'] = 'NEW';


        $data = $this->loadCommonData(
            $data,
            'Add a new Contact to VendorSite #' . $header->getId(),
            $actionCreateNewContact,
            'Create new Contact to Vendor ' . $header->getVendorSiteCode(),
            '',
            $cancelAction
        );

        $this->displayStandardLayout($layoutByCountry, $data);


    }

    public function addNew($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $header = $this->getHeaderQuery(self::HEADER2_CLASS_NAME, array('id'),  array($id))->getSingleResult();

        switch($header->getCountry()) {
            case 'PA':
                $this->form_validation->set_rules('lastName', 'lastName', 'required|max_length[60]');
                $this->form_validation->set_rules('emailAddress', 'emailAddress', 'valid_email');
                break;

            case 'CO':
            case 'MX':
            case 'NI':
            case 'HN':
            case 'CR':
            case 'SV':
                $this->form_validation->set_rules('lastName', 'lastName', 'required|max_length[60]');
                $this->form_validation->set_rules('emailAddress', 'emailAddress', 'valid_email');
                break;

            default:
                $this->form_validation->set_rules('lastName', 'lastName', 'required|max_length[60]');
                $this->form_validation->set_rules('emailAddress', 'emailAddress', 'valid_email');
                break;
        }

        if ($this->form_validation->run() === FALSE){

            $data = array();

            $data['country'] = $header->getCountry();
            $data['vendorNumber'] = $header->getVendorNumber();
            $data['vendorSiteCode'] = $header->getVendorSiteCode();
            $data['id'] = 'NEW';

            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            switch($header->getCountry()){
                case 'PA':
                    $layoutByCountry = self::LAYOUT_CUSTOM;
                    $cancelAction = self::CANCEL2.'PA/'.$header->getId();
                    break;

                case 'CO':
                case 'MX':
                case 'NI':
                case 'HN':
                case 'CR':
                case 'SV':
                    $layoutByCountry = self::LAYOUT_CUSTOM_CO;
                    $cancelAction = self::CANCEL2.$header->getCountry().'/'.$header->getId();
                    break;

                default:
                    $layoutByCountry = self::LAYOUT_BASE;
                    $cancelAction = self::CANCEL2.$header->getCountry().'/'.$header->getId();
                    break;
            }

            $actionCreateNewContact = 'VendorSiteContact/addNew/' . $header->getId() ;

            $data = $this->loadCommonData(
                $data,
                'Add a new Contact to VendorSite #' . $header->getId(),
                $actionCreateNewContact,
                'Create new Contact to Vendor ' . $header->getVendorSiteCode(),
                '',
                $cancelAction
            );

            $data['errorLastName'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('lastName')));
            $data['errorEmailAddress'] = str_replace("</p>","", str_replace("<p>", "",$this->form_validation->error('emailAddress')));

            $this->displayStandardLayout($layoutByCountry, $data);

        }else{
            $vo = new \Commons\ORA\VendorSiteContact();

            $vo->setVendorNumber($this->input->post('vendorNumber'));
            $vo->setVendorSiteCode($this->input->post('vendorSiteCode'));
            $vo->setPrefix($this->input->post('prefix'));
            $vo->setFirstName($this->input->post('firstName'));
            $vo->setMiddleName($this->input->post('middleName'));
            $vo->setLastName($this->input->post('lastName'));
            $vo->setDepartment($this->input->post('department'));
            $vo->setPhoneAreaCode($this->input->post('phoneAreaCode'));
            $vo->setPhoneNumber($this->input->post('phoneNumber'));
            $vo->setFaxAreaCode($this->input->post('faxAreaCode'));
            $vo->setFaxNumber($this->input->post('faxNumber'));
            $vo->setEmailAddress($this->input->post('emailAddress'));
            $vo->setCountry($header->getCountry());
            $vo->setOrgName($header->getOrgName());
            $vo->setDismissed(false);

            $this->em->persist($vo);
            $this->em->flush();

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getvendorNumber()))->getSingleResult();

            $header->validateCrossRef($this->em);
            $header = $this->getHeaderQuery(self::HEADER2_CLASS_NAME,
                self::HEADER2_KEY_COLUMNS,
                array($vo->getvendorSiteCode()))->getSingleResult();

            $header->validateCrossRef($this->em);

            $this->em->flush();

            redirect(base_url() . self::CANCEL2 . $header->getCountry().'/'.$header->getId());
        }


    }

    public function updateCleansed($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->loadValidationRules(self::VO_CLASS_NAME, $id, $this->form_validation);

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence(null, $data);

        }else{
            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);

            $this->em->merge($vo);

            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getvendorNumber()))->getSingleResult();

            $header->validateCrossRef($this->em);
            $header = $this->getHeaderQuery(self::HEADER2_CLASS_NAME,
                self::HEADER2_KEY_COLUMNS,
                array($vo->getvendorSiteCode()))->getSingleResult();

            $header->validateCrossRef($this->em);


            $this->em->flush();

            redirect(base_url() . self::CANCEL1 . $header->getCountry().'/'.$header->getId());
        }
    }

    public function customUpdateCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        // uncomment rules that fit the custom VendorSiteContact form
        switch($country){
            case 'PA':
                //$this->form_validation->set_rules('vendorNumber', 'vendorNumber', 'required|max_length[30]');
                //$this->form_validation->set_rules('vendorSiteCode', 'vendorSiteCode', 'required|max_length[25]');
                //$this->form_validation->set_rules('prefix', 'prefix', 'max_length[30]');
                //$this->form_validation->set_rules('firstName', 'firstName', 'max_length[15]');
                //$this->form_validation->set_rules('middleName', 'middleName', 'max_length[15]');
                $this->form_validation->set_rules('lastName', 'lastName', 'required|max_length[60]');
                //$this->form_validation->set_rules('department', 'department', 'max_length[230]');
                //$this->form_validation->set_rules('phoneAreaCode', 'phoneAreaCode', 'max_length[10]');
                //$this->form_validation->set_rules('phoneNumber', 'phoneNumber', 'max_length[40]');
                //$this->form_validation->set_rules('faxAreaCode', 'faxAreaCode', 'max_length[10]');
                //$this->form_validation->set_rules('faxNumber', 'faxNumber', 'max_length[40]');
                $this->form_validation->set_rules('emailAddress', 'emailAddress', 'max_length[2000]|valid_email');
                //$this->form_validation->set_rules('orgName', 'orgName', 'required|max_length[240]');

                break;

            case 'CO':
            case 'MX':
            case 'NI':
            case 'HN':
            case 'CR':
            case 'SV':
                //$this->form_validation->set_rules('vendorNumber', 'vendorNumber', 'required|max_length[30]');
                //$this->form_validation->set_rules('vendorSiteCode', 'vendorSiteCode', 'required|max_length[25]');
                //$this->form_validation->set_rules('prefix', 'prefix', 'max_length[30]');
                //$this->form_validation->set_rules('firstName', 'firstName', 'max_length[15]');
                //$this->form_validation->set_rules('middleName', 'middleName', 'max_length[15]');
                $this->form_validation->set_rules('lastName', 'lastName', 'required|max_length[60]');
                //$this->form_validation->set_rules('department', 'department', 'max_length[230]');
                //$this->form_validation->set_rules('phoneAreaCode', 'phoneAreaCode', 'max_length[10]');
                //$this->form_validation->set_rules('phoneNumber', 'phoneNumber', 'max_length[40]');
                //$this->form_validation->set_rules('faxAreaCode', 'faxAreaCode', 'max_length[10]');
                //$this->form_validation->set_rules('faxNumber', 'faxNumber', 'max_length[40]');
                $this->form_validation->set_rules('emailAddress', 'emailAddress', 'max_length[2000]|valid_email');
                //$this->form_validation->set_rules('orgName', 'orgName', 'required|max_length[240]');

                break;

            default:
                //$this->form_validation->set_rules('vendorNumber', 'vendorNumber', 'required|max_length[30]');
                //$this->form_validation->set_rules('vendorSiteCode', 'vendorSiteCode', 'required|max_length[25]');
                //$this->form_validation->set_rules('prefix', 'prefix', 'max_length[30]');
                //$this->form_validation->set_rules('firstName', 'firstName', 'max_length[15]');
                //$this->form_validation->set_rules('middleName', 'middleName', 'max_length[15]');
                $this->form_validation->set_rules('lastName', 'lastName', 'required|max_length[60]');
                //$this->form_validation->set_rules('department', 'department', 'max_length[230]');
                //$this->form_validation->set_rules('phoneAreaCode', 'phoneAreaCode', 'max_length[10]');
                //$this->form_validation->set_rules('phoneNumber', 'phoneNumber', 'max_length[40]');
                //$this->form_validation->set_rules('faxAreaCode', 'faxAreaCode', 'max_length[10]');
                //$this->form_validation->set_rules('faxNumber', 'faxNumber', 'max_length[40]');
                //$this->form_validation->set_rules('emailAddress', 'emailAddress', 'max_length[2000]');
                //$this->form_validation->set_rules('orgName', 'orgName', 'required|max_length[240]');

                break;
        }

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence($country, $data);

        }else{

            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);
            $this->em->merge($vo);
            $this->em->flush();

            //Vendor Header
            $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
                self::HEADER1_KEY_COLUMNS,
                array($vo->getVendorNumber()))->getSingleResult();
            $header->validateCrossRef($this->em);
            //*******//


            //Vendor Site
            $header = $this->getHeaderQuery(self::HEADER2_CLASS_NAME,
                self::HEADER2_KEY_COLUMNS,
                array($vo->getvendorSiteCode()))->getSingleResult();
            $header->validateCrossRef($this->em);
            //*******//

            $this->em->flush();

            redirect(base_url() . self::CANCEL2 . $header->getCountry(). '/'.$header->getId());
        }
    }

    public function validate($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->validate($this->em);
        $this->em->flush();

        $header = $this->getHeaderQuery(self::HEADER1_CLASS_NAME,
            self::HEADER1_KEY_COLUMNS,
            array($vo->getVendorNumber()))->getSingleResult();
        $header->validateCrossRef($this->em);
        $this->em->flush();

        $this->customEditCleansed($vo->getCountry(), $id);
    }

}

