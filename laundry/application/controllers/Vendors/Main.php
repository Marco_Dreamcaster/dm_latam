<?php
include_once APPPATH . 'controllers/MY_Controller.php';

/**
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/dql-doctrine-query-language.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/query-builder.html
 *
 * http://www.doctrine-project.org/api/orm/2.5/class-Doctrine.ORM.QueryBuilder.html
 *
 * VendorHeader Controller (header style)
 *
 */

Use Doctrine\ORM\Query;


class Main extends MY_Controller {

    const VO_CLASS_NAME = 'Commons\\ORA\\VendorHeader';
    const VO_PRE_CLASS_NAME = 'Commons\\RAW\\PreVendorHeader';

    const CHILD1_CLASS_NAME = 'Commons\\ORA\\VendorSite';
    const CHILD1_PRE_CLASS_NAME = 'Commons\\RAW\\PreVendorSite';
    const CHILD1_KEY_COLUMNS = array('vendorNumber');

    const CHILD2_CLASS_NAME = 'Commons\\ORA\\VendorSiteContact';
    const CHILD2_PRE_CLASS_NAME = 'Commons\\RAW\\PreVendorSiteContact';
    const CHILD2_KEY_COLUMNS = array('vendorNumber');

    const CHILD3_CLASS_NAME = 'Commons\\ORA\\VendorBankAccount';
    const CHILD3_PRE_CLASS_NAME = 'Commons\\RAW\\PreVendorBankAccount';
    const CHILD3_KEY_COLUMNS = array('vendorNumber');

    const ACTION_CLEANSE = 'Vendors/cleanseDirty/';
    const ACTION_CLEANSE_LABEL = 'Mark as Cleansed';

    const ACTION_BASE = 'Vendors/updateCleansed/';
    const ACTION_CUSTOM = 'Vendors/customUpdateCleansed/';
    const ACTION_LABEL = 'Update VendorHeader';

    const CANCEL = "Vendors/cleansed/";
    const CANCEL_DIRTY = "Vendors/dirty/";
    const DISCARD = 'Vendors/discardCleansed/';

    const LAYOUT_CLEANSED = array(
        'ORA/VendorHeader/lstCleansed.tpl'
    );

    const LAYOUT_DIRTY = array(
        'ORA/VendorHeader/lstDirty.tpl'
    );

    const LAYOUT_BASE = array(
        'ORA/VendorHeader/topBarCleansed.tpl',
        'ORA/VendorHeader/frmBase.tpl'
    );

    const LAYOUT_EDIT_DIRTY = array(
        'ORA/VendorHeader/frmBase.tpl'
    );

    const DEFAULT_ALL = [
        'frm' => array(
            'ORA/VendorHeader/topBarCleansed.tpl',
            'ORA/VendorHeader/frmCustom.tpl')
    ];

    const VIEW_LAYOUTS = array(
        "PA" => self::DEFAULT_ALL,
        "CO" => self::DEFAULT_ALL,
        "CR" => self::DEFAULT_ALL,
        "HN" => self::DEFAULT_ALL,
        "GT" => self::DEFAULT_ALL,
        "MX" => self::DEFAULT_ALL,
        "NI" => self::DEFAULT_ALL,
        "SV" => self::DEFAULT_ALL,
        "UY" => self::DEFAULT_ALL,
        "PY" => self::DEFAULT_ALL,
        "AR" => self::DEFAULT_ALL,
        "EC" => self::DEFAULT_ALL,
        "PE" => self::DEFAULT_ALL,
        "CL" => self::DEFAULT_ALL
    );


    public function __construct()
    {
        parent::__construct();

    }

    public function listCleansed($country, $page=0, $sortCol='lastUpdated', $sortAsc='0'){
        if ($this->checkPreConditions($country)) {
            show_error('please contact your system administrator');
        }

        if (!isset($sortCol)){
            $sortCol = $this->session->selCol;
        }

        if (!isset($sortAsc)){
            $sortAsc = $this->session->selAsc;
        }


        switch($sortCol){
            case 'name':
                $data['cleansedVOs'] = $this->getCleansedHeader(self::VO_CLASS_NAME, $country, $page, 'vendorName', $sortAsc==1)->getResult(Query::HYDRATE_ARRAY);
                break;
            case 'lastUpdated':
                $sortAsc = false;
                $this->session->selAsc = false;
                $data['cleansedVOs'] = $this->getCleansedHeader(self::VO_CLASS_NAME, $country, $page, 'lastUpdated', false)->getResult(Query::HYDRATE_ARRAY);
                break;
            case 'id':
                $data['cleansedVOs'] = $this->getCleansedHeader(self::VO_CLASS_NAME, $country, $page, 'segment1', $sortAsc==1)->getResult(Query::HYDRATE_ARRAY);
                break;
            case 'error':
                $this->session->selAsc = true;
                $sortAsc = true;
                $data['cleansedVOs'] = $this->getCleansedHeader(self::VO_CLASS_NAME, $country, $page, 'crossRefValidated', $sortAsc==1)->getResult(Query::HYDRATE_ARRAY);
                break;

            default:
                $data['cleansedVOs'] = $this->getCleansedHeader(self::VO_CLASS_NAME, $country, $page, 'lastUpdated', $sortAsc==1)->getResult(Query::HYDRATE_ARRAY);
                break;
        }

        $data['country'] = $country;
        $data['sortOrder'] = $sortCol;
        $data['sortDirection'] = $sortAsc==1?'ASC':'DESC';

        $this->session->selGroup='Vendors';
        $this->session->selCountry=strtolower($country);

        $data = $this->loadCommonData($data,$country.' Cleansed Vendors ', '', '', '' , '');

        $data = $this->addCleansedStats($data, self::VO_CLASS_NAME, $country, true);
        $data = $this->addPaginationMetadata($data, $country, $page, self::CANCEL);

        $this->displayStandardLayout(self::LAYOUT_CLEANSED, $data);
    }

    public function listDirty($country, $page = 0){
        if ($this->checkPreConditions($country)) {
            show_error('please contact your system administrator');
        }

        $data['dirtyVOs'] = $this->getDirtyHeader(self::VO_PRE_CLASS_NAME, $country, $page, 'vendorName')->getResult(Query::HYDRATE_ARRAY);

        $data['country'] = $country;

        $this->session->selGroup='Vendors';
        $this->session->selCountry=strtolower($country);

        $data = $this->loadCommonData($data,$country.' Dirty Vendors ', '', '', '' , '');

        $data = $this->loadStats($data, self::VO_PRE_CLASS_NAME, self::VO_CLASS_NAME, $country);

        $data['total'] = $data['totalPreSTG'];
        $data = $this->addPaginationMetadata($data, $country, $page, self::CANCEL_DIRTY);

        $this->displayStandardLayout(self::LAYOUT_DIRTY, $data);
    }

    public function editDirty($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }
        $data = $this->getVOHidratedByID(self::VO_PRE_CLASS_NAME,$id);

        $data = $this->loadCommonData(
            $data,
            'Edit dirty VendorHeader #' . $id,
            self::ACTION_CLEANSE . $data['id'],
            self::ACTION_CLEANSE_LABEL,
            '',
            self::CANCEL_DIRTY.$data['country']
        );

        $this->displayStandardLayout(self::LAYOUT_EDIT_DIRTY, $data);
    }

    public function toggle($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->setDismissed(!$vo->getDismissed());
        $this->em->merge($vo);
        $this->em->flush();

        $this->toggleChildren(self::CHILD1_CLASS_NAME, self::CHILD1_KEY_COLUMNS,  array($vo->getSegment1()),$vo->getDismissed());
        $this->toggleChildren(self::CHILD2_CLASS_NAME, self::CHILD2_KEY_COLUMNS,  array($vo->getSegment1()),$vo->getDismissed());
        $this->toggleChildren(self::CHILD3_CLASS_NAME, self::CHILD3_KEY_COLUMNS,  array($vo->getSegment1()),$vo->getDismissed());


        redirect(base_url() . self::CANCEL . $vo->getCountry());
    }

    public function cleanseDirty($id){
        $qb = $this->em->createQueryBuilder();
        $qb->select('pch')
            ->from(self::VO_PRE_CLASS_NAME, 'pch')
            ->where($qb->expr()->eq('pch.id', '?1'))
            ->setParameter(1, $id);

        $pre = $qb->getQuery()->getSingleResult();

        if (!$this->checkPreConditionsForCleansingVendor($pre)) {
            show_error('please check if there is already a Vendor with ID '.$pre->getSegment1());
            return;
        }

        $stg = self::VO_CLASS_NAME;
        $stg = new $stg;
        $stg = $this->cloneByReflection($pre, $stg);
        $stg->setDismissed(false);
        $stg->setCleansed(true);

        $this->em->persist($stg);
        $this->em->flush();

        $pre->setCleansed(true);
        $this->em->persist($pre);
        $this->em->flush();

        /**
         * cleanse children - clones dependencies from pre-stage to stage table
         *
         */
        $this->moveChildrenToStage( self::CHILD1_PRE_CLASS_NAME, self::CHILD1_CLASS_NAME, self::CHILD1_KEY_COLUMNS, array($stg->getSegment1()) );
        $this->moveChildrenToStage( self::CHILD2_PRE_CLASS_NAME, self::CHILD2_CLASS_NAME, self::CHILD2_KEY_COLUMNS, array($stg->getSegment1()) );
        $this->moveChildrenToStage( self::CHILD3_PRE_CLASS_NAME, self::CHILD3_CLASS_NAME, self::CHILD3_KEY_COLUMNS, array($stg->getSegment1()) );

        $stg->validateCrossRef($this->em);
        $this->em->persist($stg);
        $this->em->flush();

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "cleansing VendorHeader ".$id." - ".$stg->getVendorName(). " - ".$stg->getSegment1(), null, $stg->getCountry());

        $this->session->selGroup='Vendors';
        $this->session->selCountry=strtolower($stg->getCountry());
        $this->session->selCol = "lastUpdated";
        $this->session->selAsc = false;

        redirect(base_url().self::CANCEL.$stg->getCountry());




    }

    public function validate($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $vo = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $vo->validate($this->em);

        $this->em->flush();

        $vo->validateCrossRef($this->em);
        $this->em->flush();

        $this->customEditCleansed($vo->getCountry(), $id);
    }

    public function discardCleansed($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $stg = $this->getVOByID(self::VO_CLASS_NAME, $id);
        $pre = $this->getPreVO(self::VO_PRE_CLASS_NAME, array('segment1'), array($stg->getSegment1()));

        $this->em->remove($stg);
        $pre->setCleansed(false);
        $this->em->persist($pre);
        $this->em->flush();

        $this->discardChildren( self::CHILD1_PRE_CLASS_NAME,
            self::CHILD1_CLASS_NAME,
            self::CHILD1_KEY_COLUMNS,
            array($stg->getSegment1()));
        $this->discardChildren( self::CHILD2_PRE_CLASS_NAME,
            self::CHILD2_CLASS_NAME,
            self::CHILD2_KEY_COLUMNS,
            array($stg->getSegment1()));
        $this->discardChildren( self::CHILD3_PRE_CLASS_NAME,
            self::CHILD3_CLASS_NAME,
            self::CHILD3_KEY_COLUMNS,
            array($stg->getSegment1()));

        $this->auditor->log($_SERVER['REMOTE_ADDR'], "discarding VendorHeader ".$id." - ".$stg->getVendorName().' - '.$stg->getSegment1(), null, $stg->getCountry());

        $this->session->selGroup='Vendors';
        $this->session->selCountry=strtolower($stg->getCountry());
        $this->session->selCol = "lastUpdated";
        $this->session->selAsc = false;

        redirect(base_url().self::CANCEL_DIRTY.$stg->getCountry());
    }

    public function editCleansed($id)
    {
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence(null, $data);
    }

    public function customEditCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->startEditSequence($country, $data);
    }


    private function startEditSequence($country, $data){

        if (null ===(self::VIEW_LAYOUTS[$country])){
            show_error($country.' not configured, please contact your system administrator');

        }

        // VendorSite children
        $data['children1'] = $this->getChildQuery(self::CHILD1_CLASS_NAME, self::CHILD1_KEY_COLUMNS, array($data['segment1']))->getResult(Query::HYDRATE_ARRAY);

        // VendorBankAccount children
        $data['children3'] = $this->getChildQuery(self::CHILD3_CLASS_NAME, self::CHILD3_KEY_COLUMNS, array($data['segment1']))->getResult(Query::HYDRATE_ARRAY);

        $data = $this->loadCommonData(
            $data,
            'Edit cleansed Vendor Header #' . $data['id'],
            self::ACTION_CUSTOM . $country.'/' . $data['id'],
            self::ACTION_LABEL,
            self::DISCARD . $data['id'],
            self::CANCEL . $data['country']
        );

        $this->displayStandardLayout(self::VIEW_LAYOUTS[$country]['frm'], $data);
    }

    /**
     * @param $id
     *
     * updates cleansed VendorHeader with ID (formBase)
     *
     * updateCleansed is paired with editCleansed
     *
     */
    public function updateCleansed($id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->loadValidationRules(self::VO_CLASS_NAME, $id, $this->form_validation);

        if ($this->form_validation->run() === FALSE){
            $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }

            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence(null, $data);

        }else{
            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);

            $this->em->merge($vo);
            $this->em->flush();

            $this->session->selGroup='Vendors';
            $this->session->selCountry=strtolower($vo->getCountry());
            $this->session->selCol = "lastUpdated";
            $this->session->selAsc = false;

            redirect(base_url() . self::CANCEL . $vo->getCountry());
        }
    }

    public function customUpdateCleansed($country, $id){
        if ($this->checkPreConditions($id)) {
            show_error('please contact your system administrator');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data = $this->getVOHidratedByID(self::VO_CLASS_NAME, $id);

        $this->form_validation->set_rules('vendorName', 'vendorName', 'required|max_length[240]');
        if (!$data['dismissed']) {
            $this->form_validation->set_rules('_8id', '_8id', 'max_length[20]');
            $this->form_validation->set_rules('matchOptionLevel', 'matchOptionLevel', 'required|max_length[25]|in_list[2-WAY,3-WAY]');
            $this->form_validation->set_rules('paymentMethodLookupCode', 'paymentMethodLookupCode', 'max_length[25]|in_list[WIRE,CHECK,EFT]');

            $this->form_validation->set_rules('vendorTypeLookupCode', 'vendorTypeLookupCode', 'required|max_length[30]');
            $this->form_validation->set_rules('vatRegistrationNum', 'vatRegistrationNum', 'max_length[20]');
            $this->form_validation->set_rules('num1099', 'num1099', 'max_length[30]');
            $this->form_validation->set_rules('type1099', 'type1099', 'max_length[10]');
            $this->form_validation->set_rules('globalAttribute1', 'globalAttribute1', 'max_length[30]');
            $this->form_validation->set_rules('globalAttribute8', 'globalAttribute8', 'max_length[30]');
            $this->form_validation->set_rules('globalAttribute9', 'globalAttribute9', 'max_length[80]');
            $this->form_validation->set_rules('globalAttribute15', 'globalAttribute15', 'max_length[80]');
            $this->form_validation->set_rules('globalAttribute16', 'globalAttribute16', 'max_length[30]');
            $this->form_validation->set_rules('attributeCategory', 'attributeCategory', 'max_length[30]');
            $this->form_validation->set_rules('attribute1', 'attribute1', 'max_length[150]');
            $this->form_validation->set_rules('attribute2', 'attribute2', 'max_length[150]');
            $this->form_validation->set_rules('attribute3', 'attribute3', 'max_length[150]');
            $this->form_validation->set_rules('attribute4', 'attribute4', 'max_length[150]');
            $this->form_validation->set_rules('attribute5', 'attribute5', 'max_length[150]');
            $this->form_validation->set_rules('attribute6', 'attribute6', 'max_length[150]');
            $this->form_validation->set_rules('attribute7', 'attribute7', 'max_length[150]');
            $this->form_validation->set_rules('attribute8', 'attribute8', 'max_length[150]');
            $this->form_validation->set_rules('attribute9', 'attribute9', 'max_length[150]');
            $this->form_validation->set_rules('attribute10', 'attribute10', 'max_length[150]');


            switch($country){
                case 'PA':
                    $this->form_validation->set_rules('paExtAttribute1', 'paExtAttribute1', 'required');
                    break;

                case 'CO':
                    $this->form_validation->set_rules('globalAttribute10', 'globalAttribute10', 'required|max_length[80]');
                    $this->form_validation->set_rules('coExtAttribute2', 'coExtAttribute2', 'required');
                    $this->form_validation->set_rules('coExtAttribute1', 'coExtAttribute1', 'max_length[30]');
                    $this->form_validation->set_rules('coExtAttribute2', 'coExtAttribute2', 'max_length[3]');
                    $this->form_validation->set_rules('coExtAttribute3', 'coExtAttribute3', 'max_length[30]');
                    $this->form_validation->set_rules('coExtAttribute4', 'coExtAttribute4', 'max_length[30]');
                    $this->form_validation->set_rules('coExtAttribute5', 'coExtAttribute5', 'max_length[30]');
                    $this->form_validation->set_rules('coExtAttribute6', 'coExtAttribute6', 'required');
                    $this->form_validation->set_rules('coExtAttribute7', 'coExtAttribute7', 'required');
                    $this->form_validation->set_rules('coExtAttribute9', 'coExtAttribute9', 'required|max_length[30]');
                    break;

                case 'SV':
                    $this->form_validation->set_rules('supplierTypeSv', 'supplierTypeSv', 'required|max_length[150]');
                    $this->form_validation->set_rules('documentTypeSv', 'documentTypeSv', 'required|max_length[20]');
                    $this->form_validation->set_rules('contributorRegistrationSv', 'contributorRegistrationSv', 'required|max_length[150]');
                    $this->form_validation->set_rules('legalRepresentativeSv', 'legalRepresentativeSv', 'required|max_length[150]');
                    break;

                default:
                    break;
            }

        }


        if ($this->form_validation->run() === FALSE){
            $postedValues = $this->input->post(NULL, FALSE);
            foreach($postedValues as $key=>$value){
                $data[$key]=$value;
            }
            $data = $this->loadFormErrors(self::VO_CLASS_NAME, $id, $data, $this->form_validation);

            $this->startEditSequence($country, $data);

        }else{
            $vo = $this->loadInstanceWithFormValues(self::VO_CLASS_NAME, $id, $this->input);
            $vo->setCoExtAttribute1($this->input->post('coExtAttribute1')); // damn you non-UPPERCASED constants
            $vo->setPersonCategoryPE($this->input->post('personCategoryPE')); // damn you non-UPPERCASED constants
            if ($vo->getDocumentTypePE()=='00'){ $vo->setDocumentTypePE('0');}

            $this->em->merge($vo);
            $this->em->flush();

            $vo->validateCrossRef($this->em, false);
            $this->em->flush();

            $this->auditor->log($_SERVER['REMOTE_ADDR'], "updating VendorHeader ".$id." - ".$vo->getSegment1().' - '.$vo->getVendorName(), null, $vo->getCountry());

            $this->session->selGroup='Vendors';
            $this->session->selCountry=strtolower($vo->getCountry());
            $this->session->selCol = "lastUpdated";
            $this->session->selAsc = false;

            redirect(base_url() . self::CANCEL . $vo->getCountry());
        }
    }


    public function getPersonTypePE(){
        $urlParams =  $this->input->get();
        $personCategory = $urlParams[array_keys($urlParams)[0]];

        if ($personCategory=='Legal Person'){
            $data = [
                '0'=>'OTROS',
                '02'=>'Legal Person or Entity',
                '03'=>'Foreign Person'
            ];
        }else{
            $data = [
                '01'=>'Natural Person',
                '03'=>'Foreign Person',
                '04'=>'Acquirer - Ticket'
            ];
        }

        //add the header here
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        echo json_encode( $data );

    }

    public function getDocumentTypePE(){
        $urlParams =  $this->input->get();
        $personCategory = $urlParams[array_keys($urlParams)[0]];
        $personType = $urlParams[array_keys($urlParams)[1]];

        if ($personCategory=='Legal Person'){
            if (($personType=='0')){
                $data = [
                    '0'=>'ZERO'
                ];
            }else if ($personType=='02'){
                $data = [
                    '1'=>'LIBRETA ELECTORAL O DNI',
                    '6'=>'REG. UNICO DE CONTRIBUYENTES',
                    '7'=>'PASAPORTE',
                    'A'=>'CEDULA DIPLOMATICA'
                ];
            }else if ($personType=='03'){
                $data = [
                    '0'=>'OTROS TIPOS DE DOCUMENTOS'
                ];

            }else{
                $data = array();
            }

        }else{
            if ($personType=='01'){
                $data = [
                    '1'=>'LIBRETA ELECTORAL O DNI',
                    '2'=>'CARNET DE POLICIA',
                    '3'=>'LIBRETA MILITAR',
                    '6'=>'REG. UNICO DE CONTRIBUYENTES',
                    '7'=>'PASAPORTE',
                    'A'=>'CEDULA DIPLOMATICA'
                ];
            }else if ($personType=='03'){
                $data = [
                    '0'=>'OTROS TIPOS DE DOCUMENTOS',
                    '4'=>'CARNET DE EXTRANJERIA',
                    '7'=>'PASAPORTE',
                    '9'=>'CARNE DE SOLICIT DE REFUGIO',
                    'A'=>'CEDULA DIPLOMATICA DE IDENTIDAD'
                ];
            }else{
                $data = [
                    '0'=>'OTROS TIPOS DE DOCUMENTOS'
                ];
            }

        }

        //add the header here
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');

        echo json_encode( $data, JSON_FORCE_OBJECT );

    }


}
