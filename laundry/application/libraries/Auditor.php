<?php

Use Commons\AuditLog;
Use Doctrine\ORM\Query;



/**
 *
 * https://coderwall.com/p/7ljbyg/how-to-configure-doctrine-2-with-codeigniter-php
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/cookbook/integrating-with-codeigniter.html
 *
 */
class Auditor
{
	public $em;

	const PAGE_SIZE = 200;

	public function __construct()
    {
    }

    function setEntityManager($em){
	    $this->em = $em;
    }

    public function log($user_id, $event, $count = null, $country=null, $type=null, $snapshotDate=null, $cutoffDate=null){
        $log = new AuditLog($country,$user_id,substr($event,0,255),$count, $type, $snapshotDate, $cutoffDate);

        $this->em->persist($log);
        $this->em->flush();

    }




}
