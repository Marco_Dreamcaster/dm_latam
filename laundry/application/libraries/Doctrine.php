<?php
$autoloadEnv = getenv('AUTOLOAD_PATH');

if (!defined('AUTOLOAD_PATH')){
    if (getenv('SERVER_NAME')=='laundry.demo'){
        define('AUTOLOAD_PATH', '/var/www/dm_latam.demo/laundry/vendor/autoload.php');

    }else if (getenv('SERVER_NAME')=='localhost'){
        define('AUTOLOAD_PATH', 'D:\pub\dm_latam\laundry\vendor\autoload.php');

    }else{
        define('AUTOLOAD_PATH', 'C:\dm_latam\laundry\vendor\autoload.php');

    }

    include_once AUTOLOAD_PATH;

}

use Doctrine\Common\ClassLoader;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

/**
 * Doctrine bootstrap library for CodeIgniter
 *
 * https://coderwall.com/p/7ljbyg/how-to-configure-doctrine-2-with-codeigniter-php
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/cookbook/integrating-with-codeigniter.html
 *
 */

class Doctrine
{
	public $em;
    public $basedatos;

    protected $itemFilesBaseTemplate;
    protected $scriptBaseTemplate;
    protected $sqlCmdTemplate;
    protected $dbIniPath;

    protected $objectTags;


	public function __construct()
	{
	    require_once (AUTOLOAD_PATH);


        $isDevMode = true;
        $useSimpleAnnotationReader = false;
        $config = Setup::createAnnotationMetadataConfiguration(array(
            APPPATH."/models/Commons",
            APPPATH."/models/Commons/ORA",
            APPPATH."/models/Commons/RAW"), $isDevMode, $useSimpleAnnotationReader);


        if (getenv('SERVER_NAME')=='laundry.demo') {
            $this->basedatos='DOCKER';
            $this->itemFilesBaseTemplate='/var/www/dm_latam.demo/laundry/itemFiles';
            $this->scriptBaseTemplate='/var/www/dm_latam.demo/laundry/scripts/%location%';
            $this->dbIniPath='/var/www/dm_latam.demo/laundry/DB.ini';

        }else{
            $this->basedatos = 'PRODUCCION';
            $this->itemFilesBaseTemplate='C:\dm_latam\laundry\itemFiles';
            $this->scriptBaseTemplate='C:\dm_latam\laundry\scripts\%location%';
            $this->dbIniPath='C:\Xampp\DB.ini';

        }

        $ini = parse_ini_file($this->dbIniPath, false);
        $cUser = trim($ini['user']);
        $cPass = trim($ini['pass']);
        $cDb = trim($ini['db']);
        $cSrv = trim($ini['srv']);

        if (isset($ini) && $cUser <> ''  && $cPass <> '' && $cDb <> '' && $cSrv <> ''){
            $conn = array(
                'driver' => 'sqlsrv',
                'user' => $cUser,
                'password' => $cPass,
                'host' => $cSrv,
                'dbname' => $cDb
            );

            $this->sqlCmdTemplate='sqlcmd -S '.$cSrv.' -d '.$cDb.' -U '.$cUser.' -P "'.$cPass.'" -i %script%';

        }else{
            $conn = array(
                'driver' => 'sqlsrv',
                'user' => 'sa',
                'password' => 'Oracle2018',
                'host' => '10.148.1.44',
                'dbname' => 'DM_LATAM'
            );
            $this->basedatos='PRODUCCION';
            $this->itemFilesBaseTemplate='C:\dm_latam\laundry\itemFiles';
            $this->scriptBaseTemplate='C:\dm_latam\laundry\scripts\%location%';
            $this->sqlCmdTemplate='sqlcmd -S localhost -d DM_LATAM -U sa -P Oracle2018 -i %script%';
        }

        $entityManager = EntityManager::create($conn, $config);
        $this->em = $entityManager;

		// With this configuration, your model files need to be in application/models/Entity
		// e.g. Creating a new Entity\User loads the class from application/models/Entity/User.php
		$models_namespace = array(
            'Commons',
            'Commons\Base',
		    'Commons\ORA',
            'Commons\RAW');
		$models_path = array(
            APPPATH."/models/Commons",
            APPPATH."/models/Commons/ORA",
            APPPATH."/models/Commons/RAW");

		$loader = new ClassLoader($models_namespace, $models_path);
		$loader->register();
	}

	public function getBaseDatos(){
	    return $this->basedatos;
    }

    public function getEntityManager(){
	    return $this->em;
    }

    public function getSqlCmdTemplate(){
        return $this->sqlCmdTemplate;
    }

    public function getScriptBaseTemplate(){
        return $this->scriptBaseTemplate;
    }

    public function getItemFilesBaseTemplate()
    {
        return $this->itemFilesBaseTemplate;
    }

    public function getObjectTags()
    {
        return $this->objectTags;
    }

}
