<?php
$autoloadEnv = getenv('AUTOLOAD_PATH');

if (!$autoloadEnv){
    if (getenv('SERVER_NAME')=='laundry.demo'){
        define('AUTOLOAD_PATH', '/var/www/dm_latam.demo/laundry/vendor/autoload.php');

    }else{
        define('AUTOLOAD_PATH', 'C:\dm_latam\laundry\vendor\autoload.php');


    }

    include_once AUTOLOAD_PATH;
}


use \PhpOffice\PhpSpreadsheet\Spreadsheet;

class PhpSpreadsheet
{

	public function __construct()
	{
	    require_once (AUTOLOAD_PATH);

	}

	public function createNewSpreadsheet(){
	    return new Spreadsheet();
    }



}
