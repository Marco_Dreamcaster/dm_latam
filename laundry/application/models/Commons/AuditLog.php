<?php
/**
 * http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/types.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/events.html
 *
 * https://www.codeigniter.com/user_guide/libraries/sessions.html
 *
 */

namespace Commons;

/**
 * @Entity @Table(name="COM_AUDIT_LOGS")
 * @HasLifecycleCallbacks
 */
class AuditLog
{

    public function __construct($country, $user, $event, $count=null, $type=null, $snapshotDate=null, $cutoffDate=null, $groupRefCode=null){
        $this->country = $country;
        $this->user = $user;
        $this->event = $event;
        $this->type = $type;
        $this->snapshotDate = $snapshotDate;
        $this->cutoffDate = $cutoffDate;
        $this->count = $count;
        $this->groupRefCode = $groupRefCode;


    }

    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /**
     * @Column(name="COUNTRY", length=2, nullable=true)
     */
    protected $country;

    /**
     * @Column(name="USER_ID", length=15)
     */
    protected $user;

    /**
     * @Column(name="COUNT", type="integer", nullable=true)
     */
    protected $count;

    /**
     * @Column(name="TYPE", length=20, nullable=true)
     */
    protected $type;

    /**
     * @Column(name="SNAPSHOT_DATE", type="datetime", nullable=true)
     */
    protected $snapshotDate;

    /**
     * @Column(name="CUTOFF_DATE", type="datetime", nullable=true)
     */
    protected $cutoffDate;

    /**
     * @Column(name="EVENT", length=255)
     */
    protected $event;

    /**
     * @Column(name="STAMP", type="datetime")
     */
    protected $stamp;


    /**
     * @Column(name="GROUP_REF_CODE", type="integer", nullable=true)
     */
    protected $groupRefCode;

    /**
     * @Column(name="AMOUNT", type="float", nullable=true)
     */
    protected $amount;

    /**
     * @Column(name="TARGET", type="float", nullable=true)
     */
    protected $target;

    /**
     * @Column(name="DONE", type="float", nullable=true)
     */
    protected $done;


    /**
     * @return mixed
     */
    public function getGroupRefCode()
    {
        return $this->groupRefCode;
    }

    /**
     * @param mixed $groupRefCode
     */
    public function setGroupRefCode($groupRefCode)
    {
        $this->groupRefCode = $groupRefCode;
    }




    public function getId()
    {
        return $this->id;
    }

    public function getCountry()
    {
        return $this->country;
    }
    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function getUser()
    {
        return $this->user;
    }
    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getCount()
    {
        return $this->count;
    }
    public function setCount($count)
    {
        $this->count = $count;
    }


    public function getEvent()
    {
        return $this->event;
    }
    public function setEvent($event)
    {
        $this->event = $event;
    }

    public function getStamp()
    {
        return $this->stamp;
    }
    public function setStamp($stamp)
    {
        $this->stamp = $stamp;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getSnapshotDate()
    {
        return $this->snapshotDate;
    }

    /**
     * @param mixed $snapshotDate
     */
    public function setSnapshotDate($snapshotDate)
    {
        $this->snapshotDate = $snapshotDate;
    }

    /**
     * @return mixed
     */
    public function getCutoffDate()
    {
        return $this->cutoffDate;
    }

    /**
     * @param mixed $cutoffDate
     */
    public function setCutoffDate($cutoffDate)
    {
        $this->cutoffDate = $cutoffDate;
    }

    /** @PrePersist */
    public function doStuffOnPrePersist()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $this->stamp = new \DateTime("now");
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }


    /**
     * @return mixed
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param mixed $target
     */
    public function setTarget($target)
    {
        $this->target = $target;
    }

    /**
     * @return mixed
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * @param mixed $done
     */
    public function setDone($done)
    {
        $this->done = $done;
    }
}