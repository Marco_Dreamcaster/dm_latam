<?php
namespace Commons\Base;

use Commons\RAWBase;

class BaseCustomerAddress extends RAWBase
{
    public function __construct()
    {
    }

    // customerName generic STRING template*/
    /**
     * @Column(name="CUSTOMER_NAME", type="string", nullable=false, length=340)
     *
     */
    protected $customerName;
    public function getCustomerName(){
        return $this->customerName;
    }
    public function setCustomerName($customerName){
        $this->customerName = $customerName;
    }

    // origSystemCustomerRef generic STRING template*/
    /**
     * @Column(name="ORIG_SYSTEM_CUSTOMER_REF", type="string", nullable=false, length=240)
     *
     */
    protected $origSystemCustomerRef;
    public function getOrigSystemCustomerRef(){
        return $this->origSystemCustomerRef;
    }
    public function setOrigSystemCustomerRef($origSystemCustomerRef){
        $this->origSystemCustomerRef = $origSystemCustomerRef;
    }

    // origSystemAdressReference generic STRING template*/
    /**
     * @Column(name="ORIG_SYSTEM_ADRESS_REFERENCE", type="string", nullable=false, length=30)
     *
     */
    protected $origSystemAdressReference;
    public function getOrigSystemAdressReference(){
        return $this->origSystemAdressReference;
    }
    public function setOrigSystemAdressReference($origSystemAdressReference){
        $this->origSystemAdressReference = $origSystemAdressReference;
    }

    // siteUseCode generic STRING template*/
    /**
     * @Column(name="SITE_USE_CODE", type="string", nullable=false, length=30)
     *
     */
    protected $siteUseCode;
    public function getSiteUseCode(){
        return $this->siteUseCode;
    }
    public function setSiteUseCode($siteUseCode){
        $this->siteUseCode = $siteUseCode;
    }

    /** primarySiteUseFlag bit template*/
    /**
     * @Column(name="PRIMARY_SITE_USE_FLAG", type="boolean", nullable=false)
     */
    protected $primarySiteUseFlag;
    public function getPrimarySiteUseFlag(){
        return $this->primarySiteUseFlag;
    }
    public function setPrimarySiteUseFlag($primarySiteUseFlag){
        $this->primarySiteUseFlag = $primarySiteUseFlag;
    }

    // location generic STRING template*/
    /**
     * @Column(name="LOCATION", type="string", nullable=true, length=40)
     *
     */
    protected $location;
    public function getLocation(){
        return $this->location;
    }
    public function setLocation($location){
        $this->location = $location;
    }

    /** billToOrigAddressRef bit template*/
    /**
     * @Column(name="BILL_TO_ORIG_ADDRESS_REF", type="boolean", nullable=true)
     */
    protected $billToOrigAddressRef;
    public function getBillToOrigAddressRef(){
        return $this->billToOrigAddressRef;
    }
    public function setBillToOrigAddressRef($billToOrigAddressRef){
        $this->billToOrigAddressRef = $billToOrigAddressRef;
    }

    // addressLine1 generic STRING template*/
    /**
     * @Column(name="ADDRESS_LINE1", type="string", nullable=false, length=240)
     *
     */
    protected $addressLine1;
    public function getAddressLine1(){
        return $this->addressLine1;
    }
    public function setAddressLine1($addressLine1){
        $this->addressLine1 = $addressLine1;
    }

    // addressLine2 generic STRING template*/
    /**
     * @Column(name="ADDRESS_LINE2", type="string", nullable=true, length=240)
     *
     */
    protected $addressLine2;
    public function getAddressLine2(){
        return $this->addressLine2;
    }
    public function setAddressLine2($addressLine2){
        $this->addressLine2 = $addressLine2;
    }

    // addressLine3 generic STRING template*/
    /**
     * @Column(name="ADDRESS_LINE3", type="string", nullable=true, length=240)
     *
     */
    protected $addressLine3;
    public function getAddressLine3(){
        return $this->addressLine3;
    }
    public function setAddressLine3($addressLine3){
        $this->addressLine3 = $addressLine3;
    }

    // addressLine4 generic STRING template*/
    /**
     * @Column(name="ADDRESS_LINE4", type="string", nullable=true, length=240)
     *
     */
    protected $addressLine4;
    public function getAddressLine4(){
        return $this->addressLine4;
    }
    public function setAddressLine4($addressLine4){
        $this->addressLine4 = $addressLine4;
    }

    // addressLinesPhonetic generic STRING template*/
    /**
     * @Column(name="ADDRESS_LINES_PHONETIC", type="string", nullable=true, length=560)
     *
     */
    protected $addressLinesPhonetic;
    public function getAddressLinesPhonetic(){
        return $this->addressLinesPhonetic;
    }
    public function setAddressLinesPhonetic($addressLinesPhonetic){
        $this->addressLinesPhonetic = $addressLinesPhonetic;
    }

    // city generic STRING template*/
    /**
     * @Column(name="CITY", type="string", nullable=true, length=60)
     *
     */
    protected $city;
    public function getCity(){
        return $this->city;
    }
    public function setCity($city){
        $this->city = $city;
    }

    // province generic STRING template*/
    /**
     * @Column(name="PROVINCE", type="string", nullable=true, length=60)
     *
     */
    protected $province;
    public function getProvince(){
        return $this->province;
    }
    public function setProvince($province){
        $this->province = $province;
    }

    // state generic STRING template*/
    /**
     * @Column(name="STATE", type="string", nullable=true, length=60)
     *
     */
    protected $state;
    public function getState(){
        return $this->state;
    }
    public function setState($state){
        $this->state = $state;
    }

    // county generic STRING template*/
    /**
     * @Column(name="COUNTY", type="string", nullable=true, length=60)
     *
     */
    protected $county;
    public function getCounty(){
        return $this->county;
    }
    public function setCounty($county){
        $this->county = $county;
    }

    // postalCode generic STRING template*/
    /**
     * @Column(name="POSTAL_CODE", type="string", nullable=true, length=60)
     *
     */
    protected $postalCode;
    public function getPostalCode(){
        return $this->postalCode;
    }
    public function setPostalCode($postalCode){
        $this->postalCode = $postalCode;
    }

    // addressCountry generic STRING template*/
    /**
     * @Column(name="ADDRESS_COUNTRY", type="string", nullable=false, length=60)
     *
     */
    protected $addressCountry;
    public function getAddressCountry(){
        return $this->addressCountry;
    }
    public function setAddressCountry($addressCountry){
        $this->addressCountry = $addressCountry;
    }

    // recevablesAccount generic STRING template*/
    /**
     * @Column(name="RECEVABLES_ACCOUNT", type="string", nullable=true, length=150)
     *
     */
    protected $recevablesAccount;
    public function getRecevablesAccount(){
        return $this->recevablesAccount;
    }
    public function setRecevablesAccount($recevablesAccount){
        $this->recevablesAccount = $recevablesAccount;
    }

    // revenueAccount generic STRING template*/
    /**
     * @Column(name="REVENUE_ACCOUNT", type="string", nullable=true, length=150)
     *
     */
    protected $revenueAccount;
    public function getRevenueAccount(){
        return $this->revenueAccount;
    }
    public function setRevenueAccount($revenueAccount){
        $this->revenueAccount = $revenueAccount;
    }

    // taxAccount generic STRING template*/
    /**
     * @Column(name="TAX_ACCOUNT", type="string", nullable=true, length=150)
     *
     */
    protected $taxAccount;
    public function getTaxAccount(){
        return $this->taxAccount;
    }
    public function setTaxAccount($taxAccount){
        $this->taxAccount = $taxAccount;
    }

    // freightAccount generic STRING template*/
    /**
     * @Column(name="FREIGHT_ACCOUNT", type="string", nullable=true, length=150)
     *
     */
    protected $freightAccount;
    public function getFreightAccount(){
        return $this->freightAccount;
    }
    public function setFreightAccount($freightAccount){
        $this->freightAccount = $freightAccount;
    }

    // clearingAccount generic STRING template*/
    /**
     * @Column(name="CLEARING_ACCOUNT", type="string", nullable=true, length=150)
     *
     */
    protected $clearingAccount;
    public function getClearingAccount(){
        return $this->clearingAccount;
    }
    public function setClearingAccount($clearingAccount){
        $this->clearingAccount = $clearingAccount;
    }

    // unbilledReceivablesAccount generic STRING template*/
    /**
     * @Column(name="UNBILLED_RECEIVABLES_ACCOUNT", type="string", nullable=true, length=150)
     *
     */
    protected $unbilledReceivablesAccount;
    public function getUnbilledReceivablesAccount(){
        return $this->unbilledReceivablesAccount;
    }
    public function setUnbilledReceivablesAccount($unbilledReceivablesAccount){
        $this->unbilledReceivablesAccount = $unbilledReceivablesAccount;
    }

    // unearnedRevenueAccount generic STRING template*/
    /**
     * @Column(name="UNEARNED_REVENUE_ACCOUNT", type="string", nullable=true, length=150)
     *
     */
    protected $unearnedRevenueAccount;
    public function getUnearnedRevenueAccount(){
        return $this->unearnedRevenueAccount;
    }
    public function setUnearnedRevenueAccount($unearnedRevenueAccount){
        $this->unearnedRevenueAccount = $unearnedRevenueAccount;
    }

    // paymentMethodName generic STRING template*/
    /**
     * @Column(name="PAYMENT_METHOD_NAME", type="string", nullable=true, length=30)
     *
     */
    protected $paymentMethodName;
    public function getPaymentMethodName(){
        return $this->paymentMethodName;
    }
    public function setPaymentMethodName($paymentMethodName){
        $this->paymentMethodName = $paymentMethodName;
    }

    /** primaryFlag bit template*/
    /**
     * @Column(name="PRIMARY_FLAG", type="boolean", nullable=true)
     */
    protected $primaryFlag;
    public function getPrimaryFlag(){
        return $this->primaryFlag;
    }
    public function setPrimaryFlag($primaryFlag){
        $this->primaryFlag = $primaryFlag;
    }

    // customerProfile generic STRING template*/
    /**
     * @Column(name="CUSTOMER_PROFILE", type="string", nullable=false, length=30)
     *
     */
    protected $customerProfile;
    public function getCustomerProfile(){
        return $this->customerProfile;
    }
    public function setCustomerProfile($customerProfile){
        $this->customerProfile = $customerProfile;
    }

    // siteUseTaxReference generic STRING template*/
    /**
     * @Column(name="SITE_USE_TAX_REFERENCE", type="string", nullable=true, length=50)
     *
     */
    protected $siteUseTaxReference;
    public function getSiteUseTaxReference(){
        return $this->siteUseTaxReference;
    }
    public function setSiteUseTaxReference($siteUseTaxReference){
        $this->siteUseTaxReference = $siteUseTaxReference;
    }

    // timeZone generic STRING template*/
    /**
     * @Column(name="TIME_ZONE", type="string", nullable=false, length=50)
     *
     */
    protected $timeZone;
    public function getTimeZone(){
        return $this->timeZone;
    }
    public function setTimeZone($timeZone){
        $this->timeZone = $timeZone;
    }

    // language generic STRING template*/
    /**
     * @Column(name="LANGUAGE", type="string", nullable=false, length=3)
     *
     */
    protected $language;
    public function getLanguage(){
        return $this->language;
    }
    public function setLanguage($language){
        $this->language = $language;
    }

    // gdfAddressAttribute8 generic STRING template*/
    /**
     * @Column(name="GDF_ADDRESS_ATTRIBUTE8", type="string", nullable=true, length=30)
     *
     */
    protected $gdfAddressAttribute8;
    public function getGdfAddressAttribute8(){
        return $this->gdfAddressAttribute8;
    }
    public function setGdfAddressAttribute8($gdfAddressAttribute8){
        $this->gdfAddressAttribute8 = $gdfAddressAttribute8;
    }

    // gdfSiteUseAttribute9 generic STRING template*/
    /**
     * @Column(name="GDF_SITE_USE_ATTRIBUTE9", type="string", nullable=true, length=150)
     *
     */
    protected $gdfSiteUseAttribute9;
    public function getGdfSiteUseAttribute9(){
        return $this->gdfSiteUseAttribute9;
    }
    public function setGdfSiteUseAttribute9($gdfSiteUseAttribute9){
        $this->gdfSiteUseAttribute9 = $gdfSiteUseAttribute9;
    }

    // orgName generic STRING template*/
    /**
     * @Column(name="ORG_NAME", type="string", nullable=false, length=60)
     *
     */
    protected $orgName;
    public function getOrgName(){
        return $this->orgName;
    }
    public function setOrgName($orgName){
        $this->orgName = $orgName;
    }

    // gdfAddressAttribute9 generic STRING template*/
    /**
     * @Column(name="GDF_ADDRESS_ATTRIBUTE9", type="string", nullable=true, length=150)
     *
     */
    protected $gdfAddressAttribute9;
    public function getGdfAddressAttribute9(){
        return $this->gdfAddressAttribute9;
    }
    public function setGdfAddressAttribute9($gdfAddressAttribute9){
        $this->gdfAddressAttribute9 = $gdfAddressAttribute9;
    }

    // gdfSiteUseAttribute8 generic STRING template*/
    /**
     * @Column(name="GDF_SITE_USE_ATTRIBUTE8", type="string", nullable=true, length=30)
     *
     */
    protected $gdfSiteUseAttribute8;
    public function getGdfSiteUseAttribute8(){
        return $this->gdfSiteUseAttribute8;
    }
    public function setGdfSiteUseAttribute8($gdfSiteUseAttribute8){
        $this->gdfSiteUseAttribute8 = $gdfSiteUseAttribute8;
    }

    // economicActivity generic STRING template*/
    /**
     * @Column(name="CO_EXT_ATTRIBUTE1", type="string", nullable=true, length=15)
     *
     */
    protected $economicActivity;
    public function getEconomicActivity(){
        return $this->economicActivity;
    }
    public function setEconomicActivity($economicActivity){
        $this->economicActivity = $economicActivity;
    }

    // paymentTermName generic STRING template*/
    /**
     * @Column(name="PAYMENT_TERM_NAME", type="string", nullable=true, length=15)
     *
     */
    protected $paymentTermName;
    public function getPaymentTermName(){
        return $this->paymentTermName;
    }
    public function setPaymentTermName($paymentTermName){
        $this->paymentTermName = $paymentTermName;
    }

    // conceptCode generic STRING template*/
    /**
     * @Column(name="CONCEPT_CODE", type="string", nullable=true, length=30)
     *
     */
    protected $conceptCode;
    public function getConceptCode(){
        return $this->conceptCode;
    }
    public function setConceptCode($conceptCode){
        $this->conceptCode = $conceptCode;
    }

    // giro generic STRING template*/
    /**
     * @Column(name="GIRO", type="string", nullable=true, length=150)
     *
     */
    protected $giro;
    public function getGiro(){
        return $this->giro;
    }
    public function setGiro($giro){
        $this->giro = $giro;
    }

}

