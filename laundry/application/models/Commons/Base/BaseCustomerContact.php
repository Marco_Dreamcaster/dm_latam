<?php
namespace Commons\Base;

use Commons\RAWBase;

class BaseCustomerContact extends RAWBase
{
    public function __construct()
    {
    }

    // origSystemCustomerRef generic STRING template*/
    /**
     * @Column(name="ORIG_SYSTEM_CUSTOMER_REF", type="string", nullable=false, length=240)
     *
     */
    protected $origSystemCustomerRef;
    public function getOrigSystemCustomerRef(){
        return $this->origSystemCustomerRef;
    }
    public function setOrigSystemCustomerRef($origSystemCustomerRef){
        $this->origSystemCustomerRef = $origSystemCustomerRef;
    }

    // origSystemAdressReference generic STRING template*/
    /**
     * @Column(name="ORIG_SYSTEM_ADRESS_REFERENCE", type="string", nullable=true, length=30)
     *
     */
    protected $origSystemAdressReference;
    public function getOrigSystemAdressReference(){
        return $this->origSystemAdressReference;
    }
    public function setOrigSystemAdressReference($origSystemAdressReference){
        $this->origSystemAdressReference = $origSystemAdressReference;
    }

    // origSystemContactRef generic STRING template*/
    /**
     * @Column(name="ORIG_SYSTEM_CONTACT_REF", type="string", nullable=false, length=30)
     *
     */
    protected $origSystemContactRef;
    public function getOrigSystemContactRef(){
        return $this->origSystemContactRef;
    }
    public function setOrigSystemContactRef($origSystemContactRef){
        $this->origSystemContactRef = $origSystemContactRef;
    }

    // origSystemTelephoneRef generic STRING template*/
    /**
     * @Column(name="ORIG_SYSTEM_TELEPHONE_REF", type="string", nullable=false, length=240)
     *
     */
    protected $origSystemTelephoneRef;
    public function getOrigSystemTelephoneRef(){
        return $this->origSystemTelephoneRef;
    }
    public function setOrigSystemTelephoneRef($origSystemTelephoneRef){
        $this->origSystemTelephoneRef = $origSystemTelephoneRef;
    }

    // contactFirstName generic STRING template*/
    /**
     * @Column(name="CONTACT_FIRST_NAME", type="string", nullable=false, length=60)
     *
     */
    protected $contactFirstName;
    public function getContactFirstName(){
        return $this->contactFirstName;
    }
    public function setContactFirstName($contactFirstName){
        $this->contactFirstName = $contactFirstName;
    }

    // contactLastName generic STRING template*/
    /**
     * @Column(name="CONTACT_LAST_NAME", type="string", nullable=false, length=60)
     *
     */
    protected $contactLastName;
    public function getContactLastName(){
        return $this->contactLastName;
    }
    public function setContactLastName($contactLastName){
        $this->contactLastName = $contactLastName;
    }

    // telephoneType generic STRING template*/
    /**
     * @Column(name="TELEPHONE_TYPE", type="string", nullable=false, length=30)
     *
     */
    protected $telephoneType;
    public function getTelephoneType(){
        return $this->telephoneType;
    }
    public function setTelephoneType($telephoneType){
        $this->telephoneType = $telephoneType;
    }

    // phoneCountryCode generic STRING template*/
    /**
     * @Column(name="PHONE_COUNTRY_CODE", type="string", nullable=true, length=10)
     *
     */
    protected $phoneCountryCode;
    public function getPhoneCountryCode(){
        return $this->phoneCountryCode;
    }
    public function setPhoneCountryCode($phoneCountryCode){
        $this->phoneCountryCode = $phoneCountryCode;
    }

    // telephoneAreaCode generic STRING template*/
    /**
     * @Column(name="TELEPHONE_AREA_CODE", type="string", nullable=true, length=10)
     *
     */
    protected $telephoneAreaCode;
    public function getTelephoneAreaCode(){
        return $this->telephoneAreaCode;
    }
    public function setTelephoneAreaCode($telephoneAreaCode){
        $this->telephoneAreaCode = $telephoneAreaCode;
    }

    // telephone generic STRING template*/
    /**
     * @Column(name="TELEPHONE", type="string", nullable=true, length=25)
     *
     */
    protected $telephone;
    public function getTelephone(){
        return $this->telephone;
    }
    public function setTelephone($telephone){
        $this->telephone = $telephone;
    }

    // telephoneExtension generic STRING template*/
    /**
     * @Column(name="TELEPHONE_EXTENSION", type="string", nullable=true, length=20)
     *
     */
    protected $telephoneExtension;
    public function getTelephoneExtension(){
        return $this->telephoneExtension;
    }
    public function setTelephoneExtension($telephoneExtension){
        $this->telephoneExtension = $telephoneExtension;
    }

    // emailAddress generic STRING template*/
    /**
     * @Column(name="EMAIL_ADDRESS", type="string", nullable=true, length=240)
     *
     */
    protected $emailAddress;
    public function getEmailAddress(){
        return $this->emailAddress;
    }
    public function setEmailAddress($emailAddress){
        $this->emailAddress = $emailAddress;
    }

    // jobTitle generic STRING template*/
    /**
     * @Column(name="JOB_TITLE", type="string", nullable=true, length=30)
     *
     */
    protected $jobTitle;
    public function getJobTitle(){
        return $this->jobTitle;
    }
    public function setJobTitle($jobTitle){
        $this->jobTitle = $jobTitle;
    }

    // contactAttribute1 generic STRING template*/
    /**
     * @Column(name="CONTACT_ATTRIBUTE1", type="string", nullable=true, length=10)
     *
     */
    protected $contactAttribute1;
    public function getContactAttribute1(){
        return $this->contactAttribute1;
    }
    public function setContactAttribute1($contactAttribute1){
        $this->contactAttribute1 = $contactAttribute1;
    }

    // orgName generic STRING template*/
    /**
     * @Column(name="ORG_NAME", type="string", nullable=false, length=60)
     *
     */
    protected $orgName;
    public function getOrgName(){
        return $this->orgName;
    }
    public function setOrgName($orgName){
        $this->orgName = $orgName;
    }

}

