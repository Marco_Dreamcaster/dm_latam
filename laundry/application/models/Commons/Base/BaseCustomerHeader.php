<?php
namespace Commons\Base;

use Commons\RAWBase;

class BaseCustomerHeader extends RAWBase
{
    public function __construct()
    {
    }

    /**
     * @Column(name="SIGLA_PRIMARY_KEY", type="string", length=60, nullable=true)
     *
     */
    protected $siglaPrimaryKey;

    /**
     * @Column(name="CUSTOMER_NAME", type="string", nullable=false, length=340)
     *
     */
    protected $customerName;

    /**
     * @Column(name="ORIG_SYSTEM_PARTY_REF", type="string", nullable=false, length=240)
     *
     */
    protected $origSystemPartyRef;

    /**
     * @Column(name="ORIG_SYSTEM_CUSTOMER_REF", type="string", nullable=false, length=240)
     *
     */
    protected $origSystemCustomerRef;

    /**
     * @Column(name="CUSTOMER_TYPE", type="string", nullable=false, length=10)
     *
     */
    protected $customerType;

    /**
     * @Column(name="TAX_RETENTION ", type="string", nullable=true, length=20)
     *
     */
    protected $taxRetention;

    /**
     * @Column(name="TAX_PAYER_ID", type="string", nullable=true, length=20)
     *
     */
    protected $taxPayerId;

    /**
     * @Column(name="CUSTOMER_PROFILE", type="string", nullable=false, length=30)
     *
     */
    protected $customerProfile;

    // paymentMethodName generic STRING template*/
    /**
     * @Column(name="PAYMENT_METHOD_NAME", type="string", nullable=true, length=30)
     *
     */
    protected $paymentMethodName;

    /**
     * @Column(name="PRIMARY_FLAG", type="boolean", nullable=true)
     */
    protected $primaryFlag;

    /**
     * @Column(name="GLOBAL_FLAG", type="boolean", nullable=false)
     */
    protected $globalFlag;

    /**
     * @Column(name="GLOBAL_ATTRIBUTE9", type="string", nullable=true, length=80)
     *
     */
    protected $globalAttribute9;

    /**
     * @Column(name="GLOBAL_ATTRIBUTE10", type="string", nullable=true, length=80)
     *
     */
    protected $globalAttribute10;

    /**
     * @Column(name="GLOBAL_ATTRIBUTE12", type="string", nullable=true, length=10)
     *
     */
    protected $globalAttribute12;

    /**
     * @Column(name="ORG_NAME", type="string", nullable=false, length=60)
     *
     */
    protected $orgName;

    /**
     * @Column(name="CLASSIFICATION", type="string", nullable=true, length=60)
     *
     */
    protected $classification;

    /**
     * @Column(name="SEGMENTATION", type="string", nullable=true, length=60)
     *
     */
    protected $segmentation;

    /**
     * @Column(name="ALIASES", type="string", nullable=true, length=3000)
     *
     */
    protected $aliases;

    /**
     * @Column(name="FISCAL_CLASS_CATEGORY", type="string", nullable=true, length=30)
     *
     */
    protected $fiscalClassCategory;

    /**
     * @Column(name="FISCAL_CLASS_CODE", type="string", nullable=true, length=30)
     *
     */
    protected $fiscalClassCode;

    /**
     * @Column(name="FISCAL_CLASS_START_DATE", type="datetime", nullable=true)
     *
     */
    protected $fiscalClassStartDate;

    /**
     * @Column(name="CO_EXT_ATTRIBUTE1", type="string", nullable=true, length=3)
     *
     */
    protected $CODocumentType;

    /**
     * @Column(name="CO_EXT_ATTRIBUTE2", type="string", nullable=true, length=40)
     *
     */
    protected $COTaxPayerId;

    /**
     * @Column(name="CO_EXT_ATTRIBUTE3", type="string", nullable=true, length=30)
     *
     */
    protected $nature;

    /**
     * @Column(name="CO_EXT_ATTRIBUTE4", type="string", nullable=true, length=80)
     *
     */
    protected $regimen;

    /**
     * @Column(name="SPECIAL_CUSTOMER", type="boolean", nullable=false)
     */
    protected $specialCustomer;

    /**
     * @Column(name="BILL_TO_MIGRATED_REF", type="string", nullable=true, length=80)
     */
    protected $billToMigratedRef;

    /**
     * @Column(name="SHIP_TO_MIGRATED_REF", type="string", nullable=true, length=80)
     */
    protected $shipToMigratedRef;

    /**
     * @Column(name="CLASS_CODE", type="string", nullable=true, length=30)
     */
    protected $classCode;

    /**
     * @Column(name="PAYMENT_TERM_NAME", type="string", nullable=true, length=15)
     *
     */
    protected $paymentTermName;

    /**
     * @Column(name="CUSTOMER_CLASS_CODE ", type="string", length=30, nullable=true)
     *
     */
    protected $customerClassCode;

    /**
     * @Column(name="ORDER_NUMBER_GT", type="string", nullable=true, length=150)
     *
     */
    protected $orderNumberGt;

    /**
     * @Column(name="REGISTER_NUMBER_GT", type="string", nullable=true, length=150)
     *
     */
    protected $registerNumberGt;

    /**
     * @Column(name="CONTRIBUTOR_REGISTRATION_SV", type="string", nullable=true, length=150)
     *
     */
    protected $contributorRegistrationSv;

    /**
     * @Column(name="DOCUMENT_TYPE_SV", type="string", nullable=true, length=20)
     *
     */
    protected $documentTypeSv;

    /**
     * @Column(name="PERSON_CATEGORY_PE", type="string", nullable=true, length=15)
     *
     */
    protected $personCategoryPE;

    /**
     * @Column(name="PERSON_TYPE_PE", type="string", nullable=true, length=2)
     *
     */
    protected $personTypePE;

    /**
     * @Column(name="DOCUMENT_TYPE_PE", type="string", nullable=true, length=2)
     *
     */
    protected $documentTypePE;

    /**
     * @Column(name="DOCUMENT_NUMBER_PE", type="string", nullable=true, length=150)
     *
     */
    protected $documentNumberPE;

    /**
     * @Column(name="LAST_NAME_PE", type="string", nullable=true, length=20)
     *
     */
    protected $lastNamePE;

    /**
     * @Column(name="SECOND_LAST_NAME_PE", type="string", nullable=true, length=20)
     *
     */
    protected $secondLastNamePE;

    /**
     * @Column(name="FIRST_NAME_PE", type="string", nullable=true, length=20)
     *
     */
    protected $firstNamePE;

    /**
     * @Column(name="MIDDLE_NAME_PE", type="string", nullable=true, length=20)
     *
     */
    protected $middleNamePE;

    /**
     * @Column(name="ORIGIN_ALL", type="string", nullable=true, length=30)
     *
     */
    protected $originAll;

    /**
     * @Column(name="NATURE_ALL", type="string", nullable=true, length=30)
     *
     */
    protected $natureAll;


    /**
     * @Column(name="DOCUMENT_TYPE_UY", type="string", nullable=true, length=10)
     *
     */
    protected $documentTypeUY;

    /**
     * @Column(name="PERCEPTION_AGENT_PE", type="string", nullable=true, length=2)
     *
     */
    protected $perceptionAgentPE;

    // GETTERS - SETTERS

    /**
     * @return mixed
     */
    public function getDocumentTypeUY()
    {
        return $this->documentTypeUY;
    }

    /**
     * @param mixed $documentTypeUY
     */
    public function setDocumentTypeUY($documentTypeUY)
    {
        $this->documentTypeUY = $documentTypeUY;
    }

    /**
     * @return mixed
     */
    public function getPerceptionAgentPE()
    {
        return $this->perceptionAgentPE;
    }

    /**
     * @param mixed $perceptionAgentPE
     */
    public function setPerceptionAgentPE($perceptionAgentPE)
    {
        $this->perceptionAgentPE = $perceptionAgentPE;
    }



    /**
     * @return mixed
     */
    public function getOriginAll()
    {
        return $this->originAll;
    }

    /**
     * @param mixed $originAll
     */
    public function setOriginAll($originAll)
    {
        $this->originAll = $originAll;
    }

    /**
     * @return mixed
     */
    public function getNatureAll()
    {
        return $this->natureAll;
    }

    /**
     * @param mixed $natureAll
     */
    public function setNatureAll($natureAll)
    {
        $this->natureAll = $natureAll;
    }




    /**
     * @return mixed
     */
    public function getSiglaPrimaryKey()
    {
        return $this->siglaPrimaryKey;
    }

    /**
     * @param mixed $siglaPrimaryKey
     * @return BaseCustomerHeader
     */
    public function setSiglaPrimaryKey($siglaPrimaryKey)
    {
        $this->siglaPrimaryKey = $siglaPrimaryKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * @param mixed $customerName
     * @return BaseCustomerHeader
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrigSystemPartyRef()
    {
        return $this->origSystemPartyRef;
    }

    /**
     * @param mixed $origSystemPartyRef
     * @return BaseCustomerHeader
     */
    public function setOrigSystemPartyRef($origSystemPartyRef)
    {
        $this->origSystemPartyRef = $origSystemPartyRef;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrigSystemCustomerRef()
    {
        return $this->origSystemCustomerRef;
    }

    /**
     * @param mixed $origSystemCustomerRef
     * @return BaseCustomerHeader
     */
    public function setOrigSystemCustomerRef($origSystemCustomerRef)
    {
        $this->origSystemCustomerRef = $origSystemCustomerRef;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerType()
    {
        return $this->customerType;
    }

    /**
     * @param mixed $customerType
     * @return BaseCustomerHeader
     */
    public function setCustomerType($customerType)
    {
        $this->customerType = $customerType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTaxRetention()
    {
        return $this->taxRetention;
    }

    /**
     * @param mixed $taxRetention
     * @return BaseCustomerHeader
     */
    public function setTaxRetention($taxRetention)
    {
        $this->taxRetention = $taxRetention;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTaxPayerId()
    {
        return $this->taxPayerId;
    }

    /**
     * @param mixed $taxPayerId
     * @return BaseCustomerHeader
     */
    public function setTaxPayerId($taxPayerId)
    {
        $this->taxPayerId = $taxPayerId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerProfile()
    {
        return $this->customerProfile;
    }

    /**
     * @param mixed $customerProfile
     * @return BaseCustomerHeader
     */
    public function setCustomerProfile($customerProfile)
    {
        $this->customerProfile = $customerProfile;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethodName()
    {
        return $this->paymentMethodName;
    }

    /**
     * @param mixed $paymentMethodName
     * @return BaseCustomerHeader
     */
    public function setPaymentMethodName($paymentMethodName)
    {
        $this->paymentMethodName = $paymentMethodName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrimaryFlag()
    {
        return $this->primaryFlag;
    }

    /**
     * @param mixed $primaryFlag
     * @return BaseCustomerHeader
     */
    public function setPrimaryFlag($primaryFlag)
    {
        $this->primaryFlag = $primaryFlag;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGlobalFlag()
    {
        return $this->globalFlag;
    }

    /**
     * @param mixed $globalFlag
     * @return BaseCustomerHeader
     */
    public function setGlobalFlag($globalFlag)
    {
        $this->globalFlag = $globalFlag;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGlobalAttribute9()
    {
        return $this->globalAttribute9;
    }

    /**
     * @param mixed $globalAttribute9
     * @return BaseCustomerHeader
     */
    public function setGlobalAttribute9($globalAttribute9)
    {
        $this->globalAttribute9 = $globalAttribute9;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGlobalAttribute10()
    {
        return $this->globalAttribute10;
    }

    /**
     * @param mixed $globalAttribute10
     * @return BaseCustomerHeader
     */
    public function setGlobalAttribute10($globalAttribute10)
    {
        $this->globalAttribute10 = $globalAttribute10;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGlobalAttribute12()
    {
        return $this->globalAttribute12;
    }

    /**
     * @param mixed $globalAttribute12
     * @return BaseCustomerHeader
     */
    public function setGlobalAttribute12($globalAttribute12)
    {
        $this->globalAttribute12 = $globalAttribute12;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrgName()
    {
        return $this->orgName;
    }

    /**
     * @param mixed $orgName
     * @return BaseCustomerHeader
     */
    public function setOrgName($orgName)
    {
        $this->orgName = $orgName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClassification()
    {
        return $this->classification;
    }

    /**
     * @param mixed $classification
     * @return BaseCustomerHeader
     */
    public function setClassification($classification)
    {
        $this->classification = $classification;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSegmentation()
    {
        return $this->segmentation;
    }

    /**
     * @param mixed $segmentation
     * @return BaseCustomerHeader
     */
    public function setSegmentation($segmentation)
    {
        $this->segmentation = $segmentation;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAliases()
    {
        return $this->aliases;
    }

    /**
     * @param mixed $aliases
     * @return BaseCustomerHeader
     */
    public function setAliases($aliases)
    {
        $this->aliases = $aliases;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFiscalClassCategory()
    {
        return $this->fiscalClassCategory;
    }

    /**
     * @param mixed $fiscalClassCategory
     * @return BaseCustomerHeader
     */
    public function setFiscalClassCategory($fiscalClassCategory)
    {
        $this->fiscalClassCategory = $fiscalClassCategory;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFiscalClassCode()
    {
        return $this->fiscalClassCode;
    }

    /**
     * @param mixed $fiscalClassCode
     * @return BaseCustomerHeader
     */
    public function setFiscalClassCode($fiscalClassCode)
    {
        $this->fiscalClassCode = $fiscalClassCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFiscalClassStartDate()
    {
        return $this->fiscalClassStartDate;
    }

    /**
     * @param mixed $fiscalClassStartDate
     * @return BaseCustomerHeader
     */
    public function setFiscalClassStartDate($fiscalClassStartDate)
    {
        $this->fiscalClassStartDate = $fiscalClassStartDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCODocumentType()
    {
        return $this->CODocumentType;
    }

    /**
     * @param mixed $CODocumentType
     * @return BaseCustomerHeader
     */
    public function setCODocumentType($CODocumentType)
    {
        $this->CODocumentType = $CODocumentType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCOTaxPayerId()
    {
        return $this->COTaxPayerId;
    }

    /**
     * @param mixed $COTaxPayerId
     * @return BaseCustomerHeader
     */
    public function setCOTaxPayerId($COTaxPayerId)
    {
        $this->COTaxPayerId = $COTaxPayerId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNature()
    {
        return $this->nature;
    }

    /**
     * @param mixed $nature
     * @return BaseCustomerHeader
     */
    public function setNature($nature)
    {
        $this->nature = $nature;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegimen()
    {
        return $this->regimen;
    }

    /**
     * @param mixed $regimen
     * @return BaseCustomerHeader
     */
    public function setRegimen($regimen)
    {
        $this->regimen = $regimen;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpecialCustomer()
    {
        return $this->specialCustomer;
    }

    /**
     * @param mixed $specialCustomer
     * @return BaseCustomerHeader
     */
    public function setSpecialCustomer($specialCustomer)
    {
        $this->specialCustomer = $specialCustomer;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBillToMigratedRef()
    {
        return $this->billToMigratedRef;
    }

    /**
     * @param mixed $billToMigratedRef
     * @return BaseCustomerHeader
     */
    public function setBillToMigratedRef($billToMigratedRef)
    {
        $this->billToMigratedRef = $billToMigratedRef;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getShipToMigratedRef()
    {
        return $this->shipToMigratedRef;
    }

    /**
     * @param mixed $shipToMigratedRef
     * @return BaseCustomerHeader
     */
    public function setShipToMigratedRef($shipToMigratedRef)
    {
        $this->shipToMigratedRef = $shipToMigratedRef;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClassCode()
    {
        return $this->classCode;
    }

    /**
     * @param mixed $classCode
     * @return BaseCustomerHeader
     */
    public function setClassCode($classCode)
    {
        $this->classCode = $classCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentTermName()
    {
        return $this->paymentTermName;
    }

    /**
     * @param mixed $paymentTermName
     * @return BaseCustomerHeader
     */
    public function setPaymentTermName($paymentTermName)
    {
        $this->paymentTermName = $paymentTermName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerClassCode()
    {
        return $this->customerClassCode;
    }

    /**
     * @param mixed $customerClassCode
     * @return BaseCustomerHeader
     */
    public function setCustomerClassCode($customerClassCode)
    {
        $this->customerClassCode = $customerClassCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderNumberGt()
    {
        return $this->orderNumberGt;
    }

    /**
     * @param mixed $orderNumberGt
     * @return BaseCustomerHeader
     */
    public function setOrderNumberGt($orderNumberGt)
    {
        $this->orderNumberGt = $orderNumberGt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegisterNumberGt()
    {
        return $this->registerNumberGt;
    }

    /**
     * @param mixed $registerNumberGt
     * @return BaseCustomerHeader
     */
    public function setRegisterNumberGt($registerNumberGt)
    {
        $this->registerNumberGt = $registerNumberGt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContributorRegistrationSv()
    {
        return $this->contributorRegistrationSv;
    }

    /**
     * @param mixed $contributorRegistrationSv
     * @return BaseCustomerHeader
     */
    public function setContributorRegistrationSv($contributorRegistrationSv)
    {
        $this->contributorRegistrationSv = $contributorRegistrationSv;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDocumentTypeSv()
    {
        return $this->documentTypeSv;
    }

    /**
     * @param mixed $documentTypeSv
     * @return BaseCustomerHeader
     */
    public function setDocumentTypeSv($documentTypeSv)
    {
        $this->documentTypeSv = $documentTypeSv;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPersonCategoryPE()
    {
        return $this->personCategoryPE;
    }

    /**
     * @param mixed $personCategoryPE
     * @return BaseCustomerHeader
     */
    public function setPersonCategoryPE($personCategoryPE)
    {
        $this->personCategoryPE = $personCategoryPE;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPersonTypePE()
    {
        return $this->personTypePE;
    }

    /**
     * @param mixed $personTypePE
     * @return BaseCustomerHeader
     */
    public function setPersonTypePE($personTypePE)
    {
        $this->personTypePE = $personTypePE;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDocumentTypePE()
    {
        return $this->documentTypePE;
    }

    /**
     * @param mixed $documentTypePE
     * @return BaseCustomerHeader
     */
    public function setDocumentTypePE($documentTypePE)
    {
        $this->documentTypePE = $documentTypePE;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDocumentNumberPE()
    {
        return $this->documentNumberPE;
    }

    /**
     * @param mixed $documentNumberPE
     * @return BaseCustomerHeader
     */
    public function setDocumentNumberPE($documentNumberPE)
    {
        $this->documentNumberPE = $documentNumberPE;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastNamePE()
    {
        return $this->lastNamePE;
    }

    /**
     * @param mixed $lastNamePE
     * @return BaseCustomerHeader
     */
    public function setLastNamePE($lastNamePE)
    {
        $this->lastNamePE = $lastNamePE;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSecondLastNamePE()
    {
        return $this->secondLastNamePE;
    }

    /**
     * @param mixed $secondLastNamePE
     * @return BaseCustomerHeader
     */
    public function setSecondLastNamePE($secondLastNamePE)
    {
        $this->secondLastNamePE = $secondLastNamePE;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstNamePE()
    {
        return $this->firstNamePE;
    }

    /**
     * @param mixed $firstNamePE
     * @return BaseCustomerHeader
     */
    public function setFirstNamePE($firstNamePE)
    {
        $this->firstNamePE = $firstNamePE;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMiddleNamePE()
    {
        return $this->middleNamePE;
    }

    /**
     * @param mixed $middleNamePE
     * @return BaseCustomerHeader
     */
    public function setMiddleNamePE($middleNamePE)
    {
        $this->middleNamePE = $middleNamePE;
        return $this;
    }




}

