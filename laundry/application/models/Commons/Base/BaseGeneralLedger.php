<?php
namespace Commons\Base;

use Commons\RAWBase;

class BaseGeneralLedger extends RAWBase
{
    public function __construct()
    {
    }

    // flexfieldSegments generic STRING template*/
    /**
     * @Column(name="FLEXFIELD_SEGMENTS", type="string", nullable=false, length=150)
     *
     */
    protected $flexfieldSegments;
    public function getFlexfieldSegments(){
        return $this->flexfieldSegments;
    }
    public function setFlexfieldSegments($flexfieldSegments){
        $this->flexfieldSegments = $flexfieldSegments;
    }

    // enteredDr NUMBER template
    /**
     * @Column(name="ENTERED_DR", type="float", nullable=true)
     *
     */
    protected $enteredDr;
    public function getEnteredDr(){
        return $this->enteredDr;
    }
    public function setEnteredDr($enteredDr){
        $this->enteredDr = $enteredDr;
    }

    // enteredCr NUMBER template
    /**
     * @Column(name="ENTERED_CR", type="float", nullable=true)
     *
     */
    protected $enteredCr;
    public function getEnteredCr(){
        return $this->enteredCr;
    }
    public function setEnteredCr($enteredCr){
        $this->enteredCr = $enteredCr;
    }

    // accountingDate generic STRING template*/
    /**
     * @Column(name="ACCOUNTING_DATE", type="string", nullable=false, length=11)
     *
     */
    protected $accountingDate;
    public function getAccountingDate(){
        return $this->accountingDate;
    }
    public function setAccountingDate($accountingDate){
        $this->accountingDate = $accountingDate;
    }

    // reference1 generic STRING template*/
    /**
     * @Column(name="REFERENCE1", type="string", nullable=false, length=100)
     *
     */
    protected $reference1;
    public function getReference1(){
        return $this->reference1;
    }
    public function setReference1($reference1){
        $this->reference1 = $reference1;
    }

    // reference2 generic STRING template*/
    /**
     * @Column(name="REFERENCE2", type="string", nullable=true, length=240)
     *
     */
    protected $reference2;
    public function getReference2(){
        return $this->reference2;
    }
    public function setReference2($reference2){
        $this->reference2 = $reference2;
    }

    // reference4 generic STRING template*/
    /**
     * @Column(name="REFERENCE4", type="string", nullable=false, length=100)
     *
     */
    protected $reference4;
    public function getReference4(){
        return $this->reference4;
    }
    public function setReference4($reference4){
        $this->reference4 = $reference4;
    }

    // reference5 generic STRING template*/
    /**
     * @Column(name="REFERENCE5", type="string", nullable=true, length=240)
     *
     */
    protected $reference5;
    public function getReference5(){
        return $this->reference5;
    }
    public function setReference5($reference5){
        $this->reference5 = $reference5;
    }

    // reference10 generic STRING template*/
    /**
     * @Column(name="REFERENCE10", type="string", nullable=true, length=240)
     *
     */
    protected $reference10;
    public function getReference10(){
        return $this->reference10;
    }
    public function setReference10($reference10){
        $this->reference10 = $reference10;
    }

    // currencyCode generic STRING template*/
    /**
     * @Column(name="CURRENCY_CODE", type="string", nullable=false, length=15)
     *
     */
    protected $currencyCode;
    public function getCurrencyCode(){
        return $this->currencyCode;
    }
    public function setCurrencyCode($currencyCode){
        $this->currencyCode = $currencyCode;
    }

    // orgName generic STRING template*/
    /**
     * @Column(name="ORG_NAME", type="string", nullable=false, length=60)
     *
     */
    protected $orgName;
    public function getOrgName(){
        return $this->orgName;
    }
    public function setOrgName($orgName){
        $this->orgName = $orgName;
    }

}

