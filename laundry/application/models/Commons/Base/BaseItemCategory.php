<?php
namespace Commons\Base;

use Commons\RAWBase;

class BaseItemCategory extends RAWBase
{
    public function __construct()
    {
    }

    // origItemNumber generic STRING template*/
    /**
     * @Column(name="ORIG_ITEM_NUMBER", type="string", nullable=false, length=255)
     *
     */
    protected $origItemNumber;
    public function getOrigItemNumber(){
        return $this->origItemNumber;
    }
    public function setOrigItemNumber($origItemNumber){
        $this->origItemNumber = $origItemNumber;
    }

    // itemNumber generic STRING template*/
    /**
     * @Column(name="ITEM_NUMBER", type="string", nullable=false, length=16)
     *
     */
    protected $itemNumber;
    public function getItemNumber(){
        return $this->itemNumber;
    }
    public function setItemNumber($itemNumber){
        $this->itemNumber = $itemNumber;
    }

    // categorySetName generic STRING template*/
    /**
     * @Column(name="CATEGORY_SET_NAME", type="string", nullable=true, length=30)
     *
     */
    protected $categorySetName;
    public function getCategorySetName(){
        return $this->categorySetName;
    }
    public function setCategorySetName($categorySetName){
        $this->categorySetName = $categorySetName;
    }

    // concatCategoryName generic STRING template*/
    /**
     * @Column(name="CONCAT_CATEGORY_NAME", type="string", nullable=true, length=80)
     *
     */
    protected $concatCategoryName;
    public function getConcatCategoryName(){
        return $this->concatCategoryName;
    }
    public function setConcatCategoryName($concatCategoryName){
        $this->concatCategoryName = $concatCategoryName;
    }

    // organizationCode generic STRING template*/
    /**
     * @Column(name="ORGANIZATION_CODE", type="string", nullable=true, length=3)
     *
     */
    protected $organizationCode;
    public function getOrganizationCode(){
        return $this->organizationCode;
    }
    public function setOrganizationCode($organizationCode){
        $this->organizationCode = $organizationCode;
    }

    // orgName generic STRING template*/
    /**
     * @Column(name="ORG_NAME", type="string", nullable=true, length=60)
     *
     */
    protected $orgName;
    public function getOrgName(){
        return $this->orgName;
    }
    public function setOrgName($orgName){
        $this->orgName = $orgName;
    }

}

