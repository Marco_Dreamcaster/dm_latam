<?php
namespace Commons\Base;

use Commons\RAWBase;

class BaseItemCrossRef extends RAWBase
{
    public function __construct()
    {
    }

    // origItemNumber generic STRING template*/
    /**
     * @Column(name="ORIG_ITEM_NUMBER", type="string", nullable=false, length=255)
     *
     */
    protected $origItemNumber;
    public function getOrigItemNumber(){
        return $this->origItemNumber;
    }
    public function setOrigItemNumber($origItemNumber){
        $this->origItemNumber = $origItemNumber;
    }

    // itemNumber generic STRING template*/
    /**
     * @Column(name="ITEM_NUMBER", type="string", nullable=false, length=16)
     *
     */
    protected $itemNumber;
    public function getItemNumber(){
        return $this->itemNumber;
    }
    public function setItemNumber($itemNumber){
        $this->itemNumber = $itemNumber;
    }

    // crossReferenceType generic STRING template*/
    /**
     * @Column(name="CROSS_REFERENCE_TYPE", type="string", nullable=false, length=25)
     *
     */
    protected $crossReferenceType;
    public function getCrossReferenceType(){
        return $this->crossReferenceType;
    }
    public function setCrossReferenceType($crossReferenceType){
        $this->crossReferenceType = $crossReferenceType;
    }

    // crossReference generic STRING template*/
    /**
     * @Column(name="CROSS_REFERENCE", type="string", nullable=false, length=25)
     *
     */
    protected $crossReference;
    public function getCrossReference(){
        return $this->crossReference;
    }
    public function setCrossReference($crossReference){
        $this->crossReference = $crossReference;
    }

    // description generic STRING template*/
    /**
     * @Column(name="DESCRIPTION", type="string", nullable=true, length=240)
     *
     */
    protected $description;
    public function getDescription(){
        return $this->description;
    }
    public function setDescription($description){
        $this->description = $description;
    }

    // organizationCode generic STRING template*/
    /**
     * @Column(name="ORGANIZATION_CODE", type="string", nullable=true, length=3)
     *
     */
    protected $organizationCode;
    public function getOrganizationCode(){
        return $this->organizationCode;
    }
    public function setOrganizationCode($organizationCode){
        $this->organizationCode = $organizationCode;
    }

    // orgName generic STRING template*/
    /**
     * @Column(name="ORG_NAME", type="string", nullable=true, length=60)
     *
     */
    protected $orgName;
    public function getOrgName(){
        return $this->orgName;
    }
    public function setOrgName($orgName){
        $this->orgName = $orgName;
    }

}

