<?php
namespace Commons\Base;

use Commons\RAWBase;

class BaseItemHeader extends RAWBase
{
    public function __construct()
    {
    }

    /** @Column(type="string", name="FAMILY", length=8, nullable=true) **/
    protected $family;

    /** @Column(type="string", name="SUB_FAMILY", length=8, nullable=true) **/
    protected $subFamily;

    /** @Column(type="string", name="LINE", length=8, nullable=true) **/
    protected $line;

    /**
     * @return mixed
     */
    public function getFamily()
    {
        return $this->family;
    }

    /**
     * @param mixed $family
     */
    public function setFamily($family)
    {
        $this->family = $family;
    }

    /**
     * @return mixed
     */
    public function getSubFamily()
    {
        return $this->subFamily;
    }

    /**
     * @param mixed $subFamily
     */
    public function setSubFamily($subFamily)
    {
        $this->subFamily = $subFamily;
    }

    /**
     * @return mixed
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * @param mixed $line
     */
    public function setLine($line)
    {
        $this->line = $line;
    }

    // origItemNumber generic STRING template*/
    /**
     * @Column(name="ORIG_ITEM_NUMBER", type="string", nullable=false, length=255)
     *
     */
    protected $origItemNumber;
    public function getOrigItemNumber(){
        return $this->origItemNumber;
    }
    public function setOrigItemNumber($origItemNumber){
        $this->origItemNumber = $origItemNumber;
    }

    // itemNumber generic STRING template*/
    /**
     * @Column(name="ITEM_NUMBER", type="string", nullable=false, length=16)
     *
     */
    protected $itemNumber;
    public function getItemNumber(){
        return $this->itemNumber;
    }
    public function setItemNumber($itemNumber){
        $this->itemNumber = $itemNumber;
    }

    // description generic STRING template*/
    /**
     * @Column(name="DESCRIPTION", type="string", nullable=false, length=240)
     *
     */
    protected $description;
    public function getDescription(){
        return $this->description;
    }
    public function setDescription($description){
        $this->description = $description;
    }

    // descriptionEsa generic STRING template*/
    /**
     * @Column(name="DESCRIPTION_ESA", type="string", nullable=false, length=240)
     *
     */
    protected $descriptionEsa;
    public function getDescriptionEsa(){
        return $this->descriptionEsa;
    }
    public function setDescriptionEsa($descriptionEsa){
        $this->descriptionEsa = $descriptionEsa;
    }

    // organizationCode generic STRING template*/
    /**
     * @Column(name="ORGANIZATION_CODE", type="string", nullable=false, length=3)
     *
     */
    protected $organizationCode;
    public function getOrganizationCode(){
        return $this->organizationCode;
    }
    public function setOrganizationCode($organizationCode){
        $this->organizationCode = $organizationCode;
    }

    // templateName generic STRING template*/
    /**
     * @Column(name="TEMPLATE_NAME", type="string", nullable=true, length=50)
     *
     */
    protected $templateName;
    public function getTemplateName(){
        return $this->templateName;
    }
    public function setTemplateName($templateName){
        $this->templateName = $templateName;
    }

    // listPricePerUnit NUMBER template
    /**
     * @Column(name="LIST_PRICE_PER_UNIT", type="float", nullable=true)
     *
     */
    protected $listPricePerUnit;
    public function getListPricePerUnit(){
        return $this->listPricePerUnit;
    }
    public function setListPricePerUnit($listPricePerUnit){
        $this->listPricePerUnit = $listPricePerUnit;
    }

    /**
     * @Column(name="CALCULATED_LIST_PRICE_PER_UNIT", type="float", nullable=true)
     *
     */
    protected $calculatedListPricePerUnit;
    public function getCalculatedListPricePerUnit(){
        return $this->calculatedListPricePerUnit;
    }
    public function setCalculatedListPricePerUnit($calculatedListPricePerUnit){
        $this->calculatedListPricePerUnit = $calculatedListPricePerUnit;
    }

    /**
     * @Column(name="CALCULATED_QUANTITY", type="float", nullable=true)
     *
     */
    protected $calculatedQuantity;
    public function getCalculatedQuantity(){
        return $this->calculatedQuantity;
    }
    public function setCalculatedQuantity($calculatedQuantity){
        $this->calculatedQuantity = $calculatedQuantity;
    }

    /**
     * @Column(name="PRICE_MANUAL_OVERRIDE", type="boolean", nullable=true)
     *
     */
    protected $priceManualOverride;
    public function getPriceManualOverride(){
        return $this->priceManualOverride;
    }
    public function setPriceManualOverride($priceManualOverride){
        $this->priceManualOverride = $priceManualOverride;
    }


    /**
     * @Column(name="PE_EXT_ATTRIBUTE1", type="string", nullable=true, length=13)
     *
     */
    protected $peExtAttribute1;

    /**
     * @Column(name="PE_EXT_ATTRIBUTE2", type="string", nullable=true, length=12)
     *
     */
    protected $peExtAttribute2;

    /**
     * @return mixed
     */
    public function getPeExtAttribute1()
    {
        return $this->peExtAttribute1;
    }

    /**
     * @param mixed $peExtAttribute1
     */
    public function setPeExtAttribute1($peExtAttribute1)
    {
        $this->peExtAttribute1 = $peExtAttribute1;
    }

    /**
     * @return mixed
     */
    public function getPeExtAttribute2()
    {
        return $this->peExtAttribute2;
    }

    /**
     * @param mixed $peExtAttribute2
     */
    public function setPeExtAttribute2($peExtAttribute2)
    {
        $this->peExtAttribute2 = $peExtAttribute2;
    }




    // uomCode generic STRING template*/
    /**
     * @Column(name="UOM_CODE", type="string", nullable=true, length=3)
     *
     */
    protected $uomCode;
    public function getUomCode(){
        return $this->uomCode;
    }
    public function setUomCode($uomCode){
        $this->uomCode = $uomCode;
    }

    // weightUomCode generic STRING template*/
    /**
     * @Column(name="WEIGHT_UOM_CODE", type="string", nullable=true, length=3)
     *
     */
    protected $weightUomCode;
    public function getWeightUomCode(){
        return $this->weightUomCode;
    }
    public function setWeightUomCode($weightUomCode){
        $this->weightUomCode = $weightUomCode;
    }

    // unitWeight NUMBER template
    /**
     * @Column(name="UNIT_WEIGHT", type="float", nullable=true)
     *
     */
    protected $unitWeight;
    public function getUnitWeight(){
        return $this->unitWeight;
    }
    public function setUnitWeight($unitWeight){
        $this->unitWeight = $unitWeight;
    }

    // volumeUomCode generic STRING template*/
    /**
     * @Column(name="VOLUME_UOM_CODE", type="string", nullable=true, length=3)
     *
     */
    protected $volumeUomCode;
    public function getVolumeUomCode(){
        return $this->volumeUomCode;
    }
    public function setVolumeUomCode($volumeUomCode){
        $this->volumeUomCode = $volumeUomCode;
    }

    // unitVolume NUMBER template
    /**
     * @Column(name="UNIT_VOLUME", type="float", nullable=true)
     *
     */
    protected $unitVolume;
    public function getUnitVolume(){
        return $this->unitVolume;
    }
    public function setUnitVolume($unitVolume){
        $this->unitVolume = $unitVolume;
    }

    // minMinmaxQuantity NUMBER template
    /**
     * @Column(name="MIN_MINMAX_QUANTITY", type="float", nullable=true)
     *
     */
    protected $minMinmaxQuantity;
    public function getMinMinmaxQuantity(){
        return $this->minMinmaxQuantity;
    }
    public function setMinMinmaxQuantity($minMinmaxQuantity){
        $this->minMinmaxQuantity = $minMinmaxQuantity;
    }

    // maxMinmaxQuantity NUMBER template
    /**
     * @Column(name="MAX_MINMAX_QUANTITY", type="float", nullable=true)
     *
     */
    protected $maxMinmaxQuantity;
    public function getMaxMinmaxQuantity(){
        return $this->maxMinmaxQuantity;
    }
    public function setMaxMinmaxQuantity($maxMinmaxQuantity){
        $this->maxMinmaxQuantity = $maxMinmaxQuantity;
    }

    // fullLeadTime NUMBER template
    /**
     * @Column(name="FULL_LEAD_TIME", type="float", nullable=true)
     *
     */
    protected $fullLeadTime;
    public function getFullLeadTime(){
        return $this->fullLeadTime;
    }
    public function setFullLeadTime($fullLeadTime){
        $this->fullLeadTime = $fullLeadTime;
    }

    // fixedLotMultiplier NUMBER template
    /**
     * @Column(name="FIXED_LOT_MULTIPLIER", type="float", nullable=true)
     *
     */
    protected $fixedLotMultiplier;
    public function getFixedLotMultiplier(){
        return $this->fixedLotMultiplier;
    }
    public function setFixedLotMultiplier($fixedLotMultiplier){
        $this->fixedLotMultiplier = $fixedLotMultiplier;
    }

    // fixedOrderQuantity NUMBER template
    /**
     * @Column(name="FIXED_ORDER_QUANTITY", type="float", nullable=true)
     *
     */
    protected $fixedOrderQuantity;
    public function getFixedOrderQuantity(){
        return $this->fixedOrderQuantity;
    }
    public function setFixedOrderQuantity($fixedOrderQuantity){
        $this->fixedOrderQuantity = $fixedOrderQuantity;
    }

    // minimumOrderQuantity NUMBER template
    /**
     * @Column(name="MINIMUM_ORDER_QUANTITY", type="float", nullable=true)
     *
     */
    protected $minimumOrderQuantity;
    public function getMinimumOrderQuantity(){
        return $this->minimumOrderQuantity;
    }
    public function setMinimumOrderQuantity($minimumOrderQuantity){
        $this->minimumOrderQuantity = $minimumOrderQuantity;
    }

    // maximumOrderQuantity NUMBER template
    /**
     * @Column(name="MAXIMUM_ORDER_QUANTITY", type="float", nullable=true)
     *
     */
    protected $maximumOrderQuantity;
    public function getMaximumOrderQuantity(){
        return $this->maximumOrderQuantity;
    }
    public function setMaximumOrderQuantity($maximumOrderQuantity){
        $this->maximumOrderQuantity = $maximumOrderQuantity;
    }

    // longDescription generic STRING template*/
    /**
     * @Column(name="LONG_DESCRIPTION", type="string", nullable=true, length=4000)
     *
     */
    protected $longDescription;
    public function getLongDescription(){
        return $this->longDescription;
    }
    public function setLongDescription($longDescription){
        $this->longDescription = $longDescription;
    }

    // itemInvApplication generic STRING template*/
    /**
     * @Column(name="ITEM_INV_APPLICATION", type="string", nullable=true, length=50)
     *
     */
    protected $itemInvApplication;
    public function getItemInvApplication(){
        return $this->itemInvApplication;
    }
    public function setItemInvApplication($itemInvApplication){
        $this->itemInvApplication = $itemInvApplication;
    }

    // transactionConditionCode generic STRING template*/
    /**
     * @Column(name="TRANSACTION_CONDITION_CODE", type="string", nullable=true, length=80)
     *
     */
    protected $transactionConditionCode;
    public function getTransactionConditionCode(){
        return $this->transactionConditionCode;
    }
    public function setTransactionConditionCode($transactionConditionCode){
        $this->transactionConditionCode = $transactionConditionCode;
    }

    // adjustmentAccount generic STRING template*/
    /**
     * @Column(name="ADJUSTMENT_ACCOUNT", type="string", nullable=true, length=100)
     *
     */
    protected $adjustmentAccount;
    public function getAdjustmentAccount(){
        return $this->adjustmentAccount;
    }
    public function setAdjustmentAccount($adjustmentAccount){
        $this->adjustmentAccount = $adjustmentAccount;
    }

    // correctionAccount generic STRING template*/
    /**
     * @Column(name="CORRECTION_ACCOUNT", type="string", nullable=true, length=100)
     *
     */
    protected $correctionAccount;
    public function getCorrectionAccount(){
        return $this->correctionAccount;
    }
    public function setCorrectionAccount($correctionAccount){
        $this->correctionAccount = $correctionAccount;
    }

    // salesCostAccount generic STRING template*/
    /**
     * @Column(name="SALES_COST_ACCOUNT", type="string", nullable=true, length=100)
     *
     */
    protected $salesCostAccount;
    public function getSalesCostAccount(){
        return $this->salesCostAccount;
    }
    public function setSalesCostAccount($salesCostAccount){
        $this->salesCostAccount = $salesCostAccount;
    }

    // fourDigitCode generic STRING template*/
    /**
     * @Column(name="FOUR_DIGIT_CODE", type="string", nullable=true, length=4)
     *
     */
    protected $fourDigitCode;
    public function getFourDigitCode(){
        return $this->fourDigitCode;
    }
    public function setFourDigitCode($fourDigitCode){
        $this->fourDigitCode = $fourDigitCode;
    }

    // orgName generic STRING template*/
    /**
     * @Column(name="ORG_NAME", type="string", nullable=false, length=60)
     *
     */
    protected $orgName;
    public function getOrgName(){
        return $this->orgName;
    }
    public function setOrgName($orgName){
        $this->orgName = $orgName;
    }

    // hasPicture generic Boolean template*/
    /**
     * @Column(name="HAS_PICTURE", type="boolean", nullable=true)
     *
     */
    protected $hasPicture;
    public function getHasPicture(){
        return $this->hasPicture;
    }
    public function setHasPicture($hasPicture){
        $this->hasPicture = $hasPicture;
    }


    // aliases generic STRING template*/
    /**
     * @Column(name="ALIASES", type="string", nullable=true, length=3000)
     *
     */
    protected $aliases;
    public function getAliases(){
        return $this->aliases;
    }
    public function setAliases($aliases){
        $this->aliases = $aliases;
    }

}

