<?php
namespace Commons\Base;

use Commons\RAWBase;

class BaseItemMFGPartNum extends RAWBase
{
    public function __construct()
    {
    }

    // manufacturer generic STRING template*/
    /**
     * @Column(name="MANUFACTURER", type="string", nullable=false, length=30)
     *
     */
    protected $manufacturer;
    public function getManufacturer(){
        return $this->manufacturer;
    }
    public function setManufacturer($manufacturer){
        $this->manufacturer = $manufacturer;
    }

    // description generic STRING template*/
    /**
     * @Column(name="DESCRIPTION", type="string", nullable=true, length=240)
     *
     */
    protected $description;
    public function getDescription(){
        return $this->description;
    }
    public function setDescription($description){
        $this->description = $description;
    }

    // partNumber generic STRING template*/
    /**
     * @Column(name="PART_NUMBER", type="string", nullable=false, length=30)
     *
     */
    protected $partNumber;
    public function getPartNumber(){
        return $this->partNumber;
    }
    public function setPartNumber($partNumber){
        $this->partNumber = $partNumber;
    }

    // origItemNumber generic STRING template*/
    /**
     * @Column(name="ORIG_ITEM_NUMBER", type="string", nullable=false, length=255)
     *
     */
    protected $origItemNumber;
    public function getOrigItemNumber(){
        return $this->origItemNumber;
    }
    public function setOrigItemNumber($origItemNumber){
        $this->origItemNumber = $origItemNumber;
    }

    // itemNumber generic STRING template*/
    /**
     * @Column(name="ITEM_NUMBER", type="string", nullable=false, length=16)
     *
     */
    protected $itemNumber;
    public function getItemNumber(){
        return $this->itemNumber;
    }
    public function setItemNumber($itemNumber){
        $this->itemNumber = $itemNumber;
    }

    // orgName generic STRING template*/
    /**
     * @Column(name="ORG_NAME", type="string", nullable=false, length=60)
     *
     */
    protected $orgName;
    public function getOrgName(){
        return $this->orgName;
    }
    public function setOrgName($orgName){
        $this->orgName = $orgName;
    }

}

