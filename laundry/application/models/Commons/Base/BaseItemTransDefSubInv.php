<?php
namespace Commons\Base;

use Commons\RAWBase;

class BaseItemTransDefSubInv extends RAWBase
{
    public function __construct()
    {
    }

    // origItemNumber generic STRING template*/
    /**
     * @Column(name="ORIG_ITEM_NUMBER", type="string", nullable=false, length=255)
     *
     */
    protected $origItemNumber;
    public function getOrigItemNumber(){
        return $this->origItemNumber;
    }
    public function setOrigItemNumber($origItemNumber){
        $this->origItemNumber = $origItemNumber;
    }

    // itemNumber generic STRING template*/
    /**
     * @Column(name="ITEM_NUMBER", type="string", nullable=false, length=16)
     *
     */
    protected $itemNumber;
    public function getItemNumber(){
        return $this->itemNumber;
    }
    public function setItemNumber($itemNumber){
        $this->itemNumber = $itemNumber;
    }

    // organizationCode generic STRING template*/
    /**
     * @Column(name="ORGANIZATION_CODE", type="string", nullable=false, length=3)
     *
     */
    protected $organizationCode;
    public function getOrganizationCode(){
        return $this->organizationCode;
    }
    public function setOrganizationCode($organizationCode){
        $this->organizationCode = $organizationCode;
    }

    // subinventoryName generic STRING template*/
    /**
     * @Column(name="SUBINVENTORY_NAME", type="string", nullable=false, length=10)
     *
     */
    protected $subinventoryName;
    public function getSubinventoryName(){
        return $this->subinventoryName;
    }
    public function setSubinventoryName($subinventoryName){
        $this->subinventoryName = $subinventoryName;
    }

    // orgName generic STRING template*/
    /**
     * @Column(name="ORG_NAME", type="string", nullable=false, length=60)
     *
     */
    protected $orgName;
    public function getOrgName(){
        return $this->orgName;
    }
    public function setOrgName($orgName){
        $this->orgName = $orgName;
    }

}

