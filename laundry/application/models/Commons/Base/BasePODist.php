<?php
namespace Commons\Base;

use Commons\RAWBase;

class BasePODist extends RAWBase
{
    public function __construct()
    {
    }

    // poNumber generic STRING template*/
    /**
     * @Column(name="PO_NUMBER", type="string", nullable=false, length=20)
     *
     */
    protected $poNumber;
    public function getPoNumber(){
        return $this->poNumber;
    }
    public function setPoNumber($poNumber){
        $this->poNumber = $poNumber;
    }

    // lineNum NUMBER template
    /**
     * @Column(name="LINE_NUM", type="float", nullable=false)
     *
     */
    protected $lineNum;
    public function getLineNum(){
        return $this->lineNum;
    }
    public function setLineNum($lineNum){
        $this->lineNum = $lineNum;
    }

    // shipmentNum NUMBER template
    /**
     * @Column(name="SHIPMENT_NUM", type="float", nullable=false)
     *
     */
    protected $shipmentNum;
    public function getShipmentNum(){
        return $this->shipmentNum;
    }
    public function setShipmentNum($shipmentNum){
        $this->shipmentNum = $shipmentNum;
    }

    // distributionNum NUMBER template
    /**
     * @Column(name="DISTRIBUTION_NUM", type="float", nullable=false)
     *
     */
    protected $distributionNum;
    public function getDistributionNum(){
        return $this->distributionNum;
    }
    public function setDistributionNum($distributionNum){
        $this->distributionNum = $distributionNum;
    }

    // quantityOrdered NUMBER template
    /**
     * @Column(name="QUANTITY_ORDERED", type="float", nullable=false)
     *
     */
    protected $quantityOrdered;
    public function getQuantityOrdered(){
        return $this->quantityOrdered;
    }
    public function setQuantityOrdered($quantityOrdered){
        $this->quantityOrdered = $quantityOrdered;
    }

    // projectName generic STRING template*/
    /**
     * @Column(name="PROJECT_NAME", type="string", nullable=true, length=30)
     *
     */
    protected $projectName;
    public function getProjectName(){
        return $this->projectName;
    }
    public function setProjectName($projectName){
        $this->projectName = $projectName;
    }

    // taskNumber generic STRING template*/
    /**
     * @Column(name="TASK_NUMBER", type="string", nullable=true, length=25)
     *
     */
    protected $taskNumber;
    public function getTaskNumber(){
        return $this->taskNumber;
    }
    public function setTaskNumber($taskNumber){
        $this->taskNumber = $taskNumber;
    }

    // chargeAccount generic STRING template*/
    /**
     * @Column(name="CHARGE_ACCOUNT", type="string", nullable=true, length=200)
     *
     */
    protected $chargeAccount;
    public function getChargeAccount(){
        return $this->chargeAccount;
    }
    public function setChargeAccount($chargeAccount){
        $this->chargeAccount = $chargeAccount;
    }

    // expenditureType generic STRING template*/
    /**
     * @Column(name="EXPENDITURE_TYPE", type="string", nullable=true, length=30)
     *
     */
    protected $expenditureType;
    public function getExpenditureType(){
        return $this->expenditureType;
    }
    public function setExpenditureType($expenditureType){
        $this->expenditureType = $expenditureType;
    }

    // requisitionNumber generic STRING template*/
    /**
     * @Column(name="REQUISITION_NUMBER", type="string", nullable=true, length=25)
     *
     */
    protected $requisitionNumber;
    public function getRequisitionNumber(){
        return $this->requisitionNumber;
    }
    public function setRequisitionNumber($requisitionNumber){
        $this->requisitionNumber = $requisitionNumber;
    }

    // orgName generic STRING template*/
    /**
     * @Column(name="ORG_NAME", type="string", nullable=false, length=150)
     *
     */
    protected $orgName;
    public function getOrgName(){
        return $this->orgName;
    }
    public function setOrgName($orgName){
        $this->orgName = $orgName;
    }
}
