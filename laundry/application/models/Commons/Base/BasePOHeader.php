<?php
namespace Commons\Base;

use Commons\RAWBase;

class BasePOHeader extends RAWBase
{
    public function __construct()
    {
    }

    // poNumber generic STRING template*/
    /**
     * @Column(name="PO_NUMBER", type="string", nullable=false, length=20)
     *
     */
    protected $poNumber;
    public function getPoNumber(){
        return $this->poNumber;
    }
    public function setPoNumber($poNumber){
        $this->poNumber = $poNumber;
    }

    // poType generic STRING template*/
    /**
     * @Column(name="PO_TYPE", type="string", nullable=false, length=25)
     *
     */
    protected $poType;
    public function getPoType(){
        return $this->poType;
    }
    public function setPoType($poType){
        $this->poType = $poType;
    }

    // vendorName generic STRING template*/
    /**
     * @Column(name="VENDOR_NAME", type="string", nullable=false, length=240)
     *
     */
    protected $vendorName;
    public function getVendorName(){
        return $this->vendorName;
    }
    public function setVendorName($vendorName){
        $this->vendorName = $vendorName;
    }

    // vendorSiteCode generic STRING template*/
    /**
     * @Column(name="VENDOR_SITE_CODE", type="string", nullable=false, length=15)
     *
     */
    protected $vendorSiteCode;
    public function getVendorSiteCode(){
        return $this->vendorSiteCode;
    }
    public function setVendorSiteCode($vendorSiteCode){
        $this->vendorSiteCode = $vendorSiteCode;
    }

    // orderDate DATE template
    /**
     * @Column(name="ORDER_DATE", type="datetime", nullable=false)
     *
     */
    protected $orderDate;
    public function getOrderDate(){
        return $this->orderDate;
    }
    public function setOrderDate($orderDate){
        $this->orderDate = $orderDate;
    }

    // comments generic STRING template*/
    /**
     * @Column(name="COMMENTS", type="string", nullable=false, length=240)
     *
     */
    protected $comments;
    public function getComments(){
        return $this->comments;
    }
    public function setComments($comments){
        $this->comments = $comments;
    }

    // paymentTerms generic STRING template*/
    /**
     * @Column(name="PAYMENT_TERMS", type="string", nullable=false, length=240)
     *
     */
    protected $paymentTerms;
    public function getPaymentTerms(){
        return $this->paymentTerms;
    }
    public function setPaymentTerms($paymentTerms){
        $this->paymentTerms = $paymentTerms;
    }

    // agentEmployeeNumber generic STRING template*/
    /**
     * @Column(name="AGENT_EMPLOYEE_NUMBER", type="string", nullable=false, length=30)
     *
     */
    protected $agentEmployeeNumber;
    public function getAgentEmployeeNumber(){
        return $this->agentEmployeeNumber;
    }
    public function setAgentEmployeeNumber($agentEmployeeNumber){
        $this->agentEmployeeNumber = $agentEmployeeNumber;
    }

    // noteToVendor generic STRING template*/
    /**
     * @Column(name="NOTE_TO_VENDOR", type="string", nullable=false, length=480)
     *
     */
    protected $noteToVendor;
    public function getNoteToVendor(){
        return $this->noteToVendor;
    }
    public function setNoteToVendor($noteToVendor){
        $this->noteToVendor = $noteToVendor;
    }

    // shipToLocation generic STRING template*/
    /**
     * @Column(name="SHIP_TO_LOCATION", type="string", nullable=false, length=60)
     *
     */
    protected $shipToLocation;
    public function getShipToLocation(){
        return $this->shipToLocation;
    }
    public function setShipToLocation($shipToLocation){
        $this->shipToLocation = $shipToLocation;
    }

    // billToLocation generic STRING template*/
    /**
     * @Column(name="BILL_TO_LOCATION", type="string", nullable=false, length=60)
     *
     */
    protected $billToLocation;
    public function getBillToLocation(){
        return $this->billToLocation;
    }
    public function setBillToLocation($billToLocation){
        $this->billToLocation = $billToLocation;
    }

    // currencyCode generic STRING template*/
    /**
     * @Column(name="CURRENCY_CODE", type="string", nullable=false, length=3)
     *
     */
    protected $currencyCode;
    public function getCurrencyCode(){
        return $this->currencyCode;
    }
    public function setCurrencyCode($currencyCode){
        $this->currencyCode = $currencyCode;
    }

    // rate NUMBER template
    /**
     * @Column(name="RATE", type="float", nullable=true)
     *
     */
    protected $rate;
    public function getRate(){
        return $this->rate;
    }
    public function setRate($rate){
        $this->rate = $rate;
    }

    // approvalStatus generic STRING template*/
    /**
     * @Column(name="APPROVAL_STATUS", type="string", nullable=false, length=30)
     *
     */
    protected $approvalStatus;
    public function getApprovalStatus(){
        return $this->approvalStatus;
    }
    public function setApprovalStatus($approvalStatus){
        $this->approvalStatus = $approvalStatus;
    }

    // approvedDate DATE template
    /**
     * @Column(name="APPROVED_DATE", type="datetime", nullable=true)
     *
     */
    protected $approvedDate;
    public function getApprovedDate(){
        return $this->approvedDate;
    }
    public function setApprovedDate($approvedDate){
        $this->approvedDate = $approvedDate;
    }

    // orgName generic STRING template*/
    /**
     * @Column(name="ORG_NAME", type="string", nullable=false, length=150)
     *
     */
    protected $orgName;
    public function getOrgName(){
        return $this->orgName;
    }
    public function setOrgName($orgName){
        $this->orgName = $orgName;
    }

    // peExtAttribute1 generic STRING template*/
    /**
     * @Column(name="PE_EXT_ATTRIBUTE1", type="string", nullable=true, length=30)
     *
     */
    protected $peExtAttribute1;
    public function getPeExtAttribute1(){
        return $this->peExtAttribute1;
    }
    public function setPeExtAttribute1($peExtAttribute1){
        $this->peExtAttribute1 = $peExtAttribute1;
    }
}
