<?php
namespace Commons\Base;

use Commons\RAWBase;

class BasePOLine extends RAWBase
{
    public function __construct()
    {
    }

    // poNumber generic STRING template*/
    /**
     * @Column(name="PO_NUMBER", type="string", nullable=false, length=20)
     *
     */
    protected $poNumber;
    public function getPoNumber(){
        return $this->poNumber;
    }
    public function setPoNumber($poNumber){
        $this->poNumber = $poNumber;
    }

    // lineNum NUMBER template
    /**
     * @Column(name="LINE_NUM", type="float", nullable=false)
     *
     */
    protected $lineNum;
    public function getLineNum(){
        return $this->lineNum;
    }
    public function setLineNum($lineNum){
        $this->lineNum = $lineNum;
    }

    // lineType generic STRING template*/
    /**
     * @Column(name="LINE_TYPE", type="string", nullable=false, length=20)
     *
     */
    protected $lineType;
    public function getLineType(){
        return $this->lineType;
    }
    public function setLineType($lineType){
        $this->lineType = $lineType;
    }

    // itemNumber generic STRING template*/
    /**
     * @Column(name="ITEM_NUMBER", type="string", nullable=true, length=16)
     *
     */
    protected $itemNumber;
    public function getItemNumber(){
        return $this->itemNumber;
    }
    public function setItemNumber($itemNumber){
        $this->itemNumber = $itemNumber;
    }

    // itemDescription generic STRING template*/
    /**
     * @Column(name="ITEM_DESCRIPTION", type="string", nullable=true, length=240)
     *
     */
    protected $itemDescription;
    public function getItemDescription(){
        return $this->itemDescription;
    }
    public function setItemDescription($itemDescription){
        $this->itemDescription = $itemDescription;
    }

    // itemCategory generic STRING template*/
    /**
     * @Column(name="ITEM_CATEGORY", type="string", nullable=true, length=80)
     *
     */
    protected $itemCategory;
    public function getItemCategory(){
        return $this->itemCategory;
    }
    public function setItemCategory($itemCategory){
        $this->itemCategory = $itemCategory;
    }

    // quantity NUMBER template
    /**
     * @Column(name="QUANTITY", type="float", nullable=false)
     *
     */
    protected $quantity;
    public function getQuantity(){
        return $this->quantity;
    }
    public function setQuantity($quantity){
        $this->quantity = $quantity;
    }

    // unitPrice NUMBER template
    /**
     * @Column(name="UNIT_PRICE", type="float", nullable=false)
     *
     */
    protected $unitPrice;
    public function getUnitPrice(){
        return $this->unitPrice;
    }
    public function setUnitPrice($unitPrice){
        $this->unitPrice = $unitPrice;
    }

    // vendorProductNum generic STRING template*/
    /**
     * @Column(name="VENDOR_PRODUCT_NUM", type="string", nullable=true, length=80)
     *
     */
    protected $vendorProductNum;
    public function getVendorProductNum(){
        return $this->vendorProductNum;
    }
    public function setVendorProductNum($vendorProductNum){
        $this->vendorProductNum = $vendorProductNum;
    }

    // orgName generic STRING template*/
    /**
     * @Column(name="ORG_NAME", type="string", nullable=false, length=150)
     *
     */
    protected $orgName;
    public function getOrgName(){
        return $this->orgName;
    }
    public function setOrgName($orgName){
        $this->orgName = $orgName;
    }
}
