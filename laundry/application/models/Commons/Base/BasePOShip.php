<?php
namespace Commons\Base;

use Commons\RAWBase;

class BasePOShip extends RAWBase
{
    public function __construct()
    {
    }

    // poNumber generic STRING template*/
    /**
     * @Column(name="PO_NUMBER", type="string", nullable=false, length=20)
     *
     */
    protected $poNumber;
    public function getPoNumber(){
        return $this->poNumber;
    }
    public function setPoNumber($poNumber){
        $this->poNumber = $poNumber;
    }

    // lineNum NUMBER template
    /**
     * @Column(name="LINE_NUM", type="float", nullable=false)
     *
     */
    protected $lineNum;
    public function getLineNum(){
        return $this->lineNum;
    }
    public function setLineNum($lineNum){
        $this->lineNum = $lineNum;
    }

    // shipmentNum NUMBER template
    /**
     * @Column(name="SHIPMENT_NUM", type="float", nullable=false)
     *
     */
    protected $shipmentNum;
    public function getShipmentNum(){
        return $this->shipmentNum;
    }
    public function setShipmentNum($shipmentNum){
        $this->shipmentNum = $shipmentNum;
    }

    // quantity NUMBER template
    /**
     * @Column(name="QUANTITY", type="float", nullable=false)
     *
     */
    protected $quantity;
    public function getQuantity(){
        return $this->quantity;
    }
    public function setQuantity($quantity){
        $this->quantity = $quantity;
    }

    // needByDate DATE template
    /**
     * @Column(name="NEED_BY_DATE", type="datetime", nullable=false)
     *
     */
    protected $needByDate;
    public function getNeedByDate(){
        return $this->needByDate;
    }
    public function setNeedByDate($needByDate){
        $this->needByDate = $needByDate;
    }

    // promisedDate DATE template
    /**
     * @Column(name="PROMISED_DATE", type="datetime", nullable=false)
     *
     */
    protected $promisedDate;
    public function getPromisedDate(){
        return $this->promisedDate;
    }
    public function setPromisedDate($promisedDate){
        $this->promisedDate = $promisedDate;
    }

    // shipToOrganization generic STRING template*/
    /**
     * @Column(name="SHIP_TO_ORGANIZATION", type="string", nullable=false, length=3)
     *
     */
    protected $shipToOrganization;
    public function getShipToOrganization(){
        return $this->shipToOrganization;
    }
    public function setShipToOrganization($shipToOrganization){
        $this->shipToOrganization = $shipToOrganization;
    }

    // shipToLocation generic STRING template*/
    /**
     * @Column(name="SHIP_TO_LOCATION", type="string", nullable=false, length=60)
     *
     */
    protected $shipToLocation;
    public function getShipToLocation(){
        return $this->shipToLocation;
    }
    public function setShipToLocation($shipToLocation){
        $this->shipToLocation = $shipToLocation;
    }

    /** receiptRequiredFlag bit template*/
    /**
     * @Column(name="RECEIPT_REQUIRED_FLAG", type="boolean", nullable=false)
     */
    protected $receiptRequiredFlag;
    public function getReceiptRequiredFlag(){
        return $this->receiptRequiredFlag;
    }
    public function setReceiptRequiredFlag($receiptRequiredFlag){
        $this->receiptRequiredFlag = $receiptRequiredFlag;
    }

    // requester generic STRING template*/
    /**
     * @Column(name="REQUESTER", type="string", nullable=true, length=240)
     *
     */
    protected $requester;
    public function getRequester(){
        return $this->requester;
    }
    public function setRequester($requester){
        $this->requester = $requester;
    }

    // orgName generic STRING template*/
    /**
     * @Column(name="ORG_NAME", type="string", nullable=false, length=150)
     *
     */
    protected $orgName;
    public function getOrgName(){
        return $this->orgName;
    }
    public function setOrgName($orgName){
        $this->orgName = $orgName;
    }
}
