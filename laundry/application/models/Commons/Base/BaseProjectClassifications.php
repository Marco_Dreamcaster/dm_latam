<?php
namespace Commons\Base;

Use Commons\RAWBase;

class BaseProjectClassifications extends RAWBase
{
    public function __construct()
    {
    }

    /**
     * @Column(name="PROJECT_CLASSIFICATIONS_ID", type="string", nullable=false)
     *
     */
    protected $projectClassificationsId;

    public function getProjectClassificationsId()
    {
        return $this->projectClassificationsId;
    }
    public function setProjectClassificationsId($projectClassificationsId)
    {
        $this->projectClassificationsId = $projectClassificationsId;
    }
    /**
     * @Column(name="PROJECT_NAME", type="string", nullable=false)
     *
     */
    protected $projectName;

    public function getProjectName()
    {
        return $this->projectName;
    }
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    }
    /**
     * @Column(name="CLASS_CATEGORY", type="string", nullable=false)
     *
     */
    protected $classCategory;

    public function getClassCategory()
    {
        return $this->classCategory;
    }
    public function setClassCategory($classCategory)
    {
        $this->classCategory = $classCategory;
    }
    /**
     * @Column(name="CLASS_CODE", type="string", nullable=false)
     *
     */
    protected $classCode;

    public function getClassCode()
    {
        return $this->classCode;
    }
    public function setClassCode($classCode)
    {
        $this->classCode = $classCode;
    }
    /**
     * @Column(name="ORG_NAME", type="string", nullable=false)
     *
     */
    protected $orgName;

    public function getOrgName()
    {
        return $this->orgName;
    }
    public function setOrgName($orgName)
    {
        $this->orgName = $orgName;
    }

}
