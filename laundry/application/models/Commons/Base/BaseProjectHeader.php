<?php
namespace Commons\Base;

use Commons\RAWBase;

class BaseProjectHeader extends RAWBase
{
    public function __construct()
    {
    }

    /**
     * @Column(name="SIGLA_PRIMARY_KEY", type="string", length=60, nullable=true)
     *
     */
    protected $siglaPrimaryKey;

    /**
     * @Column(name="PM_PROJECT_REFERENCE", type="string", nullable=false)
     *
     */
    protected $pmProjectReference;

    /**
     * @Column(name="PROJECT_NAME", type="string", nullable=false)
     *
     */
    protected $projectName;

    /**
     * @Column(name="PROJECT_TYPE", type="string", nullable=false)
     *
     */
    protected $projectType;

    /**
     * @Column(name="PROJECT_STATUS", type="string", nullable=false)
     *
     */
    protected $projectStatus;

    /**
     * @Column(name="TEMPLATE_NAME", type="string", nullable=false)
     *
     */
    protected $templateName;

    /**
     * @Column(name="START_DATE", type="datetime", nullable=false)
     *
     */
    protected $startDate;

    /**
     * @Column(name="COMPLETION_DATE", type="datetime", nullable=false)
     *
     */
    protected $completionDate;

    /**
     * @Column(name="DESCRIPTION", type="string", nullable=true)
     *
     */
    protected $description;

    /**
     * @Column(name="ORGANIZATION_CODE", type="string", nullable=false)
     *
     */
    protected $organizationCode;
    /**
     * @Column(name="CUSTOMER_NUMBER", type="string", nullable=false)
     *
     */
    protected $customerNumber;
    /**
     * @Column(name="BILL_TO_ADDRESS_REF", type="string", nullable=false)
     *
     */
    protected $billToAddressRef;
    /**
     * @Column(name="SHIP_TO_ADDRESS_REF", type="string", nullable=false)
     *
     */
    protected $shipToAddressRef;
    /**
     * @Column(name="CONTACT_REF", type="string", nullable=true)
     *
     */
    protected $contactRef;
    /**
     * @Column(name="PROJECT_MANAGER_REF", type="string", nullable=false)
     *
     */
    protected $projectManagerRef;
    /**
     * @Column(name="CREDIT_RECEIVER_REF", type="string", nullable=true)
     *
     */
    protected $creditReceiverRef;
    /**
     * @Column(name="SALES_REPRESENTATIVE_REF", type="string", nullable=true)
     *
     */
    protected $salesRepresentativeRef;
    /**
     * @Column(name="SERVICE_MANAGER_REF", type="string", nullable=true)
     *
     */
    protected $serviceManagerRef;
    /**
     * @Column(name="DUMMY_KEY_MEMBER_REF", type="string", nullable=true)
     *
     */
    protected $dummyKeyMemberRef;
    /**
     * @Column(name="ATTRIBUTE2", type="string", nullable=true)
     *
     */
    protected $attribute2;
    /**
     * @Column(name="ATTRIBUTE6", type="float", nullable=true)
     *
     */
    protected $attribute6;
    /**
     * @Column(name="ATTRIBUTE8", type="float", nullable=true)
     *
     */
    protected $attribute8;
    /**
     * @Column(name="BOOKING_PKG_RCVD", type="datetime", nullable=true)
     *
     */
    protected $bookingPkgRcvd;
    /**
     * @Column(name="COMPLETE_PCKG_RCVD_DATE", type="datetime", nullable=true)
     *
     */
    protected $completePckgRcvdDate;
    /**
     * @Column(name="CONTRACT_BOOKED_DATE", type="datetime", nullable=true)
     *
     */
    protected $contractBookedDate;
    /**
     * @Column(name="CONTRACT_DATE", type="datetime", nullable=true)
     *
     */
    protected $contractDate;
    /**
     * @Column(name="PACKAGE_AND_PO_SALES", type="datetime", nullable=true)
     *
     */
    protected $packageAndPoSales;
    /**
     * @Column(name="EXECUTION_AND_RETURN_DATE", type="datetime", nullable=true)
     *
     */
    protected $executionAndReturnDate;
    /**
     * @Column(name="CONTRACT_TYPE_TKE", type="string", nullable=true)
     *
     */
    protected $contractTypeTke;
    /**
     * @Column(name="CUSTOMER_PO_NUMBER", type="string", nullable=true)
     *
     */
    protected $customerPoNumber;
    /**
     * @Column(name="OUTPUT_TAX_CODE", type="string", nullable=true)
     *
     */
    protected $outputTaxCode;
    // aliases generic STRING template*/
    /**
     * @Column(name="ALIASES", type="string", nullable=true, length=3000)
     *
     */
    protected $aliases;
    /**
     * @Column(name="OBRA_CERRADA", type="boolean", nullable=true)
     *
     */
    protected $obraCerrada;
    /**
     * @Column(name="OBRA_EN_CURSO", type="boolean", nullable=true)
     *
     */
    protected $obraEnCurso;
    /**
     * @Column(name="ORG_NAME", type="string", nullable=false)
     *
     */
    protected $orgName;


    /**
     * @Column(name="BUILDING_TYPE", type="string", nullable=true)
     *
     */
    protected $buildingType;

    /**
     * @Column(name="BUILDING_CLASSIFICATION", type="string", nullable=true)
     *
     */
    protected $buildingClassification;


    // GETTERS & SETTERS


    public function getSiglaPrimaryKey()
    {
        return $this->siglaPrimaryKey;
    }
    public function setSiglaPrimaryKey($siglaPrimaryKey)
    {
        $this->siglaPrimaryKey = $siglaPrimaryKey;
    }


    public function getPmProjectReference()
    {
        return $this->pmProjectReference;
    }
    public function setPmProjectReference($pmProjectReference)
    {
        $this->pmProjectReference = $pmProjectReference;
    }

    public function getProjectName()
    {
        return $this->projectName;
    }
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    }

    public function getProjectType()
    {
        return $this->projectType;
    }
    public function setProjectType($projectType)
    {
        $this->projectType = $projectType;
    }
    public function getProjectStatus()
    {
        return $this->projectStatus;
    }
    public function setProjectStatus($projectStatus)
    {
        $this->projectStatus = $projectStatus;
    }
    public function getTemplateName()
    {
        return $this->templateName;
    }
    public function setTemplateName($templateName)
    {
        $this->templateName = $templateName;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    public function getCompletionDate()
    {
        return $this->completionDate;
    }
    public function setCompletionDate($completionDate)
    {
        $this->completionDate = $completionDate;
    }

    public function getDescription()
    {
        return $this->description;
    }
    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getOrganizationCode()
    {
        return $this->organizationCode;
    }
    public function setOrganizationCode($organizationCode)
    {
        $this->organizationCode = $organizationCode;
    }

    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }
    public function setCustomerNumber($customerNumber)
    {
        $this->customerNumber = $customerNumber;
    }

    public function getBillToAddressRef()
    {
        return $this->billToAddressRef;
    }
    public function setBillToAddressRef($billToAddressRef)
    {
        $this->billToAddressRef = $billToAddressRef;
    }

    public function getShipToAddressRef()
    {
        return $this->shipToAddressRef;
    }
    public function setShipToAddressRef($shipToAddressRef)
    {
        $this->shipToAddressRef = $shipToAddressRef;
    }

    public function getContactRef()
    {
        return $this->contactRef;
    }
    public function setContactRef($contactRef)
    {
        $this->contactRef = $contactRef;
    }

    public function getProjectManagerRef()
    {
        return $this->projectManagerRef;
    }
    public function setProjectManagerRef($projectManagerRef)
    {
        $this->projectManagerRef = $projectManagerRef;
    }

    public function getCreditReceiverRef()
    {
        return $this->creditReceiverRef;
    }
    public function setCreditReceiverRef($creditReceiverRef)
    {
        $this->creditReceiverRef = $creditReceiverRef;
    }

    public function getSalesRepresentativeRef()
    {
        return $this->salesRepresentativeRef;
    }
    public function setSalesRepresentativeRef($salesRepresentativeRef)
    {
        $this->salesRepresentativeRef = $salesRepresentativeRef;
    }

    public function getServiceManagerRef()
    {
        return $this->serviceManagerRef;
    }
    public function setServiceManagerRef($serviceManagerRef)
    {
        $this->serviceManagerRef = $serviceManagerRef;
    }

    public function getDummyKeyMemberRef()
    {
        return $this->dummyKeyMemberRef;
    }
    public function setDummyKeyMemberRef($dummyKeyMemberRef)
    {
        $this->dummyKeyMemberRef = $dummyKeyMemberRef;
    }

    public function getAttribute2()
    {
        return $this->attribute2;
    }
    public function setAttribute2($attribute2)
    {
        $this->attribute2 = $attribute2;
    }

    public function getAttribute6()
    {
        return $this->attribute6;
    }
    public function setAttribute6($attribute6)
    {
        $this->attribute6 = $attribute6;
    }

    public function getAttribute8()
    {
        return $this->attribute8;
    }
    public function setAttribute8($attribute8)
    {
        $this->attribute8 = $attribute8;
    }


    public function getBookingPkgRcvd()
    {
        return $this->bookingPkgRcvd;
    }
    public function setBookingPkgRcvd($bookingPkgRcvd)
    {
        $this->bookingPkgRcvd = $bookingPkgRcvd;
    }

    public function getPackageAndPoSales()
    {
        return $this->packageAndPoSales;
    }
    public function setPackageAndPoSales($packageAndPoSales)
    {
        $this->packageAndPoSales = $packageAndPoSales;
    }
    public function getCompletePckgRcvdDate()
    {
        return $this->completePckgRcvdDate;
    }
    public function setCompletePckgRcvdDate($completePckgRcvdDate)
    {
        $this->completePckgRcvdDate = $completePckgRcvdDate;
    }

    public function getContractBookedDate()
    {
        return $this->contractBookedDate;
    }
    public function setContractBookedDate($contractBookedDate)
    {
        $this->contractBookedDate = $contractBookedDate;
    }

    public function getContractDate()
    {
        return $this->contractDate;
    }
    public function setContractDate($contractDate)
    {
        $this->contractDate = $contractDate;
    }

    public function getExecutionAndReturnDate()
    {
        return $this->executionAndReturnDate;
    }
    public function setExecutionAndReturnDate($executionAndReturnDate)
    {
        $this->executionAndReturnDate = $executionAndReturnDate;
    }

    public function getContractTypeTke()
    {
        return $this->contractTypeTke;
    }
    public function setContractTypeTke($contractTypeTke)
    {
        $this->contractTypeTke = $contractTypeTke;
    }

    public function getCustomerPoNumber()
    {
        return $this->customerPoNumber;
    }
    public function setCustomerPoNumber($customerPoNumber)
    {
        $this->customerPoNumber = $customerPoNumber;
    }

    public function getOutputTaxCode()
    {
        return $this->outputTaxCode;
    }
    public function setOutputTaxCode($outputTaxCode)
    {
        $this->outputTaxCode = $outputTaxCode;
    }

    public function getOrgName()
    {
        return $this->orgName;
    }
    public function setOrgName($orgName)
    {
        $this->orgName = $orgName;
    }

    public function getAliases(){
        return $this->aliases;
    }
    public function setAliases($aliases){
        $this->aliases = $aliases;
    }

    /**
     * @return mixed
     */
    public function getObraCerrada()
    {
        return $this->obraCerrada;
    }

    /**
     * @param mixed $obraCerrada
     */
    public function setObraCerrada($obraCerrada)
    {
        $this->obraCerrada = $obraCerrada;
    }

    /**
     * @return mixed
     */
    public function getObraEnCurso()
    {
        return $this->obraEnCurso;
    }

    /**
     * @param mixed $obraEnCurso
     */
    public function setObraEnCurso($obraEnCurso)
    {
        $this->obraEnCurso = $obraEnCurso;
    }

    /**
     * @return mixed
     */
    public function getBuildingType()
    {
        return $this->buildingType;
    }

    /**
     * @param mixed $buildingType
     */
    public function setBuildingType($buildingType)
    {
        $this->buildingType = $buildingType;
    }

    /**
     * @return mixed
     */
    public function getBuildingClassification()
    {
        return $this->buildingClassification;
    }

    /**
     * @param mixed $buildingClassification
     */
    public function setBuildingClassification($buildingClassification)
    {
        $this->buildingClassification = $buildingClassification;
    }



}
