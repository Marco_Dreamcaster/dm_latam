<?php
namespace Commons\Base;

Use Commons\RAWBase;

class BaseProjectRetRules extends RawBase
{
    public function __construct()
    {
    }

    /**
     * @Column(name="PROJECT_NAME", type="string", nullable=false)
     *
     */
    protected $projectName;

    public function getProjectName()
    {
        return $this->projectName;
    }
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    }
    /**
     * @Column(name="RETENTION_PERCENTAGE", type="float", nullable=false)
     *
     */
    protected $retentionPercentage;

    public function getRetentionPercentage()
    {
        return $this->retentionPercentage;
    }
    public function setRetentionPercentage($retentionPercentage)
    {
        $this->retentionPercentage = $retentionPercentage;
    }
    /**
     * @Column(name="RETAINED_AMOUNT", type="float", nullable=false)
     *
     */
    protected $retainedAmount;

    public function getRetainedAmount()
    {
        return $this->retainedAmount;
    }
    public function setRetainedAmount($retainedAmount)
    {
        $this->retainedAmount = $retainedAmount;
    }
    /**
     * @Column(name="ORG_NAME", type="string", nullable=false)
     *
     */
    protected $orgName;

    public function getOrgName()
    {
        return $this->orgName;
    }
    public function setOrgName($orgName)
    {
        $this->orgName = $orgName;
    }



}
