<?php
namespace Commons\Base;

use Commons\RAWBase;

class BaseProjectTasks extends RAWBase
{
    public function __construct()
    {
    }

    /**
     * @Column(name="PROJECT_NAME", type="string", nullable=false)
     *
     */
    protected $projectName;

    public function getProjectName()
    {
        return $this->projectName;
    }
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    }

    /**
     * @Column(name="SIGLA_PRIMARY_KEY", type="string", length=60, nullable=true)
     *
     */
    protected $siglaPrimaryKey;

    public function getSiglaPrimaryKey()
    {
        return $this->siglaPrimaryKey;
    }
    public function setSiglaPrimaryKey($siglaPrimaryKey)
    {
        $this->siglaPrimaryKey = $siglaPrimaryKey;
    }


    /**
     * @Column(name="TASK_NUMBER", type="string", nullable=false)
     *
     */
    protected $taskNumber;

    public function getTaskNumber()
    {
        return $this->taskNumber;
    }
    public function setTaskNumber($taskNumber)
    {
        $this->taskNumber = $taskNumber;
    }
    /**
     * @Column(name="TASK_NAME", type="string", nullable=false)
     *
     */
    protected $taskName;

    public function getTaskName()
    {
        return $this->taskName;
    }
    public function setTaskName($taskName)
    {
        $this->taskName = $taskName;
    }
    /**
     * @Column(name="PARENT_TASK_NUMBER", type="string", nullable=true)
     *
     */
    protected $parentTaskNumber;

    public function getParentTaskNumber()
    {
        return $this->parentTaskNumber;
    }
    public function setParentTaskNumber($parentTaskNumber)
    {
        $this->parentTaskNumber = $parentTaskNumber;
    }
    /**
     * @Column(name="DESCRIPTION", type="string", nullable=true)
     *
     */
    protected $description;

    public function getDescription()
    {
        return $this->description;
    }
    public function setDescription($description)
    {
        $this->description = $description;
    }
    /**
     * @Column(name="START_DATE", type="datetime", nullable=false)
     *
     */
    protected $startDate;

    public function getStartDate()
    {
        return $this->startDate;
    }
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }
    /**
     * @Column(name="FINISH_DATE", type="datetime", nullable=true)
     *
     */
    protected $finishDate;

    public function getFinishDate()
    {
        return $this->finishDate;
    }
    public function setFinishDate($finishDate)
    {
        $this->finishDate = $finishDate;
    }
    /**
     * @Column(name="SERVICE_TYPE", type="string", nullable=false)
     *
     */
    protected $serviceType;

    public function getServiceType()
    {
        return $this->serviceType;
    }
    public function setServiceType($serviceType)
    {
        $this->serviceType = $serviceType;
    }
    /**
     * @Column(name="ALLOW_CHARGE", type="string", nullable=true)
     *
     */
    protected $allowCharge;

    public function getAllowCharge()
    {
        return $this->allowCharge;
    }
    public function setAllowCharge($allowCharge)
    {
        $this->allowCharge = $allowCharge;
    }
    /**
     * @Column(name="ATTRIBUTE1", type="string", nullable=true)
     *
     */
    protected $attribute1;

    public function getAttribute1()
    {
        return $this->attribute1;
    }
    public function setAttribute1($attribute1)
    {
        $this->attribute1 = $attribute1;
    }
    /**
     * @Column(name="ATTRIBUTE2", type="string", nullable=false)
     *
     * Type
     *
     */
    protected $attribute2;

    public function getAttribute2()
    {
        return $this->attribute2;
    }
    public function setAttribute2($attribute2)
    {
        $this->attribute2 = $attribute2;
    }
    /**
     * @Column(name="ATTRIBUTE3", type="string", nullable=false)
     *
     */
    protected $attribute3;

    public function getAttribute3()
    {
        return $this->attribute3;
    }
    public function setAttribute3($attribute3)
    {
        $this->attribute3 = $attribute3;
    }
    /**
     * @Column(name="ATTRIBUTE4", type="string", nullable=true)
     *
     */
    protected $attribute4;

    public function getAttribute4()
    {
        return $this->attribute4;
    }
    public function setAttribute4($attribute4)
    {
        $this->attribute4 = $attribute4;
    }
    /**
     * @Column(name="ATTRIBUTE5", type="string", nullable=false)
     *
     */
    protected $attribute5;

    public function getAttribute5()
    {
        return $this->attribute5;
    }
    public function setAttribute5($attribute5)
    {
        $this->attribute5 = $attribute5;
    }
    /**
     * @Column(name="ATTRIBUTE6", type="string", nullable=false)
     *
     */
    protected $attribute6;

    public function getAttribute6()
    {
        return $this->attribute6;
    }
    public function setAttribute6($attribute6)
    {
        $this->attribute6 = $attribute6;
    }
    /**
     * @Column(name="ATTRIBUTE7", type="string", nullable=true)
     *
     */
    protected $attribute7;

    public function getAttribute7()
    {
        return $this->attribute7;
    }
    public function setAttribute7($attribute7)
    {
        $this->attribute7 = $attribute7;
    }
    /**
     * @Column(name="ATTRIBUTE8", type="string", nullable=true)
     *
     */
    protected $attribute8;

    public function getAttribute8()
    {
        return $this->attribute8;
    }
    public function setAttribute8($attribute8)
    {
        $this->attribute8 = $attribute8;
    }
    /**
     * @Column(name="NO_OF_FLOORS", type="string", nullable=true)
     *
     */
    protected $noOfFloors;

    public function getNoOfFloors()
    {
        return $this->noOfFloors;
    }
    public function setNoOfFloors($noOfFloors)
    {
        $this->noOfFloors = $noOfFloors;
    }
    /**
     * @Column(name="SPEED", type="string", nullable=true)
     *
     */
    protected $speed;

    public function getSpeed()
    {
        return $this->speed;
    }
    public function setSpeed($speed)
    {
        $this->speed = $speed;
    }
    /**
     * @Column(name="CAPACITY", type="string", nullable=true)
     *
     */
    protected $capacity;

    public function getCapacity()
    {
        return $this->capacity;
    }
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;
    }
    /**
     * @Column(name="ORG_NAME", type="string", nullable=false)
     *
     */
    protected $orgName;

    public function getOrgName()
    {
        return $this->orgName;
    }
    public function setOrgName($orgName)
    {
        $this->orgName = $orgName;
    }


}
