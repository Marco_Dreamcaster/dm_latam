<?php
namespace Commons\Base;

use Commons\RAWBase;

class BaseVendorBankAccount extends RAWBase
{
    public function __construct()
    {
    }

    /**
     * @Column(name="BIC", type="string", nullable=true, length=30)
     *
     */
    protected $bic;
    public function getBic(){
        return $this->bic;
    }
    public function setBic($bic){
        $this->bic = $bic;
    }

    /**
     * @Column(name="IBAN", type="string", nullable=true, length=50)
     *
     */
    protected $iban;
    public function getIban(){
        return $this->iban;
    }
    public function setIban($iban){
        $this->iban = $iban;
    }

    /**
     * @Column(name="TYPE", type="string", nullable=true, length=25)
     *
     */
    protected $type;
    public function getType(){
        return $this->type;
    }
    public function setType($type){
        $this->type = $type;
    }


    // vendorNumber generic STRING template*/
    /**
     * @Column(name="VENDOR_NUMBER", type="string", nullable=false, length=30)
     *
     */
    protected $vendorNumber;
    public function getVendorNumber(){
        return $this->vendorNumber;
    }
    public function setVendorNumber($vendorNumber){
        $this->vendorNumber = $vendorNumber;
    }

    // bankName generic STRING template*/
    /**
     * @Column(name="BANK_NAME", type="string", nullable=false, length=60)
     *
     */
    protected $bankName;
    public function getBankName(){
        return $this->bankName;
    }
    public function setBankName($bankName){
        $this->bankName = $bankName;
    }

    // branchName generic STRING template*/
    /**
     * @Column(name="BRANCH_NAME", type="string", nullable=false, length=60)
     *
     */
    protected $branchName;
    public function getBranchName(){
        return $this->branchName;
    }
    public function setBranchName($branchName){
        $this->branchName = $branchName;
    }

    // bankNumber generic STRING template*/
    /**
     * @Column(name="BANK_NUMBER", type="string", nullable=false, length=30)
     *
     */
    protected $bankNumber;
    public function getBankNumber(){
        return $this->bankNumber;
    }
    public function setBankNumber($bankNumber){
        $this->bankNumber = $bankNumber;
    }

    // branchNumber generic STRING template*/
    /**
     * @Column(name="BRANCH_NUMBER", type="string", nullable=true, length=25)
     *
     */
    protected $branchNumber;
    public function getBranchNumber(){
        return $this->branchNumber;
    }
    public function setBranchNumber($branchNumber){
        $this->branchNumber = $branchNumber;
    }

    // bankHomeCountry generic STRING template*/
    /**
     * @Column(name="BANK_HOME_COUNTRY", type="string", nullable=false, length=2)
     *
     */
    protected $bankHomeCountry;
    public function getBankHomeCountry(){
        return $this->bankHomeCountry;
    }
    public function setBankHomeCountry($bankHomeCountry){
        $this->bankHomeCountry = $bankHomeCountry;
    }

    // taxPayerId generic STRING template*/
    /**
     * @Column(name="TAX_PAYER_ID", type="string", nullable=true, length=20)
     *
     */
    protected $taxPayerId;
    public function getTaxPayerId(){
        return $this->taxPayerId;
    }
    public function setTaxPayerId($taxPayerId){
        $this->taxPayerId = $taxPayerId;
    }

    // bankAccountName generic STRING template*/
    /**
     * @Column(name="BANK_ACCOUNT_NAME", type="string", nullable=false, length=80)
     *
     */
    protected $bankAccountName;
    public function getBankAccountName(){
        return $this->bankAccountName;
    }
    public function setBankAccountName($bankAccountName){
        $this->bankAccountName = $bankAccountName;
    }

    // bankAccountNum generic STRING template*/
    /**
     * @Column(name="BANK_ACCOUNT_NUM", type="string", nullable=false, length=30)
     *
     */
    protected $bankAccountNum;
    public function getBankAccountNum(){
        return $this->bankAccountNum;
    }
    public function setBankAccountNum($bankAccountNum){
        $this->bankAccountNum = $bankAccountNum;
    }

    // currencyCode generic STRING template*/
    /**
     * @Column(name="CURRENCY_CODE", type="string", nullable=false, length=15)
     *
     */
    protected $currencyCode;
    public function getCurrencyCode(){
        return $this->currencyCode;
    }
    public function setCurrencyCode($currencyCode){
        $this->currencyCode = $currencyCode;
    }

    // startDate DATE template
    /**
     * @Column(name="START_DATE", type="datetime", nullable=false)
     *
     */
    protected $startDate;
    public function getStartDate(){
        return $this->startDate;
    }
    public function setStartDate($startDate){
        $this->startDate = $startDate;
    }

    // endDate DATE template
    /**
     * @Column(name="END_DATE", type="datetime", nullable=true)
     *
     */
    protected $endDate;
    public function getEndDate(){
        return $this->endDate;
    }
    public function setEndDate($endDate){
        $this->endDate = $endDate;
    }

    // orgName generic STRING template*/
    /**
     * @Column(name="ORG_NAME", type="string", nullable=false, length=240)
     *
     */
    protected $orgName;
    public function getOrgName(){
        return $this->orgName;
    }
    public function setOrgName($orgName){
        $this->orgName = $orgName;
    }

}
