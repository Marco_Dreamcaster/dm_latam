<?php
namespace Commons\Base;

use Commons\RAWBase;

class BaseVendorHeader extends RAWBase
{
    public function __construct()
    {
    }

    /**
     * @Column(name="SIGLA_PRIMARY_KEY", type="string", length=60, nullable=true)
     *
     */
    protected $siglaPrimaryKey;

    /**
     * @Column(name="VENDOR_NAME", type="string", nullable=false, length=240)
     *
     */
    protected $vendorName;

    /**
     * @Column(name="SEGMENT1", type="string", nullable=false, length=30)
     *
     */
    protected $segment1;

    /**
     * @Column(name="VENDOR_NAME_ALT", type="string", nullable=false, length=320)
     *
     */
    protected $vendorNameAlt;

    /**
     * @Column(name="VENDOR_TYPE_LOOKUP_CODE", type="string", nullable=false, length=30)
     *
     */
    protected $vendorTypeLookupCode;

    /**
     * @Column(name="VAT_REGISTRATION_NUM", type="string", nullable=true, length=20)
     *
     */
    protected $vatRegistrationNum;

    /**
     * @Column(name="NUM_1099", type="string", nullable=true, length=30)
     *
     */
    protected $num1099;

    /**
     * @Column(name="FEDERAL_REPORTABLE_FLAG", type="boolean", nullable=true)
     */
    protected $federalReportableFlag;

    /**
     * @Column(name="TYPE_1099", type="string", nullable=true, length=10)
     *
     */
    protected $type1099;

    /**
     * @Column(name="STATE_REPORTABLE_FLAG", type="boolean", nullable=true)
     */
    protected $stateReportableFlag;

    /**
     * @Column(name="ALLOW_AWT_FLAG", type="boolean", nullable=true)
     */
    protected $allowAwtFlag;

    /**
     * @Column(name="MATCH_OPTION_LEVEL", type="string", nullable=false, length=25)
     *
     */
    protected $matchOptionLevel;

    /**
     * @Column(name="PAYMENT_METHOD_LOOKUP_CODE", type="string", nullable=true, length=25)
     *
     */
    protected $paymentMethodLookupCode;

    /**
     * @Column(name="GLOBAL_FLAG", type="boolean", nullable=false)
     */
    protected $globalFlag;

    /**
     * @Column(name="SENSITIVE_VENDOR", type="boolean", nullable=false)
     */
    protected $sensitiveVendor;

    /**
     * @Column(name="GLOBAL_ATTRIBUTE1", type="string", nullable=true, length=30)
     *
     */
    protected $globalAttribute1;

    /**
     * @Column(name="GLOBAL_ATTRIBUTE8", type="string", nullable=true, length=30)
     *
     */
    protected $globalAttribute8;

    /**
     * @Column(name="GLOBAL_ATTRIBUTE9", type="string", nullable=true, length=80)
     *
     */
    protected $globalAttribute9;

    /**
     * @Column(name="GLOBAL_ATTRIBUTE10", type="string", nullable=true, length=80)
     *
     */
    protected $globalAttribute10;

    /**
     * @Column(name="GLOBAL_ATTRIBUTE12", type="string", nullable=true, length=10)
     *
     */
    protected $globalAttribute12;

    /**
     * @Column(name="GLOBAL_ATTRIBUTE15", type="string", nullable=true, length=80)
     *
     */
    protected $globalAttribute15;

    /**
     * @Column(name="GLOBAL_ATTRIBUTE16", type="string", nullable=true, length=30)
     *
     */
    protected $globalAttribute16;

    /**
     * @Column(name="ATTRIBUTE_CATEGORY", type="string", nullable=true, length=30)
     *
     */
    protected $attributeCategory;

    /**
     * @Column(name="ATTRIBUTE1", type="string", nullable=true, length=150)
     *
     */
    protected $attribute1;

    /**
     * @Column(name="ATTRIBUTE2", type="string", nullable=true, length=150)
     *
     */
    protected $attribute2;

    /**
     * @Column(name="ATTRIBUTE3", type="string", nullable=true, length=150)
     *
     */
    protected $attribute3;

    /**
     * @Column(name="ATTRIBUTE4", type="string", nullable=true, length=150)
     *
     */
    protected $attribute4;

    /**
     * @Column(name="ATTRIBUTE5", type="string", nullable=true, length=150)
     *
     */
    protected $attribute5;

    /**
     * @Column(name="ATTRIBUTE6", type="string", nullable=true, length=150)
     *
     */
    protected $attribute6;

    /**
     * @Column(name="ATTRIBUTE7", type="string", nullable=true, length=150)
     *
     */
    protected $attribute7;

    /**
     * @Column(name="ATTRIBUTE8", type="string", nullable=true, length=150)
     *
     */
    protected $attribute8;

    /**
     * @Column(name="ATTRIBUTE9", type="string", nullable=true, length=150)
     *
     */
    protected $attribute9;

    /**
     * @Column(name="ATTRIBUTE10", type="string", nullable=true, length=150)
     *
     */
    protected $attribute10;

    /**
     * @Column(name="ORG_NAME", type="string", nullable=false, length=240)
     *
     */
    protected $orgName;

    /**
     * @Column(name="PA_EXT_ATTRIBUTE1", type="string", nullable=true, length=20)
     *
     */
    protected $paExtAttribute1;

    /**
     * @Column(name="CO_EXT_ATTRIBUTE1", type="string", nullable=true, length=30)
     *
     */
    protected $coExtAttribute1;

    /**
     * @Column(name="CO_EXT_ATTRIBUTE2", type="string", nullable=true, length=3)
     *
     */
    protected $coExtAttribute2;

    /**
     * @Column(name="CO_EXT_ATTRIBUTE3", type="string", nullable=true, length=30)
     *
     */
    protected $coExtAttribute3;

    /**
     * @Column(name="CO_EXT_ATTRIBUTE4", type="string", nullable=true, length=30)
     *
     */
    protected $coExtAttribute4;

    /**
     * @Column(name="CO_EXT_ATTRIBUTE5", type="string", nullable=true, length=30)
     *
     */
    protected $coExtAttribute5;

    /**
     * @Column(name="CO_EXT_ATTRIBUTE6", type="datetime", nullable=true)
     *
     */
    protected $coExtAttribute6;

    /**
     * @Column(name="CO_EXT_ATTRIBUTE7", type="datetime", nullable=true)
     *
     */
    protected $coExtAttribute7;

    /**
     * @Column(name="CO_EXT_ATTRIBUTE8", type="string", nullable=true, length=40)
     *
     */
    protected $coExtAttribute8;

    /**
     * @Column(name="CO_EXT_ATTRIBUTE9", type="string", nullable=true, length=30)
     *
     */
    protected $coExtAttribute9;

    /**
     * @Column(name="ALIASES", type="string", nullable=true, length=3000)
     *
     */
    protected $aliases;

    /**
     * @Column(name="SUPPLIER_TYPE_NI", type="string", nullable=true, length=30)
     *
     */
    protected $supplierTypeNi;

    /**
     * @Column(name="ORDER_NUMBER_GT", type="string", nullable=true, length=150)
     *
     */
    protected $orderNumberGt;

    /**
     * @Column(name="REGISTER_NUMBER_GT", type="string", nullable=true, length=150)
     *
     */
    protected $registerNumberGt;

    /**
     * @Column(name="SUPPLIER_TYPE_SV", type="string", nullable=true, length=150)
     *
     */
    protected $supplierTypeSv;

    /**
     * @Column(name="DOCUMENT_TYPE_SV", type="string", nullable=true, length=20)
     *
     */
    protected $documentTypeSv;

    /**
     * @Column(name="CONTRIBUTOR_REGISTRATION_SV", type="string", nullable=true, length=150)
     *
     */
    protected $contributorRegistrationSv;

    /**
     * @Column(name="LEGAL_REPRESENTATIVE_SV", type="string", nullable=true, length=150)
     *
     */
    protected $legalRepresentativeSv;

    /**
     * @Column(name="PE_EXT_ATTRIBUTE_CATEGORY", type="string", nullable=true, length=15)
     *
     */
    protected $personCategoryPE;

    /**
     * @Column(name="PE_EXT_ATTRIBUTE1", type="string", nullable=true, length=2)
     *
     */
    protected $personTypePE;

    /**
     * @Column(name="PE_EXT_ATTRIBUTE2", type="string", nullable=true, length=2)
     *
     */
    protected $documentTypePE;

    /**
     * @Column(name="PE_EXT_ATTRIBUTE3", type="string", nullable=true, length=150)
     *
     */
    protected $documentNumberPE;

    /**
     * @Column(name="PE_EXT_ATTRIBUTE4", type="string", nullable=true, length=20)
     *
     */
    protected $lasNamePE;

    /**
     * @Column(name="PE_EXT_ATTRIBUTE5", type="string", nullable=true, length=20)
     *
     */
    protected $secondLasNamePE;

    /**
     * @Column(name="PE_EXT_ATTRIBUTE6", type="string", nullable=true, length=20)
     *
     */
    protected $firstNamePE;

    /**
     * @Column(name="PE_EXT_ATTRIBUTE7", type="string", nullable=true, length=20)
     *
     */
    protected $middleNamePE;

    /**
     * @Column(name="PE_EXT_ATTRIBUTE8", type="string", nullable=true, length=80)
     *
     */
    protected $supplierConditionPE;

    /**
     * @Column(name="PE_EXT_ATTRIBUTE9", type="string", nullable=true, length=80)
     *
     */
    protected $withholdingSupplierPE;


    /**
     * @Column(name="PE_EXT_ATTRIBUTE10", type="string", nullable=true, length=4)
     *
     */
    protected $countryCodePE;

    /**
     * @Column(name="PE_EXT_ATTRIBUTE11", type="string", nullable=true, length=80)
     *
     */
    protected $serviceLenderPE;

    /**
     * @Column(name="PE_EXT_ATTRIBUTE12", type="string", nullable=true, length=30)
     *
     */
    protected $conventionPE;

    /**
     * @Column(name="PE_EXT_ATTRIBUTE13", type="string", nullable=true, length=2)
     *
     */
    protected $notResidentDocumentTypePE;

    /**
     * @Column(name="PE_EXT_ATTRIBUTE15", type="string", nullable=true, length=15)
     *
     */
    protected $agreementAvoidDoubleTaxPE;


    /**
     * @Column(name="UY_EXT_ATTRIBUTE1", type="string", nullable=true, length=60)
     *
     */
    protected $contributorTypeUY;




    // GETTERS & SETTERS


    /**
     * @return mixed
     */
    public function getNotResidentDocumentTypePE()
    {
        return $this->notResidentDocumentTypePE;
    }

    /**
     * @param mixed $notResidentDocumentTypePE
     */
    public function setNotResidentDocumentTypePE($notResidentDocumentTypePE)
    {
        $this->notResidentDocumentTypePE = $notResidentDocumentTypePE;
    }

    /**
     * @return mixed
     */
    public function getContributorTypeUY()
    {
        return $this->contributorTypeUY;
    }

    /**
     * @param mixed $contributorTypeUY
     */
    public function setContributorTypeUY($contributorTypeUY)
    {
        $this->contributorTypeUY = $contributorTypeUY;
    }


    //marked for deletion, no time to fix this now
    public function set_8id($val)
    {
        return $this;
    }

    public function get_8id()
    {
        return '';
    }

    /**
     * @return mixed
     */
    public function getPersonCategoryPE()
    {
        return $this->personCategoryPE;
    }

    /**
     * @param mixed $personCategoryPE
     */
    public function setPersonCategoryPE($personCategoryPE)
    {
        $this->personCategoryPE = $personCategoryPE;
    }

    /**
     * @return mixed
     */
    public function getPersonTypePE()
    {
        return $this->personTypePE;
    }

    /**
     * @param mixed $personTypePE
     */
    public function setPersonTypePE($personTypePE)
    {
        $this->personTypePE = $personTypePE;
    }

    /**
     * @return mixed
     */
    public function getDocumentTypePE()
    {
        return $this->documentTypePE;
    }

    /**
     * @param mixed $documentTypePE
     */
    public function setDocumentTypePE($documentTypePE)
    {
        $this->documentTypePE = $documentTypePE;
    }

    /**
     * @return mixed
     */
    public function getDocumentNumberPE()
    {
        return $this->documentNumberPE;
    }

    /**
     * @param mixed $documentNumberPE
     */
    public function setDocumentNumberPE($documentNumberPE)
    {
        $this->documentNumberPE = $documentNumberPE;
    }

    /**
     * @return mixed
     */
    public function getLasNamePE()
    {
        return $this->lasNamePE;
    }

    /**
     * @param mixed $lasNamePE
     */
    public function setLasNamePE($lasNamePE)
    {
        $this->lasNamePE = $lasNamePE;
    }

    /**
     * @return mixed
     */
    public function getSecondLasNamePE()
    {
        return $this->secondLasNamePE;
    }

    /**
     * @param mixed $secondLasNamePE
     */
    public function setSecondLasNamePE($secondLasNamePE)
    {
        $this->secondLasNamePE = $secondLasNamePE;
    }

    /**
     * @return mixed
     */
    public function getFirstNamePE()
    {
        return $this->firstNamePE;
    }

    /**
     * @param mixed $firstNamePE
     */
    public function setFirstNamePE($firstNamePE)
    {
        $this->firstNamePE = $firstNamePE;
    }

    /**
     * @return mixed
     */
    public function getMiddleNamePE()
    {
        return $this->middleNamePE;
    }

    /**
     * @param mixed $middleNamePE
     */
    public function setMiddleNamePE($middleNamePE)
    {
        $this->middleNamePE = $middleNamePE;
    }

    /**
     * @return mixed
     */
    public function getSupplierConditionPE()
    {
        return $this->supplierConditionPE;
    }

    /**
     * @param mixed $supplierConditionPE
     */
    public function setSupplierConditionPE($supplierConditionPE)
    {
        $this->supplierConditionPE = $supplierConditionPE;
    }

    /**
     * @return mixed
     */
    public function getWithholdingSupplierPE()
    {
        return $this->withholdingSupplierPE;
    }

    /**
     * @param mixed $withholdingSupplierPE
     */
    public function setWithholdingSupplierPE($withholdingSupplierPE)
    {
        $this->withholdingSupplierPE = $withholdingSupplierPE;
    }

    /**
     * @return mixed
     */
    public function getCountryCodePE()
    {
        return $this->countryCodePE;
    }

    /**
     * @param mixed $countryCodePE
     */
    public function setCountryCodePE($countryCodePE)
    {
        $this->countryCodePE = $countryCodePE;
    }

    /**
     * @return mixed
     */
    public function getServiceLenderPE()
    {
        return $this->serviceLenderPE;
    }

    /**
     * @param mixed $serviceLenderPE
     */
    public function setServiceLenderPE($serviceLenderPE)
    {
        $this->serviceLenderPE = $serviceLenderPE;
    }

    /**
     * @return mixed
     */
    public function getConventionPE()
    {
        return $this->conventionPE;
    }

    /**
     * @param mixed $conventionPE
     */
    public function setConventionPE($conventionPE)
    {
        $this->conventionPE = $conventionPE;
    }

    /**
     * @return mixed
     */
    public function getAgreementAvoidDoubleTaxPE()
    {
        return $this->agreementAvoidDoubleTaxPE;
    }

    /**
     * @param mixed $agreementAvoidDoubleTaxPE
     */
    public function setAgreementAvoidDoubleTaxPE($agreementAvoidDoubleTaxPE)
    {
        $this->agreementAvoidDoubleTaxPE = $agreementAvoidDoubleTaxPE;
    }

    /**
     * @return mixed
     */
    public function getSiglaPrimaryKey()
    {
        return $this->siglaPrimaryKey;
    }

    /**
     * @param mixed $siglaPrimaryKey
     * @return BaseVendorHeader
     */
    public function setSiglaPrimaryKey($siglaPrimaryKey)
    {
        $this->siglaPrimaryKey = $siglaPrimaryKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVendorName()
    {
        return $this->vendorName;
    }

    /**
     * @param mixed $vendorName
     * @return BaseVendorHeader
     */
    public function setVendorName($vendorName)
    {
        $this->vendorName = $vendorName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSegment1()
    {
        return $this->segment1;
    }

    /**
     * @param mixed $segment1
     * @return BaseVendorHeader
     */
    public function setSegment1($segment1)
    {
        $this->segment1 = $segment1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVendorNameAlt()
    {
        return $this->vendorNameAlt;
    }

    /**
     * @param mixed $vendorNameAlt
     * @return BaseVendorHeader
     */
    public function setVendorNameAlt($vendorNameAlt)
    {
        $this->vendorNameAlt = $vendorNameAlt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVendorTypeLookupCode()
    {
        return $this->vendorTypeLookupCode;
    }

    /**
     * @param mixed $vendorTypeLookupCode
     * @return BaseVendorHeader
     */
    public function setVendorTypeLookupCode($vendorTypeLookupCode)
    {
        $this->vendorTypeLookupCode = $vendorTypeLookupCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVatRegistrationNum()
    {
        return $this->vatRegistrationNum;
    }

    /**
     * @param mixed $vatRegistrationNum
     * @return BaseVendorHeader
     */
    public function setVatRegistrationNum($vatRegistrationNum)
    {
        $this->vatRegistrationNum = $vatRegistrationNum;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNum1099()
    {
        return $this->num1099;
    }

    /**
     * @param mixed $num1099
     * @return BaseVendorHeader
     */
    public function setNum1099($num1099)
    {
        $this->num1099 = $num1099;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFederalReportableFlag()
    {
        return $this->federalReportableFlag;
    }

    /**
     * @param mixed $federalReportableFlag
     * @return BaseVendorHeader
     */
    public function setFederalReportableFlag($federalReportableFlag)
    {
        $this->federalReportableFlag = $federalReportableFlag;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType1099()
    {
        return $this->type1099;
    }

    /**
     * @param mixed $type1099
     * @return BaseVendorHeader
     */
    public function setType1099($type1099)
    {
        $this->type1099 = $type1099;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStateReportableFlag()
    {
        return $this->stateReportableFlag;
    }

    /**
     * @param mixed $stateReportableFlag
     * @return BaseVendorHeader
     */
    public function setStateReportableFlag($stateReportableFlag)
    {
        $this->stateReportableFlag = $stateReportableFlag;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAllowAwtFlag()
    {
        return $this->allowAwtFlag;
    }

    /**
     * @param mixed $allowAwtFlag
     * @return BaseVendorHeader
     */
    public function setAllowAwtFlag($allowAwtFlag)
    {
        $this->allowAwtFlag = $allowAwtFlag;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMatchOptionLevel()
    {
        return $this->matchOptionLevel;
    }

    /**
     * @param mixed $matchOptionLevel
     * @return BaseVendorHeader
     */
    public function setMatchOptionLevel($matchOptionLevel)
    {
        $this->matchOptionLevel = $matchOptionLevel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethodLookupCode()
    {
        return $this->paymentMethodLookupCode;
    }

    /**
     * @param mixed $paymentMethodLookupCode
     * @return BaseVendorHeader
     */
    public function setPaymentMethodLookupCode($paymentMethodLookupCode)
    {
        $this->paymentMethodLookupCode = $paymentMethodLookupCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGlobalFlag()
    {
        return $this->globalFlag;
    }

    /**
     * @param mixed $globalFlag
     * @return BaseVendorHeader
     */
    public function setGlobalFlag($globalFlag)
    {
        $this->globalFlag = $globalFlag;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSensitiveVendor()
    {
        return $this->sensitiveVendor;
    }

    /**
     * @param mixed $sensitiveVendor
     * @return BaseVendorHeader
     */
    public function setSensitiveVendor($sensitiveVendor)
    {
        $this->sensitiveVendor = $sensitiveVendor;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGlobalAttribute1()
    {
        return $this->globalAttribute1;
    }

    /**
     * @param mixed $globalAttribute1
     * @return BaseVendorHeader
     */
    public function setGlobalAttribute1($globalAttribute1)
    {
        $this->globalAttribute1 = $globalAttribute1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGlobalAttribute8()
    {
        return $this->globalAttribute8;
    }

    /**
     * @param mixed $globalAttribute8
     * @return BaseVendorHeader
     */
    public function setGlobalAttribute8($globalAttribute8)
    {
        $this->globalAttribute8 = $globalAttribute8;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGlobalAttribute9()
    {
        return $this->globalAttribute9;
    }

    /**
     * @param mixed $globalAttribute9
     * @return BaseVendorHeader
     */
    public function setGlobalAttribute9($globalAttribute9)
    {
        $this->globalAttribute9 = $globalAttribute9;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGlobalAttribute10()
    {
        return $this->globalAttribute10;
    }

    /**
     * @param mixed $globalAttribute10
     * @return BaseVendorHeader
     */
    public function setGlobalAttribute10($globalAttribute10)
    {
        $this->globalAttribute10 = $globalAttribute10;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGlobalAttribute12()
    {
        return $this->globalAttribute12;
    }

    /**
     * @param mixed $globalAttribute12
     * @return BaseVendorHeader
     */
    public function setGlobalAttribute12($globalAttribute12)
    {
        $this->globalAttribute12 = $globalAttribute12;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGlobalAttribute15()
    {
        return $this->globalAttribute15;
    }

    /**
     * @param mixed $globalAttribute15
     * @return BaseVendorHeader
     */
    public function setGlobalAttribute15($globalAttribute15)
    {
        $this->globalAttribute15 = $globalAttribute15;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGlobalAttribute16()
    {
        return $this->globalAttribute16;
    }

    /**
     * @param mixed $globalAttribute16
     * @return BaseVendorHeader
     */
    public function setGlobalAttribute16($globalAttribute16)
    {
        $this->globalAttribute16 = $globalAttribute16;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttributeCategory()
    {
        return $this->attributeCategory;
    }

    /**
     * @param mixed $attributeCategory
     * @return BaseVendorHeader
     */
    public function setAttributeCategory($attributeCategory)
    {
        $this->attributeCategory = $attributeCategory;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttribute1()
    {
        return $this->attribute1;
    }

    /**
     * @param mixed $attribute1
     * @return BaseVendorHeader
     */
    public function setAttribute1($attribute1)
    {
        $this->attribute1 = $attribute1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttribute2()
    {
        return $this->attribute2;
    }

    /**
     * @param mixed $attribute2
     * @return BaseVendorHeader
     */
    public function setAttribute2($attribute2)
    {
        $this->attribute2 = $attribute2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttribute3()
    {
        return $this->attribute3;
    }

    /**
     * @param mixed $attribute3
     * @return BaseVendorHeader
     */
    public function setAttribute3($attribute3)
    {
        $this->attribute3 = $attribute3;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttribute4()
    {
        return $this->attribute4;
    }

    /**
     * @param mixed $attribute4
     * @return BaseVendorHeader
     */
    public function setAttribute4($attribute4)
    {
        $this->attribute4 = $attribute4;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttribute5()
    {
        return $this->attribute5;
    }

    /**
     * @param mixed $attribute5
     * @return BaseVendorHeader
     */
    public function setAttribute5($attribute5)
    {
        $this->attribute5 = $attribute5;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttribute6()
    {
        return $this->attribute6;
    }

    /**
     * @param mixed $attribute6
     * @return BaseVendorHeader
     */
    public function setAttribute6($attribute6)
    {
        $this->attribute6 = $attribute6;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttribute7()
    {
        return $this->attribute7;
    }

    /**
     * @param mixed $attribute7
     * @return BaseVendorHeader
     */
    public function setAttribute7($attribute7)
    {
        $this->attribute7 = $attribute7;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttribute8()
    {
        return $this->attribute8;
    }

    /**
     * @param mixed $attribute8
     * @return BaseVendorHeader
     */
    public function setAttribute8($attribute8)
    {
        $this->attribute8 = $attribute8;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttribute9()
    {
        return $this->attribute9;
    }

    /**
     * @param mixed $attribute9
     * @return BaseVendorHeader
     */
    public function setAttribute9($attribute9)
    {
        $this->attribute9 = $attribute9;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttribute10()
    {
        return $this->attribute10;
    }

    /**
     * @param mixed $attribute10
     * @return BaseVendorHeader
     */
    public function setAttribute10($attribute10)
    {
        $this->attribute10 = $attribute10;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrgName()
    {
        return $this->orgName;
    }

    /**
     * @param mixed $orgName
     * @return BaseVendorHeader
     */
    public function setOrgName($orgName)
    {
        $this->orgName = $orgName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaExtAttribute1()
    {
        return $this->paExtAttribute1;
    }

    /**
     * @param mixed $paExtAttribute1
     * @return BaseVendorHeader
     */
    public function setPaExtAttribute1($paExtAttribute1)
    {
        $this->paExtAttribute1 = $paExtAttribute1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoExtAttribute1()
    {
        return $this->coExtAttribute1;
    }

    /**
     * @param mixed $coExtAttribute1
     * @return BaseVendorHeader
     */
    public function setCoExtAttribute1($coExtAttribute1)
    {
        $this->coExtAttribute1 = $coExtAttribute1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoExtAttribute2()
    {
        return $this->coExtAttribute2;
    }

    /**
     * @param mixed $coExtAttribute2
     * @return BaseVendorHeader
     */
    public function setCoExtAttribute2($coExtAttribute2)
    {
        $this->coExtAttribute2 = $coExtAttribute2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoExtAttribute3()
    {
        return $this->coExtAttribute3;
    }

    /**
     * @param mixed $coExtAttribute3
     * @return BaseVendorHeader
     */
    public function setCoExtAttribute3($coExtAttribute3)
    {
        $this->coExtAttribute3 = $coExtAttribute3;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoExtAttribute4()
    {
        return $this->coExtAttribute4;
    }

    /**
     * @param mixed $coExtAttribute4
     * @return BaseVendorHeader
     */
    public function setCoExtAttribute4($coExtAttribute4)
    {
        $this->coExtAttribute4 = $coExtAttribute4;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoExtAttribute5()
    {
        return $this->coExtAttribute5;
    }

    /**
     * @param mixed $coExtAttribute5
     * @return BaseVendorHeader
     */
    public function setCoExtAttribute5($coExtAttribute5)
    {
        $this->coExtAttribute5 = $coExtAttribute5;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoExtAttribute6()
    {
        return $this->coExtAttribute6;
    }

    /**
     * @param mixed $coExtAttribute6
     * @return BaseVendorHeader
     */
    public function setCoExtAttribute6($coExtAttribute6)
    {
        $this->coExtAttribute6 = $coExtAttribute6;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoExtAttribute7()
    {
        return $this->coExtAttribute7;
    }

    /**
     * @param mixed $coExtAttribute7
     * @return BaseVendorHeader
     */
    public function setCoExtAttribute7($coExtAttribute7)
    {
        $this->coExtAttribute7 = $coExtAttribute7;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoExtAttribute8()
    {
        return $this->coExtAttribute8;
    }

    /**
     * @param mixed $coExtAttribute8
     * @return BaseVendorHeader
     */
    public function setCoExtAttribute8($coExtAttribute8)
    {
        $this->coExtAttribute8 = $coExtAttribute8;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoExtAttribute9()
    {
        return $this->coExtAttribute9;
    }

    /**
     * @param mixed $coExtAttribute9
     * @return BaseVendorHeader
     */
    public function setCoExtAttribute9($coExtAttribute9)
    {
        $this->coExtAttribute9 = $coExtAttribute9;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAliases()
    {
        return $this->aliases;
    }

    /**
     * @param mixed $aliases
     * @return BaseVendorHeader
     */
    public function setAliases($aliases)
    {
        $this->aliases = $aliases;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSupplierTypeNi()
    {
        return $this->supplierTypeNi;
    }

    /**
     * @param mixed $supplierTypeNi
     * @return BaseVendorHeader
     */
    public function setSupplierTypeNi($supplierTypeNi)
    {
        $this->supplierTypeNi = $supplierTypeNi;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderNumberGt()
    {
        return $this->orderNumberGt;
    }

    /**
     * @param mixed $orderNumberGt
     * @return BaseVendorHeader
     */
    public function setOrderNumberGt($orderNumberGt)
    {
        $this->orderNumberGt = $orderNumberGt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegisterNumberGt()
    {
        return $this->registerNumberGt;
    }

    /**
     * @param mixed $registerNumberGt
     * @return BaseVendorHeader
     */
    public function setRegisterNumberGt($registerNumberGt)
    {
        $this->registerNumberGt = $registerNumberGt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSupplierTypeSv()
    {
        return $this->supplierTypeSv;
    }

    /**
     * @param mixed $supplierTypeSv
     * @return BaseVendorHeader
     */
    public function setSupplierTypeSv($supplierTypeSv)
    {
        $this->supplierTypeSv = $supplierTypeSv;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDocumentTypeSv()
    {
        return $this->documentTypeSv;
    }

    /**
     * @param mixed $documentTypeSv
     * @return BaseVendorHeader
     */
    public function setDocumentTypeSv($documentTypeSv)
    {
        $this->documentTypeSv = $documentTypeSv;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContributorRegistrationSv()
    {
        return $this->contributorRegistrationSv;
    }

    /**
     * @param mixed $contributorRegistrationSv
     * @return BaseVendorHeader
     */
    public function setContributorRegistrationSv($contributorRegistrationSv)
    {
        $this->contributorRegistrationSv = $contributorRegistrationSv;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLegalRepresentativeSv()
    {
        return $this->legalRepresentativeSv;
    }

    /**
     * @param mixed $legalRepresentativeSv
     * @return BaseVendorHeader
     */
    public function setLegalRepresentativeSv($legalRepresentativeSv)
    {
        $this->legalRepresentativeSv = $legalRepresentativeSv;
        return $this;
    }



}
