<?php
namespace Commons\Base;

use Commons\RAWBase;

class BaseVendorSite extends RAWBase
{
    public function __construct()
    {
    }

    /**
     * @Column(name="VENDOR_NUMBER", type="string", nullable=false, length=30)
     *
     */
    protected $vendorNumber;


    /**
     * @Column(name="VENDOR_SITE_CODE", type="string", nullable=false, length=15)
     *
     */
    protected $vendorSiteCode;

    /**
     * @Column(name="VENDOR_SITE_CODE_ALT", type="string", nullable=true, length=320)
     *
     */
    protected $vendorSiteCodeAlt;

    /**
     * @Column(name="ADDRESS_LINE1", type="string", nullable=false, length=150)
     *
     */
    protected $addressLine1;

    /**
     * @Column(name="ADDRESS_LINE2", type="string", nullable=true, length=150)
     *
     */
    protected $addressLine2;

    /**
     * @Column(name="ADDRESS_LINE3", type="string", nullable=true, length=240)
     *
     */
    protected $addressLine3;

    /**
     * @Column(name="ADDRESS_LINE4", type="string", nullable=true, length=240)
     *
     */
    protected $addressLine4;

    /**
     * @Column(name="ADDRESS_LINE_ALT", type="string", nullable=true, length=560)
     *
     */
    protected $addressLineAlt;

    /**
     * @Column(name="CITY", type="string", nullable=false, length=30)
     *
     */
    protected $city;

    /**
     * @Column(name="COUNTY", type="string", nullable=true, length=150)
     *
     */
    protected $county;

    /**
     * @Column(name="STATE", type="string", nullable=false, length=50)
     *
     */
    protected $state;

    /**
     * @Column(name="PROVINCE", type="string", nullable=true, length=150)
     *
     */
    protected $province;

    /**
     * @Column(name="ZIP", type="string", nullable=true, length=20)
     *
     */
    protected $zip;

    /**
     * @Column(name="ADDRESS_COUNTRY", type="string", nullable=false, length=25)
     *
     */
    protected $addressCountry;

    /**
     * @Column(name="AREA_CODE", type="string", nullable=true, length=15)
     *
     */
    protected $areaCode;

    /**
     * @Column(name="PHONE", type="string", nullable=true, length=15)
     *
     */
    protected $phone;

    /**
     * @Column(name="FAX_AREA_CODE", type="string", nullable=true, length=15)
     *
     */
    protected $faxAreaCode;

    /**
     * @Column(name="FAX", type="string", nullable=true, length=15)
     *
     */
    protected $fax;

    /**
     * @Column(name="PAYMENT_METHOD_LOOKUP_CODE", type="string", nullable=true, length=25)
     *
     */
    protected $paymentMethodLookupCode;

    /**
     * @Column(name="VAT_CODE", type="string", nullable=true, length=20)
     *
     */
    protected $vatCode;

    /**
     * @Column(name="ACCTS_PAY_ACCOUNT", type="string", nullable=true, length=150)
     *
     */
    protected $acctsPayAccount;

    /**
     * @Column(name="PREPAY_ACCOUNT", type="string", nullable=true, length=150)
     *
     */
    protected $prepayAccount;

    /**
     * @Column(name="PAY_GROUP_LOOKUP_CODE", type="string", nullable=true, length=25)
     *
     */
    protected $payGroupLookupCode;

    /**
     * @Column(name="INVOICE_CURRENCY_CODE", type="string", nullable=true, length=15)
     *
     */
    protected $invoiceCurrencyCode;

    /**
     * @Column(name="LANGUAGE", type="string", nullable=true, length=10)
     *
     */
    protected $language;

    /**
     * @Column(name="TERMS_NAME", type="string", nullable=true, length=200)
     *
     */
    protected $termsName;

    /**
     * @Column(name="EMAIL_ADDRESS", type="string", nullable=true, length=150)
     *
     */
    protected $emailAddress;

    /**
     * @Column(name="PURCHASING_SITE_FLAG", type="boolean", nullable=true)
     */
    protected $purchasingSiteFlag;

    /**
     * @Column(name="PAY_SITE_FLAG", type="boolean", nullable=true)
     */
    protected $paySiteFlag;

    /**
     * @Column(name="VAT_REGISTRATION_NUM", type="string", nullable=true, length=20)
     *
     */
    protected $vatRegistrationNum;

    /**
     * @Column(name="SHIP_TO_LOCATION_CODE", type="string", nullable=true, length=60)
     *
     */
    protected $shipToLocationCode;

    /**
     * @Column(name="PRIMARY_PAY_SITE_FLAG", type="boolean", nullable=true)
     */
    protected $primaryPaySiteFlag;

    /**
     * @Column(name="GAPLESS_INV_NUM_FLAG", type="boolean", nullable=true)
     */
    protected $gaplessInvNumFlag;

    /**
     * @Column(name="OFFSET_TAX_FLAG", type="boolean", nullable=true)
     */
    protected $offsetTaxFlag;

    /**
     * @Column(name="ALWAYS_TAKE_DISC_FLAG", type="boolean", nullable=true)
     */
    protected $alwaysTakeDiscFlag;

    /**
     * @Column(name="EXCLUDE_FREIGHT_FROM_DISCOUNT", type="boolean", nullable=true)
     */
    protected $excludeFreightFromDiscount;

    /**
     * @Column(name="GLOBAL_ATTRIBUTE17", type="boolean", nullable=true)
     */
    protected $globalAttribute17;

    /**
     * @Column(name="GLOBAL_ATTRIBUTE18", type="boolean", nullable=true)
     */
    protected $globalAttribute18;

    /**
     * @Column(name="GLOBAL_ATTRIBUTE19", type="float", nullable=true)
     *
     */
    protected $globalAttribute19;

    /**
     * @Column(name="GLOBAL_ATTRIBUTE20", type="datetime", nullable=true)
     *
     */
    protected $globalAttribute20;

    /**
     * @Column(name="ATTRIBUTE_CATEGORY", type="string", nullable=true, length=30)
     *
     */
    protected $attributeCategory;

    /**
     * @Column(name="ATTRIBUTE1", type="string", nullable=true, length=150)
     *
     */
    protected $attribute1;

    /**
     * @Column(name="ATTRIBUTE2", type="string", nullable=true, length=150)
     *
     */
    protected $attribute2;

    /**
     * @Column(name="ATTRIBUTE3", type="string", nullable=true, length=150)
     *
     */
    protected $attribute3;

    /**
     * @Column(name="ATTRIBUTE4", type="string", nullable=true, length=150)
     *
     */
    protected $attribute4;

    /**
     * @Column(name="ATTRIBUTE5", type="string", nullable=true, length=150)
     *
     */
    protected $attribute5;

    /**
     * @Column(name="ATTRIBUTE6", type="string", nullable=true, length=150)
     *
     */
    protected $attribute6;

    /**
     * @Column(name="ATTRIBUTE7", type="string", nullable=true, length=150)
     *
     */
    protected $attribute7;

    /**
     * @Column(name="ATTRIBUTE8", type="string", nullable=true, length=150)
     *
     */
    protected $attribute8;

    /**
     * @Column(name="ATTRIBUTE9", type="string", nullable=true, length=150)
     *
     */
    protected $attribute9;

    /**
     * @Column(name="ATTRIBUTE10", type="string", nullable=true, length=150)
     *
     */
    protected $attribute10;

    /**
     * @Column(name="ORG_NAME", type="string", nullable=false, length=240)
     *
     */
    protected $orgName;

    /**
     * @Column(name="CLASSIFICATION_TYPE_CODE", type="string", nullable=true, length=30)
     *
     */
    protected $classificationTypeCode;

    /**
     * @Column(name="CLASSIFICATION_CODE", type="string", nullable=true, length=30)
     *
     */
    protected $classificationCode;

    /**
     * @Column(name="CLASSIFICATION_START_DATE", type="datetime", nullable=true)
     *
     */
    protected $classificationStartDate;

    /**
     * @Column(name="CO_EXT_ATTRIBUTE1", type="string", nullable=true, length=15)
     *
     */
    protected $coExtAttribute1;

    /**
     * @Column(name="CONCEPT_CODE", type="string", nullable=true, length=30)
     *
     */
    protected $conceptCode;

    /**
     * @Column(name="PE_EXT_ATTRIBUTE1_CC", type="string", nullable=true, length=100)
     *
     */
    protected $exchangePaymentPE;

    /**
     * @Column(name="PE_EXT_ATTRIBUTE4", type="string", nullable=true, length=80)
     *
     */
    protected $primarySitePE;

    /**
     * @Column(name="NON_RESIDENT_ADDRESS_TYPE_PE", type="string", nullable=true, length=30)
     *
     */
    protected $nonResidentAddressTypePE;

    /**
     * @Column(name="NON_RESIDENT_CERT_TYPE_PE", type="string", nullable=true, length=30)
     *
     */
    protected $nonResidentCertNumberPE;

    /**
     * @Column(name="PRINCIPAL_SITES_SV", type="string", nullable=true, length=80)
     *
     */
    protected $principalSitesSV;

    /**
     * @return mixed
     */
    public function getVendorNumber()
    {
        return $this->vendorNumber;
    }

    /**
     * @param mixed $vendorNumber
     */
    public function setVendorNumber($vendorNumber)
    {
        $this->vendorNumber = $vendorNumber;
    }

    /**
     * @return mixed
     */
    public function getVendorSiteCode()
    {
        return $this->vendorSiteCode;
    }

    /**
     * @param mixed $vendorSiteCode
     */
    public function setVendorSiteCode($vendorSiteCode)
    {
        $this->vendorSiteCode = $vendorSiteCode;
    }

    /**
     * @return mixed
     */
    public function getVendorSiteCodeAlt()
    {
        return $this->vendorSiteCodeAlt;
    }

    /**
     * @param mixed $vendorSiteCodeAlt
     */
    public function setVendorSiteCodeAlt($vendorSiteCodeAlt)
    {
        $this->vendorSiteCodeAlt = $vendorSiteCodeAlt;
    }

    /**
     * @return mixed
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * @param mixed $addressLine1
     */
    public function setAddressLine1($addressLine1)
    {
        $this->addressLine1 = $addressLine1;
    }

    /**
     * @return mixed
     */
    public function getAddressLine2()
    {
        return $this->addressLine2;
    }

    /**
     * @param mixed $addressLine2
     */
    public function setAddressLine2($addressLine2)
    {
        $this->addressLine2 = $addressLine2;
    }

    /**
     * @return mixed
     */
    public function getAddressLine3()
    {
        return $this->addressLine3;
    }

    /**
     * @param mixed $addressLine3
     */
    public function setAddressLine3($addressLine3)
    {
        $this->addressLine3 = $addressLine3;
    }

    /**
     * @return mixed
     */
    public function getAddressLine4()
    {
        return $this->addressLine4;
    }

    /**
     * @param mixed $addressLine4
     */
    public function setAddressLine4($addressLine4)
    {
        $this->addressLine4 = $addressLine4;
    }

    /**
     * @return mixed
     */
    public function getAddressLineAlt()
    {
        return $this->addressLineAlt;
    }

    /**
     * @param mixed $addressLineAlt
     */
    public function setAddressLineAlt($addressLineAlt)
    {
        $this->addressLineAlt = $addressLineAlt;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param mixed $county
     */
    public function setCounty($county)
    {
        $this->county = $county;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * @param mixed $province
     */
    public function setProvince($province)
    {
        $this->province = $province;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return mixed
     */
    public function getAddressCountry()
    {
        return $this->addressCountry;
    }

    /**
     * @param mixed $addressCountry
     */
    public function setAddressCountry($addressCountry)
    {
        $this->addressCountry = $addressCountry;
    }

    /**
     * @return mixed
     */
    public function getAreaCode()
    {
        return $this->areaCode;
    }

    /**
     * @param mixed $areaCode
     */
    public function setAreaCode($areaCode)
    {
        $this->areaCode = $areaCode;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getFaxAreaCode()
    {
        return $this->faxAreaCode;
    }

    /**
     * @param mixed $faxAreaCode
     */
    public function setFaxAreaCode($faxAreaCode)
    {
        $this->faxAreaCode = $faxAreaCode;
    }

    /**
     * @return mixed
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param mixed $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethodLookupCode()
    {
        return $this->paymentMethodLookupCode;
    }

    /**
     * @param mixed $paymentMethodLookupCode
     */
    public function setPaymentMethodLookupCode($paymentMethodLookupCode)
    {
        $this->paymentMethodLookupCode = $paymentMethodLookupCode;
    }

    /**
     * @return mixed
     */
    public function getVatCode()
    {
        return $this->vatCode;
    }

    /**
     * @param mixed $vatCode
     */
    public function setVatCode($vatCode)
    {
        $this->vatCode = $vatCode;
    }

    /**
     * @return mixed
     */
    public function getAcctsPayAccount()
    {
        return $this->acctsPayAccount;
    }

    /**
     * @param mixed $acctsPayAccount
     */
    public function setAcctsPayAccount($acctsPayAccount)
    {
        $this->acctsPayAccount = $acctsPayAccount;
    }

    /**
     * @return mixed
     */
    public function getPrepayAccount()
    {
        return $this->prepayAccount;
    }

    /**
     * @param mixed $prepayAccount
     */
    public function setPrepayAccount($prepayAccount)
    {
        $this->prepayAccount = $prepayAccount;
    }

    /**
     * @return mixed
     */
    public function getPayGroupLookupCode()
    {
        return $this->payGroupLookupCode;
    }

    /**
     * @param mixed $payGroupLookupCode
     */
    public function setPayGroupLookupCode($payGroupLookupCode)
    {
        $this->payGroupLookupCode = $payGroupLookupCode;
    }

    /**
     * @return mixed
     */
    public function getInvoiceCurrencyCode()
    {
        return $this->invoiceCurrencyCode;
    }

    /**
     * @param mixed $invoiceCurrencyCode
     */
    public function setInvoiceCurrencyCode($invoiceCurrencyCode)
    {
        $this->invoiceCurrencyCode = $invoiceCurrencyCode;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getTermsName()
    {
        return $this->termsName;
    }

    /**
     * @param mixed $termsName
     */
    public function setTermsName($termsName)
    {
        $this->termsName = $termsName;
    }

    /**
     * @return mixed
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * @param mixed $emailAddress
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return mixed
     */
    public function getPurchasingSiteFlag()
    {
        return $this->purchasingSiteFlag;
    }

    /**
     * @param mixed $purchasingSiteFlag
     */
    public function setPurchasingSiteFlag($purchasingSiteFlag)
    {
        $this->purchasingSiteFlag = $purchasingSiteFlag;
    }

    /**
     * @return mixed
     */
    public function getPaySiteFlag()
    {
        return $this->paySiteFlag;
    }

    /**
     * @param mixed $paySiteFlag
     */
    public function setPaySiteFlag($paySiteFlag)
    {
        $this->paySiteFlag = $paySiteFlag;
    }

    /**
     * @return mixed
     */
    public function getVatRegistrationNum()
    {
        return $this->vatRegistrationNum;
    }

    /**
     * @param mixed $vatRegistrationNum
     */
    public function setVatRegistrationNum($vatRegistrationNum)
    {
        $this->vatRegistrationNum = $vatRegistrationNum;
    }

    /**
     * @return mixed
     */
    public function getShipToLocationCode()
    {
        return $this->shipToLocationCode;
    }

    /**
     * @param mixed $shipToLocationCode
     */
    public function setShipToLocationCode($shipToLocationCode)
    {
        $this->shipToLocationCode = $shipToLocationCode;
    }

    /**
     * @return mixed
     */
    public function getPrimaryPaySiteFlag()
    {
        return $this->primaryPaySiteFlag;
    }

    /**
     * @param mixed $primaryPaySiteFlag
     */
    public function setPrimaryPaySiteFlag($primaryPaySiteFlag)
    {
        $this->primaryPaySiteFlag = $primaryPaySiteFlag;
    }

    /**
     * @return mixed
     */
    public function getGaplessInvNumFlag()
    {
        return $this->gaplessInvNumFlag;
    }

    /**
     * @param mixed $gaplessInvNumFlag
     */
    public function setGaplessInvNumFlag($gaplessInvNumFlag)
    {
        $this->gaplessInvNumFlag = $gaplessInvNumFlag;
    }

    /**
     * @return mixed
     */
    public function getOffsetTaxFlag()
    {
        return $this->offsetTaxFlag;
    }

    /**
     * @param mixed $offsetTaxFlag
     */
    public function setOffsetTaxFlag($offsetTaxFlag)
    {
        $this->offsetTaxFlag = $offsetTaxFlag;
    }

    /**
     * @return mixed
     */
    public function getAlwaysTakeDiscFlag()
    {
        return $this->alwaysTakeDiscFlag;
    }

    /**
     * @param mixed $alwaysTakeDiscFlag
     */
    public function setAlwaysTakeDiscFlag($alwaysTakeDiscFlag)
    {
        $this->alwaysTakeDiscFlag = $alwaysTakeDiscFlag;
    }

    /**
     * @return mixed
     */
    public function getExcludeFreightFromDiscount()
    {
        return $this->excludeFreightFromDiscount;
    }

    /**
     * @param mixed $excludeFreightFromDiscount
     */
    public function setExcludeFreightFromDiscount($excludeFreightFromDiscount)
    {
        $this->excludeFreightFromDiscount = $excludeFreightFromDiscount;
    }

    /**
     * @return mixed
     */
    public function getGlobalAttribute17()
    {
        return $this->globalAttribute17;
    }

    /**
     * @param mixed $globalAttribute17
     */
    public function setGlobalAttribute17($globalAttribute17)
    {
        $this->globalAttribute17 = $globalAttribute17;
    }

    /**
     * @return mixed
     */
    public function getGlobalAttribute18()
    {
        return $this->globalAttribute18;
    }

    /**
     * @param mixed $globalAttribute18
     */
    public function setGlobalAttribute18($globalAttribute18)
    {
        $this->globalAttribute18 = $globalAttribute18;
    }

    /**
     * @return mixed
     */
    public function getGlobalAttribute19()
    {
        return $this->globalAttribute19;
    }

    /**
     * @param mixed $globalAttribute19
     */
    public function setGlobalAttribute19($globalAttribute19)
    {
        $this->globalAttribute19 = $globalAttribute19;
    }

    /**
     * @return mixed
     */
    public function getGlobalAttribute20()
    {
        return $this->globalAttribute20;
    }

    /**
     * @param mixed $globalAttribute20
     */
    public function setGlobalAttribute20($globalAttribute20)
    {
        $this->globalAttribute20 = $globalAttribute20;
    }

    /**
     * @return mixed
     */
    public function getAttributeCategory()
    {
        return $this->attributeCategory;
    }

    /**
     * @param mixed $attributeCategory
     */
    public function setAttributeCategory($attributeCategory)
    {
        $this->attributeCategory = $attributeCategory;
    }

    /**
     * @return mixed
     */
    public function getAttribute1()
    {
        return $this->attribute1;
    }

    /**
     * @param mixed $attribute1
     */
    public function setAttribute1($attribute1)
    {
        $this->attribute1 = $attribute1;
    }

    /**
     * @return mixed
     */
    public function getAttribute2()
    {
        return $this->attribute2;
    }

    /**
     * @param mixed $attribute2
     */
    public function setAttribute2($attribute2)
    {
        $this->attribute2 = $attribute2;
    }

    /**
     * @return mixed
     */
    public function getAttribute3()
    {
        return $this->attribute3;
    }

    /**
     * @param mixed $attribute3
     */
    public function setAttribute3($attribute3)
    {
        $this->attribute3 = $attribute3;
    }

    /**
     * @return mixed
     */
    public function getAttribute4()
    {
        return $this->attribute4;
    }

    /**
     * @param mixed $attribute4
     */
    public function setAttribute4($attribute4)
    {
        $this->attribute4 = $attribute4;
    }

    /**
     * @return mixed
     */
    public function getAttribute5()
    {
        return $this->attribute5;
    }

    /**
     * @param mixed $attribute5
     */
    public function setAttribute5($attribute5)
    {
        $this->attribute5 = $attribute5;
    }

    /**
     * @return mixed
     */
    public function getAttribute6()
    {
        return $this->attribute6;
    }

    /**
     * @param mixed $attribute6
     */
    public function setAttribute6($attribute6)
    {
        $this->attribute6 = $attribute6;
    }

    /**
     * @return mixed
     */
    public function getAttribute7()
    {
        return $this->attribute7;
    }

    /**
     * @param mixed $attribute7
     */
    public function setAttribute7($attribute7)
    {
        $this->attribute7 = $attribute7;
    }

    /**
     * @return mixed
     */
    public function getAttribute8()
    {
        return $this->attribute8;
    }

    /**
     * @param mixed $attribute8
     */
    public function setAttribute8($attribute8)
    {
        $this->attribute8 = $attribute8;
    }

    /**
     * @return mixed
     */
    public function getAttribute9()
    {
        return $this->attribute9;
    }

    /**
     * @param mixed $attribute9
     */
    public function setAttribute9($attribute9)
    {
        $this->attribute9 = $attribute9;
    }

    /**
     * @return mixed
     */
    public function getAttribute10()
    {
        return $this->attribute10;
    }

    /**
     * @param mixed $attribute10
     */
    public function setAttribute10($attribute10)
    {
        $this->attribute10 = $attribute10;
    }

    /**
     * @return mixed
     */
    public function getOrgName()
    {
        return $this->orgName;
    }

    /**
     * @param mixed $orgName
     */
    public function setOrgName($orgName)
    {
        $this->orgName = $orgName;
    }

    /**
     * @return mixed
     */
    public function getClassificationTypeCode()
    {
        return $this->classificationTypeCode;
    }

    /**
     * @param mixed $classificationTypeCode
     */
    public function setClassificationTypeCode($classificationTypeCode)
    {
        $this->classificationTypeCode = $classificationTypeCode;
    }

    /**
     * @return mixed
     */
    public function getClassificationCode()
    {
        return $this->classificationCode;
    }

    /**
     * @param mixed $classificationCode
     */
    public function setClassificationCode($classificationCode)
    {
        $this->classificationCode = $classificationCode;
    }

    /**
     * @return mixed
     */
    public function getClassificationStartDate()
    {
        return $this->classificationStartDate;
    }

    /**
     * @param mixed $classificationStartDate
     */
    public function setClassificationStartDate($classificationStartDate)
    {
        $this->classificationStartDate = $classificationStartDate;
    }

    /**
     * @return mixed
     */
    public function getCoExtAttribute1()
    {
        return $this->coExtAttribute1;
    }

    /**
     * @param mixed $coExtAttribute1
     */
    public function setCoExtAttribute1($coExtAttribute1)
    {
        $this->coExtAttribute1 = $coExtAttribute1;
    }

    /**
     * @return mixed
     */
    public function getConceptCode()
    {
        return $this->conceptCode;
    }

    /**
     * @param mixed $conceptCode
     */
    public function setConceptCode($conceptCode)
    {
        $this->conceptCode = $conceptCode;
    }

    /**
     * @return mixed
     */
    public function getExchangePaymentPE()
    {
        return $this->exchangePaymentPE;
    }

    /**
     * @param mixed $exchangePaymentPE
     */
    public function setExchangePaymentPE($exchangePaymentPE)
    {
        $this->exchangePaymentPE = $exchangePaymentPE;
    }

    /**
     * @return mixed
     */
    public function getPrimarySitePE()
    {
        return $this->primarySitePE;
    }

    /**
     * @param mixed $primarySitePE
     */
    public function setPrimarySitePE($primarySitePE)
    {
        $this->primarySitePE = $primarySitePE;
    }

    /**
     * @return mixed
     */
    public function getNonResidentAddressTypePE()
    {
        return $this->nonResidentAddressTypePE;
    }

    /**
     * @param mixed $nonResidentAddressTypePE
     */
    public function setNonResidentAddressTypePE($nonResidentAddressTypePE)
    {
        $this->nonResidentAddressTypePE = $nonResidentAddressTypePE;
    }

    /**
     * @return mixed
     */
    public function getNonResidentCertNumberPE()
    {
        return $this->nonResidentCertNumberPE;
    }

    /**
     * @param mixed $nonResidentCertNumberPE
     */
    public function setNonResidentCertNumberPE($nonResidentCertNumberPE)
    {
        $this->nonResidentCertNumberPE = $nonResidentCertNumberPE;
    }

    /**
     * @return mixed
     */
    public function getPrincipalSitesSV()
    {
        return $this->principalSitesSV;
    }

    /**
     * @param mixed $principalSitesSV
     */
    public function setPrincipalSitesSV($principalSitesSV)
    {
        $this->principalSitesSV = $principalSitesSV;
    }



}
