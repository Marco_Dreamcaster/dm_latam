<?php
namespace Commons\Base;

use Commons\RAWBase;

class BaseVendorSiteContact extends RAWBase
{
    public function __construct()
    {
    }

    // vendorNumber generic STRING template*/
    /**
     * @Column(name="VENDOR_NUMBER", type="string", nullable=false, length=30)
     *
     */
    protected $vendorNumber;
    public function getVendorNumber(){
        return $this->vendorNumber;
    }
    public function setVendorNumber($vendorNumber){
        $this->vendorNumber = $vendorNumber;
    }

    // vendorSiteCode generic STRING template*/
    /**
     * @Column(name="VENDOR_SITE_CODE", type="string", nullable=false, length=25)
     *
     */
    protected $vendorSiteCode;
    public function getVendorSiteCode(){
        return $this->vendorSiteCode;
    }
    public function setVendorSiteCode($vendorSiteCode){
        $this->vendorSiteCode = $vendorSiteCode;
    }

    // prefix generic STRING template*/
    /**
     * @Column(name="PREFIX", type="string", nullable=true, length=30)
     *
     */
    protected $prefix;
    public function getPrefix(){
        return $this->prefix;
    }
    public function setPrefix($prefix){
        $this->prefix = $prefix;
    }

    // firstName generic STRING template*/
    /**
     * @Column(name="FIRST_NAME", type="string", nullable=true, length=15)
     *
     */
    protected $firstName;
    public function getFirstName(){
        return $this->firstName;
    }
    public function setFirstName($firstName){
        $this->firstName = $firstName;
    }

    // middleName generic STRING template*/
    /**
     * @Column(name="MIDDLE_NAME", type="string", nullable=true, length=15)
     *
     */
    protected $middleName;
    public function getMiddleName(){
        return $this->middleName;
    }
    public function setMiddleName($middleName){
        $this->middleName = $middleName;
    }

    // lastName generic STRING template*/
    /**
     * @Column(name="LAST_NAME", type="string", nullable=false, length=60)
     *
     */
    protected $lastName;
    public function getLastName(){
        return $this->lastName;
    }
    public function setLastName($lastName){
        $this->lastName = $lastName;
    }

    // department generic STRING template*/
    /**
     * @Column(name="DEPARTMENT", type="string", nullable=true, length=230)
     *
     */
    protected $department;
    public function getDepartment(){
        return $this->department;
    }
    public function setDepartment($department){
        $this->department = $department;
    }

    // phoneAreaCode generic STRING template*/
    /**
     * @Column(name="PHONE_AREA_CODE", type="string", nullable=true, length=10)
     *
     */
    protected $phoneAreaCode;
    public function getPhoneAreaCode(){
        return $this->phoneAreaCode;
    }
    public function setPhoneAreaCode($phoneAreaCode){
        $this->phoneAreaCode = $phoneAreaCode;
    }

    // phoneNumber generic STRING template*/
    /**
     * @Column(name="PHONE_NUMBER", type="string", nullable=true, length=40)
     *
     */
    protected $phoneNumber;
    public function getPhoneNumber(){
        return $this->phoneNumber;
    }
    public function setPhoneNumber($phoneNumber){
        $this->phoneNumber = $phoneNumber;
    }

    // faxAreaCode generic STRING template*/
    /**
     * @Column(name="FAX_AREA_CODE", type="string", nullable=true, length=10)
     *
     */
    protected $faxAreaCode;
    public function getFaxAreaCode(){
        return $this->faxAreaCode;
    }
    public function setFaxAreaCode($faxAreaCode){
        $this->faxAreaCode = $faxAreaCode;
    }

    // faxNumber generic STRING template*/
    /**
     * @Column(name="FAX_NUMBER", type="string", nullable=true, length=40)
     *
     */
    protected $faxNumber;
    public function getFaxNumber(){
        return $this->faxNumber;
    }
    public function setFaxNumber($faxNumber){
        $this->faxNumber = $faxNumber;
    }

    // emailAddress generic STRING template*/
    /**
     * @Column(name="EMAIL_ADDRESS", type="string", nullable=true, length=2000)
     *
     */
    protected $emailAddress;
    public function getEmailAddress(){
        return $this->emailAddress;
    }
    public function setEmailAddress($emailAddress){
        $this->emailAddress = $emailAddress;
    }

    // orgName generic STRING template*/
    /**
     * @Column(name="ORG_NAME", type="string", nullable=false, length=240)
     *
     */
    protected $orgName;
    public function getOrgName(){
        return $this->orgName;
    }
    public function setOrgName($orgName){
        $this->orgName = $orgName;
    }

}
