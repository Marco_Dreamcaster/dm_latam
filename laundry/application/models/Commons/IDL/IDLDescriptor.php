<?php
/*
 * http://docs.doctrine-project.org/en/latest/tutorials/getting-started.html
 */

namespace Commons\IDL;

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity @Table(name="COM_IDL")
 */
class IDLDescriptor extends DescriptorBase
{
    public function __construct()
    {
        $this->fields = new ArrayCollection();
    }

    /**
     * @Column(name="SHORT_NAME")
     */
    protected $shortName;
    public function getShortName()
    {
        return $this->shortName;
    }
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * @Column(name="TABLE_NAME")
     */
    protected $tableName;
    public function getTableName()
    {
        return $this->tableName;
    }
    public function getStageTableTag(){
        $query = "STG_";

        if (substr($this->tableName, 0, strlen($query)) === $query){
            return substr($this->getTableName(),strlen($query), strlen($this->getTableName()));

        }else{
            return $tableName = $this->getTableName();

        }
    }
    public function getStageTableName(){
        return "O_STG_".$this->getStageTableTag();

    }
    public function getPreStageTableName(){
        return "O_PRE_".$this->getStageTableTag();

    }
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }

    public function getSuggestedShortName(){
        $parts = explode('_', $this->tableName);

        $suggested = strtolower($parts[0]);

        for ($i=1;$i<sizeof($parts);$i++){
            $suggested = $suggested.ucfirst(strtolower($parts[$i]));
        }

        return $suggested;
    }


    /**
     * @Column(name="TEMPLATE_NAME")
     */
    protected $templateName;
    public function getTemplateName()
    {
        return $this->templateName;
    }
    public function setTemplateName($templateName)
    {
        $this->templateName = $templateName;
    }

    /**
     * @Column(name="IS_HEADER", type="boolean")
     */
    protected $isHeader;
    public function getIsHeader()
    {
        return $this->isHeader;
    }
    public function setIsHeader($isHeader)
    {
        $this->isHeader = $isHeader;
    }

    /**
     * @Column(name="IS_TRANSACTIONAL", type="boolean")
     */
    protected $isTransactional;
    public function getIsTransactional()
    {
        return $this->isTransactional;
    }
    public function setIsTransactional($isTransactional)
    {
        $this->isTransactional = $isTransactional;
    }


    /**
     * @ManyToOne(targetEntity="IDLGroupDescriptor", inversedBy="descriptors")
     **/
    protected $group;
    public function setGroup(IDLGroupDescriptor $group)
    {
        $group->addDescriptor($this);
        $this->group = $group;
    }
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @OneToMany(targetEntity="IDLFieldDescriptor", mappedBy="descriptor",cascade={"persist"})
     **/
    protected $fields;
    public function addField(IDLFieldDescriptor $field)
    {
        $this->fields[] = $field;
    }
    public function getFields(){
        return $this->fields;
    }


}