<?php
namespace Commons\IDL;

Use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="COM_IDL_FIELD")  @HasLifecycleCallbacks
 */
class IDLFieldDescriptor extends DescriptorBase
{
    public function __construct()
    {
        $this->children = new ArrayCollection();

    }

    /**
     * @Column(name="COL_NAME")
     */
    protected $colName;

    /**
     * @Column(name="SHORT_NAME")
     */
    protected $shortName;

    /**
     * @Column(name="LONG_NAME", nullable=true)
     */
    protected $longName;

    /**
     * @Column(name="DESCRIPTION", length=2048, nullable=true)
     */
    protected $description;

    /**
     * @Column(name="ORA_TYPE")
     */
    protected $ORA_TYPE;

    /**
     * @Column(name="CI_FORM_VALIDATION_RULES", length=3000, nullable=true)
     */
    protected $formRules;

    /**
     * @Column(name="LOV", length=2048, nullable=true)
     */
    protected $LOV;

    /**
     * @Column(name="CONSTANT", nullable=true)
     */
    protected $constant;

    /**
     * @Column(name="IS_OUTPUT_VALUE", type="boolean")
     */
    protected $isOutputValue;

    /**
     * @Column(name="MANDATORY", type="boolean")
     */
    protected $isMandatory;


    /**
     * @Column(name="IS_QUERYABLE", type="boolean", nullable=true)
     */
    protected $isQueryable;

    /**
     * @Column(name="IS_SORTABLE", type="boolean",  nullable=true)
     */
    protected $isSortable;


    /**
     * @Column(name="MAX_LENGTH", type="integer", nullable=true)
     */
    protected $maxLength;

    /**
     * @Column(name="OUTPUT_POS", type="integer")
     */
    protected $outputPos;

    /**
     * @ManyToOne(targetEntity="IDLDescriptor", inversedBy="fields")
     **/
    protected $descriptor;



    public function setDescriptor(IDLDescriptor $descriptor)
    {
        $descriptor->addField($this);
        $this->descriptor = $descriptor;
    }
    public function getDescriptor(){
        return $this->descriptor;
    }


    public function getColName()
    {
        return $this->colName;
    }
    public function setColName($colName)
    {
        $this->colName = $colName;
    }

    public function getShortName()
    {
        return $this->shortName;
    }
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
    }

    public function getSuggestedShortName(){
        $parts = explode('_', $this->colName);

        $suggested = strtolower($parts[0]);

        for ($i=1;$i<sizeof($parts);$i++){
            $suggested = $suggested.ucfirst(strtolower($parts[$i]));
        }

        return $suggested;
    }

    public function getSuggestedTemplateName(){
        $parts = explode('_', $this->colName);

        $suggested = ucfirst($parts[0]);

        for ($i=1;$i<sizeof($parts);$i++){
            $suggested = $suggested.ucfirst(strtolower($parts[$i]));
        }

        return $suggested;
    }

    public function getLongName()
    {
        return $this->longName;
    }
    public function setLongName($longName)
    {
        $this->longName = $longName;
    }

    public function getDescription()
    {
        return $this->description;
    }
    public function setDescription($description)
    {
        $this->description = $description;
    }


    public function getORA_TYPE()
    {
        return $this->ORA_TYPE;
    }
    public function setORA_TYPE($ORA_TYPE)
    {
        $this->ORA_TYPE = $ORA_TYPE;
    }

    public function getFormRules()
    {
        return $this->formRules;
    }
    public function setFormRules($formRules)
    {
        $this->formRules = $formRules;
    }

    /** @PrePersist */
    public function prePersistRules(){
        $this->calculateFormRules();

    }

    /** @PreUpdate */
    public function calculateFormRules(){

        $this->maxLength = null;

        if ((strpos($this->ORA_TYPE,"VARCHAR2(") === 0)&&($this->ORA_TYPE!="VARCHAR2(1)")){
            $length = substr($this->ORA_TYPE, strpos($this->ORA_TYPE,"(")+1, strlen($this->ORA_TYPE)-(strlen('VARCHAR(')+2) );
            $this->setMaxLength(intval($length));
        }

        $this->formRules = '';

        if (($this->isMandatory)&&($this->ORA_TYPE!="VARCHAR2(1)")){
            $this->formRules = "required";
        }

        if (isset($this->maxLength)&&($this->maxLength>0)){
            if (strlen($this->formRules)>0){
                $this->formRules = $this->formRules."|";
            }

            $this->formRules = $this->formRules."max_length[".$this->maxLength."]";
        }

        if (isset($this->LOV) && (strlen($this->LOV)>0)){
            if (strlen($this->formRules)>0){
                $this->formRules = $this->formRules."|";
            }
            $this->formRules = $this->formRules."in_list[".$this->LOV."]";

        }


    }

    public function getLOV()
    {
        return $this->LOV;
    }
    public function setLOV($LOV)
    {
        $this->LOV = $LOV;
    }

    public function getConstant()
    {
        return $this->constant;
    }
    public function setConstant($constant)
    {
        $this->constant = $constant;
    }

    public function getIsOutputValue()
    {
        return $this->isOutputValue;
    }
    public function setIsOutputValue($isOutputValue)
    {
        $this->isOutputValue = $isOutputValue;
    }

    public function getIsMandatory()
    {
        return $this->isMandatory;
    }
    public function setIsMandatory($isMandatory)
    {
        $this->isMandatory = $isMandatory;
    }

    public function getMaxLength()
    {
        return $this->maxLength;
    }
    public function setMaxLength($maxLength)
    {
        $this->maxLength = $maxLength;
    }

    public function getOutputPos()
    {
        return $this->outputPos;
    }
    public function setOutputPos($outputPos)
    {
        $this->outputPos = $outputPos;
    }

    /**
     * @return mixed
     */
    public function getIsQueryable()
    {
        return $this->isQueryable;
    }

    /**
     * @param mixed $isQueryable
     */
    public function setIsQueryable($isQueryable)
    {
        $this->isQueryable = $isQueryable;
    }

    /**
     * @return mixed
     */
    public function getIsSortable()
    {
        return $this->isSortable;
    }

    /**
     * @param mixed $isSortable
     */
    public function setIsSortable($isSortable)
    {
        $this->isSortable = $isSortable;
    }



}