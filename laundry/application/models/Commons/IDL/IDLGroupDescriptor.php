<?php
namespace Commons\IDL;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="COM_IDL_GROUP")
 */
class IDLGroupDescriptor extends DescriptorBase
{
    public function __construct()
    {
        $this->descriptors = new ArrayCollection();
    }
    /**
     * @Column(name="SHORT_NAME")
     */
    protected $shortName;
    public function getShortName()
    {
        return $this->shortName;
    }
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * @Column(name="OUTPUT_PATH")
     */
    protected $outputPath;
    public function getOutputPath()
    {
        return $this->outputPath;
    }
    public function setOutputPath($outputPath)
    {
        $this->outputPath = $outputPath;
    }

    /**
     * @Column(name="SPEC_PATH", nullable=true)
     */
    protected $specPath;

    /**
     * @return mixed
     */
    public function getSpecPath()
    {
        return $this->specPath;
    }

    /**
     * @param mixed $specPath
     */
    public function setSpecPath($specPath)
    {
        $this->specPath = $specPath;
    }

    /**
     * @Column(name="COUNTRY")
     */
    protected $country;
    public function getCountry()
    {
        return $this->country;
    }
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @OneToMany(targetEntity="IDLDescriptor", mappedBy="group",cascade={"persist"})
     **/
    protected $descriptors;
    public function addDescriptor(IDLDescriptor $descriptor)
    {
        $this->descriptors[] = $descriptor;
    }
    public function getDescriptors(){
        return $this->descriptors;
    }
}