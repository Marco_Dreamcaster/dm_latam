<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 26/11/2018
 * Time: 09:31
 */

namespace Commons\MAPS;
/**
 * @Entity @Table(name="GLCheckRules")
 * @HasLifecycleCallbacks
 */

class GLCheckRules
{

    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", name="COUNTRY", length=2, nullable=false) * */
    protected $country;

    /** @Column(type="string", name="CTAORACLE", length=8,nullable=false) */
    protected $ctaOracle;

    /** @Column(type="string", name="CC", length=4, nullable=false) * */
    protected $cc;

    /** @Column(type="string", name="LOB", length=2, nullable=false) * */
    protected $lob;

    /** @Column(type="string", name="PL", length=2, nullable=true) * */
    protected $pl;

    /** @Column(type="string", name="IC", length=6, nullable=false) * */
    protected $ic;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getCtaOracle()
    {
        return $this->ctaOracle;
    }

    /**
     * @param mixed $ctaOracle
     */
    public function setCtaOracle($ctaOracle)
    {
        $this->ctaOracle = $ctaOracle;
    }


    /**
     * @return mixed
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * @param mixed $cc
     */
    public function setCc($cc)
    {
        $this->cc = $cc;
    }

    /**
     * @return mixed
     */
    public function getLob()
    {
        return $this->lob;
    }

    /**
     * @param mixed $lob
     */
    public function setLob($lob)
    {
        $this->lob = $lob;
    }

    /**
     * @return mixed
     */
    public function getPl()
    {
        return $this->pl;
    }

    /**
     * @param mixed $pl
     */
    public function setPl($pl)
    {
        $this->pl = $pl;
    }

    /**
     * @return mixed
     */
    public function getIc()
    {
        return $this->ic;
    }

    /**
     * @param mixed $ic
     */
    public function setIc($ic)
    {
        $this->ic = $ic;
    }


}