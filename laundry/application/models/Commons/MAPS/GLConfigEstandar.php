<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 26/11/2018
 * Time: 09:31
 */

namespace Commons\MAPS;
/**
 * @Entity @Table(name="GLConfigEstandar")
 * @HasLifecycleCallbacks
 */

class GLConfigEstandar
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", name="COUNTRY", length=2, nullable=false) * */
    protected $country;

    /** @Column(type="string", name="BRANCH", length=6, nullable=true) * */
    protected $branch;

    /** @Column(type="string", name="COMPANY2", length=240, nullable=true) * */
    protected $company;

    /** @Column(type="string", name="COMPANY", length=240, nullable=true) * */
    protected $companyAlt;

    /** @Column(type="string", name="LOCALCURRENCY", length=6, nullable=true) * */
    protected $localcurrency;

    /** @Column(type="string", name="ORG_NAME", length=60, nullable=false) * */
    protected $org_name;

    /** @Column(type="string", name="REFERENCE1", length=100, nullable=true) * */
    protected $ref1;

    /** @Column(type="string", name="REFERENCE2", length=240, nullable=true) * */
    protected $ref2;

    /** @Column(type="string", name="REFERENCE4", length=100, nullable=true) * */
    protected $ref4;

    /** @Column(type="string", name="REFERENCE5", length=240, nullable=true) * */
    protected $ref5;

    /** @Column(type="string", name="REFERENCE10", length=240, nullable=true) * */
    protected $ref10;

    /** @Column(type="string", name="SMREF", length=8, nullable=true) * */
    protected $smref;

    /** @Column(type="string", name="PMREF", length=8, nullable=true) * */
    protected $pmref;

    /** @Column(type="integer", name="migOrder", nullable=true) * */
    protected $migOrder;

    /** @Column(type="integer", name="wave", nullable=true) * */
    protected $wave;

    /** @Column(type="datetime", name="cutoffDate", nullable=true) * */
    protected $cutoffDate;

    /** @Column(type="string", name="inventoryDistAccountPrefix", nullable=true) * */
    protected $inventoryDistAccountPrefix;

    /** @Column(type="string", name="defaultLocatorName", nullable=true) * */
    protected $defaultLocatorName;

    /** @Column(type="string", name="bAccList", nullable=true) * */
    protected $bAccList;

    /** @Column(type="string", name="rAccList", nullable=true) * */
    protected $rAccList;


    /**
     * @return mixed
     */
    public function getBAccList()
    {
        return $this->bAccList;
    }

    /**
     * @param mixed $bAccList
     */
    public function setBAccList($bAccList)
    {
        $this->bAccList = $bAccList;
    }

    /**
     * @return mixed
     */
    public function getRAccList()
    {
        return $this->rAccList;
    }

    /**
     * @param mixed $rAccList
     */
    public function setRAccList($rAccList)
    {
        $this->rAccList = $rAccList;
    }



    /**
     * @return mixed
     */
    public function getDefaultLocatorName()
    {
        return $this->defaultLocatorName;
    }

    /**
     * @param mixed $defaultLocatorName
     */
    public function setDefaultLocatorName($defaultLocatorName)
    {
        $this->defaultLocatorName = $defaultLocatorName;
    }



    /**
     * @return mixed
     */
    public function getMigOrder()
    {
        return $this->migOrder;
    }

    /**
     * @param mixed $migOrder
     */
    public function setMigOrder($migOrder)
    {
        $this->migOrder = $migOrder;
    }

    /**
     * @return mixed
     */
    public function getCutoffDate()
    {
        return $this->cutoffDate;
    }

    /**
     * @param mixed $cutoffDate
     */
    public function setCutoffDate($cutoffDate)
    {
        $this->cutoffDate = $cutoffDate;
    }



    /**
     * @return mixed
     */
    public function getInventoryDistAccountPrefix()
    {
        return $this->inventoryDistAccountPrefix;
    }

    /**
     * @param mixed $inventoryDistAccountPrefix
     */
    public function setInventoryDistAccountPrefix($inventoryDistAccountPrefix)
    {
        $this->inventoryDistAccountPrefix = $inventoryDistAccountPrefix;
    }

    /**
     * @return mixed
     */
    public function getSmref()
    {
        return $this->smref;
    }

    /**
     * @param mixed $smref
     */
    public function setSmref($smref)
    {
        $this->smref = $smref;
    }

    /**
     * @return mixed
     */
    public function getPmref()
    {
        return $this->pmref;
    }

    /**
     * @param mixed $pmref
     */
    public function setPmref($pmref)
    {
        $this->pmref = $pmref;
    }


    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * @param mixed $branch
     */
    public function setBranch($branch)
    {
        $this->branch = $branch;
    }

    /**
     * @return mixed
     */
    public function getLocalcurrency()
    {
        return $this->localcurrency;
    }

    /**
     * @param mixed $localcurrency
     */
    public function setLocalcurrency($localcurrency)
    {
        $this->localcurrency = $localcurrency;
    }

    /**
     * @return mixed
     */
    public function getOrgName()
    {
        return $this->org_name;
    }

    /**
     * @param mixed $org_name
     */
    public function setOrgName($org_name)
    {
        $this->org_name = $org_name;
    }

    /**
     * @return mixed
     */
    public function getRef1()
    {
        return $this->ref1;
    }

    /**
     * @param mixed $ref1
     */
    public function setRef1($ref1)
    {
        $this->ref1 = $ref1;
    }

    /**
     * @return mixed
     */
    public function getRef2()
    {
        return $this->ref2;
    }

    /**
     * @param mixed $ref2
     */
    public function setRef2($ref2)
    {
        $this->ref2 = $ref2;
    }

    /**
     * @return mixed
     */
    public function getRef4()
    {
        return $this->ref4;
    }

    /**
     * @param mixed $ref4
     */
    public function setRef4($ref4)
    {
        $this->ref4 = $ref4;
    }

    /**
     * @return mixed
     */
    public function getRef5()
    {
        return $this->ref5;
    }

    /**
     * @param mixed $ref5
     */
    public function setRef5($ref5)
    {
        $this->ref5 = $ref5;
    }

    /**
     * @return mixed
     */
    public function getRef10()
    {
        return $this->ref10;
    }

    /**
     * @param mixed $ref10
     */
    public function setRef10($ref10)
    {
        $this->ref10 = $ref10;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getCompanyAlt()
    {
        return $this->companyAlt;
    }

    /**
     * @param mixed $companyAlt
     */
    public function setCompanyAlt($companyAlt)
    {
        $this->companyAlt = $companyAlt;
    }

    /**
     * @return mixed
     */
    public function getWave()
    {
        return $this->wave;
    }

    /**
     * @param mixed $wave
     */
    public function setWave($wave)
    {
        $this->wave = $wave;
    }






}