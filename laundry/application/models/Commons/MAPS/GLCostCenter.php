<?php
/**
 * http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/types.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/events.html
 *
 * https://www.codeigniter.com/user_guide/libraries/sessions.html
 *
 */

namespace Commons\MAPS;

/**
 * @Entity @Table(name="GLCostCenter")
 * @HasLifecycleCallbacks
 */
class GLCostCenter
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", name="CTAORACLE", length=8,nullable=false) */
    protected $ctaOracle;

    /** @Column(type="string", name="DESDE", length=4,nullable=false) */
    protected $desde;

    /** @Column(type="string", name="HASTA", length=4,nullable=false) */
    protected $hasta;

    /** @Column(type="string", name="COUNTRY", length=2, nullable=false) * */
    protected $country;

    /**
     * @return mixed
     */
    public function getCtaOracle()
    {
        return $this->ctaOracle;
    }

    /**
     * @param mixed $ctaOracle
     */
    public function setCtaOracle($ctaOracle)
    {
        $this->ctaOracle = $ctaOracle;
    }

    /**
     * @return mixed
     */
    public function getDesde()
    {
        return $this->desde;
    }

    /**
     * @param mixed $desde
     */
    public function setDesde($desde)
    {
        $this->desde = $desde;
    }

    /**
     * @return mixed
     */
    public function getHasta()
    {
        return $this->hasta;
    }

    /**
     * @param mixed $hasta
     */
    public function setHasta($hasta)
    {
        $this->hasta = $hasta;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }



}
