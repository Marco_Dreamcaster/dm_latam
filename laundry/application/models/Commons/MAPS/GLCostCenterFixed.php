<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 08/11/2018
 * Time: 11:12
 */

namespace Commons\MAPS;
/**
 * @Entity @Table(name="GLCostCenterFixed")
 * @HasLifecycleCallbacks
 */

class GLCostCenterFixed
{

    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", name="CTAORACLE", length=8,nullable=false) */
    protected $ctaOracle;

    /** @Column(type="string", name="COSTCENTER", length=4,nullable=false) */
    protected $costcenter;

    /** @Column(type="string", name="LOB", length=2,nullable=true) */
    protected $lob;

    /** @Column(type="string", name="COUNTRY", length=2, nullable=false) * */
    protected $country;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCtaOracle()
    {
        return $this->ctaOracle;
    }

    /**
     * @param mixed $ctaOracle
     */
    public function setCtaOracle($ctaOracle)
    {
        $this->ctaOracle = $ctaOracle;
    }

    /**
     * @return mixed
     */
    public function getCostcenter()
    {
        return $this->costcenter;
    }

    /**
     * @param mixed $costcenter
     */
    public function setCostcenter($costcenter)
    {
        $this->costcenter = $costcenter;
    }

    /**
     * @return mixed
     */
    public function getLob()
    {
        return $this->lob;
    }

    /**
     * @param mixed $lob
     */
    public function setLob($lob)
    {
        $this->lob = $lob;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }



}