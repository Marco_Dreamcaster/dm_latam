<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 04/12/2018
 * Time: 12:04
 */

namespace Commons\MAPS;

/**
 * @Entity @Table(name="GLCostoLocal")
 * @HasLifecycleCallbacks
 */

class GLCostoLocal

{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", name="ID", length=4, nullable=false) */
    protected $id_sigla;

    /** @Column(type="string", name="DESCRIP", length=100, nullable=true) */
    protected $descrip;

    /** @Column(type="float", name="VALOR", precision=20, scale=6, nullable=true) */
    protected $valor;

    /** @Column(type="integer", name="UNIDMEDIDA", length=4, nullable=true) */
    protected $unidmedida;

    /** @Column(type="string", name="MONEDA", length=1, nullable=true) */
    protected $moneda;

    /** @Column(type="string", name="PROCEDENCIA", length=3, nullable=true) */
    protected $procedencia;

    /** @Column(type="string", name="PRODUCTO", length=20, nullable=true) */
    protected $producto;

    /** @Column(type="string", name="TIPCOS", length=4, nullable=true) */
    protected $tipcos;

    /** @Column(type="string", name="FORMULA", length=50, nullable=true) */
    protected $formula;

    /** @Column(type="string", name="USO", length=1, nullable=true) */
    protected $uso;

    /** @Column(type="string", name="MONTAJE", length=1, nullable=true) */
    protected $montaje;

    /** @var  @Column(type="string", name="EXPENDITURE", length=200, nullable=true) */
    protected $expenditure;

    /** @var  @Column(type="string", name="CLASIFICACION", length=200, nullable=true) */
    protected $clasificacion;

    /** @var  @Column(type="string", name="COUNTRY", length=2, nullable=false) */
    protected $country;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdSigla()
    {
        return $this->id_sigla;
    }

    /**
     * @param mixed $id_sigla
     */
    public function setIdSigla($id_sigla)
    {
        $this->id_sigla = $id_sigla;
    }

    /**
     * @return mixed
     */
    public function getDescrip()
    {
        return $this->descrip;
    }

    /**
     * @param mixed $descrip
     */
    public function setDescrip($descrip)
    {
        $this->descrip = $descrip;
    }

    /**
     * @return mixed
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param mixed $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    /**
     * @return mixed
     */
    public function getUnidmedida()
    {
        return $this->unidmedida;
    }

    /**
     * @param mixed $unidmedida
     */
    public function setUnidmedida($unidmedida)
    {
        $this->unidmedida = $unidmedida;
    }

    /**
     * @return mixed
     */
    public function getMoneda()
    {
        return $this->moneda;
    }

    /**
     * @param mixed $moneda
     */
    public function setMoneda($moneda)
    {
        $this->moneda = $moneda;
    }

    /**
     * @return mixed
     */
    public function getProcedencia()
    {
        return $this->procedencia;
    }

    /**
     * @param mixed $procedencia
     */
    public function setProcedencia($procedencia)
    {
        $this->procedencia = $procedencia;
    }

    /**
     * @return mixed
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * @param mixed $producto
     */
    public function setProducto($producto)
    {
        $this->producto = $producto;
    }

    /**
     * @return mixed
     */
    public function getTipcos()
    {
        return $this->tipcos;
    }

    /**
     * @param mixed $tipcos
     */
    public function setTipcos($tipcos)
    {
        $this->tipcos = $tipcos;
    }

    /**
     * @return mixed
     */
    public function getFormula()
    {
        return $this->formula;
    }

    /**
     * @param mixed $formula
     */
    public function setFormula($formula)
    {
        $this->formula = $formula;
    }

    /**
     * @return mixed
     */
    public function getUso()
    {
        return $this->uso;
    }

    /**
     * @param mixed $uso
     */
    public function setUso($uso)
    {
        $this->uso = $uso;
    }

    /**
     * @return mixed
     */
    public function getMontaje()
    {
        return $this->montaje;
    }

    /**
     * @param mixed $montaje
     */
    public function setMontaje($montaje)
    {
        $this->montaje = $montaje;
    }

    /**
     * @return mixed
     */
    public function getExpenditure()
    {
        return $this->expenditure;
    }

    /**
     * @param mixed $expenditure
     */
    public function setExpenditure($expenditure)
    {
        $this->expenditure = $expenditure;
    }

    /**
     * @return mixed
     */
    public function getClasificacion()
    {
        return $this->clasificacion;
    }

    /**
     * @param mixed $clasificacion
     */
    public function setClasificacion($clasificacion)
    {
        $this->clasificacion = $clasificacion;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

}