<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 25/10/2018
 * Time: 17:12
 */

namespace Commons\MAPS;

/**
 * @Entity @Table(name="GLCuentas")

 */

class GLCuentas
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @var  @Column(type="string", name="COUNTRY", length=2, nullable=false) */
    protected $country;

    /** @var  @Column(type="string", name="CTA", length=8, nullable=false) */
    protected $cta;

    /** @var  @Column(type="string", name="DESCRIP", length=100, nullable=false) */
    protected $descrip;

    /** @var  @Column(type="string", name="CTAORACLE", length=8, nullable=false) */
    protected $ctaOracle;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return GLCuentas
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCta()
    {
        return $this->cta;
    }

    /**
     * @param mixed $cta
     */
    public function setCta($cta)
    {
        $this->cta = $cta;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getCtaOracle()
    {
        return $this->ctaOracle;
    }

    /**
     * @param mixed $ctaOracle
     */
    public function setCtaOracle($ctaOracle)
    {
        $this->ctaOracle = $ctaOracle;
    }

    /**
     * @return mixed
     */
    public function getDescrip()
    {
        return $this->descrip;
    }

    /**
     * @param mixed $descrip
     */
    public function setDescrip($descrip)
    {
        $this->descrip = $descrip;
    }


}