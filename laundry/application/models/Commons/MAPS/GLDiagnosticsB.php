<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 26/11/2018
 * Time: 09:31
 */

namespace Commons\MAPS;
/**
 * @Entity @Table(name="GLDiagnosticsB")
 * @HasLifecycleCallbacks
 */

class GLDiagnosticsB
{

    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="string", name="COUNTRY", length=2, nullable=false) * */
    protected $country;

    /** @Column(type="string", name="CTA", length=8, nullable=false) * */
    protected $cta;

    /** @Column(type="float", name="SALDOANT", precision=15, scale=2, nullable=true) */
    protected $saldoant;

    /** @Column(type="float", name="DEBE", precision=15, scale=2, nullable=true) */
    protected $debe;

    /** @Column(type="float", name="HABER", precision=15, scale=2, nullable=true) */
    protected $haber;

    /** @Column(type="float", name="SALDO", precision=15, scale=2, nullable=true) */
    protected $saldo;

    /** @Column(type="float", name="LAUNDRY", precision=15, scale=2, nullable=true) */
    protected $laundry;

    /** @Column(type="float", name="DIF", precision=15, scale=2, nullable=true) */
    protected $dif;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getCta()
    {
        return $this->cta;
    }

    /**
     * @param mixed $cta
     */
    public function setCta($cta)
    {
        $this->cta = $cta;
    }

    /**
     * @return mixed
     */
    public function getSaldoant()
    {
        return $this->saldoant;
    }

    /**
     * @param mixed $saldoant
     */
    public function setSaldoant($saldoant)
    {
        $this->saldoant = $saldoant;
    }

    /**
     * @return mixed
     */
    public function getDebe()
    {
        return $this->debe;
    }

    /**
     * @param mixed $debe
     */
    public function setDebe($debe)
    {
        $this->debe = $debe;
    }

    /**
     * @return mixed
     */
    public function getHaber()
    {
        return $this->haber;
    }

    /**
     * @param mixed $haber
     */
    public function setHaber($haber)
    {
        $this->haber = $haber;
    }

    /**
     * @return mixed
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * @param mixed $saldo
     */
    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;
    }

    /**
     * @return mixed
     */
    public function getLaundry()
    {
        return $this->laundry;
    }

    /**
     * @param mixed $laundry
     */
    public function setLaundry($laundry)
    {
        $this->laundry = $laundry;
    }

    /**
     * @return mixed
     */
    public function getDif()
    {
        return $this->dif;
    }

    /**
     * @param mixed $dif
     */
    public function setDif($dif)
    {
        $this->dif = $dif;
    }


}