<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 06/11/2018
 * Time: 14:04
 */

namespace Commons\MAPS;

/**
 * @Entity @Table(name="GLDiarios")
 * @HasLifecycleCallbacks
 */

class GLDiarios
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @var  @Column(type="string", name="COUNTRY", length=2, nullable=false) */
    protected $country;

    /** @var  @Column(type="string", name="DIARIO", length=4, nullable=false) */
    protected $diario;

    /** @var  @Column(type="string", name="DESCRIP", length=50, nullable=false) */
    protected $descrip;

    /** @var  @Column(type="string", name="ACTIVE", length=1, nullable=false) */
    protected $active;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getDiario()
    {
        return $this->diario;
    }

    /**
     * @param mixed $diario
     */
    public function setDiario($diario)
    {
        $this->diario = $diario;
    }

    /**
     * @return mixed
     */
    public function getDescrip()
    {
        return $this->descrip;
    }

    /**
     * @param mixed $descrip
     */
    public function setDescrip($descrip)
    {
        $this->descrip = $descrip;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }


}