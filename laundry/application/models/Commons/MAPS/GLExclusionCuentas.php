<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 25/10/2018
 * Time: 12:32
 */

namespace Commons\MAPS;

/**
 * @Entity @Table(name="GLExclusionCuentas")
 * @HasLifecycleCallbacks
 */

class GLExclusionCuentas
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @var  @Column(type="string", name="CTASIGLA", length=8, nullable=false) */
    protected $ctaSigla;

    /** @var  @Column(type="string", name="COUNTRY", length=2, nullable=false) */
    protected $country;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getCtaSigla()
    {
        return $this->ctaSigla;
    }

    /**
     * @param mixed $ctaSigla
     */
    public function setCtaSigla($ctaSigla)
    {
        $this->ctaSigla = $ctaSigla;
    }


}
