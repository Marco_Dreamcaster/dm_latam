<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 29/10/2018
 * Time: 11:20
 */

namespace Commons\MAPS;

/**
 * @Entity @Table(name="GLLineOfBusiness")
 * @HasLifecycleCallbacks
 */

class GLLineOfBusiness
{

    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @var  @Column(type="string", name="COUNTRY", length=2, nullable=false) */
    protected $country;

    /** @var  @Column(type="string", name="CTAORACLE", length=8, nullable=false) */
    protected $ctaOracle;

    /** @var  @Column(type="string", name="LOBPARAM1", length=2, nullable=false) */
    protected $lobParam1;

    /** @var  @Column(type="string", name="LOBPARAM2", length=2, nullable=true) */
    protected $lobParam2;

    /** @var  @Column(type="string", name="LOBPARAM3", length=2, nullable=true) */
    protected $lobParam3;

    /** @var  @Column(type="string", name="LOBPARAM4", length=2, nullable=true) */
    protected $lobParam4;

    /** @var  @Column(type="string", name="LOBPARAM5", length=2, nullable=true) */
    protected $lobParam5;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getCtaOracle()
    {
        return $this->ctaOracle;
    }

    /**
     * @param mixed $ctaOracle
     */
    public function setCtaOracle($ctaOracle)
    {
        $this->ctaOracle = $ctaOracle;
    }

    /**
     * @return mixed
     */
    public function getLobParam1()
    {
        return $this->lobParam1;
    }

    /**
     * @param mixed $lobParam1
     */
    public function setLobParam1($lobParam1)
    {
        $this->lobParam1 = $lobParam1;
    }

    /**
     * @return mixed
     */
    public function getLobParam2()
    {
        return $this->lobParam2;
    }

    /**
     * @param mixed $lobParam2
     */
    public function setLobParam2($lobParam2)
    {
        $this->lobParam2 = $lobParam2;
    }

    /**
     * @return mixed
     */
    public function getLobParam3()
    {
        return $this->lobParam3;
    }

    /**
     * @param mixed $lobParam3
     */
    public function setLobParam3($lobParam3)
    {
        $this->lobParam3 = $lobParam3;
    }

    /**
     * @return mixed
     */
    public function getLobParam4()
    {
        return $this->lobParam4;
    }

    /**
     * @param mixed $lobParam4
     */
    public function setLobParam4($lobParam4)
    {
        $this->lobParam4 = $lobParam4;
    }

    /**
     * @return mixed
     */
    public function getLobParam5()
    {
        return $this->lobParam5;
    }

    /**
     * @param mixed $lobParam5
     */
    public function setLobParam5($lobParam5)
    {
        $this->lobParam5 = $lobParam5;
    }


}