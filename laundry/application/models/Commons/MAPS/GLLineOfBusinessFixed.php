<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 29/10/2018
 * Time: 11:20
 */

namespace Commons\MAPS;

/**
 * @Entity @Table(name="GLLineOfBusinessFixed")
 * @HasLifecycleCallbacks
 */

class GLLineOfBusinessFixed
{

    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @var  @Column(type="string", name="COUNTRY", length=2, nullable=false) */
    protected $country;

    /** @var  @Column(type="string", name="CTAORACLE", length=8, nullable=false) */
    protected $ctaOracle;

    /** @var  @Column(type="string", name="LOBPARAM", length=2, nullable=false) */
    protected $lobParam;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getCtaOracle()
    {
        return $this->ctaOracle;
    }

    /**
     * @param mixed $ctaOracle
     */
    public function setCtaOracle($ctaOracle)
    {
        $this->ctaOracle = $ctaOracle;
    }

    /**
     * @return mixed
     */
    public function getLobParam()
    {
        return $this->lobParam;
    }

    /**
     * @param mixed $lobParam1
     */
    public function setLobParam($lobParam)
    {
        $this->lobParam = $lobParam;
    }

}