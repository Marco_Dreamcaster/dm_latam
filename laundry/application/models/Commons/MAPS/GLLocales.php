<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 25/10/2018
 * Time: 18:23
 */

namespace Commons\MAPS;
/**
 * @Entity @Table(name="GLLocales")

 */

class GLLocales
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @var  @Column(type="string", name="COUNTRY", length=2, nullable=false) */
    protected $country;

    /** @var  @Column(type="string", name="LOCAL", length=3, nullable=false) */
    protected  $local;

    /** @var  @Column(type="string", name="DESCRIP", length=50, nullable=false) */
    protected $descrip;

    /** @var  @Column(type="string", name="LOCALORACLE", length=20, nullable=false) */
    protected  $localOracle;

    /** @var  @Column(type="string", name="IDALMACEN", length=40, nullable=true) */
    protected  $idAlmacen;

    /** @var  @Column(type="string", name="SUB_INV_ORG_CODE", length=20, nullable=true) */
    protected  $subInventoryOrgCode;

    /** @var  @Column(type="string", name="SUB_INV_ACCOUNT", length=10, nullable=true) */
    protected  $subInventoryName;

    /** @var  @Column(type="string", name="SUB_INV_DISTRIBUTION_ACCOUNT", length=100, nullable=true) */
    protected  $subInventoryDistributionAccount;



    /**
     * @return mixed
     */
    public function getSubInventoryOrgCode()
    {
        return $this->subInventoryOrgCode;
    }

    /**
     * @param mixed $subInventoryOrgCode
     */
    public function setSubInventoryOrgCode($subInventoryOrgCode)
    {
        $this->subInventoryOrgCode = $subInventoryOrgCode;
    }

    /**
     * @return mixed
     */
    public function getSubInventoryName()
    {
        return $this->subInventoryName;
    }

    /**
     * @param mixed $subInventoryName
     */
    public function setSubInventoryName($subInventoryName)
    {
        $this->subInventoryName = $subInventoryName;
    }

    /**
     * @return mixed
     */
    public function getSubInventoryDistributionAccount()
    {
        return $this->subInventoryDistributionAccount;
    }

    /**
     * @param mixed $subInventoryDistributionAccount
     */
    public function setSubInventoryDistributionAccount($subInventoryDistributionAccount)
    {
        $this->subInventoryDistributionAccount = $subInventoryDistributionAccount;
    }




    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdAlmacen()
    {
        return $this->idAlmacen;
    }

    /**
     * @param mixed $idAlmacen
     */
    public function setIdAlmacen($idAlmacen)
    {
        $this->idAlmacen = $idAlmacen;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDescrip()
    {
        return $this->descrip;
    }

    /**
     * @param mixed $descrip
     */
    public function setDescrip($descrip)
    {
        $this->descrip = $descrip;
    }

    /**
     * @return mixed
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * @param mixed $local
     */
    public function setLocal($local)
    {
        $this->local = $local;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getLocalOracle()
    {
        return $this->localOracle;
    }

    /**
     * @param mixed $localOracle
     */
    public function setLocalOracle($localOracle)
    {
        $this->localOracle = $localOracle;
    }

}