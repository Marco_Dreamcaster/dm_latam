<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 25/10/2018
 * Time: 18:23
 */

namespace Commons\MAPS;
/**
 * @Entity @Table(name="GLMonedas")

 */

class GLMonedas
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @var  @Column(type="string", name="COUNTRY", length=2, nullable=false) */
    protected $country;

    /** @var  @Column(type="string", name="MONEDA", length=1, nullable=false) */
    protected $moneda;

    /** @var  @Column(type="string", name="DESCRIP", length=100, nullable=false) */
    protected $descrip;

    /** @var  @Column(type="string", name="SIGLA", length=10, nullable=false) */
    protected $sigla;

    /** @var  @Column(type="string", name="TASA", length=11, nullable=true) */
    protected $tasa;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getMoneda()
    {
        return $this->moneda;
    }

    /**
     * @param mixed $moneda
     */
    public function setMoneda($moneda)
    {
        $this->moneda = $moneda;
    }

    /**
     * @return mixed
     */
    public function getDescrip()
    {
        return $this->descrip;
    }

    /**
     * @param mixed $descrip
     */
    public function setDescrip($descrip)
    {
        $this->descrip = $descrip;
    }

    /**
     * @return mixed
     */
    public function getSigla()
    {
        return $this->sigla;
    }

    /**
     * @param mixed $sigla
     */
    public function setSigla($sigla)
    {
        $this->sigla = $sigla;
    }

    /**
     * @return mixed
     */
    public function getTasa()
    {
        return $this->tasa;
    }

    /**
     * @param mixed $tasa
     */
    public function setTasa($tasa)
    {
        $this->tasa = $tasa;
    }




}
