<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 26/10/2018
 * Time: 17:09
 */

namespace Commons\MAPS;

/**
 * @Entity @Table(name="GLProductLine")

 */
class GLProductLine
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @var  @Column(type="string", name="COUNTRY", length=2, nullable=false) */
    protected $country;

    /** @var  @Column(type="string", name="CTAORACLE", length=8, nullable=false) */
    protected $ctaOracle;

    /** @var  @Column(type="string", name="DESDE", length=2, nullable=false) */
    protected $desde;

    /** @var  @Column(type="string", name="HASTA", length=2, nullable=false) */
    protected $hasta;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getCtaOracle()
    {
        return $this->ctaOracle;
    }

    /**
     * @param mixed $ctaOracle
     */
    public function setCtaOracle($ctaOracle)
    {
        $this->ctaOracle = $ctaOracle;
    }

    /**
     * @return mixed
     */
    public function getDesde()
    {
        return $this->desde;
    }

    /**
     * @param mixed $desde
     */
    public function setDesde($desde)
    {
        $this->desde = $desde;
    }

    /**
     * @return mixed
     */
    public function getHasta()
    {
        return $this->hasta;
    }

    /**
     * @param mixed $hasta
     */
    public function setHasta($hasta)
    {
        $this->hasta = $hasta;
    }


}