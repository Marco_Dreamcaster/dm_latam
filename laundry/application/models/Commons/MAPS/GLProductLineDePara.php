<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 26/10/2018
 * Time: 10:51
 */

namespace Commons\MAPS;
/**
 * @Entity @Table(name="GLProductLineDePara")
 */

class GLProductLineDePara
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @var  @Column(type="string", name="COUNTRY", length=2, nullable=false) */
    protected $country;

    /** @var  @Column(type="string", name="CODSIGLA", length=2, nullable=false) */
    protected $codSigla;

    /** @var  @Column(type="string", name="CODORACLE", length=2, nullable=false) */
    protected $codOracle;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getCodSigla()
    {
        return $this->codSigla;
    }

    /**
     * @param mixed $codSigla
     */
    public function setCodSigla($codSigla)
    {
        $this->codSigla = $codSigla;
    }

    /**
     * @return mixed
     */
    public function getCodOracle()
    {
        return $this->codOracle;
    }

    /**
     * @param mixed $codOracle
     */
    public function setCodOracle($codOracle)
    {
        $this->codOracle = $codOracle;
    }

}