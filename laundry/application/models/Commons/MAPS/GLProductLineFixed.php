<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 26/10/2018
 * Time: 17:09
 */

namespace Commons\MAPS;

/**
 * @Entity @Table(name="GLProductLineFixed")

 */
class GLProductLineFixed
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @var  @Column(type="string", name="COUNTRY", length=2, nullable=false) */
    protected $country;

    /** @var  @Column(type="string", name="LOB", length=2, nullable=false) */
    protected $lob;

    /** @var  @Column(type="string", name="PL", length=2, nullable=false) */
    protected $pl;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getLob()
    {
        return $this->lob;
    }

    /**
     * @param mixed $lob
     */
    public function setLob($lob)
    {
        $this->lob = $lob;
    }

    /**
     * @return mixed
     */
    public function getPl()
    {
        return $this->pl;
    }

    /**
     * @param mixed $pl
     */
    public function setPl($pl)
    {
        $this->pl = $pl;
    }


}