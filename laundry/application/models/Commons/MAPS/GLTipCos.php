<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 25/10/2018
 * Time: 18:41
 */

namespace Commons\MAPS;
/**
 * @Entity @Table(name="GLTipCos")

 */

class GLTipCos
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @var  @Column(type="string", name="COUNTRY", length=2, nullable=false) */
    protected $country;

    /** @var  @Column(type="string", name="DESCRIP", length=50, nullable=false) */
    protected $descrip;

    /** @var  @Column(type="string", name="TIPCOS", length=4, nullable=false) */
    protected $tipcos;

    /** @var  @Column(type="string", name="TIPCOSORACLE", length=4, nullable=false) */
    protected $tipcosOracle;

    /** @var  @Column(type="string", name="EXPENDITURE", length=200, nullable=true) */
    protected $expenditure;

    /** @var  @Column(type="string", name="CLASIFICACION", length=200, nullable=true) */
    protected $clasificacion;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getDescrip()
    {
        return $this->descrip;
    }

    /**
     * @param mixed $descrip
     */
    public function setDescrip($descrip)
    {
        $this->descrip = $descrip;
    }

    /**
     * @return mixed
     */
    public function getTipcos()
    {
        return $this->tipcos;
    }

    /**
     * @param mixed $tipcos
     */
    public function setTipcos($tipcos)
    {
        $this->tipcos = $tipcos;
    }

    /**
     * @return mixed
     */
    public function getTipcosOracle()
    {
        return $this->tipcosOracle;
    }

    /**
     * @param mixed $tipcosOracle
     */
    public function setTipcosOracle($tipcosOracle)
    {
        $this->tipcosOracle = $tipcosOracle;
    }

    /**
     * @return mixed
     */
    public function getExpenditure()
    {
        return $this->expenditure;
    }

    /**
     * @param mixed $expenditure
     */
    public function setExpenditure($expenditure)
    {
        $this->expenditure = $expenditure;
    }

    /**
     * @return mixed
     */
    public function getClasificacion()
    {
        return $this->clasificacion;
    }

    /**
     * @param mixed $clasificacion
     */
    public function setClasificacion($clasificacion)
    {
        $this->clasificacion = $clasificacion;
    }


}