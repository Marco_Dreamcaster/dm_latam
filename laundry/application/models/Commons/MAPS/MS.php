<?php
/**
 * Created by PhpStorm.
 * User: robson.muller
 * Date: 10/01/2019
 * Time: 12:43
 */

namespace Commons\MAPS;
/**
 * @Entity @Table(name="MS")

 */

class MS
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @var  @Column(type="string", name="COUNTRY", length=2, nullable=false) */
    protected $country;

    /** @var  @Column(type="string", name="PROJECT", length=1, nullable=false) */
    protected $project;

    /** @var  @Column(type="string", name="SERVICE", length=1, nullable=false) */
    protected $service;

    /** @var  @Column(type="string", name="REPAIR", length=1, nullable=false) */
    protected $repair;

    /** @var  @Column(type="string", name="CUSTOMER", length=1, nullable=false) */
    protected $customer;

    /** @var  @Column(type="string", name="VENDOR", length=1, nullable=false) */
    protected $vendor;

    /** @var  @Column(type="string", name="INVENTORY", length=1, nullable=false) */
    protected $inventory;

    /** @var  @Column(type="string", name="GLFC", length=1, nullable=false) */
    protected $glfc;

    /** @var  @Column(type="string", name="GLFG", length=1, nullable=false) */
    protected $glfg;

    /** @var  @Column(type="integer", name="ORDERMIGRATION", nullable=false) */
    protected $ordermigration;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param mixed $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param mixed $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function getRepair()
    {
        return $this->repair;
    }

    /**
     * @param mixed $repair
     */
    public function setRepair($repair)
    {
        $this->repair = $repair;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return mixed
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param mixed $vendor
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * @return mixed
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    /**
     * @param mixed $inventory
     */
    public function setInventory($inventory)
    {
        $this->inventory = $inventory;
    }

    /**
     * @return mixed
     */
    public function getGlfc()
    {
        return $this->glfc;
    }

    /**
     * @param mixed $glfc
     */
    public function setGlfc($glfc)
    {
        $this->glfc = $glfc;
    }

    /**
     * @return mixed
     */
    public function getGlfg()
    {
        return $this->glfg;
    }

    /**
     * @param mixed $glfg
     */
    public function setGlfg($glfg)
    {
        $this->glfg = $glfg;
    }

    /**
     * @return mixed
     */
    public function getOrdermigration()
    {
        return $this->ordermigration;
    }

    /**
     * @param mixed $ordermigration
     */
    public function setOrdermigration($ordermigration)
    {
        $this->ordermigration = $ordermigration;
    }



}