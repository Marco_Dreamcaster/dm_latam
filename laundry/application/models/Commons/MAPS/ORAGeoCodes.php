<?php
/**
 * http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/types.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/events.html
 *
 * https://www.codeigniter.com/user_guide/libraries/sessions.html
 *
 */

namespace Commons\MAPS;

/**
 * @Entity @Table(name="COM_ORA_GEO_CODES")
 * @HasLifecycleCallbacks
 */
class OraGeoCodes
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="datetime", name="LAST_UPDATED", nullable=false) */
    protected $lastUpdated;

    /** @Column(type="string", name="LAST_SOURCE", length=30, nullable=false) **/
    protected $lastSource;

    /** @Column(type="string", name="COUNTRY_NAME", length=30, nullable=false) **/
    protected $countryName;

    /** @Column(type="string", name="COUNTRY_CODE", length=2, nullable=false) **/
    protected $countryCode;

    /** @Column(type="string", name="STATE", length=60, nullable=false) **/
    protected $state;

    /** @Column(type="string", name="STATE_CODE", length=10, nullable=true) **/
    protected $stateCode;

    /** @Column(type="string", name="PROVINCE", length=60, nullable=false) **/
    protected $province;

    /** @Column(type="string", name="PROVINCE_CODE", length=10, nullable=true) **/
    protected $provinceCode;

    /** @Column(type="string", name="COUNTY", length=60, nullable=true) **/
    protected $county;

    /** @Column(type="string", name="COUNTY_CODE", length=10, nullable=true) **/
    protected $countyCode;

    /** @Column(type="string", name="CITY", length=60, nullable=false) **/
    protected $city;

    /** @Column(type="string", name="CITY_CODE", length=10, nullable=false) **/
    protected $cityCode;

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCountryName()
    {
        return $this->countryName;
    }

    /**
     * @param mixed $countryName
     */
    public function setCountryName($countryName)
    {
        $this->countryName = $countryName;
    }

    /**
     * @return mixed
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param mixed $countryCode
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @return mixed
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param mixed $county
     */
    public function setCounty($county)
    {
        $this->county = $county;
    }


    /**
     * @return mixed
     */
    public function getCountyCode()
    {
        return $this->countyCode;
    }

    /**
     * @param mixed $countryCode
     */
    public function setCountyCode($countyCode)
    {
        $this->countyCode = $countyCode;
    }

    /**
     * @return mixed
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * @param mixed $province
     */
    public function setProvince($province)
    {
        $this->province = $province;
    }

    /**
     * @return mixed
     */
    public function getProvinceCode()
    {
        return $this->provinceCode;
    }

    /**
     * @param mixed $provinceCode
     */
    public function setProvinceCode($provinceCode)
    {
        $this->provinceCode = $provinceCode;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCityCode()
    {
        return $this->cityCode;
    }

    /**
     * @param mixed $cityCode
     */
    public function setCityCode($cityCode)
    {
        $this->cityCode = $cityCode;
    }

    /**
     * @return mixed
     */
    public function getStateCode()
    {
        return $this->stateCode;
    }

    /**
     * @param mixed $stateCode
     */
    public function setStateCode($stateCode)
    {
        $this->stateCode = $stateCode;
    }

    /**
     * @return mixed
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * @param mixed $lastUpdated
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;
    }

    /**
     * @return mixed
     */
    public function getLastSource()
    {
        return $this->lastSource;
    }

    /**
     * @param mixed $lastSource
     */
    public function setLastSource($lastSource)
    {
        $this->lastSource = $lastSource;
    }




}