<?php
/**
 * http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/types.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/events.html
 *
 * https://www.codeigniter.com/user_guide/libraries/sessions.html
 *
 */

namespace Commons\MAPS;

/**
 * @Entity @Table(name="COM_PROJECT_CHARTER")
 * @HasLifecycleCallbacks
 */
class ProjectCharterAccount
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /** @Column(name="DESCRIP", type="string", length=60, nullable=false) * */
    protected $descrip;

    public function getDescrip()
    {
        return $this->descrip;
    }
    public function setDescrip($descrip)
    {
        $this->descrip = $descrip;
    }

    /** @Column(name="SUBTASK_PATTERN", type="string", length=60, nullable=false) * */
    protected $subtaskPattern;

    public function getSubtaskPattern()
    {
        return $this->subtaskPattern;
    }
    public function setSubtaskPattern($subtaskPattern)
    {
        $this->subtaskPattern = $subtaskPattern;
    }

    /** @Column(name="ACCOUNT_PATTERN", type="string", length=60, nullable=false) * */
    protected $accountPattern;

    public function getAccountPattern()
    {
        return $this->accountPattern;
    }
    public function setAccountPattern($accountPattern)
    {
        $this->accountPattern = $accountPattern;
    }


}