<?php
/**
 * http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/types.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/events.html
 *
 * https://www.codeigniter.com/user_guide/libraries/sessions.html
 *
 */

namespace Commons\MAPS;

/**
 * @Entity @Table(name="COM_PROJECT_FF")
 * @HasLifecycleCallbacks
 */
class ProjectFlexFields
{
    /** @Id @Column(type="integer", nullable=false) @GeneratedValue * */
    protected $id;

    /** @Column(name="CODIGO", type="integer", nullable=false) * */
    protected $codigo;

    /** @Column(name="DENOMINACION", type="string", length=60, nullable=false) * */
    protected $denominacion;

    /** @Column(name="LINEA", type="string", length=60, nullable=true) * */
    protected $linea;

    /** @Column(name="FAMILIA", type="string", length=60, nullable=false) * */
    protected $familia;

    /** @Column(name="TYPE", type="string", length=60, nullable=false) * */
    protected $type;

    /** @Column(name="LINE", type="string", length=60, nullable=false) * */
    protected $line;

    /** @Column(name="CONFIGURATION", type="string", length=60, nullable=false) * */
    protected $configuration;

    public function getType()
    {
        return $this->type;
    }
    public function setType($type)
    {
        $this->type = $type;
    }

    public function getLine()
    {
        return $this->line;
    }
    public function setLine($line)
    {
        $this->line = $line;
    }
    public function getConfiguration()
    {
        return $this->configuration;
    }
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param mixed $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @return mixed
     */
    public function getDenominacion()
    {
        return $this->denominacion;
    }

    /**
     * @param mixed $denominacion
     */
    public function setDenominacion($denominacion)
    {
        $this->denominacion = $denominacion;
    }

    /**
     * @return mixed
     */
    public function getLinea()
    {
        return $this->linea;
    }

    /**
     * @param mixed $linea
     */
    public function setLinea($linea)
    {
        $this->linea = $linea;
    }

    /**
     * @return mixed
     */
    public function getFamilia()
    {
        return $this->familia;
    }

    /**
     * @param mixed $familia
     */
    public function setFamilia($familia)
    {
        $this->familia = $familia;
    }



}