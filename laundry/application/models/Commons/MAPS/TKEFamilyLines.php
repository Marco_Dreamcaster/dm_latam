<?php
/**
 * http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/types.html
 *
 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/events.html
 *
 * https://www.codeigniter.com/user_guide/libraries/sessions.html
 *
 */

namespace Commons\MAPS;

/**
 * @Entity @Table(name="COM_TKE_SCC_LINES")
 * @HasLifecycleCallbacks
 */
class TKEFamilyLines
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="datetime", name="LAST_UPDATED", nullable=false) */
    protected $lastUpdated;

    /** @Column(type="string", name="LAST_SOURCE", length=30, nullable=false) **/
    protected $lastSource;

    /** @Column(type="string", name="FAMILY", length=8, nullable=false) **/
    protected $family;

    /** @Column(type="string", name="FAMILY_EN", length=255, nullable=true) **/
    protected $familyEN;

    /** @Column(type="string", name="FAMILY_ESA", length=255, nullable=true) **/
    protected $familyESA;

    /** @Column(type="string", name="SUB_FAMILY", length=8, nullable=false) **/
    protected $subFamily;

    /** @Column(type="string", name="SUB_FAMILY_EN", length=255, nullable=true) **/
    protected $subFamilyEN;

    /** @Column(type="string", name="SUB_FAMILY_ESA", length=255, nullable=true) **/
    protected $subFamilyESA;

    /** @Column(type="string", name="LINE", length=8, nullable=false) **/
    protected $line;

    /** @Column(type="string", name="LINE_EN", length=255, nullable=true) **/
    protected $lineEN;

    /** @Column(type="string", name="LINE_ESA", length=255, nullable=true) **/
    protected $lineESA;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFamilyEN()
    {
        return $this->familyEN;
    }

    /**
     * @param mixed $familyEN
     */
    public function setFamilyEN($familyEN)
    {
        $this->familyEN = $familyEN;
    }

    /**
     * @return mixed
     */
    public function getFamilyESA()
    {
        return $this->familyESA;
    }

    /**
     * @param mixed $familyESA
     */
    public function setFamilyESA($familyESA)
    {
        $this->familyESA = $familyESA;
    }

    /**
     * @return mixed
     */
    public function getSubFamilyEN()
    {
        return $this->subFamilyEN;
    }

    /**
     * @param mixed $subFamilyEN
     */
    public function setSubFamilyEN($subFamilyEN)
    {
        $this->subFamilyEN = $subFamilyEN;
    }

    /**
     * @return mixed
     */
    public function getSubFamilyESA()
    {
        return $this->subFamilyESA;
    }

    /**
     * @param mixed $subFamilyESA
     */
    public function setSubFamilyESA($subFamilyESA)
    {
        $this->subFamilyESA = $subFamilyESA;
    }

    /**
     * @return mixed
     */
    public function getLineEN()
    {
        return $this->lineEN;
    }

    /**
     * @param mixed $lineEN
     */
    public function setLineEN($lineEN)
    {
        $this->lineEN = $lineEN;
    }

    /**
     * @return mixed
     */
    public function getLineESA()
    {
        return $this->lineESA;
    }

    /**
     * @param mixed $lineESA
     */
    public function setLineESA($lineESA)
    {
        $this->lineESA = $lineESA;
    }

    /**
     * @return mixed
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * @param mixed $lastUpdated
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;
    }

    /**
     * @return mixed
     */
    public function getLastSource()
    {
        return $this->lastSource;
    }

    /**
     * @param mixed $lastSource
     */
    public function setLastSource($lastSource)
    {
        $this->lastSource = $lastSource;
    }

    /**
     * @return mixed
     */
    public function getFamily()
    {
        return $this->family;
    }

    /**
     * @param mixed $family
     */
    public function setFamily($family)
    {
        $this->family = $family;
    }

    /**
     * @return mixed
     */
    public function getSubFamily()
    {
        return $this->subFamily;
    }

    /**
     * @param mixed $subFamily
     */
    public function setSubFamily($subFamily)
    {
        $this->subFamily = $subFamily;
    }

    /**
     * @return mixed
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * @param mixed $line
     */
    public function setLine($line)
    {
        $this->line = $line;
    }




}