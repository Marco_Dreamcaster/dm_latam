<?php

namespace Commons\ORA;

Use Commons\Base\BaseCustomerAddress;
use Doctrine\ORM\Query;
Use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * @Entity @Table(name="O_STG_CUSTOMER_ADDRESS") @HasLifecycleCallbacks
 *
 */
class CustomerAddress extends BaseCustomerAddress
{
    protected $em;

    public function __construct()
    {
    }

    /** @PreUpdate */
    public function validatePreUpdate(PreUpdateEventArgs $event){
        $this->validate($event->getEntityManager());
    }

    /** @PrePersist */
    public function validatePrePersist(LifecycleEventArgs  $event){
        $this->validate($event->getEntityManager());
    }

    public function validate($em, $className ='Commons\\ORA\\CustomerAddress')
    {
        // the Base class uses IDL to enforce country-based validations
        //
        parent::validate($em, $this);

        $this->checkORA_GEO_CODE($em);
        $this->checkEmptyLocation($em);

        return $this->locallyValidated;

    }

    public function validateCrossRef($em, $shouldFlush=true)
    {
        $this->crossRefValidated = true;

        $this->em = $em;

        $this->checkForInvalidChildren('Commons\\ORA\CustomerContact', array('origSystemAdressReference'), array($this->getOrigSystemAdressReference()));
        $this->updateChildrenWithParentState( 'Commons\\ORA\CustomerContact', array('origSystemAdressReference'), array($this->getOrigSystemAdressReference()));

        if ($this->crossRefValidated){
            $this->action = '';
        }

        if ($shouldFlush){
            $this->em->persist($this);
            $this->em->flush();
        }

        return $this->crossRefValidated;


    }


    private function checkEmptyLocation($em){
        if ($this->locallyValidated == true) {
            if(($this->getLocation()==null)||(trim($this->getLocation())=='')){
                $this->locallyValidated = false;
                $this->action='Location must be informed';

            }

        }
    }



}


