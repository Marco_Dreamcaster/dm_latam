<?php

namespace Commons\ORA;

Use Commons\Base\BaseCustomerContact;
use Doctrine\ORM\Query;
Use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * @Entity @Table(name="O_STG_CUSTOMER_CONTACT") @HasLifecycleCallbacks
 *
 */
class CustomerContact extends BaseCustomerContact
{
    protected $em;

    public function __construct()
    {
    }

    /** @PreUpdate */
    public function validatePreUpdate(PreUpdateEventArgs $event){
        $this->validate($event->getEntityManager());
    }

    /** @PrePersist */
    public function validatePrePersist(LifecycleEventArgs  $event){
        $this->validate($event->getEntityManager());
    }

    public function validate($em, $className = 'Commons\\ORA\\CustomerContact'){
        // the Base class uses IDL to country-based validations
        //
        parent::validate($em, $this);

        $TelephoneType = $this->getTelephoneType();
        $Telephone = $this->getTelephone();
        $TelephoneAreaCode = $this->getTelephoneAreaCode();
        $TelephoneCountryCode = $this->getPhoneCountryCode();

        switch($TelephoneType){
            case 'PHONE':
            case 'FAX':

                if (strlen(trim($Telephone)) == 0){ //required
                    $this->locallyValidated = false;
                    $this->action = 'Telephone required.';
                    break;

                }elseif (strlen($Telephone) > 25){ //max length
                    $this->locallyValidated = false;
                    $this->action = 'Telephone too large.';
                    break;
                }elseif (1 != preg_match('~[0-9]~', $Telephone)){ //max length
                    $this->locallyValidated = false;
                    $this->action = 'Telephone must have numbers.';
                    break;
                }

                if (strlen(trim($TelephoneAreaCode)) == 0){ //required
                    $this->locallyValidated = false;
                    $this->action = 'Telephone area code required.';
                    break;
                }elseif (strlen(trim($TelephoneAreaCode)) > 10){ //max length
                    $this->locallyValidated = false;
                    $this->action = 'Telephone area code very large.';
                    break;
                }elseif (1 != preg_match('~[0-9]~', $TelephoneAreaCode)){ //max length
                    $this->locallyValidated = false;
                    $this->action = 'Telephone area code must have numbers.';
                    break;
                }

                if (strlen(trim($TelephoneCountryCode)) == 0){ //required
                    $this->locallyValidated = false;
                    $this->action = 'Telephone country code required.';
                    break;

                }elseif (strlen(trim($TelephoneCountryCode)) > 10){ //max length
                    $this->locallyValidated = false;
                    $this->action = 'Telephone country code very large.';
                    break;
                }elseif (1 != preg_match('~[0-9]~', $TelephoneAreaCode)){ //max length
                    $this->locallyValidated = false;
                    $this->action = 'Telephone country code must have numbers.';
                    break;
                }
                if (strlen(trim($this->getEmailAddress())) > 240) { //max length
                    $this->locallyValidated = false;
                    $this->action = 'Email very large.';
                    break;
                }

                /**
                if (strlen(trim($this->getEmailAddress())) > 0) { //max length
                    $parts = explode(',', $this->getEmailAddress());

                    foreach($parts as $part){
                        if (!filter_var($part, FILTER_VALIDATE_EMAIL)){
                            $this->locallyValidated = false;
                            $this->action = 'Invalid email ' + $part;
                        }
                    }
                    break;
                }**/

                break;

            case 'EMAIL':
                if (strlen(trim($this->getEmailAddress())) == 0){ //required
                    $this->locallyValidated = false;
                    $this->action = 'Email required.';

                }elseif (strlen(trim($this->getEmailAddress())) > 240) { //max length
                    $this->locallyValidated = false;
                    $this->action = 'Email very large.';

                }/**else { //max length
                    $parts = explode(',', $this->getEmailAddress());

                    foreach ($parts as $part) {
                        if (!filter_var($part, FILTER_VALIDATE_EMAIL)) {
                            $this->locallyValidated = false;
                            $this->action = 'Invalid email ' + $part;
                        }
                    }
                }**/
                break;

            default: {
                $this->locallyValidated = true;
                $this->action = '';
                break;
            }
            break;
        }

        $this->locallyValidated = $this->checkDuplicateCustomerContactReference($em);

        return $this->locallyValidated;

    }


    protected function checkDuplicateCustomerContactReference($em)
    {
        if ($this->locallyValidated == true && !$this->dismissed) {
            $qb = $em->createQueryBuilder();

            $qb->select('ct')
                ->from('Commons\ORA\CustomerContact', 'ct')
                ->andWhere(
                    $qb->expr()->eq('ct.origSystemContactRef', '?1'),
                    $qb->expr()->eq('ct.dismissed', '?2')
                )
                ->setParameter(1, $this->getOrigSystemContactRef())
                ->setParameter(2, false);

            $unique = $qb->getQuery()->getResult();

            if (sizeof($unique)>1){
                $this->action = '>1 origSystemContactRef '. $this->getOrigSystemContactRef() . ' @ ';
                foreach($unique as $nonunique){
                    $this->action = $this->action.$nonunique->getOrigSystemCustomerRef().', ';
                }

                return false;
            }
            return true;

        }else{
            return $this->locallyValidated;
        }

    }


    public function validateCrossRef($em, $shouldFlush=true)
    {

        $this->crossRefValidated = $this->locallyValidated;

        $this->em = $em;

        $this->checkForInvalidChildren('Commons\\ORA\CustomerAddress', array('projectName'), array($this->getProjectName()));
        $this->checkForInvalidChildren('Commons\\ORA\CustomerHeader', array('projectName'), array($this->getProjectName()));

        $this->updateChildrenWithParentState( 'Commons\\ORA\CustomerAddress', array('projectName'), array($this->getProjectName()),$shouldFlush);
        $this->updateChildrenWithParentState( 'Commons\\ORA\CustomerHeader', array('projectName'), array($this->getProjectName()),$shouldFlush);

        if ($this->crossRefValidated){
            $this->action = '';
        }

        if ($shouldFlush){
            $this->em->persist($this);
            $this->em->flush();
        }

        return $this->crossRefValidated;
    }
}


