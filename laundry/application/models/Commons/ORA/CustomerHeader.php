<?php

namespace Commons\ORA;

Use Commons\Base\BaseCustomerHeader;
use Doctrine\ORM\Query;
Use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * @Entity @Table(name="O_STG_CUSTOMER_HEADER") @HasLifecycleCallbacks
 *
 */
class CustomerHeader extends BaseCustomerHeader
{
    protected $em;

    public function __construct()
    {
    }

    /** @PreUpdate */
    public function validatePreUpdate(PreUpdateEventArgs $event){
        $this->validate($event->getEntityManager());
    }

    /** @PrePersist */
    public function validatePrePersist(LifecycleEventArgs  $event){
        $this->validate($event->getEntityManager());
    }

    public function validate($em, $className = 'Commons\\ORA\\CustomerHeader'){
        // the Base class uses IDL to country-based validations
        //
        parent::validate($em, $this);

        $this->buildTaxPayerIdPerCountry($em);

        $this->checkSinglePrimarySite($em);
        $this->checkAtLeastOneBillToAddress($em);
        $this->checkUniqueLocationsForCustomer($em);
        $this->checkSingleDUN($em);
        $this->checkValidAliases($em);

        return $this->locallyValidated;

    }

    public function validateCrossRef($em, $shouldFlush=true)
    {
        $this->crossRefValidated = $this->locallyValidated;

        $this->em = $em;

        $this->checkForInvalidChildren('Commons\\ORA\CustomerAddress', array('origSystemCustomerRef'), array($this->getOrigSystemCustomerRef()));
        $this->checkForInvalidChildren('Commons\\ORA\CustomerContact', array('origSystemCustomerRef'), array($this->getOrigSystemCustomerRef()));

        $this->updateChildrenWithParentState( 'Commons\\ORA\CustomerAddress', array('origSystemCustomerRef'), array($this->getOrigSystemCustomerRef()), false);
        $this->updateChildrenWithParentState( 'Commons\\ORA\CustomerContact', array('origSystemCustomerRef'), array($this->getOrigSystemCustomerRef()), false);

        if ($shouldFlush){
            $this->em->persist($this);
            $this->em->flush();
        }

        return $this->crossRefValidated;


    }


    private function buildTaxPayerIdPerCountry($em){
        switch ($this->getCountry()){
            case "CO":
            case "CL":
            case "AR":
                $this->setTaxPayerId($this->getCOTaxPayerId().'-'.$this->getGlobalAttribute12());
                break;
        }
    }

    private function checkUniqueLocationsForCustomer($em){
        if ($this->locallyValidated == true) {

            $qb = $em->createQueryBuilder();
            $qb->select('child')
                ->from('Commons\\ORA\CustomerAddress', 'child')
                ->andWhere(
                    $qb->expr()->eq('child.origSystemCustomerRef', '?1'),
                    $qb->expr()->eq('child.dismissed', '?2')

                )
                ->setParameter(1,$this->getOrigSystemCustomerRef())
                ->setParameter(2,false);

            $allChildren = $qb->getQuery()->getResult();

            foreach ($allChildren as $childAddress) {
                $qb = $em->createQueryBuilder();

                switch ($this->country) {
                    default:
                        $qb->select('a')
                            ->from('Commons\\ORA\\CustomerAddress', 'a')
                            ->andWhere(
                                $qb->expr()->eq('a.location', '?1'),
                                $qb->expr()->eq('a.origSystemCustomerRef', '?2'),
                                $qb->expr()->eq('a.dismissed', '?3')

                            )
                            ->distinct()
                            ->setParameter(1, $childAddress->getLocation())
                            ->setParameter(2, $childAddress->getOrigSystemCustomerRef())
                            ->setParameter(3, false);
                        break;
                }

                $loc = $qb->getQuery()->getResult();

                if (sizeof($loc) > 1) {
                    $this->locallyValidated = false;
                    $this->action = '(' . $childAddress->getLocation() . ') is not unique for Customer '.$this->origSystemCustomerRef;
                    break;
                }

            }

        }
    }


    private function checkSinglePrimarySite($em)
    {
        $qb = $em->createQueryBuilder();
        switch ($this->country) {
            default:
                $qb->select('a')
                    ->from('Commons\\ORA\\CustomerAddress', 'a')
                    ->andWhere(
                        $qb->expr()->eq('a.primarySiteUseFlag', '?1'),
                        $qb->expr()->eq('a.origSystemCustomerRef', '?2'),
                        $qb->expr()->eq('a.dismissed', '?3')
                    )
                    ->setParameter(1, true)
                    ->setParameter(2, $this->getOrigSystemCustomerRef())
                    ->setParameter(3, false);
                break;
        }

        $primary = $qb->getQuery()->getResult();

        if ((sizeof($primary) == 0) ) {
            $this->locallyValidated = false;
            $this->action = 'at least one CustomerAddress primarySiteUseFlag must be set to YES for Customer ' . $this->getOrigSystemCustomerRef();

        }else if ((sizeof($primary) > 1)) {
            $this->locallyValidated = false;
            $this->action = 'more than one CustomerAddress primarySiteUseFlag set to YES for Customer ' . $this->getOrigSystemCustomerRef();

        }
    }

    private function checkAtLeastOneBillToAddress($em)
    {
        $qb = $em->createQueryBuilder();
        switch ($this->country) {
            default:
                $qb->select('a')
                    ->from('Commons\\ORA\\CustomerAddress', 'a')
                    ->where(
                        $qb->expr()->andX(
                            $qb->expr()->orX(
                                $qb->expr()->eq('a.siteUseCode', '?1'),
                                $qb->expr()->eq('a.siteUseCode', '?2'),
                                $qb->expr()->eq('a.siteUseCode', '?3')),
                            $qb->expr()->eq('a.origSystemCustomerRef', '?4'),
                            $qb->expr()->eq('a.dismissed', '?5')))
                    ->setParameter(1, 'ALL')
                    ->setParameter(2, 'BILL_TO')
                    ->setParameter(3, 'BOTH')
                    ->setParameter(4, $this->getOrigSystemCustomerRef())
                    ->setParameter(5, false);
                break;
        }

        $billingAddresses = $qb->getQuery()->getResult();

        if ((sizeof($billingAddresses) == 0) ) {
            $this->locallyValidated = false;
            $this->action = 'at least one address must be set to BILL_TO for Customer ' . $this->getOrigSystemCustomerRef();
        }
    }

    private function checkSingleDUN($em)
    {
        if ($this->locallyValidated == true) {

            $qb = $em->createQueryBuilder();
            $qb->select('child')
                ->from('Commons\\ORA\CustomerAddress', 'child')
                ->andWhere(
                    $qb->expr()->eq('child.origSystemCustomerRef', '?1'),
                    $qb->expr()->eq('child.dismissed', '?2')

                )
                ->setParameter(1,$this->getOrigSystemCustomerRef())
                ->setParameter(2,false);

            $allChildren = $qb->getQuery()->getResult();

            $dunCount = 0;

            foreach ($allChildren as $childAddress) {
                if (($childAddress->getSiteUseCode()=='ALL')||($childAddress->getSiteUseCode()=='DUN')){
                    $dunCount = $dunCount + 1;
                }

            }

            if ($dunCount>1){
                $this->locallyValidated = false;
                $this->action='More than one DUN address identified - check SiteUseCode on Addresses of Customer ' . $this->getOrigSystemCustomerRef();
            }

        }
    }

    private function checkValidAliases($em)
    {
        if (($this->getAliases()!=null)&&(trim($this->getAliases()!=''))){
            $aliases = explode(',', $this->getAliases());

            foreach($aliases as $alias){
                $qb = $em->createQueryBuilder();
                $qb->select('a')
                    ->from('Commons\\ORA\\CustomerHeader', 'a')
                    ->andWhere(
                        $qb->expr()->eq('a.origSystemCustomerRef', '?1')
                    )
                    ->setParameter(1, trim($alias));

                $aliasCustomer = $qb->getQuery()->getResult();

                if ((sizeof($aliasCustomer) == 0) ) {
                    $this->locallyValidated = false;
                    $this->action = 'at least one of the Aliases could not be found, check ' . $alias;
                    break;

                }else if ((sizeof($aliasCustomer) > 1)) {
                    $this->locallyValidated = false;
                    $this->action = 'THIS SHOULD NEVER HAPPEN more than one Customer with ID ' . $alias;
                    break;

                }else{
                    if (!$aliasCustomer[0]->getDismissed()){
                        $this->locallyValidated = false;
                        $this->action = 'Please dismiss the alias record ' . $alias;
                    }

                    // check if MigratedBillTo and MigratedShipTo are informed

                }

            }

        }


    }


}


