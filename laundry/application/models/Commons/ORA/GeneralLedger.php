<?php

namespace Commons\ORA;

Use Commons\Base\BaseGeneralLedger;
use Doctrine\ORM\Query;
Use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * @Entity @Table(name="O_STG_GENERAL_LEDGER") @HasLifecycleCallbacks
 *
 */
class GeneralLedger extends BaseGeneralLedger
{
    protected $em;

    public function __construct()
    {
    }

    /** @PreUpdate */
    public function validatePreUpdate(PreUpdateEventArgs $event){
        $this->validate($event->getEntityManager());
    }

    /** @PrePersist */
    public function validatePrePersist(LifecycleEventArgs  $event){
        $this->validate($event->getEntityManager());
    }

    public function validate($em, $class = 'Commons\\ORA\\GeneralLedger'){
        // the Base class uses IDL to country-based validations
        //
        parent::validate($em, $this);
    }

    public function validateCrossRef($em)
    {
        // check Commons\ORA\ProjectHeader validateCrossRef for an example of how to deal with header and detail entities
    }
}


