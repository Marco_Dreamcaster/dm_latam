<?php

namespace Commons\ORA;

Use Commons\Base\BaseItemCategory;
use Doctrine\ORM\Query;
Use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * @Entity @Table(name="O_STG_ITEM_CATEGORY") @HasLifecycleCallbacks
 *
 */
class ItemCategory extends BaseItemCategory
{
    protected $em;

    public function __construct()
    {
    }

    /** @PreUpdate */
    public function validatePreUpdate(PreUpdateEventArgs $event){
        $this->em = $event->getEntityManager();
        $this->validate($this->em);
    }

    /** @PrePersist */
    public function validatePrePersist(LifecycleEventArgs  $event){
        $this->validate($event->getEntityManager());
    }

    public function validate($em, $className = 'Commons\\ORA\\ItemCategory'){
        // the Base class uses IDL to country-based validations
        //
        parent::validate($em, $this);

        $this->checkNullifiedSCCFamily($em);

        return $this->locallyValidated;
    }

    protected function checkNullifiedSCCFamily($em)
    {
        if ($this->locallyValidated == true && !$this->dismissed) {
            if (($this->concatCategoryName==null)||(trim($this->concatCategoryName)=='')){
                $this->locallyValidated = false;
                $this->action='Category Name (SCC Codes) cannot be NULL or empty (choose a value from the combo AND update)';

                return false;
            }

        }

        return true;
    }



    public function validateCrossRef($em, $shouldFlush=true)
    {
        $this->crossRefValidated = $this->locallyValidated;

        $this->em = $em;

        if ($this->crossRefValidated){
            $this->action = '';
        }

        if ($shouldFlush){
            $this->em->flush();
        }

        return $this->crossRefValidated;
    }
}


