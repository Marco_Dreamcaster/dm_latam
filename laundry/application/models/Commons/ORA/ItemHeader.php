<?php

namespace Commons\ORA;

Use Commons\Base\BaseItemHeader;
use Doctrine\ORM\Query;
Use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * @Entity @Table(name="O_STG_ITEM_HEADER") @HasLifecycleCallbacks
 *
 */
class ItemHeader extends BaseItemHeader
{
    protected $em;

    public function __construct()
    {
    }

    /** @PreUpdate */
    public function validatePreUpdate(PreUpdateEventArgs $event){
        $this->validate($event->getEntityManager());
        $this->em = $event->getEntityManager();

        // if the template name has been updated
        // then we must generate a new ItemNumber based on the SCC family
        //
        if (($event->hasChangedField('templateName'))&&($event->getEntity()->getTemplateName()!='LA98')&&($event->getEntity()->getMergedId()==null)){

            $templateNameStem = substr($event->getEntity()->getTemplateName(),0,4);

            $sql = "SELECT TOP 1 ITEM_NUMBER FROM O_STG_ITEM_HEADER WHERE ITEM_NUMBER LIKE '%" .$templateNameStem."%' ORDER BY RIGHT(ITEM_NUMBER,6) DESC";

            $stmt = $this->em->getConnection()->prepare($sql);
            $stmt->execute();

            $itemWithMaxTemplateNameStem = $stmt->fetchAll();

            if (sizeof($itemWithMaxTemplateNameStem)>0){
                $previousMaxId = intval(substr($itemWithMaxTemplateNameStem[0]['ITEM_NUMBER'],7,16));
                $maxId = str_pad($previousMaxId+1, 6, '0', STR_PAD_LEFT);

            }else{
                $maxId = '000001';
            }
            $oldItemNumber = $event->getEntity()->getItemNumber();
            $event->getEntity()->setItemNumber($event->getEntity()->getTemplateName().$maxId);

            $this->moveAssociatedFiles($oldItemNumber, $event->getEntity()->getItemNumber());

        }

        // fix its direct dependencies
        $this->fixItemNumberFor('Commons\ORA\ItemCategory', $event->getEntity()->getOrigItemNumber(), $event->getEntity()->getItemNumber());
        $this->fixItemNumberFor('Commons\ORA\ItemMFGPartNum', $event->getEntity()->getOrigItemNumber(), $event->getEntity()->getItemNumber());
        $this->fixItemNumberFor('Commons\ORA\ItemCrossRef', $event->getEntity()->getOrigItemNumber(), $event->getEntity()->getItemNumber());
        $this->fixItemNumberFor('Commons\ORA\ItemTransDefSubInv', $event->getEntity()->getOrigItemNumber(), $event->getEntity()->getItemNumber());
        $this->fixItemNumberFor('Commons\ORA\ItemTransDefLoc', $event->getEntity()->getOrigItemNumber(), $event->getEntity()->getItemNumber());

        $this->checkForChildren($this->em);

    }


    // https://trello.com/c/ZGrvXose
    // avoid duplicates at Inventory
    //
    protected function checkDuplicateItemHeader($em)
    {
        if ($this->locallyValidated == true && !$this->dismissed) {
            $qb = $em->createQueryBuilder();

            $qb->select('i')
                ->from('Commons\ORA\ItemHeader', 'i')
                ->andWhere(
                    $qb->expr()->neq('i.origItemNumber', '?1'),
                    $qb->expr()->eq('i.itemNumber', '?2'),
                    $qb->expr()->eq('i.country', '?3'),
                    $qb->expr()->eq('i.dismissed', '?4')
                )
                ->setParameter(1, $this->getOrigItemNumber())
                ->setParameter(2, $this->getItemNumber())
                ->setParameter(3, $this->getCountry())
                ->setParameter(4, false);

            $unique = $qb->getQuery()->getResult();

            if (sizeof($unique)>=1){
                $this->action = '>1 (more than one) item with '. $this->getItemNumber() . ' - duplicated itemNumber';
                return false;
            }

        }

        return true;
    }


    protected function moveAssociatedFiles($oldItemNumber, $newItemNumber){
        if ($oldItemNumber!=$newItemNumber){
            $host = $this->em->getConnection()->getHost();

            if ($host=='10.148.1.47'){
                $itemsFolder = 'C:\dm_latam\laundry\itemFiles';
            }else{
                $itemsFolder = 'D:\pub\dm_latam\laundry\itemFiles';

            }

            $originalFolder = $itemsFolder . '/resources/' . $oldItemNumber;

            if (is_dir($originalFolder)) {
                $newFolder = $itemsFolder . '/resources/'.trim($newItemNumber);

                if (!is_dir($newFolder)) {
                    mkdir($newFolder, 0777, true);
                }

                $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($originalFolder), \RecursiveIteratorIterator::SELF_FIRST);

                foreach($files as $name => $file) {
                    $baseFileName = basename($name);

                    if (($baseFileName != '.') && ($baseFileName != '..') && is_file($name)) {
                        rename($name, $newFolder.'/'.$baseFileName);
                    }

                }

                rmdir($originalFolder);

            }

        }


    }

    /** @PostUpdate */
    public function cascadeChangesToMergedItems(LifecycleEventArgs $event){
        $this->em = $event->getEntityManager();
        $qb = $this->em->createQueryBuilder();
        $qb->select('mi')
            ->from('Commons\ORA\ItemHeader', 'mi')
            ->where($qb->expr()->eq('mi.mergedId', '?1'))
            ->setParameter(1, $event->getEntity()->getId());

        $mergedItems = $qb->getQuery()->getResult();

        foreach($mergedItems as $mi){
            $mi = $this->cloneNonMetaPropertiesByReflection($event->getEntity(), $mi);
            $mi->setHasPicture($event->getEntity()->getHasPicture());
            $this->em->merge($mi);
            $this->em->flush();
        }

    }

    public function checkForChildren($em){
        $this->em = $em;
        $qb = $this->em->createQueryBuilder();
        $qb->select('mi')
            ->from('Commons\ORA\ItemHeader', 'mi')
            ->where($qb->expr()->eq('mi.mergedId', '?1'))
            ->setParameter(1, $this->getId());

        $mergedItems = $qb->getQuery()->getResult();

        $this->childless = sizeof($mergedItems)==0;
    }


    public function cloneNonMetaPropertiesByReflection($source, $target){
        $srcRfx = new \ReflectionClass($source);
        $targetRfx = new \ReflectionClass($target);

        // all value properties, except migration meta-tags
        //
        foreach($srcRfx->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED) as $prop){
            if (($prop->getName()!="id")&&
                ($prop->getName()!="recommended")&&
                ($prop->getName()!="action")&&
                ($prop->getName()!="cleansed")&&
                ($prop->getName()!="dismissed")&&
                ($prop->getName()!="ranking")&&
                ($prop->getName()!="locallyValidated")&&
                ($prop->getName()!="crossRefValidated")&&
                ($prop->getName()!="mergedId")&&
                ($prop->getName()!="sameCountryParent")&&
                ($prop->getName()!="childless")&&
                ($prop->getName()!="country")&&
                ($prop->getName()!="observations")&&

                ($prop->getName()!="origItemNumber")&&
                ($prop->getName()!="orgName")&&
                ($prop->getName()!="organizationCode")&&

                ($targetRfx->hasMethod("set".ucfirst($prop->getName())))){
                    $getter = $srcRfx->getMethod("get".ucfirst($prop->getName()));
                    $setter = $targetRfx->getMethod("set".ucfirst($prop->getName()));

                    $setter->invoke($target, $getter->invoke($source));


            }
        }

        return $target;

    }



    private function fixItemNumberFor($className, $origItemNumber, $itemNumber){
        $qb = $this->em->createQueryBuilder();

        $qb->select('child')
            ->from($className, 'child')
            ->where($qb->expr()->eq('child.origItemNumber', '?1'))
            ->setParameter(1, $origItemNumber)
;

        $children = $qb->getQuery()->getResult();

        foreach($children as $child){
            $child->setItemNumber($itemNumber);
            $this->em->merge($child);
        }

    }

    /** @PrePersist */
    public function validatePrePersist(LifecycleEventArgs  $event){
        $this->validate($event->getEntityManager());
    }

    public function validate($em, $className = 'Commons\\ORA\\ItemHeader'){
        // the Base class uses IDL to country-based validations
        //
        parent::validate($em, $this);

        if ($this->locallyValidated){
            $this->locallyValidated = $this->checkDuplicateItemHeader($em);
        }

        return $this->locallyValidated;

    }

    public function validateCrossRef($em, $shouldFlush=true)
    {
        $this->em = $em;
        $this->crossRefValidated = $this->locallyValidated;

        if ($this->locallyValidated){
            $this->checkForInvalidChildren('Commons\\ORA\ItemCategory', array('origItemNumber'), array($this->getOrigItemNumber()));
            $this->checkForInvalidChildren('Commons\\ORA\ItemCrossRef', array('origItemNumber'), array($this->getOrigItemNumber()));
            $this->checkForInvalidChildren('Commons\\ORA\ItemMFGPartNum', array('origItemNumber'), array($this->getOrigItemNumber()));
            $this->checkForInvalidChildren('Commons\\ORA\ItemTransDefLoc', array('origItemNumber'), array($this->getOrigItemNumber()));
            $this->checkForInvalidChildren('Commons\\ORA\ItemTransDefSubInv', array('origItemNumber'), array($this->getOrigItemNumber()));
        }

        $this->em->merge($this);

        if ($shouldFlush){
            $this->em->flush();
        }

        $this->updateChildrenWithParentState( 'Commons\\ORA\ItemCategory', array('origItemNumber'), array($this->getOrigItemNumber()), $shouldFlush);
        $this->updateChildrenWithParentState( 'Commons\\ORA\ItemCrossRef', array('origItemNumber'), array($this->getOrigItemNumber()), $shouldFlush);
        $this->updateChildrenWithParentState( 'Commons\\ORA\ItemMFGPartNum', array('origItemNumber'), array($this->getOrigItemNumber()), $shouldFlush);
        $this->updateChildrenWithParentState( 'Commons\\ORA\ItemTransDefLoc', array('origItemNumber'), array($this->getOrigItemNumber()), $shouldFlush);
        $this->updateChildrenWithParentState( 'Commons\\ORA\ItemTransDefSubInv', array('origItemNumber'), array($this->getOrigItemNumber()), $shouldFlush);

        return $this->crossRefValidated;

    }
}


