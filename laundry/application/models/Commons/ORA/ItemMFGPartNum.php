<?php

namespace Commons\ORA;

Use Commons\Base\BaseItemMFGPartNum;
use Doctrine\ORM\Query;
Use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * @Entity @Table(name="O_STG_ITEM_MFG_PART_NUM") @HasLifecycleCallbacks
 *
 */
class ItemMFGPartNum extends BaseItemMFGPartNum
{
    protected $em;

    public function __construct()
    {
    }

    /** @PreUpdate */
    public function validatePreUpdate(PreUpdateEventArgs $event){
        $this->validate($event->getEntityManager());
    }

    /** @PrePersist */
    public function validatePrePersist(LifecycleEventArgs  $event){
        $this->validate($event->getEntityManager());
    }

    public function validate($em, $className = 'Commons\\ORA\\ItemMFGPartNum'){
        // the Base class uses IDL to country-based validations
        //
        parent::validate($em, $this);
        return $this->locallyValidated;

    }

    public function validateCrossRef($em, $shouldFlush=true)
    {
        $this->crossRefValidated = $this->locallyValidated;

        $this->em = $em;

        if ($this->crossRefValidated){
            $this->action = '';
        }

        if ($shouldFlush){
            $this->em->flush();
        }


        return $this->crossRefValidated;

    }
}


