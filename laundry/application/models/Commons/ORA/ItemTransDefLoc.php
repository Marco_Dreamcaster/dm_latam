<?php

namespace Commons\ORA;

Use Commons\Base\BaseItemTransDefLoc;
use Doctrine\ORM\Query;
Use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * @Entity @Table(name="O_STG_ITEM_TRANS_DEF_LOC") @HasLifecycleCallbacks
 *
 */
class ItemTransDefLoc extends BaseItemTransDefLoc
{
    protected $em;

    public function __construct()
    {
    }

    /** @PreUpdate */
    public function validatePreUpdate(PreUpdateEventArgs $event){
        $this->validate($event->getEntityManager());
    }

    /** @PrePersist */
    public function validatePrePersist(LifecycleEventArgs  $event){
        $this->validate($event->getEntityManager());
    }

    public function validate($em, $className = 'Commons\\ORA\\ItemTransDefLoc'){
        // the Base class uses IDL to country-based validations
        //
        parent::validate($em, $this);

        $matches = array();
        preg_match("/^[a-zA-Z\d]+\.[a-zA-Z\d]+\.[a-zA-Z\d]+$/", $this->locatorName, $matches);

        if (sizeof($matches)==0){
            $this->locallyValidated = false;
            $this->action='locator '.$this->locatorName.' must be formatted as RACK.ROW.BIN';
        }

        return $this->locallyValidated;


    }

    public function validateCrossRef($em, $shouldFlush=true)
    {
        $this->crossRefValidated = $this->locallyValidated;

        $this->em = $em;

        if ($this->crossRefValidated){
            $this->action = '';
        }

        if ($shouldFlush){
            $this->em->flush();
        }

        return $this->crossRefValidated;

    }
}


