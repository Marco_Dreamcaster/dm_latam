<?php

namespace Commons\ORA;

Use Commons\Base\BaseItemTransDefSubInv;
use Doctrine\ORM\Query;
Use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * @Entity @Table(name="O_STG_ITEM_TRANS_DEF_SUB_INV") @HasLifecycleCallbacks
 *
 */
class ItemTransDefSubInv extends BaseItemTransDefSubInv
{
    protected $em;

    public function __construct()
    {
    }

    /** @PreUpdate */
    public function validatePreUpdate(PreUpdateEventArgs $event){
        $this->validate($event->getEntityManager());
    }

    /** @PrePersist */
    public function validatePrePersist(LifecycleEventArgs  $event){
        $this->validate($event->getEntityManager());
    }

    public function validate($em, $className = 'Commons\\ORA\\ItemTransDefSubInv'){
        // the Base class uses IDL to country-based validations
        //
        parent::validate($em, $this);

        if ($this->checkNoTransDefLocator($em)){
            $this->setSubinventoryName("DEPCAL");

        }else{
            $this->setSubinventoryName("DEP01");

        }

        return $this->locallyValidated;

    }

    public function validateCrossRef($em, $shouldFlush=true)
    {
        $this->crossRefValidated = $this->locallyValidated;

        $this->em = $em;

        if ($this->crossRefValidated){
            $this->action = '';
        }

        if ($shouldFlush){
            $this->em->flush();
        }

        return $this->crossRefValidated;

    }

    public function checkNoTransDefLocator($em){

        $qb = $em->createQueryBuilder();
        $qb->select('mi.id')
            ->from('Commons\ORA\\ItemTransDefLoc', 'mi')
            ->andWhere(
                $qb->expr()->eq('mi.dismissed', '?1'),
                $qb->expr()->eq('mi.origItemNumber', '?2')
                )
            ->setParameter(1, false)
            ->setParameter(2, $this->getOrigItemNumber());

        $nonDismissedLocators = $qb->getQuery()->getResult();


        return sizeof($nonDismissedLocators)==0;
    }
}


