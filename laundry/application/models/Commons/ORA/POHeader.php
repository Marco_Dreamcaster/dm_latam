<?php

namespace Commons\ORA;

Use Commons\Base\BasePOHeader;
use Doctrine\ORM\Query;
Use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * @Entity @Table(name="O_STG_PO_HEADER") @HasLifecycleCallbacks
 *
 */
class POHeader extends BasePOHeader
{
    protected $em;

    public function __construct()
    {
    }

    /** @PreUpdate */
    public function validatePreUpdate(PreUpdateEventArgs $event){
        $this->validate($event->getEntityManager());
    }

    /** @PrePersist */
    public function validatePrePersist(LifecycleEventArgs  $event){
        $this->validate($event->getEntityManager());
    }

    public function validate($em, $className = 'Commons\\ORA\\POHeader'){
        // the Base class uses IDL to country-based validations
        //
        parent::validate($em, $this);
    }

    public function validateCrossRef($em)
    {
        $this->crossRefValidated = $this->locallyValidated;

        $this->em = $em;



        if ($this->crossRefValidated){
            $this->action = '';
        }

        $this->em->persist($this);
        $this->em->flush();
    }
}

