<?php
namespace Commons\ORA;

Use Commons\Base\BaseProjectClassifications;
use Doctrine\ORM\Query;
Use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;


/**
 * @Entity @Table(name="O_STG_PROJECT_CLASSIFICATIONS") @HasLifecycleCallbacks
 *
 */
class ProjectClassifications extends BaseProjectClassifications
{
    public function __construct()
    {
    }

    /** @PreUpdate */
    public function validatePreUpdate(PreUpdateEventArgs $event)
    {
        $this->validate($event->getEntityManager());
    }

    /** @PrePersist */
    public function validatePrePersist(LifecycleEventArgs  $event)
    {
        $this->validate($event->getEntityManager());
    }

    public function validate($em, $className = "Commons\\ORA\\ProjectClassifications"){
        // the Base class uses IDL to country-based validations
        //
        parent::validate($em, $this);

        return $this->locallyValidated;

    }

}
