<?php

namespace Commons\ORA;

Use Commons\Base\BaseProjectHeader;
use Doctrine\ORM\Query;
Use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * @Entity @Table(name="O_STG_PROJECT_HEADER") @HasLifecycleCallbacks
 *
 */
class ProjectHeader extends BaseProjectHeader
{
    protected $em;

    public function __construct()
    {
    }

    /** @PreUpdate */
    public function validatePreUpdate(PreUpdateEventArgs $event)
    {
        $this->validate($event->getEntityManager());
    }

    /** @PrePersist */
    public function validatePrePersist(LifecycleEventArgs  $event)
    {
        $this->validate($event->getEntityManager());
    }

    public function validate($em, $className = "Commons\\ORA\\ProjectHeader")
    {
        // the Base class uses IDL to country-based validations
        //
        parent::validate($em, $this);


        if ($this->locallyValidated) {
            $kickoffDate = $this->getStartDate()->format('Y-m-d');
            $completionDate = $this->getCompletionDate()->format('Y-m-d');

            if ($completionDate<$kickoffDate) {
                $this->locallyValidated = false;
                $this->action = 'the completion date ' .$completionDate . ' is before the project \'s kickoff date ' . $kickoffDate;
            }

        }

        // check Customer Header rules
        //
        if ($this->locallyValidated) {
            $qb = $em->createQueryBuilder();
            $qb->select('customer')
                ->from('Commons\\ORA\\CustomerHeader', 'customer')
                ->andWhere(
                    $qb->expr()->eq('customer.origSystemCustomerRef', '?1'))
                ->setParameter(1, $this->getCustomerNumber());

            $allCustomer = $qb->getQuery()->getResult();

            if (sizeof($allCustomer)==0){
                $this->locallyValidated = false;
                $this->action = 'customer ' .$this->getCustomerNumber() . ' could not be found';

            }elseif (sizeof($allCustomer)>1){
                $this->locallyValidated = false;
                $this->action = 'more than one customer with reference ' .$this->getCustomerNumber();

            }elseif ($allCustomer[0]->getDismissed()){
                $this->locallyValidated = false;
                $this->action = 'customer ' .$this->getCustomerNumber() . ' was dismissed';

            }elseif (!$allCustomer[0]->getCrossRefValidated()){
                $this->locallyValidated = false;
                $this->action = 'customer ' .$this->getCustomerNumber() . ' is not validated';

            }

        }

        // check Customer Addresses SHIP_TO
        //
        if ($this->locallyValidated) {
            $qb = $em->createQueryBuilder();
            $qb->select('ca')
                ->from('Commons\\ORA\\CustomerAddress', 'ca')
                ->andWhere(
                    $qb->expr()->eq('ca.origSystemAdressReference', '?1')
                    )
                ->setParameter(1, $this->getShipToAddressRef());

            $allAddress = $qb->getQuery()->getResult();

            if (sizeof($allAddress)==0){
                $this->locallyValidated = false;
                $this->action = 'customer address ' .$this->getShipToAddressRef() . ' could not be found';

            }elseif (sizeof($allAddress)>1){
                $this->locallyValidated = false;
                $this->action = 'more than one customer address with reference ' .$this->getShipToAddressRef();

            }elseif ($allAddress[0]->getDismissed()){
                $this->locallyValidated = false;
                $this->action = 'customer address ' .$this->getShipToAddressRef() . ' was dismissed';

            }elseif (!$allAddress[0]->getCrossRefValidated()){
                $this->locallyValidated = false;
                $this->action = 'customer address ' .$this->getShipToAddressRef() . ' is not validated';

            }elseif ($allAddress[0]->getSiteUseCode()!='ALL'&&$allAddress[0]->getSiteUseCode()!='BOTH'&&$allAddress[0]->getSiteUseCode()!='SHIP_TO'){
                $this->locallyValidated = false;
                $this->action = 'customer address ' .$this->getShipToAddressRef() . ' siteUseCode is neither SHIP_TO, BOTH, or ALL';

            }

        }

        // check Customer Addresses BILL_TO
        //
        if ($this->locallyValidated) {
            $qb = $em->createQueryBuilder();
            $qb->select('ca')
                ->from('Commons\\ORA\\CustomerAddress', 'ca')
                ->andWhere($qb->expr()->eq('ca.origSystemAdressReference', '?1'))
                ->setParameter(1, $this->getBillToAddressRef());

            $allAddress = $qb->getQuery()->getResult();

            if (sizeof($allAddress)==0){
                $this->locallyValidated = false;
                $this->action = 'customer address ' .$this->getBillToAddressRef() . ' could not be found';

            }elseif (sizeof($allAddress)>1){
                $this->locallyValidated = false;
                $this->action = 'more than one customer address with reference ' .$this->getBillToAddressRef();

            }elseif ($allAddress[0]->getDismissed()){
                $this->locallyValidated = false;
                $this->action = 'customer address ' .$this->getBillToAddressRef() . ' was dismissed';

            }elseif (!$allAddress[0]->getCrossRefValidated()){
                $this->locallyValidated = false;
                $this->action = 'customer address ' .$this->getBillToAddressRef() . ' is not validated';

            }elseif ($allAddress[0]->getSiteUseCode()!='ALL'&&$allAddress[0]->getSiteUseCode()!='BOTH'&&$allAddress[0]->getSiteUseCode()!='BILL_TO'){
                $this->locallyValidated = false;
                $this->action = 'customer address ' .$this->getBillToAddressRef() . ' siteUseCode is neither BILL_TO, BOTH, or ALL';

            }

        }


        // check Customer Contact
        //
        if (($this->locallyValidated)&&($this->getContactRef()!='')) {
            $qb = $em->createQueryBuilder();
            $qb->select('cc')
                ->from('Commons\\ORA\\CustomerContact', 'cc')
                ->andWhere($qb->expr()->eq('cc.origSystemContactRef', '?1'))
                ->setParameter(1, $this->getContactRef());

            $allContacts = $qb->getQuery()->getResult();

            if (sizeof($allContacts) == 0) {
                $this->locallyValidated = false;
                $this->action = 'customer contact ' . $this->getContactRef() . ' could not be found';

            } elseif (sizeof($allContacts) > 1) {
                $this->locallyValidated = false;
                $this->action = 'more than one customer contact with reference ' . $this->getBillToAddressRef();

            } elseif ($allContacts[0]->getDismissed()) {
                $this->locallyValidated = false;
                $this->action = 'customer contact ' . $this->getContactRef() . ' was dismissed';

            } elseif (!$allContacts[0]->getCrossRefValidated()) {
                $this->locallyValidated = false;
                $this->action = 'customer contact ' . $this->getContactRef() . ' is not validated';

            }
        }

        // check Project Tasks
        //
        if ($this->locallyValidated) {
            $qb = $em->createQueryBuilder();
            $qb->select('pt')
                ->from('Commons\\ORA\\ProjectTasks', 'pt')
                ->andWhere($qb->expr()->eq('pt.siglaPrimaryKey', '?1'))
                ->setParameter(1, $this->getSiglaPrimaryKey());

            $allTasks = $qb->getQuery()->getResult();

            if (sizeof($allTasks) == 0) {
                $this->locallyValidated = false;
                $this->action = 'no project tasks for ' . $this->getSiglaPrimaryKey();

            }


        }

        if ($this->locallyValidated){
            $this->action='';
        }

        return $this->locallyValidated;
    }


    public function validateCrossRef($em, $shouldFlush=true)
    {
        $this->crossRefValidated = $this->locallyValidated;

        $this->em = $em;

        $this->checkForInvalidChildren('Commons\\ORA\ProjectTasks', array('projectName'), array($this->getProjectName()));
        $this->checkForInvalidChildren('Commons\\ORA\ProjectClassifications', array('projectName'), array($this->getProjectName()));
        $this->checkForInvalidChildren('Commons\\ORA\ProjectRetRules', array('projectName'), array($this->getProjectName()));

        $this->updateChildrenWithParentState( 'Commons\\ORA\ProjectTasks', array('projectName'), array($this->getProjectName()),$shouldFlush);
        $this->updateChildrenWithParentState( 'Commons\\ORA\ProjectClassifications', array('projectName'), array($this->getProjectName()),$shouldFlush);
        $this->updateChildrenWithParentState( 'Commons\\ORA\ProjectRetRules', array('projectName'), array($this->getProjectName()),$shouldFlush);

        if ($this->crossRefValidated){
            $this->action = '';
        }

        if ($shouldFlush){
            $this->em->persist($this);
            $this->em->flush();
        }

        return $this->crossRefValidated;
    }
}

