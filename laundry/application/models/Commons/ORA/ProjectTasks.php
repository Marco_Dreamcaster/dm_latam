<?php

namespace Commons\ORA;

use Commons\Base\BaseProjectTasks;
use Doctrine\ORM\Query;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * @Entity @Table(name="O_STG_PROJECT_TASKS") @HasLifecycleCallbacks
 *
 */
class ProjectTasks extends BaseProjectTasks
{
    const HEADER1_CLASS_NAME = 'Commons\\ORA\\ProjectHeader';
    const HEADER1_KEY_COLUMNS = array('projectName');
    const CANCEL1 = "Projects/customEditCleansed/";

    private $em;

    public function __construct()
    {
    }

    /** @PreUpdate */
    public function validatePreUpdate(PreUpdateEventArgs $event)
    {
        $this->validate($event->getEntityManager());
    }

    /** @PrePersist */
    public function validatePrePersist(LifecycleEventArgs $event)
    {
        $this->validate($event->getEntityManager());
    }

    public function validate($em, $class = "Commons\\ORA\\ProjectTasks")
    {
        parent::validate($em, $this);

        if ($this->locallyValidated) {
            // one more check to go
            //
            $qb = $em->createQueryBuilder();

            $qb->select('pff')
                ->from('Commons\\MAPS\ProjectFlexFields', 'pff')
                ->andWhere(
                    $qb->expr()->eq('pff.type', '?1'),
                    $qb->expr()->eq('pff.line', '?2'),
                    $qb->expr()->eq('pff.configuration', '?3'))
                ->setParameter(1, $this->attribute2)
                ->setParameter(2, $this->attribute7)
                ->setParameter(3, $this->attribute3);

            $combination = $qb->getQuery()->getResult();

            if (sizeof($combination) === 0) {
                $this->locallyValidated = false;
                $this->action = '(' . $this->attribute2 . ', ' . $this->attribute7 . ', ' . $this->attribute3 . ') is invalid (Type,Line,Configuration) combination, check PFF';
            }
        }

        if ($this->locallyValidated) {
            $qb = $em->createQueryBuilder();
            $qb->select('header')
                ->from(self::HEADER1_CLASS_NAME, 'header')
                ->where($qb->expr()->eq('header.projectName', '?1'))
                ->setParameter(1, $this->getProjectName());

            try{
                $project = $qb->getQuery()->getResult();

                if (sizeof($project)==1){
                    $startDate = $this->getStartDate()->format('Y-m-d');
                    $finishDate = $this->getFinishDate()->format('Y-m-d');
                    $kickoffDate = $project[0]->getStartDate()->format('Y-m-d');
                    $completionDate = $project[0]->getCompletionDate()->format('Y-m-d');

                    if ($startDate<$kickoffDate) {
                        $this->locallyValidated = false;
                        $this->action = 'the start date ' .$startDate . ' is before the project \'s kickoff date ' . $kickoffDate;

                    }
                    elseif ($finishDate<$kickoffDate) {
                        $this->locallyValidated = false;
                        $this->action = 'the finish date ' .$finishDate . ' is before the project \'s kickoff date ' . $kickoffDate;

                    }
                    elseif ($startDate>$completionDate) {
                        $this->locallyValidated = false;
                        $this->action = 'the start date ' .$finishDate . ' is after the project \'s completion date ' . $completionDate;
                        return;
                    }
                    elseif ($finishDate>$completionDate) {
                        $this->locallyValidated = false;
                        $this->action = 'the finish date ' .$finishDate . ' is after the project \'s completion date ' . $completionDate;
                        return;
                    }
                    elseif ($startDate>$finishDate) {
                        $this->locallyValidated = false;
                        $this->action = 'the start date ' .$startDate . ' is after the task \'s finish date ' . $completionDate;
                        return;
                    }elseif($this->locallyValidated) {
                        $this->locallyValidated = $this->checkDuplicateProjectTask($em);
                    }

                }else{
                    $this->locallyValidated = false;
                    $this->action = 'more than one Project found with name ' .$this->getProjectName();

                }



            }catch(\Exception $e){
                echo $e.$this->getProjectName();

            }



        }


        if ($this->locallyValidated) {
            if ($this->crossRefValidated) {
                $this->action = '';
            } else {
                $this->action = 'cross reference problem, check header';
            }
        }


        return $this->locallyValidated;


    }



    protected function checkDuplicateProjectTask($em)
    {
        if ($this->locallyValidated == true && !$this->dismissed) {
            $qb = $em->createQueryBuilder();

            $qb->select('pt')
                ->from('Commons\ORA\ProjectTasks', 'pt')
                ->andWhere(
                    $qb->expr()->eq('pt.taskNumber', '?1'),
                    $qb->expr()->eq('pt.projectName', '?2'),
                    $qb->expr()->eq('pt.dismissed', '?3')
                )
                ->setParameter(1, $this->getTaskNumber())
                ->setParameter(2, $this->getProjectName())
                ->setParameter(3, false);

            $unique = $qb->getQuery()->getResult();

            if (sizeof($unique)>1){
                $this->action = '>1 taskNumber '. $this->getTaskNumber() . ' @ ';
                foreach($unique as $nonunique){
                    $this->action = $this->action.$nonunique->getProjectName().', ';
                }

                return false;
            }

        }

        return true;
    }


}

