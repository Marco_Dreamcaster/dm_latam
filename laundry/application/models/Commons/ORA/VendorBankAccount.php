<?php

namespace Commons\ORA;

Use Commons\Base\BaseVendorBankAccount;
use Doctrine\ORM\Query;
Use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * @Entity @Table(name="O_STG_VENDOR_BANK_ACCOUNT", indexes={@Index(name="search_vendorBankAccount", columns={"VENDOR_NUMBER"})}) @HasLifecycleCallbacks
 *
 */
class VendorBankAccount extends BaseVendorBankAccount
{
    protected $em;

    public function __construct()
    {
    }

    /** @PreUpdate */
    public function validatePreUpdate(PreUpdateEventArgs $event){
        $this->validate($event->getEntityManager());
    }

    /** @PrePersist */
    public function validatePrePersist(LifecycleEventArgs  $event){
        $this->validate($event->getEntityManager());
    }

    public function validate($em, $className = 'Commons\\ORA\\VendorBankAccount'){
        // the Base class uses IDL to country-based validations
        //
        parent::validate($em, $this);

        $disallowedValues = ['UNK','FIX', 'FIXME'];

        foreach($disallowedValues as $disallowed){
            if (stristr($this->getBankAccountName(),$disallowed)!=false){
                $this->locallyValidated = false;
                $this->action = 'please inform a valid BankAccountName';
                break;
            }else if (stristr($this->getBankName(),$disallowed)!=false){
                $this->locallyValidated = false;
                $this->action = 'please inform a valid BankName';
                break;
            }else if (stristr($this->getBankNumber(),$disallowed)!=false){
                $this->locallyValidated = false;
                $this->action = 'please inform a valid BankNumber';
                break;
            }else if (stristr($this->getBranchName(),$disallowed)!=false){
                $this->locallyValidated = false;
                $this->action = 'please inform a valid BranchName';
                break;
            }
        }

        return $this->locallyValidated;

    }

    public function validateCrossRef($em)
    {
        $this->crossRefValidated = $this->locallyValidated;

        $this->em = $em;

        if ($this->crossRefValidated){
            $this->action = '';
        }

        $this->em->persist($this);
        $this->em->flush();

        return $this->crossRefValidated;

    }
}


