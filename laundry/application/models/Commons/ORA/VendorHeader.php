<?php

namespace Commons\ORA;

Use Commons\Base\BaseVendorHeader;
use Doctrine\ORM\Query;
Use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * @Entity @Table(name="O_STG_VENDOR_HEADER", indexes={@Index(name="search_vendorHeader", columns={"SEGMENT1"})}) @HasLifecycleCallbacks
 *
 */
class VendorHeader extends BaseVendorHeader
{
    protected $em;

    public function __construct()
    {
    }

    /** @PreUpdate */
    public function validatePreUpdate(PreUpdateEventArgs $event){
        $this->validate($event->getEntityManager());
    }

    /** @PrePersist */
    public function validatePrePersist(LifecycleEventArgs  $event){
        $this->validate($event->getEntityManager());
    }

    public function validate($em, $className = 'Commons\\ORA\\VendorHeader'){
        // the Base class uses IDL to country-based validations
        //
        $this->buildTaxPayerIdPerCountry($em);

        parent::validate($em, $this);

        return $this->locallyValidated;

    }

    public function validateCrossRef($em, $shouldFlush=true)
    {
        $this->em = $em;

        $this->crossRefValidated = $this->locallyValidated;

        $this->checkForInvalidChildren('Commons\\ORA\VendorSite', array('vendorNumber'), array($this->getSegment1()));
        $this->checkForInvalidChildren('Commons\\ORA\VendorSiteContact', array('vendorNumber'), array($this->getSegment1()));
        $this->checkForInvalidChildren('Commons\\ORA\VendorBankAccount', array('vendorNumber'), array($this->getSegment1()));

        $this->updateChildrenWithParentState( 'Commons\\ORA\VendorSite', array('vendorNumber'), array($this->getSegment1()),$shouldFlush);
        $this->updateChildrenWithParentState( 'Commons\\ORA\VendorSiteContact', array('vendorNumber'), array($this->getSegment1()),$shouldFlush);
        $this->updateChildrenWithParentState( 'Commons\\ORA\VendorBankAccount', array('vendorNumber'), array($this->getSegment1()),$shouldFlush);

        if ($shouldFlush){
            $this->em->persist($this);
            $this->em->flush();
        }

        return $this->crossRefValidated;

    }

    private function buildTaxPayerIdPerCountry($em){
        $this->setNum1099($this->getVatRegistrationNum());

        switch ($this->getCountry()){
            case "CO":
            case "CL":
            case "AR":
                $parts = explode('-', $this->getNum1099());

                if (sizeof($parts)==2){
                    $this->setCoExtAttribute8($parts[0]);
                    $this->setGlobalAttribute12($parts[1]);

                }else if (sizeof($parts)>2){
                    $left = '';
                    for ($i=0;$i<sizeof($parts)-1;$i++){
                        $left = $left . $parts[$i];
                        if ($i!=sizeof($parts)-2){
                            $left=$left.'-';
                        }
                    }

                    $this->setCoExtAttribute8($left);
                    $this->setGlobalAttribute12($parts[$i]);


                }

        }
    }




}
