<?php

namespace Commons\ORA;

Use Commons\Base\BaseVendorSite;
use Doctrine\ORM\Query;
Use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * @Entity @Table(name="O_STG_VENDOR_SITE", indexes={@Index(name="search_vendorSite", columns={"VENDOR_NUMBER", "VENDOR_SITE_CODE"})}) @HasLifecycleCallbacks
 *
 */
class VendorSite extends BaseVendorSite
{
    protected $em;

    public function __construct()
    {
    }

    /** @PreUpdate */
    public function validatePreUpdate(PreUpdateEventArgs $event){
        $this->validate($event->getEntityManager());
    }

    /** @PrePersist */
    public function validatePrePersist(LifecycleEventArgs  $event){
        $this->validate($event->getEntityManager());
    }

    public function validate($em, $className = 'Commons\\ORA\\VendorSite'){
        // the Base class uses IDL to country-based validations
        //
        parent::validate($em, $this);

        $this->checkORA_GEO_CODE($em);

        return $this->locallyValidated;

    }

    public function validateCrossRef($em, $shouldFlush = true)
    {
        $this->crossRefValidated = $this->locallyValidated;

        $this->em = $em;

        $this->checkForInvalidChildren('Commons\\ORA\VendorSiteContact', array('vendorSiteCode'), array($this->getVendorSiteCode()));

        $this->updateChildrenWithParentState( 'Commons\\ORA\VendorSiteContact', array('vendorSiteCode'), array($this->getVendorSiteCode()), $shouldFlush);

        if ($this->crossRefValidated){
            $this->action = '';
        }

        $this->em->persist($this);

        if ($shouldFlush){
            $this->em->flush();

        }

        return $this->crossRefValidated;

    }
}

