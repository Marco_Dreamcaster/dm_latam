<?php

namespace Commons\ORA;

Use Commons\Base\BaseVendorSiteContact;
use Doctrine\ORM\Query;
Use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * @Entity @Table(name="O_STG_VENDOR_SITE_CONTACT", indexes={@Index(name="search_vendorSiteContact", columns={"VENDOR_NUMBER","VENDOR_SITE_CODE"})}) @HasLifecycleCallbacks
 *
 */
class VendorSiteContact extends BaseVendorSiteContact
{
    protected $em;

    public function __construct()
    {
    }

    /** @PreUpdate */
    public function validatePreUpdate(PreUpdateEventArgs $event){
        $this->validate($event->getEntityManager());
    }

    /** @PrePersist */
    public function validatePrePersist(LifecycleEventArgs  $event){
        $this->validate($event->getEntityManager());
    }

    public function validate($em, $className = 'Commons\\ORA\\VendorSiteContact'){
        // the Base class uses IDL to country-based validations
        //
        parent::validate($em, $this);

        return $this->locallyValidated;
    }

    public function validateCrossRef($em)
    {
        $this->crossRefValidated = $this->locallyValidated;

        $this->em = $em;

        if ($this->crossRefValidated){
            $this->action = '';
        }

        $this->em->persist($this);
        $this->em->flush();

        return $this->crossRefValidated;
    }
}


