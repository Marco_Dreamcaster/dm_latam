<?php
namespace Commons\RAW;

Use Commons\Base\BaseCustomerAddress;

/**
 * @Entity @Table(name="O_PRE_CUSTOMER_ADDRESS")
 */
class PreCustomerAddress extends BaseCustomerAddress
{
    public function __construct()
    {
    }

}