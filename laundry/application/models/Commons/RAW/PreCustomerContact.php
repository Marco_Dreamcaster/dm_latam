<?php
namespace Commons\RAW;

Use Commons\Base\BaseCustomerContact;

/**
 * @Entity @Table(name="O_PRE_CUSTOMER_CONTACT")
 */
class PreCustomerContact extends BaseCustomerContact
{
    public function __construct()
    {
    }

}