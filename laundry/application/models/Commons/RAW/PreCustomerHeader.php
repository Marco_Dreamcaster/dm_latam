<?php
namespace Commons\RAW;

Use Commons\Base\BaseCustomerHeader;

/**
 * @Entity @Table(name="O_PRE_CUSTOMER_HEADER")
 */
class PreCustomerHeader extends BaseCustomerHeader
{
    public function __construct()
    {
    }

}
