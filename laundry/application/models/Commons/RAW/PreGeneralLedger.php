<?php
namespace Commons\RAW;

Use Commons\Base\BaseGeneralLedger;

/**
 * @Entity @Table(name="O_PRE_GENERAL_LEDGER")
 */
class PreGeneralLedger extends BaseGeneralLedger
{
    public function __construct()
    {
    }

}
