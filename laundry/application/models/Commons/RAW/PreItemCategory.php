<?php
namespace Commons\RAW;

Use Commons\Base\BaseItemCategory;

/**
 * @Entity @Table(name="O_PRE_ITEM_CATEGORY")
 */
class PreItemCategory extends BaseItemCategory
{
    public function __construct()
    {
    }

}
