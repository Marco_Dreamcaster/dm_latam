<?php
namespace Commons\RAW;

Use Commons\Base\BaseItemCrossRef;

/**
 * @Entity @Table(name="O_PRE_ITEM_CROSSREF")
 */
class PreItemCrossRef extends BaseItemCrossRef
{
    public function __construct()
    {
    }

}
