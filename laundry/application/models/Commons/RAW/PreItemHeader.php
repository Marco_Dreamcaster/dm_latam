<?php
namespace Commons\RAW;

Use Commons\Base\BaseItemHeader;

/**
 * @Entity @Table(name="O_PRE_ITEM_HEADER")
 */
class PreItemHeader extends BaseItemHeader
{
    public function __construct()
    {
    }

}
