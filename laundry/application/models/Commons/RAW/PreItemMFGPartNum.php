<?php
namespace Commons\RAW;

Use Commons\Base\BaseItemMFGPartNum;

/**
 * @Entity @Table(name="O_PRE_ITEM_MFG_PART_NUM")
 */
class PreItemMFGPartNum extends BaseItemMFGPartNum
{
    public function __construct()
    {
    }

}
