<?php
namespace Commons\RAW;

Use Commons\Base\BaseItemTransDefSubInv;

/**
 * @Entity @Table(name="O_PRE_ITEM_TRANS_DEF_SUB_INV")
 */
class PreItemTransDefSubInv extends BaseItemTransDefSubInv
{
    public function __construct()
    {
    }

}
