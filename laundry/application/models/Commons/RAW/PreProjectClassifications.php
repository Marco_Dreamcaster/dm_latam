<?php
namespace Commons\RAW;

Use Commons\RAWBase;
Use Commons\Base\BaseProjectClassifications;

/**
 * @Entity @Table(name="O_PRE_PROJECT_CLASSIFICATIONS")
 */
class PreProjectClassifications extends BaseProjectClassifications
{
    public function __construct()
    {
    }

}
