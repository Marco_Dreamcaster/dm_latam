<?php
namespace Commons\RAW;

Use Commons\Base\BaseProjectHeader;

/**
 * @Entity @Table(name="O_PRE_PROJECT_HEADER")
 */
class PreProjectHeader extends BaseProjectHeader
{
    public function __construct()
    {
    }

}
