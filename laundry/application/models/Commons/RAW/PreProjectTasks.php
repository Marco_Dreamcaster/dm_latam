<?php
namespace Commons\RAW;

Use Commons\Base\BaseProjectTasks;

/**
 * @Entity @Table(name="O_PRE_PROJECT_TASKS")
 */
class PreProjectTasks extends BaseProjectTasks
{
    public function __construct()
    {
    }
}
