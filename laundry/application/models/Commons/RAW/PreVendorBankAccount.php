<?php
namespace Commons\RAW;

Use Commons\Base\BaseVendorBankAccount;

/**
 * @Entity @Table(name="O_PRE_VENDOR_BANK_ACCOUNT")
 */
class PreVendorBankAccount extends BaseVendorBankAccount
{
    public function __construct()
    {
    }

}
