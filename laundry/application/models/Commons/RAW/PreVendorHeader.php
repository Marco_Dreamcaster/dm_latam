<?php
namespace Commons\RAW;

Use Commons\Base\BaseVendorHeader;

/**
 * @Entity @Table(name="O_PRE_VENDOR_HEADER")
 */
class PreVendorHeader extends BaseVendorHeader
{
    public function __construct()
    {
    }

    /**
     * @Column(name="CODIGO", type="string", length=20, nullable=true)
     *
     */
    protected $codigo;

    /**
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param mixed $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    }