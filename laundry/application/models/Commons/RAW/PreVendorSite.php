<?php
namespace Commons\RAW;

Use Commons\Base\BaseVendorSite;

/**
 * @Entity @Table(name="O_PRE_VENDOR_SITE")
 */
class PreVendorSite extends BaseVendorSite
{
    public function __construct()
    {
    }

}
