<?php
namespace Commons\RAW;

Use Commons\Base\BaseVendorSiteContact;

/**
 * @Entity @Table(name="O_PRE_VENDOR_SITE_CONTACT")
 */
class PreVendorSiteContact extends BaseVendorSiteContact
{
    public function __construct()
    {
    }

}
