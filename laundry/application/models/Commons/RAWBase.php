<?php
/**
 * Created by PhpStorm.
 * User: Dreamcaster
 * Date: 3/16/2018
 * Time: 3:01 PM
 */

namespace Commons;

Use Commons\ORA\CustomerAddress;

class RAWBase
{
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /** @Column(type="boolean", name="RECOMMENDED", nullable=true) * */
    protected $recommended;

    /** @Column(type="integer", name="RANKING", nullable=true, options={"default":0}) * */
    protected $ranking;

    /** @Column(type="boolean", name="LOCAL_VALIDATED", nullable=true) * */
    protected $locallyValidated;

    /** @Column(type="boolean", name="CROSSREF_VALIDATED", nullable=true) * */
    protected $crossRefValidated;

    /** @Column(type="string", name="ACTION", nullable=true) * */
    protected $action;

    /** @Column(type="boolean", name="CLEANSED", nullable=true)* */
    protected $cleansed;

    /** @Column(type="boolean", name="DISMISSED", nullable=true)* */
    protected $dismissed;

    /** @Column(type="string", name="COUNTRY", length=2, nullable=false) * */
    protected $country;

    /** @Column(type="string", name="OBSERVATION", length=3000, nullable=true) * */
    protected $observations;

    /** @Column(type="datetime", name="LAST_UPDATED", nullable=false) */
    protected $lastUpdated;

    /** @Column(type="integer", name="MERGED_ID", nullable=true) */
    protected $mergedId = null;

    /** @Column(type="boolean", name="CHILD_LESS", nullable=true) */
    protected $childless = true;

    /** @Column(type="boolean", name="SAME_COUNTRY_PARENT", nullable=true) */
    protected $sameCountryParent = false;

    public function getId()
    {
        return $this->id;
    }


    // this is the main validation routine which brings the configuration data from IDL
    protected function validate($em, $class){
        date_default_timezone_set('America/Santiago');
        $this->lastUpdated = new \DateTime("now");
        $this->locallyValidated = true;
        $this->action='';

        $selfRfx = new \ReflectionClass($class);

        $qb = $em->createQueryBuilder();
        $qb->select('idl, og')
            ->from('Commons\IDL\IDLDescriptor', 'idl')
            ->leftJoin('idl.group', 'og')
            ->andWhere(
                $qb->expr()->eq('idl.shortName', '?1'),
                $qb->expr()->eq('og.country', '?2'))
            ->setParameter(1, $selfRfx->getShortName())
            ->setParameter(2, $this->getCountry());

        $idl = $qb->getQuery()->getSingleResult();
        assert($idl!=null);


        foreach($idl->getFields() as $field){
            $getter = $selfRfx->getMethod("get".trim(ucfirst($field->getShortName())));
            $value =  $getter->invoke($class);

            if ($field->getIsMandatory()){
                switch ($field->getORA_TYPE()) {
                    // a checkbox
                    case 'VARCHAR2(1)':
                        break;

                    // a date-picker
                    case 'DATE':
                        if (!is_a($value, 'DateTime')){
                            $this->locallyValidated = false;
                            $this->action = $field->getShortName() . ' is a mandatory date type';
                        }
                        break;

                    // a numerical value
                    case 'NUMBER':
                        if (!is_numeric($value)){
                            $this->locallyValidated = false;
                            $this->action = $field->getShortName() . ' is a mandatory numeric type';
                        }
                        break;
                    // default textual field
                    default:
                        if(($value==null)||(trim($value)=='')){
                            $this->locallyValidated = false;
                            $this->action = $field->getShortName() . ' is mandatory string type, it cannot be empty';
                            break;
                        }
                        break;
                }
           }

        }

    }

    protected function checkORA_GEO_CODE($em)
    {
        if ($this->locallyValidated == true) {
            $qb = $em->createQueryBuilder();

            $checkOnlyCountries = array("PA", "CO", "MX");

            if (in_array($this->getAddressCountry(), $checkOnlyCountries)) {
                switch ($this->getAddressCountry()) {
                    case 'PA':
                        $qb->select('geo')
                            ->from('Commons\\MAPS\\OraGeoCodes', 'geo')
                            ->andWhere(
                                $qb->expr()->eq('geo.countryCode', '?1'),
                                $qb->expr()->eq('geo.province', '?2'),
                                $qb->expr()->eq('geo.city', '?3')
                            )
                            ->setParameter(1, $this->getAddressCountry())
                            ->setParameter(2, $this->getProvince())
                            ->setParameter(3, $this->getCity());
                        break;
                    case 'CO':
                    case 'MX':
                        $qb->select('geo')
                            ->from('Commons\\MAPS\\OraGeoCodes', 'geo')
                            ->andWhere(
                                $qb->expr()->eq('geo.countryCode', '?1'),
                                $qb->expr()->eq('geo.state', '?2'),
                                $qb->expr()->eq('geo.city', '?3')
                            )
                            ->setParameter(1, $this->getAddressCountry())
                            ->setParameter(2, $this->getState())
                            ->setParameter(3, $this->getCity());
                        break;

                    default:
                        return;
                }

                // one last check before you go
                //
                $geo = $qb->getQuery()->getResult();

                if (sizeof($geo) != 1) {
                    $this->locallyValidated = false;
                    $this->action = '(' . $this->addressCountry . ', ' . $this->state . ', ' . $this->province . ', ' . $this->city . ') is invalid (Country, State, Province, City) combination, check ORA Geo Codes';

                }else{
                    switch ($this->getAddressCountry()) {
                        case 'PA':
                            $this->county = $geo[0]->getStateCode();
                            break;

                        default:
                            return;
                    }

                }
            }



        }
    }


    protected function checkForInvalidChildren($childClass, $childKeyColumn, $childKeyValue){
        $qb = $this->em->createQueryBuilder();
        $qb->select('child')
            ->from($childClass, 'child');

        for($i=0;$i<sizeof($childKeyColumn);$i++){
            $qb->where($qb->expr()->eq('child.'.$childKeyColumn[$i], '?'.$i));
        }

        for($i=0;$i<sizeof($childKeyValue);$i++){
            $qb->setParameter($i, $childKeyValue[$i]);
        }

        $qb->andWhere('child.dismissed=false');

        $allChildren = $qb->getQuery()->getResult();

        foreach ($allChildren as $childTask) {
            if (!$childTask->getLocallyValidated()) {
                $this->crossRefValidated = false;
            }
        }
    }

    protected function updateChildrenWithParentState($childClass, $childKeyColumn, $childKeyValue, $shouldFlush = true){
        $qb = $this->em->createQueryBuilder();
        $qb->select('child')
            ->from($childClass, 'child');

        for($i=0;$i<sizeof($childKeyColumn);$i++){
            $qb->where($qb->expr()->eq('child.'.$childKeyColumn[$i], '?'.$i));
        }

        for($i=0;$i<sizeof($childKeyValue);$i++){
            $qb->setParameter($i, $childKeyValue[$i]);
        }

        $allChildren = $qb->getQuery()->getResult();

        foreach ($allChildren as $child) {
            $child->setCrossRefValidated($this->crossRefValidated, $shouldFlush);
            $this->em->persist($child);
        }

        if ($shouldFlush){
            $this->em->flush();
        }

    }

    public function getSameCountryParent()
    {
        return $this->sameCountryParent;
    }
    public function setSameCountryParent($sameCountryParent)
    {
        $this->sameCountryParent = $sameCountryParent;
    }

    public function getChildless()
    {
        return $this->childless;
    }
    public function setChildless($childless)
    {
        $this->childless = $childless;
    }

    public function getMergedId()
    {
        return $this->mergedId;
    }
    public function setMergedId($mergedId)
    {
        $this->mergedId = $mergedId;
    }

    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;
    }

    public function getRecommended()
    {
        return $this->recommended;
    }

    public function setRecommended($recommended)
    {
        return $this->recommended = $recommended;
    }

    public function getLocallyValidated()
    {
        return $this->locallyValidated;
    }

    public function setLocallyValidated($locallyValidated)
    {
        return $this->locallyValidated = $locallyValidated;
    }

    public function getCrossRefValidated()
    {
        return $this->crossRefValidated;
    }

    public function setCrossRefValidated($crossRefValidated)
    {
        return $this->crossRefValidated = $crossRefValidated;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function setAction($action)
    {
        return $this->action = $action;
    }

    public function getCleansed()
    {
        return $this->cleansed;
    }

    public function setCleansed($cleansed)
    {
        return $this->cleansed = $cleansed;
    }


    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country)
    {
        return $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getObservations()
    {
        return $this->observations;
    }

    /**
     * @param mixed $observations
     */
    public function setObservations($observations)
    {
        $this->observations = $observations;
    }

    /**
     * @return mixed
     */
    public function getDismissed()
    {
        return $this->dismissed;
    }

    /**
     * @param mixed $dismissed
     */
    public function setDismissed($dismissed)
    {
        $this->dismissed = $dismissed;
    }


    /**
     * @return mixed
     */
    public function getRanking()
    {
        return $this->ranking;
    }

    /**
     * @param mixed $dismissed
     */
    public function setRanking($ranking)
    {
        $this->ranking = $ranking;
    }


}