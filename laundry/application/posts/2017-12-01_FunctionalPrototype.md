---
layout: post
title:  "DM LATAM Protoype"
date:   2017-12-01 18:00:00 +0100
categories: dm_latam
author: dreamcaster
location: Porto Alegre, Brazil
description: it's alive
---

# Prototype Status

When filtering, there is no effect on flat file generation.   Even if a row is not shown due to a filter, it still gets outputted, when validated.

The prototype is coming along nicely, but we're still missing some important features:

- it does not follow file pattern on outuput flat file
- no reports are generated
- no metrics are collected
- no dataset level validations are implemented yet

## Excel Magic codes

Excel keeps trying to help you, and gets in the way, you can't just use the cursor like in a regular text editor.  It's a pain in the ass.

Here are some formulas which take care of conditional formatting on the sheets

=$C2=""  (deep red)
=AND($C2="D") (light yellow)
=AND($A2="R",$C2="C") (dark yellow)
=AND($A2="A",$C2="C") (green)
=AND($A2="",$C2="C") (bright red)

Excel is a great tool, but it's not aging gracefully.


## VBA Cheat sheets

[Loops](https://docs.microsoft.com/en-us/dotnet/visual-basic/language-reference/statements/do-loop-statement)
[Format function](https://msdn.microsoft.com/en-us/vba/language-reference-vba/articles/format-function-visual-basic-for-applications)
[Handle Runtime Errors in VBA](https://msdn.microsoft.com/en-us/vba/access-vba/articles/handle-run-time-errors-in-vba)
[Range Properties](https://msdn.microsoft.com/en-us/library/office/aa196464(v=office.11).aspx)
