---
layout: post
title:  "DM LATAM Sprint 3 review"
date:   2017-12-20 15:00:00 +0100
categories: dm_latam
author: dreamcaster
location: Porto Alegre, Brazil
description: review of features of Commons 0.3
---


# What's new on Commons 0.3?

Commons is the Oracle EBS VBA framework that:

1. provides configurable validation and styling rules at field level
2. automatically applies XL styles to data sheet (color coding, locking, hiding)
3. validates manual user input on each entryset
4. generates flat-file outputs only for valid records

The date of release for sprint 3 has been postponed to Fri 22nd Dec 2017.

Sprint 3 release includes a PANAMA entryset for Vendors using the new Full Load synchronization, and recommended flag



## Recommended for Migration flag

Instead of filtering the possible source records, a new "recommended for migration" flag has been added

## XLConfiguratorService

Component responsible for avoid manual styling configuration of data sheets

1. applies consistent formatting to all Data worksheets
2. adds VALIDATED, ACTION, RECOMMENDED, and CLEANSED columns with proper locks and list of values
3. define color and styling schemes in a central location for easier configuration

## DM Latam toolbar

So many planned buttons that a dedicated toolbar becomes necessary.

# Planned for Commons 0.4

## Substitution of built-in XL database synchronization

Why?  Default Excel synch is totally fine, but it's one-way, and it overwrites all work previously done by user.

We need to manage synchronization via our own VBA routines to:

1. provide feedback about user progress
2. incorporate new operational data from SIGLA

## Types of synchronization

### Full load

It's a one-time operation of loading records into the sheet, without rollback.

### Incremental load

It's a recurring operation that:

1. adds new records from SIGLA to entryset worksheet
2. provides feedback about user progress (progress data flows back to staging tables)
3. applies merging policies (what to do with records touched by USER)

##### User progress

Three flags are provided for each record, implemented as boolean columns on the staging table:
1. RECOMMENDED (by DEV)
2. CLEANSED (by USER)
3. VALIDATED (by tool)

An Incremental Load button is provided in DEV mode.   States are updated after the operation.


##### Simple merging strategy

If a record only exists on the staging table, then it is uploaded to the worksheet, marked as DIRTY and REJECTED

If a record only exists on the worksheet, no action is taken.  We're assuming the USER added it, and the tool will take care of making sure it follows specifications.

If a record exists both on the worksheet and the staging table...

... and it's DIRTY on the worksheet, then we just upload it, and overwrite any information.  It remains DIRTY and REJECTED.

... and it's CLEAN, then no action is taken.   We're confident that the USER is taking care of keeping all data updated.

#### Possible problems with the simple strategy

The simple strategy might not be enough for all cases.

Think of the case of Inventory, where the user types in a certain quantity for an item, and marks the record as CLEANSED.   Let's assume the quantity changes during operation.  Using the simple strategy, these changes will be ignored on the next incremental load.

We already have individual field cleansing descriptors, an alternative would be to specify a priority direction of merge on each field (STAGE or WORKSHEET).   

We could discuss more refined strategies in the future, the simple strategy is the goal for sprint 4.





# References

[Customized Ribbon Images](http://soltechs.net/customui/imageMso01.asp?gal=1&count=no)

[Custom UI Editor](http://openxmldeveloper.org/blog/b/openxmldeveloper/archive/2006/05/26/customuieditor.aspx)

[ADO Code examples](https://docs.microsoft.com/en-us/sql/ado/reference/ado-api/ado-code-examples-in-visual-basic)

[Command ADO examples](https://docs.microsoft.com/en-us/sql/ado/reference/ado-api/command-object-ado)
