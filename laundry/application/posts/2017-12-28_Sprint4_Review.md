---
layout: post
title:  "DM LATAM Sprint 4 review"
date:   2017-12-28 17:00:00 +0100
categories: dm_latam
author: dreamcaster
location: Porto Alegre, Brazil
description: review of features of Commons 0.4
---


# What's new on Commons 0.4?

Commons is the Oracle EBS VBA framework that:

1. synchronizes data to and from staging tables on source database
2. automatically applies Excel styles to data sheets (color coding, locking, hiding)
3. allows configurable validation behaviors by country, entity, and field level
4. validates manual user inputs, displaying advice for errors (ACTION)
5. generates Transact-SQL DDL scripts based on flat file specs
4. generates flat-file outputs only for valid records


# First Customers Flat Files

## CustomerHeader

ORIG_SYSTEM_PARTY_REF
PARTY_NUMBER



# Recipe for creating a new template from scratch

1. copy Version sheet
2. copy Config Commons, and adjust parameters
3. create Config for the VO (usually start with header) (make sure naming conventions are followed)
4. Copy all modules, forms, and classes from the prototype into the VBA Project
5. fix VBA project References (Microsoft Scripting Runtime, Microsoft Forms 2.0 Object Library, MS VBA Extensibility 5.3, MS ActiveX Data Objects 6.1)
6. add the CUSTOM UI XML for customized Ribbon
7. generate DDL, and run the scripts to create staging tables
8. create the script to load records on stage
9.



# References

[Increase performance of VBA](http://www.cpearson.com/excel/ArraysAndRanges.aspx)

[Customized Ribbon Images](http://soltechs.net/customui/imageMso01.asp?gal=1&count=no)

[Custom UI Editor](http://openxmldeveloper.org/blog/b/openxmldeveloper/archive/2006/05/26/customuieditor.aspx)

[ADO Code examples](https://docs.microsoft.com/en-us/sql/ado/reference/ado-api/ado-code-examples-in-visual-basic)

[Command ADO examples](https://docs.microsoft.com/en-us/sql/ado/reference/ado-api/command-object-ado)
