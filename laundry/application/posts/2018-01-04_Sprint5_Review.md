---
layout: post
title:  "DM LATAM Sprint 5 review"
date:   2018-01-04 17:00:00 +0100
categories: dm_latam
author: dreamcaster
location: Porto Alegre, Brazil
description: review of features of Commons 0.5
---


# What's new on Commons 0.5?

Commons is the Oracle EBS and VBA Excel data migration framework that:

1. synchronizes data to and from staging tables on source database
2. automatically applies Excel styles to data sheets (color coding, locking, hiding)
3. allows configurable validation behaviors by country, entity, and field level
4. validates manual user inputs, displaying advice for errors (ACTION)
5. generates Transact-SQL DDL scripts based on flat file specs
4. generates flat-file outputs only for valid records


# Types of FK Check

## local

## remote


# Incremental Load

# Topics with Sha

  1. working on the Inventory entryset, on release Commons 0.5 by Wed of next week
    a. also includes first version of Incremental loading
    b. I prioritized work on the Inventory, but we had some progress there too

  2. fixing minor issues on last Customer test, improving quality of queries (missing email, and phone numbers)
    a. first custom validation -> conditional validation:
      IF such and such conditions are met THEN apply this validation

      e.g.
      When telephone type is PHONE or FAX,  Telephone can't be NULL

      a good chance to check how Commons deals with evolving requirements

    b. on release Commons 0.4.2 by tomorrow

  3. tomorrow I'll have a busy day at TKE:
    a. quick meeting with the engineering team
    b. Ignacio is taking care of my VPN access, he also gave me the coordinates of the servers

  4. Customer profiles (DEFAULT)
  5.


# Topics to be discussed with Ignacio when he´s back

1. review the progress on the repo: new entrysets for Vendors, Customers, and now Inventory

2. improve the quality of Vendors and Customers queries (avoid duplicating names, add email, and other info)
  a. Where do we get reliable information about Vendors Bank Accounts for PANAMA?
  b. what are the defined Customer profiles?  Ask Sha today.
  c. what is the procedure to add Addresses to existing Vendors?   SIGLA only provides one (when it does) (same question for Contacts)
  d. Use consistent naming convention to assign Contact Reference and Telephone Reference.
  e. conditional validations (not NULL for email, and telephone)
  f. recommend flag queries (Ignacio)

3. show work starting on the Inventory entryset  (Andreas, ask Sha today)
  a. What does Item MFG PartNum represent?  How do I get it?
  b. What does Item TXN Def Subinventory = ALMACEN represent?  How do I get it?  
  c. WHat does Item TXN Def Loc = UBICAPRODALM represent?  How do I get it?
  d. review loading queries for Inventory

4. review the Incremental Load feature (implementation is under way)

5. draw a deployment architecture diagram, now including Citrix XenDesktop (@see Jeremias email)
6. review Office language package issue on VM  (José Martinez)

7. review Diego´s role and participation (@see Jeremias email)

8. get a snapshot of current PANAMA db
   a. test access to VPN "servidor 47"

   192.168.16.31\schl1qa01
   sa
   tk3.marte
   TS_TK..

   b. access VPN (with Jeremias and Rodolfo)
   c. Ignacio will verify the due for new development "Alta de Productos"

192.168.16.31\schl1qa01





# References

[Increase performance of VBA](http://www.cpearson.com/excel/ArraysAndRanges.aspx)

[Customized Ribbon Images](http://soltechs.net/customui/imageMso01.asp?gal=1&count=no)

[Custom UI Editor](http://openxmldeveloper.org/blog/b/openxmldeveloper/archive/2006/05/26/customuieditor.aspx)

[ADO Code examples](https://docs.microsoft.com/en-us/sql/ado/reference/ado-api/ado-code-examples-in-visual-basic)

[Command ADO examples](https://docs.microsoft.com/en-us/sql/ado/reference/ado-api/command-object-ado)

[Excel VBA Find - A Complete Guide - Excel Macro Mastery](https://excelmacromastery.com/excel-vba-find/#Using_After_with_Find)

[Creating and Implementing an Interface](https://msdn.microsoft.com/en-us/library/aa262286(v=vs.60).aspx)

[excel - How to determine the last Row used in VBA including blank spaces in between - Stack Overflow](https://stackoverflow.com/questions/13686801/how-to-determine-the-last-row-used-in-vba-including-blank-spaces-in-between)

[Built-in Excel constant for maximum number of rows, for columns?](https://www.excelforum.com/excel-programming-vba-macros/498994-built-in-excel-constant-for-maximum-number-of-rows-for-columns.html)

[Range.Find Method (Excel)](https://msdn.microsoft.com/en-us/vba/excel-vba/articles/range-find-method-excel)

[Do...Loop Statement (Visual Basic) | Microsoft Docs](https://docs.microsoft.com/en-us/dotnet/visual-basic/language-reference/statements/do-loop-statement)

[GitHub - twbs/bootstrap: The most popular HTML, CSS, and JavaScript framework for developing responsive, mobile first projects on the web.](https://github.com/twbs/bootstrap)
[Bootstrap · The most popular HTML, CSS, and JS library in the world.](https://getbootstrap.com/)
[Cover Template for Bootstrap](https://getbootstrap.com/docs/4.0/examples/cover/#)
[Bootstrap Expo](https://expo.getbootstrap.com/)
[SRI Hash Generator](https://www.srihash.org/)
[bootstrap 4 build tools -grunt - Google Search](https://www.google.com.br/search?ei=gUJVWoitEsShwgSGmoHYBg&q=bootstrap+4+build+tools+-grunt&oq=bootstrap+4+build+tools+-grunt&gs_l=psy-ab.3...11366.13204.0.13436.7.7.0.0.0.0.218.799.0j4j1.5.0....0...1c.1.64.psy-ab..2.1.218...35i39k1.0.pQ10kY9Zh4U)
[Theming Bootstrap · Bootstrap](https://getbootstrap.com/docs/4.0/getting-started/theming/)
