---
layout: post
title:  "DM LATAM Master Data Cleansing Flow'"
date:   2018-01-31 12:00:00 -0300
categories: dm_latam
author: dreamcaster
location: Porto Alegre, Brazil
description: review of Master Data Cleansing Flow
---


# Introductions

My name is Marco, I´m a consultant working for Zallpy.  I started on this project on Nov 5th 2017.

You can reach me on Skype or through my email: marco.silva@thyssenkrupp-la.com

My mission is to facilitate the data migration tasks for the Progress LA project.

# E.T.L.

Any data migration task can be generally described as an ETL job.  ( https://en.wikipedia.org/wiki/Extract,_transform,_load )

ETL stands for Extract, Transform and Load.

In other words:

1. extract from data sources (e.g. SIGLA, other sources)
2. transform (apply rules, validation, and categorization)
3. load data into target system (e.g. Oracle EBS, VIEW)

# ETL - Load

Data is loaded into Oracle in the form of flat files.

Flat files are text files where data is grouped and serialized into rows.  

Each row has several columns which are separated by special character (in our case it´s the pipe '|')  (e.g. dm_latam\artifacts\01.PAN\Customers\datasets)

The Oracle team provides specifications for flat files:

1. file name
2. column names
3. validation rules for each field

Sha provides specifications in the form of IDL (Interface Definition Layouts) which are Excel spreadsheets with detailed information about each column.   (briefly show an example at dm_latam\artifacts\01.PAN\Customers\support)

The motives and reasons behind rules associated to each column have been extensively discussed in the previous months.

It´s important that Key Users are familiarized with these specifications.

Any doubts or questions regarding IDLs, or Oracle EBS concepts will be redirected to Sha and his team.

These flat files are clearly not suited to be manipulated by humans.


# ETL - Extract

For this project, our main source of data comes from the SIGLA database.  (briefly show SQL Server Management Studio)

If you inspect the tables on SIGLA, you´ll see that some of them are prefixed by "STG_"

These are the tables which match the layout definitions provided by Sha, except for a few additional columns which we´ll discuss later.

Ignacio and I wrote scripts that fill in those tables with the best information available on SIGLA.

We also cannot expect users to manipulate SQL tables.









# XenDesktop

There is a shared environment available through:

http://10.148.1.23/Citrix/SiglaOneWeb/

You may use your 8ID credentials to get into the "Windows Server" desktop.   Use shift+F2 to switch between full screen and windowed modes.

Local drives are mapped to the XenDesktop session.  That makes it easy to download or upload files to the environment (show mapped drives on "Equipo")
