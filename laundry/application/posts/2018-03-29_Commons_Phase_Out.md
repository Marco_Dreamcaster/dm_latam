# What is Laundry?

Laundry is the name of our new cleansing tool, and the evolution of Commons.

Excel was used in the first data cleansing iteration before the CRP.  It's clear now that the Excel files approach will not scale up well.  Commons is being phased out.

Laundry is a simple web application that overcomes much of the problems faced with Excel.   It's based on the [PHP CodeIgniter framework](https://codeigniter.com/).

# Sprints

The last sprint #8 for Commons dated to Feb 14th 2018.

As of Apr 2nd 2018, we will resume publishing sprint reports, the counter will be reset to Sprint 1.

# IDL

Interface Definition Library (IDL) are files that describe the layout of flat files, they're usually provided by Sha as the input to his PL/SQL loading scripts.

There is a large number of fields and entities to be managed.   This is potentially multiplied by the number of target countries.
  
Though very similar to each other, each target must be treated as a separate migration.


# Templates

Laundry uses IDLs to generate HTML snippets, PHP classes, and other plumbing code.

The goal is to reduce the amount of typing needed to implement HTML forms, SQL tables, and other error-prone artifacts.

With less plumbing to take care, we can focus on providing more robust validations.


## Template Pack

For each IDL, Laundry generates:

1. Flat File output template
2. PHP Base class
3. PHP ORA class
4. PHP RAW class
5. Default Controller class
6. Base HTML form
7. Custom HTML form

### 1 - Flat File Output template

IDLs are originally used for flat file specifications:
* order of fields
* type of field
* rules (mandatory, LOV)

IDLs also contain some observations, tips, sometimes sample data for each field.

It's easy to assume that IDLs correspond 1:1 to SQL tables, but that's wrong.
  
For instance, there's the case of fields needed for migration, but are not meant to be outputted.  The ALIAS field takes care of duplicated records on Customers and Vendors SIGLA tables.  You will not find ALIAS in Sha's specs.

And there are special cases that sometimes multiply the number of rows on the SQL table.

##### 1.1 - Special Case Multiple Outputs

There are two major multiple output cases:
1. CustomerAddresses can be BOTH, which means that the same address becomes becomes two lines on the output: SHIP_TO, and BILL_TO
2. Inventory Items can be multiplied by their target organizations.

##### 1.2 - Special Case Project Tasks

The information about tasks is modelled in a different way in SIGLA.  It's necessary to use a fixed template for parent and child tasks.   So each row on the database corresponds to 6 different rows on the output.

## 2 - PHP Base class

The PHP base class uses [Doctrine 2](http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/annotations-reference.html) annotations corresponding to each field on the IDL.

The use of a ORM library is justified for some reasons:
1. SQL scripts for IDL tables are generated according to the annotation metadata
2. ORM provides catching Lifecycle events (such as PreUpdate, or PrePersist)
3. Queries can be written in procedural form, which is ideal for a web-based app

Using IDL metadata to generate scripts goes according to the principle of reducing plumbing.

Catching lifecycle events is essential for keeping the consistency of the dataset.  Suppose a user cleanses a CustomerContact, the status of an associated Project may change from DIRTY to CLEANSED.

### 3 - PHP extension classes

These classes extend Base, and they are used by Doctrine to generate SQL scripts.

### 3.1 - PHP ORA class

PHP ORA class extends Base, and declare the staging table name.  

```php
```   

### 3.2 - PHP RAW class

PHP RAW classes extends Base, and declare the pre-staging table name. 

```php
/**
 * @Entity @Table(name="O_PRE_CUSTOMER_CONTACT")
 */
class PreCustomerContact extends BaseCustomerContact
{
    public function __construct()
    {
    }

}
```  

### 3.3 - Pre-stage and Stage

Pre-stage tables can be modified at any time, they contain the pool of data available from SIGLA.

Stage tables contain the records selected from the pool to be migrated.
   
Stage tables represent the work done by Key Users.


### 3.4 - COUNTRY discriminator

Each IDL could have its own table, but that would be a nightmare in terms of maintenance. 

Since the field values going to each output flat file are easily controlled (@see item 1), then similar IDLs will be kept on the same big table.
  
Each table will have its discriminator column named COUNTRY. 


## 4 - Default Controller Class




