# KPI Sampling

The auditing log table is used to keep track of key performance indexes.

## Details

Three major parts involved:

1) a scheduled procedure collects KPI in regular intervals (once a day)
2) a controller collects the latest sampled KPI and produces a JSON resource
3) a javascript embedded into the web page consumes that KPI JSON, and displays a graph to the user

## Scheduled Procedures on SQL Server 2008

Run in the command line with SQLCMD:

```
D:\> sqlcmd -U sa -P tke -S DESKTOP-4QMKTRK\SQLEXPRESS -d DM_LATAM -Q "EXECUTE dbo.runSampleKPI"
```

To be scheduled to run once a day on server .47




