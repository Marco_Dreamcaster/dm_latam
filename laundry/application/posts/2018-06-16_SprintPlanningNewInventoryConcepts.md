# Sprint Planning June 26th - New Inventory Concepts

## Inventory Cleansing Goals

1. unify records from all country databases into a single, coherent dataset
2. generate valid flat files
2. optimize workload to operators
4. provide verifiable and reliable feedback to management


### 1.Item State Chart

Um item pode estar em quatro estados:

1. Dirty (sujo)
2. Cleansed (limpo)
3. Merged (agregado)
3. Dismissed (despedido)

<img src="/laundry/img/InventoryItemStates.png" width="700px"/>

## 2.User stories

### 2.1.Scripted routines load data from SIGLA <i class="fas text-success fa-check fa-fw"></i>


Atrav�s de script TODOS os items de um snapshot do SIGLA s�o trazidos como DIRTY (sujos) para a tabela de pre-stage.

Cada item possue um ranking baseado em crit�rios de migra��o.

O crit�rio com maior peso � ter quantidade em estoque, no momento que a rotina de carga � executada.

Outros crit�rios experimentais est�o sendo testados.

Items com ranking zero n�o s�o recomendados.

Rankings superiores a zero s�o considerados "recomendados".

Quanto mais crit�rios um item atende, maior seu ranking.



### 2.2.User moves a DIRTY item to the CLEANSED item table <i class="fas text-success fa-check fa-fw"></i>

A partir da tela de itens sujos, o usu�rio move um item para a tabela de limpos.

O novo registro come�a no estado de CLEANSED (limpo)

A tela de edi��o de items CLEANSED permite:

1. alterar os campos do cabe�alho (ItemHeader) <i class="fas text-success fa-check fa-sm fa-fw"></i>
2. alterar e adicionar novos fabricantes (ItemMfgPartNum) <i class="fas text-success fa-check fa-sm fa-fw"></i>
3. adicionar novas refer�ncias cruzadas (ItemCrossRef) (vide 2.5 abaixo) <i class="fas text-danger fa-exclamation-triangle fa-sm fa-fw"></i>
4. definir SubInvent�rios padr�o (ItemTransDefSubInv) <i class="fas text-warning fa-exclamation-triangle fa-sm fa-fw"></i>
5. definir locators por cada SubInvent�rio (ItemTransDefSuInvLoc) <i class="fas text-warning fa-exclamation-triangle fa-sm fa-fw"></i>


### 2.3.Scripted routine outputs flat files <i class="fas text-success fa-check fa-fw"></i>

O usu�rio pode requisitar um download dos arquivos de sa�da (flat files).

Apenas items CLEANSED sem erros de valida��o v�o para os flat files.


### 2.4.User associates DIRTY to CLEANSED <i class="fas text-danger fa-exclamation-triangle fa-fw"></i>

A partir da tela de edi��o de item sujo, um usu�rio associa um item DIRTY com um item CLEANSED.
 
O novo registro � transferido para a tabela de limpos, mas neste caso seu estado � MERGED (agregado/fusionado).


#### MERGED Parity rules

Um item MERGED tem APENAS UM (1:1) item CLEANSED principal.

A p�gina de edi��o do item MERGED mostra a refer�ncia cruzada, permitindo navega��o de volta ao seu item CLEANSED principal.

Um item CLEANSED pode ter v�rios (1:n) items MERGED associados.

A p�gina de edi��o do item CLEANSED principal mostra todas suas refer�ncias cruzadas, permitindo navega��o a todos os itens associados.



##### MERGED items restrictions 

Por exemplo, um item MERGED n�o entra no arquivo de cabe�alho (ItemHeader), tamb�m n�o gera entradas para o arquivo de fabricantes (ItemMfgPartNum).  

Os valores do cabe�alho e fabricante do item MERGED mostrados na tela de edi��o, ser�o sempre id�nticos aos de seu CLEANSED principal.   Items MERGED n�o podem ter campos alterados diretamente na sua p�gina de edi��o.

Por�m, um item MERGED continua gerando linhas no arquivo flatfile do seu pa�s:
 
1. refer�ncias cruzadas (ItemCrossRef)
2. sub-Invent�rio padr�o (ItemTransDefSubInv)
3. locator (ItemTransDefSuInvLoc) daquele pa�s.

<img src="/laundry/img/MergedItemsRestrictions.png" width="700px"/>



### 2.5.User associates CLEANSED to CLEANSED <i class="fas text-danger fa-exclamation-triangle fa-fw"></i>

A partir da tela de edi��o de item CLEANSED, um usu�rio faz uma busca para associar com outro item CLEANSED.

Apenas itens CLEANSED SEM outras refer�ncias cruzadas aparecem na busca.
  
Itens que j� tenham outros itens MERGED dependentes n�o s�o eleg�veis para serem associados.

O usu�rio seleciona um item de destino.

O item de destino � rebaixado para o estado MERGED, passando a valer as restri��es j� descritas.


### 2.6.User dismisses a CLEANSED item <i class="fas text-success fa-check fa-fw"></i>

O usu�rio despede (dismiss) um item CLEANSED, que passa para o estado DISMISSED.

Items DISMISSED contam para o alvo.

Items DISMISSED s�o ignorados para o arquivo de sa�da.
 
Items DISMISSED n�o podem ser associados com outros items.

Apenas itens CLEANSED sem outras refer�ncias cruzadas s�o eleg�veis para serem despedidos (DISMISSED).

H� v�rios motivos para um item ser despedido.
  
Por exemplo, o item foi identificado como recomendado pelo script de carga, faz parte do alvo, mas � obviamente marcado como sujo a partir do SIGLA (e.g. 'NO USAR'). 


### 2.7.User recalls a DISMISSED item <i class="fas text-success fa-check fa-fw"></i>

O usu�rio chama de volta um item DISMISSED, retornando ao estado CLEANSED.

Recall � a opera��o inversa do dismiss, serve para corrigir enganos.


### 2.8.User discards a MERGED item <i class="fas text-danger fa-exclamation-triangle fa-fw"></i>

O usu�rio escolhe um item MERGED, clica em descartar.  

Ap�s confirma��o, o item � removido da tabela de limpos.

A associa��o com seu item CLEANSED principal tamb�m � removida.

Todas altera��es de SubInvent�rio (ItemTransDefSubInv) e Location (ItemTransDefSubLoc) s�o perdidas.

O item volta a aparecer na lista de DIRTY.

Na pr�tica, o item perde sua mem�ria e pode ser tratado novamente como se nada tivesse acontecido.



### 2.9.User discards a CLEANSED item <i class="fas text-success fa-check fa-fw"></i>

O usu�rio escolhe um item CLEANSED, clica em descartar.

Ap�s confirma��o, o item � removido da tabela de limpos.

Apenas itens CLEANSED sem outras refer�ncias cruzadas s�o eleg�veis para serem descartdos desta maneira.

No estado DIRTY, o item perde sua mem�ria e pode ser tratado novamente.


## 3.New Items <i class="fas text-warning fa-exclamation-triangle fa-fw"></i>

Nesta vers�o, ainda N�O HAVER� como incluir novos items pela ferramenta.

A maneira recomendada � que novos itens sejam cadastrados diretamente no SIGLA.
  
Quando ocorrer uma sincronia com o banco de produ��o do SIGLA, os novos �tems aparecer�o na lista de DIRTY.

J� existe uma regra que d� um peso +1 ao ranking de itens cujo ID SIGLA come�a com "L".

<br/>
<br/>
<br/>
<br/>
<br/>






