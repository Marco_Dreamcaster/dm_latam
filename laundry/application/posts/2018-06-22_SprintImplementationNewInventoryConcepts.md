# Inventory FlatFile Dataset Templates

<blockquote class="trello-card"><a href="https://trello.com/c/GDc7KsfK/123-generate-ff-inventory-co">Generate FF Inventory CO</a></blockquote><script src="https://p.trellocdn.com/embed.min.js"></script>

<br></br>

## <span class="flag-icon flag-icon-pa" style="margin-top:10px;"></span> PA Templates  

TKE, LPA, CPA are valid organizationCodes

### PA - ITEM_HEADER
<a class="btn btn-success" target="_blank" href="/laundry/IDL/Navigate/navigate/PA/Inventory/ItemHeader" data-toggle="tooltip" data-placement="top" title="specification for PA item header"><i class="fas fa-file-excel fa-sm fa-fw"></i></a> <a class="btn btn-primary" target="_blank" href="/laundry/IDL/Serializers/flatfile/PA/Inventory/ItemHeader" data-toggle="tooltip" data-placement="top" title="inspect file"><i class="fas fa-file fa-sm fa-fw"></i></a> <a class="btn btn-primary" target="_blank" href="/laundry/Debug/PA/Inventory/ItemHeader" data-toggle="tooltip" data-placement="top" title="debug"><i class="fas fa-bug fa-sm fa-fw"></i></a>

(only for PA parent item)

* TKE 

(both for PA parent for non-PA-parent-child)

* LPA 
* CPA 

### PA - ITEM_CATEGORY
(only for PA parent)

* null/ (SCCFamily).0 / TKE PO Item Category 

### PA - ITEM_CROSSREF (SIGLA id from PA)
(TKE is the only orgCode used)

* 1 row for parent  
* 1 row per PA child

### PA - ITEM_MFG_PART_NUM

* n rows per parent (1 per manufacturer)
* 0 rows for each child

### PA - ITEM_TRANS_DEF_SUB_INV

* CPA / DEPCAL
* CPA / DEP01

### PA - ITEM_TRANS_DEF_LOCATOR

* CPA / DEP01 / LOCATOR 

<br></br>

## <span class="flag-icon flag-icon-co" style="margin-top:10px;"></span> CO Templates

TKE, LCO, BAR, BOG, BUC, CAL, CAR, MAN, MED, PER are valid organizationCodes

### CO - ITEM_HEADER
<a class="btn btn-success" target="_blank" href="/laundry/IDL/Navigate/navigate/CO/Inventory/ItemHeader" data-toggle="tooltip" data-placement="top" title="specification for CO item header"><i class="fas fa-file-excel fa-sm fa-fw"></i></a> <a class="btn btn-primary" target="_blank" href="/laundry/IDL/Serializers/flatfile/CO/Inventory/ItemHeader" data-toggle="tooltip" data-placement="top" title="inspect file"><i class="fas fa-file fa-sm fa-fw"></i></a> <a class="btn btn-primary" target="_blank" href="/laundry/Debug/CO/Inventory/ItemHeader" data-toggle="tooltip" data-placement="top" title="debug"><i class="fas fa-bug fa-sm fa-fw"></i></a>

(only CO parent)

* TKE 

(both CO parent and non-CO-parent-child)

* LCO 
* BAR 
* BOG 
* BUC 
* CAL 
* CAR 
* MAN 
* MED 
* PER 

### CO - ITEM_CATEGORY

(only CO parent)

* null/ (SCCFamily).0 / TKE PO Item Category 

(both CO parent and non-CO-parent-child)

* BAR / COMERCIAL - COD 204 / FISCAL CLASSIFICATION
* BOG / COMERCIAL - COD 204 / FISCAL CLASSIFICATION
* BUC / COMERCIAL - COD 204 / FISCAL CLASSIFICATION
* CAL / COMERCIAL - COD 204 / FISCAL CLASSIFICATION
* CAR / COMERCIAL - COD 204 / FISCAL CLASSIFICATION
* MAN / COMERCIAL - COD 204 / FISCAL CLASSIFICATION
* MED / COMERCIAL - COD 204 / FISCAL CLASSIFICATION
* PER / COMERCIAL - COD 204 / FISCAL CLASSIFICATION


### CO - ITEM_CROSSREF (SIGLA id from CO)

(TKE is the only orgCode used)

* 1 row for CO parent   
* 1 row per CO child

### CO - ITEM_MFG_PART_NUM
(only for CO parent)

* n rows per CO parent (1 per manufacturer)
* 0 rows for each child 

### CO - ITEM_TRANS_DEF_SUB_INV
(both CO parent and non-CO-parent-child)

* BAR / DEPCAL
* BAR / DEP01
* BOG / DEPCAL
* BOG / DEP01
* BUC / DEPCAL
* BUC / DEP01
* CAL / DEPCAL
* CAL / DEP01
* CAR / DEPCAL
* CAR / DEP01
* MAN / DEPCAL
* MAN / DEP01
* MED / DEPCAL
* MED / DEP01
* PER / DEPCAL
* PER / DEP01

### CO - ITEM_TRANS_DEF_LOCATOR

* BAR / DEP01 / LOCATOR from SIGLA
* BOG / DEP01 / LOCATOR from SIGLA
* BUC / DEP01 / LOCATOR from SIGLA
* CAL / DEP01 / LOCATOR from SIGLA
* CAR / DEP01 / LOCATOR from SIGLA
* MAN / DEP01 / LOCATOR from SIGLA
* MED / DEP01 / LOCATOR from SIGLA
* PER / DEP01 / LOCATOR from SIGLA

<br></br>

## <span class="flag-icon flag-icon-mx" style="margin-top:10px;"></span>  MX Templates

TKE, LMX, ??, ??, ?? are valid organizationCodes


<br></br>
<br></br>
<br></br>

