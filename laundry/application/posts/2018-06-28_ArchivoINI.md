#Archivo DB.ini

Archivo responsable de almacenar los datos de acceso (local), o incluso los datos de acceso a los datos de producci�n.
</br></br>

###Configuraci�n

Para que el sistema funcione correctamente es necesario que el archivo DB.ini se encuentra en la carpeta C:\xampp.
y tenga en su estructura los siguientes datos:

</br>
[BANCO]</br>
user = nombre del usuario del banco de datos</br>
pass = contrasen� del banco de datos</br>
db = nombre del banco de datos</br>
srv = nombre del servidor de datos
    
###Direcci�n 

En caso de que alg�n par�metro no sea informado, el sistema entender� que no hay una configuraci�n v�lida para el acceso local y se conectar� en el banco de datos de producci�n.
