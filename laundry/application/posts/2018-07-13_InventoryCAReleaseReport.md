# Release 2018-07-13 F.A.Q.

<div class="alert-warning m-2 p-4">
<h6>Nova modelagem</h6>
<p>Estamos entrando numa fase mais complexa da migra��o.  A principal novidade � que estamos  nos aproximando do estado do SIGLA em produ��o.</p>
<p>Novos conceitos precisam ser apresentados e novos termos precisam ser definidos.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>00. Revis�o</h6>
<p>H� quatro �reas principais de master data: Projects, Customers, Vendors e Inventory.</p>
<p>Cada �rea � representada por um conjunto de arquivos de texto (flat files)</p>
<p>Nos �ltimos meses se definiram uma s�rie de crit�rios de recomenda��o.</p>
<p>Os resultados exatos de recomenda��o passar�o a variar conforme as atualiza��es com o SIGLA v�o acontecendo.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>01. Qual a frequ�ncia das atualiza��es com SIGLA?</h6>
<p>J� estamos trabalhando com o snapshot (imagem) do SIGLA do in�cio do m�s de julho.</p>
<p>As atualiza��es ainda s�o feitas de forma manual, com frequ�ncia m�nima de uma vez por m�s.</p>
<p>Dentro em breve, esse processo ser� automatizado.   O objetivo � chegar o mais pr�ximo poss�vel do SIGLA na data do Go-Live de cada pa�s.</p>
</div>


<div class="alert-info m-2 p-4">
<h6>02. O que significa pre-Stage (pre-STG) e Stage (STG)?</h6>
<p>Para cada tipo de master data h� dois conjuntos principais de tabelas na nova ferramenta: pre-stage e stage.<p>
<p>A estrutura de ambas � praticamente a mesma, segue o formato dos flat files</p>
<p>O <b>pre-stage (pre-STG)</b> est� conectado diretamente com o SIGLA e sofre atualiza��es conforme o andamento da produ��o.</p>
<p>O <b>stage (STG)</b> guarda os dados ap�s trabalho de limpeza.</p>
<p>Os dados no pre-stage est�o constantemente mudando, conforme a produ��o avan�a, o conte�do � vol�til.</p>
<p>Os operadores KU podem alterar campos apenas no stage.  Todas altera��es feitas pelos KUs permanecem mesmo quando o pre-stage � renovado</p>
<p>Os flat files s�o gerados a partir do stage.</p>
<p>A grosso modo, os dados no pre-STG s�o chamados de <b>"sujos" (dirty)</b>, s�o representados pela �cone de uma lata de lixo <i class="fas fa-trash-alt fa-fw"></i></p>
<p>Os dados no STG s�o chamados de <b>"limpos" (cleansed)</b>, s�o representados pela �cone de uma f�brica <i class="fas fa-industry fa-fw"></i></p>
</div>

<div class="alert-info m-2 p-4">
<h6>03. Quem transfere um registro do pre-STG <i class="fas fa-trash-alt fa-fw"></i> para o STG <i class="fas fa-industry fa-fw"></i>?</h6>
<p>� papel dos KUs transferir dados do pre-stage para o stage.</p>
<p>H� um sistema de recomenda��es, mas a decis�o final � sempre do KU.</p>
<p>Registros n�o recomendados podem ser migrados, ou registros recomendados podem ser dispensados (dismissed), conforme a decis�o do KU. <i class="fas fa-minus-circle fa-fw"></i></p>
</div>

<div class="alert-info m-2 p-4">
<h6>04. Todos os dados no STG v�o para Oracle?</h6>
<p><b class="text-danger">N�o.</b></p>
<p>Apenas dados que passam por todas as regras de valida��o do Oracle ser�o migrados.</p>
<p>Al�m disso, o registro pode ser dispensado (dismissed) da migra��o pelo KU.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>05. Como fa�o para acompanhar o andamento da migra��o?</h6>
<p>No combo da tela de entrada do sistema, basta selecionar o pa�s desejado para acessar uma tela de resumo.</p>
<a target="_blank" href="/laundry/img/InventoryStatistics.png"><img class="m-2" src="/laundry/img/InventoryStatistics.png" style="width:600px"/></a>
<p>No Invent�rio h� mais estat�sticas dispon�ves no bot�o do gr�fico <i class="fas fa-chart-line fa-fw"></i>.</p>
<a target="_blank" href="/laundry/img/InventoryStatistics01.png"><img class="m-2" src="/laundry/img/InventoryStatistics01.png" style="width:600px"/></a>
<p>S�o muitos termos, vamos revisar apenas os principais.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>06. O que signifcam os termos RFM, "staged", "staged and recommended", "dirty and recommended"?</h6>
<p><b>RFM</b> � a sigla em ingl�s para "recomendados para migra��o".  No caso do Invent�rio, os crit�rios para recomenda��o s�o listados na p�gina de estat�sticas detalhadas.</p>
<p>A recomenda��o vale para o estado atual do banco de dados, � poss�vel que o estado mude, conforme o SIGLA na produ��o avan�a.</p>
<p>Um registro � considerado <b>"staged"</b> quando foi movido pelo KU para o (STG) <i class="fas fa-industry fa-fw"></i>.</p>
<p><b>"Staged, and recommended" (cRFM)</b> s�o os registros que se encontram o (STG) <i class="fas fa-industry fa-fw"></i> e est�o recomendados no momento.</p>
<p><b>"Dirty, and recommended" (dRFM)</b> s�o os registros recomendados que ainda se encontram o (STG) <i class="fas fa-trash-alt fa-fw"></i>.</p>
<p><span class="text-danger">Registros (dRFM) ainda precisam ser tratados pelos KUs, representam trabalho por ser feito.</span></p>
<p>Um outra maneira de dizer a mesma coisa: um registro recomendado do SIGLA pode estar em dois estados: staged (cRFM) ou dirty (dRFM).</p>
<p>A soma dos (cRFM) com os (dRFM) tem que ser sempre igual ao total de recomendados (RFM).</p>
<p>(RFM = cRFM + dRFM)</p>
</div>

<div class="alert-info m-2 p-4">
<h6>07. Por qu� a tela de estat�sticas Invent�rio tem muito mais n�meros e termos?</h6>
<p>Porque a migra��o do invent�rio:</p>
<ul>
<li> foi a primeira a adotar o esquema de STG e pre-STG.</li>
<li> necessita de um sistema de recomenda��es mais complexo</li>
</ul>
</div>


<div class="alert-info m-2 p-4">
<h6>08. O que � o sistema de ranking de recomenda��o?</h6>
<p>Um dos objetivos principais da migra��o do invent�rio � a unifica��o dos cadastros.</p>
<p>Por exemplo, um mesmo item que aparece na CO j� pode ter sido limpo no PA.</p>
<p>Neste caso, h� um crit�rio de semelhan�a por c�digo interno que aumenta seu ranking de recomenda��o.</p>
<p>Quanto maior o ranking de um item, mais recomendado ele est�.</p>
<p>Al�m disso, h� uma nota de corte para o RFM.  Por exemplo, apenas items com ranking superior a +5 s�o recomendados</p>
<p>A nota de corte serve para n�o inflar demais o RFM com falsos positivos.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>09. E o ranking para outras �reas?</h6>
<p>Para Projects, Customers, Vendors h� apenas dois estados: recomendado ou n�o-recomendado.</p>
<p>Por enquanto n�o h� previs�o de ranking para outras �reas al�m do Invent�rio.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>10. O que s�o items "Staged, but Not Recommended" (cnRFM)?</h6>
<p>S�o registros que se encontram na tabela de STG, mas que n�o est�o (mais) recomendados.</p>
<p>Estamos come�ando a entrar no est�gio que a migra��o pr�ximo ao estado atual de produ��o do SIGLA.</p>
<p>� perfeitamente poss�vel que um registro deixe de estar recomendado durante o processo de migra��o.</p>
<p>Por exemplo, um item pode ter sido movido para a tabela de stage por ter seu estoque diferente de zero.   Por�m, na atualiza��o seguinte, seu estoque zerou, logo n�o est� mais recomendado.</p>
<a target="_blank" href="/laundry/img/InventoryStatistics01.png"><img class="m-2" src="/laundry/img/InventoryStatistics01.png" style="width:600px"/></a>
<p>No screenshot acima, h� 535 items (cnRFM) no STG.</p>
<p>S�o 535 items que em algum momento dos �ltimos meses tiveram algum estoque, mas que atualmente est�o zerados. 
</div>

<div class="alert-info m-2 p-4">
<h6>11. Devo descartar items "Staged, but Not Recommended" (cnRFM)?</h6>
<p>� poss�vel descart�-los, mas n�o necess�rio.</p>
<p>No caso do Invent�rio, os items staged podem servir de master para registros de outros pa�ses.</p>
<p>No caso das outras �reas, n�o � necess�rio jogar fora trabalho de limpeza j� realizado.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>12. O que s�o items master (mestre) e merged (fusionado)?</h6>
<p>Um dos principais objetivos da limpeza e migra��o � re-classificar e unificar o cadastro de materiais.</p>
<p>Um mesmo item que se encontra na CO pode j� ter sido limpo no PA.</p>
<p>Neste caso, ao inv�s de uma limpeza completa, basta associar o item da CO ao item do PA.</p>
<p>O item do PA passa a ser o <b>master (pai)</b>.  O item da CO passa a ser o <b>merged (filho, ou fusionado)</b></p>
<p>O item da CO herda todas as caracter�sticas principais do item master do PA.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>13. Como fa�o para associar um item a outro j� existente?</h6>
<p>Por exemplo, o item MXLADN22 aparece na lista de pre-STG do MX com ranking 8:</p>
<a target='_blank" href="/laundry/img/InventoryStatistics02.png"><img class="m-2" src="/laundry/img/InventoryStatistics02.png" style="width:600px"/></a>

<p>Na tela de detalhe do registro h� mais informa��es que foram automaticamente detectadas pelo sistema:</p>
<a target='_blank" href="/laundry/img/InventoryStatistics03.png"><img class="m-2" src="/laundry/img/InventoryStatistics03.png" style="width:600px"/></a>
<p>O bot�o verde do olho <i class="fas text-success fa-eye fa-fw"></i> serve para espiar o item pai recomendado</p>
<a target="_blank" href="/laundry/img/InventoryStatistics06.png"><img class="m-2" src="/laundry/img/InventoryStatistics06.png" style="width:600px"/></a>
<p>O bot�o azul leva para uma tela de confirma��o:</p>
<a target="_blank" href="/laundry/img/InventoryStatistics04.png"><img class="m-2" src="/laundry/img/InventoryStatistics04.png" style="width:600px"/></a>
<p>Neste caso � f�cil identificar que se trata do mesmo item com descri��o limpa "CONTACT AUX FRONT LADN22"</p>
<p>Ao se confirmar a associa��o, algumas coisas acontecem:
<ul>
    <li>PA PALADN22 / LA99ASE000061 se torna o <b>master</b></li>
    <li>MXLADN22 se torna <b>merged</b>, associado com LA99ASE000061</li>
    <li>todos os dados do cabe�alho PALADN22 s�o copiados para MXLADN22</li>
    <li>altera��es no cabe�alho PALADN22 s�o propagadas para MXLADN22</li>
    <li>a �nica exce��o s�o as observa��es, que continuam sendo espec�ficas ao item MXLADN22</li>
    <li>os manufacturer part number ser�o carregados apenas para o item master PALADN22</li>
    <li>pode-se navegar at� o master PALADN22 em todos bot�es com s�mbolo de corrente <i class="fas fa-link fa-sm fa-fw"></i></li>
</ul>
<a href="/laundry/img/InventoryStatistics05.png" target="_blank"><img class="m-2" src="/laundry/img/InventoryStatistics05.png" style="width:600px"/></a>
<p>A tela do item PALADN22 passa a conter um link para o item MXLADN22 associado.</p>
<a href="/laundry/img/InventoryStatistics07.png" target="_blank"><img class="m-2" src="/laundry/img/InventoryStatistics07.png" style="width:600px"/></a>
<p>Isso � tudo que precisa ser feito para associar items.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>13. � poss�vel associar items do mesmo pa�s?</h6>
<p>Sim.</p>
<p>Para isto existe a associa��o livre, dispon�vel em qualquer registro do pre-STG <i class="fas fa-trash-alt fa-fw"></i>.</p>
<a href="/laundry/img/InventoryStatistics08.png" target="_blank"><img class="m-2" src="/laundry/img/InventoryStatistics08.png" style="width:600px"/></a>
<p>A t�tulo de experi�ncia, vamos fazer uma associa��o errada com o item PALADN22.</p>
<a href="/laundry/img/InventoryStatistics09.png" target="_blank"><img class="m-2" src="/laundry/img/InventoryStatistics09.png" style="width:600px"/></a>
<p>Como no exemplo da quest�o anterior, o bot�o com link de corrente serve para confirmar a associa��o.</p>
<a href="/laundry/img/InventoryStatistics10.png" target="_blank"><img class="m-2" src="/laundry/img/InventoryStatistics10.png" style="width:600px"/></a>
<p>O item do PAP03-DIOD-001 aparece como merged. Todos seus dados do cabe�alho foram copiados do item master.</p>
<a href="/laundry/img/InventoryStatistics12.png" target="_blank"><img class="m-2" src="/laundry/img/InventoryStatistics12.png" style="width:600px"/></a>
<p>O item do PALADN22 agora com dois filhos.</p>
<a href="/laundry/img/InventoryStatistics13.png" target="_blank"><img class="m-2" src="/laundry/img/InventoryStatistics13.png" style="width:600px"/></a>
<p class="text-danger">Obviamente a associa��o est� errada.  Vamos corrigir o erro no pr�ximo ponto.</p>

</div>

<div class="alert-info m-2 p-4">
<h6>14. Temos um erro na associa��o, como proceder para corrigir?</h6>
<p>Para corrigir o erro, � necess�rio <b>descartar (discard)</b> o item merged.</p>
<a href="/laundry/img/InventoryStatistics14.png" target="_blank"><img class="m-2" src="/laundry/img/InventoryStatistics14.png" style="width:600px"/></a>
<p class="text-danger">Esta opera��o remove os dados do STG, por isso � preciso confirmar.</p>
<a href="/laundry/img/InventoryStatistics15.png" target="_blank"><img class="m-2" src="/laundry/img/InventoryStatistics15.png" style="width:600px"/></a>
<p>Ao se confirmar o descarte, o item master PALADN22 n�o lista mais o filho errado.</p>
<a href="/laundry/img/InventoryStatistics16.png" target="_blank"><img class="m-2" src="/laundry/img/InventoryStatistics16.png" style="width:600px"/></a>
<p>Al�m disso, o item PAP03-DIOD-001 volta � figurar na lista do pre-STG, pode ser tratado novamente pelo operador.</p>
<a href="/laundry/img/InventoryStatistics17.png" target="_blank"><img class="m-2" src="/laundry/img/InventoryStatistics17.png" style="width:600px"/></a>
</div>

<div class="alert-info m-2 p-4">
<h6>15. O descarte (discard) <i class="fas text-danger fa-eraser fa-sm fa-fw"></i> � a mesma coisa que a dispensa (dismiss) <i class="fas text-muted fa-minus-circle fa-sm fa-fw"></i>?</h6>
<p class="text-danger">N�o.</p>
<p>O <b>discard (descarte)</b> � representado pelo �cone da borracha <i class="fas text-danger fa-eraser fa-sm fa-fw"></i>.</p>
<p>O descarte remove o item da STG, apagando completamente a mem�ria de opera��o daquele registro.  Todas altera��es feitas s�o perdidas, o registro volta para a pre-STG e os dados aparecem como no SIGLA.</p>
<p class="text-danger">Somente registros que n�o tenham filhos podem ser descartados.  Registros master n�o podem ser descartados at� que todas as associa��es sejam desfeitas</p>
<a href="/laundry/img/InventoryStatistics18.png" target="_blank"><img class="m-2" src="/laundry/img/InventoryStatistics18.png" style="width:600px"/></a>
</div>

<div class="alert-info m-2 p-4">
<h6>16. Ent�o o que � dispensa (dismiss) <i class="fas text-muted fa-minus-circle fa-sm fa-fw"></i>?</h6>
<p>O <b>dismiss (dispensa)</b> � representado pelo �cone do sinal de menos <i class="fas text-muted fa-minus-circle fa-sm fa-fw"></i></p>
<p>A dispensa apenas bloqueia a sa�da para o Oracle, n�o remove o item do STG.  Apesar de n�o ser migrado para o ORACLE, caso seja recomendado, o item continua contando na dire��o do alvo.</p>
<p class="text-danger">Somente podem ser dispensados registros que n�o tenham filhos, ou que n�o sejam filhos de outros.</p>
<p class="text-danger">Registros master n�o podem ser dispensados.</p>
<p class="text-danger">Registros merged tamb�m n�o podem ser dispensados.</p>
<p>A raz�o fica mais clara se pensarmos no caso dos Customers e Vendors.</p>
<p>Foi verificado que diversos registros no SIGLA se referem ao mesmo Customer.  H� uma s�rie de motivos pra isso, mas o fato � que diversos registros s�o na verdade o mesmo Customer</p>
<p>Aplicando apenas o crit�rio de migra��o, n�o h� como diferenciar com certeza um registro duplicado de outro j� limpo.</p>
<p>Mesmo assim as c�pias do registro contam no RFM.</p>
<p>Se n�o houvesse o dismiss, os registros duplicados continuariam no pre-STG, atrapalhando a contagem final.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>17. Pra qu� foi criada a dispensa (dismiss) <i class="fas text-muted fa-minus-circle fa-sm fa-fw"></i>?</h6>
<p>A raz�o fica mais clara se pensarmos no caso dos Customers e Vendors.</p>
<p>Foi verificado que diversos registros no SIGLA se referem ao mesmo Customer.  H� uma s�rie de motivos pra isso, mas o fato � que diversos registros s�o na verdade o mesmo Customer</p>
<p>Aplicando apenas o crit�rio de migra��o, n�o h� como diferenciar com certeza um registro duplicado de outro j� limpo.</p>
<p>Mesmo se tratando de c�pias do mesmo Customer, continuam contando registros diferentes para o RFM.</p>
<p>Para resolver a situa��o, os registros s�o trazidos para o STG, por�m s�o dispensados de ir para o Oracle</p>
<p class="text-info">A dispensa � uma tarefa onde o KU claramente agrega valor ao processo, j� que n�o pode ser executada automaticamente via script</p>
</div>


<div class="alert-info m-2 p-4">
<h6>17. Os registros dismissed afetam a contagem de cleansed (limpos)?</h6>
<p>Depende do que se define por cleansed (limpos)</p>
<p>Antes de aplicar qualquer valida��o o sistema checa se o registro est� dispensado.</p>
<p>Com certeza reduz o n�mero de registros no ORACLE.</p>
<p>Mas pensando no caso dos Customers duplicados, isso � algo bom e desej�vel.  N�o queremos registros duplicados no ORACLE.  Tamb�m n�o queremos deixar registros recomendados no pre-STG sem checagem manual.</p>
</div>


<div class="m-2 p-4">
<h6>18. Revis�o</h6>
<a target="_blank" href="/laundry/img/InventoryItemStates.png"><img class="m-2" src="/laundry/img/InventoryItemStates.png" style="width:600px"/></a>

</div>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
