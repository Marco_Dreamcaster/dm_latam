# Release 2018-07-20 F.A.Q.


<div class="alert-info m-2 p-4">
<h6>Is it ready?</h6>
<p>For a dubious question, a dubious answer: this is as ready as it can be, for the moment.</p>
<p>There still are some gaps, and new bugs are always expected.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>Why so many bugs?</h6>
<p>Well, think about it like this: we're trying to change tires of a vehicle in motion.</p>
<p>It's even worse than that...</p>
<p class="text-danger">We're builidng a vehicle, but the vehicle is supposed to be already moving, carrying people around.</p>
<p class="text-danger">Accidents cannot be avoided, they are bound to happen.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>What is missing?</h6>
<p>There are no organization codes for MX, SV, NI, HN, GT, and CR.</p>
<p>There are no employee codes for any of these countries as well.</p>
<p>There is no block for projects with dirty clients.</p>
<a target="_blank" href="/laundry/img/MissingOrgCodes.PNG"><img class="m-2" src="/laundry/img/MissingOrgCodes.PNG" style="width:600px"/></a>
<p class="text-danger">There are no reliable Project recommendations for any country.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>The statistics are still messed up.  How do you plan to fix that?</h6>
<p>Yes, the stats are still messy.</p>
<a target="_blank" href="/laundry/img/MessedStatistics.PNG"><img class="m-2" src="/laundry/img/MessedStatistics.PNG" style="width:600px"/></a>

<p>Quite frankly, the recommendations for Projects remains a shady subject.</p>
<p>We understand the use (and need) of alternate data sources, like the "Obras en curso" spreadsheet.  No worries about that.</p>
<p>But from the perspective of our migration team, the solution would be quite simple.</p>
<p>Just provide us with a list of recommended contracts (CONTRATO) per country, and that's it.</p>
<p>All we need is a country name and a list of SIGLA ids.  The preferred formats are regular text, or Excel sheet.</p>
<p class="text-danger">The bottom line: just tell us what you want to migrate, and we'll adjust the numbers.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>How long does it take to adjust?</h6>
<p>With the proper data, not very long...</p>
<p>Give us the SIGLA ids, and we can fix it in a couple of days.  This task will be assigned to Robson.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>The numbers keep changing, it's frustrating.  Will that ever stop?</h6>
<p>Yes, we understand the frustration, but that won't ever stop.</p>
<p>It's the nature of the problem.  We're shooting at a moving target.  We're building the gun to shoot at the target.</p>
<p class="text-danger">We'll soon include some timestamps to let you know the tool has been updated, and to what date on SIGLA.</p>
<p>Let's stick to a recommendation list, probably once at the beginning of each month, and we should be safe.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>There is a lot of unwanted Projects cleansed.  How do I get rid of them?</h6>
<p>For testing purposes, we have cleansed at least one Project per country.   We just chose the first project from the dirty list, which makes no sense business-wise.</p>
<p>If you really want to throw stuff away, don't hesitate to use the discard button (in red).</p>
<a target="_blank" href="/laundry/img/DiscardIfYouWish.PNG"><img class="m-2" src="/laundry/img/DiscardIfYouWish.PNG" style="width:600px"/></a>
<p>If you're not sure you want to throw stuff away, perhaps you should consider using instead the dismiss button (in gray).</p>
<a target="_blank" href="/laundry/img/DismissIfYouWish.PNG"><img class="m-2" src="/laundry/img/DismissIfYouWish.PNG" style="width:600px"/></a>
<p>The choice is yours.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>Are the flat files ready too?</h6>
<p>Some flat files are working, but at this release, we will not guarantee anything.</p>
<p>They will be ready very soon.  Flat files are our next priority.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>When will the other areas of the tool be ready for MX and Central America?</h6>
<p>Right now, only Inventory and Projects support all mentioned countries.</p>
<p>We're pushing to make the other areas available before the end of the month, which was our initial target.</p>
<p>We do expect to meet our target.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>When will Service Projects be available too?</h6>
<p>Service projects will be available until the end of the month of July 2018.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>I'm seeing some Service projects along regular NI and MOD projects... Why is that?</h6>
<p>We have already started working on Service Projects, that's what caused some of the bugs reported lately.</p>
<p>So, yes, you might see some Service projects creeping up on the regular Projects, but only among the pre-STG (dirty ones)</p>
<p>If you see any Service projects among the cleansed ones, discard them, and please inform us.   There should be no cleansed service projects.</p>
<p>If you see any Service projects among the dirty ones, don't do anything.  Services will have its own area on the tool very soon, and these issues will be resolved.</p>
</div>

<div class="alert-warning m-2 p-4">
<h6>Where can I get more documentation about the tool?</h6>
<p>Remember that we're building a moving vehicle... </p>
<p>In this circumstances, writing documentation is like trying to apply paint on a speeding train.</p>
<p>We're doing the best possible with the available resources.</p>
<p>If you can read Portuguese, there is <a href="/laundry/Admin/displayPost/f9501ef73aab32999c4199e5f82da5ac">a page from last week</a>.</p>
</div>


<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
