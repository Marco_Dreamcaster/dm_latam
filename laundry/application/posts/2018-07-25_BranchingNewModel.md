# Novo Modelo de Branching
Baseado no esquema de branching do GIT, conforme nos artigos [A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/?)  e [Developing and Deploying with Branches](http://guides.beanstalkapp.com/version-control/branching-best-practices.html) 

<div class="alert-info m-2 p-4">
<h6>Por qu� precisamos de um novo modelo de branching?</h6>
<p>Ap�s duras penas, atingimos um certo grau de estabilidade com todas as �reas de master data do projeto.</p>
<p>A partir do come�o de agosto, as �reas transacionais come�ar�o a ser incorporadas.</p>
<p>Precisamos garantir que as novas mudan�as n�o tenham impactos no trabalho dos demais usu�rios da ferramenta.</p>
<p class="text-danger">Com o novo modelo de branching isolaremos mudan�as, preservaremos a estabilidade do sistema, o trabalho de limpeza n�o pode parar.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>O que s�o branches?</h6>
<p>Branching � um conceito de reposit�rio de dados utilizado para representar diferentes linhas de desenvolvimento.</p>
<p>No momento, s� temos um �nico branch, chamado de master.</p>
<p>Toda mudan�a no branch master � reproduzida diretamente na Produ��o (servidor 47)</p>
<p>� uma estrat�gia muito simples, por�m equivocada.  � comum que mudan�as em trechos n�o testados do c�digo afetem outras partes que est�o est�veis.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>Qual a principal mudan�a?</h6>
<p>Al�m do branch master, teremos ao menos um outro branch adicional <b>develop</b>.</p>
<p>O Apache XAMPP servidor 47 continuar� servindo a aplica��o a partir do branch master.</p>
<p>A evolu��o do projeto se passar� a partir do branch develop.</p>
<a target="_blank" href="/laundry/img/branchingModel/main-branches@2x.png"><img class="m-2" src="/laundry/img/branchingModel/main-branches@2x.png" style="width:400px"/></a>
<p>Na figura, o n�mero de commits no branch amarelo (develop) sempre ser� muito superior ao do branch azul (master).</p>
<p>Idealmente, cada commit no branch master tem um n�mero de vers�o.</p>
<p>Com exce��o do primeiro commit, todos os demais fluem no sentido do <b>develop</b> para o <b>master</b>.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>S� h� estes dois branches?</h6>
<p>N�o, o modelo � um pouco mais complexo.</p>
<p>Os branches <b>master</b> e <b>develop</b> s�o os �nicos branches permanentes.</p>
<p>Por�m h� outros branches auxiliares para atender casos espec�ficos, como por exemplo, os hotfixes.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>O que � um hotfix?</h6>
<p>J� conhecemos os casos de produ��o que exigem a��o imediata.</p>
<p>Para resolver situa��es assim s�o criados branches hotfix.</p>
<a target="_blank" href="/laundry/img/branchingModel/hotfix-branches@2x.png"><img class="m-2" src="/laundry/img/branchingModel/hotfix-branches@2x.png" style="width:400px"/></a>
<p>Um branch de hotfix se origina no master.</p>
<p>Os branches de hotfix s�o nomeados de acordo com a tag da �ltima vers�o.</p>
<p>Podem haver quantos commits forem necess�rios at� corrigir o problema.</p>
<p>O merge do hotfix com o master, incrementa a vers�o do master.</p>
<p>O merge com o develop encerra aquele hotfix em espec�fico, logo os hotfixes tem dura��o limitada.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>H� outros tipos de branches?</h6>
<p>Sim, al�m dos hotfixes, pode haver branches auxiliares para introdu��o de novas features.</p>
<p>Por�m nesta fase inicial vamos nos familiarizar apenas com o esquema dos hotfixes.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>Como fa�o para utilizar branches na pr�tica com TortoiseGit?</h6>
<p>N�o h� muita diferen�a nos procedimentos padr�o, mas � preciso entender bem os conceitos apresentados anteriormente.</p>
<p>No TortoiseGit � preciso atentar para qual branch a opera��o de push ou pull se refere.</p>
<a target="_blank" href="/laundry/img/branchingModel/MoreCommitsToDevelop.png"><img class="m-2" src="/laundry/img/branchingModel/MoreCommitsToDevelop.png" style="width:400px"/></a>
<p>Para trocar o branch sobre o qual est� se atuando h� uma op��o de menu:</p>
<a target="_blank" href="/laundry/img/branchingModel/MoreCommitsToDevelop02.png"><img class="m-2" src="/laundry/img/branchingModel/MoreCommitsToDevelop02.png" style="width:400px"/></a>
<p>Lembrando que o prefixo "remotes" indica a vers�o que est� no servidor do Bitbucket:</p>
<a target="_blank" href="/laundry/img/branchingModel/MoreCommitsToDevelop03.png"><img class="m-2" src="/laundry/img/branchingModel/MoreCommitsToDevelop03.png" style="width:400px"/></a>
</div>

<div class="alert-info m-2 p-4">
<h6>Como fa�o para dar um merge do develop com o master? O que � um Pull Request?</h6>
<p>Eventualmente as mudan�as realizadas no develop tem que ser replicadas no master.</p>
<p>Esta opera��o � chaamda de pull request:</p>
<a target="_blank" href="/laundry/img/branchingModel/MergePullRequest01.png"><img class="m-2" src="/laundry/img/branchingModel/MergePullRequest01.png" style="width:400px"/></a>
<p>Tomando sempre cuidado para que o branch develop n�o seja destru�do no final da opera��o</p>
<a target="_blank" href="/laundry/img/branchingModel/MergePullRequest02.png"><img class="m-2" src="/laundry/img/branchingModel/MergePullRequest02.png" style="width:400px"/></a>
</div>

<div class="alert-info m-2 p-4">
<h6>Surgiu um problema em produ��o? Como fa�o?</h6>
<p>Primeiro crie um branch com prefixo <i>hotfix-*</i> baseado na �ltima tag do master.</p>
<a target="_blank" href="/laundry/img/branchingModel/HotFix-01.PNG"><img class="m-2" src="/laundry/img/branchingModel/HotFix-01.png" style="width:400px"/></a>
<p>Depois mude of foco (switch to) para o branch criado:</p>
<a target="_blank" href="/laundry/img/branchingModel/HotFix-02.png"><img class="m-2" src="/laundry/img/branchingModel/HotFix-02.png" style="width:400px"/></a>
<p class="text-danger">� normal que alguns arquivos aparentemente "sumam" da sua �rvore de projeto.   Na verdade, os arquivos n�o sumiram, est�o apenas guardados no branch no qual estava trabalhando anteriormente.</p>
<p>Todas demais opera��es de commit e push seguem normalmente, realizadas neste novo branch.</p>
<p>Realizar quantos commits forem necess�rios para resolver o problema.</p>
<p>Quando terminar a corre��o do erro, fazer o pull request para merge com o master.  Marcar o branch hotfix-* para ser removido.</p>
<p>Agora o master est� na "frente" do develop por alguns commits (provenientes da corre��o).</p>
<p>Para continuar utilizando develop, � necess�rio fazer uma sincronia (na interface do BitBucket).</p>
<p>Faremos uma revis�o do processo de hotfix num outro post.</p>
</div>

<div class="alert-info m-2 p-4">
<h6>Isso tudo soa complicado.  � realmente necess�rio?</h6>
<p>Sim, sem d�vida � complicado, por�m necess�rio.</p>
<p>O esquema de commits em m�ltiplos branches, pull requests � a forma de trabalho de projetos de m�dio e grande porte</p>
<p>Se tudo correr bem, este � o primeiro passo para verdadeira integra��o cont�nua, testes e deployment automatizados</p>
</div>



<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
