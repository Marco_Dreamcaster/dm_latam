# Hotfix example

Um exemplo de como criar um branch hotfix

## 1. Verificar a �ltima vers�o do master no BitBucket

No caso a vers�o come�a em 0.9

<a target="_blank" href="/laundry/img/howToHotFix/hotfix-01.png"><img class="m-2" src="/laundry/img/howToHotFix/hotfix-01.png" style="width:400px"/></a>


## 2. Criar o branch hotfix-*

No TortoiseGit, escolher a op��o "Switch/Checkout", escolher switch to "master", certificar-se de que a op��o "create new branch" est� selecionada E o nome do novo branch est� preenchido "hotfix-0.9.1"

Ao final da cria��o os commits est�o sendo feitos no branch "hotfix-0.9.1"

<a target="_blank" href="/laundry/img/howToHotFix/hotfix-02.png"><img class="m-2" src="/laundry/img/howToHotFix/hotfix-02.png" style="width:400px"/></a>

## 3. Realizar os commits no branch hotfix-*

Realizar os commits normalmente, at� estar satisfeito de ter resolvido o problema.


## 4. Criar o pull request

Na p�gina do BitBucket, criar o pull request

<a target="_blank" href="/laundry/img/howToHotFix/hotfix-03.png"><img class="m-2" src="/laundry/img/howToHotFix/hotfix-03.png" style="width:400px"/></a>
<a target="_blank" href="/laundry/img/howToHotFix/hotfix-04.png"><img class="m-2" src="/laundry/img/howToHotFix/hotfix-04.png" style="width:400px"/></a>

## 5. Fazer o merge do pull request

Na p�gina do BitBucket, confirmar o pull request com o bot�o de merge

<a target="_blank" href="/laundry/img/howToHotFix/hotfix-05.png"><img class="m-2" src="/laundry/img/howToHotFix/hotfix-05.png" style="width:400px"/></a>

Esta opera��o traz as mudan�as do hotfix para o master:

<a target="_blank" href="/laundry/img/howToHotFix/hotfix-05.1.png"><img class="m-2" src="/laundry/img/howToHotFix/hotfix-05.1.png" style="width:400px"/></a>


## 6. Incrementar a tag no commit

Incrementar a tag de acordo com o n�mero da vers�o, no caso "0.9.1"

<a target="_blank" href="/laundry/img/howToHotFix/hotfix-06.png"><img class="m-2" src="/laundry/img/howToHotFix/hotfix-06.png" style="width:400px"/></a>

## 7. Voltar para o branch padr�o develop

No TortoiseGit, escolher "Switch/Checkout" e voltar para o branch padr�o develop.

<a target="_blank" href="/laundry/img/howToHotFix/hotfix-07.png"><img class="m-2" src="/laundry/img/howToHotFix/hotfix-07.png" style="width:400px"/></a>

## 8. Realizar o pull a partir do master

Para trazer as �ltimas altera��es do hotfix para dentro do branch develop, basta realizar um pull, tendo o master como origem:

<a target="_blank" href="/laundry/img/howToHotFix/hotfix-08.png"><img class="m-2" src="/laundry/img/howToHotFix/hotfix-08.png" style="width:400px"/></a>

Nos diagramas anteriores, isto equivale a segunda parte da opera��o:

<a target="_blank" href="/laundry/img/howToHotFix/hotfix-08.1.png"><img class="m-2" src="/laundry/img/howToHotFix/hotfix-08.1.png" style="width:400px"/></a>

## 9. Continuar desenvolvendo no branch develop

Continuar o desenvolvimento no branch padr�o develop.


<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
