# New Address Style (novo estilo de endere�os)

Temos um novo estilo de entrada de endere�os na ferramenta.  A mudan�a ocorreu por conta dos diferentes estilos de endere�os utilizados em cada pa�s.

...

De maneira simplificada, em alguns pa�ses a hierarquia �:

COUNTRY > STATE > CITY

Em outros, a hierarquia �:

COUNTRY > PROVINCE > CITY

O problema acontece quando � necess�rio registrar um endere�o de um pa�s diferente da origem.  Um exemplo � quando cadastramos um endere�o americano (US) durante a limpeza da (CO).

...

Independente do estilo adotado para cada pa�s, os formul�rios de entrada sempre ser� na hierarquia:

COUNTRY > STATE > PROVINCE > CITY

<a target="_blank" href="/laundry/img/newAddressStyle/NewAddressStyle.PNG"><img class="m-2" src="/laundry/img/newAddressStyle/NewAddressStyle.PNG" style="width:400px"/></a>

...

### Todos os campos s�o obrigat�rios agora?

A princ�pio, n�o.

A obrigatoriedade continua seguindo o padr�o adotado em cada pa�s.

Por�m, na pr�tica ser� preciso preencher todos os combos para especificar a cidade.


...

### O pa�s do endere�o n�o tem prov�ncia/estado?  Como fa�o?

Quando um pa�s n�o adotar prov�ncia ou estado, ainda assim ser� necess�rio preencher todos campos.

Nestes casos, os valores de prov�ncia e estado devem ser id�nticos.

...

### O pa�s adota tanto prov�ncia e estado, como devo proceder?

Continua valendo a regra de registrar as combina��es v�lidas na ferramenta, diretamente no link:

http://10.148.1.47/laundry/Maps/ORAGeoCodes

No caso de s� haver estado, cadastrar uma prov�ncia com mesmo nome.

No caso de s� haver prov�ncia, cadastrar um estado com mesmo nome.

No caso de ambos, n�o h� d�vidas, basta cadastrar os valores.

...

### O que acontece com a divis�o de condado (county)?

N�o estamos considerando casos com condado. 

� prov�vel que endere�os latino-americanos nunca venham a precisar de condado.

...

### Quais s�o os pa�ses com valida��o no ORACLE?

Por enquanto, os seguintes pa�ses tem valores checados: PA, CO, MX, CR, SV, GT, NI, HN, NL e US.

Caso n�o encontre na ferramenta valores geogr�ficos para estes pa�ses, por favor, avise imediatamente.

Pode ser que os valores j� estejam cadastrados em alguma inst�ncia do ORACLE.  � poss�vel que seja necess�rio fazer uma sincronia.  N�o valem apenas os dados da ferramenta.  Como sempre temos que levar em conta os dados do ORACLE.

Para outros pa�ses, o texto de cadastro � livre, por�m recomenda-se utilizar o ingl�s para padronizar os nomes de localidades, salvo em casos no qual o uso do nome local fa�a mais sentido. 

...

### Quais dados v�m do SIGLA?

A localiza��o geogr�fica no SIGLA � complexa. O valor da cidade est� sendo carregado, mas apenas para servir como indica��o.

Para registros novos, � poss�vel conferir este valor no campo CITY dos formul�rios base.


...

### A p�gina ficou mais lenta ou � impress�o minha?

N�o � apenas impress�o, a p�gina est� ligeiramente mais lenta.

Al�m disso parece travar quando vai carregar estados, prov�ncias e principalmente o combo das cidades.

H� solu��o t�cnica para minimizar este problema, mas leva algum tempo para ser codificada.  Outras tarefas mais priorit�rias est�o requerendo nossa aten��o. O ajuste ficar� para o futuro.

A p�gina foi testada na VPN, apesar de mais lenta, sempre funcionou corretamente.  Imagino que no CSC e na rede interna esteja melhor.


<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
