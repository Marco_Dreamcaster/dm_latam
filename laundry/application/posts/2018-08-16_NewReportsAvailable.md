# New CSV Reports

We're glad to inform that now the tool is able to generate CSV reports.

CSV (or comma-separated-values) are standard text files that can be opened, and manipulated via Excel.  CSV can also be opened in Notepad.

Reports are timestamped to avoid confusion regarding dates.

## A hint about CSV files

CSV files can be SAVED AS full Excel spreadsheets so that the user may add appropriate styling, formatting, or his own customized formulas.

When saved in Excel format, there is the handy AutoFit Column Width option, which makes the report a bit easier to read.

<a target="_blank" href="/laundry/img/newCSVReports/AutoFitColumnWidth.PNG"><img class="m-2" src="/laundry/img/newCSVReports/AutoFitColumnWidth.PNG" style="width:400px"/></a>


## Inventory

### Inventory Audit Reports

The Inventory Audit report lists all items on the preSTG (<a target="_blank" href="/laundry/NamedQueries/InventorySCCFamilies/PA">download the PA version from here</a>)

The report follows the format currently agreed upon for auditing:

* MIGRATION_CODE, usually a composition of the country-code + SIGLA id
* STAGED, 1 means the record has been moved to the STG (cleansed)
* DISMISSED, 0 means the record was NOT dismissed by the KU
* VALIDATED, 1 means the record passes the ORACLE validation criteria 
* RFM, 1 means the record was recommended for migration  (at the time of report generation)
* ORIG_ITEM_NUMBER, the same as MIGRATION_CODE 
* ITEM_NUMBER, the code used for the item in ORACLE

Other columns contain additional info about that record (when available on STG).

#### What gets migrated to ORACLE?

To be migrated into ORACLE, the combination of flags must be:

STAGED = 1,  DISMISSED=0,  VALIDATED=1

<a target="_blank" href="/laundry/img/newCSVReports/AuditReportsMigrationCondition.PNG"><img class="m-2" src="/laundry/img/newCSVReports/AuditReportsMigrationCondition.PNG" style="width:400px"/></a>

Any other combinations are NOT MIGRATED.

### SCC Families

The SCC families report lists all items on the STG, and their corresponding families.  (<a target="_blank" href="/laundry/NamedQueries/InventorySCCFamilies/PA">download the PA version from here</a>)

There are two files on this report:

* itemsSCCFamilies_.csv, which lists ITEM_NUMBER_SIGLA, ITEM_NUMBER_ORACLE, and SCC_FAMILY
* groupedItemsSCCFamilies_.csv, which groups how many items are on each family

The grouped query report tells which families are more frequent.












<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
