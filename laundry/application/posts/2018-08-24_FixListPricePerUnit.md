
# Regras para c�lculo de listPricePerUnit

1) ao lado do campo listPricePerUnit h� um checkbox manualPriceOverride (@see screenshot).

Caso o checkbox esteja marcado, o valor digitado listPricePerUnit ser� utilizado diretamente nos arquivos de sa�da, caso seja maior do que $1.

Caso o checkbox esteja marcado e o valor digitado seja inferior a $1, o item ser� rejeitado.

Caso o checkbox n�o esteja marcado, o valor ser� calculado automaticamente.

2) as regras de c�lculo todos items sem manualPriceOverride  s�o as seguintes:

Todos come�am valendo $1.

a) listPricePerUnit ser� atualizado de acordo com PRODUCTOS.ULTCOMPRA  (apenas se valor for maior do que $1)

b) para cada item na query transacional de estoque, listPricePerUnit ser� atualizado (apenas se o valor for maior que $1)


Ao final do procedimento, todos items ter�o listPricePerUnit:
1) digitado pelo usu�rio
2) OU o valor de PRODUCTOS.ULTCOMPRA
3) OU o valor calculado pela rotina
4) OU pelo menos $1 

...

S� foi tratado aqui o tratamento de listPricePerUnit.  O assunto do LOCATOR_NAME ser� abordado noutra mensagem.

Os arquivos correspondentes ser�o disponibilizados nas pr�ximas horas.