# Tutorial para cria��o de PHP CRD

Demonstrar passos para cria��o de entidades de mapeamento para GL.

## Passos

1. cria��o da entidade Doctrine
2. declara��o de rotas
3. defini��o de controladores
4. desenho de interface
5. teste global

### 1.Criar Entidade Doctrine

Doctrine permite mapear tabelas do banco de dados para entidades PHP.

#### 1.1. criar PHP Data Object 

dm_latam\laundry\application\models\Commons\MAPS\GLCostCenter.php

<a target="_blank" href="/laundry/img/GLMapsTutorial/GLMapsTutorial_00.PNG"><img class="m-2" src="/laundry/img/GLMapsTutorial/GLMapsTutorial_00.PNG" style="width:400px"/></a>

Para novas entidades:

Copiar arquivo de outra entidade na mesma pasta, renomear de acordo com a necessidade.

Adaptar anota��es de acordo com os par�metros da tabela fonte.

Utilizar IDE para gerar getters/setters

#### 1.2. testar schema

Este teste serve para verificar se o mapeamento da entidade est� de acordo com o esquema da tabela.

O teste � feito atrav�s da ferramenta de linha de comando do Doctrine.

Realizar o commit / push da entidade criada no passo anterior.

Conectar-se no servidor de produ��o.

Atualizar o reposit�rio dm_latam no servidor (pull)

Abrir uma linha de comando (ConEmu), navegar at� o diret�rio c:\dm_latam.

Testar se a linha de comando do Doctrine est� dispon�vel:

```
.\vendor\bin\doctrine.bat orm:info
```

<a target="_blank" href="/laundry/img/GLMapsTutorial/GLMapsTutorial_01.PNG"><img class="m-2" src="/laundry/img/GLMapsTutorial/GLMapsTutorial_01.PNG" style="width:400px"/></a>

A nova entidade deve aparecer na resposta do comando, sem nenhum aviso de erro.

Com a entidade reconhecida, testar se algum mudan�a de schema � necessaria:

```
C:\dm_latam\laundry> .\vendor\bin\doctrine.bat orm:schema-tool:update --dump-sql

```

No caso, a resposta do comando s�o os DDL para chave prim�ria e declara��es de NOT NULL

```

ALTER TABLE GLCostCenter ADD id INT IDENTITY NOT NULL;
ALTER TABLE GLCostCenter ALTER COLUMN CTAORACLE NVARCHAR(8) NOT NULL;
ALTER TABLE GLCostCenter ALTER COLUMN DESDE NVARCHAR(4) NOT NULL;
ALTER TABLE GLCostCenter ALTER COLUMN HASTA NVARCHAR(4) NOT NULL;
ALTER TABLE GLCostCenter ALTER COLUMN COUNTRY NVARCHAR(2) NOT NULL;
ALTER TABLE GLCostCenter ADD PRIMARY KEY (id);
```

Conectar ao banco e rodar o script para atualizar o schema, agora a tabela tem um id autom�tico para cada registro.

<a target="_blank" href="/laundry/img/GLMapsTutorial/GLMapsTutorial_02.PNG"><img class="m-2" src="/laundry/img/GLMapsTutorial/GLMapsTutorial_02.PNG" style="width:400px"/></a>

Testar novamente se o update foi bem sucedido:

```
C:\dm_latam\laundry> .\vendor\bin\doctrine.bat orm:schema-tool:update --dump-sql
Nothing to update - your database is already in sync with the current entity metadata.

```
A �ltima mensagem indica que:
a) o schema do banco est� completamente sincronizado com o mapeamento na entidade PHP
b) � poss�vel utilizar o Doctrine para realizar atividades b�sicas CRUD: cria��o (Create), listagem (Retrieve), atualiza��o (Update), e remo��o (Delete).

O pr�ximo passo � declarar as rotas.

### 2.Declarar Rotas

As rotas representam as a��es que o sistema pode executar.

S�o configuradas no arquivo:

dm_latam\laundry\application\config\routes.php

O trecho referente �s rotas de GL Param est�o no final do arquivo

```
/* GL Params */
$route['GLParams/(:any)'] = 'GLParams/index/$1';
$route['GLParams/addGLCostCenter/(:any)'] = 'GLParams/addGLCostCenter/$1';
$route['GLParams/removeGLCostCenter/(:any)'] = 'GLParams/removeGLCostCenter/$1';
```


### 2.1.Rota Listagem - GLParams/(:any)

Nesta rota o sistema apresenta a tela de par�metros do GL para um determinado pa�s $1.

```
/* GL Params */
$route['GLParams/(:any)'] = 'GLParams/index/$1';
```


Esta � a rota para listar todos os tipos de par�metros de GL: GLCostCenter, GLLineOfBusiness, GLExclusionCuentas, etc...

Equivale a opera��o de listagem (Retrieve)


### 2.2.Rota criar GLCostCenter - GLParams/addGLCostCenter/(:any)

Nesta rota o controlador atende a um HTTP POST para criar um GLCostCenter num determinado pa�s $1.

```
/* GL Params */
$route['GLParams/addGLCostCenter/(:any)'] = 'GLParams/addGLCostCenter/$1';
```

Este � a rota informada no atributo action do HTML Form do GLCostCenter.

O �nico par�metro (:any) � o c�digo $1 do pa�s.

Equivale a opera��o de cria��o (Create)


### 2.3.Rota remover GLCostCenter - GLParams/removeGLCostCenter/(:any)

Nesta rota o controlador atende um HTTP GET para remover o registro GLCostCenter com id $1.

```
/* GL Params */
$route['GLParams/removeGLCostCenter/(:any)'] = 'GLParams/removeGLCostCenter/$1';
```

O �nico par�metro (:any) � o id $1 do GLCostCenter.
 
Este � a rota informada no atributo href do HTML Form do GLCostCenter.

Equivale a opera��o de remo��o (Delete)


### 2.4. Entidades sem update, com create/remove

Caso o usu�rio queira atualizar uma determinada faixa de GLCostCenter, deve remover a anterior e adiconar uma nova.


### 2.5. Entidades com update, sem create/remove

Os dados s�o pr�-carregados nas tabelas de origem (a partir do SIGLA). A �nica op��o do usu�rio � alterar uma (ou mais) coluna(s) (de mapeamento).

Ao clicar numa determinada linha da tabela, os dados daquela linha s�o carregados no formul�rio no topo da tela.

(checar o que acontece quando registros est�o filtrados)


### 3.Definir endpoints nos Controladores

Um endpoint equivale � uma fun��o no controlador

Teremos apenas um controlador para todas os endpoints para toas as entidades de par�metros de GL:

### 3.1 index($country)




### 4.Desenhar interface

### 5.Teste Global

Ao contr�rio da boa pr�tica, os testes est�o sendo abordados por �ltimo.

Num mundo ideal, devemos come�ar escrevendo os testes para depois implementar uma funcionalidade.

Infelizmente, neste projeto de migra��o estamos longe desta realidade.

Faremos � moda antiga, por tentativa e erro na pr�pria interface.
 
As perguntas a seguir representam expectativas b�sicas de comportamento do sistema.


#### 5.1 Listar todos GLCostCenter

Consigo ver todos par�metros do banco de dados para um determinado pa�s?

(consigo filtrar por quantidade e por valor?)

Est� filtrando par�metros por pa�s corretamente?  N�o vemos dados de um pa�s aparecendo na tela de outro?

O que acontece quando um pa�s n�o tem dados?

#### 5.2 Adicionar um GLCostCenter

Consigo criar um novo GLCostCenter?
  
Consigo ver o novo GLCostCenter na listagem ap�s a cria��o? 

O novo registro permanece quando saio e entro da tela?

O que acontece quando informamos exatamente os mesmos par�metros de outro registro existente?

Consigo bloquear a cria��o de um registro sem todos os par�metros obrigat�rios?


#### 5.2 Remover um GLCostCenter

Consigo remover um GLCostCenter?

(o usu�rio � avisado de que a remo��o n�o tem volta e pede-se uma confima��o?)

Deixo de ver o GLCostCenter na listagem ap�s a apag�-lo?  O registro � efetivamente apagado da tabela?

O velho registro n�o reaparece quando saio e entro da tela?

#### 5.3 Alterar um registro

Ao clicar numa determinada linha da tabela, os dados daquela linha s�o carregados no formul�rio no topo da tela.

(checar o que acontece quando registros est�o filtrados)


<br/>
<br/>
<br/>
<br/>
<br/>



