<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    <div class="container">
        <div class="row">
            <div class="card border-primary mb-3" style="max-width: 18rem; margin: 10px;">
                <div class="card-header">IDL</div>
                <div class="card-body text-primary">
                    <h5 class="card-title"><a href="<?php echo base_url()?>IDL/descriptors"><?php echo $descriptors?> descriptors</a></h5>
                    <p class="card-text">Interface Definition Libraries correspond 1:1 to flat files</p>
                </div>
                <div class="card-body text-primary">
                    <h5 class="card-title"><a href="<?php echo base_url()?>IDL/fields"><?php echo $fields?> fields</a></h5>
                    <p class="card-text">IDLs are composed of fields with different loading values.</p>
                </div>
            </div>
            <div class="card border-primary mb-3" style="max-width: 18rem; margin: 10px;">
                <div class="card-header">Groups</div>
                <div class="card-body text-primary">
                    <h5 class="card-title"><a href="<?php echo base_url()?>IDL/groups"><?php echo $groups?> groups</a></h5>
                    <p class="card-text">IDLs are organized into functional groups.</p>
                </div>
            </div>
            <div class="card border-primary mb-3" style="max-width: 18rem; margin: 10px;">
                <div class="card-header">Countries</div>
                <div class="card-body text-primary">
                    <h5 class="card-title"><?php echo $countries?> countries</h5>
                    <p class="card-text">IDLs are currently spread through a few countries.</p>
                </div>
            </div>
        </div>

    </div>
</main>
