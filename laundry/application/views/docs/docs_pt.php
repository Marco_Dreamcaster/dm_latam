<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>Laundry Documentation</title>

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/reset.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/reveal.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/theme/sky.css">

    <!-- Theme used for syntax highlighting of code -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>lib/css/monokai.css">

    <!-- Printing and PDF exports -->
    <script>
        var link = document.createElement( 'link' );
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
        document.getElementsByTagName( 'head' )[0].appendChild( link );
    </script>
</head>
<body>
<div class="reveal">
    <div class="slides">
        <section data-markdown>
            <textarea data-template>
            ## TKE LatAm
            ### Migração ERP SIGLA-ORACLE
            #### (nov/17 - ago/19)
            </textarea>
        </section>
        <section data-markdown>
            <textarea data-template>
                ### Seja bem vindo à Laundry

                Ferramenta utilizada para auxiliar no Projeto Progress, em operação no período entre novembro de 2017 e agosto de 2019
            </textarea>
        </section>
        <section data-markdown>
            <textarea data-template>
                ### Projeto Progress

                O Projeto Progress buscou uniformizar processos, práticas de negócio, auditoria e controle.

                Foi um iniciativa conjunta da TKE LatAm e TKE USA, parte de um processo maior de re-estruturação na região.

            </textarea>
        </section>
        <section data-markdown>
            <textarea data-template>
                ### Troca de ERPs

                Durante 15 anos, a TKE LatAm investiu no desenvolvimento do SIGLA uma solução in-house de ERP.

                A TKE USA vinha recentemente de uma experiência bem sucedida com a implementação de ORACLE em conjunto com a TKE NL.

            </textarea>
        </section>
        <section data-markdown>
            <textarea data-template>
                ### Missão da Zallpy

                Garantir que a troca dos ERPs ocorresse da melhor maneira possível:
                - sem perda de informações
                - sem perda de capacidade operativa
                - atendendo aos requisitos da TKE USA
                - atendendo a novos requisitos identificados ao longo do projeto

                Basicamente um trabalho de ETL

            </textarea>
        </section>
        <section data-markdown>
            <textarea data-template>
                ### Extract - Transform (lado da TKE LatAm - Zallpy)

                - sem acesso direto à Produção
                - dados sincronizados regularmente com snapshots disponibilizados na rede interna
                -
                - aplicativo em PHP para transformação de dados pelo KUs (Laundry)
                - uma única máquina virtual

            </textarea>
        </section>
        <section data-markdown>
            <textarea data-template>
                ### Load (lado da TKE USA - load)

                - através de arquivos de texto (flatfiles CSV)
                - reaproveitando rotinas SQL para carga no ORACLE desenvolvidas pela TKE USA anteriormente
                - outros modos de carga incluindo WebDL e data-loaders customizados
                - cada arquivo com sua especificação de campos e tipos
                - diversos ambientes de teste e homologação

            </textarea>
        </section>
        <section>
            <section data-markdown>
                <textarea data-template>
                    ### Alguns números
                </textarea>
            </section>
            <section data-markdown>
                <textarea data-template>
                    Equipe de Projeto
                    - 3 managers (project manager, tech-lead, change manager)
                    - 10 KUs (equipe permanente no Brasil) (um para cada macro-área Projects, Customers, Vendors)
                    - 2 a 3 champions por país (treinamento e operação)
                    - 5 consultores Oracle (treinamento dos KUs)
                    - 3 programadores dedicados apenas para o ORACLE (tarefas de migração)
                </textarea>
            </section>
            <section data-markdown>
                <textarea data-template>
                    SIGLA ERP
                    - em operação por 15 anos
                    - 13 países
                    - 450 usuários
                    - 30000 operações diárias
                    - 4.5GB de dados consolidados
                    - VBasic 6.0 + MSSQL 2008
                </textarea>
            </section>
        </section>
        <section data-markdown>
            <script type="text/template">
                <!-- .slide: data-background="#EEEEEE" -->
                Markdown content
            </script>
        </section>
    </div>
</div>

<script src="<?php echo base_url(); ?>js/reveal.js"></script>

<script>
    // More info about config & dependencies:
    // - https://github.com/hakimel/reveal.js#configuration
    // - https://github.com/hakimel/reveal.js#dependencies
    Reveal.initialize({
        dependencies: [
            { src: '<?php echo base_url(); ?>plugin/markdown/marked.js' },
            { src: '<?php echo base_url(); ?>plugin/markdown/markdown.js' },
            { src: '<?php echo base_url(); ?>plugin/notes/notes.js', async: true },
            { src: '<?php echo base_url(); ?>plugin/highlight/highlight.js', async: true }
        ]
    });
</script>
</body>
</html>
