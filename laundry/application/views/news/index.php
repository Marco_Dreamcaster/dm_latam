<h2><?php echo $title; ?></h2>
<br/>
<p><a class="nav-link" href="<?php echo base_url(); ?>index.php/news/create">create news item</a></p>
<br/>
<?php foreach ($news as $news_item): ?>

    <h3><?php echo $news_item['title']; ?></h3>
    <div class="main">
        <?php echo $news_item['text']; ?>
    </div>
    <p><a href="<?php echo site_url('news/'.$news_item['slug']); ?>">View article</a></p>

<?php endforeach; ?>
