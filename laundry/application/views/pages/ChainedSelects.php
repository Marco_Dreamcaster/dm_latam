<h1>Chained Selects</h1>

<p>Thanks to the lovely <a href="https://www.npmjs.com/package/jquery-chained">jquery-chained</a> plugin.</p>

<h2>Simple Example</h2>

<form>
    <div class="form-group row">
        <div class="col-sm-3 my-1">
            <select id="addressCountry" class="form-control" name="addressCountry">
                <option value="">--</option>
                <option value="PA">PA</option>
                <option value="CO">CO</option>
            </select>
        </div>
        <div class="col-sm-3 my-1">
            <select id="province" class="form-control" name="province">
                <option value="">--</option>
                <option value="PA1" data-chained="PA">PA1</option>
                <option value="PA2" data-chained="PA">PA2</option>
                <option value="PA3" data-chained="PA">PA3</option>
                <option value="CO1" data-chained="CO">CO1</option>
                <option value="CO2" data-chained="CO">CO2</option>
                <option value="CO3" data-chained="CO">CO3</option>
            </select>
        </div>
        <div class="col-sm-3 my-1">
            <select id="city" class="form-control" name="city">
                <option value="">--</option>
                <option value="PA1.1" data-chained="PA1">PA1.1</option>
                <option value="PA1.2" data-chained="PA1">PA1.2</option>
                <option value="PA1.3" data-chained="PA1">PA1.3</option>
                <option value="PA1.4" data-chained="PA1">PA1.4</option>
                <option value="PA2.1" data-chained="PA2">PA2.1</option>
                <option value="PA2.2" data-chained="PA2">PA2.2</option>
                <option value="PA3.1" data-chained="PA3">PA3.1</option>
                <option value="CO3.1" data-chained="CO3">CO3.1</option>
                <option value="CO3.2" data-chained="CO3">CO3.2</option>
            </select>
        </div>
    </div>
</form>

<script>
    $("#province").chained("#addressCountry");
    $("#city").chained("#province");
</script>
