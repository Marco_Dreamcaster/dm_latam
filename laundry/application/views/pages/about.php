<h2>1. Minuta 2018-03-09</h2>
<ol>
    <li>
        Situación Actual:

        ·         Offline con SIGLA producción

        ·         Un usuario por objeto

        ·         Framework poco flexible

        ·         KPI offline

    </li>
    <li>
        Objetivos:

        ·         Consolidar una sola herramienta de migración.
    </li>
    <li>
        Beneficios:

        ·         En línea con SIGLA producción.
        ·         KPI en línea
        ·         Herramienta Web Multiusuario
        ·         Framework flexible (php –html5)

    </li>
    <li>
        Próximos pasos:

        ·         Terminar pendientes de template Oracle. (Marco)
        ·         Implementar primera versión con building para Panamá. (Fabiano)
        ·         Crear menú principal con opciones VIEW – Oracle (Fabiano)
        ·         Preparar primer objeto de Oracle en DM Progress  (Marco)
        ·         Preparar traspaso de Excel a DM (Marco-Nacho)
        ·         Upgrade servidor “.47” (Nacho-Eleazar)
        ·         Deshabilitar XenDesktop – Citrix (Nacho-Eleazar)

    </li>
</ol>

    <h2>2. Small CodeIgniter Tutorial and POC (done)</h2>

    <a href="https://codeigniter.com/user_guide/">CodeIgniter User Guide</a>
    <br/>
    <a href="https://codeigniter.com/user_guide/helpers/form_helper.html">Details about CodeIgniter FormHelper</a>
    <br/>
    <p>CodeIgniter has a powerful form validation library... You can read <a href="https://codeigniter.com/user_guide/libraries/form_validation.html"><span class="doc">more about this library
here</span></a>.</p>

    <h2>3. Incorporating Composer to load PHP dependencies (done) </h2>

    <p>installed Composer on ".47" <a href="https://getcomposer.org/doc/00-intro.md"">downloaded from here</a></p>
    <pre>

        PS C:\Users\90000131> composer --version
Composer version 1.6.3 2018-01-31 16:28:17

        PS C:\Users\90000131> cd C:\dm_latam\laundry

        PS C:\dm_latam\laundry> composer update

Loading composer repositories with package information
Updating dependencies (including require-dev)
Package operations: 39 installs, 0 updates, 0 removals
  - Installing mikey179/vfsstream (v1.1.0): Downloading (100%)
  - Installing smarty/smarty (v3.1.31): Downloading (100%)
  - Installing symfony/polyfill-mbstring (v1.7.0): Downloading (100%)
  - Installing psr/log (1.0.2): Downloading (100%)

        ....

phpunit/php-code-coverage suggests installing ext-xdebug (^2.5.1)
phpunit/phpunit suggests installing phpunit/php-invoker (~1.1)
phpunit/phpunit suggests installing ext-xdebug (*)
Generating autoload files
    </pre>

    <h3>4. Steps to Enable SQL Server connection for PHP (needed on .47)</h3>

    <a href="https://www.microsoft.com/en-us/download/details.aspx?id=20098">Microsoft SQL Server PHP Drivers official download site</a>

    <p> OR copy files on repo at dm_latam\laundry\support to c:\xampp\php\ext\</p>

    <p>changes to PHP.ini</p>

    <pre>
        'SQL Server extensions
        extension=php_pdo_sqlsrv_56_ts.dll
        extension=php_sqlsrv_56_ts.dll
    </pre>


    <h3>5. Steps to Host Laundry app directly from dm_latam repository  (needed on .47)</h3>


    <p>changes to httpd.conf (easy access with Apache config button on XAMPP control panel)</p>

    <pre>
        <IfModule alias_module>
    #
    # Redirect: Allows you to tell clients about documents that used to
    # exist in your server's namespace, but do not anymore. The client
            ...
            ...
            ...
    # need to provide a <Directory> section to allow access to
    # the filesystem path.
    Alias /laundry d:\pub\dm_latam\laundry\

    #
    # ScriptAlias: This controls which directories contain server scripts.
                ...
    #
    ScriptAlias /cgi-bin/ "C:/xampp/cgi-bin/"

    <Directory c:\dm_latam\laundry\ >
      AllowOverride All
      Options Indexes FollowSymLinks MultiViews
      Require all granted
    </Directory>

</IfModule>
    </pre>

    <h3>6. Debug session</h3>

    <a href="<?php echo base_url(); ?>/phpinfo.php">Check local install with PHPInfo</a>

    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
