</main>

<footer class="container-fluid  text-right">
    <nav class="navbar container-fluid bg-dark bg-TKE fixed-bottom">
        <div class="row container-fluid">
            <div class="col-md-4 text-left">
                <a><small class="text-muted text-left">BASE DE DATOS DE <?php echo $basedatos;?></small></a>
            </div>
            <div class="col-md-4 text-center">
                <?php if (isset($user8id)) : ?>
                <?php switch($clearance):
                case 0: ?>
                    <i class="fas fa-user fa-sm fa-fw text-white" title="normal"></i>
                    <?php break; ?>
                <?php case 1: ?>
                    <i class="fas fa-user-cog fa-sm fa-fw text-warning" title="tester"></i>
                    <?php break; ?>
                <?php case 2: ?>
                    <i class="fas fa-user-tie fa-sm fa-fw text-danger" title="admin"></i>
                    <?php break; ?>
                <?php endswitch; ?>
                    <span class="text-TKE"><?php echo $user8id;?></span>
                <a href="<?php echo base_url(); ?>Admin/logout" title="logout"><i class="fas fa-sign-out-alt fa-sm fa-fw"></i></a>
                <?php endif; ?>
            </div>
            <div class="col-md-4 text-right">
                <a href="<?php echo base_url(); ?>Admin/posts"><small class="text-muted">v 1.6</small></a>
            </div>

        </div>

    </nav>
</footer>


<!--
<footer class="navbar bg-TKE footer navbar-expand-lg bg-dark navbar-light col-md-12 ml-sm-auto col-lg-12 animate-area bg-dark page-footer font-small pt-4 mt-4">
</footer>
-->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>js/countrySelect/countrySelect.js" charset="UTF-8"></script>
<script src="<?php echo base_url(); ?>js/laundry.js" charset="UTF-8"></script>

<script>
    $("#country").countrySelect({
        defaultCountry: "<?php echo $sessionCountry;?>",
        onlyCountries: ['pe','cl','ar','uy','py','pa','co','mx','sv','ni','hn','gt','cr'],
        //preferredCountries: ['ca', 'gb', 'us']
    });

    $('#group').on('change', function() {
        document.forms['navigateByComboForm'].target='_self';
        document.forms['navigateByComboForm'].submit();
    });

    $('#country').on('change', function() {
        document.forms['navigateByComboForm'].target='_self';
        document.forms['navigateByComboForm'].submit();
    });

    $('#sortCol').on('change', function() {
        document.forms['navigateByComboForm'].target='_self';
        document.forms['navigateByComboForm'].submit();
    });

    $('#sortAsc').on('change', function() {
        document.forms['navigateByComboForm'].target='_self';
        document.forms['navigateByComboForm'].submit();
    });

    $("#keywords").on('keyup', function (e) {
        if (e.keyCode == 13) {
            document.forms['searchForm'].submit();
        }
    });

    $("#navigateCleansedTab").on('click', function (e) {
        $('#page_code').val("cleansed");
        document.forms['navigateByComboForm'].target='_self';
        document.forms['navigateByComboForm'].submit();
    });

    $("#navigateDirtyTab").on('click', function (e) {
        $('#page_code').val("dirty");
        document.forms['navigateByComboForm'].target='_self';
        document.forms['navigateByComboForm'].submit();
    });
</script>


</body>
</html>

