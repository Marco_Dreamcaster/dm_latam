</main>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>js/countrySelect/countrySelect.js" charset="UTF-8"></script>
<script>
    $("#country").countrySelect({
        defaultCountry: "<?php echo $countrySearch;?>",
        onlyCountries: ['pa', 'co'],
        //preferredCountries: ['ca', 'gb', 'us']
    });

    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });

</script>


</body>
</html>

