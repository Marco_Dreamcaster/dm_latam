<?php
header('Content-Type: text/html; charset=ISO-8559-1');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="DM Oracle LatAm Laundry">
    <title><?php echo $title; ?></title>

    <link href="<?php echo base_url(); ?>css/dashboard.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">


    <script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>js/gijgo.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>js/gijgo.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>js/letterfx-master/jquery-letterfx.min.js" type="text/javascript"></script>

    <link href="<?php echo base_url(); ?>/css/gijgo.min.css" rel="stylesheet" type="text/css" />


    <script src="<?php echo base_url(); ?>js/tether.min.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap.bundle.min.js"></script>

    <script src="<?php echo base_url(); ?>js/jquery.chained/jquery.chained.js" charset="UTF-8"></script>
    <script src="<?php echo base_url(); ?>js/jquery.chained/jquery.chained.remote.js" charset="UTF-8"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/countrySelect.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>js/letterfx-master/jquery-letterfx.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>fontawesome-free-5.1.0-web/css/all.css">

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url(); ?>/site.webmanifest">
    <link rel="mask-icon" href="<?php echo base_url(); ?>/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

</head>
<body>


<nav class="navbar sticky-top bg-TKE navbar-expand-lg bg-dark navbar-light col-md-12 ml-sm-auto col-lg-12 animate-area">
    <div class="col-md-12 collapse navbar-collapse" id="navbarSupportedContent">
        <div class="col-md-1 col-sm-0 d-flex justify-content-start" >
            <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>img/laundryLogo.png" class="logo"/></a>
        </div>
        <div class="col-md-11 col-sm-12 d-flex pt-3 justify-content-end" >
            <div class="ml-2">
                <form id="navigateByComboForm" action="<?php echo base_url(); ?>Admin/navigateByCombo" method="post" name="groupForm" class="form-inline my-2 my-lg-0" accept-charset="ISO-8859-1">
                    <div class="input-group mb-0 mr-1 mt-1">
                        <div class="input-group-prepend">
                            <a id="navigateCleansedTab" name="navigateCleansedTab" class="btn btn-primary" title="new tab with cleansed"><i class="fas text-white fa-industry fa-sm fa-fw"></i></a>
                            <a id="navigateDirtyTab" name="navigateDirtyTab" class="btn btn-primary" title="new tab with dirty"><i class="fas text-white fa-trash-alt fa-sm fa-fw"></i></a>
                        </div>
                        <select class="custom-select" id="group" name="group">
                            <option <?php if($sessionGroup=='Projects'){ echo 'selected';}?> value="Projects">Projects</option>
                            <option <?php if($sessionGroup=='Repairs'){ echo 'selected';}?> value="Repairs">Repairs</option>
                            <option <?php if($sessionGroup=='Customers'){ echo 'selected';}?> value="Customers">Customers</option>
                            <option <?php if($sessionGroup=='Vendors'){ echo 'selected';}?> value="Vendors">Vendors</option>
                            <option <?php if($sessionGroup=='Inventory'){ echo 'selected';}?> value="Inventory">Inventory</option>
                            <option <?php if($sessionGroup=='Setup'){ echo 'selected';}?> value="Setup">Setup</option>
                            <option <?php if($sessionGroup=='GLMenu'){ echo 'selected';}?> value="GLMenu">GL</option>
                        </select>
                        <input type="hidden" id="page_code" name="page_code" />
                    </div>
                    <div class="mr-1 mt-1">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <a class="btn btn-primary"  href="<?php echo base_url(); ?>KPI/<?php echo strtoupper($sessionCountry); ?>" target="_self" title="go to KPI for selected country"><i class="fas fa-chart-line fa-sm fa-fw"></i></a>
                                <a class="btn btn-primary"  href="<?php echo base_url(); ?>Setup/<?php echo strtoupper($sessionCountry); ?>" target="_self" title="go to Setup for selected country"><i class="fas fa-sliders-h fa-sm fa-fw"></i></a>
                            </div>
                            <input class="custom-select-country" type="text" id="country" name="country" readonly="true" />
                            <input type="hidden" id="country_code" name="country_code" />
                        </div>
                    </div>
                    <div class="mr-1 mt-1">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button type="submit" class="btn btn-primary"><i class="fas fa-sort-alpha-up fa-sm fa-fw"></i></button>
                            </div>
                            <select class="custom-select" id="sortCol" name="sortCol">
                                <option <?php if($sessionSelCol=='lastUpdated'){ echo 'selected';}?> value="lastUpdated">Last Updated</option>
                                <option <?php if($sessionSelCol=='name'){ echo 'selected';}?> value="name">Name</option>
                                <option <?php if($sessionSelCol=='id'){ echo 'selected';}?> value="id">ID</option>
                                <option <?php if($sessionSelCol=='error'){ echo 'selected';}?> value="error">Error</option>
                                <option <?php if($sessionSelCol=='ranking'){ echo 'selected';}?> value="ranking">Ranking</option>
                            </select>
                            <div class="custom-control custom-checkbox" style="margin-left:10px; margin-top:5px;">
                                <input type="checkbox" class="custom-control-input" id="sortAsc" name="sortAsc" <?php if($sessionSelAsc){ echo 'checked';}?>>
                                <label class="custom-control-label" style="color:#FFFFFF;" for="sortAsc">ASC</label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="ml-3 mt-1">
                <form id="searchForm" action="<?php echo base_url(); ?>Search" method="post" name="groupForm" class="form-inline my-2 my-lg-0" accept-charset="ISO-8859-1">
                    <div>
                        <div class="form-group mx-sm-8 mb-0">
                            <label class="sr-only" for="inlineFormInputGroup">keywords</label>
                            <div class="input-group mb-0">
                                <div class="input-group-prepend">
                                    <button type="submit" class="btn btn-primary"><i class="fas fa-search fa-sm fa-fw"></i> %</button>
                                </div>
                                <input type="text" class="form-control form-control-sm p-2" id="keywords" name="keywords" size="20" placeholder="">
                            </div>
                        </div>                                                                                                                  &nbsp;
                    </div>
                </form>
            </div>
        </div>

    </div>

</nav>

<div class="container-fluid">
    <div class="row">
    </div>
    <div class="row">
    <main role="main" class="col-md-12 ml-sm-auto col-lg-12 pt-3 px-4">