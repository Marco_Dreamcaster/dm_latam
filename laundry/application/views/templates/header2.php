<?php
header('Content-Type: text/html; charset=ISO-8859-1');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="DM Oracle LatAm Laundry">
    <title><?php echo $title; ?></title>

    <link href="<?php echo base_url(); ?>css/dashboard.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js" integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js" integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.3/combined/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.3/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />


    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>

    <script src="<?php echo base_url(); ?>js/jquery.chained/jquery.chained.js" charset="UTF-8"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/countrySelect.min.css">

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url(); ?>/site.webmanifest">
    <link rel="mask-icon" href="<?php echo base_url(); ?>/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

</head>
<body>


<nav class="navbar sticky-top bg-TKE navbar-expand-lg bg-dark navbar-light col-md-12 ml-sm-auto col-lg-12">
    <a class="col-sm-1 col-md-1 mr-0 text-center" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>img/laundryLogo.png" height="60px" width="78px"/></a>
    <div class="col-md-11 collapse navbar-collapse mt-2" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto" style="">
        </ul>
        <div class="dropdown">
            <button class="btn btn-outline btn-success dropdown-toggle" type="button" id="idlMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Inspect IDL
            </button>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="<?php echo base_url(); ?>IDL">Stats</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>IDL/groups">Groups</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>IDL/descriptors">Descriptors</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>IDL/fields">Fields</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?php echo base_url(); ?>Debug">Debug</a>
            </div>
        </div>
        &nbsp;
        <a href="<?= base_url();?>Admin/logout"><button class="btn btn-primary my-2 my-sm-0">Logout</button></a>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <main role="main" class="col-md-12 ml-sm-auto col-lg-12 pt-3 px-4">
