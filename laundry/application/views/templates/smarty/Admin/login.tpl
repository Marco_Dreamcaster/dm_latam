<?php
header('Content-Type: text/html; charset=ISO-8559-1');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="DM Oracle LatAm Laundry">
    <title>{$title}</title>

    <link href="{base_url()}css/login.css" rel="stylesheet">
    <link href="{base_url()}css/dashboard.css" rel="stylesheet">
    <link href="{base_url()}css/bootstrap.min.css" rel="stylesheet">


    <script src="{base_url()}js/jquery.min.js"></script>

    <script src="{base_url()}js/tether.min.js"></script>
    <script src="{base_url()}js/bootstrap.min.js"></script>
    <script src="{base_url()}js/bootstrap.bundle.min.js"></script>

    <link rel="stylesheet" href="{base_url()}css/flag-icon.min.css">
    <link rel="stylesheet" href="{base_url()}css/countrySelect.min.css">
    <link rel="stylesheet" href="{base_url()}fontawesome-free-5.1.0-web/css/all.css">

    <link rel="apple-touch-icon" sizes="180x180" href="{base_url()}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{base_url()}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{base_url()}/favicon-16x16.png">
    <link rel="manifest" href="{base_url()}/site.webmanifest">
    <link rel="mask-icon" href="{base_url()}/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

</head>


<body>
<div class="container">
    <div class="d-flex justify-content-center h-100">
        <div class="card">
            <div class="card-body">
                <form action="{base_url()}Admin/authenticate" method="post">
                    <input id="redirectTo" name="redirectTo" type="hidden" value="{$redirectTo}">
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-id-card-alt fa-sm fa-fw"></i></span>
                        </div>
                        <input id="user" name="user" type="text" class="form-control" value="{$user}" placeholder="8id" required maxlength="8">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key fa-sm fa-fw"></i></span>
                        </div>
                        <input id="password" name="password" type="password" class="form-control" placeholder="password" required maxlength="15">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Login" class="btn btn-primary float-right">
                    </div>
                    <small id="errorUser" name="errorUser" class="form-text text-danger">&nbsp;{$errorUser}</small>
                    <small id="errorPassword" name="errorPassword" class="form-text text-danger">&nbsp;{$errorPassword}</small>
                    <small id="errorRedirectTo" name="errorRedirectTo" class="form-text text-danger">&nbsp;{$errorRedirectTo}</small>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>