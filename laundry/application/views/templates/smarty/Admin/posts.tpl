<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><i class="fas fa-keyboard fa-lg fa-fw mr-2"></i>Development Change Log</h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}IDL/groups" data-toggle="tooltip" data-placement="top" title="view all IDLs by country"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Debug" data-toggle="tooltip" data-placement="top" title="debug and audit logs"><i class="fas fa-bug fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>
<br/>
<br/>
<table id="postTable" class="table table-hover table-sm">
    <thead>
    <tr>
        <th scope="col">When</th>
        <th scope="col">Title</th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$posts item=p name=availablePosts}
        <tr id="groupAction{$smarty.foreach.foo.index}">
            <td class="p-2">{$p.date}</td>
            <td class="p-2">
                <a href="{base_url()}Admin/displayPost/{$p.hash}">{$p.title}</a>
            </td>
        </tr>
    {/foreach}
    </tbody>
</table>





