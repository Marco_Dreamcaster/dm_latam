<script src="{base_url()}js/chartJS/Chart.bundle.js" charset="UTF-8"></script>
<script src="{base_url()}js/chartJS/utils.js" charset="UTF-8"></script>
<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><i class="fas fa-lg fa-dollar-sign fa-fw m-2 text-stage"></i>&nbsp;<span class="m-2"><span class="flag-icon flag-icon-{strtolower($country)}"></span>&nbsp;{$title}</span></h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Setup/{$country}" data-toggle="tooltip" data-placement="top" title="go to Setup for {$country}"><i class="fas fa-sliders-h fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}GLTables/{$country}" data-toggle="tooltip" data-placement="top" title="go to GL Tables for {$country}">GL Tables&nbsp;<i class="fas fa-dollar-sign fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}GLParams/{$country}" data-toggle="tooltip" data-placement="top" title="go to GL Params for {$country}">GL Params&nbsp;<i class="fas fa-dollar-sign fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>

<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade active show" id="pills-summary" role="tabpanel" aria-labelledby="pills-summary-tab">
        <div class="row mt-3 p-1">
            <div class="col-12 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <form class="form-inline justify-content-center">
                            <div class="m-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Last SIGLA Snapshot</div>
                                    </div>
                                    <input class="p-2" type="text" id="snapshotDate" name="snapshotDate" readonly="true" value="{$snapshotDate}"/>
                                </div>
                            </div>
                        </form>
                        <!--
                        <form class="form-inline justify-content-center">
                            <div class="m-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Last Parameter Change</div>
                                    </div>
                                    <input class="p-2" type="text" id="parameterChangeEvent" name="parameterChangeEvent" readonly="true" value="{$parameterChangeEvent}"/>
                                </div>
                            </div>
                            <div class="m-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">by</div>
                                    </div>
                                    <input class="p-2" type="text" id="parameterChangeResponsible" name="parameterChangeResponsible" readonly="true" value="{$parameterChangeResponsible}"/>
                                </div>
                            </div>
                            <div class="m-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">when</div>
                                    </div>
                                    <input class="p-2" type="text" id="parameterChangeDate" name="parameterChangeDate" readonly="true" value="{$parameterChangeDate}"/>
                                </div>
                            </div>
                            -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<br/>
<br/>



