<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><span class="flag-icon flag-icon-{strtolower($country)} mt-2"></span>&nbsp;<span class="mt-2">{$title}</span>&nbsp;</h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Transactional/{$country}" data-toggle="tooltip" data-placement="top" title="go to transactional for {$country}"><i class="fas fa-money-bill-wave fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-success" target="_blank" href="{base_url()}scripts/00.COM/transactional/00.Contabilidad/01.IDL/TKE_LA_IDL_FD_01_GL_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="GL specifications"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>

<div class="navbar navbar-expand-lg navbar-light bg-light col-md-4 ml-sm-auto col-lg-12">
    <div class="tabbable col-lg-12">
        <ul class="nav nav-pills mt-2 justify-content-center">
            <li class="nav-item"><a class="nav-link" href="{base_url()}GLTables/{$country}">Tablas</a></li>
            <li class="nav-item"><a class="nav-link" href="{base_url()}GLParams/{$country}">Params</a></li>
            <li class="nav-item active"><a class="nav-link {if $topActive=='diagnostics'}active{/if} show" href="#diagnostics" data-toggle="tab">Diagnostics</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane {if $topActive=='diagnostics'}active{/if}" id="diagnostics">
                <div class="span8">
                    <div class="tabbable tabs-left">
                        <ul class="nav nav-pills mt-2">
                            <li class="nav-item {if $bottomActive=='DiagnosticsB'}active{/if}"><a class="nav-link" href="#DiagnosticsB" data-toggle="tab">Diagnostics Balance</a></li>
                            <li class="nav-item {if $bottomActive=='DiagnosticsR'}active{/if}"><a class="nav-link" href="#DiagnosticsR" data-toggle="tab">Diagnostics Result</a></li>
                            <li class="nav-item {if $bottomActive=='MS'}active{/if}"><a class="nav-link" href="#MS" data-toggle="tab">Migration Schedule</a></li>

                            <li class="nav-item {if $bottomActive=='AS'}active{/if}"><a class="nav-link" href="#AS" data-toggle="tab">Account Search</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane {if $bottomActive=='AS'}active{/if} mt-4" id="AS">
                                <div class="mt-2 mb-4">
                                    <form class="inline align-top" action="{base_url()}/GLParams/searchAccount/{$country}" method="POST">
                                        <div class="form-row align-items-center">
                                            <div class="col-sm-3 my-1">
                                                <div class="input-group">
                                                <div class="input-group-text">Account Sigla</div>
                                                    <input type="text" class="form-control" id="ctaSigla" name="ctaSigla" value="{$ctaSigla}" maxlength="8" placeholder="Account Sigla" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 my-1">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">Start Date</div>
                                                        <input class="form-control" id="desde" name="desde" aria-describedby="desdeHelp"/>
                                                        <script>
                                                            $('#desde').datepicker({
                                                                uiLibrary: 'bootstrap4',
                                                                format:'dd-mm-yyyy',
                                                                value: '{$desde|date_format:"%d-%m-%Y"}'
                                                            });
                                                        </script>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 my-1">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">End Date</div>
                                                        <input class="form-control" id="hasta" name="hasta" aria-describedby="hastaHelp"/>
                                                        <script>
                                                            $('#hasta').datepicker({
                                                                uiLibrary: 'bootstrap4',
                                                                format:'dd-mm-yyyy',
                                                                value: '{$hasta|date_format:"%d-%m-%Y"}'
                                                            });
                                                        </script>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-3 my-1">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <button type="submit" id="btnAS" class="btn btn-primary"><i class="fas fa-plus-circle fa-sm fa-fw text-white"></i>  Search  </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <table id="ccTableAS" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Cta. Sigla</th>
                                        <th scope="col">Diario</th>
                                        <th scope="col">Descrip</th>
                                        <th scope="col">Control</th>
                                        <th scope="col">Fecha L.</th>
                                        <th scope="col">Fecha R.</th>
                                        <th scope="col">Debe</th>
                                        <th scope="col">Haber</th>
                                        <th scope="col">Importe</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        {foreach from=$AS item=c}
                                            <tr>
                                                <td>{$c.CtaSigla}</td>
                                                <td>{$c.Diario}</td>
                                                <td>{$c.Descrip}</td>
                                                <td>{$c.Control}</td>
                                                <td>{$c.Fecha}</td>
                                                <td>{$c.FechaReg}</td>
                                                <td>{$c.Debe|number_format:2}</td>
                                                <td>{$c.Haber|number_format:2}</td>
                                                <td>{$c.Importe|number_format:2}</td>
                                            </tr>
                                        {/foreach}
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th scope="col">Cta. Sigla</th>
                                            <th scope="col">Diario</th>
                                            <th scope="col">Descrip</th>
                                            <th scope="col">Control</th>
                                            <th scope="col">Fecha L.</th>
                                            <th scope="col">Fecha R.</th>
                                            <th scope="col">Debe</th>
                                            <th scope="col">Haber</th>
                                            <th scope="col">Importe</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTableAS').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>

                            <div class="tab-pane {if $bottomActive=='MS'}active{/if} mt-4" id="MS">
                                <table id="ccTableMS" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Migration Order</th>
                                        <th scope="col">Country</th>
                                        <th scope="col">Project</th>
                                        <th scope="col">Service</th>
                                        <th scope="col">Repair</th>
                                        <th scope="col">Customer</th>
                                        <th scope="col">Vendor</th>
                                        <th scope="col">Inventory</th>
                                        <th scope="col">GL FC</th>
                                        <th scope="col">GL FG</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$MS item=c}
                                        <tr>
                                            <td>{$c.ordermigration}</td>
                                            <td>{$c.country}</td>
                                            <td>{$c.project}</td>
                                            <td>{$c.service}</td>
                                            <td>{$c.repair}</td>
                                            <td>{$c.customer}</td>
                                            <td>{$c.vendor}</td>
                                            <td>{$c.inventory}</td>
                                            <td>{$c.glfc}</td>
                                            <td>{$c.glfg}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Migration Order</th>
                                        <th scope="col">Country</th>
                                        <th scope="col">Project</th>
                                        <th scope="col">Service</th>
                                        <th scope="col">Repair</th>
                                        <th scope="col">Customer</th>
                                        <th scope="col">Vendor</th>
                                        <th scope="col">Inventory</th>
                                        <th scope="col">GL FC</th>
                                        <th scope="col">GL FG</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTableMS').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>
                            <div class="tab-pane {if $bottomActive=='DiagnosticsB'}active{/if} mt-4" id="DiagnosticsB">
                                <table id="ccTableDiagnosticsB" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Cuenta Sigla</th>
                                        <th scope="col">Saldo Ant.</th>
                                        <th scope="col">Debe</th>
                                        <th scope="col">Haber</th>
                                        <th scope="col">Saldo</th>
                                        <th scope="col">Laundry</th>
                                        <th scope="col">Dif.</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$DiagnosticsB item=c}
                                        <tr>
                                            <td>{$c.cta}</td>
                                            <td>{$c.saldoant}</td>
                                            <td>{$c.debe}</td>
                                            <td>{$c.haber}</td>
                                            <td>{$c.saldo}</td>
                                            <td>{$c.laundry}</td>
                                            <td>{$c.dif}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Cuenta Sigla</th>
                                        <th scope="col">Saldo Ant.</th>
                                        <th scope="col">Debe</th>
                                        <th scope="col">Haber</th>
                                        <th scope="col">Saldo</th>
                                        <th scope="col">Laundry</th>
                                        <th scope="col">Dif.</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTableDiagnosticsB').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>
                            <div class="tab-pane {if $bottomActive=='DiagnosticsR'}active{/if} mt-4" id="DiagnosticsR">
                                <table id="ccTableDiagnosticsR" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Cuenta Sigla</th>
                                        <th scope="col">Saldo Ant.</th>
                                        <th scope="col">Debe</th>
                                        <th scope="col">Haber</th>
                                        <th scope="col">Saldo</th>
                                        <th scope="col">Laundry</th>
                                        <th scope="col">Dif.</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$DiagnosticsR item=c}
                                        <tr>
                                            <td>{$c.cta}</td>
                                            <td>{$c.saldoant}</td>
                                            <td>{$c.debe}</td>
                                            <td>{$c.haber}</td>
                                            <td>{$c.saldo}</td>
                                            <td>{$c.laundry}</td>
                                            <td>{$c.dif}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Cuenta Sigla</th>
                                        <th scope="col">Saldo Ant.</th>
                                        <th scope="col">Debe</th>
                                        <th scope="col">Haber</th>
                                        <th scope="col">Saldo</th>
                                        <th scope="col">Laundry</th>
                                        <th scope="col">Dif.</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTableDiagnosticsR').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<br/>
<br/>
<br/>
