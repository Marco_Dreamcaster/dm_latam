<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><i class="fas fa-lg fa-dollar-sign fa-fw m-2 text-stage"></i>&nbsp;<span class="m-2"><span class="flag-icon flag-icon-{strtolower($country)}"></span>&nbsp;{$title}</span></h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Setup/{$country}" data-toggle="tooltip" data-placement="top" title="go to Setup for {$country}"><i class="fas fa-sliders-h fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}GLMenu/{$country}" data-toggle="tooltip" data-placement="top" title="go to GL Control Center for {$country}">GL Control Center&nbsp;<i class="fas fa-dollar-sign fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}GLTables/{$country}" data-toggle="tooltip" data-placement="top" title="go to GL Tables for {$country}">GL Tables&nbsp;<i class="fas fa-dollar-sign fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>

<div class="navbar navbar-expand-lg navbar-light bg-light col-md-4 ml-sm-auto col-lg-12">
    <div class="tabbable col-lg-12">
        <ul class="nav nav-pills mt-2 justify-content-center">
            <li class="nav-item"><a class="nav-link" href="{base_url()}GLTables/{$country}">Tablas</a></li>
            <li class="nav-item"><a class="nav-link {if $topActive=='params'}active{/if}" href="#params" data-toggle="tab">Params</a></li>
            <li class="nav-item"><a class="nav-link" href="{base_url()}GLDiag/{$country}">Analysis &amp; Diagnostics</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane {if $topActive=='params'}active{/if}" id="params">
                <div class="span8">
                    <div class="tabbable justify-content-center">
                        <ul class="nav nav-pills mt-2">

                            <li class="nav-item active"><a class="nav-link {if $bottomActive=='CC'}active{/if}" href="#CC" data-toggle="tab">CC (Val. Rules)</a></li>
                            <li class="nav-item"><a class="nav-link {if $bottomActive=='CCFixed'}active{/if}" href="#CCFixed" data-toggle="tab">CC (Fixed)</a></li>

                            <li class="nav-item"><a class="nav-link {if $bottomActive=='LOB'}active{/if}" href="#LOB" data-toggle="tab">LOB (Val. Rules)</a></li>
                            <li class="nav-item"><a class="nav-link {if $bottomActive=='LOBFixed'}active{/if}" href="#LOBFixed" data-toggle="tab">LOB (Fixed)</a></li>

                            <li class="nav-item"><a class="nav-link {if $bottomActive=='PLDePara'}active{/if}" href="#PLDePara" data-toggle="tab">PL (Sigla x Oracle)</a></li>

                            <li class="nav-item"><a class="nav-link {if $bottomActive=='PL'}active{/if}" href="#PL" data-toggle="tab">PL (Val. Rules)</a></li>
                            <li class="nav-item"><a class="nav-link {if $bottomActive=='PLFixed'}active{/if}" href="#PLFixed" data-toggle="tab">PL (Fixed)</a></li>

                            <li class="nav-item"><a class="nav-link {if $bottomActive=='IC'}active{/if}" href="#IC" data-toggle="tab">IC (Val. Rules)</a></li>

                            <li class="nav-item"><a class="nav-link {if $bottomActive=='Exclusion'}active{/if}" href="#Exclusion" data-toggle="tab">Exclusion (Val. Rules)</a></li>

                            <li class="nav-item"><a class="nav-link {if $bottomActive=='CR'}active{/if}" href="#CR" data-toggle="tab">Check Rules</a></li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane {if $bottomActive=='CC'}active{/if} mt-4" id="CC">
                                <div class="mt-2 mb-4">
                                    <form class="inline align-top" action="{base_url()}/GLParams/addGLCostCenter/{$country}" method="POST">
                                        <div class="form-row align-items-center">
                                            <div class="col-sm-2 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="ctaOracle" name="ctaOracle" value="{$ctaOracle}" maxlength="8" placeholder="Cuenta Oracle" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="desde" name="desde"  value="{$desde}" maxlength="4" placeholder="Desde" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="hast" name="hasta" value="{$hasta}" maxlength="4" placeholder="Hasta" required/>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" id="btnCC" class="btn btn-primary"><i class="fas fa-plus-circle fa-sm fa-fw text-white"></i> Nuevo</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <table id="ccTable" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Cuenta Oracle</th>
                                        <th scope="col">Desde</th>
                                        <th scope="col">Hasta</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$CCs item=c}
                                        <tr>
                                            <td><a href="{base_url()}GLParams/removeGLCostCenter/{$c.id}"><i class="fas fa-minus-circle fa-sm fa-fw text-danger"></i></a>&nbsp; {$c.ctaOracle}</td>
                                            <td>{$c.desde}</td>
                                            <td>{$c.hasta}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Cuenta Oracle</th>
                                        <th scope="col">Desde</th>
                                        <th scope="col">Hasta</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTable').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>
                            <div class="tab-pane {if $bottomActive=='CCFixed'}active{/if} mt-4" id="CCFixed">
                                <div class="mt-2 mb-4">
                                    <form class="inline align-top" action="{base_url()}/GLParams/addGLCostCenterFixed/{$country}" method="POST">
                                        <div class="form-row align-items-center">
                                            <div class="col-sm-2 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="ctaOracle" name="ctaOracle" value="{$ctaOracle}" maxlength="8" placeholder="Cuenta Oracle" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="costcenter" name="costcenter"  value="{$costcenter}" maxlength="4" placeholder="Cost Center" required/>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" id="btnCCFixed" class="btn btn-primary"><i class="fas fa-plus-circle fa-sm fa-fw text-white"></i> Nuevo</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <table id="ccTableCCFixed" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Cuenta Oracle</th>
                                        <th scope="col">Cost Center</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$CCFixed item=c}
                                        <tr>
                                            <td><a href="{base_url()}GLParams/removeGLCostCenterFixed/{$c.id}"><i class="fas fa-minus-circle fa-sm fa-fw text-danger"></i></a>&nbsp; {$c.ctaOracle}</td>
                                            <td>{$c.costcenter}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Cuenta Oracle</th>
                                        <th scope="col">Cost Center</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTableCCFixed').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>

                            <div class="tab-pane {if $bottomActive=='LOB'}active{/if} mt-4" id="LOB">
                                <div class="mt-2 mb-4">
                                    <form class="inline align-top" action="{base_url()}/GLParams/addGLLOB/{$country}" method="POST">
                                        <div class="form-row align-items-center">
                                            <div class="col-sm-2 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="ctaOracle" name="ctaOracle" value="{$ctaOracle}" maxlength="8" placeholder="Cuenta Oracle" required/>
                                                </div>
</div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="lobParam1" name="lobParam1" value="{$lobParam1}" maxlength="2" placeholder="LOB. Param 1" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="lobParam2" name="lobParam2" value="{$lobParam2}" maxlength="2" placeholder="LOB. Param 2" />
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="lobParam3" name="lobParam3" value="{$lobParam3}" maxlength="2" placeholder="LOB. Param 3" />
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="lobParam4" name="lobParam4" value="{$lobParam4}" maxlength="2" placeholder="LOB. Param 4" />
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="lobParam5" name="lobParam5" value="{$lobParam5}" maxlength="2" placeholder="LOB. Param 5" />
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" id="btnLOB" class="btn btn-primary"><i class="fas fa-plus-circle fa-sm fa-fw text-white"></i> Nuevo</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <table id="ccTableLOB" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Cuenta Oracle</th>
                                        <th scope="col">LOB. Param 1</th>
                                        <th scope="col">LOB. Param 2</th>
                                        <th scope="col">LOB. Param 3</th>
                                        <th scope="col">LOB. Param 4</th>
                                        <th scope="col">LOB. Param 5</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$LOB item=c}
                                        <tr>
                                            <td><a href="{base_url()}GLParams/removeGLLOB/{$c.id}"><i class="fas fa-minus-circle fa-sm fa-fw text-danger"></i></a>&nbsp; {$c.ctaOracle}</td>
                                            <td>{$c.lobParam1}</td>
                                            <td>{$c.lobParam2}</td>
                                            <td>{$c.lobParam3}</td>
                                            <td>{$c.lobParam4}</td>
                                            <td>{$c.lobParam5}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Cuenta Oracle</th>
                                        <th scope="col">LOB. Param 1</th>
                                        <th scope="col">LOB. Param 2</th>
                                        <th scope="col">LOB. Param 3</th>
                                        <th scope="col">LOB. Param 4</th>
                                        <th scope="col">LOB. Param 5</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTableLOB').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>
                            <div class="tab-pane {if $bottomActive=='LOBFixed'}active{/if} mt-4" id="LOBFixed">
                                <div class="mt-2 mb-4">
                                    <form class="inline align-top" action="{base_url()}/GLParams/addGLLOBFixed/{$country}" method="POST">
                                        <div class="form-row align-items-center">
                                            <div class="col-sm-2 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="ctaOracle" name="ctaOracle" value="{$ctaOracle}" maxlength="8" placeholder="Cuenta Oracle" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="lobParam" name="lobParam" value="{$lobParam}" maxlength="2" placeholder="LOB. Param" required/>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" id="btnLOBFixed" class="btn btn-primary"><i class="fas fa-plus-circle fa-sm fa-fw text-white"></i> Nuevo</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <table id="ccTableLOBFixed" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Cuenta Oracle</th>
                                        <th scope="col">LOB. Param</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$LOBFixed item=c}
                                        <tr>
                                            <td><a href="{base_url()}GLParams/removeGLLOBFixed/{$c.id}"><i class="fas fa-minus-circle fa-sm fa-fw text-danger"></i></a>&nbsp; {$c.ctaOracle}</td>
                                            <td>{$c.lobParam}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Cuenta Oracle</th>
                                        <th scope="col">LOB. Param</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTableLOBFixed').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>

                            <div class="tab-pane {if $bottomActive=='PLDePara'}active{/if} mt-4" id="PLDePara">
                                <div class="mt-2 mb-4">
                                    <form class="inline align-top" action="{base_url()}/GLParams/addGLPLDePara/{$country}" method="POST">
                                        <div class="form-row align-items-center">
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="codSigla" name="codSigla" value="{$codSigla}" maxlength="2" placeholder="Codigo Sigla" required/>                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <input type="text" class="form-control" id="codOracle" name="codOracle" value="{$codOracle}" maxlength="2" placeholder="Codigo Oracle" required/>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" id="btnPLDePara" class="btn btn-primary"><i class="fas fa-plus-circle fa-sm fa-fw text-white"></i> Nuevo</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <table id="ccTablePLDePara" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Codigo Sigla</th>
                                        <th scope="col">Codigo Oracle</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$PLDePara item=c}
                                        <tr>
                                            <td><a href="{base_url()}GLParams/removeGLPLDePara/{$c.id}"><i class="fas fa-minus-circle fa-sm fa-fw text-danger"></i></a>&nbsp; {$c.codSigla}</td>
                                            <td>{$c.codOracle}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Codigo Sigla</th>
                                        <th scope="col">Codigo Oracle</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTablePLDePara').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>


                            <div class="tab-pane {if $bottomActive=='PL'}active{/if} mt-4" id="PL">
                                <div class="mt-2 mb-4">
                                    <form class="inline align-top" action="{base_url()}/GLParams/addGLPL/{$country}" method="POST">
                                        <div class="form-row align-items-center">
                                            <div class="col-sm-2 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="ctaOracle" name="ctaOracle" value="{$ctaOracle}" maxlength="8" placeholder="Cuenta Oracle" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="desde" name="desde" value="{$desde}" maxlength="4" placeholder="Desde" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="hasta" name="hasta" value="{$hasta}" maxlength="4" placeholder="Hasta" required/>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" id="btnPL" class="btn btn-primary"><i class="fas fa-plus-circle fa-sm fa-fw text-white"></i> Nuevo</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <table id="ccTablePL" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Cuenta Oracle</th>
                                        <th scope="col">Desde</th>
                                        <th scope="col">Hasta</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$PL item=c}
                                        <tr>
                                            <td><a href="{base_url()}GLParams/removeGLPL/{$c.id}"><i class="fas fa-minus-circle fa-sm fa-fw text-danger"></i></a>&nbsp; {$c.ctaOracle}</td>
                                            <td>{$c.desde}</td>
                                            <td>{$c.hasta}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Cuenta Oracle</th>
                                        <th scope="col">Desde</th>
                                        <th scope="col">Hasta</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTablePL').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>

                            <div class="tab-pane {if $bottomActive=='PLFixed'}active{/if} mt-4" id="PLFixed">
                                <div class="mt-2 mb-4">
                                    <form class="inline align-top" action="{base_url()}/GLParams/addGLPLFixed/{$country}" method="POST">
                                        <div class="form-row align-items-center">
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="lob" name="lob" value="{$lob}" maxlength="2" placeholder="Lob. Param" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <input type="text" class="form-control" id="pl" name="pl" value="{$pl}" maxlength="2" placeholder="Product Line" required/>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" id="btnPLFixed" class="btn btn-primary"><i class="fas fa-plus-circle fa-sm fa-fw text-white"></i> Nuevo</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <table id="ccTablePLFixed" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Lob. Param</th>
                                        <th scope="col">Product Line</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$PLFixed item=c}
                                        <tr>
                                            <td><a href="{base_url()}GLParams/removeGLPLFixed/{$c.id}"><i class="fas fa-minus-circle fa-sm fa-fw text-danger"></i></a>&nbsp; {$c.lob}</td>
                                            <td>{$c.pl}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Lob. Param</th>
                                        <th scope="col">Product Line</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTablePLFixed').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>

                            <div class="tab-pane {if $bottomActive=='IC'}active{/if} mt-4" id="IC">
                                <div class="mt-2 mb-4">
                                    <form class="inline align-top" action="{base_url()}/GLParams/addGLIC/{$country}" method="POST">
                                        <div class="form-row align-items-center">
                                            <div class="col-sm-2 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="ctaSigla" name="ctaSigla" value="{$ctaSigla}" maxlength="8" placeholder="Cuenta Sigla" required/>                                                </div>
                                            </div>
                                            <div class="col-sm-2 my-1">
                                                <input type="text" class="form-control" id="ICCod" name="ICCod" value="{$ICCod}" maxlength="2" placeholder="Codigo Intercompany" required/>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" id="btnIC" class="btn btn-primary"><i class="fas fa-plus-circle fa-sm fa-fw text-white"></i> Nuevo</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <table id="ccTableIC" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Cuenta Sigla</th>
                                        <th scope="col">Codigo Intercompany</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$IC item=c}
                                        <tr>
                                            <td><a href="{base_url()}GLParams/removeGLIC/{$c.id}"><i class="fas fa-minus-circle fa-sm fa-fw text-danger"></i></a>&nbsp; {$c.ctaSigla}</td>
                                            <td>{$c.ICCod}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Cuenta Sigla</th>
                                        <th scope="col">Codigo Intercompany</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTableIC').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>

                            <div class="tab-pane {if $bottomActive=='CR'}active{/if} mt-4" id="CR">
                                <div class="mt-2 mb-4">
                                    <form class="inline align-top" action="{base_url()}/GLParams/addGLCheckRules/{$country}" method="POST">
                                        <div class="form-row align-items-center">
                                            <div class="col-sm-2 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="ctaOracle" name="ctaOracle" value="{$ctaOracle}" maxlength="8" placeholder="Cuenta Oracle" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="lob" name="lob" value="{$lob}" maxlength="2" placeholder="Line Of Business" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="cc" name="cc"  value="{$cc}" maxlength="4" placeholder="Cost Center" required/>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" id="btnCCFixed" class="btn btn-primary"><i class="fas fa-plus-circle fa-sm fa-fw text-white"></i> Nuevo</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <table id="ccTableCR" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Cuenta Oracle</th>
                                        <th scope="col">Cost Center</th>
                                        <th scope="col">Line Of Business</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$CR item=c}
                                        <tr>
                                            <td><a href="{base_url()}GLParams/removeGLCheckRules/{$c.id}"><i class="fas fa-minus-circle fa-sm fa-fw text-danger"></i></a>&nbsp; {$c.ctaOracle}</td>
                                            <td>{$c.cc}</td>
                                            <td>{$c.lob}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Cuenta Oracle</th>
                                        <th scope="col">Cost Center</th>
                                        <th scope="col">Line Of Business</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTableCR').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>

                            <div class="tab-pane {if $bottomActive=='Exclusion'}active{/if} mt-4" id="Exclusion">
                                <div class="mt-2 mb-4">
                                    <form class="inline align-top" action="{base_url()}/GLParams/addGLExclusionCuentas/{$country}" method="POST">
                                        <div class="form-row align-items-center">
                                            <div class="col-sm-2 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="ctaSigla" name="ctaSigla" value="{$ctaSigla}" maxlength="8" placeholder="Cuenta Sigla" required/>                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" id="btnExclusion" class="btn btn-primary"><i class="fas fa-plus-circle fa-sm fa-fw text-white"></i> Nuevo</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <table id="ccTableExclusion" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Cuenta Sigla</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$Exclusion item=c}
                                        <tr>
                                            <td><a href="{base_url()}GLParams/removeGLExclusionCuentas/{$c.id}"><i class="fas fa-minus-circle fa-sm fa-fw text-danger"></i></a>&nbsp; {$c.ctaSigla}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Cuenta Sigla</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTableExclusion').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<br/>
<br/>
<br/>
