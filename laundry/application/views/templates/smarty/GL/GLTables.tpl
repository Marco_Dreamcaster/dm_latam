<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><i class="fas fa-lg fa-dollar-sign fa-fw m-2 text-stage"></i>&nbsp;<span class="m-2"><span class="flag-icon flag-icon-{strtolower($country)}"></span>&nbsp;{$title}</span></h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Setup/{$country}" data-toggle="tooltip" data-placement="top" title="go to Setup for {$country}"><i class="fas fa-sliders-h fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}GLMenu/{$country}" data-toggle="tooltip" data-placement="top" title="go to GL Control Center for {$country}">GL Control Center&nbsp;<i class="fas fa-dollar-sign fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}GLParams/{$country}" data-toggle="tooltip" data-placement="top" title="go to GL Params for {$country}">GL Params&nbsp;<i class="fas fa-dollar-sign fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>

<div class="navbar navbar-expand-lg navbar-light bg-light col-md-4 ml-sm-auto col-lg-12">
    <div class="tabbable col-lg-12">
        <ul class="nav nav-pills mt-2 justify-content-center">
            <li class="nav-item active"><a class="nav-link {if $topActive=='tablas'}active{/if} show" href="{base_url()}GLTables/{$country}">Tablas</a></li>
            <li class="nav-item"><a class="nav-link" href="{base_url()}GLParams/{$country}">Params</a></li>
            <li class="nav-item"><a class="nav-link" href="{base_url()}GLDiag/{$country}">Analysis &amp; Diagnostics</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane {if $topActive=='tablas'}active{/if}" id="tablas">
                <div class="span8">
                    <div class="tabbable">
                        <ul class="nav nav-pills mt-2">
                            <li class="nav-item active"><a class="nav-link {if $bottomActive=='Cuentas'}active{/if}" href="#Cuentas" data-toggle="tab">Accounts</a></li>
                            <li class="nav-item"><a class="nav-link {if $bottomActive=='TipCos'}active{/if}" href="#TipCos" data-toggle="tab">TipCos</a></li>
                            <li class="nav-item"><a class="nav-link {if $bottomActive=='TipCont'}active{/if}" href="#TipCont" data-toggle="tab">TipoCont</a></li>
                            <li class="nav-item"><a class="nav-link {if $bottomActive=='Diarios'}active{/if}" href="#Diarios" data-toggle="tab">Diarios</a></li>
                            <li class="nav-item"><a class="nav-link {if $bottomActive=='CostoLocal'}active{/if}" href="#CostoLocal" data-toggle="tab">Costo Local</a></li>
                            <li class="nav-item"><a class="nav-link {if $bottomActive=='Monedas'}active{/if}" href="#Monedas" data-toggle="tab">Monedas</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane  {if $bottomActive=='Cuentas'}active{/if} mt-4" id="Cuentas">
                                <div class="mt-2 mb-4">
                                    <form id="updateGLCuentas" class="inline align-top" action="{base_url()}GLParams/updateGLCuentas/{$country}" method="POST">
                                        <input type="hidden" id="updateGLCuentas_id" name="id"/>

                                        <div class="form-row align-items-center">
                                            <div class="col-sm-2 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLCuentas_ctaSigla" name="ctaSigla" disabled placeholder="Cuenta Sigla" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLCuentas_description" name="descrip_cuentas" disabled placeholder="Descripcion" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-2 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLCuentas_ctaOracle" name="ctaOracle" value="{$ctaOracle}" maxlength="8" required placeholder="Cuenta Oracle"/>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" id="btnCuentas" class="btn btn-primary"><i class="fas fa-check-circle fa-sm fa-fw text-white"></i> Cambiar</button>
                                            </div>
                                            <small id="errorCuentas" class="form-text text-danger">{$errorCuentas}</small>
                                        </div>
                                    </form>
                                </div>
                                <script>
                                    function onClickUpdateForm(id, ctaSigla, description, ctaOracle){
                                        $('input[id="updateGLCuentas_id"]').val(id);
                                        $('input[id="updateGLCuentas_ctaSigla"]').val(ctaSigla);
                                        $('input[id="updateGLCuentas_description"]').val(description);
                                        $('input[id="updateGLCuentas_ctaOracle"]').val(ctaOracle);
                                        $('small[id="errorCuentas"]').html("");
                                    }
                                </script>
                                <table id="ccTableCuentas" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Cuenta Sigla</th>
                                        <th scope="col">Descripcion</th>
                                        <th scope="col">Cuenta Oracle</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$Cuentas item=c}
                                        <tr onclick="onClickUpdateForm({$c.id},'{$c.cta|trim}','{$c.descrip|trim}','{$c.ctaOracle|trim}')">
                                            <td><a href="{base_url()}GLParams/clearGLCuentas/{$c.id}/{$country}"><i class="fas fa-eraser fa-sm fa-fw text-danger"></i></a>&nbsp; {$c.cta}</td>
                                            <td>{$c.descrip}</td>
                                            <td>{$c.ctaOracle}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Cuenta Sigla</th>
                                        <th scope="col">Descripcion</th>
                                        <th scope="col">Cuenta Oracle</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTableCuentas').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>
                            <div class="tab-pane {if $bottomActive=='TipCos'}active{/if} mt-4" id="TipCos">
                                <div class="mt-2 mb-4">
                                    <form id="updateGLTipCos" class="inline align-top" action="{base_url()}GLParams/updateGLTipCos/{$country}" method="POST">
                                        <input type="hidden" id="updateGLTipCos_id" name="id"/>

                                        <div class="form-row align-items-center">
                                            <div class="col-sm-2 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLTipCos_tipcos" name="tipcos" disabled placeholder="Tip. Cos." required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLTipCos_descrip" name="descrip_tipcos" disabled placeholder="Descripcion" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-2 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLTipCos_tipcosOracle" name="tipcosOracle" value="{$tipcosOracle}" maxlength="4" required placeholder="Tip. Cos. Oracle"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLTipCos_expenditure" name="expenditure" value="{$expenditure}" maxlength="200" placeholder="Expenditure"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLTipCos_clasification" name="clasification" value="{$clasification}" maxlength="200" placeholder="Clasification"/>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" id="btnTipCos" class="btn btn-primary"><i class="fas fa-check-circle fa-sm fa-fw text-white"></i> Cambiar</button>
                                            </div>
                                            <small id="errorTipCos" class="form-text text-danger">{$errorTipCos}</small>
                                        </div>
                                    </form>
                                </div>
                                <script>
                                    function onClickUpdateFormTipCos(id, tipcos, descrip, tipcosOracle, expenditure, clasificacion){
                                        $('input[id="updateGLTipCos_id"]').val(id);
                                        $('input[id="updateGLTipCos_tipcos"]').val(tipcos);
                                        $('input[id="updateGLTipCos_descrip"]').val(descrip);
                                        $('input[id="updateGLTipCos_tipcosOracle"]').val(tipcosOracle);
                                        $('input[id="updateGLTipCos_expenditure"]').val(expenditure);
                                        $('input[id="updateGLTipCos_clasificacion"]').val(clasificacion);
                                        $('small[id="errorTipCos"]').html("");
                                    }
                                </script>
                                <table id="ccTableTipCos" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Tip. Cos.</th>
                                        <th scope="col">Descripcion</th>
                                        <th scope="col">Tip. Cos. Oracle</th>
                                        <th scope="col">Expenditure</th>
                                        <th scope="col">Clasificacion</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$TipCos item=c}
                                        <tr onclick="onClickUpdateFormTipCos({$c.id},'{$c.tipcos|trim}','{$c.descrip|trim}','{$c.tipcosOracle|trim}', '{$c.expenditure|trim}', '{$c.clasificacion|trim}')">
                                            <td><a href="{base_url()}GLParams/clearGLTipCos/{$c.id}/{$country}"><i class="fas fa-eraser fa-sm fa-fw text-danger"></i></a>&nbsp; {$c.tipcos}</td>
                                            <td>{$c.descrip}</td>
                                            <td>{$c.tipcosOracle}</td>
                                            <td>{$c.expenditure}</td>
                                            <td>{$c.clasificacion}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Tip. Cos.</th>
                                        <th scope="col">Descripcion</th>
                                        <th scope="col">Tip. Cos. Oracle</th>
                                        <th scope="col">Expenditure</th>
                                        <th scope="col">Clasificacion</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTableTipCos').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>
                            <div class="tab-pane {if $bottomActive=='TipoCont'}active{/if} mt-4" id="TipCont">
                                <div class="mt-2 mb-4">
                                    <form id="updateGLTipoCont" class="inline align-top" action="{base_url()}GLParams/updateGLTipoCont/{$country}" method="POST">
                                        <input type="hidden" id="updateGLTipoCont_id" name="id"/>

                                        <div class="form-row align-items-center">
                                            <div class="col-sm-2 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLTipoCont_tipo" name="tipcos" disabled placeholder="Tipo Cont. Sigla" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLTipoCont_descrip" name="descrip_tipocont" disabled placeholder="Descripcion" required/>
                                                </div>
                                                <small id="errorDescripTipoCont" class="form-text text-danger">{$errorDescripTipoCont}</small>
                                            </div>
                                            <div class="col-sm-2 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLTipoCont_tipoOracle" name="tipoOracle" value="{$tipoOracle}" maxlength="2" required placeholder="Tipo Cont. Oracle"/>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" id="btnTipoCont" class="btn btn-primary"><i class="fas fa-check-circle fa-sm fa-fw text-white"></i> Cambiar</button>
                                            </div>
                                            <small id="errorTipoCont" class="form-text text-danger">{$errorTipoCont}</small>
                                        </div>
                                    </form>
                                </div>
                                <script>
                                    function onClickUpdateFormTipoCont(id, tipo, descrip, tipoOracle){
                                        $('input[id="updateGLTipoCont_id"]').val(id);
                                        $('input[id="updateGLTipoCont_tipo"]').val(tipo);
                                        $('input[id="updateGLTipoCont_descrip"]').val(descrip);
                                        $('input[id="updateGLTipoCont_tipoOracle"]').val(tipoOracle);
                                        $('small[id="errorTipoCont"]').html("");
                                    }
                                </script>
                                <table id="ccTableTipoCont" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Tipo Sigla</th>
                                        <th scope="col">Descripcion</th>
                                        <th scope="col">Tipo Oracle</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$TipoCont item=c}
                                        <tr onclick="onClickUpdateFormTipoCont({$c.id},'{$c.tipo|trim}','{$c.descrip|trim}','{$c.tipoOracle|trim}')">
                                            <td><a href="{base_url()}GLParams/clearGLTipoCont/{$c.id}/{$country}"><i class="fas fa-eraser fa-sm fa-fw text-danger"></i></a>&nbsp; {$c.tipo}</td>
                                            <td>{$c.descrip}</td>
                                            <td>{$c.tipoOracle}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Tipo Sigla</th>
                                        <th scope="col">Descripcion</th>
                                        <th scope="col">Tipo Oracle</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTableTipoCont').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>
                            <div class="tab-pane {if $bottomActive=='Diarios'}active{/if} mt-4" id="Diarios">
                                <div class="mt-2 mb-4">
                                    <form id="updateGLDiarios" class="inline align-top" action="{base_url()}GLParams/updateGLDiarios/{$country}" method="POST">
                                        <input type="hidden" id="updateGLDiarios_id" name="id"/>

                                        <div class="form-row align-items-center">
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLDiarios_diario" name="diario" disabled placeholder="Diario" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLDiarios_descrip" name="descrip_diarios" disabled placeholder="Descripcion" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLDiarios_active" name="active" value="{$active}" maxlength="2" required placeholder="Active"/>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" id="btnDiarios" class="btn btn-primary"><i class="fas fa-check-circle fa-sm fa-fw text-white"></i> Cambiar</button>
                                            </div>
                                            <small id="errorDiarios" class="form-text text-danger">{$errorDiarios}</small>
                                        </div>
                                    </form>
                                </div>
                                <script>
                                    function onClickUpdateFormDiarios(id, diario, descrip, active){
                                        $('input[id="updateGLDiarios_id"]').val(id);
                                        $('input[id="updateGLDiarios_diario"]').val(diario);
                                        $('input[id="updateGLDiarios_descrip"]').val(descrip);
                                        $('input[id="updateGLDiarios_active"]').val(active);
                                        $('small[id="errorDiarios"]').html("");
                                    }
                                </script>
                                <table id="ccTableDiarios" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Diario</th>
                                        <th scope="col">Descripcion</th>
                                        <th scope="col">Active</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$Diarios item=c}
                                        <tr onclick="onClickUpdateFormDiarios({$c.id},'{$c.diario|trim}','{$c.descrip|trim}','{$c.active|trim}')">
                                            <td><a href="{base_url()}GLParams/clearGLDiarios/{$c.id}/{$country}"><i class="fas fa-eraser fa-sm fa-fw text-danger"></i></a>&nbsp; {$c.diario}</td>
                                            <td>{$c.descrip}</td>
                                            <td>{$c.active}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Diario</th>
                                        <th scope="col">Descripcion</th>
                                        <th scope="col">Active</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTableDiarios').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>
                            <div class="tab-pane {if $bottomActive=='CostoLocal'}active{/if} mt-4" id="CostoLocal">
                                <div class="mt-2 mb-4">
                                    <form id="updateGLCostoLocal" class="inline align-top" action="{base_url()}GLParams/updateGLCostoLocal/{$country}" method="POST">
                                        <input type="hidden" id="updateGLCostoLocal_id_" name="id"/>

                                        <div class="form-row align-items-center">
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLCostoLocal_id_sigla" name="diario" disabled placeholder="Codigo" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLCostoLocal_descrip" name="descrip_cl" disabled placeholder="Descripcion" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLCostoLocal_clasificacion" name="clasificacion" value="{$clasificacion}" maxlength="200"  placeholder="Clasificacion"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLCostoLocal_expenditure" name="expenditure" value="{$expenditure}" maxlength="200"  placeholder="Expenditure"/>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" id="btnCostoLocal" class="btn btn-primary"><i class="fas fa-check-circle fa-sm fa-fw text-white"></i> Cambiar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <script>
                                    function onClickUpdateFormCostoLocal(id, id_sigla, descrip, clasificacion, expenditure){
                                        $('input[id="updateGLCostoLocal_id"]').val(id);
                                        $('input[id="updateGLCostoLocal_id_sigla"]').val(id_sigla);
                                        $('input[id="updateGLCostoLocal_descrip"]').val(descrip);
                                        $('input[id="updateGLCostoLocal_clasificacion"]').val(clasificacion);
                                        $('input[id="updateGLCostoLocal_expenditure"]').val(expenditure);
                                    }
                                </script>
                                <table id="ccTableCostoLocal" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Descripcion</th>
                                        <th scope="col">Clasificacion</th>
                                        <th scope="col">Expenditure</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$CostoLocal item=c}
                                        <tr onclick="onClickUpdateFormCostoLocal({$c.id},'{$c.id_sigla}','{$c.descrip|trim}','{$c.clasificacion|trim}','{$c.expenditure|trim}')">
                                            <td><a href="{base_url()}GLParams/clearGLCostoLocal/{$c.id}/{$country}"><i class="fas fa-eraser fa-sm fa-fw text-danger"></i></a>&nbsp; {$c.id_sigla}</td>
                                            <td>{$c.descrip}</td>
                                            <td>{$c.clasificacion}</td>
                                            <td>{$c.expenditure}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Descripcion</th>
                                        <th scope="col">Clasificacion</th>
                                        <th scope="col">Expenditure</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTableCostoLocal').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>
                            <div class="tab-pane  {if $bottomActive=='Monedas'}active{/if} mt-4" id="Monedas">
                                <div class="mt-2 mb-4">
                                    <form id="updateGLMonedas" class="inline align-top" action="{base_url()}GLParams/updateGLMonedas/{$country}" method="POST">
                                        <input type="hidden" id="updateGLMonedas_id" name="id" value="{$id}"/>

                                        <div class="form-row align-items-center">
                                            <div class="col-sm-2 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLMonedas_moneda" name="moneda" disabled placeholder="Moneda" value="{$moneda}" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLMonedas_descrip" name="descrip_moneda" disabled placeholder="Descripcion" value="{$descrip}" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLMonedas_sigla" name="sigla" disabled placeholder="Sigla" value="{$sigla}" required/>
                                                </div>
                                            </div>
                                            <div class="col-sm-1 my-1">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="updateGLMonedas_tasa" name="tasa" value="{$tasa}" required placeholder="Tasa"/>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" id="btnMonedas" class="btn btn-primary"><i class="fas fa-check-circle fa-sm fa-fw text-white"></i> Cambiar</button>
                                            </div>
                                            {if isset($errorMonedas)}
                                                <div class="col-12 alert-danger text-center p-2">
                                                    {$errorMonedas}
                                                </div>
                                            {/if}
                                        </div>
                                    </form>
                                </div>
                                <script>
                                    function onClickUpdateFormMonedas(id, moneda, descrip, sigla, tasa){
                                        $('input[id="updateGLMonedas_id"]').val(id);
                                        $('input[id="updateGLMonedas_moneda"]').val(moneda);
                                        $('input[id="updateGLMonedas_descrip"]').val(descrip);
                                        $('input[id="updateGLMonedas_sigla"]').val(sigla);
                                        $('input[id="updateGLMonedas_tasa"]').val(tasa);
                                        $('small[id="errorMonedas"]').html("");
                                    }
                                </script>
                                <table id="ccTableMonedas" class="display">
                                    <thead>
                                    <tr>
                                        <th scope="col">Moneda</th>
                                        <th scope="col">Descripcion</th>
                                        <th scope="col">Sigla</th>
                                        <th scope="col">Tasa</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach from=$Monedas item=c}
                                        <tr onclick="onClickUpdateFormMonedas({$c.id},'{$c.moneda|trim}','{$c.descrip|trim}','{$c.sigla|trim}','{$c.tasa|trim}')">
                                            <td><a href="{base_url()}GLParams/clearGLMonedas/{$c.id}/{$country}"><i class="fas fa-eraser fa-sm fa-fw text-danger"></i></a>&nbsp; {$c.moneda}</td>
                                            <td>{$c.descrip}</td>
                                            <td>{$c.sigla}</td>
                                            <td>{$c.tasa|number_format:8}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th scope="col">Moneda</th>
                                        <th scope="col">Descripcion</th>
                                        <th scope="col">Sigla</th>
                                        <th scope="col">Tasa</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        var table = $('#ccTableMonedas').DataTable({
                                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                        } );
                                        table.order( [ 0, 'asc' ] ).draw();
                                    } );
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<br/>
<br/>
<br/>
