<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><span class="mt-2">{if isset($country)}<span class="flag-icon flag-icon-{strtolower($country)}"  title="{$country}"></span>{/if}&nbsp;{$title}</span></h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}IDL/groups" data-toggle="tooltip" data-placement="top" title="view all IDLs by country"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Debug" data-toggle="tooltip" data-placement="top" title="debug and audit logs"><i class="fas fa-bug fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}ManageFF" data-toggle="tooltip" data-placement="top" title="manage all bundles"><i class="fas fa-boxes fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-primary" href="{base_url()}IDL/fields/create/{$id}" title="add a field to this IDL"><i class="fas fa-plus-circle fa-sm fa-fw text-white"></i></a>
            </li>
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-secondary" href="{base_url()}IDL/groups/edit/{$group.id}"><i class="fas fa-ban fa-sm fa-fw text-white"></i></a>
            </li>
        </ul>
    </div>
</nav>
<br/>
<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-3 my-1">
            <label class="sr-only" for="inlineFormInputGroupShortName">Short Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Short Name <small id="errorShortName" class="form-text text-danger">&nbsp;{$errorShortName}</small></div>
                </div>
                <input type="text" class="form-control" id="shortName" name="shortName" value="{$shortName}" placeholder="Short Name">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" {($isTransactional==true)?'checked':''} id="isTransactional" name="isTransactional">
                <label class="form-check-label" for="isTransactional">
                    Is Transactional?
                </label>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-3 my-1">
            <label class="sr-only" for="inlineFormInputGroupTableName">Table Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Table Name <small id="errorTableName" class="form-text text-danger">&nbsp;{$errorTableName}</small></div>
                </div>
                <input type="text" class="form-control" id="tableName" name="tableName" value="{$tableName}" placeholder="Table Name">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <label class="sr-only" for="inlineFormInputGroupTemplateName">Template Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Template Name <small id="errorTemplateName" class="form-text text-danger">&nbsp;{$errorTemplateName}</small></div>
                </div>
                <input type="text" class="form-control" id="templateName" name="templateName" value="{$templateName}" placeholder="Template Name">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" {($isHeader==true)?'checked':''} id="isHeader" name="isHeader">
                <label class="form-check-label" for="isHeader">
                    Is Header?
                </label>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">{$actionLabel}</button>
                <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}IDL/groups/edit/{$group.id}">Cancel&nbsp;<i class="fas fa-ban fa-sm fa-fw text-white"></i></a>
            </div>
        </div>
    </div>
</form>
<br>
<h4>Calculated Pre-sets</h4>
<fieldset disabled>
    <div class="form-group">
        <label for="preStageTableName">Pre-stage Table Name (always updated from SIGLA)</label>
        <input type="text" id="preStageTableName" value="{$preStageTableName}" class="form-control" placeholder="Pre-stage table name" disabled>
    </div>
    <div class="form-group">
        <label for="stageTableName">Stage Table Name (cleansing work in progress)</label>
        <input type="text" id="stageTableName" value="{$stageTableName}" class="form-control" placeholder="Stage table name" disabled>
    </div>
</fieldset>
<br>
<h4>Fields</h4>
{if isset($fields)}

    <table id="fieldsTable" class="display" style="width:100%">
        <thead>
        <tr>
            <th scope="col">Position</th>
            <th scope="col">Short Name</th>
            <th scope="col">Output</th>
            <th scope="col">Mandatory</th>
            <th scope="col">ORA_TYPE</th>
            <th scope="col">Description</th>
            <th scope="col">LOV</th>
            <th scope="col">Constant</th>
            <th scope="col">Form Rules</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$fields item=f}
            <tr class="clickable-row" style="cursor:pointer" data-href='{base_url()}IDL/fields/edit/{$f.id}' >
                <td>{$f.outputPos}</td>
                <td>{$f.shortName}</td>
                <td class="text-center">{if ($f.isOutputValue)}<i class="fas fa-thumbs-up text-success fa-fw" title="output to FF"></i> <span class="d-none">Y</span>{else}<i class="fas fa-thumbs-down text-danger fa-fw" title="No output on FF"></i>{/if}</td>
                <td class="text-center">{if ($f.isMandatory)}<i class="fas fa-thumbs-up text-success fa-fw" title="mandatory field"></i> <span class="d-none">N</span>{else}<i class="fas fa-thumbs-down text-danger fa-fw" title="NON mandatory field"></i> {/if}</td>
                <td>{$f.ORA_TYPE}</td>
                <td>{$f.description|truncate:20}</td>
                <td>{$f.LOV|truncate:20}</td>
                <td>{$f.constant|truncate:20}</td>
                <td>{$f.formRules|truncate:20}</td>
            </tr>
        {/foreach}
        </tbody>
        <tfoot>
        <tr>
            <th scope="col">Position</th>
            <th scope="col">Short Name</th>
            <th scope="col">Output</th>
            <th scope="col">Mandatory</th>
            <th scope="col">ORA_TYPE</th>
            <th scope="col">Description</th>
            <th scope="col">LOV</th>
            <th scope="col">Constant</th>
            <th scope="col">Form Rules</th>
        </tr>
        </tfoot>
    </table>
    <script>
        $(document).ready(function() {
            var table = $('#fieldsTable').DataTable({
                "lengthMenu": [[100, -1], [ 100, "All"]]
            } );
            table.order( [ 2, 'desc' ], [ 0, 'asc' ] ).draw();
        } );
    </script>
{/if}


