<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><span class="mt-2">{if isset($country)}<span class="flag-icon flag-icon-{strtolower($country)}" title="{$country}"></span>{/if}&nbsp;{$title}</span></h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}IDL/groups" data-toggle="tooltip" data-placement="top" title="view all IDLs by country"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Debug" data-toggle="tooltip" data-placement="top" title="debug and audit logs"><i class="fas fa-bug fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}ManageFF" data-toggle="tooltip" data-placement="top" title="manage all bundles"><i class="fas fa-boxes fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-danger" data-toggle="modal" data-target="#Modal1" title="discard this field"><i class="fas fa-eraser fa-sm fa-fw text-white"></i></a>
            </li>
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-secondary" href="{base_url()}IDL/descriptors/edit/{$field.descriptor.id}"><i class="fas fa-ban fa-sm fa-fw text-white"></i></a>
            </li>
        </ul>
    </div>
</nav>
<br/>
<form class="" action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-3 my-1">
            <label class="sr-only" for="inlineFormInputGroupColName">Column Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Column Name <small id="errorColName" class="form-text text-danger">&nbsp;{$errorColName}</small></div>
                </div>
                <input type="text" class="form-control" id="colName" name="colName" value="{$field.colName}" placeholder="Column Name">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <label class="sr-only" for="inlineFormInputGroupShortName">Short Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Short Name <small id="errorColName" class="form-text text-danger">&nbsp;{$errorShortName}</small></div>
                </div>
                <input type="text" class="form-control" id="shortName" name="shortName" value="{$field.shortName}" placeholder="Short Name">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-3 my-1">
            <label class="sr-only" for="inlineFormInputGroupORA_TYPE">Oracle Type</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Oracle Type <small id="errorTemplateName" class="form-text text-danger">&nbsp;{$errorORA_TYPE}</small></div>
                </div>
                <input type="text" class="form-control" id="ORA_TYPE" name="ORA_TYPE" value="{$field.ORA_TYPE}" placeholder="Oracle Type">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" {($field.isMandatory==true)?'checked':''} id="isMandatory" name="isMandatory">
                <label class="form-check-label" for="isMandatory">
                    Is Mandatory?
                </label>
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" {($field.isQueryable==true)?'checked':''} id="isQueryable" name="isQueryable">
                <label class="form-check-label" for="isQueryable">
                    Is Queryable?
                </label>
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" {($field.isSortable==true)?'checked':''} id="isSortable" name="isSortable">
                <label class="form-check-label" for="isSortable">
                    Is Sortable?
                </label>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-3 my-1">
            <label class="sr-only" for="inlineFormInputGroupOutputPos">Output Pos</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Output Pos<small id="errorOutputPos" class="form-text text-danger">&nbsp;{$errorOutputPos}</small></div>
                </div>
                <input type="text" class="form-control" id="outputPos" name="outputPos" value="{$field.outputPos}" placeholder="OutputPos">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" {($field.isOutputValue==true)?'checked':''} id="isOutputValue" name="isOutputValue">
                <label class="form-check-label" for="isOutputValue">
                    Is Output?
                </label>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <label class="sr-only" for="inlineFormInputGroupLOV">LOV</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">LOV <small id="errorTemplateName" class="form-text text-danger">&nbsp;{$errorLOV}</small></div>
                </div>
                <input type="text" class="form-control" id="LOV" name="LOV" value="{$field.LOV}" placeholder="List of values (separated by comma)">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <label class="sr-only" for="inlineFormInputGroupConstant">Constant</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Constant <small id="errorConstant" class="form-text text-danger">&nbsp;{$errorConstant}</small></div>
                </div>
                <input type="text" class="form-control" id="constant" name="constant" value="{$field.constant}" placeholder="constant value">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <div class="form-group">
                <small id="descriptionHelp" class="form-text text-muted">Description extracted from the eXceL specs</small>
                <textarea class="form-control" id="description" name="description" aria-describedby="descriptionHelp" placeholder="Description" rows="5">{$field.description}</textarea>
                <small id="errorDescription" class="form-text text-danger">{$errorDescription}</small>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="form-group">
                <button type="submit" class="btn btn-primary m-1">{$actionLabel}</button>
                <a class="btn btn-secondary m-1" href="{base_url()}IDL/descriptors/edit/{$field.descriptor.id}">Cancel&nbsp;<i class="fas fa-ban fa-sm fa-fw text-white"></i></a>
            </div>
        </div>
    </div>

    <h4>Calculated Pre-sets</h4>
    <div class="row">
        <div class="col-4 form-group">
            <label for="disabledTextInput">Suggested Short Name</label>
            <input type="text" id="maxLength" value="{$field.suggestedShortName}" class="form-control" placeholder="suggested short name">
        </div>
        <div class="col-4 form-group">
            <label for="disabledTextInput">max length</label>
            <input type="text" id="maxLength" value="{$field.maxLength}" class="form-control" placeholder="Max length applies only for VARCHAR2(##) fields">
        </div>
        <div class="col-4 form-group">
            <label for="disabledTextInput">CI Form Validation rule</label>
            <input type="text" id="formRules" value="{$field.formRules}" class="form-control" placeholder="Codeigniter Form Validation Rules">
        </div>
    </div>
</form>
<br/>
<h4>Similar Fields (on other countries) &nbsp;<a class="btn btn-primary m-1" href="{base_url()}IDL/fields/cloneToMissingCountries/{$field.id}" target="_self" title="clone field to missing countries">Clone to missing &nbsp;<i class="fas fa-copy fa-sm fa-fw"></i></a><a class="btn btn-secondary m-1" href="{base_url()}IDL/descriptors/edit/{$field.descriptor.id}"><i class="fas fa-ban fa-sm fa-fw text-white"></i></a></h4>
{if isset($similar)}

    <table id="fieldsTable" class="display" style="width:100%">
        <thead>
        <tr>
            <th scope="col">Country</th>
            <th scope="col">Position</th>
            <th scope="col">Short Name</th>
            <th scope="col">Output</th>
            <th scope="col">Mandatory</th>
            <th scope="col">ORA_TYPE</th>
            <th scope="col">Description</th>
            <th scope="col">LOV</th>
            <th scope="col">Constant</th>
            <th scope="col">Form Rules</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$similar item=f}
            <tr class="clickable-row" style="cursor:pointer" data-href='{base_url()}IDL/fields/edit/{$f.id}' >
                <td><span class="flag-icon flag-icon-{strtolower($f.descriptor.group.country)}" title="{$f.descriptor.group.country}"></span></td>
                <td>{$f.outputPos}</td>
                <td>{$f.shortName}</td>
                <td class="text-center">{if ($f.isOutputValue)}<i class="fas fa-thumbs-up text-success fa-fw"></i> <span class="d-none">Y</span>{else}<i class="fas fa-thumbs-down text-danger fa-fw"></i>{/if}</td>
                <td class="text-center">{if ($f.isMandatory)}<i class="fas fa-thumbs-up text-success fa-fw"></i> <span class="d-none">N</span>{else}<i class="fas fa-thumbs-down text-danger fa-fw"></i> {/if}</td>
                <td>{$f.ORA_TYPE}</td>
                <td>{$f.description|truncate:20}</td>
                <td>{$f.LOV|truncate:20}</td>
                <td>{$f.constant|truncate:20}</td>
                <td>{$f.formRules|truncate:20}</td>
            </tr>
        {/foreach}
        </tbody>
        <tfoot>
        <tr>
            <th scope="col">Country</th>
            <th scope="col">Position</th>
            <th scope="col">Short Name</th>
            <th scope="col">Output</th>
            <th scope="col">Mandatory</th>
            <th scope="col">ORA_TYPE</th>
            <th scope="col">Description</th>
            <th scope="col">LOV</th>
            <th scope="col">Constant</th>
            <th scope="col">Form Rules</th>
        </tr>
        </tfoot>
    </table>
    <script>
        $(document).ready(function() {
            var table = $('#fieldsTable').DataTable({
                "lengthMenu": [[50, 100, -1], [50, 100, "All"]]
            } );
            table.order( [ 2, 'desc' ], [ 0, 'asc' ] ).draw();
        } );
    </script>
{/if}
<br/>
<br/>
<br/>
<br/>
<br/>
<div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="Modal1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">Deleting Group #{$id}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Deleting Field #{$field.id}-{$country}-{$field.shortName} cannot be undone.  Are you sure?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="{base_url()}IDL/fields/delete/{$field.id}" class="btn btn-danger">Delete</a>
            </div>
        </div>
    </div>
</div>

