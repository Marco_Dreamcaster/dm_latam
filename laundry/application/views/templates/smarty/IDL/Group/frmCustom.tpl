<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><span class="mt-2">{if isset($country)}<span class="flag-icon flag-icon-{strtolower($country)}"  title="{$country}"></span>{/if}&nbsp;{$title}</span></h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Debug" data-toggle="tooltip" data-placement="top" title="debug and audit logs"><i class="fas fa-bug fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}ManageFF" data-toggle="tooltip" data-placement="top" title="manage all bundles"><i class="fas fa-boxes fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item mt-2 p-1">
                <form class="form-inline" method="post" action="{base_url()}IDL/groups/clone/{$id}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <button class="btn btn-primary" type="submit" target="_blank" title="clone configuration to country">Clone to <i class="fas fa-copy fa-sm fa-fw"></i></button>
                        </div>
                        <input class="custom-select-country" type="text" id="cloneToCountry" name="cloneToCountry" readonly="true"/>
                        <input type="hidden" id="cloneToCountry_code" name="cloneToCountry_code" />
                    </div>
                </form>
                <script>
                    $(function() {
                        $("#cloneToCountry").countrySelect({
                            defaultCountry: "pa",
                            onlyCountries: ['pa','co','mx','sv','ni','hn','gt','cr','cl','ar','pe','py','uy','np'],
                        });
                    });
                </script>
            </li>
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-primary" href="{base_url()}IDL/descriptors/create/{$id}" title="add an IDL to this group"><i class="fas fa-plus-circle fa-sm fa-fw text-white"></i></a>
            </li>
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-danger" data-toggle="modal" data-target="#Modal1" title="discard this group"><i class="fas fa-eraser fa-sm fa-fw text-white"></i></a>
            </li>
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-secondary" href="{base_url()}IDL/groups"><i class="fas fa-ban fa-sm fa-fw text-white"></i></a>
            </li>
        </ul>
    </div>
</nav>
<br/>
<form action="{base_url()}{$action}" method="post">
    <div class="row">
        <div class="form-group col-4">
            <label for="shortName">Short Name</label>
            <input type="text" class="form-control" id="shortName" name="shortName" aria-describedby="shortNameHelp" value="{$shortName}" placeholder="Group short name">
            <small id="errorShortName" class="form-text text-danger">{$errorShortName}</small>
        </div>
        <div class="form-group col-4">
            <label for="country">Country</label>
            <input type="text" class="form-control" id="country" name="country" aria-describedby="countryHelp" value="{$country}" placeholder="Group country">
            <small id="errorCountry" class="form-text text-danger">{$errorCountry}</small>
        </div>
        <div class="form-group col-4">
            <label for="outputPath">Default Path</label>
            <input type="text" class="form-control" id="outputPath" name="outputPath" aria-describedby="outputPathHelp" value="{$outputPath}" placeholder="Group output path">
            <small id="errorOutputPath" class="form-text text-danger">{$errorOutputPath}</small>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-primary">{$actionLabel}</button>
            <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}IDL/groups">Cancel&nbsp;<i class="fas fa-ban fa-sm fa-fw text-white"></i></a>
        </div>
    </div>
</form>
{if isset($descriptors)}
<br/>
<br/>
<br/>
<table id="descriptorsTable" class="display" style="width:100%">
    <thead>
    <tr>
        <th scope="col">Table Name</th>
        <th scope="col">Short Name</th>
        <th scope="col">Template Name</th>
        <th scope="col">Actions</th>
        <th scope="col">Is Header</th>
        <th scope="col">Is Transactional</th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$descriptors item=d}
        <tr class="{if $d.isHeader==true}alert-success{/if} clickable-row" style="cursor:pointer" data-href='{base_url()}IDL/descriptors/edit/{$d.id}'">
        <td>{$d.tableName}</td>
        <td>{$d.shortName}</td>
        <td>{$d.templateName}</td>
        <td><a class="btn btn-primary" href="{base_url()}Debug/{$country}/{$shortName}/{$d.shortName}"><i class="text-white fas fa-bug fa-sm fa-fw mt-2"></a></td>
        <td class="text-center">{if ($d.isHeader)}<i class="fas fa-thumbs-up text-success fa-fw"></i>{/if}</td>
        <td class="text-center">{if ($d.isTransactional)}<i class="fas fa-thumbs-up text-success fa-fw"></i>{/if}</td>
        </tr>
    {/foreach}
    </tbody>
    <tfoot>
    <tr>
        <th scope="col">Table Name</th>
        <th scope="col">Short Name</th>
        <th scope="col">Template Name</th>
        <th scope="col">Actions</th>
        <th scope="col">Is Header</th>
        <th scope="col">Is Transactional</th>
    </tr>
    </tfoot>
</table>
<script>
    $(document).ready(function() {
        var table = $('#descriptorsTable').DataTable({
            "lengthMenu": [[50, 100, -1], [50, 100, "All"]]
        } );
        table.order( [ 4, 'desc' ] ).draw();
    } );
</script>
{/if}

<div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="Modal1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">Deleting Group #{$id}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Deleting Group #{$id}-{$country}-{$shortName} cannot be undone.  Are you sure?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="{base_url()}IDL/groups/delete/{$id}" class="btn btn-danger">Delete</a>
            </div>
        </div>
    </div>
</div>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>