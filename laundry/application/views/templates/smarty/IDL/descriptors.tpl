<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <p class="label p-2 lead"> <i class="fas fa-th-list fa-lg fa-fw"></i>&nbsp;IDL Descriptors</span></p>
        <ul class="navbar-nav mr-auto">
        </ul>
    </div>
</nav>
<br/>
<table id="descriptorsTable" class="display" style="width:100%">
    <thead>
    <tr>
        <th scope="col">Group</th>
        <th scope="col">Countries</th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$display key=shortName item=countries}
        <tr>
            <th>{$shortName}</th>
            <td>
                {foreach from=$countries key=countryCode item=id}
                    <a class="btn btn-success mr-1 mt-1" href="{base_url()}IDL/groups/edit/{$id}" target="_blank" title="open specs for {$countryCode} {$shortName} in another tab" ><span class="flag-icon flag-icon-{strtolower($countryCode)} flag-icon-squared"></span></a>
                {/foreach}
            </td>
        </tr>
    {/foreach}
    </tbody>
    <tfoot>
    <tr>
        <th scope="col">Group</th>
        <th scope="col">Countries</th>
    </tr>
    </tfoot>
</table>
<script>
    $(document).ready(function() {
        var table = $('#groupsTable').DataTable({
            "lengthMenu": [[100, -1], [100, "All"]]
        } );
        table.order( [ 0, 'asc' ] ).draw();
    } );
</script>



<table class="table table-hover table-sm">
    <thead>
    <tr>
        <th scope="col"></th>
        <th scope="col">#</th>
        <th scope="col">Country</th>
        <th scope="col">Group Short Name</th>
        <th scope="col">Short Name</th>
        <th scope="col">Table Name</th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$descriptors item=d name=foo}
        <tr>
            <td>
                <a href="{base_url()}IDL/descriptors/edit/{$d.id}"><i class="text-primary fas fa-edit fa-lg fa-fw"></i></a>
            </td>
            <th scope="row">{$d.id}</th>
            <td>{$d.group.country}</td>
            <td>{$d.group.shortName}</td>
            <td>{$d.shortName}</td>
            <td>{$d.tableName}</td>
        </tr>
    {/foreach}
    </tbody>
</table>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>