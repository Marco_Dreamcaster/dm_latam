<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        </ul>
    </div>
</nav>
<br/>
<h3><i class="fas fa-ellipsis-h fa-lg fa-fw"></i>&nbsp;IDL Fields</h3>
<br/>

<table class="table table-hover table-sm">
    <thead>
    <tr>
        <th scope="col"></th>
        <th scope="col">#</th>
        <th scope="col">Country</th>
        <th scope="col">Group Name</th>
        <th scope="col">Descriptor Name</th>
        <th scope="col">Field Name</th>
        <th scope="col">ORA_TYPE</th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$fields item=f}
        <tr>
            <td>
                <a href="{base_url()}IDL/fields/edit/{$f.id}"><i class="text-primary fas fa-edit fa-lg fa-fw"></i></a>
            </td>
            <th scope="row">{$f.id}</th>
            <td>{$f.descriptor.group.country}</td>
            <td>{$f.descriptor.group.shortName}</td>
            <td>{$f.descriptor.shortName}</td>
            <td>{$f.shortName}</td>
            <td>{$f.ORA_TYPE}</td>
        </tr>
    {/foreach}
    </tbody>
</table>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>