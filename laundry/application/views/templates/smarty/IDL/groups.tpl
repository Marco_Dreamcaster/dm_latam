<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><span class="mt-2"><i class="fas fa-object-group fa-lg fa-fw m-2"></i>&nbsp;{$title}</span></h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Debug" data-toggle="tooltip" data-placement="top" title="debug and audit logs"><i class="fas fa-bug fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}ManageFF" data-toggle="tooltip" data-placement="top" title="manage all bundles"><i class="fas fa-boxes fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>
<br/>
<table id="groupsTable" class="display" style="width:100%">
    <thead>
    <tr>
        <th scope="col">Group</th>
        <th scope="col">Countries</th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$display key=shortName item=countries}
        <tr>
            <th>{$shortName}</th>
            <td>
                {foreach from=$countries key=countryCode item=id}
                    <a class="btn btn-success mr-1 mt-1" href="{base_url()}IDL/groups/edit/{$id}" target="_blank" title="open specs for {$countryCode} {$shortName} in another tab" ><span class="flag-icon flag-icon-{strtolower($countryCode)} flag-icon-squared"></span></a>

                {/foreach}
            </td>
        </tr>
    {/foreach}
    </tbody>
    <tfoot>
    <tr>
        <th scope="col">Group</th>
        <th scope="col">Countries</th>
    </tr>
    </tfoot>
</table>
<script>
    $(document).ready(function() {
        var table = $('#groupsTable').DataTable({
            "lengthMenu": [[50, 100, -1], [50, 100, "All"]]
        } );
        table.order( [ 0, 'asc' ] ).draw();
    } );
</script>
