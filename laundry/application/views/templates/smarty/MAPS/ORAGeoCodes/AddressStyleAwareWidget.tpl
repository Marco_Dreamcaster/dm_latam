    <div class="form-row align-items-center mb-2">
        <div class="col-sm-12 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text"  id="addressStyleIndicator">no existing address, check to select a location</div>
                </div>
                <input type="checkbox" class="m-4" id="changeGeoCodes" name="changeGeoCodes" title="check if you need to adjust">
            </div>
        </div>
    </div>

    <div id="existingGeoCodes">
        <div class="form-row align-items-center mb-2">
        {if $addressCountry=='AR'}
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="addressCountryTitle">Address Country</div>
                    </div>
                    <input type="text" class="form-control" id="displayAddressCountry" name="displayAddressCountry" value="{$addressCountry}" disabled>
                </div>
            </div>
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="addressCountryTitle">Provincia</div>
                    </div>
                    <input type="text" class="form-control" id="displayAddressCountry" name="displayAddressCountry" value="{$state}" disabled>
                </div>
            </div>
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="addressCountryTitle">Departamento</div>
                    </div>
                    <input type="text" class="form-control" id="displayAddressCountry" name="displayAddressCountry" value="{$county}" disabled>
                </div>
            </div>
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="addressCountryTitle">Cabecera</div>
                    </div>
                    <input type="text" class="form-control" id="displayAddressCountry" name="displayAddressCountry" value="{$city}" disabled>
                </div>
            </div>

        {elseif ($addressCountry=='CO'||$addressCountry=='MX')}
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Address Country</div>
                    </div>
                    <input type="text" class="form-control" id="displayAddressCountry" name="displayAddressCountry" value="{$addressCountry}" disabled>
                </div>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Estado</div>
                    </div>
                    <input type="text" class="form-control" id="displayState" name="displayState" value="{$state}" disabled>
                </div>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Ciudad</div>
                    </div>
                    <input type="text" class="form-control" id="displayCity" name="displayCity" value="{$city}" disabled>
                </div>
            </div>

        {elseif ($addressCountry=='SV'||$addressCountry=='NI'||$addressCountry=='HN'||$addressCountry=='GT')}
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Address Country</div>
                    </div>
                    <input type="text" class="form-control" id="displayAddressCountry" name="displayAddressCountry" value="{$addressCountry}" disabled>
                </div>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Departamento</div>
                    </div>
                    <input type="text" class="form-control" id="displayState" name="displayState" value="{$state}" disabled>
                </div>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Municipalidad</div>
                    </div>
                    <input type="text" class="form-control" id="displayCity" name="displayCity" value="{$city}" disabled>
                </div>
            </div>

        {elseif $addressCountry=='CR'}
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Address Country</div>
                    </div>
                    <input type="text" class="form-control" id="displayAddressCountry" name="displayAddressCountry" value="{$addressCountry}" disabled>
                </div>
            </div>
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Provincia</div>
                    </div>
                    <input type="text" class="form-control" id="displayProvince" name="displayProvince" value="{$province}" disabled>
                </div>
            </div>
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Canton</div>
                    </div>
                    <input type="text" class="form-control" id="displayCounty" name="displayCounty" value="{$county}" disabled>
                </div>
            </div>
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Ciudad</div>
                    </div>
                    <input type="text" class="form-control" id="displayCity" name="displayCity" value="{$city}" disabled>
                </div>
            </div>

        {elseif $addressCountry=='CL'}
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Address Country</div>
                    </div>
                    <input type="text" class="form-control" id="displayAddressCountry" name="displayAddressCountry" value="{$addressCountry}" disabled>
                </div>
            </div>
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Region</div>
                    </div>
                    <input type="text" class="form-control" id="displayState" name="displayState" value="{$state}" disabled>
                </div>
            </div>
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Provincia</div>
                    </div>
                    <input type="text" class="form-control" id="displayProvince" name="displayProvince" value="{$province}" disabled>
                </div>
            </div>
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Communa</div>
                    </div>
                    <input type="text" class="form-control" id="displayCity" name="displayCity" value="{$city}" disabled>
                </div>
            </div>

        {elseif $addressCountry=='PE'}
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Address Country</div>
                    </div>
                    <input type="text" class="form-control" id="displayAddressCountry" name="displayAddressCountry" value="{$addressCountry}" disabled>
                </div>
            </div>
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Region</div>
                    </div>
                    <input type="text" class="form-control" id="displayState" name="displayState" value="{$state}" disabled>
                </div>
            </div>
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Provincia</div>
                    </div>
                    <input type="text" class="form-control" id="displayProvince" name="displayProvince" value="{$province}" disabled>
                </div>
            </div>
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Distrito</div>
                    </div>
                    <input type="text" class="form-control" id="displayCity" name="displayCity" value="{$city}" disabled>
                </div>
            </div>

        {elseif ($addressCountry=='UY'||$addressCountry=='PA'||$addressCountry=='PY')}
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Address Country</div>
                    </div>
                    <input type="text" class="form-control" id="displayAddressCountry" name="displayAddressCountry" value="{$addressCountry}" disabled>
                </div>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Departamento</div>
                    </div>
                    <input type="text" class="form-control" id="displayState" name="displayState" value="{$state}" disabled>
                </div>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Municipalidad</div>
                    </div>
                    <input type="text" class="form-control" id="displayCity" name="displayCity" value="{$city}" disabled>
                </div>
            </div>

        {else}
            <div class="col-sm-12 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Address Country</div>
                    </div>
                    <input type="text" class="form-control" id="displayAddressCountry" name="displayAddressCountry" value="{$addressCountry}" disabled>
                </div>
            </div>
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">State</div>
                    </div>
                    <input type="text" class="form-control" id="displayState" name="displayState" value="{$state}" disabled>
                </div>
            </div>
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Province</div>
                    </div>
                    <input type="text" class="form-control" id="displayProvince" name="displayProvince" value="{$province}" disabled>
                </div>
            </div>
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">County</div>
                    </div>
                    <input type="text" class="form-control" id="displayCounty" name="displayCounty" value="{$county}" disabled>
                </div>
            </div>
            <div class="col-sm-3 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">City</div>
                    </div>
                    <input type="text" class="form-control" id="displayCity" name="displayCity" value="{$city}" disabled>
                </div>
            </div>
        {/if}

        </div>


    </div>

    <div id="newGeoCodes">
        <div class="form-row align-items-center mb-2">
            <div class="col-sm-12 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="addressCountryTitle">Address Country</div>
                    </div>
                    <select class="form-control" id="addressCountry" name="addressCountry" aria-describedby="addressCountryHelp">
                        <option value="">--</option>
                        {foreach from=$countryCodes item=cc}
                            <option value="{$cc['CC']}">{$cc['CC']}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
        </div>
        <div class="form-row align-items-center mb-2">
            <div class="col-3 my-1" id="divState">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="stateTitle">State</div>
                    </div>
                    <select class="form-control" id="state" name="state" aria-describedby="stateHelp">
                        <option value="">--</option>
                    </select>
                </div>
            </div>
            <div class="col-3 my-1" id="divProvince">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="provinceTitle">Province</div>
                    </div>
                    <select class="form-control" id="province" name="province" aria-describedby="provinceHelp">
                        <option value="">--</option>
                    </select>
                </div>
            </div>
            <div class="col-3 my-1" id="divCounty">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="countyTitle">County</div>
                    </div>
                    <select class="form-control" id="county" name="county" aria-describedby="countyHelp">
                        <option value="">--</option>
                    </select>
                </div>
            </div>
            <div class="col-3 my-1" id="divCity">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="cityTitle">City</div>
                    </div>
                    <select class="form-control" id="city" name="city" aria-describedby="cityHelp">
                        <option value="">--</option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <br/>

    <script>
        $("#changeGeoCodes").ready(function() {
            console.log('#changeGeoCodes is ready ');

            $('#existingGeoCodes').show();
            $('#newGeoCodes').hide();

        });

        $( "#changeGeoCodes" ).on( "change", function() {
            if ( $( "#changeGeoCodes" ).is(":checked") ){
                console.log('change');
                $('#existingGeoCodes').hide();
                $('#newGeoCodes').show();

            }else{
                console.log('existing');
                $('#existingGeoCodes').show();
                $('#newGeoCodes').hide();

            }
        });

        $("#addressCountry").ready(function() {
            console.log('#addressCountry is ready');
            var addressCountry = '{$addressCountry}';

            if (addressCountry!=''){
                console.log('addressCountry should be set to ' + addressCountry);
                $("#addressCountry").val(addressCountry);
                $("#addressCountry").trigger('change');

            }
        });

        $( "#addressCountry" ).on( "change", function() {

            function normalize(){
                $( "#addressStyleIndicator" ).html( '<i class="fas fa-globe fa-sm fa-fw text-TKE m-2"></i>&nbsp;Generic AddressStyle&nbsp;<small class="text-muted m-2">COUNTRY - STATE - PROVINCE - COUNTY - CITY' );
                $( "#addressCountryTitle" ).text( 'Country' );

                $( "#divState" ).show();
                $( "#divState" ).removeClass("col-4 col-6 col-12", 1000, "easeOutBounce" );
                $( "#divState" ).addClass("col-4", 1000, "easeOutBounce" );
                $( "#stateTitle" ).text( 'State' );
                $( "#stateTitle" ).removeClass("text-TKE text-stage", 1000, "easeOutBounce" );

                $( "#divProvince" ).show();
                $( "#divProvince" ).removeClass("col-4 col-6 col-12", 1000, "easeOutBounce" );
                $( "#divProvince" ).addClass("col-4", 1000, "easeOutBounce" );
                $( "#provinceTitle" ).text( 'Province' );
                $( "#provinceTitle" ).removeClass("text-TKE text-stage", 1000, "easeOutBounce" );

                $( "#divCounty" ).show();
                $( "#divCounty" ).removeClass("col-4 col-6 col-12", 1000, "easeOutBounce" );
                $( "#divCounty" ).addClass("col-4", 1000, "easeOutBounce" );
                $( "#countyTitle" ).text( 'County' );
                $( "#countyTitle" ).removeClass("text-TKE text-stage", 1000, "easeOutBounce" );

                $( "#divCity" ).show();
                $( "#divCity" ).removeClass("col-4 col-6 col-12", 1000, "easeOutBounce" );
                $( "#divCity" ).addClass("col-12", 1000, "easeOutBounce" );
                $( "#cityTitle" ).text( 'City' );
                $( "#cityTitle" ).removeClass("text-TKE text-stage", 1000, "easeOutBounce" );

            }

            function applyAddressStyleDM($country){
                $( "#addressStyleIndicator" ).html( '<span class="flag-icon flag-icon-' + $country.toLowerCase() + ' m-2"></span>&nbsp;' + $country + ' AddressStyle&nbsp;<small class="text-muted m-2">COUNTRY - <span class="text-TKE">DEPARTMENT (STATE == PROVINCE)</span> - <span class="text-stage">MUNICIPALITY (COUNTY == CITY)</span>' );

                $( "#divState" ).show();
                $( "#divState" ).removeClass("col-4", 1000, "easeOutBounce" );
                $( "#divState" ).addClass("col-6", 1000, "easeOutBounce" );
                $( "#stateTitle" ).text( 'Department' );
                $( "#stateTitle" ).addClass( "text-TKE", 1000, "easeOutBounce" );

                $( "#divProvince" ).hide();
                $( "#provinceTitle" ).text( 'Department' );
                $( "#provinceTitle" ).addClass( "text-TKE", 1000, "easeOutBounce" );

                $( "#divCounty" ).show();
                $( "#divCounty" ).removeClass("col-4", 1000, "easeOutBounce" );
                $( "#divCounty" ).addClass("col-6", 1000, "easeOutBounce" );
                $( "#countyTitle" ).text( 'Municipality' );
                $( "#countyTitle" ).addClass( "text-stage", 1000, "easeOutBounce" );

                $( "#divCity" ).hide();
                $( "#divCity" ).removeClass("col-4 col-6 col-12", 1000, "easeOutBounce" );
                $( "#divCity" ).addClass("text-stage col-6", 1000, "easeOutBounce" );
                $( "#cityTitle" ).text( 'Municipality' );
                $( "#cityTitle" ).addClass( "text-stage", 1000, "easeOutBounce" );
            }


            function applyAddressStyleProvinciaCanton($country){
                $( "#addressStyleIndicator" ).html( '<span class="flag-icon flag-icon-' + $country.toLowerCase() + ' m-2"></span>&nbsp;' + $country + ' AddressStyle&nbsp;<small class="text-muted m-2">COUNTRY - <span class="text-TKE">Provincia (PROVINCE == STATE)</span> - Cant�n (COUNTY) - Ciudad (CITY)' );

                $( "#divState" ).show();
                $( "#divState" ).removeClass("col-4", 1000, "easeOutBounce" );
                $( "#divState" ).addClass("col-6", 1000, "easeOutBounce" );
                $( "#stateTitle" ).text( 'Provincia' );
                $( "#stateTitle" ).addClass( "text-TKE", 1000, "easeOutBounce" );

                $( "#divProvince" ).hide();

                $( "#divCounty" ).show();
                $( "#divCounty" ).removeClass("col-4", 1000, "easeOutBounce" );
                $( "#divCounty" ).addClass("col-6", 1000, "easeOutBounce" );
                $( "#countyTitle" ).text( 'Cant�n' );

                $( "#divCity" ).show();
                $( "#divCity" ).removeClass("col-4 col-6 col-12", 1000, "easeOutBounce" );
                $( "#divCity" ).addClass("col-12", 1000, "easeOutBounce" );
                $( "#cityTitle" ).text( 'Ciudad' );
            }


            function applyAddressStylePPC($country){
                $( "#addressStyleIndicator" ).html( '<span class="flag-icon flag-icon-' + $country.toLowerCase() + ' m-2"></span>&nbsp;' + $country + ' AddressStyle&nbsp;<small class="text-muted m-2">COUNTRY - <span class="text-TKE">DEPARTMENT (STATE == PROVINCE)</span> - <span class="text-stage">MUNICIPALITY (COUNTY == CITY)</span>' );

                $( "#addressCountryTitle" ).text( 'Country' );

                $( "#divState" ).show();
                $( "#divState" ).removeClass("col-6", 1000, "easeOutBounce" );
                $( "#divState" ).addClass("col-6", 1000, "easeOutBounce" );
                $( "#stateTitle" ).text( 'Department' );
                $( "#stateTitle" ).addClass( "text-TKE", 1000, "easeOutBounce" );

                $( "#divProvince" ).hide();
                $( "#divCounty" ).hide();
                $( "#countyTitle" ).text( 'Province Code' );
                $( "#countyTitle" ).addClass( "text-stage", 1000, "easeOutBounce" );

                $( "#divCity" ).show();
                $( "#divCity" ).removeClass("col-4 col-6 col-12", 1000, "easeOutBounce" );
                $( "#divCity" ).addClass("text-stage col-6", 1000, "easeOutBounce" );
                $( "#cityTitle" ).text( 'Municipality' );
                $( "#cityTitle" ).addClass( "text-stage", 1000, "easeOutBounce" );
            }

            function applyAddressStyleSC($country){
                $( "#addressStyleIndicator" ).html( '<span class="flag-icon flag-icon-' + $country.toLowerCase()+ '  m-2"></span>&nbsp;'+ $country + '  AddressStyle&nbsp;<small class="text-muted m-2">COUNTRY - <span class="text-TKE">Estado (STATE == PROVINCE)</span> - <span class="text-stage">Ciudad (COUNTY==CITY)</span>' );
                $( "#addressCountryTitle" ).text( 'Country' );

                $( "#divState" ).show();
                $( "#divState" ).removeClass("col-6", 1000, "easeOutBounce" );
                $( "#divState" ).addClass("col-6", 1000, "easeOutBounce" );
                $( "#stateTitle" ).text( 'Estado' );
                $( "#stateTitle" ).addClass( "text-TKE", 1000, "easeOutBounce" );

                $( "#divProvince" ).hide();

                $( "#divCounty" ).hide();

                $( "#divCity" ).show();
                $( "#divCity" ).removeClass("col-12", 1000, "easeOutBounce" );
                $( "#divCity" ).addClass("col-6", 1000, "easeOutBounce" );
                $( "#cityTitle" ).text( 'Ciudad' );
                $( "#cityTitle" ).addClass( "text-stage", 1000, "easeOutBounce" );

            }

            function applyAddressStyleRegionProvinceCommuna($country){
                $( "#addressStyleIndicator" ).html( '<span class="flag-icon flag-icon-' + $country.toLowerCase()+ '  m-2"></span>&nbsp;'+ $country + '  AddressStyle&nbsp;<small class="text-muted m-2">Pais (COUNTRY) - <span class="text-muted">Regi�n (STATE)</span> - <span class="text-TKE">Provincia (PROVINCE==COUNTY)</span>- <span class="text-stage">Communa (CITY)</span>' );
                $( "#addressCountryTitle" ).text( 'Pais' );

                $( "#divState" ).show();
                $( "#divState" ).removeClass("col-6", 1000, "easeOutBounce" );
                $( "#divState" ).addClass("col-6", 1000, "easeOutBounce" );
                $( "#stateTitle" ).text( 'Regi�n' );
                $( "#provinceTitle" ).removeClass("text-TKE text-stage", 1000, "easeOutBounce" );
                $( "#stateTitle" ).addClass( "text-muted", 1000, "easeOutBounce" );

                $( "#divProvince" ).show();
                $( "#divProvince" ).removeClass("col-4 col-6 col-12", 1000, "easeOutBounce" );
                $( "#divProvince" ).addClass("col-6", 1000, "easeOutBounce" );
                $( "#provinceTitle" ).text( 'Provincia' );
                $( "#provinceTitle" ).removeClass("text-TKE text-stage", 1000, "easeOutBounce" );
                $( "#provinceTitle" ).addClass( "text-TKE", 1000, "easeOutBounce" );

                $( "#divCounty" ).hide();

                $( "#divCity" ).show();
                $( "#divCity" ).removeClass("col-12", 1000, "easeOutBounce" );
                $( "#divCity" ).addClass("col-12", 1000, "easeOutBounce" );
                $( "#cityTitle" ).text( 'Communa' );
                $( "#cityTitle" ).removeClass("text-TKE text-stage", 1000, "easeOutBounce" );
                $( "#cityTitle" ).addClass( "text-stage", 1000, "easeOutBounce" );

            }

            function applyAddressStyleRegionProvinceDistrict($country){
                $( "#addressStyleIndicator" ).html( '<span class="flag-icon flag-icon-' + $country.toLowerCase()+ '  m-2"></span>&nbsp;'+ $country + '  AddressStyle&nbsp;<small class="text-muted m-2">Pais (COUNTRY) - <span class="text-muted">Regi�n (STATE)</span> - <span class="text-TKE">Provincia (PROVINCE==COUNTY)</span>- <span class="text-stage">Distrito (CITY)</span>' );
                $( "#addressCountryTitle" ).text( 'Pais' );

                $( "#divState" ).show();
                $( "#divState" ).removeClass("col-6", 1000, "easeOutBounce" );
                $( "#divState" ).addClass("col-6", 1000, "easeOutBounce" );
                $( "#stateTitle" ).text( 'Regi�n' );
                $( "#provinceTitle" ).removeClass("text-TKE text-stage", 1000, "easeOutBounce" );
                $( "#stateTitle" ).addClass( "text-muted", 1000, "easeOutBounce" );

                $( "#divProvince" ).show();
                $( "#divProvince" ).removeClass("col-4 col-6 col-12", 1000, "easeOutBounce" );
                $( "#divProvince" ).addClass("col-6", 1000, "easeOutBounce" );
                $( "#provinceTitle" ).text( 'Provincia' );
                $( "#provinceTitle" ).removeClass("text-TKE text-stage", 1000, "easeOutBounce" );
                $( "#provinceTitle" ).addClass( "text-TKE", 1000, "easeOutBounce" );

                $( "#divCounty" ).hide();

                $( "#divCity" ).show();
                $( "#divCity" ).removeClass("col-12", 1000, "easeOutBounce" );
                $( "#divCity" ).addClass("col-12", 1000, "easeOutBounce" );
                $( "#cityTitle" ).text( 'Distrito' );
                $( "#cityTitle" ).removeClass("text-TKE text-stage", 1000, "easeOutBounce" );
                $( "#cityTitle" ).addClass( "text-stage", 1000, "easeOutBounce" );

            }

            function applyAddressStyleRegionProvinceCabecera($country){
                $( "#addressStyleIndicator" ).html( '<span class="flag-icon flag-icon-' + $country.toLowerCase()+ '  m-2"></span>&nbsp;'+ $country + '  AddressStyle&nbsp;<small class="text-muted m-2">Pais (COUNTRY) - <span class="text-stage">Provincia (STATE==PROVINCE)</span> - <span class="text-TKE">Departamento (COUNTY)</span>- <span class="text-stage">Cabecera (CITY)</span>' );
                $( "#addressCountryTitle" ).text( 'Pais' );

                $( "#divState" ).show();
                $( "#divState" ).removeClass("col-6", 1000, "easeOutBounce" );
                $( "#divState" ).addClass("col-6", 1000, "easeOutBounce" );
                $( "#stateTitle" ).text( 'Provincia' );
                $( "#stateTitle" ).removeClass("text-TKE text-stage text-TKE text-stage", 1000, "easeOutBounce" );
                $( "#stateTitle" ).addClass( "text-stage", 1000, "easeOutBounce" );

                $( "#divProvince" ).hide();

                $( "#divCounty" ).show();
                $( "#divCounty" ).removeClass("col-4", 1000, "easeOutBounce" );
                $( "#divCounty" ).addClass("col-6", 1000, "easeOutBounce" );
                $( "#countyTitle" ).text( 'Departamento' );
                $( "#countyTitle" ).removeClass("text-TKE text-stage text-TKE text-stage", 1000, "easeOutBounce" );
                $( "#countyTitle" ).addClass( "text-TKE", 1000, "easeOutBounce" );

                $( "#divCity" ).show();
                $( "#divCity" ).removeClass("col-12", 1000, "easeOutBounce" );
                $( "#divCity" ).addClass("col-12", 1000, "easeOutBounce" );
                $( "#cityTitle" ).text( 'Cabecera' );
                $( "#cityTitle" ).removeClass("text-TKE text-stage", 1000, "easeOutBounce" );
                $( "#cityTitle" ).addClass( "text-stage", 1000, "easeOutBounce" );

            }

            function applyAddressStyleDepartamentoMunicipality($country){
                $( "#addressStyleIndicator" ).html( '<span class="flag-icon flag-icon-' + $country.toLowerCase()+ '  m-2"></span>&nbsp;'+ $country + '  AddressStyle&nbsp;<small class="text-muted m-2">Pais (COUNTRY) - <span class="text-danger">Departamento (STATE==PROVINCE)</span> - <span class="text-info">Municipalidad (COUNTY==CITY)</span>' );
                $( "#addressCountryTitle" ).text( 'Pais' );

                $( "#divState" ).show();
                $( "#divState" ).removeClass("col-6", 1000, "easeOutBounce" );
                $( "#divState" ).addClass("col-6", 1000, "easeOutBounce" );
                $( "#stateTitle" ).text( 'Departamento' );
                $( "#stateTitle" ).removeClass("text-TKE text-stage text-TKE text-stage", 1000, "easeOutBounce" );
                $( "#stateTitle" ).addClass( "text-stage", 1000, "easeOutBounce" );

                $( "#divProvince" ).hide();

                $( "#divCounty" ).show();
                $( "#divCounty" ).removeClass("col-6", 1000, "easeOutBounce" );
                $( "#divCounty" ).addClass("col-6", 1000, "easeOutBounce" );
                $( "#countyTitle" ).text( 'Municipalidad' );
                $( "#countyTitle" ).removeClass("text-TKE text-stage", 1000, "easeOutBounce" );
                $( "#countyTitle" ).addClass( "text-stage", 1000, "easeOutBounce" );

                $( "#divCity" ).hide();

            }

            function applyAddressStyleDepartamentoMunicipalityUY($country){
                $( "#addressStyleIndicator" ).html( '<span class="flag-icon flag-icon-' + $country.toLowerCase()+ '  m-2"></span>&nbsp;'+ $country + '  AddressStyle&nbsp;<small class="text-muted m-2">Pais (COUNTRY) - <span class="text-danger">Departamento (STATE==PROVINCE)</span> - <span class="text-info">Municipalidad (COUNTY==CITY)</span>' );
                $( "#addressCountryTitle" ).text( 'Pais' );

                $( "#divState" ).show();
                $( "#divState" ).removeClass("col-6", 1000, "easeOutBounce" );
                $( "#divState" ).addClass("col-6", 1000, "easeOutBounce" );
                $( "#stateTitle" ).text( 'Departamento' );
                $( "#stateTitle" ).removeClass("text-TKE text-stage text-TKE text-stage", 1000, "easeOutBounce" );
                $( "#stateTitle" ).addClass( "text-stage", 1000, "easeOutBounce" );

                $( "#divProvince" ).hide();
                $( "#divCounty" ).hide();

                $( "#divCity" ).show();
                $( "#divCity" ).removeClass("col-12", 1000, "easeOutBounce" );
                $( "#divCity" ).addClass("col-12", 1000, "easeOutBounce" );
                $( "#cityTitle" ).text( 'Municipalidad' );
                $( "#cityTitle" ).removeClass("text-TKE text-stage", 1000, "easeOutBounce" );
                $( "#cityTitle" ).addClass( "text-stage", 1000, "easeOutBounce" );


            }

            normalize();

            if ($(this).val()=='PA'){
                applyAddressStylePPC($(this).val());

            }else if ($(this).val()=='CO'){
                applyAddressStyleSC($(this).val())

            }else if ($(this).val()=='MX'){
                applyAddressStyleSC($(this).val())

            }else if ($(this).val()=='SV'){
                applyAddressStyleDM($(this).val());

            }else if ($(this).val()=='HN'){
                applyAddressStyleDM($(this).val());

            }else if ($(this).val()=='GT'){
                applyAddressStyleDM($(this).val());

            }else if ($(this).val()=='CR'){
                applyAddressStyleProvinciaCanton($(this).val());

            }else if ($(this).val()=='NI'){
                applyAddressStyleDM($(this).val());

            }else if ($(this).val()=='CL'){
                applyAddressStyleRegionProvinceCommuna($(this).val());

            }else if ($(this).val()=='PE'){
                applyAddressStyleRegionProvinceDistrict($(this).val());

            }else if ($(this).val()=='AR'){
                applyAddressStyleRegionProvinceCabecera($(this).val());

            }else if ($(this).val()=='PY'){
                applyAddressStyleDepartamentoMunicipality($(this).val());

            }else if ($(this).val()=='UY'){
                applyAddressStyleDepartamentoMunicipalityUY($(this).val());

            }else{
                normalize();
            }
        });


        $("#state").remoteChained({
            parents : "#addressCountry",
            url : "/laundry/Maps/ORAGeoCodes/getStates"
        });

        $("#province").remoteChained({
            parents : "#state, #addressCountry",
            url : "/laundry/Maps/ORAGeoCodes/getProvinces"
        });

        $("#county").remoteChained({
            parents : "#province, #addressCountry",
            url : "/laundry/Maps/ORAGeoCodes/getCounties"
        });

        $("#city").remoteChained({
            parents : "#county, #addressCountry",
            url : "/laundry/Maps/ORAGeoCodes/getCities"
        });

    </script>
