<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="navbar-nav mr-auto">
        </div>
        {foreach $countries as $country}
            {if $country!=null}
                <a class="btn btn-primary my-2 my-sm-0 m-1" target="_self" href="{base_url()}RefreshFF/ORA_GEO_CODES/{$country}" data-toggle="tooltip" data-placement="top" title="diagnostics ORA_GEO_CODES {$country}"><span class="flag-icon flag-icon-{strtolower($country)} flag-icon-squared"></span></a>
            {/if}
        {/foreach}
        &nbsp;
        <a href="{base_url()}Maps/ORAGeoCodes/create"><button class="btn btn-success my-2 my-sm-0 m-1">New Geo Code</button></a>
        <a href="{base_url()}Maps/ORAGeoCodes/testJSON"><button class="btn btn-success my-2 my-sm-0 m-1"><i class="fas fa-map-marker-alt fa-sm fa-fw text-white"></i> Test</button></a>
    </div>
</nav>
<br/>
<br/>
<br/>
<div class="tab-pane fade active show" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
        <h3>ORA Geography Codes</h3>
        <br/>
        <div class="row mb-3">
            <div class="col-9">
                <ul class="pagination">
                    {if $isPaginationNeeded}
                        <li class="page-item {if $currentPage==0}active{/if}">
                            <a class="page-link" href="{base_url()}{$listAction}0" aria-label="First">
                                First
                            </a>
                        </li>
                        {for $foo=1 to ($lastPage-1)}
                            <li class="page-item {if $currentPage==$foo}active{/if}"><a class="page-link" href="{base_url()}{$listAction}{$foo}">{$foo}</a></li>
                        {/for}
                        <li class="page-item {if $currentPage==$lastPage}active{/if}">
                            <a class="page-link" href="{base_url()}{$listAction}{$lastPage}" aria-label="Last">
                                Last
                            </a>
                        </li>
                    {/if}
                </ul>
            </div>
            <div class="col-3">
            </div>
        </div>
        <br/>
        <table  id="geoTable" class="table table-hover table-sm">
            <thead>
            <tr>
                <th></th>
                <th scope="col">Country Code</th>
                <th scope="col">State</th>
                <th scope="col">Province</th>
                <th scope="col">County</th>
                <th scope="col">City</th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$oraGeoCodes item=o}
                <tr>
                    <td>
                        <a href="{base_url()}Maps/ORAGeoCodes/remove/{$o.id}"><i class="text-primary fas fa-trash fa-lg fa-fw"></i></a>
                    </td>
                    <td><span class="flag-icon flag-icon-{strtolower($o.countryCode)}"></span> {$o.countryCode}</td>
                    <td>{$o.state}</td>
                    <td>{$o.province}</td>
                    <td>{$o.county}</td>
                    <td>{$o.city}</td>
                </tr>
            {/foreach}
            </tbody>
            <tfoot>
            <tr>
                <th></th>
                <th scope="col">Country Code</th>
                <th scope="col">State</th>
                <th scope="col">Province</th>
                <th scope="col">County</th>
                <th scope="col">City</th>
            </tr>
            </tfoot>
        </table>
        <script>
            $(document).ready(function() {
                var table = $('#geoTable').DataTable({
                    "lengthMenu": [[-1], [" page {$currentPage}"]]
                } );
                table.order( [ 0, 'desc' ] ).draw();
            } );
        </script>

    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>

</div>
