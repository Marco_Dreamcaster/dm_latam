<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><i class="fas fa-lg fa-bug fa-fw m-2 text-stage"></i>&nbsp;<span class="mt-2">{strtoupper($country)} {$title}</span>&nbsp;</h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Maps/ORAGeoCodes/testJSON" data-toggle="tooltip" data-placement="top" title="new JSON combos for ORA Geo Codes"><i class="fas fa-map-marker-alt fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Maps/TKEFamilyLines/testJSON" data-toggle="tooltip" data-placement="top" title="new JSON combos for SCC Family Lines"><i class="fas fa-table fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-success" target="_blank" href="{base_url()}scripts/00.COM/master/01.Customers/01.IDL/TKE_LA_IDL_FD_02_CUSTOMERS_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="latest specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>



<div class="tab-content" id="pills-tabContent">
    <div class="alert-info p-4">
        <p>This is an example of how different addressStyles are handled.</p>
        <p>Values initially selected from the controller: {$addressCountry} - {$province} - {$state} - {$county} - {$city}.</p>
        <span class="flag-icon flag-icon-pa" title="PA"></span>
        <span class="flag-icon flag-icon-co" title="CO"></span>
        <span class="flag-icon flag-icon-mx" title="MX"></span>
        <span class="flag-icon flag-icon-sv" title="SV"></span>
        <span class="flag-icon flag-icon-gt" title="GT"></span>
        <span class="flag-icon flag-icon-cr" title="CR"></span>
        <span class="flag-icon flag-icon-ni" title="NI"></span>
        <span class="flag-icon flag-icon-hn" title="HN"></span>
        <span class="flag-icon flag-icon-cl" title="CL"></span>
        <span class="flag-icon flag-icon-pe" title="PE"></span>
        <span class="flag-icon flag-icon-ar" title="AR"></span>
        <span class="flag-icon flag-icon-py" title="PY"></span>
        <span class="flag-icon flag-icon-uy" title="UY"></span>

    </div>
    <form class="m-2">
        {include file='./AddressStyleAwareWidget.tpl'}
    </form>
</div>

<br/>
<br/>
<br/>
<br/>
