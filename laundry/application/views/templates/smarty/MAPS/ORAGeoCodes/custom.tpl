<h3>{$title}</h3>
<h4> <small>*all fields are mandatory</small></h4>
<br/>
<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-group row">
        <div class="col-sm-2 my-1">
            <div class="form-group">
                <label for="countryCode">Country Code <small id="countryCodeHelp" class="form-text text-muted"><a href="https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2">2-digit ISO</a></small></label>
                <input type="text" class="form-control" id="countryCode" name="countryCode" aria-describedby="countryCodeHelp" value="{utf8_decode ($countryCode)}">
                <small id="errorCountryCode" class="form-text text-danger">{$errorCountryCode}</small>
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <div class="form-group">
                <label for="countryName">Country Name <small id="countryNameHelp" class="form-text text-muted">name can be localized</small></label>
                <input type="text" class="form-control" id="countryName" name="countryName" aria-describedby="countryNameHelp" value="{utf8_decode ($countryName)}">
                <small id="errorCountryName" class="form-text text-danger">{$errorCountryName}</small>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-2 my-1">
            <div class="form-group">
                <label for="stateCode">State Code <small id="stateCodeHelp" class="form-text text-muted">(eg.  BOG-01)</small></label>
                <input type="text" class="form-control" id="stateCode" name="stateCode" aria-describedby="stateCodeHelp" value="{utf8_decode ($stateCode)}">
                <small id="errorStateCode" class="form-text text-danger">{$errorStateCode}</small>
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <div class="form-group">
                <label for="stateName">State Name <small id="stateNameHelp" class="form-text text-muted">when in doubt, repeat values for state, province, and county</small></label>
                <input type="text" class="form-control" id="stateName" name="stateName" aria-describedby="stateNameHelp" value="{utf8_decode ($stateName)}">
                <small id="errorStateName" class="form-text text-danger">{$errorStateName}</small>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-2 my-1">
            <div class="form-group">
                <label for="provinceCode">Province Code <small id="provinceCodeHelp" class="form-text text-muted">(eg.  BOG-01)</small></label>
                <input type="text" class="form-control" id="provinceCode" name="provinceCode" aria-describedby="provinceCodeHelp" value="{utf8_decode ($provinceCode)}">
                <small id="errorProvinceCode" class="form-text text-danger">{$errorProvinceCode}</small>
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <div class="form-group">
                <label for="provinceName">Province Name <small id="provinceNameHelp" class="form-text text-muted">when in doubt, repeat values for state, province, and county</small></label>
                <input type="text" class="form-control" id="provinceName" name="provinceName" aria-describedby="provinceNameHelp" value="{utf8_decode ($provinceName)}">
                <small id="errorProvinceName" class="form-text text-danger">{$errorProvinceName}</small>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-2 my-1">
            <div class="form-group">
                <label for="stateCode">County Code <small id="stateCodeHelp" class="form-text text-muted">(eg.  BOG-01)</small></label>
                <input type="text" class="form-control" id="countyCode" name="countyCode"  value="{utf8_decode ($countyCode)}">
                <small id="errorCountyCode" class="form-text text-danger">{$errorCountyCode}</small>
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <div class="form-group">
                <label for="stateName">County Name <small id="countyNameHelp" class="form-text text-muted">when in doubt, repeat values for state, province, and county</small></label>
                <input type="text" class="form-control" id="countyName" name="countyName" aria-describedby="countyNameHelp" value="{utf8_decode ($countyName)}">
                <small id="errorCountyName" class="form-text text-danger">{$errorCountyName}</small>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-6 my-1">
            <div class="form-group">
                <label for="cityName">City Name <small id="cityNameHelp" class="form-text text-muted">name is usually localized</small></label>
                <input type="text" class="form-control" id="cityName" name="cityName" aria-describedby="cityNameHelp" value="{utf8_decode ($cityName)}">
                <small id="errorCityName" class="form-text text-danger">{$errorCityName}</small>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-3 my-1">
            <button type="submit" class="btn btn-primary">{$actionLabel}</button>
            <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}Maps/ORAGeoCodes">Cancel</a>
        </div>
    </div>
</form>
