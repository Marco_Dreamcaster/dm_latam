<h3>{$title}</h3>
<form action="{base_url()}{$action}" method="post">
    <div class="form-group">
        <small id="codigoHelp" class="form-text text-muted">Codigo</small>
        <label for="codigo">Codigo</label>
        <input type="text" class="form-control" id="codigo" name="codigo" aria-describedby="codigoHelp" value="{$codigo}" placeholder="Codigo...">
        <small id="errorCodigo" class="form-text text-danger">{$errorCodigo}</small>
    </div>
    <div class="form-group">
        <small id="denominacionHelp" class="form-text text-muted">Denominacion</small>
        <label for="denominacion">Denominacion</label>
        <input type="text" class="form-control" id="denominacion" name="denominacion" aria-describedby="denominacionHelp" value="{$denominacion}" placeholder="Denominacion...">
        <small id="errorDenominacion" class="form-text text-danger">{$errorDenominacion}</small>
    </div>
    <div class="form-group">
        <small id="lineaHelp" class="form-text text-muted">Linea</small>
        <label for="linea">Linea</label>
        <input type="text" class="form-control" id="linea" name="linea" aria-describedby="lineaHelp" value="{$linea}" placeholder="Linea...">
        <small id="errorLinea" class="form-text text-danger">{$errorLinea}</small>
    </div>
    <div class="form-group">
        <small id="typeHelp" class="form-text text-muted">Type</small>
        <label for="type">Type</label>
        <input type="text" class="form-control" id="type" name="type" aria-describedby="typeHelp" value="{$type}" placeholder="Type...">
        <small id="errorType" class="form-text text-danger">{$errorType}</small>
    </div>
    <div class="form-group">
        <small id="lineHelp" class="form-text text-muted">Line</small>
        <label for="line">Line</label>
        <input type="text" class="form-control" id="line" name="line" aria-describedby="lineHelp" value="{$line}" placeholder="Line...">
        <small id="errorLine" class="form-text text-danger">{$errorLine}</small>
    </div>
    <div class="form-group">
        <small id="configurationHelp" class="form-text text-muted">Configuration</small>
        <label for="configuration">Configuration</label>
        <input type="text" class="form-control" id="configuration" name="configuration" aria-describedby="configurationHelp" value="{$configuration}" placeholder="Project configuration...">
        <small id="errorConfiguration" class="form-text text-danger">{$errorConfiguration}</small>
    </div>
    <br/>
    <button type="submit" class="btn btn-primary">{$actionLabel}</button>
    <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}PA/MAPS/">Cancel</a>
</form>
<br/>
<br/>
<br/>

