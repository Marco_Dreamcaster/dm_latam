<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="navbar-nav mr-auto">
        </div>
        &nbsp;
        <div class="dropdown">
            <button class="btn btn-success  dropleft dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {$country} Projects
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="{base_url()}Projects">Cleansed</a>
                <a class="dropdown-item" href="{base_url()}Projects/Maps/PFF">PFF</a>
            </div>
        </div>

        &nbsp;
        <a href="{base_url()}Projects/Maps/createPFF"><button class="btn btn-success my-2 my-sm-0">New Project Flex Field</button></a>
        &nbsp;
    </div>
</nav>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade active show" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
        <h3>Project Flex Fields</h3>
        <br/>
        <table class="table table-hover table-sm">
            <thead>
            <tr>
                <th></th>
                <th scope="col">CODIGO</th>
                <th scope="col">DENOMINACION</th>
                <th scope="col">LINEA</th>
                <th scope="col">TYPE</th>
                <th scope="col">LINE</th>
                <th scope="col">CONFIGURATION</th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$projectFlexFields item=f}
                <tr>
                    <td>
                        <a href="{base_url()}Projects/Maps/removePFF/{urlencode({$f.type})}/{urlencode({$f.configuration})}/{urlencode({$f.line})}"><i class="text-primary fas fa-trash fa-lg fa-fw"></i></a>
                    </td>
                    <td>{$f.codigo}</td>
                    <td>{$f.denominacion}</td>
                    <td>{$f.linea}</td>
                    <td>{$f.type}</td>
                    <td>{$f.line}</td>
                    <td>{$f.configuration}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</div>
