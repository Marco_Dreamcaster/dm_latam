<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><i class="fas fa-lg fa-bug fa-fw m-2 text-stage"></i>&nbsp;<span class="mt-2">{strtoupper($country)} {$title}</span>&nbsp;</h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Maps/ORAGeoCodes/testJSON" data-toggle="tooltip" data-placement="top" title="new JSON combos for ORA Geo Codes"><i class="fas fa-map-marker-alt fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Maps/TKEFamilyLines/testJSON" data-toggle="tooltip" data-placement="top" title="new JSON combos for SCC Family Lines"><i class="fas fa-table fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>
<div id="pills-tabContent" class="m-2">
    <h4>Fresh combos (no pre-set values)</h4>
    <div class="alert-info p-2">
        <p>This is basically the same case fresh out of the box, as seen in <a href="https://github.com/tuupola/jquery_chained" target="_blank">JQuery Chained</a> sample page.</p>
        <p>No values are previously selected from the controller.</p>
    </div>
    <form class="m-2">
        <div class="form-row align-items-center">
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Family</div>
                    </div>
                    <select class="form-control" id="familyJSON" name="familyJSON" aria-describedby="familyJSONHelp">
                        <option value="">--</option>
                        {foreach from=$sccFamilies item=f}
                            <option value="{$f['family']}">{$f['family']} - {$f['familyESA']}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Sub Family</div>
                    </div>
                    <select class="form-control" id="subFamilyJSON" name="subFamilyJSON" aria-describedby="subFamilyJSONHelp">
                        <option value="">--</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Line</div>
                    </div>
                    <select class="form-control" id="lineJSON" name="lineJSON" aria-describedby="lineJSONHelp">
                        <option value="">--</option>
                    </select>
                </div>
            </div>
        </div>
    </form>
    <br/>
    <script>
        $("#subFamilyJSON").remoteChained({
            parents : "#familyJSON",
            url : "/laundry/Maps/TKEFamilyLines/getSubFamilies",
            data: function (json) {
                console.log('#subFamilyJSON list is refreshed');
                return json;
            }
        });
        $("#lineJSON").remoteChained({
            parents : "#subFamilyJSON",
            url : "/laundry/Maps/TKEFamilyLines/getLines",
            data: function (json) {
                console.log('#lineJSON list is refreshed');
                return json;
            }
        });
    </script>
</div>

<div class="tab-content" id="pills-tabContent">
    <h4>Editing combos (pre-set values from Controller)</h4>
    <div class="alert-info p-2">
        <p>This is a slightly changed version of the example above.</p>
        <p>Values are previously selected from the controller: {$family} - {$subFamily} - {$line}.</p>
    </div>
    <form class="m-2">
        <div class="form-row align-items-center">
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Family</div>
                    </div>
                    <select class="form-control" id="family" name="family" aria-describedby="familyHelp">
                        <option value="">--</option>
                        {foreach from=$sccFamilies item=f}
                            <option value="{$f['family']}">{$f['family']} - {$f['familyESA']}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Sub Family</div>
                    </div>
                    <select class="form-control" id="subFamily" name="subFamily" aria-describedby="subFamilyHelp">
                        <option value="">--</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Line</div>
                    </div>
                    <select class="form-control" id="line" name="line" aria-describedby="lineHelp">
                        <option value="">--</option>
                    </select>
                </div>
            </div>
        </div>
    </form>
    <br/>
    <script>
        $("#family").ready(function() {
            console.log('#family is ready');
            var family = '{$family}';

            if (family!=''){
                console.log('family should be set to ' + family);
                $("#family").val(family);
                $("#family").trigger('change');

            }
        });

        $("#subFamily").remoteChained({
            parents : "#family",
            url : "/laundry/Maps/TKEFamilyLines/getSubFamilies",
            data: function (json) {
                console.log('#subFamily list is refreshed');
                return json;
            }
        });
        $("#line").remoteChained({
            parents : "#subFamily",
            url : "/laundry/Maps/TKEFamilyLines/getLines",
            data: function (json) {
                console.log('#line list is refreshed');
                return json;
            }
        });

        $( "#subFamily" ).one( "change", function() {
            var subFamily = '{$subFamily}';

            if (subFamily!=''){
                console.log('initial subFamily should be set to ' + subFamily);
                $("#subFamily").val(subFamily);
                $("#subFamily").trigger('change');

            }
        });

        $("#line" ).one( "change", function() {
            var line = '{$line}';

            if (line!=''){
                console.log('initial line should be set to ' + line);
                $("#line").val(line);
                $("#line").trigger('change');

            }
        });
    </script>
</div>
<br/>
<br/>
<br/>
<br/>
