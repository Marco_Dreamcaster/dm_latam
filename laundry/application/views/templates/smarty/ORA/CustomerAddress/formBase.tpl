<form action="{base_url()}{$action}" method="post">
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>
    <div class="form-group">
        <label for="customerName">Customer Name <small id="customerNameHelp" class="form-text text-muted">(Name of the customer)</small></label>
        <input type="text" class="form-control" id="customerName" name="customerName" aria-describedby="customerNameHelp" value="{$customerName}" placeholder="Customer Name...">
        <small id="errorCustomerName" class="form-text text-danger">{$errorCustomerName}</small>
    </div>
    <div class="form-group">
        <label for="origSystemCustomerRef">Orig System Customer Ref <small id="origSystemCustomerRefHelp" class="form-text text-muted">(Original Customer Number from Legacy,  use the...)</small></label>
        <input type="text" class="form-control" id="origSystemCustomerRef" name="origSystemCustomerRef" aria-describedby="origSystemCustomerRefHelp" value="{$origSystemCustomerRef}" placeholder="Orig System Customer Ref...">
        <small id="errorOrigSystemCustomerRef" class="form-text text-danger">{$errorOrigSystemCustomerRef}</small>
    </div>
    <div class="form-group">
        <label for="origSystemAdressReference">Orig System Adress Reference <small id="origSystemAdressReferenceHelp" class="form-text text-muted">(Please provide an unique reference to identify...)</small></label>
        <input type="text" class="form-control" id="origSystemAdressReference" name="origSystemAdressReference" aria-describedby="origSystemAdressReferenceHelp" value="{$origSystemAdressReference}" placeholder="Orig System Adress Reference...">
        <small id="errorOrigSystemAdressReference" class="form-text text-danger">{$errorOrigSystemAdressReference}</small>
    </div>
    <div class="form-group">
        <label for="siteUseCode">Site Use Code <small id="siteUseCodeHelp" class="form-text text-muted">(Business purpose of customer address, either...)</small></label>
        <input type="text" class="form-control" id="siteUseCode" name="siteUseCode" aria-describedby="siteUseCodeHelp" value="{$siteUseCode}" placeholder="Site Use Code...">
        <small id="errorSiteUseCode" class="form-text text-danger">{$errorSiteUseCode}</small>
    </div>
    <div class="form-group">
        <label for="primarySiteUseFlag">PrimarySiteUseFlag</label><br/>
        <small id="primarySiteUseFlagHelp" class="form-text text-muted">(Specify the primay business purpose address,...)</small>
        <input type="checkbox" {($primarySiteUseFlag==true)?'checked':''} id="primarySiteUseFlag" name="primarySiteUseFlag">
    </div>
    <div class="form-group">
        <label for="location">Location <small id="locationHelp" class="form-text text-muted">(A shorthand (descriptive) name for the business...)</small></label>
        <input type="text" class="form-control" id="location" name="location" aria-describedby="locationHelp" value="{$location}" placeholder="Location...">
        <small id="errorLocation" class="form-text text-danger">{$errorLocation}</small>
    </div>
    <div class="form-group">
        <label for="billToOrigAddressRef">BillToOrigAddressRef</label><br/>
        <small id="billToOrigAddressRefHelp" class="form-text text-muted">(The address reference of Bill-To linked to the...)</small>
        <input type="checkbox" {($billToOrigAddressRef==true)?'checked':''} id="billToOrigAddressRef" name="billToOrigAddressRef">
    </div>
    <div class="form-group">
        <label for="addressLine1">Address Line1 <small id="addressLine1Help" class="form-text text-muted">(Address line 1 for customer address  (Street...)</small></label>
        <input type="text" class="form-control" id="addressLine1" name="addressLine1" aria-describedby="addressLine1Help" value="{$addressLine1}" placeholder="Address Line1...">
        <small id="errorAddressLine1" class="form-text text-danger">{$errorAddressLine1}</small>
    </div>
    <div class="form-group">
        <label for="addressLine2">Address Line2 <small id="addressLine2Help" class="form-text text-muted">(Address line 2 for customer address  (Possible...)</small></label>
        <input type="text" class="form-control" id="addressLine2" name="addressLine2" aria-describedby="addressLine2Help" value="{$addressLine2}" placeholder="Address Line2...">
        <small id="errorAddressLine2" class="form-text text-danger">{$errorAddressLine2}</small>
    </div>
    <div class="form-group">
        <label for="addressLine3">Address Line3 <small id="addressLine3Help" class="form-text text-muted">(Additional address line when needed)</small></label>
        <input type="text" class="form-control" id="addressLine3" name="addressLine3" aria-describedby="addressLine3Help" value="{$addressLine3}" placeholder="Address Line3...">
        <small id="errorAddressLine3" class="form-text text-danger">{$errorAddressLine3}</small>
    </div>
    <div class="form-group">
        <label for="addressLine4">Address Line4 <small id="addressLine4Help" class="form-text text-muted">(Additional address line when needed)</small></label>
        <input type="text" class="form-control" id="addressLine4" name="addressLine4" aria-describedby="addressLine4Help" value="{$addressLine4}" placeholder="Address Line4...">
        <small id="errorAddressLine4" class="form-text text-danger">{$errorAddressLine4}</small>
    </div>
    <div class="form-group">
        <label for="addressLinesPhonetic">Address Lines Phonetic <small id="addressLinesPhoneticHelp" class="form-text text-muted">(Required at Colombia's Customer Address,  which...)</small></label>
        <input type="text" class="form-control" id="addressLinesPhonetic" name="addressLinesPhonetic" aria-describedby="addressLinesPhoneticHelp" value="{$addressLinesPhonetic}" placeholder="Address Lines Phonetic...">
        <small id="errorAddressLinesPhonetic" class="form-text text-danger">{$errorAddressLinesPhonetic}</small>
    </div>
    <div class="form-group">
        <label for="city">City <small id="cityHelp" class="form-text text-muted">(City of the customer addreses)</small></label>
        <input type="text" class="form-control" id="city" name="city" aria-describedby="cityHelp" value="{$city}" placeholder="City...">
        <small id="errorCity" class="form-text text-danger">{$errorCity}</small>
    </div>
    <div class="form-group">
        <label for="province">Province <small id="provinceHelp" class="form-text text-muted">(Province of the customer addreses)</small></label>
        <input type="text" class="form-control" id="province" name="province" aria-describedby="provinceHelp" value="{$province}" placeholder="Province...">
        <small id="errorProvince" class="form-text text-danger">{$errorProvince}</small>
    </div>
    <div class="form-group">
        <label for="state">State <small id="stateHelp" class="form-text text-muted">(State of the customer addreses)</small></label>
        <input type="text" class="form-control" id="state" name="state" aria-describedby="stateHelp" value="{$state}" placeholder="State...">
        <small id="errorState" class="form-text text-danger">{$errorState}</small>
    </div>
    <div class="form-group">
        <label for="county">County <small id="countyHelp" class="form-text text-muted">(State Code of the customer addreses)</small></label>
        <input type="text" class="form-control" id="county" name="county" aria-describedby="countyHelp" value="{$county}" placeholder="County...">
        <small id="errorCounty" class="form-text text-danger">{$errorCounty}</small>
    </div>
    <div class="form-group">
        <label for="postalCode">Postal Code <small id="postalCodeHelp" class="form-text text-muted">(Postal Code of City)</small></label>
        <input type="text" class="form-control" id="postalCode" name="postalCode" aria-describedby="postalCodeHelp" value="{$postalCode}" placeholder="Postal Code...">
        <small id="errorPostalCode" class="form-text text-danger">{$errorPostalCode}</small>
    </div>
    <div class="form-group">
        <label for="addressCountry">Address Country <small id="addressCountryHelp" class="form-text text-muted">(Country of the customer address.)</small></label>
        <input type="text" class="form-control" id="addressCountry" name="addressCountry" aria-describedby="addressCountryHelp" value="{$addressCountry}" placeholder="Address Country...">
        <small id="errorAddressCountry" class="form-text text-danger">{$errorAddressCountry}</small>
    </div>
    <div class="form-group">
        <label for="recevablesAccount">Recevables Account <small id="recevablesAccountHelp" class="form-text text-muted">(Reveivable Account (optional), only applicable...)</small></label>
        <input type="text" class="form-control" id="recevablesAccount" name="recevablesAccount" aria-describedby="recevablesAccountHelp" value="{$recevablesAccount}" placeholder="Recevables Account...">
        <small id="errorRecevablesAccount" class="form-text text-danger">{$errorRecevablesAccount}</small>
    </div>
    <div class="form-group">
        <label for="revenueAccount">Revenue Account <small id="revenueAccountHelp" class="form-text text-muted">(Revenue Account (optional), only applicable to...)</small></label>
        <input type="text" class="form-control" id="revenueAccount" name="revenueAccount" aria-describedby="revenueAccountHelp" value="{$revenueAccount}" placeholder="Revenue Account...">
        <small id="errorRevenueAccount" class="form-text text-danger">{$errorRevenueAccount}</small>
    </div>
    <div class="form-group">
        <label for="taxAccount">Tax Account <small id="taxAccountHelp" class="form-text text-muted">(Tax Account (optional), only applicable to a...)</small></label>
        <input type="text" class="form-control" id="taxAccount" name="taxAccount" aria-describedby="taxAccountHelp" value="{$taxAccount}" placeholder="Tax Account...">
        <small id="errorTaxAccount" class="form-text text-danger">{$errorTaxAccount}</small>
    </div>
    <div class="form-group">
        <label for="freightAccount">Freight Account <small id="freightAccountHelp" class="form-text text-muted">(Freight Account (optional), only applicable to...)</small></label>
        <input type="text" class="form-control" id="freightAccount" name="freightAccount" aria-describedby="freightAccountHelp" value="{$freightAccount}" placeholder="Freight Account...">
        <small id="errorFreightAccount" class="form-text text-danger">{$errorFreightAccount}</small>
    </div>
    <div class="form-group">
        <label for="clearingAccount">Clearing Account <small id="clearingAccountHelp" class="form-text text-muted">(Clearing Account (optional), only applicable to...)</small></label>
        <input type="text" class="form-control" id="clearingAccount" name="clearingAccount" aria-describedby="clearingAccountHelp" value="{$clearingAccount}" placeholder="Clearing Account...">
        <small id="errorClearingAccount" class="form-text text-danger">{$errorClearingAccount}</small>
    </div>
    <div class="form-group">
        <label for="unbilledReceivablesAccount">Unbilled Receivables Account <small id="unbilledReceivablesAccountHelp" class="form-text text-muted">(Unbilled Reveivable Account (optional), only...)</small></label>
        <input type="text" class="form-control" id="unbilledReceivablesAccount" name="unbilledReceivablesAccount" aria-describedby="unbilledReceivablesAccountHelp" value="{$unbilledReceivablesAccount}" placeholder="Unbilled Receivables Account...">
        <small id="errorUnbilledReceivablesAccount" class="form-text text-danger">{$errorUnbilledReceivablesAccount}</small>
    </div>
    <div class="form-group">
        <label for="unearnedRevenueAccount">Unearned Revenue Account <small id="unearnedRevenueAccountHelp" class="form-text text-muted">(Unearned Revenue Account (optional), only...)</small></label>
        <input type="text" class="form-control" id="unearnedRevenueAccount" name="unearnedRevenueAccount" aria-describedby="unearnedRevenueAccountHelp" value="{$unearnedRevenueAccount}" placeholder="Unearned Revenue Account...">
        <small id="errorUnearnedRevenueAccount" class="form-text text-danger">{$errorUnearnedRevenueAccount}</small>
    </div>
    <div class="form-group">
        <label for="paymentMethodName">Payment Method Name <small id="paymentMethodNameHelp" class="form-text text-muted">(Specify the payment method the customer site...)</small></label>
        <input type="text" class="form-control" id="paymentMethodName" name="paymentMethodName" aria-describedby="paymentMethodNameHelp" value="{$paymentMethodName}" placeholder="Payment Method Name...">
        <small id="errorPaymentMethodName" class="form-text text-danger">{$errorPaymentMethodName}</small>
    </div>
    <div class="form-group">
        <label for="primaryFlag">PrimaryFlag</label><br/>
        <small id="primaryFlagHelp" class="form-text text-muted">("Enter 'Y' or 'N' to indicate whether this is...)</small>
        <input type="checkbox" {($primaryFlag==true)?'checked':''} id="primaryFlag" name="primaryFlag">
    </div>
    <div class="form-group">
        <label for="customerProfile">Customer Profile <small id="customerProfileHelp" class="form-text text-muted">(The profile will be populated in the customer's...)</small></label>
        <input type="text" class="form-control" id="customerProfile" name="customerProfile" aria-describedby="customerProfileHelp" value="{$customerProfile}" placeholder="Customer Profile...">
        <small id="errorCustomerProfile" class="form-text text-danger">{$errorCustomerProfile}</small>
    </div>
    <div class="form-group">
        <label for="siteUseTaxReference">Site Use Tax Reference <small id="siteUseTaxReferenceHelp" class="form-text text-muted">("Tax registration number for this customer...)</small></label>
        <input type="text" class="form-control" id="siteUseTaxReference" name="siteUseTaxReference" aria-describedby="siteUseTaxReferenceHelp" value="{$siteUseTaxReference}" placeholder="Site Use Tax Reference...">
        <small id="errorSiteUseTaxReference" class="form-text text-danger">{$errorSiteUseTaxReference}</small>
    </div>
    <div class="form-group">
        <label for="timeZone">Time Zone <small id="timeZoneHelp" class="form-text text-muted">(Time zone of the customer site, required for...)</small></label>
        <input type="text" class="form-control" id="timeZone" name="timeZone" aria-describedby="timeZoneHelp" value="{$timeZone}" placeholder="Time Zone...">
        <small id="errorTimeZone" class="form-text text-danger">{$errorTimeZone}</small>
    </div>
    <div class="form-group">
        <label for="language">Language <small id="languageHelp" class="form-text text-muted">(Language for documentation to customers,...)</small></label>
        <input type="text" class="form-control" id="language" name="language" aria-describedby="languageHelp" value="{$language}" placeholder="Language...">
        <small id="errorLanguage" class="form-text text-danger">{$errorLanguage}</small>
    </div>
    <div class="form-group">
        <label for="gdfAddressAttribute8">Gdf Address Attribute8 <small id="gdfAddressAttribute8Help" class="form-text text-muted">(Optional, used by Argentina only)</small></label>
        <input type="text" class="form-control" id="gdfAddressAttribute8" name="gdfAddressAttribute8" aria-describedby="gdfAddressAttribute8Help" value="{$gdfAddressAttribute8}" placeholder="Gdf Address Attribute8...">
        <small id="errorGdfAddressAttribute8" class="form-text text-danger">{$errorGdfAddressAttribute8}</small>
    </div>
    <div class="form-group">
        <label for="gdfSiteUseAttribute9">Gdf Site Use Attribute9 <small id="gdfSiteUseAttribute9Help" class="form-text text-muted">("Flag to indicate whether Customer Site Profile...)</small></label>
        <input type="text" class="form-control" id="gdfSiteUseAttribute9" name="gdfSiteUseAttribute9" aria-describedby="gdfSiteUseAttribute9Help" value="{$gdfSiteUseAttribute9}" placeholder="Gdf Site Use Attribute9...">
        <small id="errorGdfSiteUseAttribute9" class="form-text text-danger">{$errorGdfSiteUseAttribute9}</small>
    </div>
    <div class="form-group">
        <label for="orgName">Org Name <small id="orgNameHelp" class="form-text text-muted">(check eXceL specs for further details)</small></label>
        <input type="text" class="form-control" id="orgName" name="orgName" aria-describedby="orgNameHelp" value="{$orgName}" placeholder="Org Name...">
        <small id="errorOrgName" class="form-text text-danger">{$errorOrgName}</small>
    </div>
    <div class="form-group">
        <label for="gdfAddressAttribute9">Gdf Address Attribute9 <small id="gdfAddressAttribute9Help" class="form-text text-muted">("Flag to indicate whether Customer Site Profile...)</small></label>
        <input type="text" class="form-control" id="gdfAddressAttribute9" name="gdfAddressAttribute9" aria-describedby="gdfAddressAttribute9Help" value="{$gdfAddressAttribute9}" placeholder="Gdf Address Attribute9...">
        <small id="errorGdfAddressAttribute9" class="form-text text-danger">{$errorGdfAddressAttribute9}</small>
    </div>
    <div class="form-group">
        <label for="gdfSiteUseAttribute8">Gdf Site Use Attribute8 <small id="gdfSiteUseAttribute8Help" class="form-text text-muted">(Optional, used by Argentina only)</small></label>
        <input type="text" class="form-control" id="gdfSiteUseAttribute8" name="gdfSiteUseAttribute8" aria-describedby="gdfSiteUseAttribute8Help" value="{$gdfSiteUseAttribute8}" placeholder="Gdf Site Use Attribute8...">
        <small id="errorGdfSiteUseAttribute8" class="form-text text-danger">{$errorGdfSiteUseAttribute8}</small>
    </div>
    <div class="form-group">
        <label for="economicActivity">Co Ext Attribute1 <small id="economicActivityHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="economicActivity" name="economicActivity" aria-describedby="economicActivityHelp" value="{$economicActivity}" placeholder="Co Ext Attribute1...">
        <small id="errorEconomicActivity" class="form-text text-danger">{$errorEconomicActivity}</small>
    </div>
    <br/>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>
</form>
<br/>
<br/>
<br/>
<br/>
<br/>
