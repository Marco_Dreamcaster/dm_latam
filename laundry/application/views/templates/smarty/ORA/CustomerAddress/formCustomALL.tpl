<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <label class="sr-only" for="inlineFormInputOrigSystemAdressReference">Orig System Address Ref</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Orig System Address Ref</div>
                </div>
                <input type="text" class="form-control" id="origSystemAdressReference" name="origSystemAdressReference" value="{$origSystemAdressReference}" disabled>
            </div>
            <small id="errorOrigSystemAdressReference" class="form-text text-warning">&nbsp;{$errorOrigSystemAdressReference}</small>
        </div>
        <div class="col-sm-6 my-1">
            <label class="sr-only" for="inlineFormInputOrigSystemCustomerRef">Orig System Customer Ref</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Orig System Customer Ref </div>
                </div>
                <input type="text" class="form-control" id="origSystemCustomerRef" name="origSystemCustomerRef" value="{$origSystemCustomerRef}" placeholder="Orig System Customer Ref" disabled>
            </div>
            <small id="errorOrigSystemCustomerRef" class="form-text text-warning">&nbsp;{$errorOrigSystemCustomerRef}</small>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-4 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Customer Profile</div>
                </div>
                <select class="form-control" id="customerProfile" name="customerProfile" aria-describedby="$descriptor.shortNameHelp" placeholder="Customer Profile">
                    <option {if $customerProfile==='TKE LATAM STANDARD'}selected{/if}>TKE LATAM STANDARD</option>
                    <option {if $customerProfile==='TKE LATAM COMPANY GROUP'}selected{/if}>TKE LATAM COMPANY GROUP</option>
                    <option {if $customerProfile==='TKE LATAM GOVERNMENT'}selected{/if}>TKE LATAM GOVERNMENT</option>
                    <option {if $customerProfile==='TKE LATAM DISTRIBUTOR'}selected{/if}>TKE LATAM DISTRIBUTOR</option>
                    <option {if $customerProfile==='TKE LATAM INTERCOMPANY'}selected{/if}>TKE LATAM INTERCOMPANY</option>
                    <option {if $customerProfile==='TKE LATAM VIP'}selected{/if}>TKE LATAM VIP</option>
                </select>
            </div>
            <small id="errorCustomerProfile" class="form-text text-danger">&nbsp;{$errorCustomerProfile}</small>
        </div>

        {if $country=='CO'}
            <div class="col-sm-1 col-md-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="gdfAddressAttribute8" name="gdfAddressAttribute8">
                        <option {if $gdfAddressAttribute8 ==='REGIMEN COMUN'}selected{/if} value="REGIMEN COMUN">REGIMEN COMUN</option>
                        <option {if $gdfAddressAttribute8 ==='REGIMEN COMUN AUTORETENEDOR'}selected{/if} value="REGIMEN COMUN AUTORETENEDOR">REGIMEN COMUN AUTORETENEDOR</option>
                        <option {if $gdfAddressAttribute8 ==='REGIMEN SIMPLIFICADO'}selected{/if} value="REGIMEN SIMPLIFICADO">REGIMEN SIMPLIFICADO</option>
                        <option {if $gdfAddressAttribute8 ==='GRAN CONTRIBUYENTE'}selected{/if} value="GRAN CONTRIBUYENTE">GRAN CONTRIBUYENTE</option>
                        <option {if $gdfAddressAttribute8 ==='GRAN CONTRIB. AUTORETENEDOR'}selected{/if} value="GRAN CONTRIB. AUTORETENEDOR">GRAN CONTRIB. AUTORETENEDOR</option>
                        <option {if $gdfAddressAttribute8 ==='ENTIDADES ESTATALES'}selected{/if} value="ENTIDADES ESTATALES">ENTIDADES ESTATALES</option>
                        <option {if $gdfAddressAttribute8 ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR</option>
                    </select>
                </div>
                <small id="errorGdfAddressAttribute8 " class="form-text text-danger">&nbsp;{$errorGdfAddressAttribute8}</small>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-co mr-2 ml-2" title="MX only"></div>
                    </div>
                    <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                        <option {if $paymentMethodName==='CO - BANCOLOMBIA'}selected{/if} value="CO - BANCOLOMBIA">CO - BANCOLOMBIA</option>
                        <option {if $paymentMethodName==='CO - BBVA MANUAL'}selected{/if} value="CO - BBVA MANUAL">CO - BBVA MANUAL</option>
                        <option {if $paymentMethodName==='CO - BBVA MANUAL - USD'}selected{/if} value="CO - BBVA MANUAL - USD">CO - BBVA MANUAL - USD</option>
                        <option {if $paymentMethodName==='CO - CITIBANK MANUAL'}selected{/if} value="CO - CITIBANK MANUAL">CO - CITIBANK MANUAL</option>
                        <option {if $paymentMethodName==='CO - CITIBANK MANUAL - EUR'}selected{/if} value="CO - CITIBANK MANUAL - EUR">CO - CITIBANK MANUAL - EUR</option>
                    </select>
                </div>
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            </div>
        {elseif $country=='MX'}
            <div class="col-sm-1 col-md-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="gdfAddressAttribute8" name="gdfAddressAttribute8">
                        <option {if $gdfAddressAttribute8 ==='CONTRIBUYENTE'}selected{/if} value="CONTRIBUYENTE">CONTRIBUYENTE</option>
                        <option {if $gdfAddressAttribute8 ==='AUTORETENEDOR'}selected{/if} value="AUTORETENEDOR">AUTORETENEDOR</option>
                        <option {if $gdfAddressAttribute8 ==='EXENTO'}selected{/if} value="EXENTO">EXENTO</option>
                        <option {if $gdfAddressAttribute8 ==='ZONA FRONTERIZA NORTE'}selected{/if} value="ZONA FRONTERIZA NORTE">ZONA FRONTERIZA NORTE</option>
                    </select>
                </div>
                <small id="errorGdfAddressAttribute8 " class="form-text text-danger">&nbsp;{$errorGdfAddressAttribute8}</small>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-mx mr-2 ml-2" title="MX only"></div>
                    </div>
                    <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                        <option {if $paymentMethodName==='MX - BBVA'}selected{/if} value="MX - BBVA">MX - BBVA</option>
                        <option {if $paymentMethodName==='MX - BBVA - EUR'}selected{/if} value="MX - BBVA - EUR">MX - BBVA - EUR</option>
                        <option {if $paymentMethodName==='MX - BBVA - USD'}selected{/if} value="MX - BBVA - USD">MX - BBVA - USD</option>
                        <option {if $paymentMethodName==='MX - CITIBANK - EUR'}selected{/if} value="MX - CITIBANK - EUR">MX - CITIBANK - EUR</option>
                        <option {if $paymentMethodName==='MX - CITIBANK - USD'}selected{/if} value="MX - CITIBANK - USD">MX - CITIBANK - USD</option>
                        <option {if $paymentMethodName==='MX - BANAMEX MANUAL'}selected{/if} value="MX - BANAMEX MANUAL ">MX - BANAMEX MANUAL</option>
                    </select>
                </div>
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            </div>
        {elseif $country=='GT'}
            <div class="col-sm-1 col-md-8 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="gdfAddressAttribute8" name="gdfAddressAttribute8">
                        <option {if $gdfAddressAttribute8 ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL  &ndash; 12%</option>
                        <option {if $gdfAddressAttribute8 ==='CONTRIBUYENTE PEQUENO'}selected{/if} value="CONTRIBUYENTE PEQUENO">CONTRIBUYENTE PEQUENO  &ndash; 6%</option>
                        <option {if $gdfAddressAttribute8 ==='CONTRIBUYENTE SIN FACTURA'}selected{/if} value="CONTRIBUYENTE SIN FACTURA">CONTRIBUYENTE SIN FACTURA  &ndash; 12%</option>
                        <option {if $gdfAddressAttribute8 ==='EXENTO'}selected{/if} value="EXENTO">EXENTO  &ndash; 0%</option>
                        <option {if $gdfAddressAttribute8 ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR  &ndash; 0%</option>
                    </select>
                </div>
                <small id="errorGdfAddressAttribute8 " class="form-text text-danger">&nbsp;{$errorGdfAddressAttribute8}</small>
            </div>
        </div>
        <div class="form-row align-items-center">
            <div class="col-sm-12 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-gt mr-2 ml-2" title="GT only"></div>
                    </div>
                    <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                        <option {if $paymentMethodName==='GT - CITIBANK MANUAL'}selected{/if} value="GT - CITIBANK MANUAL">GT - CITIBANK MANUAL</option>
                        <option {if $paymentMethodName==='GT - CITIBANK MANUAL - USD'}selected{/if} value="GT - CITIBANK MANUAL - USD">GT - CITIBANK MANUAL &ndash; USD</option>
                        <option {if $paymentMethodName==='GT - BAC MANUAL'}selected{/if} value="GT - BAC MANUAL">GT - BAC MANUAL</option>
                        <option {if $paymentMethodName==='GT - BAC MANUAL - USD'}selected{/if} value="GT - BAC MANUAL - USD">GT - BAC MANUAL &ndash; USD</option>
                        <option {if $paymentMethodName==='GT - INDUSTRIAL MANUAL'}selected{/if} value="GT - INDUSTRIAL MANUAL">GT - INDUSTRIAL MANUAL</option>
                    </select>
                </div>
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            </div>
    </div>
        {elseif $country=='HN'}

            <div class="col-sm-1 col-md-8 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="gdfAddressAttribute8" name="gdfAddressAttribute8">
                        <option {if $gdfAddressAttribute8 ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL  &ndash; 15%</option>
                        <option {if $gdfAddressAttribute8 ==='CONTRIBUYENTE SPECIAL'}selected{/if} value="CONTRIBUYENTE SPECIAL">CONTRIBUYENTE SPECIAL  &ndash; 18%</option>
                        <option {if $gdfAddressAttribute8 ==='CONTRIBUYENTE HOTELES'}selected{/if} value="CONTRIBUYENTE HOTELES">CONTRIBUYENTE HOTELES  &ndash; 19%</option>
                        <option {if $gdfAddressAttribute8 ==='EXENTO'}selected{/if} value="EXENTO">EXENTO  &ndash; 0%</option>
                        <option {if $gdfAddressAttribute8 ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR  &ndash; 0%</option>
                    </select>
                </div>
                <small id="errorGdfAddressAttribute8 " class="form-text text-danger">&nbsp;{$errorGdfAddressAttribute8}</small>
            </div>
    <div class="form-row align-items-center">
            <div class="col-sm-1 col-md-12 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-hn mr-2 ml-2" title="HN only"></div>
                    </div>
                    <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                        <option {if $paymentMethodName==='HN - CITIBANK MANUAL'}selected{/if} value="HN - CITIBANK MANUAL">HN - CITIBANK MANUAL</option>
                        <option {if $paymentMethodName==='HN - CITIBANK MANUAL - USD'}selected{/if} value="HN - CITIBANK MANUAL - USD">HN - CITIBANK MANUAL &ndash; USD</option>
                        <option {if $paymentMethodName==='HN - BAC MANUAL'}selected{/if} value="HN - BAC MANUAL">HN - BAC MANUAL</option>
                        <option {if $paymentMethodName==='HN - BAC - USD'}selected{/if} value="HN - BAC - USD">HN - BAC &ndash; USD</option>
                        <option {if $paymentMethodName==='HN - FICOHSA  MANUAL'}selected{/if} value="HN - FICOHSA  MANUAL">HN - FICOHSA MANUAL</option>
                        <option {if $paymentMethodName==='HN - FICOSA - USD'}selected{/if} value="HN - FICOSA - USD">HN - FICOSA &ndash; USD</option>
                    </select>
                </div>
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            </div>
    </div>
        {elseif $country=='SV'}
            <div class="col-sm-1 col-md-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="gdfAddressAttribute8" name="gdfAddressAttribute8">
                        <option {if $gdfAddressAttribute8 ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL  &ndash; 13%</option>
                        <option {if $gdfAddressAttribute8 ==='CONTRIBUYENTE MEDIUM'}selected{/if} value="CONTRIBUYENTE MEDIUM">CONTRIBUYENTE MEDIUM  &ndash; 13%</option>
                        <option {if $gdfAddressAttribute8 ==='CONTRIBUYENTE PEQUENO'}selected{/if} value="CONTRIBUYENTE PEQUENO">CONTRIBUYENTE PEQUENO  &ndash; 13%</option>
                        <option {if $gdfAddressAttribute8 ==='EXENTO'}selected{/if} value="EXENTO">EXENTO  &ndash; 0%</option>
                        <option {if $gdfAddressAttribute8 ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR  &ndash; 0%</option>
                    </select>
                </div>
                <small id="errorGdfAddressAttribute8 " class="form-text text-danger">&nbsp;{$errorGdfAddressAttribute8}</small>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-sv mr-2 ml-2" title="SV only"></div>
                    </div>
                    <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                        <option {if $paymentMethodName==='SV - CITIBANK MANUAL'}selected{/if} value="SV - CITIBANK MANUAL">SV - CITIBANK MANUAL</option>
                        <option {if $paymentMethodName==='SV - BAC MANUAL'}selected{/if} value="SV - BAC MANUAL">SV - BAC MANUAL</option>
                        <option {if $paymentMethodName==='SV - CUSCATLAN MANUAL'}selected{/if} value="SV - CUSCATLAN  MANUAL">SV - CUSCATLAN MANUAL</option>
                    </select>
                </div>

                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            </div>
        {elseif $country=='NI'}
            <div class="col-sm-1 col-md-8 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="gdfAddressAttribute8" name="gdfAddressAttribute8">
                        <option {if $gdfAddressAttribute8 ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL  &ndash; 15%</option>
                        <option {if $gdfAddressAttribute8 ==='DIPLOMATICOS'}selected{/if} value="DIPLOMATICOS">DIPLOMATICOS  &ndash; 0%</option>
                        <option {if $gdfAddressAttribute8 ==='CONSTANCIA DE EXONERACION'}selected{/if} value="CONSTANCIA DE EXONERACION">CONSTANCIA DE EXONERACION &ndash; 0%</option>
                        <option {if $gdfAddressAttribute8 ==='ENERGIA ELECTRICA'}selected{/if} value="ENERGIA ELECTRICA">ENERGIA ELECTRICA &ndash; 7%</option>
                        <option {if $gdfAddressAttribute8 ==='EXENTO'}selected{/if} value="EXENTO">EXENTO  &ndash; 0%</option>
                        <option {if $gdfAddressAttribute8 ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR  &ndash; 0%</option>
                    </select>
                </div>
                <small id="errorGdfAddressAttribute8 " class="form-text text-danger">&nbsp;{$errorGdfAddressAttribute8}</small>
            </div>
</div>
<div class="form-row align-items-center">
            <div class="col-sm-12 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-ni mr-2 ml-2" title="NI only"></div>
                    </div>
                    <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                        <option {if $paymentMethodName==='NI - FICOHSA MANUAL'}selected{/if} value="NI - FICOHSA MANUAL">NI - FICOHSA MANUAL</option>
                        <option {if $paymentMethodName==='NI - FICOHSA - USD'}selected{/if} value="NI - FICOHSA - USD">NI - FICOHSA &ndash; USD</option>
                        <option {if $paymentMethodName==='NI - BAC - USD'}selected{/if} value="NI - BAC - USD">NI - BAC &ndash; USD</option>
                        <option {if $paymentMethodName==='NI - BAC MANUAL'}selected{/if} value="NI - BAC MANUAL">NI - BAC MANUAL</option>
                    </select>
                </div>
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            </div>
</div>
        {elseif $country=='CR'}
            <div class="col-sm-1 col-md-8 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="gdfAddressAttribute8" name="gdfAddressAttribute8">
                        <option {if $gdfAddressAttribute8 ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL  &ndash; 13%</option>
                        <option {if $gdfAddressAttribute8 ==='EXENTO'}selected{/if} value="EXENTO">EXENTO  &ndash; 0%</option>
                        <option {if $gdfAddressAttribute8 ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR  &ndash; 0%</option>
                    </select>
                </div>
                <small id="errorGdfAddressAttribute8 " class="form-text text-danger">&nbsp;{$errorGdfAddressAttribute8}</small>
            </div>
</div>
<div class="form-row align-items-center">
            <div class="col-sm-12 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-cr mr-2 ml-2" title="CR only"/></div>
                    </div>
                    <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                        <option {if $paymentMethodName==='CR - CITIBANK MANUAL'}selected{/if} value="CR - CITIBANK MANUAL">CR - CITIBANK MANUAL</option>
                        <option {if $paymentMethodName==='CR - CITIBANK - USD'}selected{/if} value="CR - CITIBANK - USD">CR - CITIBANK - USD</option>
                        <option {if $paymentMethodName==='CR - PROMERICA MANUAL'}selected{/if} value="CR - PROMERICA MANUAL">CR - PROMERICA MANUAL</option>
                        <option {if $paymentMethodName==='CR - PROMERICA - USD'}selected{/if} value="CR - PROMERICA - USD">CR - PROMERICA - USD</option>
                    </select>
                </div>
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            </div>
</div>
        {elseif $country=='AR'}
            <div class="col-sm-1 col-md-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="gdfAddressAttribute8" name="gdfAddressAttribute8">
                        <option {if $gdfAddressAttribute8 ==='RESPONSABLES NO INSCRIPTOS'}selected{/if} value="RESPONSABLES NO INSCRIPTOS">RESPONSABLES NO INSCRIPTOS</option>
                        <option {if $gdfAddressAttribute8 ==='RESPONSABLES INSCRIPTOS'}selected{/if} value="RESPONSABLES INSCRIPTOS">RESPONSABLES INSCRIPTOS</option>
                        <option {if $gdfAddressAttribute8 ==='EXENTOS'}selected{/if} value="EXENTOS">EXENTOS</option>
                        <option {if $gdfAddressAttribute8 ==='CONSUMIDOR FINAL'}selected{/if} value="CONSUMIDOR FINAL">CONSUMIDOR FINAL</option>
                        <option {if $gdfAddressAttribute8 ==='MONOTRIBUTO'}selected{/if} value="MONOTRIBUTO">MONOTRIBUTO</option>
                        <option {if $gdfAddressAttribute8 ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR</option>
                    </select>
                </div>
                <small id="errorGdfAddressAttribute8 " class="form-text text-danger">&nbsp;{$errorGdfAddressAttribute8}</small>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-ar mr-2 ml-2" title="AR only"/></div>
                    </div>
                    <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                        <option {if $paymentMethodName==='AR - SANTANDER MANUAL'}selected{/if} value="AR - SANTANDER MANUAL">AR - SANTANDER MANUAL</option>
                        <option {if $paymentMethodName==='AR - BANCO CIUDAD MANUAL'}selected{/if} value="AR - BANCO CIUDAD MANUAL">AR - BANCO CIUDAD MANUAL</option>
                        <option {if $paymentMethodName==='AR - CITIBANK - USD'}selected{/if} value="AR - CITIBANK - USD">AR - CITIBANK - USD</option>
                        <option {if $paymentMethodName==='AR - CITIBANK MANUAL'}selected{/if} value="AR - CITIBANK MANUAL">AR - CITIBANK MANUAL</option>
                    </select>
                </div>
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            </div>
        {elseif $country=='PE'}
            <div class="col-sm-1 col-md-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="gdfAddressAttribute8" name="gdfAddressAttribute8">
                        <option {if $gdfAddressAttribute8 ==='GRABADAS DEL IGV'}selected{/if} value="GRABADAS DEL IGV">GRABADAS DEL IGV</option>
                        <option {if $gdfAddressAttribute8 ==='NO GRABADAS DEL IGV'}selected{/if} value="NO GRABADAS DEL IGV">NO GRABADAS DEL IGV</option>
                        <option {if $gdfAddressAttribute8 ==='EXENTOS'}selected{/if} value="EXENTOS">EXENTOS</option>
                    </select>
                </div>
                <small id="errorGdfAddressAttribute8 " class="form-text text-danger">&nbsp;{$errorGdfAddressAttribute8}</small>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-pe mr-2 ml-2" title="PE only"/></div>
                    </div>
                    <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                        <option {if $paymentMethodName==='PE - BBVA MANUAL'}selected{/if} value="PE - BBVA MANUAL">PE - BBVA MANUAL</option>
                        <option {if $paymentMethodName==='PE - BANCO DE LA NACION MANUAL'}selected{/if} value="PE - BANCO DE LA NACION MANUAL">PE - BANCO DE LA NACION MANUAL</option>
                        <option {if $paymentMethodName==='PE - BBVA - EUR'}selected{/if} value="PE - BBVA - EUR">PE - BBVA - EUR</option>
                        <option {if $paymentMethodName==='PE - BBVA - USD'}selected{/if} value="PE - BBVA - USD">PE - BBVA - USD</option>
                        <option {if $paymentMethodName==='PE - BCP - USD'}selected{/if} value="PE - BCP - USD">PE - BCP - USD</option>
                        <option {if $paymentMethodName==='PE - BCP MANUAL'}selected{/if} value="PE - BCP MANUAL">PE - BCP MANUAL</option>
                    </select>
                </div>
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            </div>
        {elseif $country=='CL'}
            <div class="col-sm-1 col-md-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="gdfAddressAttribute8" name="gdfAddressAttribute8">
                        <option {if $gdfAddressAttribute8 ==='AFECTAS DEL IVA'}selected{/if} value="AFECTAS DEL IVA">AFECTAS DEL IVA</option>
                        <option {if $gdfAddressAttribute8 ==='EXENTAS DEL IVA'}selected{/if} value="EXENTAS DEL IVA">EXENTAS DEL IVA</option>
                    </select>
                </div>
                <small id="errorGdfAddressAttribute8 " class="form-text text-danger">&nbsp;{$errorGdfAddressAttribute8}</small>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-cl mr-2 ml-2" title="CL only"/></div>
                    </div>
                    <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                        <option {if $paymentMethodName==='CL - SANTANDER MANUAL'}selected{/if} value="CL - SANTANDER MANUAL">CL - SANTANDER MANUAL</option>
                        <option {if $paymentMethodName==='CL - BCI MANUAL'}selected{/if} value="CL - BCI MANUAL">CL - BCI MANUAL</option>
                        <option {if $paymentMethodName==='CL - SANTANDER - EUR'}selected{/if} value="CL - SANTANDER - EUR">CL - SANTANDER - EUR</option>
                        <option {if $paymentMethodName==='CL - SANTANDER - USD'}selected{/if} value="CL - SANTANDER - USD">CL - SANTANDER - USD</option>
                    </select>
                </div>
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            </div>
            <div class="col-sm-12 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">GIRO <span class="flag-icon flag-icon-cl mr-2 ml-2" title="CL only"/></div>
                    </div>
                    <select class="form-control" id="giro" name="giro">
                        <option {if $giro==='ACONDICIONAMIENTO DE EDIFICIOS'}selected{/if} value="ACONDICIONAMIENTO DE EDIFICIOS">ACONDICIONAMIENTO DE EDIFICIOS</option>
                        <option {if $giro==='ACTIVIDADES DE ASESORAMIENTO EMPRESARIAL Y EN MATE'}selected{/if} value="ACTIVIDADES DE ASESORAMIENTO EMPRESARIAL Y EN MATE">ACTIVIDADES DE ASESORAMIENTO EMPRESARIAL Y EN MATE</option>
                        <option {if $giro==='ACTIVIDADES DE BIBLIOTECAS Y ARCHIVOS'}selected{/if} value="ACTIVIDADES DE BIBLIOTECAS Y ARCHIVOS">ACTIVIDADES DE BIBLIOTECAS Y ARCHIVOS</option>
                        <option {if $giro==='ACTIVIDADES DE CASINO DE JUEGOS'}selected{/if} value="ACTIVIDADES DE CASINO DE JUEGOS">ACTIVIDADES DE CASINO DE JUEGOS</option>
                        <option {if $giro==='ACTIVIDADES DE ORGANIZACIONES RELIGIOSAS'}selected{/if} value="ACTIVIDADES DE ORGANIZACIONES RELIGIOSAS">ACTIVIDADES DE ORGANIZACIONES RELIGIOSAS</option>
                        <option {if $giro==='ADMINISTRACION'}selected{/if} value="ADMINISTRACION">ADMINISTRACION</option>
                        <option {if $giro==='ADMINISTRACION DE ACTIVOS'}selected{/if} value="ADMINISTRACION DE ACTIVOS">ADMINISTRACION DE ACTIVOS</option>
                        <option {if $giro==='ADMINISTRACION DE BIENES INMUEBLES'}selected{/if} value="ADMINISTRACION DE BIENES INMUEBLES">ADMINISTRACION DE BIENES INMUEBLES</option>
                        <option {if $giro==='ADMINISTRACION DE CENTROS COMERCIALES'}selected{/if} value="ADMINISTRACION DE CENTROS COMERCIALES">ADMINISTRACION DE CENTROS COMERCIALES</option>
                        <option {if $giro==='ADMINISTRACION DE INMUEBLES Y ASESORIAS DE NEGOCIO'}selected{/if} value="ADMINISTRACION DE INMUEBLES Y ASESORIAS DE NEGOCIO">ADMINISTRACION DE INMUEBLES Y ASESORIAS DE NEGOCIO</option>
                        <option {if $giro==='ADMINISTRACION DE SERVICIOS DE EDUCACION Y SALUD T'}selected{/if} value="ADMINISTRACION DE SERVICIOS DE EDUCACION Y SALUD T">ADMINISTRACION DE SERVICIOS DE EDUCACION Y SALUD T</option>
                        <option {if $giro==='ADMINISTRACION FONDOS DE PENSIONES'}selected{/if} value="ADMINISTRACION FONDOS DE PENSIONES">ADMINISTRACION FONDOS DE PENSIONES</option>
                        <option {if $giro==='ADMINISTRACION PUBLICA'}selected{/if} value="ADMINISTRACION PUBLICA">ADMINISTRACION PUBLICA</option>
                        <option {if $giro==='ADMINISTRACION SERVICIOS DE VIVIENDA'}selected{/if} value="ADMINISTRACION SERVICIOS DE VIVIENDA">ADMINISTRACION SERVICIOS DE VIVIENDA</option>
                        <option {if $giro==='ADMINISTRACION Y CAPACITACION DE RECURSOS'}selected{/if} value="ADMINISTRACION Y CAPACITACION DE RECURSOS">ADMINISTRACION Y CAPACITACION DE RECURSOS</option>
                        <option {if $giro==='ADMINSTRADORA DE  ESTABLECIMIENTOS COMERCIALES'}selected{/if} value="ADMINSTRADORA DE  ESTABLECIMIENTOS COMERCIALES">ADMINSTRADORA DE  ESTABLECIMIENTOS COMERCIALES</option>
                        <option {if $giro==='ADQUISICION ENAJ COMERC Y EXP DE BIENES RAICES'}selected{/if} value="ADQUISICION ENAJ COMERC Y EXP DE BIENES RAICES">ADQUISICION ENAJ COMERC Y EXP DE BIENES RAICES</option>
                        <option {if $giro==='AEROPUERTO'}selected{/if} value="AEROPUERTO">AEROPUERTO</option>
                        <option {if $giro==='AGENCIA'}selected{/if} value="AGENCIA">AGENCIA</option>
                        <option {if $giro==='AGENCIA DE NAVES'}selected{/if} value="AGENCIA DE NAVES">AGENCIA DE NAVES</option>
                        <option {if $giro==='ALIMENTACION'}selected{/if} value="ALIMENTACION">ALIMENTACION</option>
                        <option {if $giro==='ALMACENAMIENTO Y LOGISTICA'}selected{/if} value="ALMACENAMIENTO Y LOGISTICA">ALMACENAMIENTO Y LOGISTICA</option>
                        <option {if $giro==='ALMACENES MEDIANOS (VENTA DE ALIMENTOS)'}selected{/if} value="ALMACENES MEDIANOS (VENTA DE ALIMENTOS)">ALMACENES MEDIANOS (VENTA DE ALIMENTOS)</option>
                        <option {if $giro==='ARR. Y EXPL. DE BIE. INMUEBLES'}selected{/if} value="ARR. Y EXPL. DE BIE. INMUEBLES">ARR. Y EXPL. DE BIE. INMUEBLES</option>
                        <option {if $giro==='ARRDO. Y EXPLOT.BS INMUEBLES, CONST. VIVENDAS, OBR'}selected{/if} value="ARRDO. Y EXPLOT.BS INMUEBLES, CONST. VIVENDAS, OBR">ARRDO. Y EXPLOT.BS INMUEBLES, CONST. VIVENDAS, OBR</option>
                        <option {if $giro==='ARRIENDO DE BIENES RAICES SIN AMOBLAR'}selected{/if} value="ARRIENDO DE BIENES RAICES SIN AMOBLAR">ARRIENDO DE BIENES RAICES SIN AMOBLAR</option>
                        <option {if $giro==='ARRIENDO DE INMUEBLES'}selected{/if} value="ARRIENDO DE INMUEBLES">ARRIENDO DE INMUEBLES</option>
                        <option {if $giro==='ARRIENDO DE INMUEBLES AMOBLADOS'}selected{/if} value="ARRIENDO DE INMUEBLES AMOBLADOS">ARRIENDO DE INMUEBLES AMOBLADOS</option>
                        <option {if $giro==='ARRIENDO DE INMUEBLES SIN AMOBLAR Y AMOBLADO CON I'}selected{/if} value="ARRIENDO DE INMUEBLES SIN AMOBLAR Y AMOBLADO CON I">ARRIENDO DE INMUEBLES SIN AMOBLAR Y AMOBLADO CON I</option>
                        <option {if $giro==='ARRIENDO DE LOCALES'}selected{/if} value="ARRIENDO DE LOCALES">ARRIENDO DE LOCALES</option>
                        <option {if $giro==='ARRIENDO DE MAQUINARIA Y EQUIPOS'}selected{/if} value="ARRIENDO DE MAQUINARIA Y EQUIPOS">ARRIENDO DE MAQUINARIA Y EQUIPOS</option>
                        <option {if $giro==='ARRIENDO Y EXPLOTACION DE BIENES INMUEBLES'}selected{/if} value="ARRIENDO Y EXPLOTACION DE BIENES INMUEBLES">ARRIENDO Y EXPLOTACION DE BIENES INMUEBLES</option>
                        <option {if $giro==='ARTICULOS ELECTRICOS'}selected{/if} value="ARTICULOS ELECTRICOS">ARTICULOS ELECTRICOS</option>
                        <option {if $giro==='ASESORIAS E INVERSIONES'}selected{/if} value="ASESORIAS E INVERSIONES">ASESORIAS E INVERSIONES</option>
                        <option {if $giro==='ASOCIACION DE DIALIZADOS Y TRASPLANTADOS DE CHILE'}selected{/if} value="ASOCIACION DE DIALIZADOS Y TRASPLANTADOS DE CHILE">ASOCIACION DE DIALIZADOS Y TRASPLANTADOS DE CHILE</option>
                        <option {if $giro==='ASOCIACION GREMIAL'}selected{/if} value="ASOCIACION GREMIAL">ASOCIACION GREMIAL</option>
                        <option {if $giro==='BOLSA DE COMERCIO'}selected{/if} value="BOLSA DE COMERCIO">BOLSA DE COMERCIO</option>
                        <option {if $giro==='CASINO'}selected{/if} value="CASINO">CASINO</option>
                        <option {if $giro==='CCAF LOS ANDES'}selected{/if} value="CCAF LOS ANDES">CCAF LOS ANDES</option>
                        <option {if $giro==='CENTRO COMERCIAL                                  '}selected{/if} value="CENTRO COMERCIAL                                  ">CENTRO COMERCIAL                                  </option>
                        <option {if $giro==='CENTRO MEDICO'}selected{/if} value="CENTRO MEDICO">CENTRO MEDICO</option>
                        <option {if $giro==='CLINICA HOSPITAL'}selected{/if} value="CLINICA HOSPITAL">CLINICA HOSPITAL</option>
                        <option {if $giro==='COMERCIAL'}selected{/if} value="COMERCIAL">COMERCIAL</option>
                        <option {if $giro==='COMERCIALIZACION DE PRODUCTOS TEXTILES'}selected{/if} value="COMERCIALIZACION DE PRODUCTOS TEXTILES">COMERCIALIZACION DE PRODUCTOS TEXTILES</option>
                        <option {if $giro==='COMERCIALIZADORA DE ARTICULOS DE VESTIR'}selected{/if} value="COMERCIALIZADORA DE ARTICULOS DE VESTIR">COMERCIALIZADORA DE ARTICULOS DE VESTIR</option>
                        <option {if $giro==='COMERCIALIZADORA DE LOCALES'}selected{/if} value="COMERCIALIZADORA DE LOCALES">COMERCIALIZADORA DE LOCALES</option>
                        <option {if $giro==='COMERCIALIZADORA DE LOCALES Y SERV. DEL CDPA'}selected{/if} value="COMERCIALIZADORA DE LOCALES Y SERV. DEL CDPA">COMERCIALIZADORA DE LOCALES Y SERV. DEL CDPA</option>
                        <option {if $giro==='COMPANIA DE SEGUROS'}selected{/if} value="COMPANIA DE SEGUROS">COMPANIA DE SEGUROS</option>
                        <option {if $giro==='COMPRA - VENTA - ARRIENDO INMUEBLES ARRENDADOS Y S'}selected{/if} value="COMPRA - VENTA - ARRIENDO INMUEBLES ARRENDADOS Y S">COMPRA - VENTA - ARRIENDO INMUEBLES ARRENDADOS Y S</option>
                        <option {if $giro==='COMPRA VENTA Y ALQUILER DE INMUEBLES'}selected{/if} value="COMPRA VENTA Y ALQUILER DE INMUEBLES">COMPRA VENTA Y ALQUILER DE INMUEBLES</option>
                        <option {if $giro==='COMPRA, VENTA Y ALQUILER'}selected{/if} value="COMPRA, VENTA Y ALQUILER">COMPRA, VENTA Y ALQUILER</option>
                        <option {if $giro==='COMPRA, VENTA Y ALQUILER (EXCEPTO AMOBLADOS) '}selected{/if} value="COMPRA, VENTA Y ALQUILER (EXCEPTO AMOBLADOS) ">COMPRA, VENTA Y ALQUILER (EXCEPTO AMOBLADOS) </option>
                        <option {if $giro==='COMPRA, VENTA Y ARRIENDO DE BIENES INMUEBLES'}selected{/if} value="COMPRA, VENTA Y ARRIENDO DE BIENES INMUEBLES">COMPRA, VENTA Y ARRIENDO DE BIENES INMUEBLES</option>
                        <option {if $giro==='COMUNICACIONES'}selected{/if} value="COMUNICACIONES">COMUNICACIONES</option>
                        <option {if $giro==='COMUNIDAD ADMINISTRACION'}selected{/if} value="COMUNIDAD ADMINISTRACION">COMUNIDAD ADMINISTRACION</option>
                        <option {if $giro==='COMUNIDAD COMERCIAL'}selected{/if} value="COMUNIDAD COMERCIAL">COMUNIDAD COMERCIAL</option>
                        <option {if $giro==='COMUNIDAD CONDOMINIO'}selected{/if} value="COMUNIDAD CONDOMINIO">COMUNIDAD CONDOMINIO</option>
                        <option {if $giro==='COMUNIDAD EDIFICIO'}selected{/if} value="COMUNIDAD EDIFICIO">COMUNIDAD EDIFICIO</option>
                        <option {if $giro==='COMUNIDAD ELECTRICA'}selected{/if} value="COMUNIDAD ELECTRICA">COMUNIDAD ELECTRICA</option>
                        <option {if $giro==='COMUNIDAD HABITACIONAL'}selected{/if} value="COMUNIDAD HABITACIONAL">COMUNIDAD HABITACIONAL</option>
                        <option {if $giro==='COMUNIDAD INMOBILIARIA'}selected{/if} value="COMUNIDAD INMOBILIARIA">COMUNIDAD INMOBILIARIA</option>
                        <option {if $giro==='COMUNIDAD INVERSIONES'}selected{/if} value="COMUNIDAD INVERSIONES">COMUNIDAD INVERSIONES</option>
                        <option {if $giro==='COMUNIDAD JARDINERIA'}selected{/if} value="COMUNIDAD JARDINERIA">COMUNIDAD JARDINERIA</option>
                        <option {if $giro==='COMUNIDAD MEDICOS'}selected{/if} value="COMUNIDAD MEDICOS">COMUNIDAD MEDICOS</option>
                        <option {if $giro==='COMUNIDAD TIENDAS'}selected{/if} value="COMUNIDAD TIENDAS">COMUNIDAD TIENDAS</option>
                        <option {if $giro==='COMUNIDAD UNIVERSIDAD'}selected{/if} value="COMUNIDAD UNIVERSIDAD">COMUNIDAD UNIVERSIDAD</option>
                        <option {if $giro==='CONCEJO DE ADMINISTRACION'}selected{/if} value="CONCEJO DE ADMINISTRACION">CONCEJO DE ADMINISTRACION</option>
                        <option {if $giro==='CONCESIONARIA'}selected{/if} value="CONCESIONARIA">CONCESIONARIA</option>
                        <option {if $giro==='CONDOMINIO'}selected{/if} value="CONDOMINIO">CONDOMINIO</option>
                        <option {if $giro==='CONDOMINIO HABITACIONAL'}selected{/if} value="CONDOMINIO HABITACIONAL">CONDOMINIO HABITACIONAL</option>
                        <option {if $giro==='CONGREGACION PROVINCIAL'}selected{/if} value="CONGREGACION PROVINCIAL">CONGREGACION PROVINCIAL</option>
                        <option {if $giro==='CONJUNTO HABITACIONAL'}selected{/if} value="CONJUNTO HABITACIONAL">CONJUNTO HABITACIONAL</option>
                        <option {if $giro==='CONSEJO DE ADMINISTRACION DE EDIFICIOS Y CONDOMIN'}selected{/if} value="CONSEJO DE ADMINISTRACION DE EDIFICIOS Y CONDOMIN">CONSEJO DE ADMINISTRACION DE EDIFICIOS Y CONDOMIN</option>
                        <option {if $giro==='CONSTRUCCION'}selected{/if} value="CONSTRUCCION">CONSTRUCCION</option>
                        <option {if $giro==='CONSTRUCCION DE EDIFICIOS - VIVIENDAS - HOTELES'}selected{/if} value="CONSTRUCCION DE EDIFICIOS - VIVIENDAS - HOTELES">CONSTRUCCION DE EDIFICIOS - VIVIENDAS - HOTELES</option>
                        <option {if $giro==='CONSTRUCCION DE EDIFICIOS COMPLETOS O DE PARTES DE'}selected{/if} value="CONSTRUCCION DE EDIFICIOS COMPLETOS O DE PARTES DE">CONSTRUCCION DE EDIFICIOS COMPLETOS O DE PARTES DE</option>
                        <option {if $giro==='CONSTRUCCION DE EDIFICIOS COMPLETOS O DE PARTES DE'}selected{/if} value="CONSTRUCCION DE EDIFICIOS COMPLETOS O DE PARTES DE">CONSTRUCCION DE EDIFICIOS COMPLETOS O DE PARTES DE</option>
                        <option {if $giro==='CONTRATISTA DE SERV. PARA LA ADM. '}selected{/if} value="CONTRATISTA DE SERV. PARA LA ADM. ">CONTRATISTA DE SERV. PARA LA ADM. </option>
                        <option {if $giro==='CORREDORES DE PROPIEDADES'}selected{/if} value="CORREDORES DE PROPIEDADES">CORREDORES DE PROPIEDADES</option>
                        <option {if $giro==='CULTIVOS Y VENTA DE SALMONES'}selected{/if} value="CULTIVOS Y VENTA DE SALMONES">CULTIVOS Y VENTA DE SALMONES</option>
                        <option {if $giro==='DESARROLLO Y PARTC DE HOTELERIA - GASTRONOMIA Y AC'}selected{/if} value="DESARROLLO Y PARTC DE HOTELERIA - GASTRONOMIA Y AC">DESARROLLO Y PARTC DE HOTELERIA - GASTRONOMIA Y AC</option>
                        <option {if $giro==='DISCOTECA, SALA DE BAILE, CABARET'}selected{/if} value="DISCOTECA, SALA DE BAILE, CABARET">DISCOTECA, SALA DE BAILE, CABARET</option>
                        <option {if $giro==='DIST. MAY. Y MINOR. DE PROD.'}selected{/if} value="DIST. MAY. Y MINOR. DE PROD.">DIST. MAY. Y MINOR. DE PROD.</option>
                        <option {if $giro==='DISTRIBUCION DE GAS'}selected{/if} value="DISTRIBUCION DE GAS">DISTRIBUCION DE GAS</option>
                        <option {if $giro==='DISTRIBUCION Y VENTA DE ENERGIA DECTRICA Y VENTA D'}selected{/if} value="DISTRIBUCION Y VENTA DE ENERGIA DECTRICA Y VENTA D">DISTRIBUCION Y VENTA DE ENERGIA DECTRICA Y VENTA D</option>
                        <option {if $giro==='EDIFICIO'}selected{/if} value="EDIFICIO">EDIFICIO</option>
                        <option {if $giro==='EDUCACION'}selected{/if} value="EDUCACION">EDUCACION</option>
                        <option {if $giro==='EJECUCION Y CONSTRUCCION OBRAS CIVILES'}selected{/if} value="EJECUCION Y CONSTRUCCION OBRAS CIVILES">EJECUCION Y CONSTRUCCION OBRAS CIVILES</option>
                        <option {if $giro==='ELABORACION DE ALIMENTOS PREPARADOS PARA ANIMALES'}selected{/if} value="ELABORACION DE ALIMENTOS PREPARADOS PARA ANIMALES">ELABORACION DE ALIMENTOS PREPARADOS PARA ANIMALES</option>
                        <option {if $giro==='ENSENANZA PRIMARIA, SECUNDARIA CIENTIFICO HUMANIST'}selected{/if} value="ENSENANZA PRIMARIA, SECUNDARIA CIENTIFICO HUMANIST">ENSENANZA PRIMARIA, SECUNDARIA CIENTIFICO HUMANIST</option>
                        <option {if $giro==='ENSENANZA SUPERIOR EN UNIVERSIDADES PUBLICAS'}selected{/if} value="ENSENANZA SUPERIOR EN UNIVERSIDADES PUBLICAS">ENSENANZA SUPERIOR EN UNIVERSIDADES PUBLICAS</option>
                        <option {if $giro==='ENTRETENCION'}selected{/if} value="ENTRETENCION">ENTRETENCION</option>
                        <option {if $giro==='ESTABLECIMIENTO MEDICO'}selected{/if} value="ESTABLECIMIENTO MEDICO">ESTABLECIMIENTO MEDICO</option>
                        <option {if $giro==='ESTABLECIMIENTOS DE ENSENANZA PREUNIVERSITARIA'}selected{/if} value="ESTABLECIMIENTOS DE ENSENANZA PREUNIVERSITARIA">ESTABLECIMIENTOS DE ENSENANZA PREUNIVERSITARIA</option>
                        <option {if $giro==='ESTABLECIMIENTOS DE ENSENANZA PRIMARIA'}selected{/if} value="ESTABLECIMIENTOS DE ENSENANZA PRIMARIA">ESTABLECIMIENTOS DE ENSENANZA PRIMARIA</option>
                        <option {if $giro==='ESTABLECIMIENTOS MEDICOS DE ATENCION AMBULATORIA'}selected{/if} value="ESTABLECIMIENTOS MEDICOS DE ATENCION AMBULATORIA">ESTABLECIMIENTOS MEDICOS DE ATENCION AMBULATORIA</option>
                        <option {if $giro==='ESTACIONAMIENTO DE VEHICULOS Y PARQUIMETROS'}selected{/if} value="ESTACIONAMIENTO DE VEHICULOS Y PARQUIMETROS">ESTACIONAMIENTO DE VEHICULOS Y PARQUIMETROS</option>
                        <option {if $giro==='ESTACIONAMIENTOS'}selected{/if} value="ESTACIONAMIENTOS">ESTACIONAMIENTOS</option>
                        <option {if $giro==='EXPLOTACION DE AEROPUERTO'}selected{/if} value="EXPLOTACION DE AEROPUERTO">EXPLOTACION DE AEROPUERTO</option>
                        <option {if $giro==='EXPLOTACION DE AEROPUERTOS Y ESTACIONAMIENTOS'}selected{/if} value="EXPLOTACION DE AEROPUERTOS Y ESTACIONAMIENTOS">EXPLOTACION DE AEROPUERTOS Y ESTACIONAMIENTOS</option>
                        <option {if $giro==='EXPLOTACION DE OTRAS MINAS Y CANTERAS      '}selected{/if} value="EXPLOTACION DE OTRAS MINAS Y CANTERAS      ">EXPLOTACION DE OTRAS MINAS Y CANTERAS      </option>
                        <option {if $giro==='FAB PROD PRIMARIOS DE MET PRECIOSOS Y OTROS NO'}selected{/if} value="FAB PROD PRIMARIOS DE MET PRECIOSOS Y OTROS NO">FAB PROD PRIMARIOS DE MET PRECIOSOS Y OTROS NO</option>
                        <option {if $giro==='FABR. Y DIST. PROD. LACTEOS VTAS. INSUMOS AGRICOLA'}selected{/if} value="FABR. Y DIST. PROD. LACTEOS VTAS. INSUMOS AGRICOLA">FABR. Y DIST. PROD. LACTEOS VTAS. INSUMOS AGRICOLA</option>
                        <option {if $giro==='FABRICA DE PANELES DE MADERA, PRODUCCION , VENTA'}selected{/if} value="FABRICA DE PANELES DE MADERA, PRODUCCION , VENTA">FABRICA DE PANELES DE MADERA, PRODUCCION , VENTA</option>
                        <option {if $giro==='FABRICACION  DE PULPA DE MADERA'}selected{/if} value="FABRICACION  DE PULPA DE MADERA">FABRICACION  DE PULPA DE MADERA</option>
                        <option {if $giro==='FABRICACION DE PAPELES Y CARTONES'}selected{/if} value="FABRICACION DE PAPELES Y CARTONES">FABRICACION DE PAPELES Y CARTONES</option>
                        <option {if $giro==='FABRICACION DE PLASTICOS EN FORMAS PRIMARIAS Y DE '}selected{/if} value="FABRICACION DE PLASTICOS EN FORMAS PRIMARIAS Y DE ">FABRICACION DE PLASTICOS EN FORMAS PRIMARIAS Y DE </option>
                        <option {if $giro==='FABRICACION DE PRODUCTOS DE REFINACION DE PETROLEO'}selected{/if} value="FABRICACION DE PRODUCTOS DE REFINACION DE PETROLEO">FABRICACION DE PRODUCTOS DE REFINACION DE PETROLEO</option>
                        <option {if $giro==='FABRICACION Y VENTA DE CELULOSA Y PAPEL'}selected{/if} value="FABRICACION Y VENTA DE CELULOSA Y PAPEL">FABRICACION Y VENTA DE CELULOSA Y PAPEL</option>
                        <option {if $giro==='FABRICACION,REPARACION,ELEBORACION ,COMERCIALIZACI'}selected{/if} value="FABRICACION,REPARACION,ELEBORACION ,COMERCIALIZACI">FABRICACION,REPARACION,ELEBORACION ,COMERCIALIZACI</option>
                        <option {if $giro==='FISCAL'}selected{/if} value="FISCAL">FISCAL</option>
                        <option {if $giro==='FONDO DE INVERSION'}selected{/if} value="FONDO DE INVERSION">FONDO DE INVERSION</option>
                        <option {if $giro==='FONDOS Y SOCIEDADES DE INVERSION Y ENTIDADES FINAN'}selected{/if} value="FONDOS Y SOCIEDADES DE INVERSION Y ENTIDADES FINAN">FONDOS Y SOCIEDADES DE INVERSION Y ENTIDADES FINAN</option>
                        <option {if $giro==='GENERACION DE ELECTRICIDAD'}selected{/if} value="GENERACION DE ELECTRICIDAD">GENERACION DE ELECTRICIDAD</option>
                        <option {if $giro==='GENERACION EN OTRAS CENTRALES TERMOELECTRICA'}selected{/if} value="GENERACION EN OTRAS CENTRALES TERMOELECTRICA">GENERACION EN OTRAS CENTRALES TERMOELECTRICA</option>
                        <option {if $giro==='GENERACION, DISTRIBUCION Y TRANSMISION DE ENERGIA ELECTRICA'}selected{/if} value="GENERACION, DISTRIBUCION Y TRANSMISION DE ENERGIA ELECTRICA">GENERACION, DISTRIBUCION Y TRANSMISION DE ENERGIA ELECTRICA</option>
                        <option {if $giro==='GOBIERNO CENTRAL '}selected{/if} value="GOBIERNO CENTRAL ">GOBIERNO CENTRAL </option>
                        <option {if $giro==='GRANDES ESTABLECIMIENTOS (VENTAS DE ALIMENTOS) HIP'}selected{/if} value="GRANDES ESTABLECIMIENTOS (VENTAS DE ALIMENTOS) HIP">GRANDES ESTABLECIMIENTOS (VENTAS DE ALIMENTOS) HIP</option>
                        <option {if $giro==='GRANDES TIENDAS'}selected{/if} value="GRANDES TIENDAS">GRANDES TIENDAS</option>
                        <option {if $giro==='HIPODROMOS'}selected{/if} value="HIPODROMOS">HIPODROMOS</option>
                        <option {if $giro==='HOSPITAL'}selected{/if} value="HOSPITAL">HOSPITAL</option>
                        <option {if $giro==='HOSPITAL - CLINICAS Y OTROS'}selected{/if} value="HOSPITAL - CLINICAS Y OTROS">HOSPITAL - CLINICAS Y OTROS</option>
                        <option {if $giro==='HOSPITALES Y CLINICAS'}selected{/if} value="HOSPITALES Y CLINICAS">HOSPITALES Y CLINICAS</option>
                        <option {if $giro==='HOTELERIA'}selected{/if} value="HOTELERIA">HOTELERIA</option>
                        <option {if $giro==='HOTELERIA Y TURISMO'}selected{/if} value="HOTELERIA Y TURISMO">HOTELERIA Y TURISMO</option>
                        <option {if $giro==='HOTELES, ASESORIAS, RESTORANTES, BAR, INVERSIONES'}selected{/if} value="HOTELES, ASESORIAS, RESTORANTES, BAR, INVERSIONES">HOTELES, ASESORIAS, RESTORANTES, BAR, INVERSIONES</option>
                        <option {if $giro==='HOTELES, RESTAURANTES Y OTROS'}selected{/if} value="HOTELES, RESTAURANTES Y OTROS">HOTELES, RESTAURANTES Y OTROS</option>
                        <option {if $giro==='IMPORTACIONES'}selected{/if} value="IMPORTACIONES">IMPORTACIONES</option>
                        <option {if $giro==='IMPORTADORA DE ACEROS'}selected{/if} value="IMPORTADORA DE ACEROS">IMPORTADORA DE ACEROS</option>
                        <option {if $giro==='INDUSTRIAL'}selected{/if} value="INDUSTRIAL">INDUSTRIAL</option>
                        <option {if $giro==='INGENIERIA EN CONSTRUCCION'}selected{/if} value="INGENIERIA EN CONSTRUCCION">INGENIERIA EN CONSTRUCCION</option>
                        <option {if $giro==='INGENIERIA Y CONSTRUCCION'}selected{/if} value="INGENIERIA Y CONSTRUCCION">INGENIERIA Y CONSTRUCCION</option>
                        <option {if $giro==='INMOBILIARIA'}selected{/if} value="INMOBILIARIA">INMOBILIARIA</option>
                        <option {if $giro==='INMOBILIARIA CONSTRUCCIONES -  ASES INMOBILIARIAS'}selected{/if} value="INMOBILIARIA CONSTRUCCIONES -  ASES INMOBILIARIAS">INMOBILIARIA CONSTRUCCIONES -  ASES INMOBILIARIAS</option>
                        <option {if $giro==='INMOBILIARIA E INVERSIONES '}selected{/if} value="INMOBILIARIA E INVERSIONES ">INMOBILIARIA E INVERSIONES </option>
                        <option {if $giro==='INMOBILIARIA Y HOTELERA'}selected{/if} value="INMOBILIARIA Y HOTELERA">INMOBILIARIA Y HOTELERA</option>
                        <option {if $giro==='INMUEBLES AMOBLADOS'}selected{/if} value="INMUEBLES AMOBLADOS">INMUEBLES AMOBLADOS</option>
                        <option {if $giro==='INSTALACIONES'}selected{/if} value="INSTALACIONES">INSTALACIONES</option>
                        <option {if $giro==='INSTITUCION FINANCIERA'}selected{/if} value="INSTITUCION FINANCIERA">INSTITUCION FINANCIERA</option>
                        <option {if $giro==='INSTITUCION PUBLICA'}selected{/if} value="INSTITUCION PUBLICA">INSTITUCION PUBLICA</option>
                        <option {if $giro==='INSTITUTO PROFESIONAL'}selected{/if} value="INSTITUTO PROFESIONAL">INSTITUTO PROFESIONAL</option>
                        <option {if $giro==='INVERSIONES'}selected{/if} value="INVERSIONES">INVERSIONES</option>
                        <option {if $giro==='INVERSIONES E INMOBILIARIA'}selected{/if} value="INVERSIONES E INMOBILIARIA">INVERSIONES E INMOBILIARIA</option>
                        <option {if $giro==='INVERSIONES EN BIENES'}selected{/if} value="INVERSIONES EN BIENES">INVERSIONES EN BIENES</option>
                        <option {if $giro==='INVESTIGACIONES Y DESARROLLO EXPERIMENTAL CIENCIAS'}selected{/if} value="INVESTIGACIONES Y DESARROLLO EXPERIMENTAL CIENCIAS">INVESTIGACIONES Y DESARROLLO EXPERIMENTAL CIENCIAS</option>
                        <option {if $giro==='LABORATORIO CLINICO'}selected{/if} value="LABORATORIO CLINICO">LABORATORIO CLINICO</option>
                        <option {if $giro==='LABORATORIO FARMACEUTICO'}selected{/if} value="LABORATORIO FARMACEUTICO">LABORATORIO FARMACEUTICO</option>
                        <option {if $giro==='LINEA AEREA'}selected{/if} value="LINEA AEREA">LINEA AEREA</option>
                        <option {if $giro==='MINERIA'}selected{/if} value="MINERIA">MINERIA</option>
                        <option {if $giro==='MINISTERIO BIENES NACIONALES'}selected{/if} value="MINISTERIO BIENES NACIONALES">MINISTERIO BIENES NACIONALES</option>
                        <option {if $giro==='MUNICIPALIDAD'}selected{/if} value="MUNICIPALIDAD">MUNICIPALIDAD</option>
                        <option {if $giro==='MUTUAL'}selected{/if} value="MUTUAL">MUTUAL</option>
                        <option {if $giro==='OBRAS CIVILES Y TRANSPORTE DE CARGA'}selected{/if} value="OBRAS CIVILES Y TRANSPORTE DE CARGA">OBRAS CIVILES Y TRANSPORTE DE CARGA</option>
                        <option {if $giro==='OBRAS DE INGENIERIA'}selected{/if} value="OBRAS DE INGENIERIA">OBRAS DE INGENIERIA</option>
                        <option {if $giro==='OBRAS MENORES DE CONSTRUCCION'}selected{/if} value="OBRAS MENORES DE CONSTRUCCION">OBRAS MENORES DE CONSTRUCCION</option>
                        <option {if $giro==='OBRAS MENORES EN CONSTRUCCION (CONTRATISTAS, ALBAN'}selected{/if} value="OBRAS MENORES EN CONSTRUCCION (CONTRATISTAS, ALBAN">OBRAS MENORES EN CONSTRUCCION (CONTRATISTAS, ALBAN</option>
                        <option {if $giro==='OPTICA'}selected{/if} value="OPTICA">OPTICA</option>
                        <option {if $giro==='ORDEN RELIGIOSA'}selected{/if} value="ORDEN RELIGIOSA">ORDEN RELIGIOSA</option>
                        <option {if $giro==='ORGANIZACION INTERNACIONAL'}selected{/if} value="ORGANIZACION INTERNACIONAL">ORGANIZACION INTERNACIONAL</option>
                        <option {if $giro==='OTRAS ACTIVIDADES DE SERVICIOS DE APOYO A LAS EMPR'}selected{/if} value="OTRAS ACTIVIDADES DE SERVICIOS DE APOYO A LAS EMPR">OTRAS ACTIVIDADES DE SERVICIOS DE APOYO A LAS EMPR</option>
                        <option {if $giro==='OTRAS ACTIVIDADES EMPRESARIALES N.C.P.'}selected{/if} value="OTRAS ACTIVIDADES EMPRESARIALES N.C.P.">OTRAS ACTIVIDADES EMPRESARIALES N.C.P.</option>
                        <option {if $giro==='OTROS CULTIVOS N.C.P.'}selected{/if} value="OTROS CULTIVOS N.C.P.">OTROS CULTIVOS N.C.P.</option>
                        <option {if $giro==='OTROS'}selected{/if} value="OTROS">OTROS</option>
                        <option {if $giro==='PELUQUERIAS Y SALONES DE BELLEZA'}selected{/if} value="PELUQUERIAS Y SALONES DE BELLEZA">PELUQUERIAS Y SALONES DE BELLEZA</option>
                        <option {if $giro==='PLANES DE SEGUROS DE VIDA'}selected{/if} value="PLANES DE SEGUROS DE VIDA">PLANES DE SEGUROS DE VIDA</option>
                        <option {if $giro==='PLANES DE SEGUROS GENERALES'}selected{/if} value="PLANES DE SEGUROS GENERALES">PLANES DE SEGUROS GENERALES</option>
                        <option {if $giro==='PRESTACION DE SERVICIOS'}selected{/if} value="PRESTACION DE SERVICIOS">PRESTACION DE SERVICIOS</option>
                        <option {if $giro==='PRODUCCION DE EVENTOS'}selected{/if} value="PRODUCCION DE EVENTOS">PRODUCCION DE EVENTOS</option>
                        <option {if $giro==='PUBLICIDAD Y PRODUCCIONES DE EVENTOS'}selected{/if} value="PUBLICIDAD Y PRODUCCIONES DE EVENTOS">PUBLICIDAD Y PRODUCCIONES DE EVENTOS</option>
                        <option {if $giro==='RELIGIOSO'}selected{/if} value="RELIGIOSO">RELIGIOSO</option>
                        <option {if $giro==='REPARACION DE AERONAVES, ARRIENDO DE INSTALACIONES'}selected{/if} value="REPARACION DE AERONAVES, ARRIENDO DE INSTALACIONES">REPARACION DE AERONAVES, ARRIENDO DE INSTALACIONES</option>
                        <option {if $giro==='REPARTICION ESTATAL'}selected{/if} value="REPARTICION ESTATAL">REPARTICION ESTATAL</option>
                        <option {if $giro==='REPARTICION FISCAL'}selected{/if} value="REPARTICION FISCAL">REPARTICION FISCAL</option>
                        <option {if $giro==='REPARTICION PUBLICA'}selected{/if} value="REPARTICION PUBLICA">REPARTICION PUBLICA</option>
                        <option {if $giro==='RESTAURANT'}selected{/if} value="RESTAURANT">RESTAURANT</option>
                        <option {if $giro==='SALUD'}selected{/if} value="SALUD">SALUD</option>
                        <option {if $giro==='SERV INMOB - CORRETAJE - ASESORIAS FINANCIERAS '}selected{/if} value="SERV INMOB - CORRETAJE - ASESORIAS FINANCIERAS ">SERV INMOB - CORRETAJE - ASESORIAS FINANCIERAS </option>
                        <option {if $giro==='SERVICIO DE ALIMENTACION ASEO LAVANDERIA Y MANTENC'}selected{/if} value="SERVICIO DE ALIMENTACION ASEO LAVANDERIA Y MANTENC">SERVICIO DE ALIMENTACION ASEO LAVANDERIA Y MANTENC</option>
                        <option {if $giro==='SERVICIO DE DESARROLLO SOCIAL'}selected{/if} value="SERVICIO DE DESARROLLO SOCIAL">SERVICIO DE DESARROLLO SOCIAL</option>
                        <option {if $giro==='SERVICIOS'}selected{/if} value="SERVICIOS">SERVICIOS</option>
                        <option {if $giro==='SERVICIOS DE ADMINISTRACION'}selected{/if} value="SERVICIOS DE ADMINISTRACION">SERVICIOS DE ADMINISTRACION</option>
                        <option {if $giro==='SERVICIOS DE ALIMENTACION'}selected{/if} value="SERVICIOS DE ALIMENTACION">SERVICIOS DE ALIMENTACION</option>
                        <option {if $giro==='SERVICIOS DE ASEO, JARDINERIA, MANTENCION DE AREAS'}selected{/if} value="SERVICIOS DE ASEO, JARDINERIA, MANTENCION DE AREAS">SERVICIOS DE ASEO, JARDINERIA, MANTENCION DE AREAS</option>
                        <option {if $giro==='SERVICIOS DE MANTENCION'}selected{/if} value="SERVICIOS DE MANTENCION">SERVICIOS DE MANTENCION</option>
                        <option {if $giro==='SERVICIOS DE SALUD DENTAL'}selected{/if} value="SERVICIOS DE SALUD DENTAL">SERVICIOS DE SALUD DENTAL</option>
                        <option {if $giro==='SERVICIOS GERENCIALES YADMINISTRATIVOS'}selected{/if} value="SERVICIOS GERENCIALES YADMINISTRATIVOS">SERVICIOS GERENCIALES YADMINISTRATIVOS</option>
                        <option {if $giro==='SERVICIOS INMOBILIARIOS Y ASESORIAS'}selected{/if} value="SERVICIOS INMOBILIARIOS Y ASESORIAS">SERVICIOS INMOBILIARIOS Y ASESORIAS</option>
                        <option {if $giro==='SERVICIOS MEDICOS'}selected{/if} value="SERVICIOS MEDICOS">SERVICIOS MEDICOS</option>
                        <option {if $giro==='SERVICIOS PROFESIONALES INDEPENDIENTES'}selected{/if} value="SERVICIOS PROFESIONALES INDEPENDIENTES">SERVICIOS PROFESIONALES INDEPENDIENTES</option>
                        <option {if $giro==='SOCIEDAD DE INVERSIONES'}selected{/if} value="SOCIEDAD DE INVERSIONES">SOCIEDAD DE INVERSIONES</option>
                        <option {if $giro==='SOCIEDADES DE INVERSION Y RENTISTA'}selected{/if} value="SOCIEDADES DE INVERSION Y RENTISTA">SOCIEDADES DE INVERSION Y RENTISTA</option>
                        <option {if $giro==='SUPERMERCADOS'}selected{/if} value="SUPERMERCADOS">SUPERMERCADOS</option>
                        <option {if $giro==='TERMINAL DE BUSES'}selected{/if} value="TERMINAL DE BUSES">TERMINAL DE BUSES</option>
                        <option {if $giro==='TRANSMISION Y DISTRIBUCION DE ENERGIA ELECTRICA'}selected{/if} value="TRANSMISION Y DISTRIBUCION DE ENERGIA ELECTRICA">TRANSMISION Y DISTRIBUCION DE ENERGIA ELECTRICA</option>
                        <option {if $giro==='TRANSPORTE'}selected{/if} value="TRANSPORTE">TRANSPORTE</option>
                        <option {if $giro==='TRANSPORTE AEREO'}selected{/if} value="TRANSPORTE AEREO">TRANSPORTE AEREO</option>
                        <option {if $giro==='TRANSPORTE DE CARGA'}selected{/if} value="TRANSPORTE DE CARGA">TRANSPORTE DE CARGA</option>
                        <option {if $giro==='TRANSPORTE FERROVIARIO DE PASAJEROS'}selected{/if} value="TRANSPORTE FERROVIARIO DE PASAJEROS">TRANSPORTE FERROVIARIO DE PASAJEROS</option>
                        <option {if $giro==='TRANSPORTE URBANO DE PASAJEROS'}selected{/if} value="TRANSPORTE URBANO DE PASAJEROS">TRANSPORTE URBANO DE PASAJEROS</option>
                        <option {if $giro==='TRANSPORTE VERTICAL'}selected{/if} value="TRANSPORTE VERTICAL">TRANSPORTE VERTICAL</option>
                        <option {if $giro==='UNIVERSIDAD'}selected{/if} value="UNIVERSIDAD">UNIVERSIDAD</option>
                        <option {if $giro==='VENTA AL POR MAYOR DE MATERIALES DE CONSTRUCCION'}selected{/if} value="VENTA AL POR MAYOR DE MATERIALES DE CONSTRUCCION">VENTA AL POR MAYOR DE MATERIALES DE CONSTRUCCION</option>
                        <option {if $giro==='VENTA AL POR MAYOR DE PRODUCTOS TEXTILES, PRENDAS'}selected{/if} value="VENTA AL POR MAYOR DE PRODUCTOS TEXTILES, PRENDAS">VENTA AL POR MAYOR DE PRODUCTOS TEXTILES, PRENDAS</option>
                        <option {if $giro==='VENTA AL POR MENOR EN COMERCIOS DE ALIME'}selected{/if} value="VENTA AL POR MENOR EN COMERCIOS DE ALIME">VENTA AL POR MENOR EN COMERCIOS DE ALIME</option>
                        <option {if $giro==='VENTA DE ALIMENTOS'}selected{/if} value="VENTA DE ALIMENTOS">VENTA DE ALIMENTOS</option>
                        <option {if $giro==='VENTA DE AUTOMOVILES Y PARTES'}selected{/if} value="VENTA DE AUTOMOVILES Y PARTES">VENTA DE AUTOMOVILES Y PARTES</option>
                        <option {if $giro==='VENTA DE PRENDAS DE VESTIR'}selected{/if} value="VENTA DE PRENDAS DE VESTIR">VENTA DE PRENDAS DE VESTIR</option>
                        <option {if $giro==='VENTA DE REPUESTOS DE ASCENSORES'}selected{/if} value="VENTA DE REPUESTOS DE ASCENSORES">VENTA DE REPUESTOS DE ASCENSORES</option>
                        <option {if $giro==='VENTAS DE SERVICIOS Y MAQUINARIAS'}selected{/if} value="VENTAS DE SERVICIOS Y MAQUINARIAS">VENTAS DE SERVICIOS Y MAQUINARIAS</option>
                    </select>
                </div>
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            </div>
        {elseif $country=='PY'}
            <div class="col-sm-1 col-md-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="gdfAddressAttribute8" name="gdfAddressAttribute8">
                        <option {if $gdfAddressAttribute8 ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL</option>
                        <option {if $gdfAddressAttribute8 ==='EXENTO'}selected{/if} value="EXENTO">EXENTO</option>
                        <option {if $gdfAddressAttribute8 ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR</option>
                        <option {if $gdfAddressAttribute8 ==='ZONA FRANCA'}selected{/if} value="ZONA FRANCA">ZONA FRANCA</option>
                    </select>
                </div>
                <small id="errorGdfAddressAttribute8 " class="form-text text-danger">&nbsp;{$errorGdfAddressAttribute8}</small>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-py mr-2 ml-2" title="PY only"/></div>
                    </div>
                    <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                        <option {if $paymentMethodName==='PY - ITAU MANUAL?'}selected{/if} value="PY - ITAU MANUAL">PY - ITAU MANUAL</option>
                        <option {if $paymentMethodName==='PY - ITAU - USD'}selected{/if} value="PY - ITAU - USD">PY - ITAU - USD</option>
                    </select>
                </div>
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            </div>
        {elseif $country=='UY'}
            <div class="col-sm-1 col-md-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="gdfAddressAttribute8" name="gdfAddressAttribute8">
                        <option {if $gdfAddressAttribute8 ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL</option>
                        <option {if $gdfAddressAttribute8 ==='EXENTO'}selected{/if} value="EXENTO">EXENTO</option>
                        <option {if $gdfAddressAttribute8 ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR</option>
                    </select>
                </div>
                <small id="errorGdfAddressAttribute8 " class="form-text text-danger">&nbsp;{$errorGdfAddressAttribute8}</small>
            </div>
            <div class="col-sm-4 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-uy mr-2 ml-2" title="UY only"/></div>
                    </div>
                    <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                        <option {if $paymentMethodName==='UY - SANTANDER MANUAL'}selected{/if} value="UY - SANTANDER MANUAL">UY - SANTANDER MANUAL</option>
                        <option {if $paymentMethodName==='UY - BROU - USD'}selected{/if} value="UY - BROU - USD">UY - BROU - USD</option>
                        <option {if $paymentMethodName==='UY - BROU MANUAL'}selected{/if} value="UY - BROU MANUAL">UY - BROU MANUAL</option>
                        <option {if $paymentMethodName==='UY - SANTANDER - EUR'}selected{/if} value="UY - SANTANDER - EUR">UY - SANTANDER - EUR</option>
                        <option {if $paymentMethodName==='UY - SANTANDER - USD'}selected{/if} value="UY - SANTANDER - USD">UY - SANTANDER - USD</option>
                    </select>
                </div>
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            </div>
        {/if}
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <label class="sr-only" for="inlineFormInputLocation">Location</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Location</div>
                </div>
                <input type="text" class="form-control" id="location" name="location" value="{$location}" placeholder="Location">
            </div>
            <small id="errorLocation" class="form-text text-danger">&nbsp;{$errorLocation}</small>
        </div>
        <div class="col-sm-3 my-1">
            <label class="sr-only" for="inlineFormInputSiteUseCode">Site Use Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Site Use Code</div>
                </div>
                <select class="form-control" id="siteUseCode" name="siteUseCode" aria-describedby="" placeholder="">
                    <option value="">--</option>
                    <option {if $siteUseCode==='BILL_TO'}selected{/if} value="BILL_TO">BILL_TO</option>
                    <option {if $siteUseCode==='SHIP_TO'}selected{/if} value="SHIP_TO">SHIP_TO</option>
                    <option {if $siteUseCode==='BOTH'}selected{/if} value="BOTH">BILL_TO &amp; SHIP_TO</option>
                    <option {if $siteUseCode==='DUN'}selected{/if} value="DUN">DUN</option>
                    <option {if $siteUseCode==='ALL'}selected{/if} value="ALL">ALL</option>
                </select>

            </div>
            <small id="errorSiteUseCode" class="form-text text-danger">&nbsp;{$errorSiteUseCode}</small>
        </div>
        <div class="col-sm-3 my-1">
            <label class="sr-only" for="inlineFormInputPrimarySiteUseFlag">Primary Site Use</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Primary Site Use</div>
                </div>
                <input type="checkbox" {($primarySiteUseFlag==true)?'checked':''} style="margin:10px;" id="primarySiteUseFlag" name="primarySiteUseFlag">
            </div>
            <small id="errorPrimarySiteUseFlag" class="form-text text-danger">&nbsp;{$errorPrimarySiteUseFlag}</small>
        </div>
    </div>
    {if $country=='CO'}
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-warning">Economic Activity <span class="flag-icon flag-icon-co mr-2 ml-2" title="CO only"/></div>
                </div>
                <select class="form-control" id="economicActivity" name="economicActivity" aria-describedby="" placeholder="">
                    <option {if $economicActivity==='0000'}selected{/if} value="0000">0000 - pendiente de informaci�n</option>
                    <option {if $economicActivity==='0111'}selected{/if} value="0111">0111 - Cultivo de cereales (excepto arroz), legumbres y semillas oleaginosas</option>
                    <option {if $economicActivity==='0112'}selected{/if} value="0112">0112 - Cultivo de arroz</option>
                    <option {if $economicActivity==='0113'}selected{/if} value="0113">0113 - Cultivo de hortalizas, ra?ces y tub?rculos</option>
                    <option {if $economicActivity==='0114'}selected{/if} value="0114">0114 - Cultivo de tabaco</option>
                    <option {if $economicActivity==='0115'}selected{/if} value="0115">0115 - Cultivo de plantas textiles</option>
                    <option {if $economicActivity==='0119'}selected{/if} value="0119">0119 - Otros cultivos transitorios n.c.p.</option>
                    <option {if $economicActivity==='0121'}selected{/if} value="0121">0121 - Cultivo de frutas tropicales y subtropicales</option>
                    <option {if $economicActivity==='0122'}selected{/if} value="0122">0122 - Cultivo de pl?tano y banano</option>
                    <option {if $economicActivity==='0123'}selected{/if} value="0123">0123 - Cultivo de caf?</option>
                    <option {if $economicActivity==='0124'}selected{/if} value="0124">0124 - Cultivo de ca?a de az?car</option>
                    <option {if $economicActivity==='0125'}selected{/if} value="0125">0125 - Cultivo de flor de corte</option>
                    <option {if $economicActivity==='0126'}selected{/if} value="0126">0126 - Cultivo de palma para aceite (palma africana) y otros frutos oleaginosos</option>
                    <option {if $economicActivity==='0127'}selected{/if} value="0127">0127 - Cultivo de plantas con las que se preparan bebidas</option>
                    <option {if $economicActivity==='0128'}selected{/if} value="0128">0128 - Cultivo de especias y de plantas arom?ticas y medicinales</option>
                    <option {if $economicActivity==='0129'}selected{/if} value="0129">0129 - Otros cultivos permanentes n.c.p.</option>
                    <option {if $economicActivity==='0130'}selected{/if} value="0130">0130 - Propagaci?n de plantas (actividades de los viveros, excepto viveros forestales)</option>
                    <option {if $economicActivity==='0141'}selected{/if} value="0141">0141 - Cr?a de ganado bovino y bufalino</option>
                    <option {if $economicActivity==='0142'}selected{/if} value="0142">0142 - Cr?a de caballos y otros equinos</option>
                    <option {if $economicActivity==='0143'}selected{/if} value="0143">0143 - Cr?a de ovejas y cabras</option>
                    <option {if $economicActivity==='0144'}selected{/if} value="0144">0144 - Cr?a de ganado porcino</option>
                    <option {if $economicActivity==='0145'}selected{/if} value="0145">0145 - Cr?a de aves de corral</option>
                    <option {if $economicActivity==='0149'}selected{/if} value="0149">0149 - Cr?a de otros animales n.c.p.</option>
                    <option {if $economicActivity==='0150'}selected{/if} value="0150">0150 - Explotaci?n mixta (agr?cola y pecuaria)</option>
                    <option {if $economicActivity==='0161'}selected{/if} value="0161">0161 - Actividades de apoyo a la agricultura</option>
                    <option {if $economicActivity==='0162'}selected{/if} value="0162">0162 - Actividades de apoyo a la ganader?a</option>
                    <option {if $economicActivity==='0163'}selected{/if} value="0163">0163 - Actividades posteriores a la cosecha</option>
                    <option {if $economicActivity==='0164'}selected{/if} value="0164">0164 - Tratamiento de semillas para propagaci?n</option>
                    <option {if $economicActivity==='0170'}selected{/if} value="0170">0170 - Caza ordinaria y mediante trampas y actividades de servicios conexas</option>
                    <option {if $economicActivity==='0210'}selected{/if} value="0210">0210 - Silvicultura y otras actividades forestales</option>
                    <option {if $economicActivity==='0220'}selected{/if} value="0220">0220 - Extracci?n de madera</option>
                    <option {if $economicActivity==='0230'}selected{/if} value="0230">0230 - Recolecci?n de productos forestales diferentes a la madera</option>
                    <option {if $economicActivity==='0240'}selected{/if} value="0240">0240 - Servicios de apoyo a la silvicultura</option>
                    <option {if $economicActivity==='0311'}selected{/if} value="0311">0311 - Pesca mar?tima</option>
                    <option {if $economicActivity==='0312'}selected{/if} value="0312">0312 - Pesca de agua dulce</option>
                    <option {if $economicActivity==='0321'}selected{/if} value="0321">0321 - Acuicultura mar?tima</option>
                    <option {if $economicActivity==='0322'}selected{/if} value="0322">0322 - Acuicultura de agua dulce</option>
                    <option {if $economicActivity==='0510'}selected{/if} value="0510">0510 - Extracci?n de hulla (carb?n de piedra)</option>
                    <option {if $economicActivity==='0520'}selected{/if} value="0520">0520 - Extracci?n de carb?n lignito</option>
                    <option {if $economicActivity==='0610'}selected{/if} value="0610">0610 - Extracci?n de petr?leo crudo</option>
                    <option {if $economicActivity==='0620'}selected{/if} value="0620">0620 - Extracci?n de gas natural</option>
                    <option {if $economicActivity==='0710'}selected{/if} value="0710">0710 - Extracci?n de minerales de hierro</option>
                    <option {if $economicActivity==='0721'}selected{/if} value="0721">0721 - Extracci?n de minerales de uranio y de torio</option>
                    <option {if $economicActivity==='0722'}selected{/if} value="0722">0722 - Extracci?n de oro y otros metales preciosos</option>
                    <option {if $economicActivity==='0723'}selected{/if} value="0723">0723 - Extracci?n de minerales de n?quel</option>
                    <option {if $economicActivity==='0729'}selected{/if} value="0729">0729 - Extracci?n de otros minerales metal?feros no ferrosos n.c.p.</option>
                    <option {if $economicActivity==='0811'}selected{/if} value="0811">0811 - Extracci?n de piedra, arena, arcillas comunes, yeso y anhidrita</option>
                    <option {if $economicActivity==='0812'}selected{/if} value="0812">0812 - Extracci?n de arcillas de uso industrial, caliza, caol?n y bentonitas</option>
                    <option {if $economicActivity==='0820'}selected{/if} value="0820">0820 - Extracci?n de esmeraldas, piedras preciosas y semipreciosas</option>
                    <option {if $economicActivity==='0891'}selected{/if} value="0891">0891 - Extracci?n de minerales para la fabricaci?n de abonos y productos qu?micos</option>
                    <option {if $economicActivity==='0892'}selected{/if} value="0892">0892 - Extracci?n de halita (sal)</option>
                    <option {if $economicActivity==='0899'}selected{/if} value="0899">0899 - Extracci?n de otros minerales no met?licos n.c.p.</option>
                    <option {if $economicActivity==='0910'}selected{/if} value="0910">0910 - Actividades de apoyo para la extracci?n de petr?leo y de gas natural</option>
                    <option {if $economicActivity==='0990'}selected{/if} value="0990">0990 - Actividades de apoyo para otras actividades de explotaci?n de minas y canteras</option>
                    <option {if $economicActivity==='1011'}selected{/if} value="1011">1011 - Procesamiento y conservaci?n de carne y productos c?rnicos</option>
                    <option {if $economicActivity==='1012'}selected{/if} value="1012">1012 - Procesamiento y conservaci?n de pescados, crust?ceos y moluscos</option>
                    <option {if $economicActivity==='1020'}selected{/if} value="1020">1020 - Procesamiento y conservaci?n de frutas, legumbres, hortalizas y tub?rculos</option>
                    <option {if $economicActivity==='1030'}selected{/if} value="1030">1030 - Elaboraci?n de aceites y grasas de origen vegetal y animal</option>
                    <option {if $economicActivity==='1040'}selected{/if} value="1040">1040 - Elaboraci?n de productos l?cteos</option>
                    <option {if $economicActivity==='1051'}selected{/if} value="1051">1051 - Elaboraci?n de productos de moliner?a</option>
                    <option {if $economicActivity==='1052'}selected{/if} value="1052">1052 - Elaboraci?n de almidones y productos derivados del almid?n</option>
                    <option {if $economicActivity==='1061'}selected{/if} value="1061">1061 - Trilla de caf?</option>
                    <option {if $economicActivity==='1062'}selected{/if} value="1062">1062 - Descafeinado, tosti?n y molienda del caf?</option>
                    <option {if $economicActivity==='1063'}selected{/if} value="1063">1063 - Otros derivados del caf?</option>
                    <option {if $economicActivity==='1071'}selected{/if} value="1071">1071 - Elaboraci?n y refinaci?n de az?car</option>
                    <option {if $economicActivity==='1072'}selected{/if} value="1072">1072 - Elaboraci?n de panela</option>
                    <option {if $economicActivity==='1081'}selected{/if} value="1081">1081 - Elaboraci?n de productos de panader?a</option>
                    <option {if $economicActivity==='1082'}selected{/if} value="1082">1082 - Elaboraci?n de cacao, chocolate y productos de confiter?a</option>
                    <option {if $economicActivity==='1083'}selected{/if} value="1083">1083 - Elaboraci?n de macarrones, fideos, alcuzcuz y productos farin?ceos similares</option>
                    <option {if $economicActivity==='1084'}selected{/if} value="1084">1084 - Elaboraci?n de comidas y platos preparados</option>
                    <option {if $economicActivity==='1089'}selected{/if} value="1089">1089 - Elaboraci?n de otros productos alimenticios n.c.p.</option>
                    <option {if $economicActivity==='1090'}selected{/if} value="1090">1090 - Elaboraci?n de alimentos preparados para animales</option>
                    <option {if $economicActivity==='11001-0140'}selected{/if} value="11001-0140">11001-0140 - Activity of Services Agricolas and Ganaders - Veterinary</option>
                    <option {if $economicActivity==='1101'}selected{/if} value="1101">1101 - Destilaci?n, rectificaci?n y mezcla de bebidas alcoh?licas</option>
                    <option {if $economicActivity==='1102'}selected{/if} value="1102">1102 - Elaboraci?n de bebidas fermentadas no destiladas</option>
                    <option {if $economicActivity==='1103'}selected{/if} value="1103">1103 - Producci?n de malta, elaboraci?n de cervezas y otras bebidas malteadas</option>
                    <option {if $economicActivity==='1104'}selected{/if} value="1104">1104 - Elaboraci?n de bebidas no alcoh?licas, producci?n de aguas minerales y de otras aguas embotelladas</option>
                    <option {if $economicActivity==='1200'}selected{/if} value="1200">1200 - Elaboraci?n de productos de tabaco</option>
                    <option {if $economicActivity==='1311'}selected{/if} value="1311">1311 - Preparaci?n e hilatura de fibras textiles</option>
                    <option {if $economicActivity==='1312'}selected{/if} value="1312">1312 - Tejedur?a de productos textiles</option>
                    <option {if $economicActivity==='1313'}selected{/if} value="1313">1313 - Acabado de productos textiles</option>
                    <option {if $economicActivity==='1391'}selected{/if} value="1391">1391 - Fabricaci?n de tejidos de punto y ganchillo</option>
                    <option {if $economicActivity==='1392'}selected{/if} value="1392">1392 - Confecci?n de art?culos con materiales textiles, excepto prendas de vestir</option>
                    <option {if $economicActivity==='1393'}selected{/if} value="1393">1393 - Fabricaci?n de tapetes y alfombras para pisos</option>
                    <option {if $economicActivity==='1394'}selected{/if} value="1394">1394 - Fabricaci?n de cuerdas, cordeles, cables, bramantes y redes</option>
                    <option {if $economicActivity==='1399'}selected{/if} value="1399">1399 - Fabricaci?n de otros art?culos textiles n.c.p.</option>
                    <option {if $economicActivity==='1410'}selected{/if} value="1410">1410 - Confecci?n de prendas de vestir, excepto prendas de piel</option>
                    <option {if $economicActivity==='1420'}selected{/if} value="1420">1420 - Fabricaci?n de art?culos de piel</option>
                    <option {if $economicActivity==='1430'}selected{/if} value="1430">1430 - Fabricaci?n de art?culos de punto y ganchillo</option>
                    <option {if $economicActivity==='1511'}selected{/if} value="1511">1511 - Curtido y recurtido de cueros; recurtido y te?ido de pieles</option>
                    <option {if $economicActivity==='1512'}selected{/if} value="1512">1512 - Fabricaci?n de art?culos de viaje, bolsos de mano y art?culos similares elaborados en cuero, y fabricaci?n de art?culos de talabarter?a y guarnicioner?a</option>
                    <option {if $economicActivity==='1513'}selected{/if} value="1513">1513 - Fabricaci?n de art?culos de viaje, bolsos de mano y art?culos similares; art?culos de talabarter?a y guarnicioner?a elaborados en otros materiales</option>
                    <option {if $economicActivity==='1521'}selected{/if} value="1521">1521 - Fabricaci?n de calzado de cuero y piel, con cualquier tipo de suela</option>
                    <option {if $economicActivity==='1522'}selected{/if} value="1522">1522 - Fabricaci?n de otros tipos de calzado, excepto calzado de cuero y piel</option>
                    <option {if $economicActivity==='1523'}selected{/if} value="1523">1523 - Fabricaci?n de partes del calzado</option>
                    <option {if $economicActivity==='1610'}selected{/if} value="1610">1610 - Aserrado, acepillado e impregnaci?n de la madera</option>
                    <option {if $economicActivity==='1620'}selected{/if} value="1620">1620 - Fabricaci?n de hojas de madera para enchapado; fabricaci?n de tableros contrachapados, tableros laminados, tableros de part?culas y otros tableros y paneles</option>
                    <option {if $economicActivity==='1630'}selected{/if} value="1630">1630 - Fabricaci?n de partes y piezas de madera, de carpinter?a y ebanister?a para la construcci?n</option>
                    <option {if $economicActivity==='1640'}selected{/if} value="1640">1640 - Fabricaci?n de recipientes de madera</option>
                    <option {if $economicActivity==='1690'}selected{/if} value="1690">1690 - Fabricaci?n de otros productos de madera; fabricaci?n de art?culos de corcho, cester?a y esparter?a</option>
                    <option {if $economicActivity==='1701'}selected{/if} value="1701">1701 - Fabricaci?n de pulpas (pastas) celul?sicas; papel y cart?n</option>
                    <option {if $economicActivity==='1702'}selected{/if} value="1702">1702 - Fabricaci?n de papel y cart?n ondulado (corrugado); fabricaci?n de envases, empaques y de embalajes de papel y cart?n</option>
                    <option {if $economicActivity==='1709'}selected{/if} value="1709">1709 - Fabricaci?n de otros art?culos de papel y cart?n</option>
                    <option {if $economicActivity==='1811'}selected{/if} value="1811">1811 - Actividades de impresi?n</option>
                    <option {if $economicActivity==='1812'}selected{/if} value="1812">1812 - Actividades de servicios relacionados con la impresi?n</option>
                    <option {if $economicActivity==='1820'}selected{/if} value="1820">1820 - Producci?n de copias a partir de grabaciones originales</option>
                    <option {if $economicActivity==='1910'}selected{/if} value="1910">1910 - Fabricaci?n de productos de hornos de coque</option>
                    <option {if $economicActivity==='1921'}selected{/if} value="1921">1921 - Fabricaci?n de productos de la refinaci?n del petr?leo</option>
                    <option {if $economicActivity==='1922'}selected{/if} value="1922">1922 - Actividad de mezcla de combustibles</option>
                    <option {if $economicActivity==='2011'}selected{/if} value="2011">2011 - Fabricaci?n de sustancias y productos qu?micos b?sicos</option>
                    <option {if $economicActivity==='2012'}selected{/if} value="2012">2012 - Fabricaci?n de abonos y compuestos inorg?nicos nitrogenados</option>
                    <option {if $economicActivity==='2013'}selected{/if} value="2013">2013 - Fabricaci?n de pl?sticos en formas primarias</option>
                    <option {if $economicActivity==='2014'}selected{/if} value="2014">2014 - Fabricaci?n de caucho sint?tico en formas primarias</option>
                    <option {if $economicActivity==='2021'}selected{/if} value="2021">2021 - Fabricaci?n de plaguicidas y otros productos qu?micos de uso agropecuario</option>
                    <option {if $economicActivity==='2022'}selected{/if} value="2022">2022 - Fabricaci?n de pinturas, barnices y revestimientos similares, tintas para impresi?n y masillas</option>
                    <option {if $economicActivity==='2023'}selected{/if} value="2023">2023 - Fabricaci?n de jabones y detergentes, preparados para limpiar y pulir; perfumes y preparados de tocador</option>
                    <option {if $economicActivity==='2029'}selected{/if} value="2029">2029 - Fabricaci?n de otros productos qu?micos n.c.p.</option>
                    <option {if $economicActivity==='2030'}selected{/if} value="2030">2030 - Fabricaci?n de fibras sint?ticas y artificiales</option>
                    <option {if $economicActivity==='2100'}selected{/if} value="2100">2100 - Fabricaci?n de productos farmac?uticos, sustancias qu?micas medicinales y productos bot?nicos de uso farmac?utico</option>
                    <option {if $economicActivity==='2211'}selected{/if} value="2211">2211 - Fabricaci?n de llantas y neum?ticos de caucho</option>
                    <option {if $economicActivity==='2212'}selected{/if} value="2212">2212 - Reencauche de llantas usadas</option>
                    <option {if $economicActivity==='2219'}selected{/if} value="2219">2219 - Fabricaci?n de formas b?sicas de caucho y otros productos de caucho n.c.p.</option>
                    <option {if $economicActivity==='2221'}selected{/if} value="2221">2221 - Fabricaci?n de formas b?sicas de pl?stico</option>
                    <option {if $economicActivity==='2229'}selected{/if} value="2229">2229 - Fabricaci?n de art?culos de pl?stico n.c.p.</option>
                    <option {if $economicActivity==='2310'}selected{/if} value="2310">2310 - Fabricaci?n de vidrio y productos de vidrio</option>
                    <option {if $economicActivity==='2391'}selected{/if} value="2391">2391 - Fabricaci?n de productos refractarios</option>
                    <option {if $economicActivity==='2392'}selected{/if} value="2392">2392 - Fabricaci?n de materiales de arcilla para la construcci?n</option>
                    <option {if $economicActivity==='2393'}selected{/if} value="2393">2393 - Fabricaci?n de otros productos de cer?mica y porcelana</option>
                    <option {if $economicActivity==='2394'}selected{/if} value="2394">2394 - Fabricaci?n de cemento, cal y yeso</option>
                    <option {if $economicActivity==='2395'}selected{/if} value="2395">2395 - Fabricaci?n de art?culos de hormig?n, cemento y yeso</option>
                    <option {if $economicActivity==='2396'}selected{/if} value="2396">2396 - Corte, tallado y acabado de la piedra</option>
                    <option {if $economicActivity==='2399'}selected{/if} value="2399">2399 - Fabricaci?n de otros productos minerales no met?licos n.c.p.</option>
                    <option {if $economicActivity==='2410'}selected{/if} value="2410">2410 - Industrias b?sicas de hierro y de acero</option>
                    <option {if $economicActivity==='2421'}selected{/if} value="2421">2421 - Industrias b?sicas de metales preciosos</option>
                    <option {if $economicActivity==='2429'}selected{/if} value="2429">2429 - Industrias b?sicas de otros metales no ferrosos</option>
                    <option {if $economicActivity==='2431'}selected{/if} value="2431">2431 - Fundici?n de hierro y de acero</option>
                    <option {if $economicActivity==='2432'}selected{/if} value="2432">2432 - Fundici?n de metales no ferrosos</option>
                    <option {if $economicActivity==='2511'}selected{/if} value="2511">2511 - Fabricaci?n de productos met?licos para uso estructural</option>
                    <option {if $economicActivity==='2512'}selected{/if} value="2512">2512 - Fabricaci?n de tanques, dep?sitos y recipientes de metal, excepto los utilizados para el envase o transporte de mercanc?as</option>
                    <option {if $economicActivity==='2513'}selected{/if} value="2513">2513 - Fabricaci?n de generadores de vapor, excepto calderas de agua caliente para calefacci?n central</option>
                    <option {if $economicActivity==='2520'}selected{/if} value="2520">2520 - Fabricaci?n de armas y municiones</option>
                    <option {if $economicActivity==='2591'}selected{/if} value="2591">2591 - Forja, prensado, estampado y laminado de metal; pulvimetalurgia</option>
                    <option {if $economicActivity==='2592'}selected{/if} value="2592">2592 - Tratamiento y revestimiento de metales; mecanizado</option>
                    <option {if $economicActivity==='2593'}selected{/if} value="2593">2593 - Fabricaci?n de art?culos de cuchiller?a, herramientas de mano y art?culos de ferreter?a</option>
                    <option {if $economicActivity==='2599'}selected{/if} value="2599">2599 - Fabricaci?n de otros productos elaborados de metal n.c.p.</option>
                    <option {if $economicActivity==='2610'}selected{/if} value="2610">2610 - Fabricaci?n de componentes y tableros electr?nicos</option>
                    <option {if $economicActivity==='2620'}selected{/if} value="2620">2620 - Fabricaci?n de computadoras y de equipo perif?rico</option>
                    <option {if $economicActivity==='2630'}selected{/if} value="2630">2630 - Fabricaci?n de equipos de comunicaci?n</option>
                    <option {if $economicActivity==='2640'}selected{/if} value="2640">2640 - Fabricaci?n de aparatos electr?nicos de consumo</option>
                    <option {if $economicActivity==='2651'}selected{/if} value="2651">2651 - Fabricaci?n de equipo de medici?n, prueba, navegaci?n y control</option>
                    <option {if $economicActivity==='2652'}selected{/if} value="2652">2652 - Fabricaci?n de relojes</option>
                    <option {if $economicActivity==='2660'}selected{/if} value="2660">2660 - Fabricaci?n de equipo de irradiaci?n y equipo electr?nico de uso m?dico y terap?utico</option>
                    <option {if $economicActivity==='2670'}selected{/if} value="2670">2670 - Fabricaci?n de instrumentos ?pticos y equipo fotogr?fico</option>
                    <option {if $economicActivity==='2680'}selected{/if} value="2680">2680 - Fabricaci?n de medios magn?ticos y ?pticos para almacenamiento de datos</option>
                    <option {if $economicActivity==='2711'}selected{/if} value="2711">2711 - Fabricaci?n de motores, generadores y transformadores el?ctricos</option>
                    <option {if $economicActivity==='2712'}selected{/if} value="2712">2712 - Fabricaci?n de aparatos de distribuci?n y control de la energ?a el?ctrica</option>
                    <option {if $economicActivity==='2720'}selected{/if} value="2720">2720 - Fabricaci?n de pilas, bater?as y acumuladores el?ctricos</option>
                    <option {if $economicActivity==='2731'}selected{/if} value="2731">2731 - Fabricaci?n de hilos y cables el?ctricos y de fibra ?ptica</option>
                    <option {if $economicActivity==='2732'}selected{/if} value="2732">2732 - Fabricaci?n de dispositivos de cableado</option>
                    <option {if $economicActivity==='2740'}selected{/if} value="2740">2740 - Fabricaci?n de equipos el?ctricos de iluminaci?n</option>
                    <option {if $economicActivity==='2750'}selected{/if} value="2750">2750 - Fabricaci?n de aparatos de uso dom?stico</option>
                    <option {if $economicActivity==='2790'}selected{/if} value="2790">2790 - Fabricaci?n de otros tipos de equipo el?ctrico n.c.p.</option>
                    <option {if $economicActivity==='2811'}selected{/if} value="2811">2811 - Fabricaci?n de motores, turbinas, y partes para motores de combusti?n interna</option>
                    <option {if $economicActivity==='2812'}selected{/if} value="2812">2812 - Fabricaci?n de equipos de potencia hidr?ulica y neum?tica</option>
                    <option {if $economicActivity==='2813'}selected{/if} value="2813">2813 - Fabricaci?n de otras bombas, compresores, grifos y v?lvulas</option>
                    <option {if $economicActivity==='2814'}selected{/if} value="2814">2814 - Fabricaci?n de cojinetes, engranajes, trenes de engranajes y piezas de transmisi?n</option>
                    <option {if $economicActivity==='2815'}selected{/if} value="2815">2815 - Fabricaci?n de hornos, hogares y quemadores industriales</option>
                    <option {if $economicActivity==='2816'}selected{/if} value="2816">2816 - Fabricaci?n de equipo de elevaci?n y manipulaci?n</option>
                    <option {if $economicActivity==='2817'}selected{/if} value="2817">2817 - Fabricaci?n de maquinaria y equipo de oficina (excepto computadoras y equipo perif?rico)</option>
                    <option {if $economicActivity==='2818'}selected{/if} value="2818">2818 - Fabricaci?n de herramientas manuales con motor</option>
                    <option {if $economicActivity==='2819'}selected{/if} value="2819">2819 - Fabricaci?n de otros tipos de maquinaria y equipo de uso general n.c.p.</option>
                    <option {if $economicActivity==='2821'}selected{/if} value="2821">2821 - Fabricaci?n de maquinaria agropecuaria y forestal</option>
                    <option {if $economicActivity==='2822'}selected{/if} value="2822">2822 - Fabricaci?n de m?quinas formadoras de metal y de m?quinas herramienta</option>
                    <option {if $economicActivity==='2823'}selected{/if} value="2823">2823 - Fabricaci?n de maquinaria para la metalurgia</option>
                    <option {if $economicActivity==='2824'}selected{/if} value="2824">2824 - Fabricaci?n de maquinaria para explotaci?n de minas y canteras y para obras de construcci?n</option>
                    <option {if $economicActivity==='2825'}selected{/if} value="2825">2825 - Fabricaci?n de maquinaria para la elaboraci?n de alimentos, bebidas y tabaco</option>
                    <option {if $economicActivity==='2826'}selected{/if} value="2826">2826 - Fabricaci?n de maquinaria para la elaboraci?n de productos textiles, prendas de vestir y cueros</option>
                    <option {if $economicActivity==='2829'}selected{/if} value="2829">2829 - Fabricaci?n de otros tipos de maquinaria y equipo de uso especial n.c.p.</option>
                    <option {if $economicActivity==='2910'}selected{/if} value="2910">2910 - Fabricaci?n de veh?culos automotores y sus motores</option>
                    <option {if $economicActivity==='2920'}selected{/if} value="2920">2920 - Fabricaci?n de carrocer?as para veh?culos automotores; fabricaci?n de remolques y semirremolques</option>
                    <option {if $economicActivity==='2930'}selected{/if} value="2930">2930 - Fabricaci?n de partes, piezas (autopartes) y accesorios (lujos) para veh?culos automotores</option>
                    <option {if $economicActivity==='3011'}selected{/if} value="3011">3011 - Construcci?n de barcos y de estructuras flotantes</option>
                    <option {if $economicActivity==='3012'}selected{/if} value="3012">3012 - Construcci?n de embarcaciones de recreo y deporte</option>
                    <option {if $economicActivity==='3020'}selected{/if} value="3020">3020 - Fabricaci?n de locomotoras y de material rodante para ferrocarriles</option>
                    <option {if $economicActivity==='3030'}selected{/if} value="3030">3030 - Fabricaci?n de aeronaves, naves espaciales y de maquinaria conexa</option>
                    <option {if $economicActivity==='3040'}selected{/if} value="3040">3040 - Fabricaci?n de veh?culos militares de combate</option>
                    <option {if $economicActivity==='3091'}selected{/if} value="3091">3091 - Fabricaci?n de motocicletas</option>
                    <option {if $economicActivity==='3092'}selected{/if} value="3092">3092 - Fabricaci?n de bicicletas y de sillas de ruedas para personas con discapacidad</option>
                    <option {if $economicActivity==='3099'}selected{/if} value="3099">3099 - Fabricaci?n de otros tipos de equipo de transporte n.c.p.</option>
                    <option {if $economicActivity==='3110'}selected{/if} value="3110">3110 - Fabricaci?n de muebles</option>
                    <option {if $economicActivity==='3120'}selected{/if} value="3120">3120 - Fabricaci?n de colchones y somieres</option>
                    <option {if $economicActivity==='3210'}selected{/if} value="3210">3210 - Fabricaci?n de joyas, bisuter?a y art?culos conexos</option>
                    <option {if $economicActivity==='3220'}selected{/if} value="3220">3220 - Fabricaci?n de instrumentos musicales</option>
                    <option {if $economicActivity==='3230'}selected{/if} value="3230">3230 - Fabricaci?n de art?culos y equipo para la pr?ctica del deporte</option>
                    <option {if $economicActivity==='3240'}selected{/if} value="3240">3240 - Fabricaci?n de juegos, juguetes y rompecabezas</option>
                    <option {if $economicActivity==='3250'}selected{/if} value="3250">3250 - Fabricaci?n de instrumentos, aparatos y materiales m?dicos y odontol?gicos (incluido mobiliario)</option>
                    <option {if $economicActivity==='3290'}selected{/if} value="3290">3290 - Otras industrias manufactureras n.c.p.</option>
                    <option {if $economicActivity==='3311'}selected{/if} value="3311">3311 - Mantenimiento y reparaci?n especializado de productos elaborados en metal</option>
                    <option {if $economicActivity==='3312'}selected{/if} value="3312">3312 - Mantenimiento y reparaci?n especializado de maquinaria y equipo</option>
                    <option {if $economicActivity==='3313'}selected{/if} value="3313">3313 - Mantenimiento y reparaci?n especializado de equipo electr?nico y ?ptico</option>
                    <option {if $economicActivity==='3314'}selected{/if} value="3314">3314 - Mantenimiento y reparaci?n especializado de equipo el?ctrico</option>
                    <option {if $economicActivity==='3315'}selected{/if} value="3315">3315 - Mantenimiento y reparaci?n especializado de equipo de transporte, excepto los veh?culos automotores, motocicletas y bicicletas</option>
                    <option {if $economicActivity==='3319'}selected{/if} value="3319">3319 - Mantenimiento y reparaci?n de otros tipos de equipos y sus componentes n.c.p.</option>
                    <option {if $economicActivity==='3320'}selected{/if} value="3320">3320 - Instalaci?n especializada de maquinaria y equipo industrial</option>
                    <option {if $economicActivity==='3511'}selected{/if} value="3511">3511 - Generaci?n de energ?a el?ctrica</option>
                    <option {if $economicActivity==='3512'}selected{/if} value="3512">3512 - Transmisi?n de energ?a el?ctrica</option>
                    <option {if $economicActivity==='3513'}selected{/if} value="3513">3513 - Distribuci?n de energ?a el?ctrica</option>
                    <option {if $economicActivity==='3514'}selected{/if} value="3514">3514 - Comercializaci?n de energ?a el?ctrica</option>
                    <option {if $economicActivity==='3520'}selected{/if} value="3520">3520 - Producci?n de gas; distribuci?n de combustibles gaseosos por tuber?as</option>
                    <option {if $economicActivity==='3530'}selected{/if} value="3530">3530 - Suministro de vapor y aire acondicionado</option>
                    <option {if $economicActivity==='3600'}selected{/if} value="3600">3600 - Captaci?n, tratamiento y distribuci?n de agua</option>
                    <option {if $economicActivity==='3700'}selected{/if} value="3700">3700 - Evacuaci?n y tratamiento de aguas residuales</option>
                    <option {if $economicActivity==='3811'}selected{/if} value="3811">3811 - Recolecci?n de desechos no peligrosos</option>
                    <option {if $economicActivity==='3812'}selected{/if} value="3812">3812 - Recolecci?n de desechos peligrosos</option>
                    <option {if $economicActivity==='3821'}selected{/if} value="3821">3821 - Tratamiento y disposici?n de desechos no peligrosos</option>
                    <option {if $economicActivity==='3822'}selected{/if} value="3822">3822 - Tratamiento y disposici?n de desechos peligrosos</option>
                    <option {if $economicActivity==='3830'}selected{/if} value="3830">3830 - Recuperaci?n de materiales</option>
                    <option {if $economicActivity==='3900'}selected{/if} value="3900">3900 - Actividades de saneamiento ambiental y otros servicios de gesti?n de desechos</option>
                    <option {if $economicActivity==='4111'}selected{/if} value="4111">4111 - Construcci?n de edificios residenciales</option>
                    <option {if $economicActivity==='4112'}selected{/if} value="4112">4112 - Construcci?n de edificios no residenciales</option>
                    <option {if $economicActivity==='4210'}selected{/if} value="4210">4210 - Construcci?n de carreteras y v?as de ferrocarril</option>
                    <option {if $economicActivity==='4220'}selected{/if} value="4220">4220 - Construcci?n de proyectos de servicio p?blico</option>
                    <option {if $economicActivity==='4290'}selected{/if} value="4290">4290 - Construcci?n de otras obras de ingenier?a civil</option>
                    <option {if $economicActivity==='4311'}selected{/if} value="4311">4311 - Demolici?n</option>
                    <option {if $economicActivity==='4312'}selected{/if} value="4312">4312 - Preparaci?n del terreno</option>
                    <option {if $economicActivity==='4321'}selected{/if} value="4321">4321 - Instalaciones el?ctricas</option>
                    <option {if $economicActivity==='4322'}selected{/if} value="4322">4322 - Instalaciones de fontaner?a, calefacci?n y aire acondicionado</option>
                    <option {if $economicActivity==='4329'}selected{/if} value="4329">4329 - Otras instalaciones especializadas</option>
                    <option {if $economicActivity==='4330'}selected{/if} value="4330">4330 - Terminaci?n y acabado de edificios y obras de ingenier?a civil</option>
                    <option {if $economicActivity==='4390'}selected{/if} value="4390">4390 - Otras actividades especializadas para la construcci?n de edificios y obras de ingenier?a civil</option>
                    <option {if $economicActivity==='4511'}selected{/if} value="4511">4511 - Comercio de veh?culos automotores nuevos</option>
                    <option {if $economicActivity==='4512'}selected{/if} value="4512">4512 - Comercio de veh?culos automotores usados</option>
                    <option {if $economicActivity==='4520'}selected{/if} value="4520">4520 - Mantenimiento y reparaci?n de veh?culos automotores</option>
                    <option {if $economicActivity==='4530'}selected{/if} value="4530">4530 - Comercio de partes, piezas (autopartes) y accesorios (lujos) para veh?culos automotores</option>
                    <option {if $economicActivity==='4541'}selected{/if} value="4541">4541 - Comercio de motocicletas y de sus partes, piezas y accesorios</option>
                    <option {if $economicActivity==='4542'}selected{/if} value="4542">4542 - Mantenimiento y reparaci?n de motocicletas y de sus partes y piezas</option>
                    <option {if $economicActivity==='4610'}selected{/if} value="4610">4610 - Comercio al por mayor a cambio de una retribuci?n o por contrata</option>
                    <option {if $economicActivity==='4620'}selected{/if} value="4620">4620 - Comercio al por mayor de materias primas agropecuarias; animales vivos</option>
                    <option {if $economicActivity==='4631'}selected{/if} value="4631">4631 - Comercio al por mayor de productos alimenticios</option>
                    <option {if $economicActivity==='4632'}selected{/if} value="4632">4632 - Comercio al por mayor de bebidas y tabaco</option>
                    <option {if $economicActivity==='4641'}selected{/if} value="4641">4641 - Comercio al por mayor de productos textiles, productos confeccionados para uso dom?stico</option>
                    <option {if $economicActivity==='4642'}selected{/if} value="4642">4642 - Comercio al por mayor de prendas de vestir</option>
                    <option {if $economicActivity==='4643'}selected{/if} value="4643">4643 - Comercio al por mayor de calzado</option>
                    <option {if $economicActivity==='4644'}selected{/if} value="4644">4644 - Comercio al por mayor de aparatos y equipo de uso dom?stico</option>
                    <option {if $economicActivity==='4645'}selected{/if} value="4645">4645 - Comercio al por mayor de productos farmac?uticos, medicinales, cosm?ticos y de tocador</option>
                    <option {if $economicActivity==='4649'}selected{/if} value="4649">4649 - Comercio al por mayor de otros utensilios dom?sticos n.c.p.</option>
                    <option {if $economicActivity==='4651'}selected{/if} value="4651">4651 - Comercio al por mayor de computadores, equipo perif?rico y programas de inform?tica</option>
                    <option {if $economicActivity==='4652'}selected{/if} value="4652">4652 - Comercio al por mayor de equipo, partes y piezas electr?nicos y de telecomunicaciones</option>
                    <option {if $economicActivity==='4653'}selected{/if} value="4653">4653 - Comercio al por mayor de maquinaria y equipo agropecuarios</option>
                    <option {if $economicActivity==='4659'}selected{/if} value="4659">4659 - Comercio al por mayor de otros tipos de maquinaria y equipo n.c.p.</option>
                    <option {if $economicActivity==='4661'}selected{/if} value="4661">4661 - Comercio al por mayor de combustibles s?lidos, l?quidos, gaseosos y productos conexos</option>
                    <option {if $economicActivity==='4662'}selected{/if} value="4662">4662 - Comercio al por mayor de metales y productos metal?feros</option>
                    <option {if $economicActivity==='4663'}selected{/if} value="4663">4663 - Comercio al por mayor de materiales de construcci?n, art?culos de ferreter?a, pinturas, productos</option>
                    <option {if $economicActivity==='4664'}selected{/if} value="4664">4664 - Comercio al por mayor de productos qu?micos b?sicos, cauchos y pl?sticos en formas primarias y productos qu?micos de uso agropecuario</option>
                    <option {if $economicActivity==='4665'}selected{/if} value="4665">4665 - Comercio al por mayor de desperdicios, desechos y chatarra</option>
                    <option {if $economicActivity==='4669'}selected{/if} value="4669">4669 - Comercio al por mayor de otros productos n.c.p.</option>
                    <option {if $economicActivity==='4690'}selected{/if} value="4690">4690 - Comercio al por mayor no especializado</option>
                    <option {if $economicActivity==='4711'}selected{/if} value="4711">4711 - Comercio al por menor en establecimientos no especializados con surtido compuesto principalmente por alimentos, bebidas o tabaco</option>
                    <option {if $economicActivity==='4719'}selected{/if} value="4719">4719 - Comercio al por menor en establecimientos no especializados, con surtido compuesto principalmente por productos diferentes de alimentos (v?veres en general), bebidas y tabaco</option>
                    <option {if $economicActivity==='4721'}selected{/if} value="4721">4721 - Comercio al por menor de productos agr?colas para el consumo en establecimientos especializados</option>
                    <option {if $economicActivity==='4722'}selected{/if} value="4722">4722 - Comercio al por menor de leche, productos l?cteos y huevos, en establecimientos especializados</option>
                    <option {if $economicActivity==='4723'}selected{/if} value="4723">4723 - Comercio al por menor de carnes (incluye aves de corral), productos c?rnicos, pescados y productos de mar, en establecimientos especializados</option>
                    <option {if $economicActivity==='4724'}selected{/if} value="4724">4724 - Comercio al por menor de bebidas y productos del tabaco, en establecimientos especializados</option>
                    <option {if $economicActivity==='4729'}selected{/if} value="4729">4729 - Comercio al por menor de otros productos alimenticios n.c.p., en establecimientos especializados</option>
                    <option {if $economicActivity==='4731'}selected{/if} value="4731">4731 - Comercio al por menor de combustible para automotores</option>
                    <option {if $economicActivity==='4732'}selected{/if} value="4732">4732 - Comercio al por menor de lubricantes (aceites, grasas), aditivos y productos de limpieza para veh?culos automotores</option>
                    <option {if $economicActivity==='4741'}selected{/if} value="4741">4741 - Comercio al por menor de computadores, equipos perif?ricos, programas de inform?tica y equipos de telecomunicaciones en establecimientos especializados</option>
                    <option {if $economicActivity==='4742'}selected{/if} value="4742">4742 - Comercio al por menor de equipos y aparatos de sonido y de video, en establecimientos especializados</option>
                    <option {if $economicActivity==='4751'}selected{/if} value="4751">4751 - Comercio al por menor de productos textiles en establecimientos especializados</option>
                    <option {if $economicActivity==='4752'}selected{/if} value="4752">4752 - Comercio al por menor de art?culos de ferreter?a, pinturas y productos de vidrio en establecimientos especializados</option>
                    <option {if $economicActivity==='4753'}selected{/if} value="4753">4753 - Comercio al por menor de tapices, alfombras y cubrimientos para paredes y pisos en establecimientos especializados</option>
                    <option {if $economicActivity==='4754'}selected{/if} value="4754">4754 - Comercio al por menor de electrodom?sticos y gasodom?sticos de uso dom?stico, muebles y equipos de iluminaci?n</option>
                    <option {if $economicActivity==='4755'}selected{/if} value="4755">4755 - Comercio al por menor de art?culos y utensilios de uso dom?stico</option>
                    <option {if $economicActivity==='4759'}selected{/if} value="4759">4759 - Comercio al por menor de otros art?culos dom?sticos en establecimientos especializados</option>
                    <option {if $economicActivity==='4761'}selected{/if} value="4761">4761 - Comercio al por menor de libros, peri?dicos, materiales y art?culos de papeler?a y escritorio, en establecimientos especializados</option>
                    <option {if $economicActivity==='4762'}selected{/if} value="4762">4762 - Comercio al por menor de art?culos deportivos, en establecimientos especializados</option>
                    <option {if $economicActivity==='4769'}selected{/if} value="4769">4769 - Comercio al por menor de otros art?culos culturales y de entretenimiento n.c.p. en establecimientos especializados</option>
                    <option {if $economicActivity==='4771'}selected{/if} value="4771">4771 - Comercio al por menor de prendas de vestir y sus accesorios (incluye art?culos de piel) en establecimientos especializados</option>
                    <option {if $economicActivity==='4772'}selected{/if} value="4772">4772 - Comercio al por menor de todo tipo de calzado y art?culos de cuero y suced?neos del cuero en establecimientos especializados</option>
                    <option {if $economicActivity==='4773'}selected{/if} value="4773">4773 - Comercio al por menor de productos farmac?uticos y medicinales, cosm?ticos y art?culos de tocador en establecimientos especializados</option>
                    <option {if $economicActivity==='4774'}selected{/if} value="4774">4774 - Comercio al por menor de otros productos nuevos en establecimientos especializados</option>
                    <option {if $economicActivity==='4775'}selected{/if} value="4775">4775 - Comercio al por menor de art?culos de segunda mano</option>
                    <option {if $economicActivity==='4781'}selected{/if} value="4781">4781 - Comercio al por menor de alimentos, bebidas y tabaco, en puestos de venta m?viles</option>
                    <option {if $economicActivity==='4782'}selected{/if} value="4782">4782 - Comercio al por menor de productos textiles, prendas de vestir y calzado, en puestos de venta m?viles</option>
                    <option {if $economicActivity==='4789'}selected{/if} value="4789">4789 - Comercio al por menor de otros productos en puestos de venta m?viles</option>
                    <option {if $economicActivity==='4791'}selected{/if} value="4791">4791 - Comercio al por menor realizado a trav?s de internet</option>
                    <option {if $economicActivity==='4792'}selected{/if} value="4792">4792 - Comercio al por menor realizado a trav?s de casas de venta o por correo</option>
                    <option {if $economicActivity==='4799'}selected{/if} value="4799">4799 - Otros tipos de comercio al por menor no realizado en establecimientos, puestos de venta o mercados</option>
                    <option {if $economicActivity==='4911'}selected{/if} value="4911">4911 - Transporte f?rreo de pasajeros</option>
                    <option {if $economicActivity==='4912'}selected{/if} value="4912">4912 - Transporte f?rreo de carga</option>
                    <option {if $economicActivity==='4921'}selected{/if} value="4921">4921 - Transporte de pasajeros</option>
                    <option {if $economicActivity==='4922'}selected{/if} value="4922">4922 - Transporte mixto</option>
                    <option {if $economicActivity==='4923'}selected{/if} value="4923">4923 - Transporte de carga por carretera</option>
                    <option {if $economicActivity==='4930'}selected{/if} value="4930">4930 - Transporte por tuber?as</option>
                    <option {if $economicActivity==='5011'}selected{/if} value="5011">5011 - Transporte de pasajeros mar?timo y de cabotaje</option>
                    <option {if $economicActivity==='5012'}selected{/if} value="5012">5012 - Transporte de carga mar?timo y de cabotaje</option>
                    <option {if $economicActivity==='5021'}selected{/if} value="5021">5021 - Transporte fluvial de pasajeros</option>
                    <option {if $economicActivity==='5022'}selected{/if} value="5022">5022 - Transporte fluvial de carga</option>
                    <option {if $economicActivity==='5111'}selected{/if} value="5111">5111 - Transporte a?reo nacional de pasajeros</option>
                    <option {if $economicActivity==='5112'}selected{/if} value="5112">5112 - Transporte a?reo internacional de pasajeros</option>
                    <option {if $economicActivity==='5121'}selected{/if} value="5121">5121 - Transporte a?reo nacional de carga</option>
                    <option {if $economicActivity==='5122'}selected{/if} value="5122">5122 - Transporte a?reo internacional de carga</option>
                    <option {if $economicActivity==='5210'}selected{/if} value="5210">5210 - Almacenamiento y dep?sito</option>
                    <option {if $economicActivity==='5221'}selected{/if} value="5221">5221 - Actividades de estaciones, v?as y servicios complementarios para el transporte terrestre</option>
                    <option {if $economicActivity==='5222'}selected{/if} value="5222">5222 - Actividades de puertos y servicios complementarios para el transporte acu?tico</option>
                    <option {if $economicActivity==='5223'}selected{/if} value="5223">5223 - Actividades de aeropuertos, servicios de navegaci?n a?rea y dem?s actividades conexas al transporte a?reo</option>
                    <option {if $economicActivity==='5224'}selected{/if} value="5224">5224 - Manipulaci?n de carga</option>
                    <option {if $economicActivity==='5229'}selected{/if} value="5229">5229 - Otras actividades complementarias al transporte</option>
                    <option {if $economicActivity==='5310'}selected{/if} value="5310">5310 - Actividades postales nacionales</option>
                    <option {if $economicActivity==='5320'}selected{/if} value="5320">5320 - Actividades de mensajer?a</option>
                    <option {if $economicActivity==='5511'}selected{/if} value="5511">5511 - Alojamiento en hoteles</option>
                    <option {if $economicActivity==='5512'}selected{/if} value="5512">5512 - Alojamiento en apartahoteles</option>
                    <option {if $economicActivity==='5513'}selected{/if} value="5513">5513 - Alojamiento en centros vacacionales</option>
                    <option {if $economicActivity==='5514'}selected{/if} value="5514">5514 - Alojamiento rural</option>
                    <option {if $economicActivity==='5519'}selected{/if} value="5519">5519 - Otros tipos de alojamientos para visitantes</option>
                    <option {if $economicActivity==='5520'}selected{/if} value="5520">5520 - Actividades de zonas de camping y parques para veh?culos recreacionales</option>
                    <option {if $economicActivity==='5530'}selected{/if} value="5530">5530 - Servicio por horas</option>
                    <option {if $economicActivity==='5590'}selected{/if} value="5590">5590 - Otros tipos de alojamiento n.c.p.</option>
                    <option {if $economicActivity==='5611'}selected{/if} value="5611">5611 - Expendio a la mesa de comidas preparadas</option>
                    <option {if $economicActivity==='5612'}selected{/if} value="5612">5612 - Expendio por autoservicio de comidas preparadas</option>
                    <option {if $economicActivity==='5613'}selected{/if} value="5613">5613 - Expendio de comidas preparadas en cafeter?as</option>
                    <option {if $economicActivity==='5619'}selected{/if} value="5619">5619 - Otros tipos de expendio de comidas preparadas n.c.p.</option>
                    <option {if $economicActivity==='5621'}selected{/if} value="5621">5621 - Catering para eventos</option>
                    <option {if $economicActivity==='5629'}selected{/if} value="5629">5629 - Actividades de otros servicios de comidas</option>
                    <option {if $economicActivity==='5630'}selected{/if} value="5630">5630 - Expendio de bebidas alcoh?licas para el consumo dentro del establecimiento</option>
                    <option {if $economicActivity==='5811'}selected{/if} value="5811">5811 - Edici?n de libros</option>
                    <option {if $economicActivity==='5812'}selected{/if} value="5812">5812 - Edici?n de directorios y listas de correo</option>
                    <option {if $economicActivity==='5813'}selected{/if} value="5813">5813 - Edici?n de peri?dicos, revistas y otras publicaciones peri?dicas</option>
                    <option {if $economicActivity==='5819'}selected{/if} value="5819">5819 - Otros trabajos de edici?n</option>
                    <option {if $economicActivity==='5820'}selected{/if} value="5820">5820 - Edici?n de programas de inform?tica (software)</option>
                    <option {if $economicActivity==='5911'}selected{/if} value="5911">5911 - Actividades de producci?n de pel?culas cinematogr?ficas, videos, programas, anuncios y comerciales de televisi?n</option>
                    <option {if $economicActivity==='5912'}selected{/if} value="5912">5912 - Actividades de posproducci?n de pel?culas cinematogr?ficas, videos, programas, anuncios y comerciales de televisi?n</option>
                    <option {if $economicActivity==='5913'}selected{/if} value="5913">5913 - Actividades de distribuci?n de pel?culas cinematogr?ficas, videos, programas, anuncios y comerciales de televisi?n</option>
                    <option {if $economicActivity==='5914'}selected{/if} value="5914">5914 - Actividades de exhibici?n de pel?culas cinematogr?ficas y videos</option>
                    <option {if $economicActivity==='5920'}selected{/if} value="5920">5920 - Actividades de grabaci?n de sonido y edici?n de m?sica</option>
                    <option {if $economicActivity==='6010'}selected{/if} value="6010">6010 - Actividades de programaci?n y transmisi?n en el servicio de radiodifusi?n sonora</option>
                    <option {if $economicActivity==='6020'}selected{/if} value="6020">6020 - Actividades de programaci?n y transmisi?n de televisi?n</option>
                    <option {if $economicActivity==='6110'}selected{/if} value="6110">6110 - Actividades de telecomunicaciones al?mbricas</option>
                    <option {if $economicActivity==='6120'}selected{/if} value="6120">6120 - Actividades de telecomunicaciones inal?mbricas</option>
                    <option {if $economicActivity==='6130'}selected{/if} value="6130">6130 - Actividades de telecomunicaci?n satelital</option>
                    <option {if $economicActivity==='6190'}selected{/if} value="6190">6190 - Otras actividades de telecomunicaciones</option>
                    <option {if $economicActivity==='6201'}selected{/if} value="6201">6201 - Actividades de desarrollo de sistemas inform?ticos (planificaci?n, an?lisis, dise?o, programaci?n, pruebas)</option>
                    <option {if $economicActivity==='6202'}selected{/if} value="6202">6202 - Actividades de consultor?a inform?tica y actividades de administraci?n de instalaciones inform?ticas</option>
                    <option {if $economicActivity==='6209'}selected{/if} value="6209">6209 - Otras actividades de tecnolog?as de informaci?n y actividades de servicios inform?ticos</option>
                    <option {if $economicActivity==='6311'}selected{/if} value="6311">6311 - Procesamiento de datos, alojamiento (hosting) y actividades relacionadas</option>
                    <option {if $economicActivity==='6312'}selected{/if} value="6312">6312 - Portales web</option>
                    <option {if $economicActivity==='6391'}selected{/if} value="6391">6391 - Actividades de agencias de noticias</option>
                    <option {if $economicActivity==='6399'}selected{/if} value="6399">6399 - Otras actividades de servicio de informaci?n n.c.p.</option>
                    <option {if $economicActivity==='6411'}selected{/if} value="6411">6411 - Banco Central</option>
                    <option {if $economicActivity==='6412'}selected{/if} value="6412">6412 - Bancos comerciales</option>
                    <option {if $economicActivity==='6421'}selected{/if} value="6421">6421 - Actividades de las corporaciones financieras</option>
                    <option {if $economicActivity==='6422'}selected{/if} value="6422">6422 - Actividades de las compa??as de financiamiento</option>
                    <option {if $economicActivity==='6423'}selected{/if} value="6423">6423 - Banca de segundo piso</option>
                    <option {if $economicActivity==='6424'}selected{/if} value="6424">6424 - Actividades de las cooperativas financieras</option>
                    <option {if $economicActivity==='6431'}selected{/if} value="6431">6431 - Fideicomisos, fondos y entidades financieras similares</option>
                    <option {if $economicActivity==='6432'}selected{/if} value="6432">6432 - Fondos de cesant?as</option>
                    <option {if $economicActivity==='6491'}selected{/if} value="6491">6491 - Leasing financiero (arrendamiento financiero)</option>
                    <option {if $economicActivity==='6492'}selected{/if} value="6492">6492 - Actividades financieras de fondos de empleados y otras formas asociativas del sector solidario</option>
                    <option {if $economicActivity==='6493'}selected{/if} value="6493">6493 - Actividades de compra de cartera o factoring</option>
                    <option {if $economicActivity==='6494'}selected{/if} value="6494">6494 - Otras actividades de distribuci?n de fondos</option>
                    <option {if $economicActivity==='6495'}selected{/if} value="6495">6495 - Instituciones especiales oficiales</option>
                    <option {if $economicActivity==='6499'}selected{/if} value="6499">6499 - Otras actividades de servicio financiero, excepto las de seguros y pensiones n.c.p.</option>
                    <option {if $economicActivity==='6511'}selected{/if} value="6511">6511 - Seguros generales</option>
                    <option {if $economicActivity==='6512'}selected{/if} value="6512">6512 - Seguros de vida</option>
                    <option {if $economicActivity==='6513'}selected{/if} value="6513">6513 - Reaseguros</option>
                    <option {if $economicActivity==='6514'}selected{/if} value="6514">6514 - Capitalizaci?n</option>
                    <option {if $economicActivity==='6521'}selected{/if} value="6521">6521 - Servicios de seguros sociales de salud</option>
                    <option {if $economicActivity==='6522'}selected{/if} value="6522">6522 - Servicios de seguros sociales de riesgos profesionales</option>
                    <option {if $economicActivity==='6531'}selected{/if} value="6531">6531 - R?gimen de prima media con prestaci?n definida (RPM)</option>
                    <option {if $economicActivity==='6532'}selected{/if} value="6532">6532 - R?gimen de ahorro individual (RAI)</option>
                    <option {if $economicActivity==='6611'}selected{/if} value="6611">6611 - Administraci?n de mercados financieros</option>
                    <option {if $economicActivity==='6612'}selected{/if} value="6612">6612 - Corretaje de valores y de contratos de productos b?sicos</option>
                    <option {if $economicActivity==='6613'}selected{/if} value="6613">6613 - Otras actividades relacionadas con el mercado de valores</option>
                    <option {if $economicActivity==='6614'}selected{/if} value="6614">6614 - Actividades de las casas de cambio</option>
                    <option {if $economicActivity==='6615'}selected{/if} value="6615">6615 - Actividades de los profesionales de compra y venta de divisas</option>
                    <option {if $economicActivity==='6619'}selected{/if} value="6619">6619 - Otras actividades auxiliares de las actividades de servicios financieros n.c.p.</option>
                    <option {if $economicActivity==='6621'}selected{/if} value="6621">6621 - Actividades de agentes y corredores de seguros</option>
                    <option {if $economicActivity==='6629'}selected{/if} value="6629">6629 - Evaluaci?n de riesgos y da?os, y otras actividades de servicios auxiliares</option>
                    <option {if $economicActivity==='6630'}selected{/if} value="6630">6630 - Actividades de administraci?n de fondos</option>
                    <option {if $economicActivity==='6810'}selected{/if} value="6810">6810 - Actividades inmobiliarias realizadas con bienes propios o arrendados</option>
                    <option {if $economicActivity==='6820'}selected{/if} value="6820">6820 - Actividades inmobiliarias realizadas a cambio de una retribuci?n o por contrata</option>
                    <option {if $economicActivity==='6910'}selected{/if} value="6910">6910 - Actividades jur?dicas</option>
                    <option {if $economicActivity==='6920'}selected{/if} value="6920">6920 - Actividades de contabilidad, tenedur?a de libros, auditor?a financiera y asesor?a tributaria</option>
                    <option {if $economicActivity==='7010'}selected{/if} value="7010">7010 - Actividades de administraci?n empresarial</option>
                    <option {if $economicActivity==='7020'}selected{/if} value="7020">7020 - Actividades de consultar?a de gesti?n</option>
                    <option {if $economicActivity==='7110'}selected{/if} value="7110">7110 - Actividades de arquitectura e ingenier?a y otras actividades conexas de consultor?a t?cnica</option>
                    <option {if $economicActivity==='7120'}selected{/if} value="7120">7120 - Ensayos y an?lisis t?cnicos</option>
                    <option {if $economicActivity==='7210'}selected{/if} value="7210">7210 - Investigaciones y desarrollo experimental en el campo de las ciencias naturales y la ingenier?a</option>
                    <option {if $economicActivity==='7220'}selected{/if} value="7220">7220 - Investigaciones y desarrollo experimental en el campo de las ciencias sociales y las humanidades</option>
                    <option {if $economicActivity==='7310'}selected{/if} value="7310">7310 - Publicidad</option>
                    <option {if $economicActivity==='7320'}selected{/if} value="7320">7320 - Estudios de mercado y realizaci?n de encuestas de opini?n p?blica</option>
                    <option {if $economicActivity==='7410'}selected{/if} value="7410">7410 - Actividades especializadas de dise?o</option>
                    <option {if $economicActivity==='7420'}selected{/if} value="7420">7420 - Actividades de fotograf?a</option>
                    <option {if $economicActivity==='7490'}selected{/if} value="7490">7490 - Otras actividades profesionales, cient?ficas y t?cnicas n.c.p.</option>
                    <option {if $economicActivity==='7499'}selected{/if} value="7499">7499 - Other business activities</option>
                    <option {if $economicActivity==='7500'}selected{/if} value="7500">7500 - Actividades veterinarias</option>
                    <option {if $economicActivity==='7515'}selected{/if} value="7515">7515 - Auxiliary service activities</option>
                    <option {if $economicActivity==='7530'}selected{/if} value="7530">7530 - Activities of social security</option>
                    <option {if $economicActivity==='7710'}selected{/if} value="7710">7710 - Alquiler y arrendamiento de veh?culos automotores</option>
                    <option {if $economicActivity==='7721'}selected{/if} value="7721">7721 - Alquiler y arrendamiento de equipo recreativo y deportivo</option>
                    <option {if $economicActivity==='7722'}selected{/if} value="7722">7722 - Alquiler de videos y discos</option>
                    <option {if $economicActivity==='7729'}selected{/if} value="7729">7729 - Alquiler y arrendamiento de otros efectos personales y enseres dom?sticos n.c.p.</option>
                    <option {if $economicActivity==='7730'}selected{/if} value="7730">7730 - Alquiler y arrendamiento de otros tipos de maquinaria, equipo y bienes tangibles n.c.p.</option>
                    <option {if $economicActivity==='7740'}selected{/if} value="7740">7740 - Arrendamiento de propiedad intelectual y productos similares, excepto obras protegidas por derechos de autor</option>
                    <option {if $economicActivity==='7810'}selected{/if} value="7810">7810 - Actividades de agencias de empleo</option>
                    <option {if $economicActivity==='7820'}selected{/if} value="7820">7820 - Actividades de agencias de empleo temporal</option>
                    <option {if $economicActivity==='7830'}selected{/if} value="7830">7830 - Otras actividades de suministro de recurso humano</option>
                    <option {if $economicActivity==='7911'}selected{/if} value="7911">7911 - Actividades de las agencias de viaje</option>
                    <option {if $economicActivity==='7912'}selected{/if} value="7912">7912 - Actividades de operadores tur?sticos</option>
                    <option {if $economicActivity==='7990'}selected{/if} value="7990">7990 - Otros servicios de reserva y actividades relacionadas</option>
                    <option {if $economicActivity==='8010'}selected{/if} value="8010">8010 - Actividades de seguridad privada</option>
                    <option {if $economicActivity==='8020'}selected{/if} value="8020">8020 - Actividades de servicios de sistemas de seguridad</option>
                    <option {if $economicActivity==='8022'}selected{/if} value="8022">8022 - Average education academic</option>
                    <option {if $economicActivity==='8030'}selected{/if} value="8030">8030 - Actividades de detectives e investigadores privados</option>
                    <option {if $economicActivity==='8110'}selected{/if} value="8110">8110 - Actividades combinadas de apoyo a instalaciones</option>
                    <option {if $economicActivity==='8121'}selected{/if} value="8121">8121 - Limpieza general interior de edificios</option>
                    <option {if $economicActivity==='8129'}selected{/if} value="8129">8129 - Otras actividades de limpieza de edificios e instalaciones industriales</option>
                    <option {if $economicActivity==='8130'}selected{/if} value="8130">8130 - Actividades de paisajismo y servicios de mantenimiento conexos</option>
                    <option {if $economicActivity==='8211'}selected{/if} value="8211">8211 - Actividades combinadas de servicios administrativos de oficina</option>
                    <option {if $economicActivity==='8219'}selected{/if} value="8219">8219 - Fotocopiado, preparaci?n de documentos y otras actividades especializadas de apoyo a oficina</option>
                    <option {if $economicActivity==='8220'}selected{/if} value="8220">8220 - Actividades de centros de llamadas (Call center)</option>
                    <option {if $economicActivity==='8230'}selected{/if} value="8230">8230 - Organizaci?n de convenciones y eventos comerciales</option>
                    <option {if $economicActivity==='8291'}selected{/if} value="8291">8291 - Actividades de agencias de cobranza y oficinas de calificaci?n crediticia</option>
                    <option {if $economicActivity==='8292'}selected{/if} value="8292">8292 - Actividades de envase y empaque</option>
                    <option {if $economicActivity==='8299'}selected{/if} value="8299">8299 - Otras actividades de servicio de apoyo a las empresas n.c.p.</option>
                    <option {if $economicActivity==='8411'}selected{/if} value="8411">8411 - Actividades legislativas de la administraci?n p?blica</option>
                    <option {if $economicActivity==='8412'}selected{/if} value="8412">8412 - Actividades ejecutivas de la administraci?n p?blica</option>
                    <option {if $economicActivity==='8413'}selected{/if} value="8413">8413 - Regulaci?n de las actividades de organismos que prestan servicios de salud, educativos, culturales y otros servicios sociales, excepto servicios de seguridad social</option>
                    <option {if $economicActivity==='8414'}selected{/if} value="8414">8414 - Actividades reguladoras y facilitadoras de la actividad econ?mica</option>
                    <option {if $economicActivity==='8415'}selected{/if} value="8415">8415 - Actividades de los otros ?rganos de control</option>
                    <option {if $economicActivity==='8421'}selected{/if} value="8421">8421 - Relaciones exteriores</option>
                    <option {if $economicActivity==='8422'}selected{/if} value="8422">8422 - Actividades de defensa</option>
                    <option {if $economicActivity==='8423'}selected{/if} value="8423">8423 - Orden p?blico y actividades de seguridad</option>
                    <option {if $economicActivity==='8424'}selected{/if} value="8424">8424 - Administraci?n de justicia</option>
                    <option {if $economicActivity==='8430'}selected{/if} value="8430">8430 - Actividades de planes de seguridad social de afiliaci?n obligatoria</option>
                    <option {if $economicActivity==='8511'}selected{/if} value="8511">8511 - Educaci?n de la primera infancia</option>
                    <option {if $economicActivity==='8512'}selected{/if} value="8512">8512 - Educaci?n preescolar</option>
                    <option {if $economicActivity==='8513'}selected{/if} value="8513">8513 - Educaci?n b?sica primaria</option>
                    <option {if $economicActivity==='8521'}selected{/if} value="8521">8521 - Educaci?n b?sica secundaria</option>
                    <option {if $economicActivity==='8522'}selected{/if} value="8522">8522 - Educaci?n media acad?mica</option>
                    <option {if $economicActivity==='8523'}selected{/if} value="8523">8523 - Educaci?n media t?cnica y de formaci?n laboral</option>
                    <option {if $economicActivity==='8530'}selected{/if} value="8530">8530 - Establecimientos que combinan diferentes niveles de educaci?n</option>
                    <option {if $economicActivity==='8541'}selected{/if} value="8541">8541 - Educaci?n t?cnica profesional</option>
                    <option {if $economicActivity==='8542'}selected{/if} value="8542">8542 - Educaci?n tecnol?gica</option>
                    <option {if $economicActivity==='8543'}selected{/if} value="8543">8543 - Educaci?n de instituciones universitarias o de escuelas tecnol?gicas</option>
                    <option {if $economicActivity==='8544'}selected{/if} value="8544">8544 - Educaci?n de universidades</option>
                    <option {if $economicActivity==='8551'}selected{/if} value="8551">8551 - Formaci?n acad?mica no formal</option>
                    <option {if $economicActivity==='8552'}selected{/if} value="8552">8552 - Ense?anza deportiva y recreativa</option>
                    <option {if $economicActivity==='8553'}selected{/if} value="8553">8553 - Ense?anza cultural</option>
                    <option {if $economicActivity==='8559'}selected{/if} value="8559">8559 - Otros tipos de educaci?n n.c.p.</option>
                    <option {if $economicActivity==='8560'}selected{/if} value="8560">8560 - Actividades de apoyo a la educaci?n</option>
                    <option {if $economicActivity==='8610'}selected{/if} value="8610">8610 - Actividades de hospitales y cl?nicas, con internaci?n</option>
                    <option {if $economicActivity==='8621'}selected{/if} value="8621">8621 - Actividades de la pr?ctica m?dica, sin internaci?n</option>
                    <option {if $economicActivity==='8622'}selected{/if} value="8622">8622 - Actividades de la pr?ctica odontol?gica</option>
                    <option {if $economicActivity==='8691'}selected{/if} value="8691">8691 - Actividades de apoyo diagn?stico</option>
                    <option {if $economicActivity==='8692'}selected{/if} value="8692">8692 - Actividades de apoyo terap?utico</option>
                    <option {if $economicActivity==='8699'}selected{/if} value="8699">8699 - Otras actividades de atenci?n de la salud humana</option>
                    <option {if $economicActivity==='8710'}selected{/if} value="8710">8710 - Actividades de atenci?n residencial medicalizada de tipo general</option>
                    <option {if $economicActivity==='8720'}selected{/if} value="8720">8720 - Actividades de atenci?n residencial, para el cuidado de pacientes con retardo mental, enfermedad mental y consumo de sustancias psicoactivas</option>
                    <option {if $economicActivity==='8730'}selected{/if} value="8730">8730 - Actividades de atenci?n en instituciones para el cuidado de personas mayores y/o discapacitadas</option>
                    <option {if $economicActivity==='8790'}selected{/if} value="8790">8790 - Otras actividades de atenci?n en instituciones con alojamiento</option>
                    <option {if $economicActivity==='8810'}selected{/if} value="8810">8810 - Actividades de asistencia social sin alojamiento para personas mayores y discapacitadas</option>
                    <option {if $economicActivity==='8890'}selected{/if} value="8890">8890 - Otras actividades de asistencia social sin alojamiento</option>
                    <option {if $economicActivity==='9001'}selected{/if} value="9001">9001 - Creaci?n literaria</option>
                    <option {if $economicActivity==='9002'}selected{/if} value="9002">9002 - Creaci?n musical</option>
                    <option {if $economicActivity==='9003'}selected{/if} value="9003">9003 - Creaci?n teatral</option>
                    <option {if $economicActivity==='9004'}selected{/if} value="9004">9004 - Creaci?n audiovisual</option>
                    <option {if $economicActivity==='9005'}selected{/if} value="9005">9005 - Artes pl?sticas y visuales</option>
                    <option {if $economicActivity==='9006'}selected{/if} value="9006">9006 - Actividades teatrales</option>
                    <option {if $economicActivity==='9007'}selected{/if} value="9007">9007 - Actividades de espect?culos musicales en vivo</option>
                    <option {if $economicActivity==='9008'}selected{/if} value="9008">9008 - Otras actividades de espect?culos en vivo</option>
                    <option {if $economicActivity==='9101'}selected{/if} value="9101">9101 - Actividades de bibliotecas y archivos</option>
                    <option {if $economicActivity==='9102'}selected{/if} value="9102">9102 - Actividades y funcionamiento de museos, conservaci?n de edificios y sitios hist?ricos</option>
                    <option {if $economicActivity==='9103'}selected{/if} value="9103">9103 - Actividades de jardines bot?nicos, zool?gicos y reservas naturales</option>
                    <option {if $economicActivity==='9200'}selected{/if} value="9200">9200 - Actividades de juegos de azar y apuestas</option>
                    <option {if $economicActivity==='9311'}selected{/if} value="9311">9311 - Gesti?n de instalaciones deportivas</option>
                    <option {if $economicActivity==='9312'}selected{/if} value="9312">9312 - Actividades de clubes deportivos</option>
                    <option {if $economicActivity==='9319'}selected{/if} value="9319">9319 - Otras actividades deportivas</option>
                    <option {if $economicActivity==='9321'}selected{/if} value="9321">9321 - Actividades de parques de atracciones y parques tem?ticos</option>
                    <option {if $economicActivity==='9329'}selected{/if} value="9329">9329 - Otras actividades recreativas y de esparcimiento n.c.p.</option>
                    <option {if $economicActivity==='9411'}selected{/if} value="9411">9411 - Actividades de asociaciones empresariales y de empleadores</option>
                    <option {if $economicActivity==='9412'}selected{/if} value="9412">9412 - Actividades de asociaciones profesionales</option>
                    <option {if $economicActivity==='9420'}selected{/if} value="9420">9420 - Actividades de sindicatos de empleados</option>
                    <option {if $economicActivity==='9491'}selected{/if} value="9491">9491 - Actividades de asociaciones religiosas</option>
                    <option {if $economicActivity==='9492'}selected{/if} value="9492">9492 - Actividades de asociaciones pol?ticas</option>
                    <option {if $economicActivity==='9499'}selected{/if} value="9499">9499 - Actividades de otras asociaciones n.c.p.</option>
                    <option {if $economicActivity==='9511'}selected{/if} value="9511">9511 - Mantenimiento y reparaci?n de computadores y de equipo perif?rico</option>
                    <option {if $economicActivity==='9512'}selected{/if} value="9512">9512 - Mantenimiento y reparaci?n de equipos de comunicaci?n</option>
                    <option {if $economicActivity==='9521'}selected{/if} value="9521">9521 - Mantenimiento y reparaci?n de aparatos electr?nicos de consumo</option>
                    <option {if $economicActivity==='9522'}selected{/if} value="9522">9522 - Mantenimiento y reparaci?n de aparatos y equipos dom?sticos y de jardiner?a</option>
                    <option {if $economicActivity==='9523'}selected{/if} value="9523">9523 - Reparaci?n de calzado y art?culos de cuero</option>
                    <option {if $economicActivity==='9524'}selected{/if} value="9524">9524 - Reparaci?n de muebles y accesorios para el hogar</option>
                    <option {if $economicActivity==='9529'}selected{/if} value="9529">9529 - Mantenimiento y reparaci?n de otros efectos personales y enseres dom?sticos</option>
                    <option {if $economicActivity==='9601'}selected{/if} value="9601">9601 - Lavado y limpieza, incluso la limpieza en seco, de productos textiles y de piel</option>
                    <option {if $economicActivity==='9602'}selected{/if} value="9602">9602 - Peluquer?a y otros tratamientos de belleza</option>
                    <option {if $economicActivity==='9603'}selected{/if} value="9603">9603 - Pompas f?nebres y actividades relacionadas</option>
                    <option {if $economicActivity==='9609'}selected{/if} value="9609">9609 - Otras actividades de servicios personales n.c.p.</option>
                    <option {if $economicActivity==='9700'}selected{/if} value="9700">9700 - Actividades de los hogares individuales como empleadores de personal dom?stico</option>
                    <option {if $economicActivity==='9810'}selected{/if} value="9810">9810 - Actividades no diferenciadas de los hogares individuales como productores de bienes para uso pr?prio</option>
                    <option {if $economicActivity==='9820'}selected{/if} value="9820">9820 - Actividades no diferenciadas de los hogares individuales como productores de servicios para uso pr?prio</option>
                    <option {if $economicActivity==='9900'}selected{/if} value="9900">9900 - Actividades de organizaciones y entidades extraterritoriales</option>
                </select>
            </div>
            <small id="errorEconomicActivity" class="form-text text-danger">&nbsp;{$errorEconomicActivity}</small>
        </div>
    </div>
    {/if}


    {include file='../../MAPS/ORAGeoCodes/AddressStyleAwareWidget.tpl'}

    <div class="form-row align-items-center">
        <div class="col-sm-9 my-1">
            <label class="sr-only" for="inlineFormInputAddressLine1">Address Line 1</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Address Line 1</div>
                </div>
                <input type="text" class="form-control" id="addressLine1" name="addressLine1" value="{$addressLine1}" placeholder="Address Line 1">
            </div>
            <small id="errorAddressLine1" class="form-text text-danger">&nbsp;{$errorAddressLine1}</small>
        </div>
        <div class="col-sm-3 my-1">
            <label class="sr-only" for="inlineFormInputAddressLine1">Postal Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Postal Code</div>
                </div>
                <input type="text" class="form-control" id="postalCode" name="postalCode" value="{$postalCode}">
            </div>
            <small id="errorPostalCode" class="form-text text-danger">&nbsp;{$errorPostalCode}</small>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <label class="sr-only" for="inlineFormInputAddressLine1">Address Line 2</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Address Line 2</div>
                </div>
                <input type="text" class="form-control" id="addressLine2" name="addressLine2" value="{$addressLine2}" placeholder="(second address line, if needed)">
            </div>
            <small id="errorAddressLine2" class="form-text text-danger">&nbsp;{$errorAddressLine2}</small>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <label class="sr-only" for="inlineFormInputAddressLinesPhonetic">Address Lines Phonetic</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Address Lines Phonetic</div>
                </div>
                <input type="text" class="form-control" id="addressLinesPhonetic" name="addressLinesPhonetic" value="{$addressLinesPhonetic}" placeholder="(mandatory phonetic representation of address)">
            </div>
            <small id="errorAddressLinesPhonetic" class="form-text text-danger">&nbsp;{$errorAddressLinesPhonetic}</small>
        </div>
    </div>

    
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Time Zone</div>
                </div>
                <select class="form-control" id="timeZone" name="timeZone" aria-describedby="" placeholder="">
                    <option {if $timeZone=='Africa/Abidjan'}selected{/if} value='Africa/Abidjan'>Africa/Abidjan</option>
                    <option {if $timeZone=='Africa/Accra'}selected{/if} value='Africa/Accra'>Africa/Accra</option>
                    <option {if $timeZone=='Africa/Addis_Ababa'}selected{/if} value='Africa/Addis_Ababa'>Africa/Addis_Ababa</option>
                    <option {if $timeZone=='Africa/Algiers'}selected{/if} value='Africa/Algiers'>Africa/Algiers</option>
                    <option {if $timeZone=='Africa/Asmera'}selected{/if} value='Africa/Asmera'>Africa/Asmera</option>
                    <option {if $timeZone=='Africa/Bamako'}selected{/if} value='Africa/Bamako'>Africa/Bamako</option>
                    <option {if $timeZone=='Africa/Bangui'}selected{/if} value='Africa/Bangui'>Africa/Bangui</option>
                    <option {if $timeZone=='Africa/Banjul'}selected{/if} value='Africa/Banjul'>Africa/Banjul</option>
                    <option {if $timeZone=='Africa/Bissau'}selected{/if} value='Africa/Bissau'>Africa/Bissau</option>
                    <option {if $timeZone=='Africa/Blantyre'}selected{/if} value='Africa/Blantyre'>Africa/Blantyre</option>
                    <option {if $timeZone=='Africa/Brazzaville'}selected{/if} value='Africa/Brazzaville'>Africa/Brazzaville</option>
                    <option {if $timeZone=='Africa/Bujumbura'}selected{/if} value='Africa/Bujumbura'>Africa/Bujumbura</option>
                    <option {if $timeZone=='Africa/Cairo'}selected{/if} value='Africa/Cairo'>Africa/Cairo</option>
                    <option {if $timeZone=='Africa/Casablanca'}selected{/if} value='Africa/Casablanca'>Africa/Casablanca</option>
                    <option {if $timeZone=='Africa/Ceuta'}selected{/if} value='Africa/Ceuta'>Africa/Ceuta</option>
                    <option {if $timeZone=='Africa/Conakry'}selected{/if} value='Africa/Conakry'>Africa/Conakry</option>
                    <option {if $timeZone=='Africa/Dakar'}selected{/if} value='Africa/Dakar'>Africa/Dakar</option>
                    <option {if $timeZone=='Africa/Dar_es_Salaam'}selected{/if} value='Africa/Dar_es_Salaam'>Africa/Dar_es_Salaam</option>
                    <option {if $timeZone=='Africa/Djibouti'}selected{/if} value='Africa/Djibouti'>Africa/Djibouti</option>
                    <option {if $timeZone=='Africa/Douala'}selected{/if} value='Africa/Douala'>Africa/Douala</option>
                    <option {if $timeZone=='Africa/El_Aaiun'}selected{/if} value='Africa/El_Aaiun'>Africa/El_Aaiun</option>
                    <option {if $timeZone=='Africa/Freetown'}selected{/if} value='Africa/Freetown'>Africa/Freetown</option>
                    <option {if $timeZone=='Africa/Gaborone'}selected{/if} value='Africa/Gaborone'>Africa/Gaborone</option>
                    <option {if $timeZone=='Africa/Harare'}selected{/if} value='Africa/Harare'>Africa/Harare</option>
                    <option {if $timeZone=='Africa/Johannesburg'}selected{/if} value='Africa/Johannesburg'>Africa/Johannesburg</option>
                    <option {if $timeZone=='Africa/Kampala'}selected{/if} value='Africa/Kampala'>Africa/Kampala</option>
                    <option {if $timeZone=='Africa/Khartoum'}selected{/if} value='Africa/Khartoum'>Africa/Khartoum</option>
                    <option {if $timeZone=='Africa/Kigali'}selected{/if} value='Africa/Kigali'>Africa/Kigali</option>
                    <option {if $timeZone=='Africa/Kinshasa'}selected{/if} value='Africa/Kinshasa'>Africa/Kinshasa</option>
                    <option {if $timeZone=='Africa/Lagos'}selected{/if} value='Africa/Lagos'>Africa/Lagos</option>
                    <option {if $timeZone=='Africa/Libreville'}selected{/if} value='Africa/Libreville'>Africa/Libreville</option>
                    <option {if $timeZone=='Africa/Lome'}selected{/if} value='Africa/Lome'>Africa/Lome</option>
                    <option {if $timeZone=='Africa/Luanda'}selected{/if} value='Africa/Luanda'>Africa/Luanda</option>
                    <option {if $timeZone=='Africa/Lubumbashi'}selected{/if} value='Africa/Lubumbashi'>Africa/Lubumbashi</option>
                    <option {if $timeZone=='Africa/Lusaka'}selected{/if} value='Africa/Lusaka'>Africa/Lusaka</option>
                    <option {if $timeZone=='Africa/Malabo'}selected{/if} value='Africa/Malabo'>Africa/Malabo</option>
                    <option {if $timeZone=='Africa/Maputo'}selected{/if} value='Africa/Maputo'>Africa/Maputo</option>
                    <option {if $timeZone=='Africa/Maseru'}selected{/if} value='Africa/Maseru'>Africa/Maseru</option>
                    <option {if $timeZone=='Africa/Mbabane'}selected{/if} value='Africa/Mbabane'>Africa/Mbabane</option>
                    <option {if $timeZone=='Africa/Mogadishu'}selected{/if} value='Africa/Mogadishu'>Africa/Mogadishu</option>
                    <option {if $timeZone=='Africa/Monrovia'}selected{/if} value='Africa/Monrovia'>Africa/Monrovia</option>
                    <option {if $timeZone=='Africa/Nairobi'}selected{/if} value='Africa/Nairobi'>Africa/Nairobi</option>
                    <option {if $timeZone=='Africa/Ndjamena'}selected{/if} value='Africa/Ndjamena'>Africa/Ndjamena</option>
                    <option {if $timeZone=='Africa/Niamey'}selected{/if} value='Africa/Niamey'>Africa/Niamey</option>
                    <option {if $timeZone=='Africa/Nouakchott'}selected{/if} value='Africa/Nouakchott'>Africa/Nouakchott</option>
                    <option {if $timeZone=='Africa/Ouagadougou'}selected{/if} value='Africa/Ouagadougou'>Africa/Ouagadougou</option>
                    <option {if $timeZone=='Africa/Porto-Novo'}selected{/if} value='Africa/Porto-Novo'>Africa/Porto-Novo</option>
                    <option {if $timeZone=='Africa/Sao_Tome'}selected{/if} value='Africa/Sao_Tome'>Africa/Sao_Tome</option>
                    <option {if $timeZone=='Africa/Timbuktu'}selected{/if} value='Africa/Timbuktu'>Africa/Timbuktu</option>
                    <option {if $timeZone=='Africa/Tripoli'}selected{/if} value='Africa/Tripoli'>Africa/Tripoli</option>
                    <option {if $timeZone=='Africa/Tunis'}selected{/if} value='Africa/Tunis'>Africa/Tunis</option>
                    <option {if $timeZone=='Africa/Windhoek'}selected{/if} value='Africa/Windhoek'>Africa/Windhoek</option>
                    <option {if $timeZone=='America/Adak'}selected{/if} value='America/Adak'>America/Adak</option>
                    <option {if $timeZone=='America/Anchorage'}selected{/if} value='America/Anchorage'>America/Anchorage</option>
                    <option {if $timeZone=='America/Anguilla'}selected{/if} value='America/Anguilla'>America/Anguilla</option>
                    <option {if $timeZone=='America/Antigua'}selected{/if} value='America/Antigua'>America/Antigua</option>
                    <option {if $timeZone=='America/Araguaina'}selected{/if} value='America/Araguaina'>America/Araguaina</option>
                    <option {if $timeZone=='America/Aruba'}selected{/if} value='America/Aruba'>America/Aruba</option>
                    <option {if $timeZone=='America/Asuncion'}selected{/if} value='America/Asuncion'>America/Asuncion</option>
                    <option {if $timeZone=='America/Barbados'}selected{/if} value='America/Barbados'>America/Barbados</option>
                    <option {if $timeZone=='America/Belem'}selected{/if} value='America/Belem'>America/Belem</option>
                    <option {if $timeZone=='America/Belize'}selected{/if} value='America/Belize'>America/Belize</option>
                    <option {if $timeZone=='America/Boa_Vista'}selected{/if} value='America/Boa_Vista'>America/Boa_Vista</option>
                    <option {if $timeZone=='America/Bogota'}selected{/if} value='America/Bogota'>America/Bogota</option>
                    <option {if $timeZone=='America/Boise'}selected{/if} value='America/Boise'>America/Boise</option>
                    <option {if $timeZone=='America/Buenos_Aires'}selected{/if} value='America/Buenos_Aires'>America/Buenos_Aires</option>
                    <option {if $timeZone=='America/Cambridge_Bay'}selected{/if} value='America/Cambridge_Bay'>America/Cambridge_Bay</option>
                    <option {if $timeZone=='America/Cancun'}selected{/if} value='America/Cancun'>America/Cancun</option>
                    <option {if $timeZone=='America/Caracas'}selected{/if} value='America/Caracas'>America/Caracas</option>
                    <option {if $timeZone=='America/Catamarca'}selected{/if} value='America/Catamarca'>America/Catamarca</option>
                    <option {if $timeZone=='America/Cayenne'}selected{/if} value='America/Cayenne'>America/Cayenne</option>
                    <option {if $timeZone=='America/Cayman'}selected{/if} value='America/Cayman'>America/Cayman</option>
                    <option {if $timeZone=='America/Chicago'}selected{/if} value='America/Chicago'>America/Chicago</option>
                    <option {if $timeZone=='America/Chihuahua'}selected{/if} value='America/Chihuahua'>America/Chihuahua</option>
                    <option {if $timeZone=='America/Cordoba'}selected{/if} value='America/Cordoba'>America/Cordoba</option>
                    <option {if $timeZone=='America/Costa_Rica'}selected{/if} value='America/Costa_Rica'>America/Costa_Rica</option>
                    <option {if $timeZone=='America/Cuiaba'}selected{/if} value='America/Cuiaba'>America/Cuiaba</option>
                    <option {if $timeZone=='America/Curacao'}selected{/if} value='America/Curacao'>America/Curacao</option>
                    <option {if $timeZone=='America/Dawson'}selected{/if} value='America/Dawson'>America/Dawson</option>
                    <option {if $timeZone=='America/Dawson_Creek'}selected{/if} value='America/Dawson_Creek'>America/Dawson_Creek</option>
                    <option {if $timeZone=='America/Denver'}selected{/if} value='America/Denver'>America/Denver</option>
                    <option {if $timeZone=='America/Detroit'}selected{/if} value='America/Detroit'>America/Detroit</option>
                    <option {if $timeZone=='America/Dominica'}selected{/if} value='America/Dominica'>America/Dominica</option>
                    <option {if $timeZone=='America/Edmonton'}selected{/if} value='America/Edmonton'>America/Edmonton</option>
                    <option {if $timeZone=='America/El_Salvador'}selected{/if} value='America/El_Salvador'>America/El_Salvador</option>
                    <option {if $timeZone=='America/Fortaleza'}selected{/if} value='America/Fortaleza'>America/Fortaleza</option>
                    <option {if $timeZone=='America/Glace_Bay'}selected{/if} value='America/Glace_Bay'>America/Glace_Bay</option>
                    <option {if $timeZone=='America/Godthab'}selected{/if} value='America/Godthab'>America/Godthab</option>
                    <option {if $timeZone=='America/Goose_Bay'}selected{/if} value='America/Goose_Bay'>America/Goose_Bay</option>
                    <option {if $timeZone=='America/Grand_Turk'}selected{/if} value='America/Grand_Turk'>America/Grand_Turk</option>
                    <option {if $timeZone=='America/Grenada'}selected{/if} value='America/Grenada'>America/Grenada</option>
                    <option {if $timeZone=='America/Guadeloupe'}selected{/if} value='America/Guadeloupe'>America/Guadeloupe</option>
                    <option {if $timeZone=='America/Guatemala'}selected{/if} value='America/Guatemala'>America/Guatemala</option>
                    <option {if $timeZone=='America/Guayaquil'}selected{/if} value='America/Guayaquil'>America/Guayaquil</option>
                    <option {if $timeZone=='America/Guyana'}selected{/if} value='America/Guyana'>America/Guyana</option>
                    <option {if $timeZone=='America/Halifax'}selected{/if} value='America/Halifax'>America/Halifax</option>
                    <option {if $timeZone=='America/Havana'}selected{/if} value='America/Havana'>America/Havana</option>
                    <option {if $timeZone=='America/Hermosillo'}selected{/if} value='America/Hermosillo'>America/Hermosillo</option>
                    <option {if $timeZone=='America/Indiana/Knox'}selected{/if} value='America/Indiana/Knox'>America/Indiana/Knox</option>
                    <option {if $timeZone=='America/Indiana/Marengo'}selected{/if} value='America/Indiana/Marengo'>America/Indiana/Marengo</option>
                    <option {if $timeZone=='America/Indiana/Vevay'}selected{/if} value='America/Indiana/Vevay'>America/Indiana/Vevay</option>
                    <option {if $timeZone=='America/Indianapolis'}selected{/if} value='America/Indianapolis'>America/Indianapolis</option>
                    <option {if $timeZone=='America/Inuvik'}selected{/if} value='America/Inuvik'>America/Inuvik</option>
                    <option {if $timeZone=='America/Iqaluit'}selected{/if} value='America/Iqaluit'>America/Iqaluit</option>
                    <option {if $timeZone=='America/Jamaica'}selected{/if} value='America/Jamaica'>America/Jamaica</option>
                    <option {if $timeZone=='America/Jujuy'}selected{/if} value='America/Jujuy'>America/Jujuy</option>
                    <option {if $timeZone=='America/Juneau'}selected{/if} value='America/Juneau'>America/Juneau</option>
                    <option {if $timeZone=='America/La_Paz'}selected{/if} value='America/La_Paz'>America/La_Paz</option>
                    <option {if $timeZone=='America/Lima'}selected{/if} value='America/Lima'>America/Lima</option>
                    <option {if $timeZone=='America/Los_Angeles'}selected{/if} value='America/Los_Angeles'>America/Los_Angeles</option>
                    <option {if $timeZone=='America/Louisville'}selected{/if} value='America/Louisville'>America/Louisville</option>
                    <option {if $timeZone=='America/Maceio'}selected{/if} value='America/Maceio'>America/Maceio</option>
                    <option {if $timeZone=='America/Managua'}selected{/if} value='America/Managua'>America/Managua</option>
                    <option {if $timeZone=='America/Manaus'}selected{/if} value='America/Manaus'>America/Manaus</option>
                    <option {if $timeZone=='America/Martinique'}selected{/if} value='America/Martinique'>America/Martinique</option>
                    <option {if $timeZone=='America/Mazatlan'}selected{/if} value='America/Mazatlan'>America/Mazatlan</option>
                    <option {if $timeZone=='America/Mendoza'}selected{/if} value='America/Mendoza'>America/Mendoza</option>
                    <option {if $timeZone=='America/Menominee'}selected{/if} value='America/Menominee'>America/Menominee</option>
                    <option {if $timeZone=='America/Mexico_City'}selected{/if} value='America/Mexico_City'>America/Mexico_City</option>
                    <option {if $timeZone=='America/Miquelon'}selected{/if} value='America/Miquelon'>America/Miquelon</option>
                    <option {if $timeZone=='America/Montevideo'}selected{/if} value='America/Montevideo'>America/Montevideo</option>
                    <option {if $timeZone=='America/Montreal'}selected{/if} value='America/Montreal'>America/Montreal</option>
                    <option {if $timeZone=='America/Montserrat'}selected{/if} value='America/Montserrat'>America/Montserrat</option>
                    <option {if $timeZone=='America/Nassau'}selected{/if} value='America/Nassau'>America/Nassau</option>
                    <option {if $timeZone=='America/New_York'}selected{/if} value='America/New_York'>America/New_York</option>
                    <option {if $timeZone=='America/Nipigon'}selected{/if} value='America/Nipigon'>America/Nipigon</option>
                    <option {if $timeZone=='America/Nome'}selected{/if} value='America/Nome'>America/Nome</option>
                    <option {if $timeZone=='America/Noronha'}selected{/if} value='America/Noronha'>America/Noronha</option>
                    <option {if $timeZone=='America/Panama'}selected{/if} value='America/Panama'>America/Panama</option>
                    <option {if $timeZone=='America/Pangnirtung'}selected{/if} value='America/Pangnirtung'>America/Pangnirtung</option>
                    <option {if $timeZone=='America/Paramaribo'}selected{/if} value='America/Paramaribo'>America/Paramaribo</option>
                    <option {if $timeZone=='America/Phoenix'}selected{/if} value='America/Phoenix'>America/Phoenix</option>
                    <option {if $timeZone=='America/Port-au-Prince'}selected{/if} value='America/Port-au-Prince'>America/Port-au-Prince</option>
                    <option {if $timeZone=='America/Port_of_Spain'}selected{/if} value='America/Port_of_Spain'>America/Port_of_Spain</option>
                    <option {if $timeZone=='America/Porto_Acre'}selected{/if} value='America/Porto_Acre'>America/Porto_Acre</option>
                    <option {if $timeZone=='America/Porto_Velho'}selected{/if} value='America/Porto_Velho'>America/Porto_Velho</option>
                    <option {if $timeZone=='America/Puerto_Rico'}selected{/if} value='America/Puerto_Rico'>America/Puerto_Rico</option>
                    <option {if $timeZone=='America/Rainy_River'}selected{/if} value='America/Rainy_River'>America/Rainy_River</option>
                    <option {if $timeZone=='America/Rankin_Inlet'}selected{/if} value='America/Rankin_Inlet'>America/Rankin_Inlet</option>
                    <option {if $timeZone=='America/Regina'}selected{/if} value='America/Regina'>America/Regina</option>
                    <option {if $timeZone=='America/Rosario'}selected{/if} value='America/Rosario'>America/Rosario</option>
                    <option {if $timeZone=='America/Santiago'}selected{/if} value='America/Santiago'>America/Santiago</option>
                    <option {if $timeZone=='America/Santo_Domingo'}selected{/if} value='America/Santo_Domingo'>America/Santo_Domingo</option>
                    <option {if $timeZone=='America/Sao_Paulo'}selected{/if} value='America/Sao_Paulo'>America/Sao_Paulo</option>
                    <option {if $timeZone=='America/Scoresbysund'}selected{/if} value='America/Scoresbysund'>America/Scoresbysund</option>
                    <option {if $timeZone=='America/St_Johns'}selected{/if} value='America/St_Johns'>America/St_Johns</option>
                    <option {if $timeZone=='America/St_Kitts'}selected{/if} value='America/St_Kitts'>America/St_Kitts</option>
                    <option {if $timeZone=='America/St_Lucia'}selected{/if} value='America/St_Lucia'>America/St_Lucia</option>
                    <option {if $timeZone=='America/St_Thomas'}selected{/if} value='America/St_Thomas'>America/St_Thomas</option>
                    <option {if $timeZone=='America/St_Vincent'}selected{/if} value='America/St_Vincent'>America/St_Vincent</option>
                    <option {if $timeZone=='America/Swift_Current'}selected{/if} value='America/Swift_Current'>America/Swift_Current</option>
                    <option {if $timeZone=='America/Tegucigalpa'}selected{/if} value='America/Tegucigalpa'>America/Tegucigalpa</option>
                    <option {if $timeZone=='America/Thule'}selected{/if} value='America/Thule'>America/Thule</option>
                    <option {if $timeZone=='America/Thunder_Bay'}selected{/if} value='America/Thunder_Bay'>America/Thunder_Bay</option>
                    <option {if $timeZone=='America/Tijuana'}selected{/if} value='America/Tijuana'>America/Tijuana</option>
                    <option {if $timeZone=='America/Tortola'}selected{/if} value='America/Tortola'>America/Tortola</option>
                    <option {if $timeZone=='America/Vancouver'}selected{/if} value='America/Vancouver'>America/Vancouver</option>
                    <option {if $timeZone=='America/Whitehorse'}selected{/if} value='America/Whitehorse'>America/Whitehorse</option>
                    <option {if $timeZone=='America/Winnipeg'}selected{/if} value='America/Winnipeg'>America/Winnipeg</option>
                    <option {if $timeZone=='America/Yakutat'}selected{/if} value='America/Yakutat'>America/Yakutat</option>
                    <option {if $timeZone=='America/Yellowknife'}selected{/if} value='America/Yellowknife'>America/Yellowknife</option>
                    <option {if $timeZone=='Antarctica/Casey'}selected{/if} value='Antarctica/Casey'>Antarctica/Casey</option>
                    <option {if $timeZone=='Antarctica/Davis'}selected{/if} value='Antarctica/Davis'>Antarctica/Davis</option>
                    <option {if $timeZone=='Antarctica/DumontDUrville'}selected{/if} value='Antarctica/DumontDUrville'>Antarctica/DumontDUrville</option>
                    <option {if $timeZone=='Antarctica/Mawson'}selected{/if} value='Antarctica/Mawson'>Antarctica/Mawson</option>
                    <option {if $timeZone=='Antarctica/McMurdo'}selected{/if} value='Antarctica/McMurdo'>Antarctica/McMurdo</option>
                    <option {if $timeZone=='Antarctica/Palmer'}selected{/if} value='Antarctica/Palmer'>Antarctica/Palmer</option>
                    <option {if $timeZone=='Antarctica/Syowa'}selected{/if} value='Antarctica/Syowa'>Antarctica/Syowa</option>
                    <option {if $timeZone=='Asia/Aden'}selected{/if} value='Asia/Aden'>Asia/Aden</option>
                    <option {if $timeZone=='Asia/Almaty'}selected{/if} value='Asia/Almaty'>Asia/Almaty</option>
                    <option {if $timeZone=='Asia/Amman'}selected{/if} value='Asia/Amman'>Asia/Amman</option>
                    <option {if $timeZone=='Asia/Anadyr'}selected{/if} value='Asia/Anadyr'>Asia/Anadyr</option>
                    <option {if $timeZone=='Asia/Aqtau'}selected{/if} value='Asia/Aqtau'>Asia/Aqtau</option>
                    <option {if $timeZone=='Asia/Aqtobe'}selected{/if} value='Asia/Aqtobe'>Asia/Aqtobe</option>
                    <option {if $timeZone=='Asia/Ashkhabad'}selected{/if} value='Asia/Ashkhabad'>Asia/Ashkhabad</option>
                    <option {if $timeZone=='Asia/Baghdad'}selected{/if} value='Asia/Baghdad'>Asia/Baghdad</option>
                    <option {if $timeZone=='Asia/Bahrain'}selected{/if} value='Asia/Bahrain'>Asia/Bahrain</option>
                    <option {if $timeZone=='Asia/Baku'}selected{/if} value='Asia/Baku'>Asia/Baku</option>
                    <option {if $timeZone=='Asia/Bangkok'}selected{/if} value='Asia/Bangkok'>Asia/Bangkok</option>
                    <option {if $timeZone=='Asia/Beirut'}selected{/if} value='Asia/Beirut'>Asia/Beirut</option>
                    <option {if $timeZone=='Asia/Bishkek'}selected{/if} value='Asia/Bishkek'>Asia/Bishkek</option>
                    <option {if $timeZone=='Asia/Brunei'}selected{/if} value='Asia/Brunei'>Asia/Brunei</option>
                    <option {if $timeZone=='Asia/Calcutta'}selected{/if} value='Asia/Calcutta'>Asia/Calcutta</option>
                    <option {if $timeZone=='Asia/Chungking'}selected{/if} value='Asia/Chungking'>Asia/Chungking</option>
                    <option {if $timeZone=='Asia/Colombo'}selected{/if} value='Asia/Colombo'>Asia/Colombo</option>
                    <option {if $timeZone=='Asia/Dacca'}selected{/if} value='Asia/Dacca'>Asia/Dacca</option>
                    <option {if $timeZone=='Asia/Damascus'}selected{/if} value='Asia/Damascus'>Asia/Damascus</option>
                    <option {if $timeZone=='Asia/Dili'}selected{/if} value='Asia/Dili'>Asia/Dili</option>
                    <option {if $timeZone=='Asia/Dubai'}selected{/if} value='Asia/Dubai'>Asia/Dubai</option>
                    <option {if $timeZone=='Asia/Dushanbe'}selected{/if} value='Asia/Dushanbe'>Asia/Dushanbe</option>
                    <option {if $timeZone=='Asia/Gaza'}selected{/if} value='Asia/Gaza'>Asia/Gaza</option>
                    <option {if $timeZone=='Asia/Harbin'}selected{/if} value='Asia/Harbin'>Asia/Harbin</option>
                    <option {if $timeZone=='Asia/Hong_Kong'}selected{/if} value='Asia/Hong_Kong'>Asia/Hong_Kong</option>
                    <option {if $timeZone=='Asia/Hovd'}selected{/if} value='Asia/Hovd'>Asia/Hovd</option>
                    <option {if $timeZone=='Asia/Irkutsk'}selected{/if} value='Asia/Irkutsk'>Asia/Irkutsk</option>
                    <option {if $timeZone=='Asia/Jakarta'}selected{/if} value='Asia/Jakarta'>Asia/Jakarta</option>
                    <option {if $timeZone=='Asia/Jayapura'}selected{/if} value='Asia/Jayapura'>Asia/Jayapura</option>
                    <option {if $timeZone=='Asia/Jerusalem'}selected{/if} value='Asia/Jerusalem'>Asia/Jerusalem</option>
                    <option {if $timeZone=='Asia/Kabul'}selected{/if} value='Asia/Kabul'>Asia/Kabul</option>
                    <option {if $timeZone=='Asia/Kamchatka'}selected{/if} value='Asia/Kamchatka'>Asia/Kamchatka</option>
                    <option {if $timeZone=='Asia/Karachi'}selected{/if} value='Asia/Karachi'>Asia/Karachi</option>
                    <option {if $timeZone=='Asia/Kashgar'}selected{/if} value='Asia/Kashgar'>Asia/Kashgar</option>
                    <option {if $timeZone=='Asia/Katmandu'}selected{/if} value='Asia/Katmandu'>Asia/Katmandu</option>
                    <option {if $timeZone=='Asia/Krasnoyarsk'}selected{/if} value='Asia/Krasnoyarsk'>Asia/Krasnoyarsk</option>
                    <option {if $timeZone=='Asia/Kuala_Lumpur'}selected{/if} value='Asia/Kuala_Lumpur'>Asia/Kuala_Lumpur</option>
                    <option {if $timeZone=='Asia/Kuching'}selected{/if} value='Asia/Kuching'>Asia/Kuching</option>
                    <option {if $timeZone=='Asia/Kuwait'}selected{/if} value='Asia/Kuwait'>Asia/Kuwait</option>
                    <option {if $timeZone=='Asia/Macao'}selected{/if} value='Asia/Macao'>Asia/Macao</option>
                    <option {if $timeZone=='Asia/Magadan'}selected{/if} value='Asia/Magadan'>Asia/Magadan</option>
                    <option {if $timeZone=='Asia/Manila'}selected{/if} value='Asia/Manila'>Asia/Manila</option>
                    <option {if $timeZone=='Asia/Muscat'}selected{/if} value='Asia/Muscat'>Asia/Muscat</option>
                    <option {if $timeZone=='Asia/Nicosia'}selected{/if} value='Asia/Nicosia'>Asia/Nicosia</option>
                    <option {if $timeZone=='Asia/Novosibirsk'}selected{/if} value='Asia/Novosibirsk'>Asia/Novosibirsk</option>
                    <option {if $timeZone=='Asia/Omsk'}selected{/if} value='Asia/Omsk'>Asia/Omsk</option>
                    <option {if $timeZone=='Asia/Phnom_Penh'}selected{/if} value='Asia/Phnom_Penh'>Asia/Phnom_Penh</option>
                    <option {if $timeZone=='Asia/Pyongyang'}selected{/if} value='Asia/Pyongyang'>Asia/Pyongyang</option>
                    <option {if $timeZone=='Asia/Qatar'}selected{/if} value='Asia/Qatar'>Asia/Qatar</option>
                    <option {if $timeZone=='Asia/Rangoon'}selected{/if} value='Asia/Rangoon'>Asia/Rangoon</option>
                    <option {if $timeZone=='Asia/Riyadh'}selected{/if} value='Asia/Riyadh'>Asia/Riyadh</option>
                    <option {if $timeZone=='Asia/Saigon'}selected{/if} value='Asia/Saigon'>Asia/Saigon</option>
                    <option {if $timeZone=='Asia/Samarkand'}selected{/if} value='Asia/Samarkand'>Asia/Samarkand</option>
                    <option {if $timeZone=='Asia/Seoul'}selected{/if} value='Asia/Seoul'>Asia/Seoul</option>
                    <option {if $timeZone=='Asia/Shanghai'}selected{/if} value='Asia/Shanghai'>Asia/Shanghai</option>
                    <option {if $timeZone=='Asia/Singapore'}selected{/if} value='Asia/Singapore'>Asia/Singapore</option>
                    <option {if $timeZone=='Asia/Taipei'}selected{/if} value='Asia/Taipei'>Asia/Taipei</option>
                    <option {if $timeZone=='Asia/Tashkent'}selected{/if} value='Asia/Tashkent'>Asia/Tashkent</option>
                    <option {if $timeZone=='Asia/Tbilisi'}selected{/if} value='Asia/Tbilisi'>Asia/Tbilisi</option>
                    <option {if $timeZone=='Asia/Tehran'}selected{/if} value='Asia/Tehran'>Asia/Tehran</option>
                    <option {if $timeZone=='Asia/Thimbu'}selected{/if} value='Asia/Thimbu'>Asia/Thimbu</option>
                    <option {if $timeZone=='Asia/Tokyo'}selected{/if} value='Asia/Tokyo'>Asia/Tokyo</option>
                    <option {if $timeZone=='Asia/Ujung_Pandang'}selected{/if} value='Asia/Ujung_Pandang'>Asia/Ujung_Pandang</option>
                    <option {if $timeZone=='Asia/Ulaanbaatar'}selected{/if} value='Asia/Ulaanbaatar'>Asia/Ulaanbaatar</option>
                    <option {if $timeZone=='Asia/Urumqi'}selected{/if} value='Asia/Urumqi'>Asia/Urumqi</option>
                    <option {if $timeZone=='Asia/Vientiane'}selected{/if} value='Asia/Vientiane'>Asia/Vientiane</option>
                    <option {if $timeZone=='Asia/Vladivostok'}selected{/if} value='Asia/Vladivostok'>Asia/Vladivostok</option>
                    <option {if $timeZone=='Asia/Yakutsk'}selected{/if} value='Asia/Yakutsk'>Asia/Yakutsk</option>
                    <option {if $timeZone=='Asia/Yekaterinburg'}selected{/if} value='Asia/Yekaterinburg'>Asia/Yekaterinburg</option>
                    <option {if $timeZone=='Asia/Yerevan'}selected{/if} value='Asia/Yerevan'>Asia/Yerevan</option>
                    <option {if $timeZone=='Atlantic/Azores'}selected{/if} value='Atlantic/Azores'>Atlantic/Azores</option>
                    <option {if $timeZone=='Atlantic/Bermuda'}selected{/if} value='Atlantic/Bermuda'>Atlantic/Bermuda</option>
                    <option {if $timeZone=='Atlantic/Canary'}selected{/if} value='Atlantic/Canary'>Atlantic/Canary</option>
                    <option {if $timeZone=='Atlantic/Cape_Verde'}selected{/if} value='Atlantic/Cape_Verde'>Atlantic/Cape_Verde</option>
                    <option {if $timeZone=='Atlantic/Faeroe'}selected{/if} value='Atlantic/Faeroe'>Atlantic/Faeroe</option>
                    <option {if $timeZone=='Atlantic/Jan_Mayen'}selected{/if} value='Atlantic/Jan_Mayen'>Atlantic/Jan_Mayen</option>
                    <option {if $timeZone=='Atlantic/Madeira'}selected{/if} value='Atlantic/Madeira'>Atlantic/Madeira</option>
                    <option {if $timeZone=='Atlantic/Reykjavik'}selected{/if} value='Atlantic/Reykjavik'>Atlantic/Reykjavik</option>
                    <option {if $timeZone=='Atlantic/South_Georgia'}selected{/if} value='Atlantic/South_Georgia'>Atlantic/South_Georgia</option>
                    <option {if $timeZone=='Atlantic/St_Helena'}selected{/if} value='Atlantic/St_Helena'>Atlantic/St_Helena</option>
                    <option {if $timeZone=='Atlantic/Stanley'}selected{/if} value='Atlantic/Stanley'>Atlantic/Stanley</option>
                    <option {if $timeZone=='Australia/Adelaide'}selected{/if} value='Australia/Adelaide'>Australia/Adelaide</option>
                    <option {if $timeZone=='Australia/Brisbane'}selected{/if} value='Australia/Brisbane'>Australia/Brisbane</option>
                    <option {if $timeZone=='Australia/Broken_Hill'}selected{/if} value='Australia/Broken_Hill'>Australia/Broken_Hill</option>
                    <option {if $timeZone=='Australia/Darwin'}selected{/if} value='Australia/Darwin'>Australia/Darwin</option>
                    <option {if $timeZone=='Australia/Hobart'}selected{/if} value='Australia/Hobart'>Australia/Hobart</option>
                    <option {if $timeZone=='Australia/Lindeman'}selected{/if} value='Australia/Lindeman'>Australia/Lindeman</option>
                    <option {if $timeZone=='Australia/Lord_Howe'}selected{/if} value='Australia/Lord_Howe'>Australia/Lord_Howe</option>
                    <option {if $timeZone=='Australia/Melbourne'}selected{/if} value='Australia/Melbourne'>Australia/Melbourne</option>
                    <option {if $timeZone=='Australia/Perth'}selected{/if} value='Australia/Perth'>Australia/Perth</option>
                    <option {if $timeZone=='Australia/Sydney'}selected{/if} value='Australia/Sydney'>Australia/Sydney</option>
                    <option {if $timeZone=='CET'}selected{/if} value='CET'>CET</option>
                    <option {if $timeZone=='EET'}selected{/if} value='EET'>EET</option>
                    <option {if $timeZone=='Europe/Amsterdam'}selected{/if} value='Europe/Amsterdam'>Europe/Amsterdam</option>
                    <option {if $timeZone=='Europe/Andorra'}selected{/if} value='Europe/Andorra'>Europe/Andorra</option>
                    <option {if $timeZone=='Europe/Athens'}selected{/if} value='Europe/Athens'>Europe/Athens</option>
                    <option {if $timeZone=='Europe/Belfast'}selected{/if} value='Europe/Belfast'>Europe/Belfast</option>
                    <option {if $timeZone=='Europe/Belgrade'}selected{/if} value='Europe/Belgrade'>Europe/Belgrade</option>
                    <option {if $timeZone=='Europe/Berlin'}selected{/if} value='Europe/Berlin'>Europe/Berlin</option>
                    <option {if $timeZone=='Europe/Brussels'}selected{/if} value='Europe/Brussels'>Europe/Brussels</option>
                    <option {if $timeZone=='Europe/Bucharest'}selected{/if} value='Europe/Bucharest'>Europe/Bucharest</option>
                    <option {if $timeZone=='Europe/Budapest'}selected{/if} value='Europe/Budapest'>Europe/Budapest</option>
                    <option {if $timeZone=='Europe/Chisinau'}selected{/if} value='Europe/Chisinau'>Europe/Chisinau</option>
                    <option {if $timeZone=='Europe/Copenhagen'}selected{/if} value='Europe/Copenhagen'>Europe/Copenhagen</option>
                    <option {if $timeZone=='Europe/Dublin'}selected{/if} value='Europe/Dublin'>Europe/Dublin</option>
                    <option {if $timeZone=='Europe/Gibraltar'}selected{/if} value='Europe/Gibraltar'>Europe/Gibraltar</option>
                    <option {if $timeZone=='Europe/Helsinki'}selected{/if} value='Europe/Helsinki'>Europe/Helsinki</option>
                    <option {if $timeZone=='Europe/Istanbul'}selected{/if} value='Europe/Istanbul'>Europe/Istanbul</option>
                    <option {if $timeZone=='Europe/Kaliningrad'}selected{/if} value='Europe/Kaliningrad'>Europe/Kaliningrad</option>
                    <option {if $timeZone=='Europe/Kiev'}selected{/if} value='Europe/Kiev'>Europe/Kiev</option>
                    <option {if $timeZone=='Europe/Lisbon'}selected{/if} value='Europe/Lisbon'>Europe/Lisbon</option>
                    <option {if $timeZone=='Europe/London'}selected{/if} value='Europe/London'>Europe/London</option>
                    <option {if $timeZone=='Europe/Luxembourg'}selected{/if} value='Europe/Luxembourg'>Europe/Luxembourg</option>
                    <option {if $timeZone=='Europe/Madrid'}selected{/if} value='Europe/Madrid'>Europe/Madrid</option>
                    <option {if $timeZone=='Europe/Malta'}selected{/if} value='Europe/Malta'>Europe/Malta</option>
                    <option {if $timeZone=='Europe/Minsk'}selected{/if} value='Europe/Minsk'>Europe/Minsk</option>
                    <option {if $timeZone=='Europe/Monaco'}selected{/if} value='Europe/Monaco'>Europe/Monaco</option>
                    <option {if $timeZone=='Europe/Moscow'}selected{/if} value='Europe/Moscow'>Europe/Moscow</option>
                    <option {if $timeZone=='Europe/Oslo'}selected{/if} value='Europe/Oslo'>Europe/Oslo</option>
                    <option {if $timeZone=='Europe/Paris'}selected{/if} value='Europe/Paris'>Europe/Paris</option>
                    <option {if $timeZone=='Europe/Prague'}selected{/if} value='Europe/Prague'>Europe/Prague</option>
                    <option {if $timeZone=='Europe/Riga'}selected{/if} value='Europe/Riga'>Europe/Riga</option>
                    <option {if $timeZone=='Europe/Rome'}selected{/if} value='Europe/Rome'>Europe/Rome</option>
                    <option {if $timeZone=='Europe/Samara'}selected{/if} value='Europe/Samara'>Europe/Samara</option>
                    <option {if $timeZone=='Europe/Simferopol'}selected{/if} value='Europe/Simferopol'>Europe/Simferopol</option>
                    <option {if $timeZone=='Europe/Sofia'}selected{/if} value='Europe/Sofia'>Europe/Sofia</option>
                    <option {if $timeZone=='Europe/Stockholm'}selected{/if} value='Europe/Stockholm'>Europe/Stockholm</option>
                    <option {if $timeZone=='Europe/Tallinn'}selected{/if} value='Europe/Tallinn'>Europe/Tallinn</option>
                    <option {if $timeZone=='Europe/Tirane'}selected{/if} value='Europe/Tirane'>Europe/Tirane</option>
                    <option {if $timeZone=='Europe/Tiraspol'}selected{/if} value='Europe/Tiraspol'>Europe/Tiraspol</option>
                    <option {if $timeZone=='Europe/Uzhgorod'}selected{/if} value='Europe/Uzhgorod'>Europe/Uzhgorod</option>
                    <option {if $timeZone=='Europe/Vaduz'}selected{/if} value='Europe/Vaduz'>Europe/Vaduz</option>
                    <option {if $timeZone=='Europe/Vienna'}selected{/if} value='Europe/Vienna'>Europe/Vienna</option>
                    <option {if $timeZone=='Europe/Vilnius'}selected{/if} value='Europe/Vilnius'>Europe/Vilnius</option>
                    <option {if $timeZone=='Europe/Warsaw'}selected{/if} value='Europe/Warsaw'>Europe/Warsaw</option>
                    <option {if $timeZone=='Europe/Zaporozhye'}selected{/if} value='Europe/Zaporozhye'>Europe/Zaporozhye</option>
                    <option {if $timeZone=='Europe/Zurich'}selected{/if} value='Europe/Zurich'>Europe/Zurich</option>
                    <option {if $timeZone=='GMT'}selected{/if} value='GMT'>GMT</option>
                    <option {if $timeZone=='Indian/Antananarivo'}selected{/if} value='Indian/Antananarivo'>Indian/Antananarivo</option>
                    <option {if $timeZone=='Indian/Chagos'}selected{/if} value='Indian/Chagos'>Indian/Chagos</option>
                    <option {if $timeZone=='Indian/Christmas'}selected{/if} value='Indian/Christmas'>Indian/Christmas</option>
                    <option {if $timeZone=='Indian/Cocos'}selected{/if} value='Indian/Cocos'>Indian/Cocos</option>
                    <option {if $timeZone=='Indian/Comoro'}selected{/if} value='Indian/Comoro'>Indian/Comoro</option>
                    <option {if $timeZone=='Indian/Kerguelen'}selected{/if} value='Indian/Kerguelen'>Indian/Kerguelen</option>
                    <option {if $timeZone=='Indian/Mahe'}selected{/if} value='Indian/Mahe'>Indian/Mahe</option>
                    <option {if $timeZone=='Indian/Maldives'}selected{/if} value='Indian/Maldives'>Indian/Maldives</option>
                    <option {if $timeZone=='Indian/Mauritius'}selected{/if} value='Indian/Mauritius'>Indian/Mauritius</option>
                    <option {if $timeZone=='Indian/Mayotte'}selected{/if} value='Indian/Mayotte'>Indian/Mayotte</option>
                    <option {if $timeZone=='Indian/Reunion'}selected{/if} value='Indian/Reunion'>Indian/Reunion</option>
                    <option {if $timeZone=='MET'}selected{/if} value='MET'>MET</option>
                    <option {if $timeZone=='Pacific/Apia'}selected{/if} value='Pacific/Apia'>Pacific/Apia</option>
                    <option {if $timeZone=='Pacific/Auckland'}selected{/if} value='Pacific/Auckland'>Pacific/Auckland</option>
                    <option {if $timeZone=='Pacific/Chatham'}selected{/if} value='Pacific/Chatham'>Pacific/Chatham</option>
                    <option {if $timeZone=='Pacific/Easter'}selected{/if} value='Pacific/Easter'>Pacific/Easter</option>
                    <option {if $timeZone=='Pacific/Efate'}selected{/if} value='Pacific/Efate'>Pacific/Efate</option>
                    <option {if $timeZone=='Pacific/Enderbury'}selected{/if} value='Pacific/Enderbury'>Pacific/Enderbury</option>
                    <option {if $timeZone=='Pacific/Fakaofo'}selected{/if} value='Pacific/Fakaofo'>Pacific/Fakaofo</option>
                    <option {if $timeZone=='Pacific/Fiji'}selected{/if} value='Pacific/Fiji'>Pacific/Fiji</option>
                    <option {if $timeZone=='Pacific/Funafuti'}selected{/if} value='Pacific/Funafuti'>Pacific/Funafuti</option>
                    <option {if $timeZone=='Pacific/Galapagos'}selected{/if} value='Pacific/Galapagos'>Pacific/Galapagos</option>
                    <option {if $timeZone=='Pacific/Gambier'}selected{/if} value='Pacific/Gambier'>Pacific/Gambier</option>
                    <option {if $timeZone=='Pacific/Guadalcanal'}selected{/if} value='Pacific/Guadalcanal'>Pacific/Guadalcanal</option>
                    <option {if $timeZone=='Pacific/Guam'}selected{/if} value='Pacific/Guam'>Pacific/Guam</option>
                    <option {if $timeZone=='Pacific/Honolulu'}selected{/if} value='Pacific/Honolulu'>Pacific/Honolulu</option>
                    <option {if $timeZone=='Pacific/Johnston'}selected{/if} value='Pacific/Johnston'>Pacific/Johnston</option>
                    <option {if $timeZone=='Pacific/Kiritimati'}selected{/if} value='Pacific/Kiritimati'>Pacific/Kiritimati</option>
                    <option {if $timeZone=='Pacific/Kosrae'}selected{/if} value='Pacific/Kosrae'>Pacific/Kosrae</option>
                    <option {if $timeZone=='Pacific/Kwajalein'}selected{/if} value='Pacific/Kwajalein'>Pacific/Kwajalein</option>
                    <option {if $timeZone=='Pacific/Majuro'}selected{/if} value='Pacific/Majuro'>Pacific/Majuro</option>
                    <option {if $timeZone=='Pacific/Marquesas'}selected{/if} value='Pacific/Marquesas'>Pacific/Marquesas</option>
                    <option {if $timeZone=='Pacific/Midway'}selected{/if} value='Pacific/Midway'>Pacific/Midway</option>
                    <option {if $timeZone=='Pacific/Nauru'}selected{/if} value='Pacific/Nauru'>Pacific/Nauru</option>
                    <option {if $timeZone=='Pacific/Niue'}selected{/if} value='Pacific/Niue'>Pacific/Niue</option>
                    <option {if $timeZone=='Pacific/Norfolk'}selected{/if} value='Pacific/Norfolk'>Pacific/Norfolk</option>
                    <option {if $timeZone=='Pacific/Noumea'}selected{/if} value='Pacific/Noumea'>Pacific/Noumea</option>
                    <option {if $timeZone=='Pacific/Pago_Pago'}selected{/if} value='Pacific/Pago_Pago'>Pacific/Pago_Pago</option>
                    <option {if $timeZone=='Pacific/Palau'}selected{/if} value='Pacific/Palau'>Pacific/Palau</option>
                    <option {if $timeZone=='Pacific/Pitcairn'}selected{/if} value='Pacific/Pitcairn'>Pacific/Pitcairn</option>
                    <option {if $timeZone=='Pacific/Ponape'}selected{/if} value='Pacific/Ponape'>Pacific/Ponape</option>
                    <option {if $timeZone=='Pacific/Port_Moresby'}selected{/if} value='Pacific/Port_Moresby'>Pacific/Port_Moresby</option>
                    <option {if $timeZone=='Pacific/Rarotonga'}selected{/if} value='Pacific/Rarotonga'>Pacific/Rarotonga</option>
                    <option {if $timeZone=='Pacific/Saipan'}selected{/if} value='Pacific/Saipan'>Pacific/Saipan</option>
                    <option {if $timeZone=='Pacific/Tahiti'}selected{/if} value='Pacific/Tahiti'>Pacific/Tahiti</option>
                    <option {if $timeZone=='Pacific/Tarawa'}selected{/if} value='Pacific/Tarawa'>Pacific/Tarawa</option>
                    <option {if $timeZone=='Pacific/Tongatapu'}selected{/if} value='Pacific/Tongatapu'>Pacific/Tongatapu</option>
                    <option {if $timeZone=='Pacific/Truk'}selected{/if} value='Pacific/Truk'>Pacific/Truk</option>
                    <option {if $timeZone=='Pacific/Wake'}selected{/if} value='Pacific/Wake'>Pacific/Wake</option>
                    <option {if $timeZone=='Pacific/Wallis'}selected{/if} value='Pacific/Wallis'>Pacific/Wallis</option>
                    <option {if $timeZone=='Pacific/Yap'}selected{/if} value='Pacific/Yap'>Pacific/Yap</option>
                    <option {if $timeZone=='WET'}selected{/if} value='WET'>WET</option>
                </select>

            </div>
            <small id="errorTimeZone" class="form-text text-danger">&nbsp;{$errorTimeZone}</small>
        </div>
        <div class="col-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Language</div>
                </div>
                <select class="form-control" id="language" name="language" aria-describedby="" placeholder="">
                    <option {if $language==='ESA'}selected{/if} value="ESA">ESA</option>
                </select>

            </div>
            <small id="errorLanguage" class="form-text text-danger">&nbsp;{$errorLanguage}</small>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Payment Term Name</div>
                </div>
                <select class="form-control" id="paymentTermName" name="paymentTermName">
                    <option {if $paymentTermName==='DAY 05'}selected{/if} value='DAY 05'>DAY 05</option>
                    <option {if $paymentTermName==='IMMEDIATE'}selected{/if} value='IMMEDIATE'>IMMEDIATE</option>
                    <option {if $paymentTermName==='30 NET'}selected{/if} value='30 NET'>30 NET</option>
                    <option {if $paymentTermName==='30-120 FIX10'}selected{/if} value='30-120 FIX10'>30-120 FIX10</option>
                    <option {if $paymentTermName==='30-120 FIX15'}selected{/if} value='30-120 FIX15'>30-120 FIX15</option>
                    <option {if $paymentTermName==='30-120 FIX20'}selected{/if} value='30-120 FIX20'>30-120 FIX20</option>
                    <option {if $paymentTermName==='30-120 FIX5'}selected{/if} value='30-120 FIX5'>30-120 FIX5</option>
                    <option {if $paymentTermName==='30-120 SPLIT'}selected{/if} value='30-120 SPLIT'>30-120 SPLIT</option>
                    <option {if $paymentTermName==='30-150 FIX10'}selected{/if} value='30-150 FIX10'>30-150 FIX10</option>
                    <option {if $paymentTermName==='30-150 FIX15'}selected{/if} value='30-150 FIX15'>30-150 FIX15</option>
                    <option {if $paymentTermName==='30-150 FIX20'}selected{/if} value='30-150 FIX20'>30-150 FIX20</option>
                    <option {if $paymentTermName==='30-150 FIX5'}selected{/if} value='30-150 FIX5'>30-150 FIX5</option>
                    <option {if $paymentTermName==='30-150 SPLIT'}selected{/if} value='30-150 SPLIT'>30-150 SPLIT</option>
                    <option {if $paymentTermName==='30-180 FIX10'}selected{/if} value='30-180 FIX10'>30-180 FIX10</option>
                    <option {if $paymentTermName==='30-180 FIX15'}selected{/if} value='30-180 FIX15'>30-180 FIX15</option>
                    <option {if $paymentTermName==='30-180 FIX20'}selected{/if} value='30-180 FIX20'>30-180 FIX20</option>
                    <option {if $paymentTermName==='30-180 FIX5'}selected{/if} value='30-180 FIX5'>30-180 FIX5</option>
                    <option {if $paymentTermName==='30-180 SPLIT'}selected{/if} value='30-180 SPLIT'>30-180 SPLIT</option>
                    <option {if $paymentTermName==='30-210 FIX10'}selected{/if} value='30-210 FIX10'>30-210 FIX10</option>
                    <option {if $paymentTermName==='30-210 FIX15'}selected{/if} value='30-210 FIX15'>30-210 FIX15</option>
                    <option {if $paymentTermName==='30-210 FIX20'}selected{/if} value='30-210 FIX20'>30-210 FIX20</option>
                    <option {if $paymentTermName==='30-210 FIX5'}selected{/if} value='30-210 FIX5'>30-210 FIX5</option>
                    <option {if $paymentTermName==='30-210 SPLIT'}selected{/if} value='30-210 SPLIT'>30-210 SPLIT</option>
                    <option {if $paymentTermName==='30-240 FIX10'}selected{/if} value='30-240 FIX10'>30-240 FIX10</option>
                    <option {if $paymentTermName==='30-240 FIX15'}selected{/if} value='30-240 FIX15'>30-240 FIX15</option>
                    <option {if $paymentTermName==='30-240 FIX20'}selected{/if} value='30-240 FIX20'>30-240 FIX20</option>
                    <option {if $paymentTermName==='30-240 FIX5'}selected{/if} value='30-240 FIX5'>30-240 FIX5</option>
                    <option {if $paymentTermName==='30-240 SPLIT'}selected{/if} value='30-240 SPLIT'>30-240 SPLIT</option>
                    <option {if $paymentTermName==='30-60 FIX10'}selected{/if} value='30-60 FIX10'>30-60 FIX10</option>
                    <option {if $paymentTermName==='30-60 FIX15'}selected{/if} value='30-60 FIX15'>30-60 FIX15</option>
                    <option {if $paymentTermName==='30-60 FIX20'}selected{/if} value='30-60 FIX20'>30-60 FIX20</option>
                    <option {if $paymentTermName==='30-60 FIX5'}selected{/if} value='30-60 FIX5'>30-60 FIX5</option>
                    <option {if $paymentTermName==='30-60 SPLIT'}selected{/if} value='30-60 SPLIT'>30-60 SPLIT</option>
                    <option {if $paymentTermName==='30-90 FIX10'}selected{/if} value='30-90 FIX10'>30-90 FIX10</option>
                    <option {if $paymentTermName==='30-90 FIX15'}selected{/if} value='30-90 FIX15'>30-90 FIX15</option>
                    <option {if $paymentTermName==='30-90 FIX20'}selected{/if} value='30-90 FIX20'>30-90 FIX20</option>
                    <option {if $paymentTermName==='30-90 FIX5'}selected{/if} value='30-90 FIX5'>30-90 FIX5</option>
                    <option {if $paymentTermName==='DAY 01'}selected{/if} value='DAY 01'>DAY 01</option>
                    <option {if $paymentTermName==='DAY 10'}selected{/if} value='DAY 10'>DAY 10</option>
                    <option {if $paymentTermName==='DAY 15'}selected{/if} value='DAY 15'>DAY 15</option>
                    <option {if $paymentTermName==='DAY 20'}selected{/if} value='DAY 20'>DAY 20</option>
                    <option {if $paymentTermName==='DAY 25'}selected{/if} value='DAY 25'>DAY 25</option>
                    <option {if $paymentTermName==='DAY 30'}selected{/if} value='DAY 30'>DAY 30</option>
                    <option {if $paymentTermName==='NET 10'}selected{/if} value='NET 10'>NET 10</option>
                    <option {if $paymentTermName==='SIIF CREDIT'}selected{/if} value='SIIF CREDIT'>SIIF CREDIT</option>
                </select>
            </div>
            <small id="errorPaymentTermName" class="form-text text-danger">&nbsp;{$errorPaymentTermName}</small>
        </div>
    {if ($country=='CR')}
        <div class="col-sm-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Concept Code &nbsp; <span class="flag-icon flag-icon-cr" title="CR only"></span></div>
                </div>
                <select class="form-control" id="conceptCode" name="conceptCode">
                    <option {if $conceptCode==='V'}selected{/if} value="V">V: Sale of goods and services</option>
                </select>
            </div>
            <small id="conceptCode" class="form-text text-danger">&nbsp;{$errorConceptCode}</small>
        </div>
    {/if}
    </div>



    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">{$actionLabel}</button>
                <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
            </div>
        </div>
    </div>
</form>

<br>
{if $id!='NEW'}
<h4>Customer Contacts <a data-toggle="tooltip" data-placement="top" title="add a new Contact to {$origSystemCustomerRef}" href="{base_url()}CustomerContact/createNew/{$id}" style="cursor:pointer"><small><i class="text-primary fas fa-plus-circle fa-sm fa-fw"></i></small></a></h4>
{if isset($contacts) && (sizeof($contacts)>0)}
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th scope="col"></th>
            <th>#</th>
            <th>Action</th>
            <th>Contact Reference</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$contacts item=c}
            <tr class="{if ($c.dismissed)} dismissed {elseif (($c.locallyValidated)&&($c.crossRefValidated)) } alert-success {else} alert-warning {/if} clickable-row" style="cursor:pointer" data-href='{base_url()}CustomerContact/customEditCleansed/{$c.country}/{$c.id}'>
                <th scope="row">
                    <a href="{base_url()}CustomerContact/editCleansed/{$c.id}" data-toggle="tooltip" data-placement="top" title="inspect all fields from contact {$c.origSystemAdressReference}"><i class="text-primary fas fa-edit fa-sm fa-fw"></i></a>
                    <a href="{base_url()}CustomerContact/toggle/{$c.id}" data-toggle="tooltip" data-placement="top" title="{if ($c.dismissed)}Recall{else}Dismiss{/if} {$c.lastName}"><i class="text-primary fas fa-minus-circle fa-sm fa-fw"></i></a>
                </th>
                <th scope="row">{$c.id}</th>
                <td>{$c.action}</td>
                <td>{$c.origSystemContactRef}</td>
                <td>{$c.contactFirstName}</td>
                <td>{$c.contactLastName}</td>
                <td>{$c.emailAddress}</td>
                <td>{$c.action}</td>

            </tr>
        {/foreach}
        </tbody>
    </table>
{else}
    <p class="p-3 alert-warning">There are no contacts at this address.  You may add a new contact <a data-toggle="tooltip" data-placement="top" title="add a new Contact to {$origSystemCustomerRef}" href="{base_url()}CustomerContact/createNew/{$id}">here</a></p>
{/if}
{/if}
<br/>
<br/>
<br/>
<br/>
<br/>
