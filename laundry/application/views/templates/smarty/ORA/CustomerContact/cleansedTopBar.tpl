<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><span class="flag-icon flag-icon-{strtolower($country)}" style="margin-top:10px;"></span>&nbsp;<span class="mt-2">{strtoupper($country)} {$title}</span>&nbsp;</h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}CustomerContact/editCleansed/{$id}" data-toggle="tooltip" data-placement="top" title="view ALL fields of customerContact {$origSystemContactRef}"><i class="fas fa-edit fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}CustomerContact/customEditCleansed/{$country}/{$id}" data-toggle="tooltip" data-placement="top" title="quick view of customerContact {$origSystemContactRef}"><i class="fas fa-eye fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}CustomerContact/validate/{$id}" data-toggle="tooltip" data-placement="top" title="manual validation of customerContact {$origSystemContactRef}"><i class="fas fa-check-circle fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-secondary" target="_self" href="{base_url()}CustomerContact/toggle/{$id}" data-toggle="tooltip" data-placement="top" title="{if ($dismissed)}Recall{else}Dismiss{/if} customerContact {$origSystemContactRef}"><i class="fas fa-minus-circle fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-success" target="_blank" href="{base_url()}IDL/Navigate/navigate/{$country}/Customers/CustomerContact" data-toggle="tooltip" data-placement="top" title="specification for customerContact"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-secondary" target="_self" href="{base_url()}{$cancelAction}" data-toggle="tooltip" data-placement="top" title="cancel, and go back"><i class="fas fa-ban fa-sm fa-fw"></i></a>
            </li>
        </ul>
        <div class="dropdown">
            <button class="btn btn-success  dropleft dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {$country} Customers
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="{base_url()}Customers/cleansed/{$country}">Cleansed</a>
                <a class="dropdown-item" href="{base_url()}Customers/dirty/{$country}">Dirty</a>
                <a class="dropdown-item" href="{base_url()}Maps/ORAGeoCodes">Oracle Geo Codes</a>
            </div>
        </div>
        &nbsp;
        <div class="dropdown">
            <button class="btn btn-success  dropleft dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Maintenance
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item text-info" data-toggle="modal" data-target="#dismissModalLong" style="cursor:pointer">{if ($dismissed)}Recall{else}Dismiss{/if}</a>
                <a class="dropdown-item text-danger" data-toggle="modal" data-target="#discardModalLong" style="cursor:pointer">Discard</a>
            </div>
        </div>
    </div>
</nav>


<div id="dismissModalLong" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="dismissModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="dismissModalLongLongTitle">Please Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"/>
            </div>
            <div class="modal-body">
                <p>
                    Please write down the reasons for {if ($dismissed)}recalling{else}dismissing{/if} this CustomerContact, please use the Observations field on the header.<br/>
                    Are you sure?
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No, forget about it.</button>
                <a href="{base_url()}CustomerContact/toggle/{$id}" class="btn btn-danger">Yes, {if ($dismissed)}recall{else}dismiss{/if} this CustomerContact</a>
            </div>
        </div>
    </div>
</div>
{if ($dismissed)}
    <div class="row">
        <div class="col-sm-12 p-2">
            <p class="dismissed text-center p-2">dismissed row, not going to flat file</p>
        </div>
    </div>
{else}
    <div class="row">
        <div class="col-sm-12 p-2">
            <p class="alert-success text-center p-2">active row</p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 p-2">
            {if (isset($cleansingAction)&&($cleansingAction!=''))}
                <p class="alert-warning text-center p-2">{$cleansingAction}</p>
            {else}
                <p class="alert-success text-center p-2">locally validated - no action required at header level</p>
            {/if}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 p-2">
            {if !($crossRefValidated)}
                <p class="alert-warning text-center p-2">problems in dependencies - check parent CustomerAddress or CustomerHeader</p>
            {else}
                <p class="alert-success text-center p-2">no problems, going to flat file</p>
            {/if}
        </div>
    </div>
{/if}


