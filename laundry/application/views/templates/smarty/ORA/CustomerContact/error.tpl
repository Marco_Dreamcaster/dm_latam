<h3>Multiple Instances of CustomerAddress {$origSystemAdressReference}</h3>
<br/>
<div class="row align-items-center">
    <div class="col-12">
        <p class="alert-danger p-2 text-center">{$message['PT']}</p>
        <p class="alert-danger p-2 text-center">{$message['EN']}</p>
    </div>

    {foreach $headerIds as $hId}
        <div class="col-12">
            <p class="text-center"><a class="btn btn-primary" href="{base_url()}CA/customEditCleansed/{$country}/{$hId}">go to CustomerAddress {$origSystemAdressReference}</a></p>
        </div>
    {/foreach}



</div>
<br/>
<br/>
<br/>
<br/>
<br/>
