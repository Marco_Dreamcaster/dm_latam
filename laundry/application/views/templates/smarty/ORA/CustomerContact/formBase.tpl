{if (isset($cleansingAction)&&($cleansingAction!=''))}
    <p class="alert-warning text-center">{$cleansingAction}</p>
{else}
    <p class="alert-success text-center">validated - no action required</p>
{/if}
<br/>
<form action="{base_url()}{$action}" method="post">
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>
    <div class="form-group">
        <label for="origSystemCustomerRef">Orig System Customer Ref <small id="origSystemCustomerRefHelp" class="form-text text-muted">(Original Customer Number from Legacy,  use the...)</small></label>
        <input type="text" class="form-control" id="origSystemCustomerRef" name="origSystemCustomerRef" aria-describedby="origSystemCustomerRefHelp" value="{$origSystemCustomerRef}" placeholder="Orig System Customer Ref...">
        <small id="errorOrigSystemCustomerRef" class="form-text text-danger">{$errorOrigSystemCustomerRef}</small>
    </div>
    <div class="form-group">
        <label for="origSystemAdressReference">Orig System Adress Reference <small id="origSystemAdressReferenceHelp" class="form-text text-muted">("Original  Customer address reference from...)</small></label>
        <input type="text" class="form-control" id="origSystemAdressReference" name="origSystemAdressReference" aria-describedby="origSystemAdressReferenceHelp" value="{$origSystemAdressReference}" placeholder="Orig System Adress Reference...">
        <small id="errorOrigSystemAdressReference" class="form-text text-danger">{$errorOrigSystemAdressReference}</small>
    </div>
    <div class="form-group">
        <label for="origSystemContactRef">Orig System Contact Ref <small id="origSystemContactRefHelp" class="form-text text-muted">(Please provide a unique Reference number to...)</small></label>
        <input type="text" class="form-control" id="origSystemContactRef" name="origSystemContactRef" aria-describedby="origSystemContactRefHelp" value="{$origSystemContactRef}" placeholder="Orig System Contact Ref...">
        <small id="errorOrigSystemContactRef" class="form-text text-danger">{$errorOrigSystemContactRef}</small>
    </div>
    <div class="form-group">
        <label for="origSystemTelephoneRef">Orig System Telephone Ref <small id="origSystemTelephoneRefHelp" class="form-text text-muted">(Please provide a unique Reference for the...)</small></label>
        <input type="text" class="form-control" id="origSystemTelephoneRef" name="origSystemTelephoneRef" aria-describedby="origSystemTelephoneRefHelp" value="{$origSystemTelephoneRef}" placeholder="Orig System Telephone Ref...">
        <small id="errorOrigSystemTelephoneRef" class="form-text text-danger">{$errorOrigSystemTelephoneRef}</small>
    </div>
    <div class="form-group">
        <label for="contactFirstName">Contact First Name <small id="contactFirstNameHelp" class="form-text text-muted">(Contact's first Name. For all construction...)</small></label>
        <input type="text" class="form-control" id="contactFirstName" name="contactFirstName" aria-describedby="contactFirstNameHelp" value="{$contactFirstName}" placeholder="Contact First Name...">
        <small id="errorContactFirstName" class="form-text text-danger">{$errorContactFirstName}</small>
    </div>
    <div class="form-group">
        <label for="contactLastName">Contact Last Name <small id="contactLastNameHelp" class="form-text text-muted">(Contact's last Name. For all construction...)</small></label>
        <input type="text" class="form-control" id="contactLastName" name="contactLastName" aria-describedby="contactLastNameHelp" value="{$contactLastName}" placeholder="Contact Last Name...">
        <small id="errorContactLastName" class="form-text text-danger">{$errorContactLastName}</small>
    </div>
    <div class="form-group">
        <label for="telephoneType">Telephone Type <small id="telephoneTypeHelp" class="form-text text-muted">(This is the Contact Point Type, which can be...)</small></label>
        <input type="text" class="form-control" id="telephoneType" name="telephoneType" aria-describedby="telephoneTypeHelp" value="{$telephoneType}" placeholder="Telephone Type...">
        <small id="errorTelephoneType" class="form-text text-danger">{$errorTelephoneType}</small>
    </div>
    <div class="form-group">
        <label for="phoneCountryCode">Phone Country Code <small id="phoneCountryCodeHelp" class="form-text text-muted">(Contry Code for international phone or fax Number.)</small></label>
        <input type="text" class="form-control" id="phoneCountryCode" name="phoneCountryCode" aria-describedby="phoneCountryCodeHelp" value="{$phoneCountryCode}" placeholder="Phone Country Code...">
        <small id="errorPhoneCountryCode" class="form-text text-danger">{$errorPhoneCountryCode}</small>
    </div>
    <div class="form-group">
        <label for="telephoneAreaCode">Telephone Area Code <small id="telephoneAreaCodeHelp" class="form-text text-muted">(Area Code for Phone or Fax number within a...)</small></label>
        <input type="text" class="form-control" id="telephoneAreaCode" name="telephoneAreaCode" aria-describedby="telephoneAreaCodeHelp" value="{$telephoneAreaCode}" placeholder="Telephone Area Code...">
        <small id="errorTelephoneAreaCode" class="form-text text-danger">{$errorTelephoneAreaCode}</small>
    </div>
    <div class="form-group">
        <label for="telephone">Telephone <small id="telephoneHelp" class="form-text text-muted">(Phone or Fax Number.)</small></label>
        <input type="text" class="form-control" id="telephone" name="telephone" aria-describedby="telephoneHelp" value="{$telephone}" placeholder="Telephone...">
        <small id="errorTelephone" class="form-text text-danger">{$errorTelephone}</small>
    </div>
    <div class="form-group">
        <label for="telephoneExtension">Telephone Extension <small id="telephoneExtensionHelp" class="form-text text-muted">(Phone extension)</small></label>
        <input type="text" class="form-control" id="telephoneExtension" name="telephoneExtension" aria-describedby="telephoneExtensionHelp" value="{$telephoneExtension}" placeholder="Telephone Extension...">
        <small id="errorTelephoneExtension" class="form-text text-danger">{$errorTelephoneExtension}</small>
    </div>
    <div class="form-group">
        <label for="emailAddress">Email Address <small id="emailAddressHelp" class="form-text text-muted">(When Telephone_Type = 'EMAIL',  provide the...)</small></label>
        <input type="text" class="form-control" id="emailAddress" name="emailAddress" aria-describedby="emailAddressHelp" value="{$emailAddress}" placeholder="Email Address...">
        <small id="errorEmailAddress" class="form-text text-danger">{$errorEmailAddress}</small>
    </div>
    <div class="form-group">
        <label for="jobTitle">Job Title <small id="jobTitleHelp" class="form-text text-muted">(As requested by Manu)</small></label>
        <input type="text" class="form-control" id="jobTitle" name="jobTitle" aria-describedby="jobTitleHelp" value="{$jobTitle}" placeholder="Job Title...">
        <small id="errorJobTitle" class="form-text text-danger">{$errorJobTitle}</small>
    </div>
    <div class="form-group">
        <label for="contactAttribute1">Contact Attribute1 <small id="contactAttribute1Help" class="form-text text-muted">("Invoice Delivery Method specified on Bill To...)</small></label>
        <input type="text" class="form-control" id="contactAttribute1" name="contactAttribute1" aria-describedby="contactAttribute1Help" value="{$contactAttribute1}" placeholder="Contact Attribute1...">
        <small id="errorContactAttribute1" class="form-text text-danger">{$errorContactAttribute1}</small>
    </div>
    <div class="form-group">
        <label for="orgName">Org Name <small id="orgNameHelp" class="form-text text-muted">(Name of operating unit)</small></label>
        <input type="text" class="form-control" id="orgName" name="orgName" aria-describedby="orgNameHelp" value="{$orgName}" placeholder="Org Name...">
        <small id="errorOrgName" class="form-text text-danger">{$errorOrgName}</small>
    </div>
    <br/>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>
</form>
<br/>
<br/>
<br/>
<br/>
<br/>
