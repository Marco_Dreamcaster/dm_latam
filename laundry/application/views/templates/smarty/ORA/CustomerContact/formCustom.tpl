<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-3 my-1">
            <small id="errorOrigSystemCustomerRef" class="form-text text-danger">&nbsp;{$errorOrigSystemCustomerRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Orig System Customer Ref </div>
                </div>
                <input type="text" class="form-control" id="origSystemCustomerRef" name="origSystemCustomerRef" value="{$origSystemCustomerRef}" placeholder="Orig System Customer Ref" disabled>
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <small id="errorOrigSystemAdressReference" class="form-text text-danger">&nbsp;{$errorOrigSystemAdressReference}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Orig System Adress Reference </div>
                </div>
                <input type="text" class="form-control" id="origSystemAdressReference" name="origSystemAdressReference" value="{$origSystemAdressReference}" placeholder="Orig System Adress Reference" disabled>
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <small id="errorOrigSystemContactRef" class="form-text text-danger">&nbsp;{$errorOrigSystemContactRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Orig System Contact Ref </div>
                </div>
                <input type="text" class="form-control" id="origSystemContactRef" name="origSystemContactRef" value="{$origSystemContactRef}" placeholder="Orig System Contact Ref"  disabled>
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <small id="errorOrigSystemTelephoneRef" class="form-text text-danger">&nbsp;{$errorOrigSystemTelephoneRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Orig System Telephone Ref </div>
                </div>
                <input type="text" class="form-control" id="origSystemTelephoneRef" name="origSystemTelephoneRef" value="{$origSystemTelephoneRef}" placeholder="Orig System Telephone Ref"  disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-3 my-1">
            <small id="errorContactFirstName" class="form-text text-danger">&nbsp;{$errorContactFirstName}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Contact First Name</div>
                </div>
                <input type="text" class="form-control" id="contactFirstName" name="contactFirstName" value="{$contactFirstName}" placeholder="Contact First Name">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <small id="errorContactLastName" class="form-text text-danger">&nbsp;{$errorContactLastName}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <div>Contact Last Name</div>
                    </div>
                </div>
                <input type="text" class="form-control" id="contactLastName" name="contactLastName" value="{$contactLastName}" placeholder="Contact Last Name">
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorJobTitle" class="form-text text-danger">&nbsp;{$errorJobTitle}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Job Title</div>
                </div>
                <input type="text" class="form-control" id="jobTitle" name="jobTitle" value="{$jobTitle}" placeholder="Job Title">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-3 my-1">
            <small id="errorTelephoneType" class="form-text text-danger">&nbsp;{$errorTelephoneType}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Telephone Type </div>
                </div>
                <select class="form-control" id="telephoneType" name="telephoneType" aria-describedby="$descriptor.shortNameHelp" placeholder="Telephone Type">
                    <option {if $telephoneType==='PHONE'}selected{/if}>PHONE</option>
                    <option {if $telephoneType==='FAX'}selected{/if}>FAX</option>
                    <option {if $telephoneType==='EMAIL'}selected{/if}>EMAIL</option>
                </select>
            </div>
        </div>
        <div class="col-sm-9 my-1">
            <small id="errorEmailAddress" class="form-text text-danger">&nbsp;{$errorEmailAddress}</small>
            <label class="sr-only" for="inlineFormInputEmailAddress">Email Address</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Email Address</div>
                </div>
                <input type="text" class="form-control" id="emailAddress" name="emailAddress" value="{$emailAddress}" placeholder="Email Address">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-3 my-1">
            <small id="errorPhoneCountryCode" class="form-text text-danger">&nbsp;{$errorPhoneCountryCode}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Phone Country Code</div>
                </div>
                <input type="text" class="form-control" id="phoneCountryCode" name="phoneCountryCode" value="{$phoneCountryCode}" placeholder="Phone Country Code">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <small id="errorTelephoneAreaCode" class="form-text text-danger">&nbsp;{$errorTelephoneAreaCode}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Telephone Area Code</div>
                </div>
                <input type="text" class="form-control" id="telephoneAreaCode" name="telephoneAreaCode" value="{$telephoneAreaCode}" placeholder="Telephone Area Code">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <small id="errorTelephoneAreaCode" class="form-text text-danger">&nbsp;{$errorTelephone}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Telephone</div>
                </div>
                <input type="text" class="form-control" id="telephone" name="telephone" value="{$telephone}" placeholder="Telephone">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <small id="errorTelephoneExtension" class="form-text text-danger">&nbsp;{$errorTelephoneExtension}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Telephone Extension</div>
                </div>
                <input type="text" class="form-control" id="telephoneExtension" name="telephoneExtension" value="{$telephoneExtension}" placeholder="Telephone Extension">
            </div>
        </div>
    </div>
    <br/>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">{$actionLabel}</button>
                <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
            </div>
        </div>
    </div>
</form>
<br/>
<br/>
<br/>
<br/>
<br/>
