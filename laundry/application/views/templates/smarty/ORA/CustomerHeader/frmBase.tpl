{if (isset($cleansingAction)&&($cleansingAction!=''))}
    <p class="alert-warning text-center">{$cleansingAction}</p>
{else}
    <p class="alert-success text-center">validated - no action required</p>
{/if}
<br/>
<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>
    <div class="form-group">
        <label for="customerName">Customer Name <small id="customerNameHelp" class="form-text text-muted">(Name of the customer)</small></label>
        <input type="text" class="form-control" id="customerName" name="customerName" aria-describedby="customerNameHelp" value="{$customerName}" placeholder="Customer Name...">
        <small id="errorCustomerName" class="form-text text-danger">{$errorCustomerName}</small>
    </div>
    <div class="form-group">
        <label for="origSystemPartyRef">Orig System Party Ref <small id="origSystemPartyRefHelp" class="form-text text-muted">(Original Customer Party Number from legacy,...)</small></label>
        <input type="text" class="form-control" id="origSystemPartyRef" name="origSystemPartyRef" aria-describedby="origSystemPartyRefHelp" value="{$origSystemPartyRef}" placeholder="Orig System Party Ref...">
        <small id="errorOrigSystemPartyRef" class="form-text text-danger">{$errorOrigSystemPartyRef}</small>
    </div>
    <div class="form-group">
        <label for="origSystemCustomerRef">Orig System Customer Ref <small id="origSystemCustomerRefHelp" class="form-text text-muted">(Customer Account Number, if the party has only...)</small></label>
        <input type="text" class="form-control" id="origSystemCustomerRef" name="origSystemCustomerRef" aria-describedby="origSystemCustomerRefHelp" value="{$origSystemCustomerRef}" placeholder="Orig System Customer Ref...">
        <small id="errorOrigSystemCustomerRef" class="form-text text-danger">{$errorOrigSystemCustomerRef}</small>
    </div>
    <div class="form-group">
        <label for="customerType">Customer Type <small id="customerTypeHelp" class="form-text text-muted">(Give INTERNAL (TKE affiliated & Associated...)</small></label>
        <input type="text" class="form-control" id="customerType" name="customerType" aria-describedby="customerTypeHelp" value="{$customerType}" placeholder="Customer Type...">
        <small id="errorCustomerType" class="form-text text-danger">{$errorCustomerType}</small>
    </div>
    <div class="form-group">
        <label for="taxRetention ">Tax Retention  <small id="taxRetention Help" class="form-text text-muted">(check eXceL specs for further details)</small></label>
        <input type="text" class="form-control" id="taxRetention " name="taxRetention " aria-describedby="taxRetention Help" value="{$taxRetention }" placeholder="Tax Retention ...">
        <small id="errorTaxRetention " class="form-text text-danger">{$errorTaxRetention }</small>
    </div>
    <div class="form-group">
        <label for="taxPayerId">Tax Payer Id <small id="taxPayerIdHelp" class="form-text text-muted">(main fiscal identification number NIT, RUC)</small></label>
        <input type="text" class="form-control" id="taxPayerId" name="taxPayerId" aria-describedby="taxPayerIdHelp" value="{$taxPayerId}" placeholder="Tax Payer Id...">
        <small id="errorTaxPayerId" class="form-text text-danger">{$errorTaxPayerId}</small>
    </div>
    <div class="form-group">
        <label for="customerProfile">Customer Profile <small id="customerProfileHelp" class="form-text text-muted">(This Customer profile will be populated in the...)</small></label>
        <input type="text" class="form-control" id="customerProfile" name="customerProfile" aria-describedby="customerProfileHelp" value="{$customerProfile}" placeholder="Customer Profile...">
        <small id="errorCustomerProfile" class="form-text text-danger">{$errorCustomerProfile}</small>
    </div>
    <div class="form-group">
        <label for="paymentMethodName">Payment Method Name <small id="paymentMethodNameHelp" class="form-text text-muted">(Specify the payment method the customer will use.)</small></label>
        <input type="text" class="form-control" id="paymentMethodName" name="paymentMethodName" aria-describedby="paymentMethodNameHelp" value="{$paymentMethodName}" placeholder="Payment Method Name...">
        <small id="errorPaymentMethodName" class="form-text text-danger">{$errorPaymentMethodName}</small>
    </div>
    <div class="form-group">
        <label for="primaryFlag">PrimaryFlag</label><br/>
        <small id="primaryFlagHelp" class="form-text text-muted">("Enter 'Y' or 'N' to indicate whether this is...)</small>
        <input type="checkbox" {($primaryFlag==true)?'checked':''} id="primaryFlag" name="primaryFlag">
    </div>
    <div class="form-group">
        <label for="globalFlag">GlobalFlag</label><br/>
        <small id="globalFlagHelp" class="form-text text-muted">("Y/N flag to indicate if this is a global...)</small>
        <input type="checkbox" {($globalFlag==true)?'checked':''} id="globalFlag" name="globalFlag">
    </div>
    <div class="form-group">
        <label for="globalAttribute9">Global Attribute9 <small id="globalAttribute9Help" class="form-text text-muted">(*Required by Argentina Only)</small></label>
        <input type="text" class="form-control" id="globalAttribute9" name="globalAttribute9" aria-describedby="globalAttribute9Help" value="{$globalAttribute9}" placeholder="Global Attribute9...">
        <small id="errorGlobalAttribute9" class="form-text text-danger">{$errorGlobalAttribute9}</small>
    </div>
    <div class="form-group">
        <label for="globalAttribute10">Global Attribute10 <small id="globalAttribute10Help" class="form-text text-muted">(Primary ID Type)</small></label>
        <input type="text" class="form-control" id="globalAttribute10" name="globalAttribute10" aria-describedby="globalAttribute10Help" value="{$globalAttribute10}" placeholder="Global Attribute10...">
        <small id="errorGlobalAttribute10" class="form-text text-danger">{$errorGlobalAttribute10}</small>
    </div>
    <div class="form-group">
        <label for="globalAttribute12">Global Attribute12 <small id="globalAttribute12Help" class="form-text text-muted">(Primary ID Validation Digit)</small></label>
        <input type="text" class="form-control" id="globalAttribute12" name="globalAttribute12" aria-describedby="globalAttribute12Help" value="{$globalAttribute12}" placeholder="Global Attribute12...">
        <small id="errorGlobalAttribute12" class="form-text text-danger">{$errorGlobalAttribute12}</small>
    </div>
    <div class="form-group">
        <label for="orgName">Org Name <small id="orgNameHelp" class="form-text text-muted">(Name of operating unit)</small></label>
        <input type="text" class="form-control" id="orgName" name="orgName" aria-describedby="orgNameHelp" value="{$orgName}" placeholder="Org Name...">
        <small id="errorOrgName" class="form-text text-danger">{$errorOrgName}</small>
    </div>
    <div class="form-group">
        <label for="classification">Classification <small id="classificationHelp" class="form-text text-muted">(check eXceL specs for further details)</small></label>
        <input type="text" class="form-control" id="classification" name="classification" aria-describedby="classificationHelp" value="{$classification}" placeholder="Classification...">
        <small id="errorClassification" class="form-text text-danger">{$errorClassification}</small>
    </div>
    <div class="form-group">
        <label for="segmentation">Segmentation <small id="segmentationHelp" class="form-text text-muted">(check eXceL specs for further details)</small></label>
        <input type="text" class="form-control" id="segmentation" name="segmentation" aria-describedby="segmentationHelp" value="{$segmentation}" placeholder="Segmentation...">
        <small id="errorSegmentation" class="form-text text-danger">{$errorSegmentation}</small>
    </div>
    <div class="form-group">
        <label for="aliases">Aliases <small id="aliasesHelp" class="form-text text-muted">(Aliases to this customer)</small></label>
        <input type="text" class="form-control" id="aliases" name="aliases" aria-describedby="aliasesHelp" value="{$aliases}" placeholder="Aliases...">
        <small id="errorAliases" class="form-text text-danger">{$errorAliases}</small>
    </div>
    <div class="form-group">
        <label for="fiscalClassCategory">Fiscal Class Category <small id="fiscalClassCategoryHelp" class="form-text text-muted">(Associated Fiscal Classification Type Code)</small></label>
        <input type="text" class="form-control" id="fiscalClassCategory" name="fiscalClassCategory" aria-describedby="fiscalClassCategoryHelp" value="{$fiscalClassCategory}" placeholder="Fiscal Class Category...">
        <small id="errorFiscalClassCategory" class="form-text text-danger">{$errorFiscalClassCategory}</small>
    </div>
    <div class="form-group">
        <label for="fiscalClassCode">Fiscal Class Code <small id="fiscalClassCodeHelp" class="form-text text-muted">(Associated Fiscal Classification Code)</small></label>
        <input type="text" class="form-control" id="fiscalClassCode" name="fiscalClassCode" aria-describedby="fiscalClassCodeHelp" value="{$fiscalClassCode}" placeholder="Fiscal Class Code...">
        <small id="errorFiscalClassCode" class="form-text text-danger">{$errorFiscalClassCode}</small>
    </div>
    <div class="form-group">
        <label for="fiscalClassStartDate">Fiscal Class Start Date <small id="fiscalClassStartDateHelp" class="form-text text-muted">(Effective start date of the Fiscal Classification)</small></label>
        <input class="form-control" id="fiscalClassStartDate" name="fiscalClassStartDate" aria-describedby="fiscalClassStartDateHelp" width="276" />
        <script>
            $('#fiscalClassStartDate').datepicker({
                uiLibrary: 'bootstrap4',
                format:'dd-mm-yyyy',
                value: '{$fiscalClassStartDate|date_format:"%d-%m-%Y"}'
            });
        </script>
        <small id="errorFiscalClassStartDate" class="form-text text-danger">{$errorFiscalClassStartDate}</small>
    </div>
    <div class="form-group">
        <label for="CODocumentType">Co Ext Attribute1 <small id="CODocumentTypeHelp" class="form-text text-muted">(Document Type)</small></label>
        <input type="text" class="form-control" id="CODocumentType" name="CODocumentType" aria-describedby="CODocumentTypeHelp" value="{$CODocumentType}" placeholder="Co Ext Attribute1...">
        <small id="errorCODocumentType" class="form-text text-danger">{$errorCODocumentType}</small>
    </div>
    <div class="form-group">
        <label for="COTaxPayerId">Co Ext Attribute2 <small id="COTaxPayerIdHelp" class="form-text text-muted">(CO Taxpayer ID)</small></label>
        <input type="text" class="form-control" id="COTaxPayerId" name="COTaxPayerId" aria-describedby="COTaxPayerIdHelp" value="{$COTaxPayerId}" placeholder="Co Ext Attribute2...">
        <small id="errorCOTaxPayerId" class="form-text text-danger">{$errorCOTaxPayerId}</small>
    </div>
    <div class="form-group">
        <label for="nature">Co Ext Attribute3 <small id="natureHelp" class="form-text text-muted">(legal or natural)</small></label>
        <input type="text" class="form-control" id="nature" name="nature" aria-describedby="natureHelp" value="{$nature}" placeholder="Co Ext Attribute3...">
        <small id="errorNature" class="form-text text-danger">{$errorNature}</small>
    </div>
    <div class="form-group">
        <label for="regimen">Co Ext Attribute4 <small id="regimenHelp" class="form-text text-muted">(simplified or custom)</small></label>
        <input type="text" class="form-control" id="regimen" name="regimen" aria-describedby="regimenHelp" value="{$regimen}" placeholder="Co Ext Attribute4...">
        <small id="errorRegimen" class="form-text text-danger">{$errorRegimen}</small>
    </div>

    <br/>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>
</form>
<br/>
<br/>
<br/>
<br/>
<br/>
