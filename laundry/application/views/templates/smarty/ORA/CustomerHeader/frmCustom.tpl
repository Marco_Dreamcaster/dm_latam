<br/>
<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <label class="sr-only" for="inlineFormInputOrigSystemPartyRef">Orig System Party Ref</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Orig System Party Ref</div>
                </div>
                <input type="text" class="form-control" id="origSystemPartyRef" name="origSystemPartyRef" value="{$origSystemPartyRef}" placeholder="Orig System Party Ref" disabled>
            </div>
            <small id="errorOrigSystemPartyRef" class="form-text text-danger">&nbsp;{$errorOrigSystemPartyRef}</small>
        </div>
        <div class="col-sm-1 col-md-6 my-1">
            <label class="sr-only" for="inlineFormInputOrigSystemCustomerRef">Orig System Customer Ref</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Orig System Customer Ref </div>
                </div>
                <input type="text" class="form-control" id="origSystemCustomerRef" name="origSystemCustomerRef" value="{$origSystemCustomerRef}" placeholder="Orig System Customer Ref" disabled>
            </div>
            <small id="errorOrigSystemCustomerRef" class="form-text text-danger">&nbsp;{$errorOrigSystemCustomerRef}</small>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <label class="sr-only" for="inlineFormInputCustomerName">Customer Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Customer Name</div>
                </div>
                <input type="text" class="form-control" id="customerName" name="customerName" value="{$customerName}" placeholder="Customer Name">
            </div>
            <small id="errorCustomerName" class="form-text text-danger">&nbsp;{$errorCustomerName}</small>
        </div>
        <div class="col-sm-1 col-md-6 my-1">
            <label class="sr-only" for="inlineFormInputCustomerProfile">Customer Profile</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Customer Profile</div>
                </div>
                <select class="form-control" id="customerProfile" name="customerProfile" aria-describedby="$descriptor.shortNameHelp" placeholder="Customer Profile">
                    <option {if $customerProfile==='TKE LATAM COMPANY GROUP'}selected{/if}>TKE LATAM COMPANY GROUP</option>
                    <option {if $customerProfile==='TKE LATAM GOVERNMENT'}selected{/if}>TKE LATAM GOVERNMENT</option>
                    <option {if $customerProfile==='TKE LATAM DISTRIBUTOR'}selected{/if}>TKE LATAM DISTRIBUTOR</option>
                    <option {if $customerProfile==='TKE LATAM INTERCOMPANY'}selected{/if}>TKE LATAM INTERCOMPANY</option>
                    <option {if $customerProfile==='TKE LATAM STANDARD'}selected{/if}>TKE LATAM STANDARD</option>
                    <option {if $customerProfile==='TKE LATAM STANDARD'}selected{/if}>TKE LATAM VIP</option>

                </select>
            </div>
            <small id="errorCustomerProfile" class="form-text text-danger">&nbsp;{$errorCustomerProfile}</small>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-4 my-1">
            <label class="sr-only" for="inlineFormInputClassification ">Classification</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Classification</div>
                </div>
                <select class="form-control" id="classification" name="classification" aria-describedby="$descriptor.classificationHelp">
                    <option value="">--</option>
                    <option {if $classification ==='Private/Commercial companies'}selected{/if}>Private/Commercial companies</option>
                    <option {if $classification ==='Holdings/Company group'}selected{/if}>Holdings/Company group</option>
                    <option {if $classification ==='Government owned companies'}selected{/if}>Government owned companies</option>
                    <option {if $classification ==='Private individuals'}selected{/if}>Private individuals</option>
                    <option {if $classification ==='Association'}selected{/if}>Association</option>
                    <option {if $classification ==='Government'}selected{/if}>Government</option>
                    <option {if $classification ==='Federal'}selected{/if}>Federal</option>
                    <option {if $classification ==='Public Sector Companies'}selected{/if}>Public Sector Companies</option>
                </select>
            </div>
            <small id="errorClassification" class="form-text text-danger">&nbsp;{$errorClassification}</small>
        </div>
        <div class="col-sm-1 col-md-4 my-1">
            <label class="sr-only" for="inlineFormInputOldClassification">Old Classification</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Old Classification </div>
                </div>
                <input type="text" class="form-control" id="oldClassification" name="oldClassification" value="{$classification}" placeholder="Old Classification">
            </div>
            <small id="errorOldClassification" class="form-text text-danger">&nbsp;{$errorOldClassification}</small>
        </div>
        <div class="col-sm-1 col-md-4 my-1">
            <label class="sr-only" for="inlineFormInputCustomerType">Customer Type</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Customer Type</div>
                </div>
                <select class="form-control" id="customerType" name="customerType" aria-describedby="$descriptor.shortNameHelp" placeholder="Customer Type">
                    <option {if $customerType==='INTERNAL'}selected{/if}>INTERNAL</option>
                    <option {if $customerType==='EXTERNAL'}selected{/if}>EXTERNAL</option>

                </select>
            </div>
            <small id="errorCustomerProfile" class="form-text text-danger">&nbsp;{$errorCustomerProfile}</small>
        </div>
    </div>

    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-12 my-1">
            <label class="sr-only" for="inlineFormInputTaxPayerId">Tax Payer Id</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Tax Payer Id </div>
                </div>
                <input type="text" class="form-control" id="taxPayerId" name="taxPayerId" value="{$taxPayerId}" placeholder="Tax Payer Id">
            </div>
            <small id="errorTaxPayerId" class="form-text text-danger">&nbsp;{$errorTaxPayerId}</small>
        </div>
    </div>

    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <label class="sr-only" for="inlineFormFiscalClassCode">Fiscal Classification Type Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Type Code</div>
                </div>
                <select class="form-control" id="fiscalClassCode" name="fiscalClassCode" aria-describedby="fiscalClassCodeHelp" placeholder="Fiscal Class Code">
                    <option value="">--</option>
                    <option {if $fiscalClassCode ==='TKE_LATAM_PA_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_PA_CONTRIBUTOR">TKE_LATAM_PA_CONTRIBUTOR</option>
                </select>
            </div>
            <small id="errorFiscalClassCode" class="form-text text-danger">&nbsp;{$errorFiscalClassCode}</small>
        </div>
        <div class="col-sm-1 col-md-6 my-1">
            <label class="sr-only" for="inlineFormFiscalClassCategory">Fiscal Classification Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Code</div>
                </div>
                <select class="form-control" id="fiscalClassCategory" name="fiscalClassCategory" aria-describedby="fiscalClassCategoryHelp" placeholder="Fiscal Class Category">
                    <option value="">--</option>
                    <option {if $fiscalClassCategory ==='AUTORETENEDOR'}selected{/if} value="AUTORETENEDOR">AUTORETENEDOR</option>
                    <option {if $fiscalClassCategory ==='CONTRIBUYENTE'}selected{/if} value="CONTRIBUYENTE">CONTRIBUYENTE</option>
                    <option {if $fiscalClassCategory ==='EXENTO'}selected{/if} value="EXENTO">EXENTO</option>
                </select>
            </div>
            <small id="errorFiscalClassCategory" class="form-text text-danger">&nbsp;{$errorFiscalClassCategory}</small>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <label class="sr-only" for="inlineFormFiscalClassStartDate">Fiscal Classification Start Date</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Start Date</div>
                </div>
                <input class="form-control" id="fiscalClassStartDate" name="fiscalClassStartDate" aria-describedby="fiscalClassStartDateHelp"/>
                <script>
                    $('#fiscalClassStartDate').datepicker({
                        uiLibrary: 'bootstrap4',
                        format:'dd-mm-yyyy',
                        value: '{$fiscalClassStartDate|date_format:"%d-%m-%Y"}'
                    });
                </script>
            </div>
            <small id="errorRegimen" class="form-text text-danger">&nbsp;{$errorRegimen}</small>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-4 my-1">
            <label class="sr-only" for="inlineFormInputSpecialCustomer">Special Customer</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Special Customer <small id="errorSpecialCustomer" class="form-text text-danger">&nbsp;{$errorSpecialCustomer}</small></div>
                </div>
                <input type="checkbox" {($specialCustomer==true)?'checked':''} style="margin:10px;" id="specialCustomer" name="specialCustomer">
            </div>
            <small id="errorSpecialCustomer" class="form-text text-danger">&nbsp;{$errorSpecialCustomer}</small>
        </div>
        <div class="col-sm-1 col-md-4 my-1">
            <label class="sr-only" for="inlineFormInputPrimaryFlag">Primary Flag</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Primary Flag</div>
                </div>
                <input type="checkbox" {($primaryFlag==true)?'checked':''} style="margin:10px;" id="primaryFlag" name="primaryFlag">
            </div>
            <small id="errorPrimaryFlag" class="form-text text-danger">&nbsp;{$errorPrimaryFlag}</small>
        </div>
        <div class="col-sm-1 col-md-4 my-1">
            <label class="sr-only" for="inlineFormInputGlobalFlag">Global Flag</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Global Flag </div>
                </div>
                <input type="checkbox" {($globalFlag==true)?'checked':''}  style="margin:10px;" id="globalFlag" name="globalFlag">
            </div>
            <small id="errorGlobalFlag" class="form-text text-danger">&nbsp;{$errorGlobalFlag}</small>
        </div>
    </div>
    <div class="form-row align-items-center" id="paymentMethodNameFormRow">
        <div class="col-sm-6 my-1">
            <label class="sr-only" for="inlineFormInputPaymentMethodName">Payment Method Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-danger">Payment Method Name</div>
                </div>
                <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                    <option {if $paymentMethodName==='PAYMENT_METHOD_1'}selected{/if} value="PAYMENT_METHOD_1">PAYMENT_METHOD_1</option>
                    <option {if $paymentMethodName==='PAYMENT_METHOD_2'}selected{/if} value="PAYMENT_METHOD_2">PAYMENT_METHOD_2</option>
                </select>

            </div>
            <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorAliases" class="form-text text-danger">&nbsp;{$errorAliases}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Aliases</div>
                </div>
                <textarea type="text" class="form-control" id="aliases" name="aliases" value="" placeholder="Aliases">{$aliases}</textarea>
            </div>
            <label class="alert-info text-info"><i class="fas fa-id-card fa-sm fa-fw"></i> An alias is an alternative ID fot this item.  Use this field to unify records to be migrated to ORACLE. Use a comma-separated list, with no spaces (e.g. PA1000,PA1002,CO1003)</label>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorObservations" class="form-text text-danger">&nbsp;{$errorObservations}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Observations</div>
                </div>
                <textarea type="text" class="form-control" id="observations" name="observations" value="" placeholder="Observations" rows="5">{$observations}</textarea>
            </div>
            <label class="alert-info text-info"><i class="fas fa-bug fa-lg fa-fw"></i> Please help us detect bugs.  Use this field to annotate special cases, cleansing decisions, and relevant issues related to this record.</label>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">{$actionLabel}</button>
                <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
            </div>
        </div>
    </div>
</form>

<br>
<h4>Customer Addresses &nbsp;<a data-toggle="tooltip" data-placement="top" title="add a new Address to {$origSystemCustomerRef}" href="{base_url()}CA/createNew/{$id}" style="cursor:pointer"><small><i class="text-primary fas fa-plus-circle fa-sm fa-fw"></i></small></a></h4>
{if isset($addresses) && (sizeof($addresses)>0)}
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th scope="col"></th>
            <th>#</th>
            <th>Action</th>
            <th>Address Ref</th>
            <th>Location</th>
            <th>Primary Site</th>
            <th>Site Use</th>
            <th>Address Line 1</th>
            <th>City</th>
            <th>Province</th>
            <th>State</th>
            <th>Country</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$addresses item=a}
            <tr class="{if ($a.dismissed)} dismissed {elseif (($a.locallyValidated)&&($a.crossRefValidated)) } alert-success {else} alert-warning {/if} clickable-row" style="cursor:pointer" data-href='{base_url()}CA/customEditCleansed/{$a.country}/{$a.id}'>
                <th scope="row">
                    <a href="{base_url()}CA/editCleansed/{$a.id}" data-toggle="tooltip" data-placement="top" title="inspect all fields from address {$a.origSystemAdressReference}"><i class="text-primary fas fa-edit fa-sm fa-fw"></i></a>
                    <a href="{base_url()}CA/toggle/{$a.id}" data-toggle="tooltip" data-placement="top" title="{if ($c.dismissed)}Recall{else}Dismiss{/if} {$a.origSystemAdressReference}"><i class="text-primary fas fa-minus-circle fa-sm fa-fw"></i></a>
                </th>
                <th scope="row">{$a.id}</th>
                <td>{$a.action|truncate:40}</td>
                <td>{$a.origSystemAdressReference}</td>
                <td>{$a.location}</td>
                <td>{($a.primarySiteUseFlag===true)?'Y':'N'}</td>
                <td>{$a.siteUseCode}</td>
                <td>{$a.addressLine1|truncate:20}</td>
                <td>{$a.city}</td>
                <td>{$a.province}</td>
                <td>{$a.state}</td>
                <td>{$a.addressCountry}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{else}
    <p class="p-3 alert-warning">There are no addresses at this customer.  You may add a new address <a data-toggle="tooltip" data-placement="top" title="add a new Address to {$origSystemCustomerRef}" href="{base_url()}CA/createNew/{$id}">here</a></p>
{/if}
<br/>
<br/>
<br/>
<br/>
<br/>

