<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorOrigSystemPartyRef" class="form-text text-danger">&nbsp;{$errorOrigSystemPartyRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Orig System Party Ref</div>
                </div>
                <input type="text" class="form-control" id="origSystemPartyRef" name="origSystemPartyRef" value="{$origSystemPartyRef}" placeholder="Orig System Party Ref" disabled>
            </div>
        </div>
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorOrigSystemCustomerRef" class="form-text text-danger">&nbsp;{$errorOrigSystemCustomerRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Orig System Customer Ref </div>
                </div>
                <input type="text" class="form-control" id="origSystemCustomerRef" name="origSystemCustomerRef" value="{$origSystemCustomerRef}" placeholder="Orig System Customer Ref" disabled>
            </div>
        </div>
    </div>
    {if $dismissed}
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorBillToMigratedRef" class="form-text text-danger">&nbsp;{$errorBillToMigratedRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-danger">Migrated BillTo Ref</div>
                </div>
                <input type="text" class="form-control" id="billToMigratedRef" name="billToMigratedRef" value="{$billToMigratedRef}">
            </div>
        </div>
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorShipToMigratedRef" class="form-text text-danger">&nbsp;{$errorShipToMigratedRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-danger">Migrated ShipTo Ref</div>
                </div>
                <input type="text" class="form-control" id="shipToMigratedRef" name="shipToMigratedRef" value="{$shipToMigratedRef}">
            </div>
        </div>
    </div>
    {else}
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorCustomerName" class="form-text text-danger">&nbsp;{$errorCustomerName}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Customer Name</div>
                </div>
                <input type="text" class="form-control" id="customerName" name="customerName" value="{$customerName}" placeholder="Customer Name">
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorCustomerProfile" class="form-text text-danger">&nbsp;{$errorCustomerProfile}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Customer Profile </div>
                </div>
                <select class="form-control" id="customerProfile" name="customerProfile" aria-describedby="$descriptor.shortNameHelp" placeholder="Customer Profile">
                    <option {if $customerProfile==='TKE LATAM COMPANY GROUP'}selected{/if}>TKE LATAM COMPANY GROUP</option>
                    <option {if $customerProfile==='TKE LATAM GOVERNMENT'}selected{/if}>TKE LATAM GOVERNMENT</option>
                    <option {if $customerProfile==='TKE LATAM DISTRIBUTOR'}selected{/if}>TKE LATAM DISTRIBUTOR</option>
                    <option {if $customerProfile==='TKE LATAM INTERCOMPANY'}selected{/if}>TKE LATAM INTERCOMPANY</option>
                    <option {if $customerProfile==='TKE LATAM STANDARD'}selected{/if}>TKE LATAM STANDARD</option>
                    <option {if $customerProfile==='TKE LATAM VIP'}selected{/if}>TKE LATAM VIP</option>
                </select>

            </div>
        </div>
    </div>
    {if ( $country=='AR')}
        <div class="form-row align-items-center">
            <div class="col-sm-4 my-1">
                <small id="errorOriginAll" class="form-text text-danger">&nbsp;{$errorOriginAll}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Origin</div>
                    </div>
                    <select class="form-control" id="originAll" name="originAll">
                        <option {if $originAll==='Domestic Origin'}selected{/if}>Domestic Origin</option>
                        <option {if $originAll==='Foreign Origin'}selected{/if}>Foreign Origin</option>
                    </select>

                </div>
            </div>
            <div class="col-sm-4 my-1">
                <small id="errorGlobalAttribute9" class="form-text text-danger">&nbsp;{$errorGlobalAttribute9}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Origin <span class="flag-icon flag-icon-ar mr-1 ml-1" title="AR"></span></div>
                    </div>
                    <select class="form-control" id="globalAttribute9" name="globalAttribute9">
                        <option {if $globalAttribute9==='DOMESTIC_ORIGIN'}selected{/if}>DOMESTIC_ORIGIN</option>
                        <option {if $globalAttribute9==='FOREIGN_ORIGIN'}selected{/if}>FOREIGN_ORIGIN</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4 my-1">
                <small id="errorNatureAll" class="form-text text-danger">&nbsp;{$errorNatureAll}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Nature </div>
                    </div>
                    <select class="form-control" id="natureAll" name="natureAll">
                        <option {if $natureAll==='Legal Entity'}selected{/if}>Legal Entity</option>
                        <option {if $natureAll==='Natural Person'}selected{/if}>Natural Person</option>
                    </select>

                </div>
            </div>
        </div>
    {else}
        <div class="form-row align-items-center">
            <div class="col-sm-6 my-1">
                <small id="errorOriginAll" class="form-text text-danger">&nbsp;{$errorOriginAll}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Origin</div>
                    </div>
                    <select class="form-control" id="originAll" name="originAll">
                        <option {if $originAll==='Domestic Origin'}selected{/if}>Domestic Origin</option>
                        <option {if $originAll==='Foreign Origin'}selected{/if}>Foreign Origin</option>
                    </select>

                </div>
            </div>
            <div class="col-sm-6 my-1">
                <small id="errorNatureAll" class="form-text text-danger">&nbsp;{$errorNatureAll}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Nature </div>
                    </div>
                    <select class="form-control" id="natureAll" name="natureAll">
                        <option {if $natureAll==='Legal Entity'}selected{/if}>Legal Entity</option>
                        <option {if $natureAll==='Natural Person'}selected{/if}>Natural Person</option>
                    </select>

                </div>
            </div>
        </div>
    {/if}

    <div class="form-row align-items-center">
        {if ( $country=='AR' || $country=='CL' || $country=='CO')}
            <div class="col-sm-12 my-1">
                <small id="errorTaxPayerId" class="form-text">&nbsp;{$errorTaxPayerId}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Tax Payer ID (NUM_1099) (auto) &nbsp;
                            <span class="flag-icon flag-icon-co mr-1 ml-1" title="CO"></span>
                            <span class="flag-icon flag-icon-ar mr-1 ml-1" title="AR"></span>
                            <span class="flag-icon flag-icon-cl mr-1 ml-1" title="CL"></span>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="taxPayerId" name="taxPayerId" value="{$taxPayerId}" readonly>
                </div>
            </div>
            <div class="col-sm-8 my-1">
                <small id="errorcoCOTaxPayerId" class="form-text text-danger">&nbsp;{$errorCOTaxPayerId}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Tax Payer ID (no digit)
                            <span class="flag-icon flag-icon-co mr-1 ml-1" title="CO"></span>
                            <span class="flag-icon flag-icon-ar mr-1 ml-1" title="AR"></span>
                            <span class="flag-icon flag-icon-cl mr-1 ml-1" title="CL"></span>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="COTaxPayerId" name="COTaxPayerId" value="{$COTaxPayerId}">
                </div>
            </div>
            <div class="col-sm-4 my-1">
                <small id="errorGlobalAttribute12" class="form-text text-danger">&nbsp;{$errorGlobalAttribute12}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Tax Payer ID Digit
                            <span class="flag-icon flag-icon-co mr-1 ml-1" title="CO"></span>
                            <span class="flag-icon flag-icon-ar mr-1 ml-1" title="AR"></span>
                            <span class="flag-icon flag-icon-cl mr-1 ml-1" title="CL"></span>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="globalAttribute12" name="globalAttribute12" value="{$globalAttribute12}">
                </div>
            </div>
        {else}
            <div class="col-sm-12 my-1">
                <small id="errorTaxPayerId" class="form-text">&nbsp;{$errorTaxPayerId}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Tax Payer ID (JGZZ_FISCAL_CODE) &nbsp;
                            <span class="flag-icon flag-icon-pa mr-1 ml-1" title="PA"></span>
                            <span class="flag-icon flag-icon-mx mr-1 ml-1" title="MX"></span>
                            <span class="flag-icon flag-icon-cr mr-1 ml-1" title="CR"></span>
                            <span class="flag-icon flag-icon-hn mr-1 ml-1" title="HN"></span>
                            <span class="flag-icon flag-icon-ni mr-1 ml-1" title="NI"></span>
                            <span class="flag-icon flag-icon-gt mr-1 ml-1" title="GT"></span>
                            <span class="flag-icon flag-icon-sv mr-1 ml-1" title="SV"></span>
                            <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                            <span class="flag-icon flag-icon-uy mr-1 ml-1" title="UY"></span>
                            <span class="flag-icon flag-icon-py mr-1 ml-1" title="PY"></span>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="taxPayerId" name="taxPayerId" value="{$taxPayerId}" placeholder="">
                </div>
            </div>
        {/if}
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorClassification" class="form-text text-danger">&nbsp;{$errorCustomerClassCode}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Classification (Customer Class Code)</div>
                </div>
                <select class="form-control" id="customerClassCode" name="customerClassCode" aria-describedby="$descriptor.classificationHelp">
                    <option {if $customerClassCode=='01'}selected{/if} value='01'>01 Private / Commercial companies</option>
                    <option {if $customerClassCode=='02'}selected{/if} value='02'>02 Holdings / Company group</option>
                    <option {if $customerClassCode=='03'}selected{/if} value='03'>03 Government owned companies</option>
                    <option {if $customerClassCode=='04'}selected{/if} value='04'>04 Private individuals</option>
                    <option {if $customerClassCode=='05'}selected{/if} value='05'>05 Association</option>
                    <option {if $customerClassCode=='06'}selected{/if} value='06'>06 Government</option>
                    <option {if $customerClassCode=='FEDERAL'}selected{/if} value='FEDERAL'>Federal</option>
                    <option {if $customerClassCode=='PUBLIC SECTOR COMPANIES'}selected{/if} value='PUBLIC SECTOR COMPANIES'>Public Sector Companies</option>
                </select>
            </div>
        </div>
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorCustomerProfile" class="form-text text-danger">&nbsp;{$errorCustomerProfile}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Customer Type</div>
                </div>
                <select class="form-control" id="customerType" name="customerType" aria-describedby="$descriptor.shortNameHelp" placeholder="Customer Type">
                    <option value="">--</option>
                    <option {if $customerType==='INTERNAL'}selected{/if}>INTERNAL</option>
                    <option {if $customerType==='EXTERNAL'}selected{/if}>EXTERNAL</option>

                </select>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorPaymentTermName" class="form-text text-danger">&nbsp;{$errorPaymentTermName}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Payment Term Name</div>
                </div>
                <select class="form-control" id="paymentTermName" name="paymentTermName">
                    <option {if $paymentTermName==='DAY 05'}selected{/if} value='DAY 05'>DAY 05</option>
                    <option {if $paymentTermName==='IMMEDIATE'}selected{/if} value='IMMEDIATE'>IMMEDIATE</option>
                    <option {if $paymentTermName==='30 NET'}selected{/if} value='30 NET'>30 NET</option>
                    <option {if $paymentTermName==='30-120 FIX10'}selected{/if} value='30-120 FIX10'>30-120 FIX10</option>
                    <option {if $paymentTermName==='30-120 FIX15'}selected{/if} value='30-120 FIX15'>30-120 FIX15</option>
                    <option {if $paymentTermName==='30-120 FIX20'}selected{/if} value='30-120 FIX20'>30-120 FIX20</option>
                    <option {if $paymentTermName==='30-120 FIX5'}selected{/if} value='30-120 FIX5'>30-120 FIX5</option>
                    <option {if $paymentTermName==='30-120 SPLIT'}selected{/if} value='30-120 SPLIT'>30-120 SPLIT</option>
                    <option {if $paymentTermName==='30-150 FIX10'}selected{/if} value='30-150 FIX10'>30-150 FIX10</option>
                    <option {if $paymentTermName==='30-150 FIX15'}selected{/if} value='30-150 FIX15'>30-150 FIX15</option>
                    <option {if $paymentTermName==='30-150 FIX20'}selected{/if} value='30-150 FIX20'>30-150 FIX20</option>
                    <option {if $paymentTermName==='30-150 FIX5'}selected{/if} value='30-150 FIX5'>30-150 FIX5</option>
                    <option {if $paymentTermName==='30-150 SPLIT'}selected{/if} value='30-150 SPLIT'>30-150 SPLIT</option>
                    <option {if $paymentTermName==='30-180 FIX10'}selected{/if} value='30-180 FIX10'>30-180 FIX10</option>
                    <option {if $paymentTermName==='30-180 FIX15'}selected{/if} value='30-180 FIX15'>30-180 FIX15</option>
                    <option {if $paymentTermName==='30-180 FIX20'}selected{/if} value='30-180 FIX20'>30-180 FIX20</option>
                    <option {if $paymentTermName==='30-180 FIX5'}selected{/if} value='30-180 FIX5'>30-180 FIX5</option>
                    <option {if $paymentTermName==='30-180 SPLIT'}selected{/if} value='30-180 SPLIT'>30-180 SPLIT</option>
                    <option {if $paymentTermName==='30-210 FIX10'}selected{/if} value='30-210 FIX10'>30-210 FIX10</option>
                    <option {if $paymentTermName==='30-210 FIX15'}selected{/if} value='30-210 FIX15'>30-210 FIX15</option>
                    <option {if $paymentTermName==='30-210 FIX20'}selected{/if} value='30-210 FIX20'>30-210 FIX20</option>
                    <option {if $paymentTermName==='30-210 FIX5'}selected{/if} value='30-210 FIX5'>30-210 FIX5</option>
                    <option {if $paymentTermName==='30-210 SPLIT'}selected{/if} value='30-210 SPLIT'>30-210 SPLIT</option>
                    <option {if $paymentTermName==='30-240 FIX10'}selected{/if} value='30-240 FIX10'>30-240 FIX10</option>
                    <option {if $paymentTermName==='30-240 FIX15'}selected{/if} value='30-240 FIX15'>30-240 FIX15</option>
                    <option {if $paymentTermName==='30-240 FIX20'}selected{/if} value='30-240 FIX20'>30-240 FIX20</option>
                    <option {if $paymentTermName==='30-240 FIX5'}selected{/if} value='30-240 FIX5'>30-240 FIX5</option>
                    <option {if $paymentTermName==='30-240 SPLIT'}selected{/if} value='30-240 SPLIT'>30-240 SPLIT</option>
                    <option {if $paymentTermName==='30-60 FIX10'}selected{/if} value='30-60 FIX10'>30-60 FIX10</option>
                    <option {if $paymentTermName==='30-60 FIX15'}selected{/if} value='30-60 FIX15'>30-60 FIX15</option>
                    <option {if $paymentTermName==='30-60 FIX20'}selected{/if} value='30-60 FIX20'>30-60 FIX20</option>
                    <option {if $paymentTermName==='30-60 FIX5'}selected{/if} value='30-60 FIX5'>30-60 FIX5</option>
                    <option {if $paymentTermName==='30-60 SPLIT'}selected{/if} value='30-60 SPLIT'>30-60 SPLIT</option>
                    <option {if $paymentTermName==='30-90 FIX10'}selected{/if} value='30-90 FIX10'>30-90 FIX10</option>
                    <option {if $paymentTermName==='30-90 FIX15'}selected{/if} value='30-90 FIX15'>30-90 FIX15</option>
                    <option {if $paymentTermName==='30-90 FIX20'}selected{/if} value='30-90 FIX20'>30-90 FIX20</option>
                    <option {if $paymentTermName==='30-90 FIX5'}selected{/if} value='30-90 FIX5'>30-90 FIX5</option>
                    <option {if $paymentTermName==='DAY 01'}selected{/if} value='DAY 01'>DAY 01</option>
                    <option {if $paymentTermName==='DAY 10'}selected{/if} value='DAY 10'>DAY 10</option>
                    <option {if $paymentTermName==='DAY 15'}selected{/if} value='DAY 15'>DAY 15</option>
                    <option {if $paymentTermName==='DAY 20'}selected{/if} value='DAY 20'>DAY 20</option>
                    <option {if $paymentTermName==='DAY 25'}selected{/if} value='DAY 25'>DAY 25</option>
                    <option {if $paymentTermName==='DAY 30'}selected{/if} value='DAY 30'>DAY 30</option>
                    <option {if $paymentTermName==='NET 10'}selected{/if} value='NET 10'>NET 10</option>
                    <option {if $paymentTermName==='SIIF CREDIT'}selected{/if} value='SIIF CREDIT'>SIIF CREDIT</option>
                </select>
            </div>
        </div>
        {if ($country=='PE')}
            <div class="col-sm-6 my-1">
                <small id="errorPerceptionAgentPE" class="form-text text-danger">&nbsp;{$errorPerceptionPE}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Perception Agent </div>
                    </div>
                    <select class="form-control" id="perceptionAgentPE" name="perceptionAgentPE">
                        <option {if $perceptionAgentPE==='N'}selected{/if}>N</option>
                        <option {if $perceptionAgentPE==='Y'}selected{/if}>Y</option>
                    </select>
                </div>
            </div>
        {/if}
        {if ($country=='UY')}
            <div class="col-sm-6 my-1">
                <small id="errorDocumentTypeUY" class="form-text text-danger">&nbsp;{$errorDocumentTypeUY}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Document Type </div>
                    </div>
                    <select class="form-control" id="documentTypeUY" name="documentTypeUY">
                        <option {if $documentTypeUY==='2'}selected{/if} value='2'>RUC</option>
                        <option {if $documentTypeUY==='3'}selected{/if} value='3'>CI</option>
                        <option {if $documentTypeUY==='4'}selected{/if} value='4'>Others</option>
                        <option {if $documentTypeUY==='5'}selected{/if} value='5'>Passport</option>
                        <option {if $documentTypeUY==='6'}selected{/if} value='6'>DNI</option>
                        <option {if $documentTypeUY==='7'}selected{/if} value='7'>NIFE</option>
                    </select>
                </div>
            </div>
        {/if}
    </div>

    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorFiscalClassCode" class="form-text text-danger">&nbsp;{$errorFiscalClassCategory}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Type Code</div>
                </div>
                <select class="form-control" id="fiscalClassCategory " name="fiscalClassCategory">
                    <option {if $fiscalClassCategory =='TKE_LATAM_CO_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_CO_CONTRIBUTOR">TKE_LATAM_CO_CONTRIBUTOR</option>
                    <option {if $fiscalClassCategory =='TKE_LATAM_MX_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_MX_CONTRIBUTOR">TKE_LATAM_MX_CONTRIBUTOR</option>
                    <option {if $fiscalClassCategory =='TKE_LATAM_HN_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_HN_CONTRIBUTOR">TKE_LATAM_HN_CONTRIBUTOR</option>
                    <option {if $fiscalClassCategory =='TKE_LATAM_NI_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_NI_CONTRIBUTOR">TKE_LATAM_NI_CONTRIBUTOR</option>
                    <option {if $fiscalClassCategory =='TKE_LATAM_CR_CONTRIBUTOR2'}selected{/if} value="TKE_LATAM_CR_CONTRIBUTOR2">TKE_LATAM_CR_CONTRIBUTOR2</option>
                    <option {if $fiscalClassCategory =='TKE_LATAM_GT_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_GT_CONTRIBUTOR">TKE_LATAM_GT_CONTRIBUTOR</option>
                    <option {if $fiscalClassCategory =='TKE_LATAM_SV_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_SV_CONTRIBUTOR">TKE_LATAM_SV_CONTRIBUTOR</option>
                    <option {if $fiscalClassCategory =='TKE_LATAM_CL_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_CL_CONTRIBUTOR">TKE_LATAM_CL_CONTRIBUTOR</option>
                    <option {if $fiscalClassCategory =='TKE_LATAM_PE_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_PE_CONTRIBUTOR">TKE_LATAM_PE_CONTRIBUTOR</option>
                    <option {if $fiscalClassCategory =='TKE_LATAM_AR_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_AR_CONTRIBUTOR">TKE_LATAM_AR_CONTRIBUTOR</option>
                    <option {if $fiscalClassCategory =='TKE_LATAM_PY_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_PY_CONTRIBUTOR">TKE_LATAM_PY_CONTRIBUTOR</option>
                    <option {if $fiscalClassCategory =='TKE_LATAM_UY_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_UY_CONTRIBUTOR">TKE_LATAM_UY_CONTRIBUTOR</option>
                </select>
            </div>
        </div>

        {if $country=='CO'}
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorFiscalClassCode" class="form-text text-danger">&nbsp;{$errorFiscalClassCode}</small>
            <label class="sr-only" for="inlineFormInputFiscalClassCode">Fiscal Classification Code  <span class="flag-icon flag-icon-co mr-2 ml-2" title="CO only"></span></label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Code <span class="flag-icon flag-icon-co mr-2 ml-2" title="CO"></span></div>
                </div>
                <select class="form-control" id="fiscalClassCode" name="fiscalClassCode">
                    <option {if $fiscalClassCode ==='REGIMEN COMUN'}selected{/if} value="REGIMEN COMUN">REGIMEN COMUN</option>
                    <option {if $fiscalClassCode ==='REGIMEN COMUN AUTORETENEDOR'}selected{/if} value="REGIMEN COMUN AUTORETENEDOR">REGIMEN COMUN AUTORETENEDOR</option>
                    <option {if $fiscalClassCode ==='REGIMEN SIMPLIFICADO'}selected{/if} value="REGIMEN SIMPLIFICADO">REGIMEN SIMPLIFICADO</option>
                    <option {if $fiscalClassCode ==='GRAN CONTRIBUYENTE'}selected{/if} value="GRAN CONTRIBUYENTE">GRAN CONTRIBUYENTE</option>
                    <option {if $fiscalClassCode ==='GRAN CONTRIB. AUTORETENEDOR'}selected{/if} value="GRAN CONTRIB. AUTORETENEDOR">GRAN CONTRIB. AUTORETENEDOR</option>
                    <option {if $fiscalClassCode ==='ENTIDADES ESTATALES'}selected{/if} value="ENTIDADES ESTATALES">ENTIDADES ESTATALES</option>
                    <option {if $fiscalClassCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR</option>
                </select>
            </div>
        </div>
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorGlobalAttribute9" class="form-text text-danger">&nbsp;{$errorGlobalAttribute9}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Origin <span class="flag-icon flag-icon-co mr-2 ml-2" title="CO"></span></div>
                </div>
                <select class="form-control" id="globalAttribute9" name="globalAttribute9" aria-describedby="$descriptor.shortNameHelp" placeholder="Global Attribute9">
                    <option {if $globalAttribute9==='DOMESTIC_ORIGIN'}selected{/if}>DOMESTIC_ORIGIN</option>
                    <option {if $globalAttribute9==='FOREIGN_ORIGIN'}selected{/if}>FOREIGN_ORIGIN</option>
                </select>
            </div>
        </div>
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorRegimen" class="form-text text-danger">&nbsp;{$errorRegimen}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Regimen  <span class="flag-icon flag-icon-co mr-2 ml-2" title="CO only"></span></div>
                </div>
                <select class="form-control" id="regimen" name="regimen" aria-describedby="regimenHelp">
                    <option {if $regimen ==='2'}selected{/if} value="2">2 - COMUN</option>
                    <option {if $regimen ==='0'}selected{/if} value="0">0 - SIMPLIFICADO</option>
                </select>
            </div>
        </div>
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorNature" class="form-text text-danger">&nbsp;{$errorNature}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Nature  <span class="flag-icon flag-icon-co mr-2 ml-2" title="CO only"></span></div>
                </div>
                <select class="form-control" id="nature" name="nature" aria-describedby="" placeholder="">
                    <option {if $nature==='J'}selected{/if} value="J">Legal Entity</option>
                    <option {if $nature==='N'}selected{/if} value="N">Natural Person</option>
                </select>
            </div>
        </div>
        <div class="col-6 my-1">
            <small id="errorGlobalAttribute10" class="form-text text-danger">&nbsp;{$errorGlobalAttribute10}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Primary ID Type <span class="flag-icon flag-icon-co mr-2 ml-2" title="CO only"></span></div>
                </div>
                <select class="form-control" id="globalAttribute10" name="globalAttribute10" aria-describedby="$descriptor.shortNameHelp" placeholder="Global Attribute10">
                    <option {if $globalAttribute10==='LEGAL_ENTITY'}selected{/if}>LEGAL_ENTITY</option>
                    <option {if $globalAttribute10==='FOREIGN_ENTITY'}selected{/if}>FOREIGN_ENTITY</option>
                    <option {if $globalAttribute10==='INDIVIDUAL'}selected{/if}>INDIVIDUAL</option>
                    <option {if $globalAttribute10==='MERCHANT'}selected{/if}>MERCHANT</option>
                </select>
            </div>
        </div>
        <div class="col-12 my-1">
            <small id="errorCODocumentType" class="form-text text-danger">&nbsp;{$errorCODocumentType}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">CO Document Type <span class="flag-icon flag-icon-co mr-2 ml-2" title="CO only"></span></div>
                </div>
                <select class="form-control" id="CODocumentType" name="CODocumentType" aria-describedby="" placeholder="">
                    <option {if $CODocumentType==='31'}selected{/if} value="31">NIT</option>
                    <option {if $CODocumentType==='11'}selected{/if} value="11">Birth Civil Registration</option>
                    <option {if $CODocumentType==='12'}selected{/if} value="12">Identification Card</option>
                    <option {if $CODocumentType==='13'}selected{/if} value="13">Citizenship Identification</option>
                    <option {if $CODocumentType==='14'}selected{/if} value="14">Certificate of the Registrar for illiquid companies</option>
                    <option {if $CODocumentType==='15'}selected{/if} value="15">Inheritance illiquid issued by the notary</option>
                    <option {if $CODocumentType==='21'}selected{/if} value="21">Foreign Card</option>
                    <option {if $CODocumentType==='22'}selected{/if} value="22">Foreign Identification</option>
                    <option {if $CODocumentType==='33'}selected{/if} value="33">Identification of different foreign NIT assigned DIAN</option>
                    <option {if $CODocumentType==='41'}selected{/if} value="41">Passport</option>
                    <option {if $CODocumentType==='46'}selected{/if} value="46">Diplomatic passport</option>
                    <option {if $CODocumentType==='42'}selected{/if} value="42">Foreign Document Type</option>
                    <option {if $CODocumentType==='43'}selected{/if} value="43">Document defined for external information</option>
                    <option {if $CODocumentType==='44'}selected{/if} value="44">Foreign legal entity identification document</option>
                </select>
            </div>
        </div>
        <div class="col-sm-12 my-1">
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            <label class="sr-only" for="inlineFormInputPaymentMethodName">Payment Method Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Payment Method Name <span class="flag-icon flag-icon-co mr-2 ml-2" title="CO only"></span></div>
                </div>
                <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                    <option {if $paymentMethodName==='CO - BANCOLOMBIA'}selected{/if} value="CO - BANCOLOMBIA">CO - BANCOLOMBIA</option>
                    <option {if $paymentMethodName==='CO - BBVA MANUAL'}selected{/if} value="CO - BBVA MANUAL">CO - BBVA MANUAL</option>
                    <option {if $paymentMethodName==='CO - BBVA MANUAL - USD'}selected{/if} value="CO - BBVA MANUAL - USD">CO - BBVA MANUAL - USD</option>
                    <option {if $paymentMethodName==='CO - CITIBANK MANUAL'}selected{/if} value="CO - CITIBANK MANUAL">CO - CITIBANK MANUAL</option>
                    <option {if $paymentMethodName==='CO - CITIBANK MANUAL - EUR'}selected{/if} value="CO - CITIBANK MANUAL - EUR">CO - CITIBANK MANUAL - EUR</option>
                </select>
            </div>
        </div>
        {/if}
        {if $country=='MX'}
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorFiscalClassCode" class="form-text text-danger">&nbsp;{$errorFiscalClassCode}</small>
            <label class="sr-only" for="inlineFormInputFiscalClassCode">Fiscal Classification Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Code</div>
                </div>
                <select class="form-control" id="fiscalClassCode" name="fiscalClassCode">
                    <option {if $fiscalClassCode ==='CONTRIBUYENTE'}selected{/if} value="CONTRIBUYENTE">CONTRIBUYENTE</option>
                    <option {if $fiscalClassCode ==='AUTORETENEDOR'}selected{/if} value="AUTORETENEDOR">AUTORETENEDOR</option>
                    <option {if $fiscalClassCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO</option>
                    <option {if $fiscalClassCode ==='ZONA FRONTERIZA NORTE'}selected{/if} value="ZONA FRONTERIZA NORTE">ZONA FRONTERIZA NORTE</option>
                </select>
            </div>
        </div>
        <div class="col-sm-12 my-1">
            <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            <label class="sr-only" for="inlineFormInputPaymentMethodName">Payment Method Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-danger">Payment Method Name <span class="flag-icon flag-icon-mx mr-2 ml-2" title="MX only"></div>
                </div>
                <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                    <option {if $paymentMethodName==='MX - BBVA'}selected{/if} value="MX - BBVA">MX - BBVA</option>
                    <option {if $paymentMethodName==='MX - BBVA - EUR'}selected{/if} value="MX - BBVA - EUR">MX - BBVA - EUR</option>
                    <option {if $paymentMethodName==='MX - BBVA - USD'}selected{/if} value="MX - BBVA - USD">MX - BBVA - USD</option>
                    <option {if $paymentMethodName==='MX - CITIBANK - EUR'}selected{/if} value="MX - CITIBANK - EUR">MX - CITIBANK - EUR</option>
                    <option {if $paymentMethodName==='MX - CITIBANK - USD'}selected{/if} value="MX - CITIBANK - USD">MX - CITIBANK - USD</option>
                    <option {if $paymentMethodName==='MX - BANAMEX MANUAL'}selected{/if} value="MX - BANAMEX MANUAL ">MX - BANAMEX MANUAL</option>
                </select>
            </div>
        </div>
        {/if}
        {if $country=='GT'}
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorFiscalClassCode" class="form-text text-danger">&nbsp;{$errorFiscalClassCode}</small>
                <label class="sr-only" for="inlineFormInputFiscalClassCode">Fiscal Classification Code</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="fiscalClassCode" name="fiscalClassCode">
                        <option {if $fiscalClassCode ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL  &ndash; 12%</option>
                        <option {if $fiscalClassCode ==='CONTRIBUYENTE PEQUENO'}selected{/if} value="CONTRIBUYENTE PEQUENO">CONTRIBUYENTE PEQUENO  &ndash; 6%</option>
                        <option {if $fiscalClassCode ==='CONTRIBUYENTE SIN FACTURA'}selected{/if} value="CONTRIBUYENTE SIN FACTURA">CONTRIBUYENTE SIN FACTURA  &ndash; 12%</option>
                        <option {if $fiscalClassCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO  &ndash; 0%</option>
                        <option {if $fiscalClassCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR  &ndash; 0%</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-12 my-1">
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
                <label class="sr-only" for="inlineFormInputPaymentMethodName">Payment Method Name</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-gt mr-2 ml-2" title="GT only"></div>
                    </div>
                    <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                        <option {if $paymentMethodName==='GT - CITIBANK MANUAL'}selected{/if} value="GT - CITIBANK MANUAL">GT - CITIBANK MANUAL</option>
                        <option {if $paymentMethodName==='GT - CITIBANK MANUAL - USD'}selected{/if} value="GT - CITIBANK MANUAL - USD">GT - CITIBANK MANUAL &ndash; USD</option>
                        <option {if $paymentMethodName==='GT - BAC MANUAL'}selected{/if} value="GT - BAC MANUAL">GT - BAC MANUAL</option>
                        <option {if $paymentMethodName==='GT - BAC MANUAL - USD'}selected{/if} value="GT - BAC MANUAL - USD">GT - BAC MANUAL &ndash; USD</option>
                        <option {if $paymentMethodName==='GT - INDUSTRIAL MANUAL'}selected{/if} value="GT - INDUSTRIAL MANUAL">GT - INDUSTRIAL MANUAL</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 my-1">
                <small class="form-text text-danger">&nbsp;{$errorOrderNumberGt}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Order Number&nbsp;<span class="flag-icon flag-icon-gt mr-2 ml-2" title="GT only"></span></div>
                    </div>
                    <input type="text" class="form-control" id="orderNumberGt" name="orderNumberGt" value="{$orderNumberGt}" placeholder="Order Number of the cedula">
                </div>
            </div>
            <div class="col-sm-6 my-1">
                <small class="form-text text-danger">&nbsp;{$errorRegisterNumberGt}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Register Number&nbsp;<span class="flag-icon flag-icon-gt mr-2 ml-2" title="GT only"></span></div>
                    </div>
                    <input type="text" class="form-control" id="registerNumberGt" name="registerNumberGt" value="{$registerNumberGt}" placeholder="Register Number of the cedula">
                </div>
            </div>
        {/if}
        {if $country=='HN'}
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorFiscalClassCode " class="form-text text-danger">&nbsp;{$errorFiscalClassCode}</small>
                <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="fiscalClassCode" name="fiscalClassCode">
                        <option {if $fiscalClassCode ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL  &ndash; 15%</option>
                        <option {if $fiscalClassCode ==='CONTRIBUYENTE SPECIAL'}selected{/if} value="CONTRIBUYENTE SPECIAL">CONTRIBUYENTE SPECIAL  &ndash; 18%</option>
                        <option {if $fiscalClassCode ==='CONTRIBUYENTE HOTELES'}selected{/if} value="CONTRIBUYENTE HOTELES">CONTRIBUYENTE HOTELES  &ndash; 19%</option>
                        <option {if $fiscalClassCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO  &ndash; 0%</option>
                        <option {if $fiscalClassCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR  &ndash; 0%</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-12 my-1">
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
                <label class="sr-only" for="inlineFormInputPaymentMethodName">Payment Method Name</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-hn mr-2 ml-2" title="HN only"></div>
                    </div>
                    <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                        <option {if $paymentMethodName==='HN - CITIBANK MANUAL'}selected{/if} value="HN - CITIBANK MANUAL">HN - CITIBANK MANUAL</option>
                        <option {if $paymentMethodName==='HN - CITIBANK MANUAL - USD'}selected{/if} value="HN - CITIBANK MANUAL - USD">HN - CITIBANK MANUAL &ndash; USD</option>
                        <option {if $paymentMethodName==='HN - BAC MANUAL'}selected{/if} value="HN - BAC MANUAL">HN - BAC MANUAL</option>
                        <option {if $paymentMethodName==='HN - BAC - USD'}selected{/if} value="HN - BAC - USD">HN - BAC &ndash; USD</option>
                        <option {if $paymentMethodName==='HN - FICOHSA  MANUAL'}selected{/if} value="HN - FICOHSA  MANUAL">HN - FICHOHSA MANUAL</option>
                        <option {if $paymentMethodName==='HN - FICOSA - USD'}selected{/if} value="HN - FICOSA - USD">HN - FICOSA &ndash; USD</option>
                    </select>
                </div>
            </div>
        {/if}
        {if $country=='SV'}
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorFiscalClassCode " class="form-text text-danger">&nbsp;{$errorFiscalClassCode}</small>
                <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="fiscalClassCode" name="fiscalClassCode" placeholder="Classification Code ">
                        <option {if $fiscalClassCode ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL  &ndash; 13%</option>
                        <option {if $fiscalClassCode ==='CONTRIBUYENTE MEDIUM'}selected{/if} value="CONTRIBUYENTE MEDIUM">CONTRIBUYENTE MEDIUM  &ndash; 13%</option>
                        <option {if $fiscalClassCode ==='CONTRIBUYENTE PEQUENO'}selected{/if} value="CONTRIBUYENTE PEQUENO">CONTRIBUYENTE PEQUENO  &ndash; 13%</option>
                        <option {if $fiscalClassCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO  &ndash; 0%</option>
                        <option {if $fiscalClassCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR  &ndash; 0%</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-12 my-1">
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
                <label class="sr-only" for="inlineFormInputPaymentMethodName">Payment Method Name</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-sv mr-2 ml-2" title="SV only"></div>
                    </div>
                    <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                        <option {if $paymentMethodName==='SV - CITIBANK MANUAL'}selected{/if} value="SV - CITIBANK MANUAL">SV - CITIBANK MANUAL</option>
                        <option {if $paymentMethodName==='SV - BAC MANUAL'}selected{/if} value="SV - BAC MANUAL">SV - BAC MANUAL</option>
                        <option {if $paymentMethodName==='SV - CUSCATLAN MANUAL'}selected{/if} value="SV - CUSCATLAN  MANUAL">SV - CUSCATLAN MANUAL</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 my-1">
                <small class="form-text text-danger">&nbsp;{$errorDocumentTypeSV}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Document Type&nbsp;<span class="flag-icon flag-icon-sv mr-2 ml-2" title="SV only"></span></div>
                    </div>
                    <select class="form-control" id="documentTypeSv" name="documentTypeSv">
                        <option {if $documentTypeSv==='1'}selected{/if} value="1">1 : NIT</option>
                        <option {if $documentTypeSv==='3'}selected{/if} value="3">3 : DUI</option>
                        <option {if $documentTypeSv==='4'}selected{/if} value="4">4 : Passport</option>
                        <option {if $documentTypeSv==='5'}selected{/if} value="5">5 : Residence Card</option>
                        <option {if $documentTypeSv==='6'}selected{/if} value="6">6 : Others</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 my-1">
                <small class="form-text text-danger">&nbsp;{$errorContributorRegistrationSv}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Contributor Registration &nbsp;<span class="flag-icon flag-icon-sv mr-2 ml-2" title="SV only"></span></div>
                    </div>
                    <input type="text" class="form-control" id="contributorRegistrationSv" name="contributorRegistrationSv" value="{$contributorRegistrationSv}" placeholder="Contributor Registration SV">
                </div>
            </div>
        {/if}
        {if $country=='NI'}
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorFiscalClassCode " class="form-text text-danger">&nbsp;{$errorFiscalClassCode}</small>
                <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="fiscalClassCode" name="fiscalClassCode" placeholder="Classification Code ">
                        <option {if $fiscalClassCode ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL  &ndash; 15%</option>
                        <option {if $fiscalClassCode ==='DIPLOMATICOS'}selected{/if} value="DIPLOMATICOS">DIPLOMATICOS  &ndash; 0%</option>
                        <option {if $fiscalClassCode ==='CONSTANCIA DE EXONERACION'}selected{/if} value="CONSTANCIA DE EXONERACION">CONSTANCIA DE EXONERACION &ndash; 0%</option>
                        <option {if $fiscalClassCode ==='ENERGIA ELECTRICA'}selected{/if} value="ENERGIA ELECTRICA">ENERGIA ELECTRICA &ndash; 7%</option>
                        <option {if $fiscalClassCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO  &ndash; 0%</option>
                        <option {if $fiscalClassCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR  &ndash; 0%</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-12 my-1">
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
                <label class="sr-only" for="inlineFormInputPaymentMethodName">Payment Method Name</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-ni mr-2 ml-2" title="NI only"></div>
                    </div>
                    <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                        <option {if $paymentMethodName==='NI - FICOHSA MANUAL'}selected{/if} value="NI - FICOHSA MANUAL">NI - FICOHSA MANUAL</option>
                        <option {if $paymentMethodName==='NI - FICOHSA - USD'}selected{/if} value="NI - FICOHSA - USD">NI - FICOHSA &ndash; USD</option>
                        <option {if $paymentMethodName==='NI - BAC - USD'}selected{/if} value="NI - BAC - USD">NI - BAC &ndash; USD</option>
                        <option {if $paymentMethodName==='NI - BAC MANUAL'}selected{/if} value="NI - BAC MANUAL">NI - BAC MANUAL</option>
                    </select>
                </div>
            </div>
        {/if}
        {if $country=='CR'}
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorFiscalClassCode " class="form-text text-danger">&nbsp;{$errorFiscalClassCode}</small>
                <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="fiscalClassCode" name="fiscalClassCode" placeholder="Classification Code ">
                        <option {if $fiscalClassCode ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL  &ndash; 13%</option>
                        <option {if $fiscalClassCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO  &ndash; 0%</option>
                        <option {if $fiscalClassCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR  &ndash; 0%</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-12 my-1">
                <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
                <label class="sr-only" for="inlineFormInputPaymentMethodName">Payment Method Name</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-cr mr-2 ml-2" title="CR only"></div>
                    </div>
                    <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                        <option {if $paymentMethodName==='CR - CITIBANK MANUAL'}selected{/if} value="CR - CITIBANK MANUAL">CR - CITIBANK MANUAL</option>
                        <option {if $paymentMethodName==='CR - CITIBANK - USD'}selected{/if} value="CR - CITIBANK - USD">CR - CITIBANK - USD</option>
                        <option {if $paymentMethodName==='CR - PROMERICA MANUAL'}selected{/if} value="CR - PROMERICA MANUAL">CR - PROMERICA MANUAL</option>
                        <option {if $paymentMethodName==='CR - PROMERICA - USD'}selected{/if} value="CR - PROMERICA - USD">CR - PROMERICA - USD</option>
                    </select>
                </div>
            </div>
        {/if}
        {if $country=='AR'}
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorFiscalClassCode " class="form-text text-danger">&nbsp;{$errorFiscalClassCode}</small>
            <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Code</div>
                </div>
                <select class="form-control" id="fiscalClassCode" name="fiscalClassCode" placeholder="Classification Code ">
                    <option {if $fiscalClassCode ==='RESPONSABLES NO INSCRIPTOS'}selected{/if} value="RESPONSABLES NO INSCRIPTOS">RESPONSABLES NO INSCRIPTOS</option>
                    <option {if $fiscalClassCode ==='RESPONSABLES INSCRIPTOS'}selected{/if} value="RESPONSABLES INSCRIPTOS">RESPONSABLES INSCRIPTOS</option>
                    <option {if $fiscalClassCode ==='EXENTOS'}selected{/if} value="EXENTOS">EXENTOS  &ndash; 0%</option>
                    <option {if $fiscalClassCode ==='CONSUMIDOR FINAL'}selected{/if} value="CONSUMIDOR FINAL">CONSUMIDOR FINAL</option>
                    <option {if $fiscalClassCode ==='MONOTRIBUTO'}selected{/if} value="MONOTRIBUTO">MONOTRIBUTO</option>
                    <option {if $fiscalClassCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR</option>
                </select>
            </div>
        </div>
        <div class="col-6 my-1">
            <small id="errorGlobalAttribute10" class="form-text text-danger">&nbsp;{$errorGlobalAttribute10}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Primary ID Type <span class="flag-icon flag-icon-ar mr-2 ml-2" title="AR only"></span></div>
                </div>
                <select class="form-control" id="globalAttribute10" name="globalAttribute10">
                    <option {if $globalAttribute10==='80'}selected{/if} value="80">Domestic Corporation or Foreign Business Entity</option>
                    <option {if $globalAttribute10==='82'}selected{/if} value="82">Employee</option>
                    <option {if $globalAttribute10==='96'}selected{/if} value="96">Individual</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            <label class="sr-only" for="inlineFormInputPaymentMethodName">Payment Method Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-ar mr-2 ml-2" title="AR only"></div>
                </div>
                <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                    <option {if $paymentMethodName==='AR - SANTANDER MANUAL'}selected{/if} value="AR - SANTANDER MANUAL">AR - SANTANDER MANUAL</option>
                    <option {if $paymentMethodName==='AR - BANCO CIUDAD MANUAL'}selected{/if} value="AR - BANCO CIUDAD MANUAL">AR - BANCO CIUDAD MANUAL</option>
                    <option {if $paymentMethodName==='AR - CITIBANK - USD'}selected{/if} value="AR - CITIBANK - USD">AR - CITIBANK - USD</option>
                    <option {if $paymentMethodName==='AR - CITIBANK MANUAL'}selected{/if} value="AR - CITIBANK MANUAL">AR - CITIBANK MANUAL</option>
                </select>
            </div>
        </div>
        {/if}
        {if $country=='PE'}
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorFiscalClassCode " class="form-text text-danger">&nbsp;{$errorFiscalClassCode}</small>
            <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Code</div>
                </div>
                <select class="form-control" id="fiscalClassCode" name="fiscalClassCode" placeholder="Classification Code ">
                    <option {if $fiscalClassCode ==='GRABADAS DEL IGV'}selected{/if} value="GRABADAS DEL IGV">GRABADAS DEL IGV</option>
                    <option {if $fiscalClassCode ==='NO GRABADAS DEL IGV'}selected{/if} value="NO GRABADAS DEL IGV">NO GRABADAS DEL IGV</option>
                    <option {if $fiscalClassCode ==='EXENTOS'}selected{/if} value="EXENTOS">EXENTOS</option>
                </select>
            </div>
        </div>
        <div class="col-sm-12 my-1">
            <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            <label class="sr-only" for="inlineFormInputPaymentMethodName">Payment Method Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-pe mr-2 ml-2" title="PE only"></div>
                </div>
                <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                    <option {if $paymentMethodName==='PE - BBVA MANUAL'}selected{/if} value="PE - BBVA MANUAL">PE - BBVA MANUAL</option>
                    <option {if $paymentMethodName==='PE - BANCO DE LA NACION MANUAL'}selected{/if} value="PE - BANCO DE LA NACION MANUAL">PE - BANCO DE LA NACION MANUAL</option>
                    <option {if $paymentMethodName==='PE - BBVA - EUR'}selected{/if} value="PE - BBVA - EUR">PE - BBVA - EUR</option>
                    <option {if $paymentMethodName==='PE - BBVA - USD'}selected{/if} value="PE - BBVA - USD">PE - BBVA - USD</option>
                    <option {if $paymentMethodName==='PE - BCP - USD'}selected{/if} value="PE - BCP - USD">PE - BCP - USD</option>
                    <option {if $paymentMethodName==='PE - BCP MANUAL'}selected{/if} value="PE - BCP MANUAL">PE - BCP MANUAL</option>
                </select>
            </div>
        </div>
        {/if}
        {if $country=='CL'}
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorFiscalClassCode " class="form-text text-danger">&nbsp;{$errorFiscalClassCode}</small>
            <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Code</div>
                </div>
                <select class="form-control" id="fiscalClassCode" name="fiscalClassCode" placeholder="Classification Code ">
                    <option {if $fiscalClassCode ==='AFECTAS DEL IVA'}selected{/if} value="AFECTAS DEL IVA">AFECTAS DEL IVA</option>
                    <option {if $fiscalClassCode ==='EXENTAS DEL IVA'}selected{/if} value="EXENTAS DEL IVA">EXENTAS DEL IVA</option>
                </select>
            </div>
        </div>
        <div class="col-6 my-1">
            <small id="errorGlobalAttribute10" class="form-text text-danger">&nbsp;{$errorGlobalAttribute10}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Primary ID Type <span class="flag-icon flag-icon-cl mr-2 ml-2" title="CL only"></span></div>
                </div>
                <select class="form-control" id="globalAttribute10" name="globalAttribute10">
                    <option {if $globalAttribute10==='DOMESTIC_ORIGIN'}selected{/if}>DOMESTIC_ORIGIN</option>
                    <option {if $globalAttribute10==='FOREIGN_ORIGIN'}selected{/if}>FOREIGN_ORIGIN</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            <label class="sr-only" for="inlineFormInputPaymentMethodName">Payment Method Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-cl mr-2 ml-2" title="CL only"></div>
                </div>
                <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                    <option {if $paymentMethodName==='CL - SANTANDER MANUAL'}selected{/if} value="CL - SANTANDER MANUAL">CL - SANTANDER MANUAL</option>
                    <option {if $paymentMethodName==='CL - BCI MANUAL'}selected{/if} value="CL - BCI MANUAL">CL - BCI MANUAL</option>
                    <option {if $paymentMethodName==='CL - SANTANDER - EUR'}selected{/if} value="CL - SANTANDER - EUR">CL - SANTANDER - EUR</option>
                    <option {if $paymentMethodName==='CL - SANTANDER - USD'}selected{/if} value="CL - SANTANDER - USD">CL - SANTANDER - USD</option>
                </select>
            </div>
        </div>
        {/if}
        {if $country=='PY'}
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorFiscalClassCode " class="form-text text-danger">&nbsp;{$errorFiscalClassCode}</small>
            <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Code</div>
                </div>
                <select class="form-control" id="fiscalClassCode" name="fiscalClassCode" placeholder="Classification Code ">
                    <option {if $fiscalClassCode ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL</option>
                    <option {if $fiscalClassCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO</option>
                    <option {if $fiscalClassCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR</option>
                    <option {if $fiscalClassCode ==='ZONA FRANCA'}selected{/if} value="ZONA FRANCA">ZONA FRANCA</option>
                </select>
            </div>
        </div>
        <div class="col-sm-12 my-1">
            <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            <label class="sr-only" for="inlineFormInputPaymentMethodName">Payment Method Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-py mr-2 ml-2" title="PY only"></div>
                </div>
                <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                    <option {if $paymentMethodName==='PY - ITAU MANUAL?'}selected{/if} value="PY - ITAU MANUAL">PY - ITAU MANUAL</option>
                    <option {if $paymentMethodName==='PY - ITAU - USD'}selected{/if} value="PY - ITAU - USD">PY - ITAU - USD</option>
                </select>
            </div>
        </div>
        {/if}
        {if $country=='UY'}
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorFiscalClassCode " class="form-text text-danger">&nbsp;{$errorFiscalClassCode}</small>
            <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Code</div>
                </div>
                <select class="form-control" id="fiscalClassCode" name="fiscalClassCode" placeholder="Classification Code ">
                    <option {if $fiscalClassCode ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL</option>
                    <option {if $fiscalClassCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO</option>
                    <option {if $fiscalClassCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR</option>
                </select>
            </div>
        </div>
        <div class="col-sm-12 my-1">
            <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            <label class="sr-only" for="inlineFormInputPaymentMethodName">Payment Method Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-warning">Payment Method Name <span class="flag-icon flag-icon-uy mr-2 ml-2" title="UY only"></div>
                </div>
                <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                    <option {if $paymentMethodName==='UY - SANTANDER MANUAL'}selected{/if} value="UY - SANTANDER MANUAL">UY - SANTANDER MANUAL</option>
                    <option {if $paymentMethodName==='UY - BROU - USD'}selected{/if} value="UY - BROU - USD">UY - BROU - USD</option>
                    <option {if $paymentMethodName==='UY - BROU MANUAL'}selected{/if} value="UY - BROU MANUAL">UY - BROU MANUAL</option>
                    <option {if $paymentMethodName==='UY - SANTANDER - EUR'}selected{/if} value="UY - SANTANDER - EUR">UY - SANTANDER - EUR</option>
                    <option {if $paymentMethodName==='UY - SANTANDER - USD'}selected{/if} value="UY - SANTANDER - USD">UY - SANTANDER - USD</option>
                </select>
            </div>
        </div>
    {/if}
    </div>

    {if ($country=='PE')}
        <div id="nameFieldsPE" class="form-row align-items-center">
            <div class="col-3 my-1">
                <small id="errorFirstNamePE" class="form-text text-danger">&nbsp;{$errorFirstNamePE}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">First Name&nbsp;
                            <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                        </div>
                    </div>
                    <input class="form-control" id="firstNamePE" name="firstNamePE" value="{$firstNamePE}"/>
                </div>
            </div>
            <div class="col-3 my-1">
                <small id="errorMiddleNamePE" class="form-text text-danger">&nbsp;{$errorMiddleNamePE}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Middle Name&nbsp;
                            <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                        </div>
                    </div>
                    <input class="form-control" id="middleNamePE" name="middleNamePE" value="{$middleNamePE}"/>
                </div>
            </div>
            <div class="col-3 my-1">
                <small id="errorFirstLastNamePE" class="form-text text-danger">&nbsp;{$errorFirstLastNamePE}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">First Last Name&nbsp;
                            <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                        </div>
                    </div>
                    <input class="form-control" id="lastNamePE" name="lastNamePE" value="{$lastNamePE}"/>
                </div>
            </div>
            <div class="col-3 my-1">
                <small id="errorSecondLasNamePE" class="form-text text-danger">&nbsp;{$errorSecondLasNamePE}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Second Last Name&nbsp;
                            <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                        </div>
                    </div>
                    <input class="form-control" id="secondLastNamePE" name="secondLastNamePE" value="{$secondLastNamePE}"/>
                </div>
            </div>
        </div>
        <div class="form-row align-items-center">
            <div class="col-6 my-1">
                <small id="errorPersonCategoryPE" class="form-text text-danger">&nbsp;{$errorPersonCategoryPE}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Person Category&nbsp;
                            <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                        </div>
                    </div>
                    <select class="form-control" id="personCategoryPE" name="personCategoryPE">
                        <option {if $personCategoryPE==='Legal Person'}selected{/if} value='Legal Person'>Legal Person</option>
                        <option {if $personCategoryPE==='Natural Person'}selected{/if} value='Natural Person'>Natural Person</option>
                    </select>
                </div>
            </div>
            <div class="col-6 my-1">
                <small id="errorPersonTypePE" class="form-text text-danger">&nbsp;{$errorPersonTypePE}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Person Type&nbsp;
                            <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                        </div>
                    </div>
                    <select class="form-control" id="personTypePE" name="personTypePE">
                    </select>
                </div>
            </div>
            <div class="col-6 my-1">
                <small id="errorDocumentTypePE" class="form-text text-danger">&nbsp;{$errorDocumentTypePE}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Document Type&nbsp;
                            <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                        </div>
                    </div>
                    <select class="form-control" id="documentTypePE" name="documentTypePE">
                    </select>
                </div>
            </div>
            <div class="col-6 my-1">
                <small id="errorDocumentNumberPE" class="form-text text-danger">&nbsp;{$errorDocumentNumberPE}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Document Number&nbsp;
                            <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                        </div>
                    </div>
                    <input class="form-control" id="documentNumberPE" name="documentNumberPE" value="{$documentNumberPE}"/>
                </div>
            </div>
        </div>

        <script type="application/javascript">

            var setValueForPersonType = false;
            var setValueForDocumentType = false;

            $("#personCategoryPE").ready(function() {
                var personCategoryPE = '{$personCategoryPE}';

                if (personCategoryPE=='')
                    personCategoryPE ='Legal Person';

                $("#personCategoryPE").val(personCategoryPE);
                $("#personCategoryPE").trigger('change');
            });


            $( "#personCategoryPE" ).change(function() {
                if ($(this).val()=='Legal Person'){
                    $('#nameFieldsPE').hide();
                }else{
                    $('#nameFieldsPE').show();
                }

            });

            $("#documentTypePE").remoteChained({
                parents : "#personCategoryPE,#personTypePE",
                url : "/laundry/Vendors/getDocumentTypePE",
                data: function (json) {
                    var documentTypePE = '{$documentTypePE}';

                    if (setValueForPersonType&&!setValueForDocumentType&&(documentTypePE!='')){
                        setTimeout(function(){
                            $("#documentTypePE").val(documentTypePE).change();
                        }, 250);
                        setValueForDocumentType = true;
                    }

                    return json;
                }
            });


            $("#personTypePE").remoteChained({
                parents : "#personCategoryPE",
                url : "/laundry/Vendors/getPersonTypePE",
                data: function (json) {
                    var personTypePE = '{$personTypePE}';

                    if (!setValueForPersonType&&(personTypePE!='')){
                        setTimeout(function(){
                            $("#personTypePE").val(personTypePE).change();
                        }, 250);

                        setValueForPersonType = true;

                    }

                    return json;
                }
            });

        </script>
    {/if}

    <div class="form-row align-items-center">
        <div class="col-6 my-1">
            <small id="errorFiscalClassStartDate" class="form-text text-danger">&nbsp;{$errorFiscalClassStartDate}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Class Start Date</div>
                </div>
                <input class="form-control" id="fiscalClassStartDate" name="fiscalClassStartDate" aria-describedby="fiscalClassStartDateHelp" width="250" />
                <script>
                    $('#fiscalClassStartDate').datepicker({
                        uiLibrary: 'bootstrap4',
                        format:'dd-mm-yyyy',
                        value: '{$fiscalClassStartDate|date_format:"%d-%m-%Y"}'
                    });
                </script>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-4 my-1">
            <small id="errorSpecialCustomer class="form-text text-danger">&nbsp;{$errorSpecialCustomer}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Special Customer <small id="errorSpecialCustomer" class="form-text text-danger">&nbsp;{$errorSpecialCustomer}</small></div>
                </div>
                <input type="checkbox" {($specialCustomer==true)?'checked':''} style="margin:10px;" id="specialCustomer" name="specialCustomer">
            </div>
        </div>
        <div class="col-sm-1 col-md-4 my-1">
            <small id="errorPrimaryFlag" class="form-text text-danger">&nbsp;{$errorPrimaryFlag}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Primary Flag</div>
                </div>
                <input type="checkbox" {($primaryFlag==true)?'checked':''} style="margin:10px;" id="primaryFlag" name="primaryFlag">
            </div>
        </div>
        <div class="col-sm-1 col-md-4 my-1">
            <small id="errorGlobalFlag" class="form-text text-danger">&nbsp;{$errorGlobalFlag}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Global Flag </div>
                </div>
                <input type="checkbox" {($globalFlag==true)?'checked':''}  style="margin:10px;" id="globalFlag" name="globalFlag">
            </div>
        </div>
    </div>


    {/if}
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorAliases" class="form-text text-danger">&nbsp;{$errorAliases}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Aliases</div>
                </div>
                <textarea type="text" class="form-control" id="aliases" name="aliases" value="" placeholder="Aliases">{$aliases}</textarea>
            </div>
            <label class="alert-info text-info"><i class="fas fa-id-card fa-sm fa-fw"></i> An alias is an alternative ID fot this item.  Use this field to unify records to be migrated to ORACLE. Use a comma-separated list, with no spaces (e.g. PA1000,PA1002,CO1003)</label>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorObservations" class="form-text text-danger">&nbsp;{$errorObservations}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Observations</div>
                </div>
                <textarea type="text" class="form-control" id="observations" name="observations" value="" placeholder="Observations" rows="5">{$observations}</textarea>
            </div>
            <label class="alert-info text-info"><i class="fas fa-bug fa-lg fa-fw"></i> Please help us detect bugs.  Use this field to annotate special cases, cleansing decisions, and relevant issues related to this record.</label>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">{$actionLabel}</button>
                <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
            </div>
        </div>
    </div>
</form>

<br>
<h4>Customer Addresses &nbsp;<a data-toggle="tooltip" data-placement="top" title="add a new Address to {$origSystemCustomerRef}" href="{base_url()}CA/createNew/{$id}" style="cursor:pointer"><small><i class="text-primary fas fa-plus-circle fa-sm fa-fw"></i></small></a></h4>
{if isset($addresses) && (sizeof($addresses)>0)}
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th scope="col"></th>
            <th>#</th>
            <th>Action</th>
            <th>Address Ref</th>
            <th>Primary Site</th>
            <th>Site Use</th>
            <th>Address Line 1</th>
            <th>City</th>
            <th>Province</th>
            <th>State</th>
            <th>Country</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$addresses item=a}
            <tr class="{if ($a.dismissed)} dismissed {elseif (($a.locallyValidated)&&($a.crossRefValidated)) } alert-success {else} alert-warning {/if} clickable-row" style="cursor:pointer" data-href='{base_url()}CA/customEditCleansed/{$a.country}/{$a.id}'>
                <th scope="row">
                    <a href="{base_url()}CA/editCleansed/{$a.id}" data-toggle="tooltip" data-placement="top" title="inspect all fields from address {$a.origSystemAdressReference}"><i class="text-primary fas fa-edit fa-sm fa-fw"></i></a>
                    <a href="{base_url()}CA/toggle/{$a.id}" data-toggle="tooltip" data-placement="top" title="{if ($c.dismissed)}Recall{else}Dismiss{/if} {$a.origSystemAdressReference}"><i class="text-primary fas fa-minus-circle fa-sm fa-fw"></i></a>
                </th>
                <th scope="row">{$a.id}</th>
                <td>{$a.action|truncate:40}</td>
                <td>{$a.origSystemAdressReference}</td>
                <td>{($a.primarySiteUseFlag===true)?'Y':'N'}</td>
                <td>{$a.siteUseCode}</td>
                <td>{$a.addressLine1|truncate:20}</td>
                <td>{$a.city}</td>
                <td>{$a.province}</td>
                <td>{$a.state}</td>
                <td>{$a.addressCountry}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{else}
    <p class="p-3 alert-warning">There are no addresses at this customer.  You may add a new address <a data-toggle="tooltip" data-placement="top" title="add a new Address to {$origSystemCustomerRef}" href="{base_url()}CA/createNew/{$id}">here</a></p>
{/if}
<br/>
<br/>
<br/>
<br/>
<br/>
