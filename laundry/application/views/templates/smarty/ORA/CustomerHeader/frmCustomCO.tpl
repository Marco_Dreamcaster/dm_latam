<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorOrigSystemPartyRef" class="form-text text-danger">&nbsp;{$errorOrigSystemPartyRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Orig System Party Ref</div>
                </div>
                <input type="text" class="form-control" id="origSystemPartyRef" name="origSystemPartyRef" value="{$origSystemPartyRef}" placeholder="Orig System Party Ref" disabled>
            </div>
        </div>
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorOrigSystemCustomerRef" class="form-text text-danger">&nbsp;{$errorOrigSystemCustomerRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Orig System Customer Ref </div>
                </div>
                <input type="text" class="form-control" id="origSystemCustomerRef" name="origSystemCustomerRef" value="{$origSystemCustomerRef}" placeholder="Orig System Customer Ref" disabled>
            </div>
        </div>
        <div class="col-sm-1 col-md-12 my-1">
            <small id="errorCustomerName" class="form-text text-danger">&nbsp;{$errorCustomerName}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Customer Name</div>
                </div>
                <input type="text" class="form-control" id="customerName" name="customerName" value="{$customerName}" placeholder="Customer Name">
            </div>
        </div>
    </div>
    {if $dismissed}
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorBillToMigratedRef" class="form-text text-danger">&nbsp;{$errorBillToMigratedRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-danger">Migrated BillTo Ref</div>
                </div>
                <input type="text" class="form-control" id="billToMigratedRef" name="billToMigratedRef" value="{$billToMigratedRef}">
            </div>
        </div>
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorShipToMigratedRef" class="form-text text-danger">&nbsp;{$errorShipToMigratedRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-danger">Migrated ShipTo Ref</div>
                </div>
                <input type="text" class="form-control" id="shipToMigratedRef" name="shipToMigratedRef" value="{$shipToMigratedRef}">
            </div>
        </div>
    </div>
    {else}
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorCustomerProfile" class="form-text text-danger">&nbsp;{$errorCustomerProfile}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Customer Profile </div>
                </div>
                <select class="form-control" id="customerProfile" name="customerProfile" aria-describedby="$descriptor.shortNameHelp" placeholder="Customer Profile">
                    <option {if $customerProfile=='TKE LATAM COMPANY GROUP'}selected{/if} value='TKE LATAM COMPANY GROUP'>TKE LATAM COMPANY GROUP</option>
                    <option {if $customerProfile=='TKE LATAM GOVERNMENT'}selected{/if} value='TKE LATAM GOVERNMENT'>TKE LATAM GOVERNMENT</option>
                    <option {if $customerProfile=='TKE LATAM DISTRIBUTOR'}selected{/if} value='TKE LATAM DISTRIBUTOR'>TKE LATAM DISTRIBUTOR</option>
                    <option {if $customerProfile=='TKE LATAM INTERCOMPANY'}selected{/if} value='TKE LATAM INTERCOMPANY'>TKE LATAM INTERCOMPANY</option>
                    <option {if $customerProfile=='TKE LATAM STANDARD'}selected{/if} value='TKE LATAM STANDARD'>TKE LATAM STANDARD</option>
                    <option {if $customerProfile=='TKE LATAM VIP'}selected{/if} value='TKE LATAM VIP'>TKE LATAM VIP</option>
                </select>

            </div>
        </div>
    </div>
        <div class="form-row align-items-center">
            <div class="col-sm-6 my-1">
                <small id="errorCustomerClassCode" class="form-text text-danger">&nbsp;{$errorCustomerClassCode}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">Class Code</div>
                    </div>
                    <select class="form-control" id="customerClassCode" name="customerClassCode">
                        <option {if $customerClassCode=='01'}selected{/if} value='01'>01 Private / Commercial companies</option>
                        <option {if $customerClassCode=='02'}selected{/if} value='02'>02 Holdings / Company group</option>
                        <option {if $customerClassCode=='03'}selected{/if} value='03'>03 Government owned companies</option>
                        <option {if $customerClassCode=='04'}selected{/if} value='04'>04 Private individuals</option>
                        <option {if $customerClassCode=='05'}selected{/if} value='05'>05 Association</option>
                        <option {if $customerClassCode=='06'}selected{/if} value='06'>06 Government</option>
                        <option {if $customerClassCode=='FEDERAL'}selected{/if} value='FEDERAL'>Federal</option>
                        <option {if $customerClassCode=='PUBLIC SECTOR COMPANIES'}selected{/if} value='PUBLIC SECTOR COMPANIES'>Public Sector Companies</option>
                    </select>
                </div>
            </div>
        </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorClassification" class="form-text text-danger">&nbsp;{$errorClassification}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Classification</div>
                </div>
                <select class="form-control" id="classification" name="classification" aria-describedby="$descriptor.classificationHelp">
                    <option value="">--</option>
                    <option {if $classification ==='Private/Commercial companies'}selected{/if}>Private/Commercial companies</option>
                    <option {if $classification ==='Holdings/Company group'}selected{/if}>Holdings/Company group</option>
                    <option {if $classification ==='Government owned companies'}selected{/if}>Government owned companies</option>
                    <option {if $classification ==='Private individuals'}selected{/if}>Private individuals</option>
                    <option {if $classification ==='Association'}selected{/if}>Association</option>
                    <option {if $classification ==='Government'}selected{/if}>Government</option>
                    <option {if $classification ==='Federal'}selected{/if}>Federal</option>
                    <option {if $classification ==='Public Sector Companies'}selected{/if}>Public Sector Companies</option>
                </select>
            </div>
        </div>
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorCustomerProfile" class="form-text text-danger">&nbsp;{$errorCustomerProfile}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Customer Type</div>
                </div>
                <select class="form-control" id="customerType" name="customerType" aria-describedby="$descriptor.shortNameHelp" placeholder="Customer Type">
                    <option value="">--</option>
                    <option {if $customerType==='INTERNAL'}selected{/if}>INTERNAL</option>
                    <option {if $customerType==='EXTERNAL'}selected{/if}>EXTERNAL</option>

                </select>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorTaxPayerId" class="form-text text-danger">&nbsp;{$errorTaxPayerId}</small>
            <label class="sr-only" for="inlineFormInputCoExtAttribute8">Tax Payer ID (JGZZ_FISCAL_CODE)</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-warning">Tax Payer ID (JGZZ_FISCAL_CODE)</div>
                </div>
                <input type="text" class="form-control" id="taxPayerId" name="taxPayerId" value="{$taxPayerId}" placeholder="">
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorGlobalAttribute12" class="form-text text-danger">&nbsp;{$errorGlobalAttribute12}</small>
            <label class="sr-only" for="inlineFormInputGlobalAttribute12">Tax Payer ID Digit (globalAttribute12)</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-warning">Tax Payer ID Digit (globalAttribute12)</div>
                </div>
                <input type="text" class="form-control" id="globalAttribute12" name="globalAttribute12" value="{$globalAttribute12}" placeholder="">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center" id="paymentMethodNameFormRow">
        <div class="col-sm-6 my-1">
            <small id="errorPaymentMethodName" class="form-text text-danger">&nbsp;{$errorPaymentMethodName}</small>
            <label class="sr-only" for="inlineFormInputPaymentMethodName">Payment Method Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-danger">Payment Method Name</div>
                </div>
                <select class="form-control" id="paymentMethodName" name="paymentMethodName">
                    <option {if $paymentMethodName==='CO - BANCOLOMBIA'}selected{/if} value="CO - BANCOLOMBIA">CO - BANCOLOMBIA</option>
                    <option {if $paymentMethodName==='CO - BBVA MANUAL'}selected{/if} value="CO - BBVA MANUAL">CO - BBVA MANUAL</option>
                    <option {if $paymentMethodName==='CO - BBVA MANUAL - USD'}selected{/if} value="CO - BBVA MANUAL - USD">CO - BBVA MANUAL - USD</option>
                    <option {if $paymentMethodName==='CO - CITIBANK MANUAL'}selected{/if} value="CO - CITIBANK MANUAL">CO - CITIBANK MANUAL</option>
                    <option {if $paymentMethodName==='CO - CITIBANK MANUAL - EUR'}selected{/if} value="CO - CITIBANK MANUAL - EUR">CO - CITIBANK MANUAL - EUR</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorPaymentTermName" class="form-text text-danger">&nbsp;{$errorPaymentTermName}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-warning">Payment Term Name</div>
                </div>
                <select class="form-control" id="paymentTermName" name="paymentTermName">
                    <option {if $paymentTermName=='1 of the Month'}selected{/if} value='1 of the Month'>1 of the Month - </option>
                    <option {if $paymentTermName=='1.5% 12 NET 12'}selected{/if} value='1.5% 12 NET 12'>1.5% 12 NET 12 - 1.5% 12 NET 12</option>
                    <option {if $paymentTermName=='1.5/12 Net 30'}selected{/if} value='1.5/12 Net 30'>1.5/12 Net 30 - 1.5 in 12 days Net in 30 days</option>
                    <option {if $paymentTermName=='1.5/15 Net 60'}selected{/if} value='1.5/15 Net 60'>1.5/15 Net 60 - 1.5% in 15 days Net in 60 days</option>
                    <option {if $paymentTermName=='10/30 NET 45'}selected{/if} value='10/30 NET 45'>10/30 NET 45 - 10 % Discount paid within 30 days Net 45</option>
                    <option {if $paymentTermName=='120 Days Net'}selected{/if} value='120 Days Net'>120 Days Net - 120 Days Net</option>
                    <option {if $paymentTermName=='14 Days Net'}selected{/if} value='14 Days Net'>14 Days Net - 14 Days Net</option>
                    <option {if $paymentTermName=='15 Days Net'}selected{/if} value='15 Days Net'>15 Days Net - 15 Days Net</option>
                    <option {if $paymentTermName=='2.5%/30 Net 30'}selected{/if} value='2.5%/30 Net 30'>2.5%/30 Net 30 - 2.5% in 30 days net in 30 days</option>
                    <option {if $paymentTermName=='2/12 Net 30'}selected{/if} value='2/12 Net 30'>2/12 Net 30 - 2% in 12 days/Net in 30 days</option>
                    <option {if $paymentTermName=='2/15 Net 45'}selected{/if} value='2/15 Net 45'>2/15 Net 45 - 2% in 15 days/Net in 45 days</option>
                    <option {if $paymentTermName=='20 Days Net'}selected{/if} value='20 Days Net'>20 Days Net - 20 Days Net</option>
                    <option {if $paymentTermName=='25 Days Net'}selected{/if} value='25 Days Net'>25 Days Net - 25 Days Net</option>
                    <option {if $paymentTermName=='3% 12 NET 12'}selected{/if} value='3% 12 NET 12'>3% 12 NET 12 - 3% 12 NET 12MTS</option>
                    <option {if $paymentTermName=='3% Annual Bill'}selected{/if} value='3% Annual Bill'>3% Annual Bill - 3% DISCOUNT (NOT INCLUDING TAX) IF PAID BY END OF MONTH</option>
                    <option {if $paymentTermName=='3/12 Net 30'}selected{/if} value='3/12 Net 30'>3/12 Net 30 - 3% in 12 days/Net in 30 days</option>
                    <option {if $paymentTermName=='3/15 Net 45'}selected{/if} value='3/15 Net 45'>3/15 Net 45 - 3% in 15 days/Net in 45 days</option>
                    <option {if $paymentTermName=='30 Days Net'}selected{/if} value='30 Days Net'>30 Days Net - 30 Days Net</option>
                    <option {if $paymentTermName=='30 NET'}selected{/if} value='30 NET'>30 NET - Net Due in 30 Days</option>
                    <option {if $paymentTermName=='30-120 FIX10'}selected{/if} value='30-120 FIX10'>30-120 FIX10 - SPLIT 4 INSTALLMENTS FIXED DAY 10</option>
                    <option {if $paymentTermName=='30-120 FIX15'}selected{/if} value='30-120 FIX15'>30-120 FIX15 - SPLIT 4 INSTALLMENTS FIXED DAY 15</option>
                    <option {if $paymentTermName=='30-120 FIX20'}selected{/if} value='30-120 FIX20'>30-120 FIX20 - SPLIT 4 INSTALLMENTS FIXED DAY 20</option>
                    <option {if $paymentTermName=='30-120 FIX5'}selected{/if} value='30-120 FIX5'>30-120 FIX5 - SPLIT 4 INSTALLMENTS FIXED DAY 05</option>
                    <option {if $paymentTermName=='30-120 SPLIT'}selected{/if} value='30-120 SPLIT'>30-120 SPLIT - SPLIT 4 INSTALLMENTS</option>
                    <option {if $paymentTermName=='30-150 FIX10'}selected{/if} value='30-150 FIX10'>30-150 FIX10 - SPLIT 5 INSTALLMENTS FIXED DAY 10</option>
                    <option {if $paymentTermName=='30-150 FIX15'}selected{/if} value='30-150 FIX15'>30-150 FIX15 - SPLIT 5 INSTALLMENTS FIXED DAY 15</option>
                    <option {if $paymentTermName=='30-150 FIX20'}selected{/if} value='30-150 FIX20'>30-150 FIX20 - SPLIT 5 INSTALLMENTS FIXED DAY 20</option>
                    <option {if $paymentTermName=='30-150 FIX5'}selected{/if} value='30-150 FIX5'>30-150 FIX5 - SPLIT 5 INSTALLMENTS FIXED DAY 05</option>
                    <option {if $paymentTermName=='30-150 SPLIT'}selected{/if} value='30-150 SPLIT'>30-150 SPLIT - SPLIT 5 INSTALLMENTS</option>
                    <option {if $paymentTermName=='30-180 FIX10'}selected{/if} value='30-180 FIX10'>30-180 FIX10 - SPLIT 6 INSTALLMENTS FIXED DAY 10</option>
                    <option {if $paymentTermName=='30-180 FIX15'}selected{/if} value='30-180 FIX15'>30-180 FIX15 - SPLIT 6 INSTALLMENTS FIXED DAY 15</option>
                    <option {if $paymentTermName=='30-180 FIX20'}selected{/if} value='30-180 FIX20'>30-180 FIX20 - SPLIT 6 INSTALLMENTS FIXED DAY 20</option>
                    <option {if $paymentTermName=='30-180 FIX5'}selected{/if} value='30-180 FIX5'>30-180 FIX5 - SPLIT 6 INSTALLMENTS FIXED DAY 05</option>
                    <option {if $paymentTermName=='30-180 SPLIT'}selected{/if} value='30-180 SPLIT'>30-180 SPLIT - SPLIT 6 INSTALLMENTS</option>
                    <option {if $paymentTermName=='30-210 FIX10'}selected{/if} value='30-210 FIX10'>30-210 FIX10 - SPLIT 7 INSTALLMENTS FIXED DAY 10</option>
                    <option {if $paymentTermName=='30-210 FIX15'}selected{/if} value='30-210 FIX15'>30-210 FIX15 - SPLIT 7 INSTALLMENTS FIXED DAY 15</option>
                    <option {if $paymentTermName=='30-210 FIX20'}selected{/if} value='30-210 FIX20'>30-210 FIX20 - SPLIT 7 INSTALLMENTS FIXED DAY 20</option>
                    <option {if $paymentTermName=='30-210 FIX5'}selected{/if} value='30-210 FIX5'>30-210 FIX5 - SPLIT 7 INSTALLMENTS FIXED DAY 05</option>
                    <option {if $paymentTermName=='30-210 SPLIT'}selected{/if} value='30-210 SPLIT'>30-210 SPLIT - SPLIT 7 INSTALLMENTS</option>
                    <option {if $paymentTermName=='30-240 FIX10'}selected{/if} value='30-240 FIX10'>30-240 FIX10 - SPLIT 8 INSTALLMENTS FIXED DAY 10</option>
                    <option {if $paymentTermName=='30-240 FIX15'}selected{/if} value='30-240 FIX15'>30-240 FIX15 - SPLIT 8 INSTALLMENTS FIXED DAY 15</option>
                    <option {if $paymentTermName=='30-240 FIX20'}selected{/if} value='30-240 FIX20'>30-240 FIX20 - SPLIT 8 INSTALLMENTS FIXED DAY 20</option>
                    <option {if $paymentTermName=='30-240 FIX5'}selected{/if} value='30-240 FIX5'>30-240 FIX5 - SPLIT 8 INSTALLMENTS FIXED DAY 05</option>
                    <option {if $paymentTermName=='30-240 SPLIT'}selected{/if} value='30-240 SPLIT'>30-240 SPLIT - SPLIT 8 INSTALLMENTS</option>
                    <option {if $paymentTermName=='30-60 FIX10'}selected{/if} value='30-60 FIX10'>30-60 FIX10 - SPLIT 2 INSTALLMENTS FIXED DAY 10</option>
                    <option {if $paymentTermName=='30-60 FIX15'}selected{/if} value='30-60 FIX15'>30-60 FIX15 - SPLIT 2 INSTALLMENTS FIXED DAY 15</option>
                    <option {if $paymentTermName=='30-60 FIX20'}selected{/if} value='30-60 FIX20'>30-60 FIX20 - SPLIT 2 INSTALLMENTS FIXED DAY 20</option>
                    <option {if $paymentTermName=='30-60 FIX5'}selected{/if} value='30-60 FIX5'>30-60 FIX5 - SPLIT 2 INSTALLMENTS FIXED DAY 05</option>
                    <option {if $paymentTermName=='30-60 SPLIT'}selected{/if} value='30-60 SPLIT'>30-60 SPLIT - SLIPT 2 INSTALLMENTS</option>
                    <option {if $paymentTermName=='30-90 FIX10'}selected{/if} value='30-90 FIX10'>30-90 FIX10 - SPLIT 3 INSTALLMENTS FIXED DAY 10</option>
                    <option {if $paymentTermName=='30-90 FIX15'}selected{/if} value='30-90 FIX15'>30-90 FIX15 - SPLIT 3 INSTALLMENTS FIXED DAY 15</option>
                    <option {if $paymentTermName=='30-90 FIX20'}selected{/if} value='30-90 FIX20'>30-90 FIX20 - SPLIT 3 INSTALLMENTS FIXED DAY 20</option>
                    <option {if $paymentTermName=='30-90 FIX5'}selected{/if} value='30-90 FIX5'>30-90 FIX5 - SPLIT 3 INSTALLMENTS FIXED DAY 05</option>
                    <option {if $paymentTermName=='30/60/90 Split'}selected{/if} value='30/60/90 Split'>30/60/90 Split - Split 3 Installments</option>
                    <option {if $paymentTermName=='35 Days Net'}selected{/if} value='35 Days Net'>35 Days Net - 35 Days Net</option>
                    <option {if $paymentTermName=='45 Days Net'}selected{/if} value='45 Days Net'>45 Days Net - 45 Days Net</option>
                    <option {if $paymentTermName=='50 Days Net'}selected{/if} value='50 Days Net'>50 Days Net - 50 Days Net</option>
                    <option {if $paymentTermName=='60 Days Net'}selected{/if} value='60 Days Net'>60 Days Net - 60 Days Net</option>
                    <option {if $paymentTermName=='70 Days Net'}selected{/if} value='70 Days Net'>70 Days Net - 70 Days Net</option>
                    <option {if $paymentTermName=='75 Days Net'}selected{/if} value='75 Days Net'>75 Days Net - 75 Days Net</option>
                    <option {if $paymentTermName=='8 Days Net'}selected{/if} value='8 Days Net'>8 Days Net - 8 Days Net</option>
                    <option {if $paymentTermName=='90 Days Net'}selected{/if} value='90 Days Net'>90 Days Net - 90 Days Net</option>
                    <option {if $paymentTermName=='All in Month'}selected{/if} value='All in Month'>All in Month - All in Month</option>
                    <option {if $paymentTermName=='DAY 01'}selected{/if} value='DAY 01'>DAY 01 - FIXED DAY 01</option>
                    <option {if $paymentTermName=='DAY 05'}selected{/if} value='DAY 05'>DAY 05 - FIXED DAY 05</option>
                    <option {if $paymentTermName=='DAY 10'}selected{/if} value='DAY 10'>DAY 10 - FIXED DAY 10</option>
                    <option {if $paymentTermName=='DAY 15'}selected{/if} value='DAY 15'>DAY 15 - FIXED DAY 15</option>
                    <option {if $paymentTermName=='DAY 20'}selected{/if} value='DAY 20'>DAY 20 - FIXED DAY 20</option>
                    <option {if $paymentTermName=='DAY 25'}selected{/if} value='DAY 25'>DAY 25 - FIXED DAY 25</option>
                    <option {if $paymentTermName=='DAY 30'}selected{/if} value='DAY 30'>DAY 30 - FIXED DAY 30</option>
                    <option {if $paymentTermName=='IMMEDIATE'}selected{/if} value='IMMEDIATE'>IMMEDIATE - Term for chargeback or debit memo</option>
                    <option {if $paymentTermName=='Month/30th +1m'}selected{/if} value='Month/30th +1m'>Month/30th +1m - Consolidated billing, all invoices in current month, due date 30th of next month</option>
                    <option {if $paymentTermName=='Month/30th +2m'}selected{/if} value='Month/30th +2m'>Month/30th +2m - Consolidated billing, all invoices in current month, due date 30th of second month</option>
                    <option {if $paymentTermName=='Month/30th +3m'}selected{/if} value='Month/30th +3m'>Month/30th +3m - Consolidated billing, all invoices in current month, due date 30th of third month</option>
                    <option {if $paymentTermName=='NET 10'}selected{/if} value='NET 10'>NET 10 - Net Due in 10 Days</option>
                    <option {if $paymentTermName=='SIIF CREDIT'}selected{/if} value='SIIF CREDIT'>SIIF CREDIT - 45 DAYS NET (URUGUAY GOVERNMENT)</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-4 my-1">
            <small id="errorCOTaxPayerId" class="form-text text-danger">&nbsp;{$errorCOTaxPayerId}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">CO Tax Payer Id </div>
                </div>
                <input type="text" class="form-control" id="COTaxPayerId" name="COTaxPayerId" value="{$COTaxPayerId}" placeholder="CO Tax Payer Id">
            </div>
        </div>
        <div class="col-sm-1 col-md-4 my-1">
            <small id="errorCODocumentType" class="form-text text-danger">&nbsp;{$errorCODocumentType}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">CO Document Type</div>
                </div>
                <select class="form-control" id="CODocumentType" name="CODocumentType" aria-describedby="" placeholder="">
                    <option value="">--</option>
                    <option {if $CODocumentType==='11'}selected{/if} value="11">Birth Civil Registration</option>
                    <option {if $CODocumentType==='12'}selected{/if} value="12">Identification Card</option>
                    <option {if $CODocumentType==='13'}selected{/if} value="13">Citizenship Identification</option>
                    <option {if $CODocumentType==='14'}selected{/if} value="14">Certificate of the Registrar for illiquid companies</option>
                    <option {if $CODocumentType==='15'}selected{/if} value="15">Inheritance illiquid issued by the notary</option>
                    <option {if $CODocumentType==='21'}selected{/if} value="21">Foreign Card</option>
                    <option {if $CODocumentType==='22'}selected{/if} value="22">Foreign Identification</option>
                    <option {if $CODocumentType==='31'}selected{/if} value="31">NIT</option>
                    <option {if $CODocumentType==='33'}selected{/if} value="33">Identification of different foreign NIT assigned DIAN</option>
                    <option {if $CODocumentType==='41'}selected{/if} value="41">Passport</option>
                    <option {if $CODocumentType==='46'}selected{/if} value="46">Diplomatic passport</option>
                    <option {if $CODocumentType==='42'}selected{/if} value="42">Foreign Document Type</option>
                    <option {if $CODocumentType==='43'}selected{/if} value="43">Document defined for external information</option>
                    <option {if $CODocumentType==='44'}selected{/if} value="44">Foreign legal entity identification document</option>
                </select>

            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-4 my-1">
            <small id="errorGlobalAttribute9" class="form-text text-danger">&nbsp;{$errorGlobalAttribute9}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Origin</div>
                </div>
                <select class="form-control" id="globalAttribute9" name="globalAttribute9" aria-describedby="$descriptor.shortNameHelp" placeholder="Global Attribute9">
                    <option {if $globalAttribute9==='DOMESTIC_ORIGIN'}selected{/if}>DOMESTIC_ORIGIN</option>
                    <option {if $globalAttribute9==='FOREIGN_ORIGIN'}selected{/if}>FOREIGN_ORIGIN</option>
                </select>
            </div>
        </div>
        <div class="col-sm-1 col-md-8 my-1">
            <small id="errorNature" class="form-text text-danger">&nbsp;{$errorNature}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Nature</div>
                </div>
                <select class="form-control" id="nature" name="nature" aria-describedby="" placeholder="">
                    <option {if $nature==='J'}selected{/if} value="J">Legal Entity</option>
                    <option {if $nature==='N'}selected{/if} value="N">Natural Person</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorFiscalClassCode" class="form-text text-danger">&nbsp;{$errorFiscalClassCode}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Type Code</div>
                </div>
                <select class="form-control" id="fiscalClassCode " name="fiscalClassCode" aria-describedby="fiscalClassCodeHelp" placeholder="Fiscal Class Code">
                    <option value="">--</option>
                    <option {if $fiscalClassCode ==='TKE_LATAM_CO_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_CO_CONTRIBUTOR">TKE_LATAM_CO_CONTRIBUTOR</option>
                </select>
            </div>
        </div>
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorRegimen" class="form-text text-danger">&nbsp;{$errorRegimen}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Regimen</div>
                </div>

                <select class="form-control" id="regimen" name="regimen" aria-describedby="regimenHelp">
                    <option value="">--</option>
                        <option {if $regimen ==='0'}selected{/if} value="0">0 - SIMPLIFICADO</option>
                        <option {if $regimen ==='2'}selected{/if} value="2">2 - COMUN</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorFiscalClassStartDate" class="form-text text-danger">&nbsp;{$errorFiscalClassStartDate}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Class Start Date</div>
                </div>
                <input class="form-control" id="fiscalClassStartDate" name="fiscalClassStartDate" aria-describedby="fiscalClassStartDateHelp"/>
                <script>
                    $('#fiscalClassStartDate').datepicker({
                        uiLibrary: 'bootstrap4',
                        format:'dd-mm-yyyy',
                        value: '{$fiscalClassStartDate|date_format:"%d-%m-%Y"}'
                    });
                </script>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-4 my-1">
            <small id="errorSpecialCustomer class="form-text text-danger">&nbsp;{$errorSpecialCustomer}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Special Customer <small id="errorSpecialCustomer" class="form-text text-danger">&nbsp;{$errorSpecialCustomer}</small></div>
                </div>
                <input type="checkbox" {($specialCustomer==true)?'checked':''} style="margin:10px;" id="specialCustomer" name="specialCustomer">
            </div>
        </div>
        <div class="col-sm-1 col-md-4 my-1">
            <small id="errorPrimaryFlag" class="form-text text-danger">&nbsp;{$errorPrimaryFlag}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Primary Flag</div>
                </div>
                <input type="checkbox" {($primaryFlag==true)?'checked':''} style="margin:10px;" id="primaryFlag" name="primaryFlag">
            </div>
        </div>
        <div class="col-sm-1 col-md-4 my-1">
            <small id="errorGlobalFlag" class="form-text text-danger">&nbsp;{$errorGlobalFlag}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Global Flag </div>
                </div>
                <input type="checkbox" {($globalFlag==true)?'checked':''}  style="margin:10px;" id="globalFlag" name="globalFlag">
            </div>
        </div>
    </div>
    {/if}
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorAliases" class="form-text text-danger">&nbsp;{$errorAliases}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Aliases</div>
                </div>
                <textarea type="text" class="form-control" id="aliases" name="aliases" value="" placeholder="Aliases">{$aliases}</textarea>
            </div>
            <label class="alert-info text-info"><i class="fas fa-id-card fa-sm fa-fw"></i> An alias is an alternative ID fot this item.  Use this field to unify records to be migrated to ORACLE. Use a comma-separated list, with no spaces (e.g. PA1000,PA1002,CO1003)</label>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorObservations" class="form-text text-danger">&nbsp;{$errorObservations}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Observations</div>
                </div>
                <textarea type="text" class="form-control" id="observations" name="observations" value="" placeholder="Observations" rows="5">{$observations}</textarea>
            </div>
            <label class="alert-info text-info"><i class="fas fa-bug fa-lg fa-fw"></i> Please help us detect bugs.  Use this field to annotate special cases, cleansing decisions, and relevant issues related to this record.</label>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">{$actionLabel}</button>
                <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
            </div>
        </div>
    </div>
</form>

<br>
<h4>Customer Addresses &nbsp;<a data-toggle="tooltip" data-placement="top" title="add a new Address to {$origSystemCustomerRef}" href="{base_url()}CA/createNew/{$id}" style="cursor:pointer"><small><i class="text-primary fas fa-plus-circle fa-sm fa-fw"></i></small></a></h4>
{if isset($addresses) && (sizeof($addresses)>0)}
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th scope="col"></th>
            <th>#</th>
            <th>Action</th>
            <th>Address Ref</th>
            <th>Primary Site</th>
            <th>Site Use</th>
            <th>Address Line 1</th>
            <th>City</th>
            <th>Province</th>
            <th>State</th>
            <th>Country</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$addresses item=a}
            <tr class="{if ($a.dismissed)} dismissed {elseif (($a.locallyValidated)&&($a.crossRefValidated)) } alert-success {else} alert-warning {/if} clickable-row" style="cursor:pointer" data-href='{base_url()}CA/customEditCleansed/{$a.country}/{$a.id}'>
                <th scope="row">
                    <a href="{base_url()}CA/editCleansed/{$a.id}" data-toggle="tooltip" data-placement="top" title="inspect all fields from address {$a.origSystemAdressReference}"><i class="text-primary fas fa-edit fa-sm fa-fw"></i></a>
                    <a href="{base_url()}CA/toggle/{$a.id}" data-toggle="tooltip" data-placement="top" title="{if ($c.dismissed)}Recall{else}Dismiss{/if} {$a.origSystemAdressReference}"><i class="text-primary fas fa-minus-circle fa-sm fa-fw"></i></a>
                </th>
                <th scope="row">{$a.id}</th>
                <td>{$a.action|truncate:40}</td>
                <td>{$a.origSystemAdressReference}</td>
                <td>{($a.primarySiteUseFlag===true)?'Y':'N'}</td>
                <td>{$a.siteUseCode}</td>
                <td>{$a.addressLine1|truncate:20}</td>
                <td>{$a.city}</td>
                <td>{$a.province}</td>
                <td>{$a.state}</td>
                <td>{$a.addressCountry}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{else}
    <p class="p-3 alert-warning">There are no addresses at this customer.  You may add a new address <a data-toggle="tooltip" data-placement="top" title="add a new Address to {$origSystemCustomerRef}" href="{base_url()}CustomerAddress/createNew/{$id}">here</a></p>
{/if}
<br/>
<br/>
<br/>
<br/>
<br/>
