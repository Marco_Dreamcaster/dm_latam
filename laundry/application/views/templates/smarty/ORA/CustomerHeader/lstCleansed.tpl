<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="navbar-nav mr-auto">
            <h5 class="nav p-2"><i class="fas fa-lg fa-industry fa-fw m-2 text-stage"></i>&nbsp;<span class="m-2"><span class="flag-icon flag-icon-{strtolower($country)}"></span> {strtoupper($country)} STG Customers (cleansed)</span></h5>
            <ul class="navbar-nav mr-auto">
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_blank" href="{base_url()}Customers/dirty/{$country}" data-toggle="tooltip" data-placement="top" title="dirty Customers"><i class="fas fa-trash-alt fa-sm fa-fw"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-warning" target="_blank" href="{base_url()}Maps/ORAGeoCodes/list" data-toggle="tooltip" data-placement="top" title="renewed Oracle Geo Codes"><i class="fas fa-map-marker-alt fa-sm fa-fw text-white"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_self" href="{base_url()}RefreshFF/Customers/{$country}" data-toggle="tooltip" data-placement="top" title="Refresh datasets"><i class="fas fa-redo fa-sm fa-fw"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_self" href="{base_url()}DownloadFF/Customers/{$country}" data-toggle="tooltip" data-placement="top" title="Dowload current dataset"><i class="fas fa-file-archive fa-sm fa-fw"></i>&nbsp;ORA Zip</a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-success" target="_blank" href="{base_url()}IDL/Navigate/navigateGroup/{$country}/Customers" data-toggle="tooltip" data-placement="top" title="specification for {$country} Customers group"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-success" target="_blank" href="{base_url()}scripts/00.COM/master/01.Customers/01.IDL/TKE_LA_IDL_FD_02_CUSTOMERS_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="latest specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                </li>
            </ul>
        </div>
        <div class="dropdown">
            <button class="btn btn-success  dropleft dropdown-toggle mr-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Inspect dataset <i class="fas fa-cloud-download-alt fa-sm fa-fw ml-1 mr-1"></i>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="{base_url()}DownloadFF/Customers/{$country}"><i class="fas fa-file-archive fa-sm fa-fw m-1"></i> Download ZIP</a>
                <a class="dropdown-item" href="{base_url()}InspectFF/Customers/{$country}/TKE_LA_FD_02_CUSTOMER_HEADER" target="_blank"><i class="fas fa-eye fa-sm fa-fw m-1"></i> CustomerHeader</a>
                <a class="dropdown-item" href="{base_url()}InspectFF/Customers/{$country}/TKE_LA_FD_02_CUSTOMER_CROSS_REF" target="_blank"><i class="fas fa-eye fa-sm fa-fw m-1"></i> CustomerCrossRef</a>
                <a class="dropdown-item" href="{base_url()}InspectFF/Customers/{$country}/TKE_LA_FD_02_CUSTOMER_ADDRESS" target="_blank"><i class="fas fa-eye fa-sm fa-fw m-1"></i> CustomerAddress</a>
                <a class="dropdown-item" href="{base_url()}InspectFF/Customers/{$country}/TKE_LA_FD_02_CUSTOMER_CONTACT" target="_blank"><i class="fas fa-eye fa-sm fa-fw m-1"></i> CustomerContact</a>
                <a class="dropdown-item" href="{base_url()}InspectFF/Customers/{$country}/TKE_LA_FD_02_CUSTOMER_ORG_CONTACTS" target="_blank"><i class="fas fa-eye fa-sm fa-fw m-1"></i> Org Contacts</a>
            </div>
        </div>
        <div class="dropdown">
            <button class="btn btn-success  dropleft dropdown-toggle mr-4" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Custom Reports <i class="fas fa-ruler fa-sm fa-fw ml-1 mr-1"></i>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="{base_url()}RefreshFF/CustomersAudit/{$country}"><i class="fas fa-redo fa-sm fa-fw m-1"></i> refresh Audit</a>
                <a class="dropdown-item" href="{base_url()}DownloadFF/CustomersAudit/{$country}"><i class="fas fa-file-archive fa-sm fa-fw m-1"></i> download Audit ZIP</a>
                <a class="dropdown-item" href="{base_url()}InspectFF/CustomersAudit/{$country}/TKE_LA_FD_02_CUSTOMERS_AUDIT" target="_blank"><i class="fas fa-eye fa-sm fa-fw m-1"></i> Customers Audit</a>
                <a class="dropdown-item" href="{base_url()}InspectFF/CustomersAudit/{$country}/TKE_LA_FD_02_CUSTOMERS_FISCAL_CLASSIFICATION_AUDIT" target="_blank"><i class="fas fa-eye fa-sm fa-fw m-1"></i>Fiscal Classifications</a>
            </div>
        </div>
        &nbsp;
    </div>
</nav>
<br/>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade active show" id="pills-data" role="tabpanel" aria-labelledby="pills-data-tab">
        <div class="row mb-3">
            <div class="col-9">
                <ul class="pagination">
                    <li>
                        <small class="text-muted m-4"> by {$sortOrder}&nbsp;{$sortDirection}</small>
                    </li>
                    {if $isPaginationNeeded}
                        <li class="page-item {if $currentPage==0}active{/if}">
                            <a class="page-link" href="{base_url()}{$listAction}0/{$sessionSelCol}/{if $sessionSelAsc}1{else}0{/if}" aria-label="First">
                                First
                            </a>
                        </li>
                        {for $foo=1 to ($lastPage-1)}
                            <li class="page-item {if $currentPage==$foo}active{/if}"><a class="page-link" href="{base_url()}{$listAction}{$foo}/{$sessionSelCol}/{if $sessionSelAsc}1{else}0{/if}">{$foo}</a></li>
                        {/for}
                        <li class="page-item {if $currentPage==$lastPage}active{/if}">
                            <a class="page-link" href="{base_url()}{$listAction}{$lastPage}/{$sessionSelCol}/{if $sessionSelAsc}1{else}0{/if}" aria-label="Last">
                                Last
                            </a>
                        </li>
                    {/if}
                </ul>
            </div>
            <div class="col-3">
            </div>
        </div>
        <table class="table table-hover table-sm">
            <thead>
            <tr>
                <th></th>
                <th scope="col">#</th>
                <th>action</th>
                <th>Customer Name</th>
                <th>Customer Ref</th>
                <th>Customer Profile</th>
                <th>Tax Payer ID</th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$cleansedVOs item=p}
                <tr class="{if ($p.dismissed)} dismissed {elseif (($p.locallyValidated)&&($p.crossRefValidated)) } alert-success {else} alert-warning {/if} clickable-row" style="cursor:pointer" data-href='{base_url()}Customers/customEditCleansed/{$p.country}/{$p.id}' >
                    <th>
                        <a href="{base_url()}Customers/editCleansed/{$p.id}" data-toggle="tooltip" data-placement="top" title="default view for {$p.customerName}" class="text-TKE"><i class="text-primary fas fa-edit fa-sm fa-fw"></i></a>
                        &nbsp;
                        <a href="{base_url()}Customers/customEditCleansed/{$p.id}" data-toggle="tooltip" data-placement="top" title="customized view for {$p.customerName}" class="text-TKE"><i class="text-primary fas fa-eye fa-sm fa-fw"></i></a>
                    </th>
                    <th scope="row"><a href="{base_url()}Customers/customEditCleansed/{$p.country}/{$p.id}" data-toggle="tooltip" data-placement="top" title="Customer {$p.origSystemCustomerRef}" class="text-TKE"><small>{$p.id}</small></a></th>
                    <td>
{if (strlen($p.observations)>0)}
    <i class="text-normal fas fa-sticky-note fa-sm fa-fw" title='OBS: {$p.observations|truncate:200}'></i>
{/if}
{if ($p.dismissed)}
    <i class="text-muted fas fa-thumbs-down fa-sm fa-fw"></i>
{elseif $p.action!=''}
    <a href="{base_url()}Customers/customEditCleansed/{$p.country}/{$p.id}" data-toggle="tooltip" data-placement="top" title="{$p.action} - updated at {$p.lastUpdated|date_format:"%d-%m-%Y %H:%M:%S"}" class="text-TKE text-warning"><small>{$p.action|truncate:20}</small></a>
{elseif (($p.locallyValidated)&&($p.crossRefValidated)) }

{else}
    <i data-toggle="tooltip" data-placement="top" title='problem in a dependency' class="text-warning fas fa-leaf fa-sm fa-fw"></i>
{/if}

                    </td>
                    <td>{$p.customerName}</td>
                    <td>{$p.origSystemCustomerRef}</td>
                    <td>{$p.customerProfile}</td>
                    <td>{$p.taxPayerId}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
        <br/>
        <br/>
    </div>
    <div class="tab-pane fade show" id="pills-stats" role="tabpanel" aria-labelledby="pills-stats-tab">
        <div class="row p-1">
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Cleansed</h5>
                        <h6 class="card-subtitle mb-2 text-muted">total rows moved from pre-staging table to stage</h6>
                        <h1 class="card-text">{$total}</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="row p-1">
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Recommended</h5>
                        <h6 class="card-subtitle mb-2 text-muted">according to fixed functional criteria</h6>
                        <h1 class="card-text">{$recommendedCleansed}</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Dismissed</h5>
                        <h6 class="card-subtitle mb-2 text-muted">manually removed from migration by KU</h6>
                        <h1 class="card-text">{$dismissed}</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Multiple Aliases</h5>
                        <h6 class="card-subtitle mb-2 text-muted">referred by more than one SIGLA ID (alias)</h6>
                        <h1 class="card-text">{$cleansedMultipleAliases}</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="row p-1">
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Locally Validated</h5>
                        <h6 class="card-subtitle mb-2 text-muted">matches all criteria for local validation</h6>
                        <h1 class="card-text">{$cleansedLocallyValidated}</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">CrossRef Validated</h5>
                        <h6 class="card-subtitle mb-2 text-muted">has no dependencies on other Groups</h6>
                        <h1 class="card-text">{$cleansedCrossRefValidated}</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Ready for Output</h5>
                        <h6 class="card-subtitle mb-2 text-muted">matches all criteria (and it's not dismissed)</h6>
                        <h1 class="card-text">{$cleansedValidatedForOutput}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<br/>
<br/>
<br/>
<br/>
