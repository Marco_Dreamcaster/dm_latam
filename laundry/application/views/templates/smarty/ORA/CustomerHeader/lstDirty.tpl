<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="navbar-nav mr-auto">
            <h5 class="nav p-2"><i class="fas fa-lg fa-trash-alt fa-fw m-2 text-pre-stage"></i>&nbsp;<span class="m-2"><span class="flag-icon flag-icon-{strtolower($country)}"></span> {strtoupper($country)} pre-STG Customers (dirty)</span></h5>
            <ul class="navbar-nav mr-auto">
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_blank" href="{base_url()}Customers/cleansed/{$country}" data-toggle="tooltip" data-placement="top" title="cleansed Customers"><i class="fas fa-industry fa-sm fa-fw"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_blank" href="{base_url()}Maps/ORAGeoCodes" data-toggle="tooltip" data-placement="top" title="Oracle Geo Codes"><i class="fas fa-map-marker-alt fa-sm fa-fw"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-success" target="_blank" href="{base_url()}IDL/Navigate/navigateGroup/{$country}/Customers" data-toggle="tooltip" data-placement="top" title="specification for {$country} Customers group"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<br/>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade active show" id="pills-data" role="tabpanel" aria-labelledby="pills-data-tab">
        <div class="row mb-3">
            <div class="col-9">
                {if $isPaginationNeeded}
                    <ul class="pagination">
                        <li class="page-item {if $currentPage==0}active{/if}">
                            <a class="page-link" href="{base_url()}{$listAction}0" aria-label="First">
                                First
                            </a>
                        </li>
                        {for $foo=1 to ($lastPage-1)}
                            <li class="page-item {if $currentPage==$foo}active{/if}"><a class="page-link" href="{base_url()}{$listAction}{$foo}">{$foo}</a></li>
                        {/for}
                        <li class="page-item {if $currentPage==$lastPage}active{/if}">
                            <a class="page-link" href="{base_url()}{$listAction}{$lastPage}" aria-label="Last">
                                Last
                            </a>
                        </li>
                    </ul>
                {else}
                    &nbsp;
                {/if}

            </div>
            <div class="col-3">
            </div>
        </div>
        <table class="table table-hover table-sm">
            <thead>
            <tr>
                <th></th>
                <th scope="col">#</th>
                <th>Customer Name</th>
                <th>Party Ref</th>
                <th>Customer Ref</th>
                <th>Profile</th>
                <th>action</th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$dirtyVOs item=p}
                <tr class="{if ($p.recommended) } alert-success {else} alert-warning {/if} clickable-row" style="cursor:pointer" data-href='{base_url()}Customers/editDirty/{$p.id}'>
                    <td><a href="{base_url()}Customers/editDirty/{$p.id}" data-toggle="tooltip" data-placement="top" title="inspect all fields from dirty Customer {$p.customerName}"><i class="text-primary fas fa-edit fa-lg fa-fw"></i></a></td>
                    <td scope="row">{$p.id}</td>
                    <td>{$p.customerName}</td>
                    <td>{$p.origSystemPartyRef}</td>
                    <td>{$p.origSystemCustomerRef}</td>
                    <td>{$p.customerProfile}</td>
                    <td>{$p.action}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</div>
<br/>
<br/>
<br/>
<br/>
<br/>
