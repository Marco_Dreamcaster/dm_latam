<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><span class="flag-icon flag-icon-{strtolower($country)}" style="margin-top:10px;"></span>&nbsp;<span class="mt-2">{strtoupper($country)} {$title}</span>&nbsp;</h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Customers/customEditCleansed/{$country}/{$id}" data-toggle="tooltip" data-placement="top" title="quick view of customer {$origSystemCustomerRef}"><i class="fas fa-eye fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Customers/validate/{$id}" data-toggle="tooltip" data-placement="top" title="manual validation of customer {$origSystemCustomerRef}"><i class="fas fa-check-circle fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-secondary" target="_self" href="{base_url()}Customers/toggle/{$id}" data-toggle="tooltip" data-placement="top" title="{if ($dismissed)}recall{else}dismiss{/if} customer {$origSystemCustomerRef}"><i class="fas fa-minus-circle fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-danger" target="_self" data-toggle="modal" data-target="#discardModal" title="discard customer {$origSystemCustomerRef} - completely remove from stage"><i class="fas text-white fa-eraser fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-success" target="_blank" href="{base_url()}IDL/Navigate/navigate/{$country}/Customers/CustomerHeader" data-toggle="tooltip" data-placement="top" title="specification for customer header"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-success" target="_blank" href="{base_url()}scripts/00.COM/master/01.Customers/01.IDL/TKE_LA_IDL_FD_02_CUSTOMERS_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="latest specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-secondary" target="_self" href="{base_url()}{$cancelAction}" data-toggle="tooltip" data-placement="top" title="cancel, and go back"><i class="fas fa-ban fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>

<div id="discardModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="discardModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="discardModalLongTitle">Confirm Discard</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"/>
            </div>
            <div class="modal-body">
                <p>You're about to discard cleansed information on customer {$customerName}.<br/> This cannot be undone.<br/> Are you sure?</p>
            </div>
            <div class="modal-footer">
                <a href="{base_url()}Customers/discard/{$id}" class="btn btn-danger"><i class="fas fa-eraser fa-sm fa-fw"></i> Yes, revert to dirty state</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal" ><i class="fas fa-ban fa-sm fa-fw"></i> No, forget about it. </button>
            </div>
        </div>
    </div>
</div>

<div id="dismissModalLong" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="dismissModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="dismissModalLongLongTitle">Please Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"/>
            </div>
            <div class="modal-body">
                <p>
                    Please write down the reasons for {if ($dismissed)}recalling{else}dismissing{/if} this Customer, please use the Observations.<br/>
                    This will automatically {if ($dismissed)}recall{else}dismiss{/if} all associated CustomerAddresses and CustomerContacts.<br/>
                    Are you sure?
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No, forget about it.</button>
                <a href="{base_url()}Customers/toggle/{$id}" class="btn btn-danger">Yes, {if ($dismissed)}recall{else}dismiss{/if} this row</a>
            </div>
        </div>
    </div>
</div>
{if ($dismissed)}
    <div class="row">
        <div class="col-sm-4 p-2">
            <p class="dismissed text-center p-2">dismissed row, not going to flat file</p>
        </div>
    </div>
{else}
    <div class="row">
        <div class="col-sm-4 p-2">
            <p class="alert-success text-center p-2">active row</p>
        </div>
        <div class="col-sm-4 p-2">
            {if (isset($cleansingAction)&&($cleansingAction!=''))}
                <p class="alert-warning text-center p-2">{$cleansingAction}</p>
            {else}
                <p class="alert-success text-center p-2">locally validated - no action required at header level</p>
            {/if}
        </div>
        <div class="col-sm-4 p-2">
            {if !($crossRefValidated)}
                <p class="alert-warning text-center p-2">problems in dependencies - check CustomerAddress or CustomerContact</p>
            {else}
                <p class="alert-success text-center p-2">no problems, going to flat file</p>
            {/if}
        </div>
    </div>
{/if}


