<h4>Confirm <i class="fas fa-sm fa-user-tie fa-fw mt-2"></i> Master (parent) - <i class="fas fa-sm fa-user-tag fa-fw mt-2"></i> Merged (child)</h4>
<br/>
<form id="confirmItemAssociation" name="confirmItemAssociation" action="{base_url()}{$confirmAction}" method="post" accept-charset="UTF-8">
    <h5 class="alert-success p-2"><i class="fas fa-lg fa-user-tie fa-fw mt-2 mr-2"></i>Master Item (parent) #{$parent.origItemNumber} from {$parent.country} <span style="vertical-align: middle" class="flag-icon flag-icon-{strtolower($parent.country)}"/></h5>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <a class="btn {if (($parent.locallyValidated)&&($parent.crossRefValidated)) }btn-success{else}btn-warning{/if}" href="{base_url()}{$actionPeekParentUrl}" title="peek at item {$parent.origItemNumber}" target="_blank"><i class="text-white fas fa-eye fa-sm"></i></a>
                    <div class="input-group-text"> Original Item Number (SIGLA)</div>
                </div>
                <input type="text" class="form-control" id="origItemNumber" name="origItemNumber" value="{$parent.origItemNumber}" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Item Number (ORACLE)</div>
                </div>
                <input type="text" class="form-control" id="itemNumber" name="itemNumber" value="{$parent.itemNumber}" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Description (EN)</div>
                </div>
                <input type="text" class="form-control" id="description" name="description" value="{$parent.description}" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Observations</div>
                </div>
                <input type="text" class="form-control" id="description" name="description" value="{$parent.observations}" disabled>
            </div>
        </div>
    </div>
    <br/>
    <br/>
    <h5 class="{if (($parent.country == $child.country)) }dismissed{else}alert-merged{/if} p-2"> <i class="fas fa-lg fa-user-tag fa-fw  mt-2 mr-2"></i>Merged Item (child) #{$child.origItemNumber} from {$child.country} <span style="vertical-align: middle" class="flag-icon flag-icon-{strtolower($child.country)}"/> </h5>
    {if (($parent.country == $child.country)) }
        <div class="alert-danger p-2">
            <p>child items merged to parents on same country are dismissed by default</p>
            <p>items child com master do mesmo pa�s s�o automaticamente dispensados (dismissed) (aparecem em cinza)</p>
        </div>
    {/if}
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <a class="btn {if (($child.locallyValidated)&&($child.crossRefValidated)) }btn-success{else}btn-warning{/if}" href="{base_url()}{$actionPeekChildUrl}" title="peek at item {$child.origItemNumber}" target="_blank"><i class="text-white fas fa-eye fa-sm"></i></a>
                    <div class="input-group-text">Original Item Number (SIGLA)</div>
                </div>
                <input type="text" class="form-control" id="origItemNumber" name="origItemNumber" value="{$child.origItemNumber}" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Item Number (ORACLE)</div>
                </div>
                <input type="text" class="form-control" id="itemNumber" name="itemNumber" value="{$child.itemNumber}" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Description (EN)</div>
                </div>
                <input type="text" class="form-control" id="description" name="description" value="{$child.description}" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Observations</div>
                </div>
                <input type="text" class="form-control" id="description" name="description" value="{$child.observations}" disabled>
            </div>
        </div>
    </div>
    <br/>


    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$confirmLabel}&nbsp;<i class="text-white fas fa-link fa-sm"></i></button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel&nbsp;<i class="text-white fas fa-ban fa-sm"></i></a>
    </div>

</form>
<br/>

<br/>
<br/>

