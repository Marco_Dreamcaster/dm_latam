<form id="customEditItem" name="customEditItem" action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorOrigItemNumber" class="form-text text-danger">&nbsp;{$errorOrigItemNumber}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Original Item Number (SIGLA)</div>
                </div>
                <input type="text" class="form-control" id="origItemNumber" name="origItemNumber" value="{$origItemNumber}" placeholder="Orig Item Number" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorItemNumber" name="errorItemNumber" class="form-text text-danger">&nbsp;{$errorItemNumber}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Item Number (ORACLE)</div>
                </div>
                <input type="text" class="form-control" id="itemNumber" name="itemNumber" value="{$itemNumber}" placeholder="Item Number">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorDescription" class="form-text text-danger">&nbsp;{$errorDescription}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Description (EN)</div>
                </div>
                <input type="text" class="form-control" id="description" name="description" value="{$description}" placeholder="Description">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorDescriptionEsa" class="form-text text-danger">&nbsp;{$errorDescriptionEsa}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Description (ESA)</div>
                </div>
                <input type="text" class="form-control" id="descriptionEsa" name="descriptionEsa" value="{$descriptionEsa}" placeholder="Description Esa">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorObservations" class="form-text text-danger">&nbsp;{$errorLongDescription}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Long Description</div>
                </div>
                <textarea type="text" class="form-control" id="longDescription" name="longDescription" rows="5">{$longDescription}</textarea>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorTemplateName" class="form-text text-danger">&nbsp;{$errorTemplateName}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Template Name</div>
                </div>
                <select class="form-control" id="templateName" name="templateName" aria-describedby="templateNameHelp" placeholder="Template Name">
                    <option {if $templateName==='LA98'}selected{/if} value='LA98'>LA98</option>
                    <option {if $templateName==='LA01ASE'}selected{/if} value='LA01ASE'>LA01ASE</option>
                    <option {if $templateName==='LA01ASM'}selected{/if} value='LA01ASM'>LA01ASM</option>
                    <option {if $templateName==='LA02ESE'}selected{/if} value='LA02ESE'>LA02ESE</option>
                    <option {if $templateName==='LA02ESM'}selected{/if} value='LA02ESM'>LA02ESM</option>
                    <option {if $templateName==='LA02RAE'}selected{/if} value='LA02RAE'>LA02RAE</option>
                    <option {if $templateName==='LA02RAM'}selected{/if} value='LA02RAM'>LA02RAM</option>
                    <option {if $templateName==='LA03ASE'}selected{/if} value='LA03ASE'>LA03ASE</option>
                    <option {if $templateName==='LA03ASM'}selected{/if} value='LA03ASM'>LA03ASM</option>
                    <option {if $templateName==='LA04ASE'}selected{/if} value='LA04ASE'>LA04ASE</option>
                    <option {if $templateName==='LA04ASM'}selected{/if} value='LA04ASM'>LA04ASM</option>
                    <option {if $templateName==='LA05ASE'}selected{/if} value='LA05ASE'>LA05ASE</option>
                    <option {if $templateName==='LA05ASM'}selected{/if} value='LA05ASM'>LA05ASM</option>
                    <option {if $templateName==='LA05ESE'}selected{/if} value='LA05ESE'>LA05ESE</option>
                    <option {if $templateName==='LA05ESM'}selected{/if} value='LA05ESM'>LA05ESM</option>
                    <option {if $templateName==='LA05RAE'}selected{/if} value='LA05RAE'>LA05RAE</option>
                    <option {if $templateName==='LA05RAM'}selected{/if} value='LA05RAM'>LA05RAM</option>
                    <option {if $templateName==='LA06AEE'}selected{/if} value='LA06AEE'>LA06AEE</option>
                    <option {if $templateName==='LA06AEM'}selected{/if} value='LA06AEM'>LA06AEM</option>
                    <option {if $templateName==='LA07ASE'}selected{/if} value='LA07ASE'>LA07ASE</option>
                    <option {if $templateName==='LA07ASM'}selected{/if} value='LA07ASM'>LA07ASM</option>
                    <option {if $templateName==='LA08FIE'}selected{/if} value='LA08FIE'>LA08FIE</option>
                    <option {if $templateName==='LA08FIM'}selected{/if} value='LA08FIM'>LA08FIM</option>
                    <option {if $templateName==='LA09ASE'}selected{/if} value='LA09ASE'>LA09ASE</option>
                    <option {if $templateName==='LA09ASM'}selected{/if} value='LA09ASM'>LA09ASM</option>
                    <option {if $templateName==='LA10ASE'}selected{/if} value='LA10ASE'>LA10ASE</option>
                    <option {if $templateName==='LA10ASM'}selected{/if} value='LA10ASM'>LA10ASM</option>
                    <option {if $templateName==='LA10ESE'}selected{/if} value='LA10ESE'>LA10ESE</option>
                    <option {if $templateName==='LA10ESM'}selected{/if} value='LA10ESM'>LA10ESM</option>
                    <option {if $templateName==='LA10RAE'}selected{/if} value='LA10RAE'>LA10RAE</option>
                    <option {if $templateName==='LA10RAM'}selected{/if} value='LA10RAM'>LA10RAM</option>
                    <option {if $templateName==='LA11ASE'}selected{/if} value='LA11ASE'>LA11ASE</option>
                    <option {if $templateName==='LA11ASM'}selected{/if} value='LA11ASM'>LA11ASM</option>
                    <option {if $templateName==='LA11ESE'}selected{/if} value='LA11ESE'>LA11ESE</option>
                    <option {if $templateName==='LA11ESM'}selected{/if} value='LA11ESM'>LA11ESM</option>
                    <option {if $templateName==='LA11RAE'}selected{/if} value='LA11RAE'>LA11RAE</option>
                    <option {if $templateName==='LA11RAM'}selected{/if} value='LA11RAM'>LA11RAM</option>
                    <option {if $templateName==='LA12ASE'}selected{/if} value='LA12ASE'>LA12ASE</option>
                    <option {if $templateName==='LA12ASM'}selected{/if} value='LA12ASM'>LA12ASM</option>
                    <option {if $templateName==='LA12ESE'}selected{/if} value='LA12ESE'>LA12ESE</option>
                    <option {if $templateName==='LA12ESM'}selected{/if} value='LA12ESM'>LA12ESM</option>
                    <option {if $templateName==='LA12RAE'}selected{/if} value='LA12RAE'>LA12RAE</option>
                    <option {if $templateName==='LA12RAM'}selected{/if} value='LA12RAM'>LA12RAM</option>
                    <option {if $templateName==='LA13ASE'}selected{/if} value='LA13ASE'>LA13ASE</option>
                    <option {if $templateName==='LA13ASM'}selected{/if} value='LA13ASM'>LA13ASM</option>
                    <option {if $templateName==='LA14ASE'}selected{/if} value='LA14ASE'>LA14ASE</option>
                    <option {if $templateName==='LA14ASM'}selected{/if} value='LA14ASM'>LA14ASM</option>
                    <option {if $templateName==='LA99ASE'}selected{/if} value='LA99ASE'>LA99ASE</option>
                    <option {if $templateName==='LA99ASM'}selected{/if} value='LA99ASM'>LA99ASM</option>
                    <option {if $templateName==='LA99ESE'}selected{/if} value='LA99ESE'>LA99ESE</option>
                    <option {if $templateName==='LA99ESM'}selected{/if} value='LA99ESM'>LA99ESM</option>
                    <option {if $templateName==='LA99RAE'}selected{/if} value='LA99RAE'>LA99RAE</option>
                    <option {if $templateName==='LA99RAM'}selected{/if} value='LA99RAM'>LA99RAM</option>
                    <option {if $templateName==='LA99HEH'}selected{/if} value='LA99HEH'>LA99HEH</option>
                    <option {if $templateName==='LA99COI'}selected{/if} value='LA99COI'>LA99COI</option>
                    <option {if $templateName==='LA99COC'}selected{/if} value='LA99COC'>LA99COC</option>
                    <option {if $templateName==='LA99PPP'}selected{/if} value='LA99PPP'>LA99PPP</option>
                    <option {if $templateName==='LA99ROR'}selected{/if} value='LA99ROR'>LA99ROR</option>
                    <option {if $templateName==='LA00SES'}selected{/if} value='LA00SES'>LA00SES</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorCategorySetName" class="form-text text-danger">&nbsp;{$errorCategorySetName}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Category Set Name (LPA)</div>
                </div>
                <select class="form-control" id="itemCategorySetName" name="itemCategorySetName">
                    <option {if $itemCategory['categorySetName']==='TKE PO Item Category'}selected{/if} value='TKE PO Item Category'>TKE PO Item Category</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorCategoryName" class="form-text text-danger">&nbsp;{$errorCategoryName}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Category Name (SCC Codes) {$itemCategory['concatCategoryName']}</div>
                </div>
                <select class="form-control" id="itemCategoryName" name="itemCategoryName">
                    <option {if $itemCategory['concatCategoryName']==='10039800'}selected{/if} value='10039800'>10039800</option>
                    <option {if $itemCategory['concatCategoryName']==='10159800'}selected{/if} value='10159800'>10159800</option>
                    <option {if $itemCategory['concatCategoryName']==='10180298'}selected{/if} value='10180298'>10180298</option>
                    <option {if $itemCategory['concatCategoryName']==='10219800'}selected{/if} value='10219800'>10219800</option>
                    <option {if $itemCategory['concatCategoryName']==='20240998'}selected{/if} value='20240998'>20240998</option>
                    <option {if $itemCategory['concatCategoryName']==='20241198'}selected{/if} value='20241198'>20241198</option>
                    <option {if $itemCategory['concatCategoryName']==='20241398'}selected{/if} value='20241398'>20241398</option>
                    <option {if $itemCategory['concatCategoryName']==='20249800'}selected{/if} value='20249800'>20249800</option>
                    <option {if $itemCategory['concatCategoryName']==='20270200'}selected{/if} value='20270200'>20270200</option>
                    <option {if $itemCategory['concatCategoryName']==='20270300'}selected{/if} value='20270300'>20270300</option>
                    <option {if $itemCategory['concatCategoryName']==='20271000'}selected{/if} value='20271000'>20271000</option>
                    <option {if $itemCategory['concatCategoryName']==='20279800'}selected{/if} value='20279800'>20279800</option>
                    <option {if $itemCategory['concatCategoryName']==='20450300'}selected{/if} value='20450300'>20450300</option>
                    <option {if $itemCategory['concatCategoryName']==='20519800'}selected{/if} value='20519800'>20519800</option>
                    <option {if $itemCategory['concatCategoryName']==='20609800'}selected{/if} value='20609800'>20609800</option>
                    <option {if $itemCategory['concatCategoryName']==='29060203'}selected{/if} value='29060203'>29060203</option>
                    <option {if $itemCategory['concatCategoryName']==='29060204'}selected{/if} value='29060204'>29060204</option>
                    <option {if $itemCategory['concatCategoryName']==='35065400'}selected{/if} value='35065400'>35065400</option>
                    <option {if $itemCategory['concatCategoryName']==='35065600'}selected{/if} value='35065600'>35065600</option>
                    <option {if $itemCategory['concatCategoryName']==='35210200'}selected{/if} value='35210200'>35210200</option>
                    <option {if $itemCategory['concatCategoryName']==='35219800'}selected{/if} value='35219800'>35219800</option>
                    <option {if $itemCategory['concatCategoryName']==='35249800'}selected{/if} value='35249800'>35249800</option>
                    <option {if $itemCategory['concatCategoryName']==='35309800'}selected{/if} value='35309800'>35309800</option>
                    <option {if $itemCategory['concatCategoryName']==='35330200'}selected{/if} value='35330200'>35330200</option>
                    <option {if $itemCategory['concatCategoryName']==='35330300'}selected{/if} value='35330300'>35330300</option>
                    <option {if $itemCategory['concatCategoryName']==='35339800'}selected{/if} value='35339800'>35339800</option>
                    <option {if $itemCategory['concatCategoryName']==='35369800'}selected{/if} value='35369800'>35369800</option>
                    <option {if $itemCategory['concatCategoryName']==='35399800'}selected{/if} value='35399800'>35399800</option>
                    <option {if $itemCategory['concatCategoryName']==='35519800'}selected{/if} value='35519800'>35519800</option>
                    <option {if $itemCategory['concatCategoryName']==='44003020'}selected{/if} value='44003020'>44003020</option>
                    <option {if $itemCategory['concatCategoryName']==='44003050'}selected{/if} value='44003050'>44003050</option>
                    <option {if $itemCategory['concatCategoryName']==='44003060'}selected{/if} value='44003060'>44003060</option>
                    <option {if $itemCategory['concatCategoryName']==='44003070'}selected{/if} value='44003070'>44003070</option>
                    <option {if $itemCategory['concatCategoryName']==='44003980'}selected{/if} value='44003980'>44003980</option>
                    <option {if $itemCategory['concatCategoryName']==='44006040'}selected{/if} value='44006040'>44006040</option>
                    <option {if $itemCategory['concatCategoryName']==='44006980'}selected{/if} value='44006980'>44006980</option>
                    <option {if $itemCategory['concatCategoryName']==='44009020'}selected{/if} value='44009020'>44009020</option>
                    <option {if $itemCategory['concatCategoryName']==='44009020'}selected{/if} value='44009020'>44009020</option>
                    <option {if $itemCategory['concatCategoryName']==='44009029'}selected{/if} value='44009029'>44009029</option>
                    <option {if $itemCategory['concatCategoryName']==='44009980'}selected{/if} value='44009980'>44009980</option>
                    <option {if $itemCategory['concatCategoryName']==='44012980'}selected{/if} value='44012980'>44012980</option>
                    <option {if $itemCategory['concatCategoryName']==='44015030'}selected{/if} value='44015030'>44015030</option>
                    <option {if $itemCategory['concatCategoryName']==='44015050'}selected{/if} value='44015050'>44015050</option>
                    <option {if $itemCategory['concatCategoryName']==='44015060'}selected{/if} value='44015060'>44015060</option>
                    <option {if $itemCategory['concatCategoryName']==='44015070'}selected{/if} value='44015070'>44015070</option>
                    <option {if $itemCategory['concatCategoryName']==='44015110'}selected{/if} value='44015110'>44015110</option>
                    <option {if $itemCategory['concatCategoryName']==='44015980'}selected{/if} value='44015980'>44015980</option>
                    <option {if $itemCategory['concatCategoryName']==='44018020'}selected{/if} value='44018020'>44018020</option>
                    <option {if $itemCategory['concatCategoryName']==='44018020'}selected{/if} value='44018020'>44018020</option>
                    <option {if $itemCategory['concatCategoryName']==='44018030'}selected{/if} value='44018030'>44018030</option>
                    <option {if $itemCategory['concatCategoryName']==='44021020'}selected{/if} value='44021020'>44021020</option>
                    <option {if $itemCategory['concatCategoryName']==='44021030'}selected{/if} value='44021030'>44021030</option>
                    <option {if $itemCategory['concatCategoryName']==='44021040'}selected{/if} value='44021040'>44021040</option>
                    <option {if $itemCategory['concatCategoryName']==='40210500'}selected{/if} value='40210500'>40210500</option>
                    <option {if $itemCategory['concatCategoryName']==='40210600'}selected{/if} value='40210600'>40210600</option>
                    <option {if $itemCategory['concatCategoryName']==='40210700'}selected{/if} value='40210700'>40210700</option>
                    <option {if $itemCategory['concatCategoryName']==='40210800'}selected{/if} value='40210800'>40210800</option>
                    <option {if $itemCategory['concatCategoryName']==='40211000'}selected{/if} value='40211000'>40211000</option>
                    <option {if $itemCategory['concatCategoryName']==='40211100'}selected{/if} value='40211100'>40211100</option>
                    <option {if $itemCategory['concatCategoryName']==='40211200'}selected{/if} value='40211200'>40211200</option>
                    <option {if $itemCategory['concatCategoryName']==='40219800'}selected{/if} value='40219800'>40219800</option>
                    <option {if $itemCategory['concatCategoryName']==='40240298'}selected{/if} value='40240298'>40240298</option>
                    <option {if $itemCategory['concatCategoryName']==='40240398'}selected{/if} value='40240398'>40240398</option>
                    <option {if $itemCategory['concatCategoryName']==='40240400'}selected{/if} value='40240400'>40240400</option>
                    <option {if $itemCategory['concatCategoryName']==='40240600'}selected{/if} value='40240600'>40240600</option>
                    <option {if $itemCategory['concatCategoryName']==='40240700'}selected{/if} value='40240700'>40240700</option>
                    <option {if $itemCategory['concatCategoryName']==='40241000'}selected{/if} value='40241000'>40241000</option>
                    <option {if $itemCategory['concatCategoryName']==='40249800'}selected{/if} value='40249800'>40249800</option>
                    <option {if $itemCategory['concatCategoryName']==='40259800'}selected{/if} value='40259800'>40259800</option>
                    <option {if $itemCategory['concatCategoryName']==='40270200'}selected{/if} value='40270200'>40270200</option>
                    <option {if $itemCategory['concatCategoryName']==='40270300'}selected{/if} value='40270300'>40270300</option>
                    <option {if $itemCategory['concatCategoryName']==='40270400'}selected{/if} value='40270400'>40270400</option>
                    <option {if $itemCategory['concatCategoryName']==='40270500'}selected{/if} value='40270500'>40270500</option>
                    <option {if $itemCategory['concatCategoryName']==='40270600'}selected{/if} value='40270600'>40270600</option>
                    <option {if $itemCategory['concatCategoryName']==='40270700'}selected{/if} value='40270700'>40270700</option>
                    <option {if $itemCategory['concatCategoryName']==='40279800'}selected{/if} value='40279800'>40279800</option>
                    <option {if $itemCategory['concatCategoryName']==='40309800'}selected{/if} value='40309800'>40309800</option>
                    <option {if $itemCategory['concatCategoryName']==='40339800'}selected{/if} value='40339800'>40339800</option>
                    <option {if $itemCategory['concatCategoryName']==='40360500'}selected{/if} value='40360500'>40360500</option>
                    <option {if $itemCategory['concatCategoryName']==='40369800'}selected{/if} value='40369800'>40369800</option>
                    <option {if $itemCategory['concatCategoryName']==='40399800'}selected{/if} value='40399800'>40399800</option>
                    <option {if $itemCategory['concatCategoryName']==='40429800'}selected{/if} value='40429800'>40429800</option>
                    <option {if $itemCategory['concatCategoryName']==='40451000'}selected{/if} value='40451000'>40451000</option>
                    <option {if $itemCategory['concatCategoryName']==='40459800'}selected{/if} value='40459800'>40459800</option>
                    <option {if $itemCategory['concatCategoryName']==='40480200'}selected{/if} value='40480200'>40480200</option>
                    <option {if $itemCategory['concatCategoryName']==='40480300'}selected{/if} value='40480300'>40480300</option>
                    <option {if $itemCategory['concatCategoryName']==='40480400'}selected{/if} value='40480400'>40480400</option>
                    <option {if $itemCategory['concatCategoryName']==='40480600'}selected{/if} value='40480600'>40480600</option>
                    <option {if $itemCategory['concatCategoryName']==='40489800'}selected{/if} value='40489800'>40489800</option>
                    <option {if $itemCategory['concatCategoryName']==='40519800'}selected{/if} value='40519800'>40519800</option>
                    <option {if $itemCategory['concatCategoryName']==='45060200'}selected{/if} value='45060200'>45060200</option>
                    <option {if $itemCategory['concatCategoryName']==='45060300'}selected{/if} value='45060300'>45060300</option>
                    <option {if $itemCategory['concatCategoryName']==='45060400'}selected{/if} value='45060400'>45060400</option>
                    <option {if $itemCategory['concatCategoryName']==='45060500'}selected{/if} value='45060500'>45060500</option>
                    <option {if $itemCategory['concatCategoryName']==='45060600'}selected{/if} value='45060600'>45060600</option>
                    <option {if $itemCategory['concatCategoryName']==='45060800'}selected{/if} value='45060800'>45060800</option>
                    <option {if $itemCategory['concatCategoryName']==='45060900'}selected{/if} value='45060900'>45060900</option>
                    <option {if $itemCategory['concatCategoryName']==='45069800'}selected{/if} value='45069800'>45069800</option>
                    <option {if $itemCategory['concatCategoryName']==='45090200'}selected{/if} value='45090200'>45090200</option>
                    <option {if $itemCategory['concatCategoryName']==='45095000'}selected{/if} value='45095000'>45095000</option>
                    <option {if $itemCategory['concatCategoryName']==='45095100'}selected{/if} value='45095100'>45095100</option>
                    <option {if $itemCategory['concatCategoryName']==='45099800'}selected{/if} value='45099800'>45099800</option>
                    <option {if $itemCategory['concatCategoryName']==='45129800'}selected{/if} value='45129800'>45129800</option>
                    <option {if $itemCategory['concatCategoryName']==='45150302'}selected{/if} value='45150302'>45150302</option>
                    <option {if $itemCategory['concatCategoryName']==='45150303'}selected{/if} value='45150303'>45150303</option>
                    <option {if $itemCategory['concatCategoryName']==='45150398'}selected{/if} value='45150398'>45150398</option>
                    <option {if $itemCategory['concatCategoryName']==='45150400'}selected{/if} value='45150400'>45150400</option>
                    <option {if $itemCategory['concatCategoryName']==='45150500'}selected{/if} value='45150500'>45150500</option>
                    <option {if $itemCategory['concatCategoryName']==='45150600'}selected{/if} value='45150600'>45150600</option>
                    <option {if $itemCategory['concatCategoryName']==='45150700'}selected{/if} value='45150700'>45150700</option>
                    <option {if $itemCategory['concatCategoryName']==='45150802'}selected{/if} value='45150802'>45150802</option>
                    <option {if $itemCategory['concatCategoryName']==='45150803'}selected{/if} value='45150803'>45150803</option>
                    <option {if $itemCategory['concatCategoryName']==='45150804'}selected{/if} value='45150804'>45150804</option>
                    <option {if $itemCategory['concatCategoryName']==='45150805'}selected{/if} value='45150805'>45150805</option>
                    <option {if $itemCategory['concatCategoryName']==='45150806'}selected{/if} value='45150806'>45150806</option>
                    <option {if $itemCategory['concatCategoryName']==='45150807'}selected{/if} value='45150807'>45150807</option>
                    <option {if $itemCategory['concatCategoryName']==='45150898'}selected{/if} value='45150898'>45150898</option>
                    <option {if $itemCategory['concatCategoryName']==='45150900'}selected{/if} value='45150900'>45150900</option>
                    <option {if $itemCategory['concatCategoryName']==='45151000'}selected{/if} value='45151000'>45151000</option>
                    <option {if $itemCategory['concatCategoryName']==='45151100'}selected{/if} value='45151100'>45151100</option>
                    <option {if $itemCategory['concatCategoryName']==='45151202'}selected{/if} value='45151202'>45151202</option>
                    <option {if $itemCategory['concatCategoryName']==='45151203'}selected{/if} value='45151203'>45151203</option>
                    <option {if $itemCategory['concatCategoryName']==='45151300'}selected{/if} value='45151300'>45151300</option>
                    <option {if $itemCategory['concatCategoryName']==='45151400'}selected{/if} value='45151400'>45151400</option>
                    <option {if $itemCategory['concatCategoryName']==='45151500'}selected{/if} value='45151500'>45151500</option>
                    <option {if $itemCategory['concatCategoryName']==='45151600'}selected{/if} value='45151600'>45151600</option>
                    <option {if $itemCategory['concatCategoryName']==='45151800'}selected{/if} value='45151800'>45151800</option>
                    <option {if $itemCategory['concatCategoryName']==='45151900'}selected{/if} value='45151900'>45151900</option>
                    <option {if $itemCategory['concatCategoryName']==='45152000'}selected{/if} value='45152000'>45152000</option>
                    <option {if $itemCategory['concatCategoryName']==='45152100'}selected{/if} value='45152100'>45152100</option>
                    <option {if $itemCategory['concatCategoryName']==='45152200'}selected{/if} value='45152200'>45152200</option>
                    <option {if $itemCategory['concatCategoryName']==='45152300'}selected{/if} value='45152300'>45152300</option>
                    <option {if $itemCategory['concatCategoryName']==='45152400'}selected{/if} value='45152400'>45152400</option>
                    <option {if $itemCategory['concatCategoryName']==='45152502'}selected{/if} value='45152502'>45152502</option>
                    <option {if $itemCategory['concatCategoryName']==='45152503'}selected{/if} value='45152503'>45152503</option>
                    <option {if $itemCategory['concatCategoryName']==='45152598'}selected{/if} value='45152598'>45152598</option>
                    <option {if $itemCategory['concatCategoryName']==='45152602'}selected{/if} value='45152602'>45152602</option>
                    <option {if $itemCategory['concatCategoryName']==='45152603'}selected{/if} value='45152603'>45152603</option>
                    <option {if $itemCategory['concatCategoryName']==='45152698'}selected{/if} value='45152698'>45152698</option>
                    <option {if $itemCategory['concatCategoryName']==='45152700'}selected{/if} value='45152700'>45152700</option>
                    <option {if $itemCategory['concatCategoryName']==='45152800'}selected{/if} value='45152800'>45152800</option>
                    <option {if $itemCategory['concatCategoryName']==='45155000'}selected{/if} value='45155000'>45155000</option>
                    <option {if $itemCategory['concatCategoryName']==='45155102'}selected{/if} value='45155102'>45155102</option>
                    <option {if $itemCategory['concatCategoryName']==='45155103'}selected{/if} value='45155103'>45155103</option>
                    <option {if $itemCategory['concatCategoryName']==='45155104'}selected{/if} value='45155104'>45155104</option>
                    <option {if $itemCategory['concatCategoryName']==='45155105'}selected{/if} value='45155105'>45155105</option>
                    <option {if $itemCategory['concatCategoryName']==='45155198'}selected{/if} value='45155198'>45155198</option>
                    <option {if $itemCategory['concatCategoryName']==='45155202'}selected{/if} value='45155202'>45155202</option>
                    <option {if $itemCategory['concatCategoryName']==='45155203'}selected{/if} value='45155203'>45155203</option>
                    <option {if $itemCategory['concatCategoryName']==='45155204'}selected{/if} value='45155204'>45155204</option>
                    <option {if $itemCategory['concatCategoryName']==='45155205'}selected{/if} value='45155205'>45155205</option>
                    <option {if $itemCategory['concatCategoryName']==='45155298'}selected{/if} value='45155298'>45155298</option>
                    <option {if $itemCategory['concatCategoryName']==='45155302'}selected{/if} value='45155302'>45155302</option>
                    <option {if $itemCategory['concatCategoryName']==='45155316'}selected{/if} value='45155316'>45155316</option>
                    <option {if $itemCategory['concatCategoryName']==='45159800'}selected{/if} value='45159800'>45159800</option>
                    <option {if $itemCategory['concatCategoryName']==='45159900'}selected{/if} value='45159900'>45159900</option>
                    <option {if $itemCategory['concatCategoryName']==='45160202'}selected{/if} value='45160202'>45160202</option>
                    <option {if $itemCategory['concatCategoryName']==='45160203'}selected{/if} value='45160203'>45160203</option>
                    <option {if $itemCategory['concatCategoryName']==='45160298'}selected{/if} value='45160298'>45160298</option>
                    <option {if $itemCategory['concatCategoryName']==='45160398'}selected{/if} value='45160398'>45160398</option>
                    <option {if $itemCategory['concatCategoryName']==='45160698'}selected{/if} value='45160698'>45160698</option>
                    <option {if $itemCategory['concatCategoryName']==='45241000'}selected{/if} value='45241000'>45241000</option>
                    <option {if $itemCategory['concatCategoryName']==='45249800'}selected{/if} value='45249800'>45249800</option>
                    <option {if $itemCategory['concatCategoryName']==='45309800'}selected{/if} value='45309800'>45309800</option>
                    <option {if $itemCategory['concatCategoryName']==='45339800'}selected{/if} value='45339800'>45339800</option>
                    <option {if $itemCategory['concatCategoryName']==='45420300'}selected{/if} value='45420300'>45420300</option>
                    <option {if $itemCategory['concatCategoryName']==='45420700'}selected{/if} value='45420700'>45420700</option>
                    <option {if $itemCategory['concatCategoryName']==='45420900'}selected{/if} value='45420900'>45420900</option>
                    <option {if $itemCategory['concatCategoryName']==='45429800'}selected{/if} value='45429800'>45429800</option>
                    <option {if $itemCategory['concatCategoryName']==='45459800'}selected{/if} value='45459800'>45459800</option>
                    <option {if $itemCategory['concatCategoryName']==='45489800'}selected{/if} value='45489800'>45489800</option>
                    <option {if $itemCategory['concatCategoryName']==='45510300'}selected{/if} value='45510300'>45510300</option>
                    <option {if $itemCategory['concatCategoryName']==='45510400'}selected{/if} value='45510400'>45510400</option>
                    <option {if $itemCategory['concatCategoryName']==='45510500'}selected{/if} value='45510500'>45510500</option>
                    <option {if $itemCategory['concatCategoryName']==='45510600'}selected{/if} value='45510600'>45510600</option>
                    <option {if $itemCategory['concatCategoryName']==='45519800'}selected{/if} value='45519800'>45519800</option>
                    <option {if $itemCategory['concatCategoryName']==='45549800'}selected{/if} value='45549800'>45549800</option>
                    <option {if $itemCategory['concatCategoryName']==='45579800'}selected{/if} value='45579800'>45579800</option>
                    <option {if $itemCategory['concatCategoryName']==='45609800'}selected{/if} value='45609800'>45609800</option>
                    <option {if $itemCategory['concatCategoryName']==='45639800'}selected{/if} value='45639800'>45639800</option>
                    <option {if $itemCategory['concatCategoryName']==='45660200'}selected{/if} value='45660200'>45660200</option>
                    <option {if $itemCategory['concatCategoryName']==='45660300'}selected{/if} value='45660300'>45660300</option>
                    <option {if $itemCategory['concatCategoryName']==='45660400'}selected{/if} value='45660400'>45660400</option>
                    <option {if $itemCategory['concatCategoryName']==='45669800'}selected{/if} value='45669800'>45669800</option>
                    <option {if $itemCategory['concatCategoryName']==='45690200'}selected{/if} value='45690200'>45690200</option>
                    <option {if $itemCategory['concatCategoryName']==='45699800'}selected{/if} value='45699800'>45699800</option>
                    <option {if $itemCategory['concatCategoryName']==='45720200'}selected{/if} value='45720200'>45720200</option>
                    <option {if $itemCategory['concatCategoryName']==='45720300'}selected{/if} value='45720300'>45720300</option>
                    <option {if $itemCategory['concatCategoryName']==='45720400'}selected{/if} value='45720400'>45720400</option>
                    <option {if $itemCategory['concatCategoryName']==='45720500'}selected{/if} value='45720500'>45720500</option>
                    <option {if $itemCategory['concatCategoryName']==='45720600'}selected{/if} value='45720600'>45720600</option>
                    <option {if $itemCategory['concatCategoryName']==='45729800'}selected{/if} value='45729800'>45729800</option>
                    <option {if $itemCategory['concatCategoryName']==='45750298'}selected{/if} value='45750298'>45750298</option>
                    <option {if $itemCategory['concatCategoryName']==='45759800'}selected{/if} value='45759800'>45759800</option>
                    <option {if $itemCategory['concatCategoryName']==='50090998'}selected{/if} value='50090998'>50090998</option>
                    <option {if $itemCategory['concatCategoryName']==='50091000'}selected{/if} value='50091000'>50091000</option>
                    <option {if $itemCategory['concatCategoryName']==='50099800'}selected{/if} value='50099800'>50099800</option>
                    <option {if $itemCategory['concatCategoryName']==='50129800'}selected{/if} value='50129800'>50129800</option>
                    <option {if $itemCategory['concatCategoryName']==='50212100'}selected{/if} value='50212100'>50212100</option>
                    <option {if $itemCategory['concatCategoryName']==='50212300'}selected{/if} value='50212300'>50212300</option>
                    <option {if $itemCategory['concatCategoryName']==='50212900'}selected{/if} value='50212900'>50212900</option>
                    <option {if $itemCategory['concatCategoryName']==='50213100'}selected{/if} value='50213100'>50213100</option>
                    <option {if $itemCategory['concatCategoryName']==='50213300'}selected{/if} value='50213300'>50213300</option>
                    <option {if $itemCategory['concatCategoryName']==='50213500'}selected{/if} value='50213500'>50213500</option>
                    <option {if $itemCategory['concatCategoryName']==='50213900'}selected{/if} value='50213900'>50213900</option>
                    <option {if $itemCategory['concatCategoryName']==='50214100'}selected{/if} value='50214100'>50214100</option>
                    <option {if $itemCategory['concatCategoryName']==='50214500'}selected{/if} value='50214500'>50214500</option>
                    <option {if $itemCategory['concatCategoryName']==='50215200'}selected{/if} value='50215200'>50215200</option>
                    <option {if $itemCategory['concatCategoryName']==='50249800'}selected{/if} value='50249800'>50249800</option>
                    <option {if $itemCategory['concatCategoryName']==='50279800'}selected{/if} value='50279800'>50279800</option>
                    <option {if $itemCategory['concatCategoryName']==='55039800'}selected{/if} value='55039800'>55039800</option>
                    <option {if $itemCategory['concatCategoryName']==='55069800'}selected{/if} value='55069800'>55069800</option>
                    <option {if $itemCategory['concatCategoryName']==='55099800'}selected{/if} value='55099800'>55099800</option>
                    <option {if $itemCategory['concatCategoryName']==='55129800'}selected{/if} value='55129800'>55129800</option>
                    <option {if $itemCategory['concatCategoryName']==='55159800'}selected{/if} value='55159800'>55159800</option>
                    <option {if $itemCategory['concatCategoryName']==='55189800'}selected{/if} value='55189800'>55189800</option>
                    <option {if $itemCategory['concatCategoryName']==='55219800'}selected{/if} value='55219800'>55219800</option>
                    <option {if $itemCategory['concatCategoryName']==='60279800'}selected{/if} value='60279800'>60279800</option>
                    <option {if $itemCategory['concatCategoryName']==='60330698'}selected{/if} value='60330698'>60330698</option>
                    <option {if $itemCategory['concatCategoryName']==='60360798'}selected{/if} value='60360798'>60360798</option>
                    <option {if $itemCategory['concatCategoryName']==='60399800'}selected{/if} value='60399800'>60399800</option>
                    <option {if $itemCategory['concatCategoryName']==='60429800'}selected{/if} value='60429800'>60429800</option>
                    <option {if $itemCategory['concatCategoryName']==='60489800'}selected{/if} value='60489800'>60489800</option>
                    <option {if $itemCategory['concatCategoryName']==='60549800'}selected{/if} value='60549800'>60549800</option>
                    <option {if $itemCategory['concatCategoryName']==='60579800'}selected{/if} value='60579800'>60579800</option>
                    <option {if $itemCategory['concatCategoryName']==='62030300'}selected{/if} value='62030300'>62030300</option>
                    <option {if $itemCategory['concatCategoryName']==='62069800'}selected{/if} value='62069800'>62069800</option>
                    <option {if $itemCategory['concatCategoryName']==='62099800'}selected{/if} value='62099800'>62099800</option>
                    <option {if $itemCategory['concatCategoryName']==='62129800'}selected{/if} value='62129800'>62129800</option>
                    <option {if $itemCategory['concatCategoryName']==='62149800'}selected{/if} value='62149800'>62149800</option>
                    <option {if $itemCategory['concatCategoryName']==='62159800'}selected{/if} value='62159800'>62159800</option>
                    <option {if $itemCategory['concatCategoryName']==='66039800'}selected{/if} value='66039800'>66039800</option>
                    <option {if $itemCategory['concatCategoryName']==='66069800'}selected{/if} value='66069800'>66069800</option>
                    <option {if $itemCategory['concatCategoryName']==='66099800'}selected{/if} value='66099800'>66099800</option>
                    <option {if $itemCategory['concatCategoryName']==='66109800'}selected{/if} value='66109800'>66109800</option>
                    <option {if $itemCategory['concatCategoryName']==='66120298'}selected{/if} value='66120298'>66120298</option>
                    <option {if $itemCategory['concatCategoryName']==='66120700'}selected{/if} value='66120700'>66120700</option>
                    <option {if $itemCategory['concatCategoryName']==='66120898'}selected{/if} value='66120898'>66120898</option>
                    <option {if $itemCategory['concatCategoryName']==='66129800'}selected{/if} value='66129800'>66129800</option>
                    <option {if $itemCategory['concatCategoryName']==='66189800'}selected{/if} value='66189800'>66189800</option>
                    <option {if $itemCategory['concatCategoryName']==='70039800'}selected{/if} value='70039800'>70039800</option>
                    <option {if $itemCategory['concatCategoryName']==='70060698'}selected{/if} value='70060698'>70060698</option>
                    <option {if $itemCategory['concatCategoryName']==='70069900'}selected{/if} value='70069900'>70069900</option>
                    <option {if $itemCategory['concatCategoryName']==='70099800'}selected{/if} value='70099800'>70099800</option>
                    <option {if $itemCategory['concatCategoryName']==='70109800'}selected{/if} value='70109800'>70109800</option>
                    <option {if $itemCategory['concatCategoryName']==='70129800'}selected{/if} value='70129800'>70129800</option>
                    <option {if $itemCategory['concatCategoryName']==='70159800'}selected{/if} value='70159800'>70159800</option>
                    <option {if $itemCategory['concatCategoryName']==='70189800'}selected{/if} value='70189800'>70189800</option>
                    <option {if $itemCategory['concatCategoryName']==='70219800'}selected{/if} value='70219800'>70219800</option>
                    <option {if $itemCategory['concatCategoryName']==='70249800'}selected{/if} value='70249800'>70249800</option>
                    <option {if $itemCategory['concatCategoryName']==='70279800'}selected{/if} value='70279800'>70279800</option>
                    <option {if $itemCategory['concatCategoryName']==='70309800'}selected{/if} value='70309800'>70309800</option>
                    <option {if $itemCategory['concatCategoryName']==='70339800'}selected{/if} value='70339800'>70339800</option>
                    <option {if $itemCategory['concatCategoryName']==='70360200'}selected{/if} value='70360200'>70360200</option>
                    <option {if $itemCategory['concatCategoryName']==='70360400'}selected{/if} value='70360400'>70360400</option>
                    <option {if $itemCategory['concatCategoryName']==='70360700'}selected{/if} value='70360700'>70360700</option>
                    <option {if $itemCategory['concatCategoryName']==='70360800'}selected{/if} value='70360800'>70360800</option>
                    <option {if $itemCategory['concatCategoryName']==='70369800'}selected{/if} value='70369800'>70369800</option>
                    <option {if $itemCategory['concatCategoryName']==='70399800'}selected{/if} value='70399800'>70399800</option>
                    <option {if $itemCategory['concatCategoryName']==='70429800'}selected{/if} value='70429800'>70429800</option>
                    <option {if $itemCategory['concatCategoryName']==='70489800'}selected{/if} value='70489800'>70489800</option>
                    <option {if $itemCategory['concatCategoryName']==='70549800'}selected{/if} value='70549800'>70549800</option>
                    <option {if $itemCategory['concatCategoryName']==='70579800'}selected{/if} value='70579800'>70579800</option>
                    <option {if $itemCategory['concatCategoryName']==='70709800'}selected{/if} value='70709800'>70709800</option>
                    <option {if $itemCategory['concatCategoryName']==='71039800'}selected{/if} value='71039800'>71039800</option>
                    <option {if $itemCategory['concatCategoryName']==='71069800'}selected{/if} value='71069800'>71069800</option>
                    <option {if $itemCategory['concatCategoryName']==='71099800'}selected{/if} value='71099800'>71099800</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorObservations" class="form-text text-danger">&nbsp;{$errorObservations}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Observations</div>
                </div>
                <textarea type="text" class="form-control" id="observations" name="observations" value="" placeholder="Observations" rows="5">{$observations}</textarea>
            </div>
            <label class="alert-info text-info"><i class="fas fa-bug fa-lg fa-fw"></i> Help us detect bugs: use this field to annotate special cases, cleansing decisions, and relevant issues related to this record.</label>
            <label class="alert-warning text-normal"><i class="fas fa-exclamation-circle fa-lg fa-fw"></i>Please don't erase the original CODIGO INTERNO (aka CODIGO MASSIVO) from this observation.   This information will be used to reduce the amount of cleansing work in future migrations</label>
        </div>
    </div>
    <br/>


    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>

</form>
<script>
    function blockItemNumberEdit(){
        var templateName = $('#customEditItem').find('select[name="templateName"]').val();
        var itemNumberInput = $('#customEditItem').find('input[name="itemNumber"]');

        if (templateName=='LA98'){
            itemNumberInput.prop('disabled', false);
        }else{

            if (templateName.substring(0,4)==itemNumberInput.val().substring(0,4)){
                $('#customEditItem').find('small[name="errorItemNumber"]').html('<span>&nbsp;</span>');

            }else{
                $('#customEditItem').find('small[name="errorItemNumber"]').text('new value will be assigned when update ItemHeader is clicked');

            }
            itemNumberInput.prop('disabled', true);
        }
    }

    $( document ).ready(function() {
        blockItemNumberEdit();
    });

    $('#templateName').on('change', function() {
        blockItemNumberEdit();

    });

</script>
<br/>
<br/>
<h4>Manufacture Part Number <a data-toggle="tooltip" data-placement="top" title="add another part number to {$origItemNumber}" href="{base_url()}ItemMFGPartNum/createNew/{$id}" style="cursor:pointer"><small><i class="text-primary fas fa-plus-circle fa-sm fa-fw"></i></small></a> </h4>
{if isset($itemMfgPartNum) && (sizeof($itemMfgPartNum)>0)}
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th style="width:90px">#</th>
            <th>Item Number</th>
            <th>Manufacturer</th>
            <th>Description</th>
            <th>Part Number</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$itemMfgPartNum item=imf name=foo}
            <tr class="{if ($imf.dismissed)} dismissed {elseif (($imf.locallyValidated)&&($imf.crossRefValidated)) } alert-success {else} alert-warning {/if} clickable-row" style="cursor:pointer" data-href='{base_url()}ItemMFGPartNum/customEditCleansed/{$imf.country}/{$imf.id}'>
                <td>{$imf.id}</td>
                <td>{$imf.itemNumber}</td>
                <td>{$imf.manufacturer}</td>
                <td>{$imf.description}</td>
                <td>{$imf.partNumber}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{/if}
<br/>
<h4>Cross Reference Records <a data-toggle="tooltip" data-placement="top" title="associate another record to {$origItemNumber}" href="{base_url()}Inventory/associateSIGLARecordTo/{$id}" style="cursor:pointer"><small><i class="text-primary fas fa-plus-circle fa-sm fa-fw"></i></small></a> </h4>
{if isset($itemCrossRef) && (sizeof($itemCrossRef)>0)}
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th style="width:90px">#</th>
            <th>Country</th>
            <th>Item Number</th>
            <th>Cross Reference Type</th>
            <th>ID</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$itemCrossRef item=icr name=foo}
            <tr class="{if ($icr.dismissed)} dismissed {elseif (($icr.locallyValidated)&&($icr.crossRefValidated)) } alert-success {else} alert-warning {/if}">
                <td>{$icr.id}</td>
                <td>{$icr.country}</td>
                <td>{$icr.itemNumber}</td>
                <td>{$icr.crossReferenceType}</td>
                <td>{$icr.crossReference}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{/if}
<br/>

<br/>
<br/>

