<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-3 my-1">
            <small id="errorOrigItemNumber" class="form-text text-danger">&nbsp;{$errorOrigItemNumber}</small>
            <label class="sr-only" for="inlineFormInputOrigItemNumber">Orig Item Number</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Orig Item Number</div>
                </div>
                <input type="text" class="form-control" id="origItemNumber" name="origItemNumber" value="{$origItemNumber}" placeholder="Orig Item Number">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <small id="errorItemNumber" class="form-text text-danger">&nbsp;{$errorItemNumber}</small>
            <label class="sr-only" for="inlineFormInputItemNumber">Item Number</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Item Number</div>
                </div>
                <input type="text" class="form-control" id="itemNumber" name="itemNumber" value="{$itemNumber}" placeholder="Item Number">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <small id="errorDescription" class="form-text text-danger">&nbsp;{$errorDescription}</small>
            <label class="sr-only" for="inlineFormInputDescription">Description</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Description</div>
                </div>
                <input type="text" class="form-control" id="description" name="description" value="{$description}" placeholder="Description">
            </div>
        </div>
    </div>    <div class="form-row align-items-center">
        <div class="col-sm-3 my-1">
            <small id="errorDescriptionEsa" class="form-text text-danger">&nbsp;{$errorDescriptionEsa}</small>
            <label class="sr-only" for="inlineFormInputDescriptionEsa">Description Esa</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Description Esa</div>
                </div>
                <input type="text" class="form-control" id="descriptionEsa" name="descriptionEsa" value="{$descriptionEsa}" placeholder="Description Esa">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <small id="errorOrganizationCode" class="form-text text-danger">&nbsp;{$errorOrganizationCode}</small>
            <label class="sr-only" for="inlineFormInputOrganizationCode">Organization Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Organization Code</div>
                </div>
                <input type="text" class="form-control" id="organizationCode" name="organizationCode" value="{$organizationCode}" placeholder="Organization Code">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <small id="errorOrgName" class="form-text text-danger">&nbsp;{$errorOrgName}</small>
            <label class="sr-only" for="inlineFormInputOrgName">Org Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Org Name</div>
                </div>
                <input type="text" class="form-control" id="orgName" name="orgName" value="{$orgName}" placeholder="Org Name">
            </div>
        </div>
    </div>
    <br/>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>

</form>