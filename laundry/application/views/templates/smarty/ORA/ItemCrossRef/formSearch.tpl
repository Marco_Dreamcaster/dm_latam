<h4 class="p-3 text-center">{$title}</h4>
<form id="searchAssociationForDirty" name="customEditItem" action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Original Item Number (SIGLA) &nbsp;<span class="flag-icon flag-icon-{strtolower($source.country)}"></div>
                </div>
                <input type="text" class="form-control" id="origItemNumber" name="origItemNumber" value="{$source.origItemNumber}" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Item Number (ORACLE)</div>
                </div>
                <input type="text" class="form-control" id="itemNumber" name="itemNumber" value="{$source.itemNumber}" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Description (EN)</div>
                </div>
                <input type="text" class="form-control" id="description" name="description" value="{$source.description}" placeholder="Description">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <label class="d-flex justify-content-center text-danger p-3">{$errorKeywords}</label>
            <div class="form-group mx-sm-8 mb-0">
                <div class="input-group mb-0">
                    <div class="input-group-prepend">
                        <div class="input-group-text">%</div>
                    </div>
                    <input type="text" class="form-control form-control-sm p-2" id="keywords" name="keywords" size="60" value="{$keywords}">
                    &nbsp;
                    <button type="submit" class="btn btn-primary">Search Cleansed items <i class="fas fa-search fa-sm fa-fw"></i> % </button>
                    &nbsp;
                    <a class="btn btn-secondary" href="{base_url()}{$cancelAction}/{$id}" data-toggle="tooltip" data-placement="top" title="cancel"><i class="fas fa-ban fa-sm fa-fw"></i></a>
                </div>
            </div>
        </div>
    </div>
</form>

{if isset($results) && (sizeof($results)>0)}
<br/>
<br/>
<h5>Possible Candidates</h5>
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th>Country</th>
            <th>Detail</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$results item=r name=results}
            <tr>
                <td><span style="vertical-align: middle" class="flag-icon flag-icon-{strtolower($r.country)}"></span>&nbsp;<span>{strtoupper($r.country)}</td>
                <td>{$r.detail}</td>
                <td>
                    <a class="btn btn-primary" href="{base_url()}{$r.actionPeekUrl}" title="peek at item {$r.detail}" target="_blank"><i class="text-white fas fa-eye fa-sm"></i></a>
                    <a class="btn btn-primary" href="{base_url()}{$r.actionUrl}" title="confirm association for item {$r.detail}" target="_self"><i class="text-white fas fa-link fa-sm"></i></a>
                </td>
                <td>
                </td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{/if}
</div>
