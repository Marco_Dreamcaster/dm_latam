<h3>{$title}</h3>
<br/>
<div class="row align-items-center">
    <div class="col-12">
        <p class="alert-danger p-2 text-center">{$message['PT']}</p>
        <p class="alert-danger p-2 text-center">{$message['EN']}</p>
    </div>

    <div class="col-12 alert-info p-5">
        <p class="text-left">{$origItemNumber}</p>
        <pre class="text-left">
SELECT ID,* FROM O_STG_ITEM_HEADER WHERE ORIG_ITEM_NUMBER LIKE '%{$origItemNumber}%'
SELECT ID,* FROM O_STG_ITEM_CATEGORY WHERE ORIG_ITEM_NUMBER LIKE '%{$origItemNumber}%'
SELECT ID,* FROM O_STG_ITEM_CROSSREF WHERE ORIG_ITEM_NUMBER LIKE '%{$origItemNumber}%'
SELECT ID,* FROM O_STG_ITEM_MFG_PART_NUM WHERE ORIG_ITEM_NUMBER LIKE '%{$origItemNumber}%'
SELECT ID,* FROM O_STG_ITEM_TRANS_DEF_LOC WHERE ORIG_ITEM_NUMBER LIKE '%{$origItemNumber}%'
SELECT ID,* FROM O_STG_ITEM_TRANS_DEF_SUB_INV WHERE ORIG_ITEM_NUMBER LIKE '%{$origItemNumber}%'

/**
DELETE FROM O_STG_ITEM_HEADER WHERE ID IN ({foreach from=$headers item=i name=fooHeaders}{$i.id}{if not $smarty.foreach.fooHeaders.last},{/if}{/foreach})
DELETE FROM O_STG_ITEM_CATEGORY WHERE ID IN ({foreach from=$itemCategories item=i name=fooCategories}{$i.id}{if not $smarty.foreach.fooCategories.last},{/if}{/foreach})
DELETE FROM O_STG_ITEM_CROSSREF WHERE ID IN ({foreach from=$itemCrossRefs item=i name=fooCrossRef}{$i.id}{if not $smarty.foreach.fooCrossRef.last},{/if}{/foreach})
DELETE FROM O_STG_ITEM_MFG_PART_NUM WHERE ID IN ({foreach from=$itemMfgPartNums item=i name=fooMfgPartNum}{$i.id}{if not $smarty.foreach.fooMfgPartNum.last},{/if}{/foreach})
DELETE FROM O_STG_ITEM_TRANS_DEF_LOC WHERE ID IN ({foreach from=$itemTransDefLocs item=i name=fooTransDefLocs}{$i.id}{if not $smarty.foreach.fooTransDefLocs.last},{/if}{/foreach})
DELETE FROM O_STG_ITEM_TRANS_DEF_SUB_INV WHERE ID IN ({foreach from=$itemTransDefSubInvs item=i name=fooTransDefSubInvs}{$i.id}{if not $smarty.foreach.fooTransDefSubInvs.last},{/if}{/foreach})
**/
        </pre>
    </div>

</div>
<br/>
<br/>
<br/>
<br/>
<br/>
