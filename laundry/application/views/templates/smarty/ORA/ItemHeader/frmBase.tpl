<br/>
<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>
    <div class="form-group">
        <label for="origItemNumber">Orig Item Number <small id="origItemNumberHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="origItemNumber" name="origItemNumber" aria-describedby="origItemNumberHelp" value="{$origItemNumber}" placeholder="Orig Item Number...">
        <small id="errorOrigItemNumber" class="form-text text-danger">{$errorOrigItemNumber}</small>
    </div>
    <div class="form-group">
        <label for="itemNumber">Item Number <small id="itemNumberHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="itemNumber" name="itemNumber" aria-describedby="itemNumberHelp" value="{$itemNumber}" placeholder="Item Number...">
        <small id="errorItemNumber" class="form-text text-danger">{$errorItemNumber}</small>
    </div>
    <div class="form-group">
        <label for="description">Description <small id="descriptionHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="description" name="description" aria-describedby="descriptionHelp" value="{$description}" placeholder="Description...">
        <small id="errorDescription" class="form-text text-danger">{$errorDescription}</small>
    </div>
    <div class="form-group">
        <label for="descriptionEsa">Description Esa <small id="descriptionEsaHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="descriptionEsa" name="descriptionEsa" aria-describedby="descriptionEsaHelp" value="{$descriptionEsa}" placeholder="Description Esa...">
        <small id="errorDescriptionEsa" class="form-text text-danger">{$errorDescriptionEsa}</small>
    </div>
    <div class="form-group">
        <label for="organizationCode">Organization Code <small id="organizationCodeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="organizationCode" name="organizationCode" aria-describedby="organizationCodeHelp" value="{$organizationCode}" placeholder="Organization Code...">
        <small id="errorOrganizationCode" class="form-text text-danger">{$errorOrganizationCode}</small>
    </div>
    <div class="form-group">
        <label for="templateName">Template Name <small id="templateNameHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="templateName" name="templateName" aria-describedby="templateNameHelp" value="{$templateName}" placeholder="Template Name...">
        <small id="errorTemplateName" class="form-text text-danger">{$errorTemplateName}</small>
    </div>
    <div class="form-group">
        <label for="listPricePerUnit">List Price Per Unit <small id="listPricePerUnitHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="listPricePerUnit" name="listPricePerUnit" aria-describedby="listPricePerUnitHelp" value="{$listPricePerUnit}" placeholder="List Price Per Unit...">
        <small id="errorListPricePerUnit" class="form-text text-danger">{$errorListPricePerUnit}</small>
    </div>
    <div class="form-group">
        <label for="uomCode">Uom Code <small id="uomCodeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="uomCode" name="uomCode" aria-describedby="uomCodeHelp" value="{$uomCode}" placeholder="Uom Code...">
        <small id="errorUomCode" class="form-text text-danger">{$errorUomCode}</small>
    </div>
    <div class="form-group">
        <label for="weightUomCode">Weight Uom Code <small id="weightUomCodeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="weightUomCode" name="weightUomCode" aria-describedby="weightUomCodeHelp" value="{$weightUomCode}" placeholder="Weight Uom Code...">
        <small id="errorWeightUomCode" class="form-text text-danger">{$errorWeightUomCode}</small>
    </div>
    <div class="form-group">
        <label for="unitWeight">Unit Weight <small id="unitWeightHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="unitWeight" name="unitWeight" aria-describedby="unitWeightHelp" value="{$unitWeight}" placeholder="Unit Weight...">
        <small id="errorUnitWeight" class="form-text text-danger">{$errorUnitWeight}</small>
    </div>
    <div class="form-group">
        <label for="volumeUomCode">Volume Uom Code <small id="volumeUomCodeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="volumeUomCode" name="volumeUomCode" aria-describedby="volumeUomCodeHelp" value="{$volumeUomCode}" placeholder="Volume Uom Code...">
        <small id="errorVolumeUomCode" class="form-text text-danger">{$errorVolumeUomCode}</small>
    </div>
    <div class="form-group">
        <label for="unitVolume">Unit Volume <small id="unitVolumeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="unitVolume" name="unitVolume" aria-describedby="unitVolumeHelp" value="{$unitVolume}" placeholder="Unit Volume...">
        <small id="errorUnitVolume" class="form-text text-danger">{$errorUnitVolume}</small>
    </div>
    <div class="form-group">
        <label for="minMinmaxQuantity">Min Minmax Quantity <small id="minMinmaxQuantityHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="minMinmaxQuantity" name="minMinmaxQuantity" aria-describedby="minMinmaxQuantityHelp" value="{$minMinmaxQuantity}" placeholder="Min Minmax Quantity...">
        <small id="errorMinMinmaxQuantity" class="form-text text-danger">{$errorMinMinmaxQuantity}</small>
    </div>
    <div class="form-group">
        <label for="maxMinmaxQuantity">Max Minmax Quantity <small id="maxMinmaxQuantityHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="maxMinmaxQuantity" name="maxMinmaxQuantity" aria-describedby="maxMinmaxQuantityHelp" value="{$maxMinmaxQuantity}" placeholder="Max Minmax Quantity...">
        <small id="errorMaxMinmaxQuantity" class="form-text text-danger">{$errorMaxMinmaxQuantity}</small>
    </div>
    <div class="form-group">
        <label for="fullLeadTime">Full Lead Time <small id="fullLeadTimeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="fullLeadTime" name="fullLeadTime" aria-describedby="fullLeadTimeHelp" value="{$fullLeadTime}" placeholder="Full Lead Time...">
        <small id="errorFullLeadTime" class="form-text text-danger">{$errorFullLeadTime}</small>
    </div>
    <div class="form-group">
        <label for="fixedLotMultiplier">Fixed Lot Multiplier <small id="fixedLotMultiplierHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="fixedLotMultiplier" name="fixedLotMultiplier" aria-describedby="fixedLotMultiplierHelp" value="{$fixedLotMultiplier}" placeholder="Fixed Lot Multiplier...">
        <small id="errorFixedLotMultiplier" class="form-text text-danger">{$errorFixedLotMultiplier}</small>
    </div>
    <div class="form-group">
        <label for="fixedOrderQuantity">Fixed Order Quantity <small id="fixedOrderQuantityHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="fixedOrderQuantity" name="fixedOrderQuantity" aria-describedby="fixedOrderQuantityHelp" value="{$fixedOrderQuantity}" placeholder="Fixed Order Quantity...">
        <small id="errorFixedOrderQuantity" class="form-text text-danger">{$errorFixedOrderQuantity}</small>
    </div>
    <div class="form-group">
        <label for="minimumOrderQuantity">Minimum Order Quantity <small id="minimumOrderQuantityHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="minimumOrderQuantity" name="minimumOrderQuantity" aria-describedby="minimumOrderQuantityHelp" value="{$minimumOrderQuantity}" placeholder="Minimum Order Quantity...">
        <small id="errorMinimumOrderQuantity" class="form-text text-danger">{$errorMinimumOrderQuantity}</small>
    </div>
    <div class="form-group">
        <label for="maximumOrderQuantity">Maximum Order Quantity <small id="maximumOrderQuantityHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="maximumOrderQuantity" name="maximumOrderQuantity" aria-describedby="maximumOrderQuantityHelp" value="{$maximumOrderQuantity}" placeholder="Maximum Order Quantity...">
        <small id="errorMaximumOrderQuantity" class="form-text text-danger">{$errorMaximumOrderQuantity}</small>
    </div>
    <div class="form-group">
        <label for="longDescription">Long Description <small id="longDescriptionHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="longDescription" name="longDescription" aria-describedby="longDescriptionHelp" value="{$longDescription}" placeholder="Long Description...">
        <small id="errorLongDescription" class="form-text text-danger">{$errorLongDescription}</small>
    </div>
    <div class="form-group">
        <label for="itemInvApplication">Item Inv Application <small id="itemInvApplicationHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="itemInvApplication" name="itemInvApplication" aria-describedby="itemInvApplicationHelp" value="{$itemInvApplication}" placeholder="Item Inv Application...">
        <small id="errorItemInvApplication" class="form-text text-danger">{$errorItemInvApplication}</small>
    </div>
    <div class="form-group">
        <label for="transactionConditionCode">Transaction Condition Code <small id="transactionConditionCodeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="transactionConditionCode" name="transactionConditionCode" aria-describedby="transactionConditionCodeHelp" value="{$transactionConditionCode}" placeholder="Transaction Condition Code...">
        <small id="errorTransactionConditionCode" class="form-text text-danger">{$errorTransactionConditionCode}</small>
    </div>
    <div class="form-group">
        <label for="adjustmentAccount">Adjustment Account <small id="adjustmentAccountHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="adjustmentAccount" name="adjustmentAccount" aria-describedby="adjustmentAccountHelp" value="{$adjustmentAccount}" placeholder="Adjustment Account...">
        <small id="errorAdjustmentAccount" class="form-text text-danger">{$errorAdjustmentAccount}</small>
    </div>
    <div class="form-group">
        <label for="correctionAccount">Correction Account <small id="correctionAccountHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="correctionAccount" name="correctionAccount" aria-describedby="correctionAccountHelp" value="{$correctionAccount}" placeholder="Correction Account...">
        <small id="errorCorrectionAccount" class="form-text text-danger">{$errorCorrectionAccount}</small>
    </div>
    <div class="form-group">
        <label for="salesCostAccount">Sales Cost Account <small id="salesCostAccountHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="salesCostAccount" name="salesCostAccount" aria-describedby="salesCostAccountHelp" value="{$salesCostAccount}" placeholder="Sales Cost Account...">
        <small id="errorSalesCostAccount" class="form-text text-danger">{$errorSalesCostAccount}</small>
    </div>
    <div class="form-group">
        <label for="fourDigitCode">Four Digit Code <small id="fourDigitCodeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="fourDigitCode" name="fourDigitCode" aria-describedby="fourDigitCodeHelp" value="{$fourDigitCode}" placeholder="Four Digit Code...">
        <small id="errorFourDigitCode" class="form-text text-danger">{$errorFourDigitCode}</small>
    </div>
    <div class="form-group">
        <label for="orgName">Org Name <small id="orgNameHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="orgName" name="orgName" aria-describedby="orgNameHelp" value="{$orgName}" placeholder="Org Name...">
        <small id="errorOrgName" class="form-text text-danger">{$errorOrgName}</small>
    </div>
    <br/>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>
    <br/>
    <br/>
    <br/>
</form>