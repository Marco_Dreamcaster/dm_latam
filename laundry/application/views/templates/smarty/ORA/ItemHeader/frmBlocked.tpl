<h4>{$title}</h4>
<form id="blocked" name="customEditItem" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-4 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Locked by IP</div>
                </div>
                <input type="text" class="form-control" id="user" name="user" value="{$previousEdit.user}" disabled>
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">valid until</div>
                </div>
                <input type="text" class="form-control text-danger" id="lockedUntil" name="lockedUntil" value="{$previousEdit.validUntil}" disabled>
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">time on server</div>
                </div>
                <input type="text" class="form-control text-primary" id="timeOnServer" name="timeOnServer" value="{$timeOnServer}" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Original Item Number (SIGLA)</div>
                </div>
                <input type="text" class="form-control" id="origItemNumber" name="origItemNumber" value="{$origItemNumber}" placeholder="Orig Item Number" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Item Number (ORACLE)</div>
                </div>
                <input type="text" class="form-control" id="itemNumber" name="itemNumber" value="{$itemNumber}" placeholder="Item Number" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Description (EN)</div>
                </div>
                <input type="text" class="form-control" id="description" name="description" value="{$description}" placeholder="Description" disabled>
            </div>
        </div>
    </div>
    <br/>

    <div class="form-group">
        <a class="btn btn-primary my-2 my-sm-0" href="{base_url()}{$action}">{$actionLabel}</a>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel <i class="fas fa-ban fa-sm fa-fw"></i></a>
    </div>

</form>
<br/>
<br/>
<br/>

