<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><span class="flag-icon flag-icon-{strtolower($country)}" style="margin-top:10px;"></span>&nbsp;<span class="mt-2">{strtoupper($country)} {$title}</span>&nbsp;</h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-success" target="_blank" href="{base_url()}IDL/Navigate/navigate/{$country}/Inventory/ItemHeader" data-toggle="tooltip" data-placement="top" title="specification for {$country} item header"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Inventory/cleansed/{$country}/0/{$sessionSelCol}/{if $sessionSelAsc}1{else}0{/if}" data-toggle="tooltip" data-placement="top" title="cleansed Items for {$country}"><i class="fas fa-industry fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Inventory/dirty/{$country}" data-toggle="tooltip" data-placement="top" title="dirty Items for {$country}"><i class="fas fa-trash-alt fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-secondary" href="{base_url()}{$cancelAction}" data-toggle="tooltip" data-placement="top" title="return to dirty Items for {$country}"><i class="fas fa-ban fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>
{if ($recommended)}
<div class="row">
    <div class="col-sm-12 p-2 text-success">
        <p class="alert-success text-center p-2">recommended item</p>
    </div>
</div>
{/if}
<div class="row">
    <div class="col-sm-12 p-2 text-success">
        <p class="alert-info text-center p-2">ranking {$ranking}</p>
    </div>
</div>
<form id="customDirtyItem" name="customDirtyItem" action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorOrigItemNumber" class="form-text text-danger">&nbsp;{$errorOrigItemNumber}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Original Item Number (SIGLA)</div>
                </div>
                <input type="text" class="form-control" id="origItemNumber" name="origItemNumber" value="{$origItemNumber}" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorItemNumber" name="errorItemNumber" class="form-text text-danger">&nbsp;{$errorItemNumber}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Item Number (ORACLE)</div>
                </div>
                <input type="text" class="form-control" id="itemNumber" name="itemNumber" value="{$itemNumber}" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Description (EN)</div>
                </div>
                <input type="text" class="form-control" id="description" name="description" value="{$description}" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Description (ESA)</div>
                </div>
                <input type="text" class="form-control" id="descriptionEsa" name="descriptionEsa" value="{$descriptionEsa}" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Observations</div>
                </div>
                <textarea type="text" class="form-control" id="observations" name="observations" value="" placeholder="Observations" rows="2" disabled>{$observations}</textarea>
            </div>
            <label class="alert-warning text-normal"><i class="fas fa-exclamation-circle fa-lg fa-fw"></i>The loading routines always bring CODIGO INTERNO (aka CODIGO MASSIVO) as an observation.  This is used to reduce the amount of cleansing work in future migrations</label>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 mt-4">
            <h5>Cleansing Rules</h5>
            <h6>ranking: {$ranking}</h6>
        </div>
    </div>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="rule1-tab" data-toggle="tab" href="#rule1" role="tab" aria-controls="rule1" aria-selected="true">By decision of Key User</a>
        </li>
{if (($recommended==1)&&($ranking>=5))}
        <li class="nav-item">
            <a class="nav-link" id="rule2-tab" data-toggle="tab" href="#rule2" role="tab" aria-controls="rule2" aria-selected="false">SALDO > 0</a>
        </li>
{/if}
{if (($startsWithL==1))}
        <li class="nav-item">
            <a class="nav-link" id="rule3-tab" data-toggle="tab" href="#rule3" role="tab" aria-controls="rule3" aria-selected="false">Item CODIGO INTERNO starting with L</a>
        </li>
{/if}
{if (sizeof($relatedOrigItemNumber)>0)}
    <li class="nav-item">
        <a class="nav-link" id="rule4-tab" data-toggle="tab" href="#rule4" role="tab" aria-controls="rule4" aria-selected="false">similar SIGLA ID</a>
    </li>
{/if}
{if (sizeof($relatedCodigoInterno)>0)}
    <li class="nav-item">
        <a class="nav-link" id="rule5-tab" data-toggle="tab" href="#rule5" role="tab" aria-controls="rule5" aria-selected="false">similar CODIGO INTERNO</a>
    </li>
{/if}

    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="rule1" role="tabpanel" aria-labelledby="home-tab">
            <h6 class="m-2">Migrated only by KU decision</h6>
            <div class="form-row">
                <div class="col-sm-12 m-2">
                    <div class="align-items-center">
                        <div class="">
                            <button type="submit" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="move to stage">{$actionLabel} <i class="fas fa-industry fa-sm fa-fw"></i></button>
                            <a class="btn btn-primary my-2 my-sm-0" href="{base_url()}ItemCrossRef/searchAssociationDirtyToCleansed/{$id}">Associate to Item <i class="fas fa-search fa-sm fa-fw"></i> % </a>
                            <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}"><i class="fas fa-ban fa-sm fa-fw"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="rule2" role="tabpanel" aria-labelledby="rule2-tab">
            <h6 class="m-2">+5 for SALDO>0 (as of May/2018)</h6>
            <div class="form-row">
                <div class="col-sm-12 m-2">
                    <div class="align-items-center">
                        <div class="">
                            <button type="submit" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="move to stage">{$actionLabel} <i class="fas fa-industry fa-sm fa-fw"></i></button>
                            <a class="btn btn-primary my-2 my-sm-0" href="{base_url()}ItemCrossRef/searchAssociationDirtyToCleansed/{$id}">Associate to Item <i class="fas fa-search fa-sm fa-fw"></i> % </a>
                            <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}"><i class="fas fa-ban fa-sm fa-fw"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="rule3" role="tabpanel" aria-labelledby="rule3-tab">
            <h6 class="m-2">+1 for either SIGLA ID (itemNumber) or CODIGO INTERNO starting with L : {$origItemNumber} OR {$itemNumber}</h6>
            <div class="form-row">
                <div class="col-sm-12 m-2">
                    <div class="align-items-center">
                        <div class="">
                            <button type="submit" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="move to stage">{$actionLabel} <i class="fas fa-industry fa-sm fa-fw"></i></button>
                            <a class="btn btn-primary my-2 my-sm-0" href="{base_url()}ItemCrossRef/searchAssociationDirtyToCleansed/{$id}">Associate to Item <i class="fas fa-search fa-sm fa-fw"></i> % </a>
                            <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}"><i class="fas fa-ban fa-sm fa-fw"></i> </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="tab-pane fade" id="rule4" role="tabpanel" aria-labelledby="rule4-tab">
            <h6 class="m-2">+1 for similar SIGLA ID (originalItemNumber)</h6>
            {foreach from=$relatedOrigItemNumber item=rel}
                <div class="form-row">
                    <div class="col-sm-12 m-2">
                        <div class="align-items-center">
                            <div class="">
                                <a class="btn btn-success my-2 my-sm-0" href="{base_url()}Inventory/customEditCleansed/{$rel.country}/{$rel.id}" target="_blank" data-toggle="tooltip" data-placement="top" title="inspect related item {$rel.origItemNumber}"><i class="fas fa-eye fa-sm fa-fw"></i> </a>
                                <a class="btn btn-primary my-2 my-sm-0" href="{base_url()}ItemCrossRef/confirmAssociationDirtyToCleansed/{$id}/{$rel.id}">Associate to Item {$rel.origItemNumber}</i> </a>
                                <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}"><i class="fas fa-ban fa-sm fa-fw"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            {/foreach}
        </div>
        <div class="tab-pane fade" id="rule5" role="tabpanel" aria-labelledby="rule5-tab">
            <h6 class="m-2">+1 for similar CODIGO INTERNO (itemNumber, aka CODIGO MASSIVO)</h6>
            {foreach from=$relatedCodigoInterno item=rel}
                <div class="form-row">
                    <div class="col-sm-12 m-2">
                        <div class="align-items-center">
                            <div class="">
                                <a class="btn btn-success my-2 my-sm-0" href="{base_url()}Inventory/customEditCleansed/{$rel.country}/{$rel.id}" target="_blank" data-toggle="tooltip" data-placement="top" title="inspect related item {$rel.origItemNumber}"><i class="fas fa-eye fa-sm fa-fw"></i> </a>
                                <a class="btn btn-primary my-2 my-sm-0" href="{base_url()}ItemCrossRef/confirmAssociationDirtyToCleansed/{$id}/{$rel.id}">Associate to Item {$rel.itemNumber}</i> </a>
                                <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}"><i class="fas fa-ban fa-sm fa-fw"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            {/foreach}
        </div>
    </div>






    </br>
</br>


</form>
<br/>
<br/>
<br/>
