<form id="customEditItem" name="customEditItem" action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <a class="btn text-light {if (($parent.locallyValidated)&&($parent.crossRefValidated)) }btn-success{else}btn-warning{/if}" target="_blank" href="{base_url()}Inventory/customEditCleansed/{$country}/{$parent.id}" data-toggle="tooltip" data-placement="top" title="quick view of Item {$parent.itemNumber}"><i class="fas fa-link fa-sm fa-fw"></i></a>
                </div>
                <div class="input-group-prepend">
                    <div class="input-group-text">Parent Original Item Number (SIGLA)</div>
                </div>
                <input type="text" class="form-control" id="parentOriginalItemNumber" name="parentOrigItemNumber" value="{$parent['origItemNumber']}" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Parent Item Number (ORACLE)</div>
                </div>
                <input type="text" class="form-control" id="parentItemNumber" name="parentItemNumber" value="{$parent['itemNumber']}" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Original Item Number (SIGLA)</div>
                </div>
                <input type="text" class="form-control" id="origItemNumber" name="origItemNumber" value="{$origItemNumber}" placeholder="Orig Item Number" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Item Number (ORACLE)</div>
                </div>
                <input type="text" class="form-control" id="itemNumber" name="itemNumber" value="{$itemNumber}" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Description (EN)</div>
                </div>
                <input type="text" class="form-control" id="description" name="description" value="{$description}" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Description (ESA)</div>
                </div>
                <input type="text" class="form-control" id="descriptionEsa" name="descriptionEsa" value="{$descriptionEsa}" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Long Description</div>
                </div>
                <textarea type="text" class="form-control" id="longDescription" name="longDescription" rows="5" disabled>{$longDescription}</textarea>
            </div>
        </div>
    </div>
    {if $country=='PE'}
        <div class="form-row align-items-center">
            <div class="col-sm-6 my-1">
                <small id="errorPeExtAttribute1" class="form-text text-danger">&nbsp;{$errorPeExtAttribute1}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            National Subheading <small class="text-muted">&nbsp;(peExtAttribute1)</small><span class="flag-icon flag-icon-pe mr-2 ml-2" title="PE only"></span>
                        </div>
                    </div>
                    <select class="form-control" id="peExtAttribute1" name="peExtAttribute1">
                        <option {if $peExtAttribute1==='270112000000'}selected{/if} value="270112000000">270112000000 - HULLA BITUMINOSA</option>
                        <option {if $peExtAttribute1==='270119000000'}selected{/if} value="270119000000">270119000000 - ANOTHER HULLAS</option>
                        <option {if $peExtAttribute1==='271011121090'}selected{/if} value="271011121090">271011121090 - FUEL TO MOTORS</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 my-1">
                <small id="errorPeExtAttribute2" class="form-text text-danger">&nbsp;{$errorPeExtAttribute2}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            Existence Type <small class="text-muted">&nbsp;(peExtAttribute2)</small><span class="flag-icon flag-icon-pe mr-2 ml-2" title="PE only"></span>
                        </div>
                    </div>
                    <select class="form-control" id="peExtAttribute2" name="peExtAttribute2">
                        <option {if $peExtAttribute2==='01'}selected{/if} value="01">01 - MERCHANDISE</option>
                        <option {if $peExtAttribute2==='02'}selected{/if} value="02">02 - FINISHED PRODUCT</option>
                        <option {if $peExtAttribute2==='03'}selected{/if} value="03">03 - RAW MATERIAL AND AUXILIARY</option>
                        <option {if $peExtAttribute2==='04'}selected{/if} value="04">04 - PACKAGING</option>
                        <option {if $peExtAttribute2==='05'}selected{/if} value="05">05 - MISCELLANEOUS SUPPLIES</option>
                        <option {if $peExtAttribute2==='99'}selected{/if} value="99">99 - OTHERS (SPECIFY)</option>
                    </select>
                </div>
            </div>
        </div>
    {/if}
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Template Name</div>
                </div>
                <select class="form-control" id="templateName" name="templateName" aria-describedby="templateNameHelp" disabled>
                    <option {if $templateName==='LA98'}selected{/if} value='LA98'>LA98</option>
                    <option {if $templateName==='LA01ASE'}selected{/if} value='LA01ASE'>LA01ASE</option>
                    <option {if $templateName==='LA01ASM'}selected{/if} value='LA01ASM'>LA01ASM</option>
                    <option {if $templateName==='LA02ESE'}selected{/if} value='LA02ESE'>LA02ESE</option>
                    <option {if $templateName==='LA02ESM'}selected{/if} value='LA02ESM'>LA02ESM</option>
                    <option {if $templateName==='LA02RAE'}selected{/if} value='LA02RAE'>LA02RAE</option>
                    <option {if $templateName==='LA02RAM'}selected{/if} value='LA02RAM'>LA02RAM</option>
                    <option {if $templateName==='LA03ASE'}selected{/if} value='LA03ASE'>LA03ASE</option>
                    <option {if $templateName==='LA03ASM'}selected{/if} value='LA03ASM'>LA03ASM</option>
                    <option {if $templateName==='LA04ASE'}selected{/if} value='LA04ASE'>LA04ASE</option>
                    <option {if $templateName==='LA04ASM'}selected{/if} value='LA04ASM'>LA04ASM</option>
                    <option {if $templateName==='LA05ASE'}selected{/if} value='LA05ASE'>LA05ASE</option>
                    <option {if $templateName==='LA05ASM'}selected{/if} value='LA05ASM'>LA05ASM</option>
                    <option {if $templateName==='LA05ESE'}selected{/if} value='LA05ESE'>LA05ESE</option>
                    <option {if $templateName==='LA05ESM'}selected{/if} value='LA05ESM'>LA05ESM</option>
                    <option {if $templateName==='LA05RAE'}selected{/if} value='LA05RAE'>LA05RAE</option>
                    <option {if $templateName==='LA05RAM'}selected{/if} value='LA05RAM'>LA05RAM</option>
                    <option {if $templateName==='LA06AEE'}selected{/if} value='LA06AEE'>LA06AEE</option>
                    <option {if $templateName==='LA06AEM'}selected{/if} value='LA06AEM'>LA06AEM</option>
                    <option {if $templateName==='LA07ASE'}selected{/if} value='LA07ASE'>LA07ASE</option>
                    <option {if $templateName==='LA07ASM'}selected{/if} value='LA07ASM'>LA07ASM</option>
                    <option {if $templateName==='LA08FIE'}selected{/if} value='LA08FIE'>LA08FIE</option>
                    <option {if $templateName==='LA08FIM'}selected{/if} value='LA08FIM'>LA08FIM</option>
                    <option {if $templateName==='LA09ASE'}selected{/if} value='LA09ASE'>LA09ASE</option>
                    <option {if $templateName==='LA09ASM'}selected{/if} value='LA09ASM'>LA09ASM</option>
                    <option {if $templateName==='LA10ASE'}selected{/if} value='LA10ASE'>LA10ASE</option>
                    <option {if $templateName==='LA10ASM'}selected{/if} value='LA10ASM'>LA10ASM</option>
                    <option {if $templateName==='LA10ESE'}selected{/if} value='LA10ESE'>LA10ESE</option>
                    <option {if $templateName==='LA10ESM'}selected{/if} value='LA10ESM'>LA10ESM</option>
                    <option {if $templateName==='LA10RAE'}selected{/if} value='LA10RAE'>LA10RAE</option>
                    <option {if $templateName==='LA10RAM'}selected{/if} value='LA10RAM'>LA10RAM</option>
                    <option {if $templateName==='LA11ASE'}selected{/if} value='LA11ASE'>LA11ASE</option>
                    <option {if $templateName==='LA11ASM'}selected{/if} value='LA11ASM'>LA11ASM</option>
                    <option {if $templateName==='LA11ESE'}selected{/if} value='LA11ESE'>LA11ESE</option>
                    <option {if $templateName==='LA11ESM'}selected{/if} value='LA11ESM'>LA11ESM</option>
                    <option {if $templateName==='LA11RAE'}selected{/if} value='LA11RAE'>LA11RAE</option>
                    <option {if $templateName==='LA11RAM'}selected{/if} value='LA11RAM'>LA11RAM</option>
                    <option {if $templateName==='LA12ASE'}selected{/if} value='LA12ASE'>LA12ASE</option>
                    <option {if $templateName==='LA12ASM'}selected{/if} value='LA12ASM'>LA12ASM</option>
                    <option {if $templateName==='LA12ESE'}selected{/if} value='LA12ESE'>LA12ESE</option>
                    <option {if $templateName==='LA12ESM'}selected{/if} value='LA12ESM'>LA12ESM</option>
                    <option {if $templateName==='LA12RAE'}selected{/if} value='LA12RAE'>LA12RAE</option>
                    <option {if $templateName==='LA12RAM'}selected{/if} value='LA12RAM'>LA12RAM</option>
                    <option {if $templateName==='LA13ASE'}selected{/if} value='LA13ASE'>LA13ASE</option>
                    <option {if $templateName==='LA13ASM'}selected{/if} value='LA13ASM'>LA13ASM</option>
                    <option {if $templateName==='LA14ASE'}selected{/if} value='LA14ASE'>LA14ASE</option>
                    <option {if $templateName==='LA14ASM'}selected{/if} value='LA14ASM'>LA14ASM</option>
                    <option {if $templateName==='LA15FIE'}selected{/if} value='LA15FIE'>LA15FIE</option>
                    <option {if $templateName==='LA15FIM'}selected{/if} value='LA15FIM'>LA15FIM</option>
                    <option {if $templateName==='LA99ASE'}selected{/if} value='LA99ASE'>LA99ASE</option>
                    <option {if $templateName==='LA99ASM'}selected{/if} value='LA99ASM'>LA99ASM</option>
                    <option {if $templateName==='LA99ESE'}selected{/if} value='LA99ESE'>LA99ESE</option>
                    <option {if $templateName==='LA99ESM'}selected{/if} value='LA99ESM'>LA99ESM</option>
                    <option {if $templateName==='LA99RAE'}selected{/if} value='LA99RAE'>LA99RAE</option>
                    <option {if $templateName==='LA99RAM'}selected{/if} value='LA99RAM'>LA99RAM</option>
                    <option {if $templateName==='LA99HEH'}selected{/if} value='LA99HEH'>LA99HEH</option>
                    <option {if $templateName==='LA99COI'}selected{/if} value='LA99COI'>LA99COI</option>
                    <option {if $templateName==='LA99COC'}selected{/if} value='LA99COC'>LA99COC</option>
                    <option {if $templateName==='LA99PPP'}selected{/if} value='LA99PPP'>LA99PPP</option>
                    <option {if $templateName==='LA99ROR'}selected{/if} value='LA99ROR'>LA99ROR</option>
                    <option {if $templateName==='LA00SES'}selected{/if} value='LA00SES'>LA00SES</option>
                </select>
            </div>
        </div>
        <div class="col-sm-1 col-md-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Has Picture?</div>
                </div>
                <input type="checkbox" {($hasPicture==true)?'checked':''} style="margin:10px;" id="hasPicture" name="hasPicture" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Category Set Name (LPA)</div>
                </div>
                <select class="form-control" id="itemCategorySetName" name="itemCategorySetName" disabled>
                    <option {if $itemCategory['categorySetName']==='TKE PO Item Category'}selected{/if} value='TKE PO Item Category'>TKE PO Item Category</option>
                </select>
            </div>
        </div>
    </div>

    {include file='./itemCategoryName.tpl'}

    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorObservations" class="form-text text-danger">&nbsp;{$errorObservations}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Observations</div>
                </div>
                <textarea type="text" class="form-control" id="observations" name="observations" value="" placeholder="Observations" rows="5">{$observations}</textarea>
            </div>
            <label class="alert-info text-info"><i class="fas fa-bug fa-lg fa-fw"></i> Help us detect bugs: use this field to annotate special cases, cleansing decisions, and relevant issues related to this record.</label>
            <label class="alert-warning text-normal"><i class="fas fa-exclamation-circle fa-lg fa-fw"></i>Please don't erase the original CODIGO INTERNO (aka CODIGO MASSIVO) from this observation.   This information will be used to reduce the amount of cleansing work in future migrations</label>
        </div>
    </div>
    <br/>


    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel} <i class="fas fa-check fa-sm fa-fw"></i></button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel <i class="fas fa-ban fa-sm fa-fw"></i></a>
    </div>

</form>
<br/>
<br/>
<h4>Manufacturer Part Number</h4>
<div class="row mt-2">
    <div class="col-sm-12">
        <div class="alert-success text-center p-4">
            <p>same manufacturers as <span class="flag-icon flag-icon-{strtolower($parent['country'])}" style="margin-top:10px;"></span> {$parent['itemNumber']} </p>
            <a class="btn text-light {if (($parent.locallyValidated)&&($parent.crossRefValidated)) }btn-success{else}btn-warning{/if}" target="_blank" href="{base_url()}Inventory/customEditCleansed/{$country}/{$parent.id}" data-toggle="tooltip" data-placement="top" title="quick view of Item {$parent.itemNumber}"><i class="fas fa-link fa-sm fa-fw"></i></a>
        </div>
    </div>
</div>
<br/>
<h4>Cross Reference Records  </h4>
{if isset($itemCrossRef) && (sizeof($itemCrossRef)>0)}
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th style="width:90px">#</th>
            <th style="width:90px">id</th>
            <th>Item Number</th>
            <th>Country</th>
            <th>Cross Reference Type</th>
            <th>ID</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$itemCrossRef item=icr name=foo}
            <tr class="{if ($icr.dismissed)} dismissed {elseif (($icr.locallyValidated)&&($icr.crossRefValidated)) } alert-success {else} alert-warning {/if}">
                <td></td>
                <td>{$icr.id}</td>
                <td>{$icr.itemNumber}</td>
                <td>{$icr.country}</td>
                <td>{$icr.crossReferenceType}</td>
                <td>{$icr.crossReference}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{/if}
<br/>
<br/>
<h4>Item Transaction Default Sub Inventory</h4>
{if isset($itemTransDefSubInv) && (sizeof($itemTransDefSubInv)>0)}
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th style="width:90px">#</th>
            <th style="width:90px">id</th>
            <th>Item Number</th>
            <th>Organization Code</th>
            <th>Sub Inventory Name</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$itemTransDefSubInv item=itdsi name=foo}
            <tr class="{if ($itdsi.dismissed)} dismissed {elseif (($itdsi.locallyValidated)&&($itdsi.crossRefValidated)) } alert-success {else} alert-warning {/if}">
                <td scope="row">
                </td>
                <td>{$itdsi.id}</td>
                <td>{$itdsi.itemNumber}</td>
                <td>{$itdsi.organizationCode}</td>
                <td>{$itdsi.subinventoryName}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{/if}
<br/>
<br/>
<h4>Item Transaction Default Locators</h4>
{if isset($itemTransDefLoc) && (sizeof($itemTransDefLoc)>0)}
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th style="width:90px">#</th>
            <th style="width:90px">id</th>
            <th>action</th>
            <th>Item Number</th>
            <th>Organization Code</th>
            <th>Sub Inventory Name</th>
            <th>Locator Name</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$itemTransDefLoc item=itdl name=foo}
            <tr class="{if ($itdl.dismissed)} dismissed {elseif (($itdl.locallyValidated)&&($itdl.crossRefValidated)) } alert-success {else} alert-warning {/if} clickable-row" data-href='{base_url()}ItemTransDefLoc/customEditCleansed/{$itdl.country}/{$itdl.id}'>
                <td scope="row">
                    {if !$dismissed}
                    <a href="{base_url()}ItemTransDefLoc/toggle/{$itdl.id}" data-toggle="tooltip" data-placement="top" title="{if ($itdl.dismissed)}Recall{else}Dismiss{/if} {$itdl.organizationCode} - {$itdl.subinventoryName}">{if ($itdl.dismissed)}<i class="text-primary fas fa-plus-circle fa-sm fa-fw">{else}<i class="text-secondary fas fa-minus-circle fa-sm fa-fw">{/if}</i></a>
                    {/if}
                </td>
                <td>{$itdl.id}</td>
                <td>{$itdl.action}</td>
                <td>{$itdl.itemNumber}</td>
                <td>{$itdl.organizationCode}</td>
                <td>{$itdl.subinventoryName}</td>
                <td>{$itdl.locatorName}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{/if}
<br/>

<br/>
<br/>

