<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2">{if !isset($mergedId)}{if $childless}<i class="fas fa-lg fa-user fa-fw m-2 text-stage"></i>{else}<i class="fas fa-lg fa-user-tie fa-fw m-2 text-stage"></i>{/if}{else}<i class="fas fa-lg fa-user-tag fa-fw m-2 text-stage"></i>{/if}&nbsp;<span class="m-2"><span class="flag-icon flag-icon-{strtolower($country)}"></span> {strtoupper($country)} {$title}</span></h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-success" target="_blank" href="{base_url()}IDL/Navigate/navigate/{$country}/Inventory/ItemHeader" data-toggle="tooltip" data-placement="top" title="specification for {$country} item header"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Inventory/cleansed/{$country}/0/{$sessionSelCol}/{if $sessionSelAsc}1{else}0{/if}" data-toggle="tooltip" data-placement="top" title="cleansed Items for {$country}"><i class="fas fa-industry fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Inventory/dirty/{$country}" data-toggle="tooltip" data-placement="top" title="dirty Items for {$country}"><i class="fas fa-trash-alt fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-secondary" target="_self" href="{base_url()}{$cancelAction}" data-toggle="tooltip" data-placement="top" title="go back to cleansed items"><i class="fas fa-ban fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>
<div class="alert-warning p-3">
    <p><span class="flag-icon flag-icon-br"></span> &nbsp;Tela de associa��o de arquivos em fase inicial de testes</p>
    <ul>
        <li>apenas arquivos do tipo JPG, GIF, PNG</li>
        <li>nomes de arquivo sem espa�os e acentos</li>
        <li>tamanho m�ximo : 5120b (5M)</li>
    </ul>
</div>
<form id="uploadResource" name="uploadResource" action="{base_url()}{$action}" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <small id="uploadError" name="uploadError" class="form-text text-danger">&nbsp;{$uploadError}</small>
        <div class="custom-file">
            <input id="userfile" name="userfile" type="file" class="custom-file-input">
            <label for="userfile" class="custom-file-label text-truncate">Choose file...</label>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel} <i class="fas fa-file-upload fa-sm fa-fw"></i></button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel <i class="fas fa-ban fa-sm fa-fw"></i></a>
    </div>
</form>


<script>
    $('.custom-file-input').on('change', function() {
        fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
    });
</script>
<br/>

<br/>
<br/>

