<div class="form-row align-items-center">
    <div class="col-sm-12 my-1">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text alert-danger">Category Name (OLD to Oracle)</div>
            </div>
            <select class="form-control" id="itemCategoryName" name="itemCategoryName">
                <option {if $itemCategory['concatCategoryName']==='10000000'}selected{/if} value='10000000'>10000000 Energ�a</option>
                <option {if $itemCategory['concatCategoryName']==='10039800'}selected{/if} value='10039800'>10039800 Energ�a el�ctrica sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='10159800'}selected{/if} value='10159800'>10159800 Combustibles sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='10180298'}selected{/if} value='10180298'>10180298 Gas natural sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='10219800'}selected{/if} value='10219800'>10219800 Agua sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='20240998'}selected{/if} value='20240998'>20240998 Aceites lubricantes </option>
                <option {if $itemCategory['concatCategoryName']==='20241198'}selected{/if} value='20241198'>20241198 Grasas/pastas lubricantes</option>
                <option {if $itemCategory['concatCategoryName']==='20241398'}selected{/if} value='20241398'>20241398 Fluidos a presi�n sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='20249800'}selected{/if} value='20249800'>20249800 Lubricantes sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='20270200'}selected{/if} value='20270200'>20270200 Pinturas</option>
                <option {if $itemCategory['concatCategoryName']==='20270300'}selected{/if} value='20270300'>20270300 Barnices</option>
                <option {if $itemCategory['concatCategoryName']==='20271000'}selected{/if} value='20271000'>20271000 Agentes anticorrosivos</option>
                <option {if $itemCategory['concatCategoryName']==='20279800'}selected{/if} value='20279800'>20279800 Protecci�n superficial sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='20450300'}selected{/if} value='20450300'>20450300 Limpiadores dom�sticos</option>
                <option {if $itemCategory['concatCategoryName']==='20519800'}selected{/if} value='20519800'>20519800 Gases industriales sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='20609800'}selected{/if} value='20609800'>20609800 Sustancias de ba�o sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='29060203'}selected{/if} value='29060203'>29060203 Soportes de tubo Aleados</option>
                <option {if $itemCategory['concatCategoryName']==='29060204'}selected{/if} value='29060204'>29060204 Soportes de tubo Inoxidables</option>
                <option {if $itemCategory['concatCategoryName']==='35065400'}selected{/if} value='35065400'>35065400 Otros paneles </option>
                <option {if $itemCategory['concatCategoryName']==='35065600'}selected{/if} value='35065600'>35065600 Cristal cabina</option>
                <option {if $itemCategory['concatCategoryName']==='35210200'}selected{/if} value='35210200'>35210200 Pieza moldeada por inyecci�n</option>
                <option {if $itemCategory['concatCategoryName']==='35219800'}selected{/if} value='35219800'>35219800 Productos pl�stico </option>
                <option {if $itemCategory['concatCategoryName']==='35249800'}selected{/if} value='35249800'>35249800 Productos caucho </option>
                <option {if $itemCategory['concatCategoryName']==='35309800'}selected{/if} value='35309800'>35309800 Juntas sin diferenciacion</option>
                <option {if $itemCategory['concatCategoryName']==='35330200'}selected{/if} value='35330200'>35330200 Mangueras hidr�ulicas</option>
                <option {if $itemCategory['concatCategoryName']==='35330300'}selected{/if} value='35330300'>35330300 Uniones de mangueras</option>
                <option {if $itemCategory['concatCategoryName']==='35339800'}selected{/if} value='35339800'>35339800 Mangueras sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='35369800'}selected{/if} value='35369800'>35369800 Resinas sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='35399800'}selected{/if} value='35399800'>35399800 Adhesivos sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='35519800'}selected{/if} value='35519800'>35519800 Productos textiles sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='40030200'}selected{/if} value='40030200'>40030200 Material de fijaci�n electr.</option>
                <option {if $itemCategory['concatCategoryName']==='40030500'}selected{/if} value='40030500'>40030500 Conexiones por enchufe generales</option>
                <option {if $itemCategory['concatCategoryName']==='40030600'}selected{/if} value='40030600'>40030600 Canales para cables y mangueras</option>
                <option {if $itemCategory['concatCategoryName']==='40030700'}selected{/if} value='40030700'>40030700 Bornes</option>
                <option {if $itemCategory['concatCategoryName']==='40039800'}selected{/if} value='40039800'>40039800 Material el�ctrico</option>
                <option {if $itemCategory['concatCategoryName']==='40060400'}selected{/if} value='40060400'>40060400 Instalaciones USV</option>
                <option {if $itemCategory['concatCategoryName']==='40069800'}selected{/if} value='40069800'>40069800 Abastecimiento energ�a </option>
                <option {if $itemCategory['concatCategoryName']==='40090202'}selected{/if} value='40090202'>40090202 Transformadores de baja tensi�n</option>
                <option {if $itemCategory['concatCategoryName']==='40090204'}selected{/if} value='40090204'>40090204 Minitransformadores</option>
                <option {if $itemCategory['concatCategoryName']==='40090298'}selected{/if} value='40090298'>40090298 Transformadores sin diferenciaci�n </option>
                <option {if $itemCategory['concatCategoryName']==='40099800'}selected{/if} value='40099800'>40099800 Transformaci�n energ�a </option>
                <option {if $itemCategory['concatCategoryName']==='40129800'}selected{/if} value='40129800'>40129800 Bater�as sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='40150300'}selected{/if} value='40150300'>40150300 Conductos aislados</option>
                <option {if $itemCategory['concatCategoryName']==='40150500'}selected{/if} value='40150500'>40150500 Cables para corriente de alta intensidad</option>
                <option {if $itemCategory['concatCategoryName']==='40150600'}selected{/if} value='40150600'>40150600 Tuber�as de mando</option>
                <option {if $itemCategory['concatCategoryName']==='40150700'}selected{/if} value='40150700'>40150700 Conductos electr�nicos confeccionados</option>
                <option {if $itemCategory['concatCategoryName']==='40151100'}selected{/if} value='40151100'>40151100 Cables blindados</option>
                <option {if $itemCategory['concatCategoryName']==='40159800'}selected{/if} value='40159800'>40159800 Cables y conductos sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='40180202'}selected{/if} value='40180202'>40180202 Armarios de distribuci�n sin equipamiento</option>
                <option {if $itemCategory['concatCategoryName']==='40180203'}selected{/if} value='40180203'>40180203 Armarios de distribuci�n equipados</option>
                <option {if $itemCategory['concatCategoryName']==='40180300'}selected{/if} value='40180300'>40180300 Accesorios para armarios el�ctricos</option>
                <option {if $itemCategory['concatCategoryName']==='40210200'}selected{/if} value='40210200'>40210200 Resistencias</option>
                <option {if $itemCategory['concatCategoryName']==='40210300'}selected{/if} value='40210300'>40210300 Condensadores</option>
                <option {if $itemCategory['concatCategoryName']==='40210400'}selected{/if} value='40210400'>40210400 Potenci�metros</option>
                <option {if $itemCategory['concatCategoryName']==='40210500'}selected{/if} value='40210500'>40210500 Semiconductores</option>
                <option {if $itemCategory['concatCategoryName']==='40210600'}selected{/if} value='40210600'>40210600 Placa de circuito impreso</option>
                <option {if $itemCategory['concatCategoryName']==='40210700'}selected{/if} value='40210700'>40210700 Conjunto de circuitos impresos</option>
                <option {if $itemCategory['concatCategoryName']==='40210800'}selected{/if} value='40210800'>40210800 Transistores</option>
                <option {if $itemCategory['concatCategoryName']==='40211000'}selected{/if} value='40211000'>40211000 Cuerpos refrigerantes</option>
                <option {if $itemCategory['concatCategoryName']==='40211100'}selected{/if} value='40211100'>40211100 Generadores de impulsos</option>
                <option {if $itemCategory['concatCategoryName']==='40211200'}selected{/if} value='40211200'>40211200 M�dulos de potencia </option>
                <option {if $itemCategory['concatCategoryName']==='40219800'}selected{/if} value='40219800'>40219800 Elementos de construcci�n electr�nicos </option>
                <option {if $itemCategory['concatCategoryName']==='40240298'}selected{/if} value='40240298'>40240298 Motores corriente continua </option>
                <option {if $itemCategory['concatCategoryName']==='40240398'}selected{/if} value='40240398'>40240398 Motor corriente trif�sica </option>
                <option {if $itemCategory['concatCategoryName']==='40240400'}selected{/if} value='40240400'>40240400 Motorreductor</option>
                <option {if $itemCategory['concatCategoryName']==='40240600'}selected{/if} value='40240600'>40240600 Freno de motor</option>
                <option {if $itemCategory['concatCategoryName']==='40240700'}selected{/if} value='40240700'>40240700 Servomotor</option>
                <option {if $itemCategory['concatCategoryName']==='40241000'}selected{/if} value='40241000'>40241000 Piezas de repuesto para electromotores</option>
                <option {if $itemCategory['concatCategoryName']==='40249800'}selected{/if} value='40249800'>40249800 Electromotores sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='40259800'}selected{/if} value='40259800'>40259800 Generadores sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='40270200'}selected{/if} value='40270200'>40270200 Aparatos comando/se�alizadores</option>
                <option {if $itemCategory['concatCategoryName']==='40270300'}selected{/if} value='40270300'>40270300 Contactores</option>
                <option {if $itemCategory['concatCategoryName']==='40270400'}selected{/if} value='40270400'>40270400 Rel�s</option>
                <option {if $itemCategory['concatCategoryName']==='40270500'}selected{/if} value='40270500'>40270500 Elementos indicadores</option>
                <option {if $itemCategory['concatCategoryName']==='40270600'}selected{/if} value='40270600'>40270600 Elementos de mando</option>
                <option {if $itemCategory['concatCategoryName']==='40270700'}selected{/if} value='40270700'>40270700 Conmutadores de baja tensi�n</option>
                <option {if $itemCategory['concatCategoryName']==='40279800'}selected{/if} value='40279800'>40279800 Aparatos conmutaci�n/indicaci�n </option>
                <option {if $itemCategory['concatCategoryName']==='40309800'}selected{/if} value='40309800'>40309800 Fusibles el�ctricos sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='40339800'}selected{/if} value='40339800'>40339800 L�mparas sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='40360500'}selected{/if} value='40360500'>40360500 LED  </option>
                <option {if $itemCategory['concatCategoryName']==='40369800'}selected{/if} value='40369800'>40369800 Medios luminosos sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='40399800'}selected{/if} value='40399800'>40399800 Imanes sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='40429800'}selected{/if} value='40429800'>40429800 T�cnica automatizaci�n/control</option>
                <option {if $itemCategory['concatCategoryName']==='40451000'}selected{/if} value='40451000'>40451000 Adaptaciones el�ctricas</option>
                <option {if $itemCategory['concatCategoryName']==='40459800'}selected{/if} value='40459800'>40459800 T�cnica medici�n/control/regulaci�n </option>
                <option {if $itemCategory['concatCategoryName']==='40480200'}selected{/if} value='40480200'>40480200 Transductor de posici�n</option>
                <option {if $itemCategory['concatCategoryName']==='40480300'}selected{/if} value='40480300'>40480300 Interruptor magn�tico</option>
                <option {if $itemCategory['concatCategoryName']==='40480400'}selected{/if} value='40480400'>40480400 Barreras luminosas</option>
                <option {if $itemCategory['concatCategoryName']==='40480600'}selected{/if} value='40480600'>40480600 Dispositivos medici�n carga ascensores</option>
                <option {if $itemCategory['concatCategoryName']==='40489800'}selected{/if} value='40489800'>40489800 Sensores sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='40519800'}selected{/if} value='40519800'>40519800 Aparatos el�ctricos sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45060200'}selected{/if} value='45060200'>45060200 Pares de ruedas</option>
                <option {if $itemCategory['concatCategoryName']==='45060300'}selected{/if} value='45060300'>45060300 Correas trapezoidales</option>
                <option {if $itemCategory['concatCategoryName']==='45060400'}selected{/if} value='45060400'>45060400 Acoplamientos</option>
                <option {if $itemCategory['concatCategoryName']==='45060500'}selected{/if} value='45060500'>45060500 Engranajes</option>
                <option {if $itemCategory['concatCategoryName']==='45060600'}selected{/if} value='45060600'>45060600 Frenos y piezas de repuesto para frenos</option>
                <option {if $itemCategory['concatCategoryName']==='45060800'}selected{/if} value='45060800'>45060800 �rboles</option>
                <option {if $itemCategory['concatCategoryName']==='45060900'}selected{/if} value='45060900'>45060900 Engranajes</option>
                <option {if $itemCategory['concatCategoryName']==='45069800'}selected{/if} value='45069800'>45069800 T�cnica accionamiento mec�nico </option>
                <option {if $itemCategory['concatCategoryName']==='45090200'}selected{/if} value='45090200'>45090200 Cilindros hidr�ulicos</option>
                <option {if $itemCategory['concatCategoryName']==='45095000'}selected{/if} value='45095000'>45095000 Componentes hidr�ulicos</option>
                <option {if $itemCategory['concatCategoryName']==='45095100'}selected{/if} value='45095100'>45095100 Componentes neum�ticos</option>
                <option {if $itemCategory['concatCategoryName']==='45099800'}selected{/if} value='45099800'>45099800 Sistemas hidr�ulicos/neum�ticos </option>
                <option {if $itemCategory['concatCategoryName']==='45129800'}selected{/if} value='45129800'>45129800 Bombas sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45150302'}selected{/if} value='45150302'>45150302 Contrapeso  Acero</option>
                <option {if $itemCategory['concatCategoryName']==='45150303'}selected{/if} value='45150303'>45150303 Contrapeso  Hormig�n</option>
                <option {if $itemCategory['concatCategoryName']==='45150398'}selected{/if} value='45150398'>45150398 Contrapeso sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45150400'}selected{/if} value='45150400'>45150400 Unidad de accionamiento </option>
                <option {if $itemCategory['concatCategoryName']==='45150500'}selected{/if} value='45150500'>45150500 Regulador / limitador velocidad </option>
                <option {if $itemCategory['concatCategoryName']==='45150600'}selected{/if} value='45150600'>45150600 Unidad accionamiento hidr�ulicos </option>
                <option {if $itemCategory['concatCategoryName']==='45150700'}selected{/if} value='45150700'>45150700 Bastidor soporte </option>
                <option {if $itemCategory['concatCategoryName']==='45150802'}selected{/if} value='45150802'>45150802 Ra�les gu�a - laminados en fr�o</option>
                <option {if $itemCategory['concatCategoryName']==='45150803'}selected{/if} value='45150803'>45150803 Ra�les gu�a - armados</option>
                <option {if $itemCategory['concatCategoryName']==='45150804'}selected{/if} value='45150804'>45150804 Ra�les gu�a - de alta precisi�n</option>
                <option {if $itemCategory['concatCategoryName']==='45150805'}selected{/if} value='45150805'>45150805 Ra�les gu�a - perfiles conformados</option>
                <option {if $itemCategory['concatCategoryName']==='45150806'}selected{/if} value='45150806'>45150806 Ra�les gu�a - eclisas</option>
                <option {if $itemCategory['concatCategoryName']==='45150807'}selected{/if} value='45150807'>45150807 Ra�les gu�a - accesorios</option>
                <option {if $itemCategory['concatCategoryName']==='45150898'}selected{/if} value='45150898'>45150898 Ra�les/gu�as puertas</option>
                <option {if $itemCategory['concatCategoryName']==='45150900'}selected{/if} value='45150900'>45150900 Dispositivos de sujeci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45151000'}selected{/if} value='45151000'>45151000 Interruptores de la caja</option>
                <option {if $itemCategory['concatCategoryName']==='45151100'}selected{/if} value='45151100'>45151100 Puertas de cierre de cabina y de caja</option>
                <option {if $itemCategory['concatCategoryName']==='45151202'}selected{/if} value='45151202'>45151202 Accionamientos de puerta para ascensores</option>
                <option {if $itemCategory['concatCategoryName']==='45151203'}selected{/if} value='45151203'>45151203 Cierres/mecanismos puertas </option>
                <option {if $itemCategory['concatCategoryName']==='45151300'}selected{/if} value='45151300'>45151300 Tapizados/cercos puerta/p�rticos/umbrales puerta</option>
                <option {if $itemCategory['concatCategoryName']==='45151400'}selected{/if} value='45151400'>45151400 Cables acero cabina</option>
                <option {if $itemCategory['concatCategoryName']==='45151500'}selected{/if} value='45151500'>45151500 Cables pl�stico </option>
                <option {if $itemCategory['concatCategoryName']==='45151600'}selected{/if} value='45151600'>45151600 Elementos de mando/indicadores </option>
                <option {if $itemCategory['concatCategoryName']==='45151800'}selected{/if} value='45151800'>45151800 Rodillos de gu�a </option>
                <option {if $itemCategory['concatCategoryName']==='45151900'}selected{/if} value='45151900'>45151900 Componentes para la cabina</option>
                <option {if $itemCategory['concatCategoryName']==='45152000'}selected{/if} value='45152000'>45152000 Cableado el�ctrico del hueco</option>
                <option {if $itemCategory['concatCategoryName']==='45152100'}selected{/if} value='45152100'>45152100 Cableado del equipo de control</option>
                <option {if $itemCategory['concatCategoryName']==='45152200'}selected{/if} value='45152200'>45152200 Cadenas de compensaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45152300'}selected{/if} value='45152300'>45152300 Viga de m�quina</option>
                <option {if $itemCategory['concatCategoryName']==='45152400'}selected{/if} value='45152400'>45152400 Cabinas de ascensor</option>
                <option {if $itemCategory['concatCategoryName']==='45152502'}selected{/if} value='45152502'>45152502 Poleas para ascensores Fundici�n</option>
                <option {if $itemCategory['concatCategoryName']==='45152503'}selected{/if} value='45152503'>45152503 Poleas para ascensores Pl�stico</option>
                <option {if $itemCategory['concatCategoryName']==='45152598'}selected{/if} value='45152598'>45152598 Poleas de inversi�n sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45152602'}selected{/if} value='45152602'>45152602 Cables colgantes </option>
                <option {if $itemCategory['concatCategoryName']==='45152603'}selected{/if} value='45152603'>45152603 Cables colgantes montado </option>
                <option {if $itemCategory['concatCategoryName']==='45152698'}selected{/if} value='45152698'>45152698 Cables colgantes sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45152700'}selected{/if} value='45152700'>45152700 Mando de ascensores</option>
                <option {if $itemCategory['concatCategoryName']==='45152800'}selected{/if} value='45152800'>45152800 Interfono de ascensor</option>
                <option {if $itemCategory['concatCategoryName']==='45155000'}selected{/if} value='45155000'>45155000 Cubierta suelos</option>
                <option {if $itemCategory['concatCategoryName']==='45155102'}selected{/if} value='45155102'>45155102 Piezas repuesto para ascensores Otis</option>
                <option {if $itemCategory['concatCategoryName']==='45155103'}selected{/if} value='45155103'>45155103 Piezas repuesto para ascensores Schindler</option>
                <option {if $itemCategory['concatCategoryName']==='45155104'}selected{/if} value='45155104'>45155104 Piezas repuesto para ascensores Kone</option>
                <option {if $itemCategory['concatCategoryName']==='45155105'}selected{/if} value='45155105'>45155105 Piezas repuesto para ascensores Mitsubishi</option>
                <option {if $itemCategory['concatCategoryName']==='45155198'}selected{/if} value='45155198'>45155198 Piezas repuesto para ascensores sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45155202'}selected{/if} value='45155202'>45155202 Piezas repuesto para escaleras mec�nicas y rampas Otis</option>
                <option {if $itemCategory['concatCategoryName']==='45155203'}selected{/if} value='45155203'>45155203 Piezas repuesto para escaleras mec�nicas y rampas Schindler</option>
                <option {if $itemCategory['concatCategoryName']==='45155204'}selected{/if} value='45155204'>45155204 Piezas repuesto para escaleras mec�nicas y rampas Kone</option>
                <option {if $itemCategory['concatCategoryName']==='45155205'}selected{/if} value='45155205'>45155205 Piezas repuesto para escaleras mec�nicas y rampas Mitsubishi</option>
                <option {if $itemCategory['concatCategoryName']==='45155298'}selected{/if} value='45155298'>45155298 Piezas repuesto para escaleras mec�nicas y rampas</option>
                <option {if $itemCategory['concatCategoryName']==='45155302'}selected{/if} value='45155302'>45155302 Barreras</option>
                <option {if $itemCategory['concatCategoryName']==='45155316'}selected{/if} value='45155316'>45155316 Piezas de recambio puertas autom�ticas</option>
                <option {if $itemCategory['concatCategoryName']==='45159800'}selected{/if} value='45159800'>45159800 Componentes y cables para la t�cnica de ascensores sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45159900'}selected{/if} value='45159900'>45159900 Componentes y cables para la t�cnica de ascensores Otros</option>
                <option {if $itemCategory['concatCategoryName']==='45160202'}selected{/if} value='45160202'>45160202 Pasamanos de pl�stico</option>
                <option {if $itemCategory['concatCategoryName']==='45160203'}selected{/if} value='45160203'>45160203 Pasamanos de goma</option>
                <option {if $itemCategory['concatCategoryName']==='45160298'}selected{/if} value='45160298'>45160298 Pasamanos de escaleras mec�nicas/rampas</option>
                <option {if $itemCategory['concatCategoryName']==='45160398'}selected{/if} value='45160398'>45160398 Niveles y Palets sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45160698'}selected{/if} value='45160698'>45160698 Otros comp. Escalatera mecancia/rampas</option>
                <option {if $itemCategory['concatCategoryName']==='45241000'}selected{/if} value='45241000'>45241000 Kit de fijaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45249800'}selected{/if} value='45249800'>45249800 Elementos de uni�n sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45309800'}selected{/if} value='45309800'>45309800 Cojinetes sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45339800'}selected{/if} value='45339800'>45339800 Piezas torneadas y fresadas </option>
                <option {if $itemCategory['concatCategoryName']==='45420300'}selected{/if} value='45420300'>45420300 Chapas a medida</option>
                <option {if $itemCategory['concatCategoryName']==='45420700'}selected{/if} value='45420700'>45420700 Piezas de acero estampadas/curvadas </option>
                <option {if $itemCategory['concatCategoryName']==='45420900'}selected{/if} value='45420900'>45420900 Piezas de aluminio estampadas/curvadas</option>
                <option {if $itemCategory['concatCategoryName']==='45429800'}selected{/if} value='45429800'>45429800 Piezas estampadas y curvadas sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45459800'}selected{/if} value='45459800'>45459800 Recipientes sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45489800'}selected{/if} value='45489800'>45489800 Cilindros sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45510300'}selected{/if} value='45510300'>45510300 Rodillos revestidos</option>
                <option {if $itemCategory['concatCategoryName']==='45510400'}selected{/if} value='45510400'>45510400 Rodillos conductores de corriente</option>
                <option {if $itemCategory['concatCategoryName']==='45510500'}selected{/if} value='45510500'>45510500 Rodillos deflectores</option>
                <option {if $itemCategory['concatCategoryName']==='45510600'}selected{/if} value='45510600'>45510600 Rodillos de solera y horno</option>
                <option {if $itemCategory['concatCategoryName']==='45519800'}selected{/if} value='45519800'>45519800 Rodillos sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45549800'}selected{/if} value='45549800'>45549800 Valvuler�a industrial sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45579800'}selected{/if} value='45579800'>45579800 T�cnica de lubricaci�n/engrase</option>
                <option {if $itemCategory['concatCategoryName']==='45609800'}selected{/if} value='45609800'>45609800 Filtros, separadores sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45639800'}selected{/if} value='45639800'>45639800 Amortiguadores sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45660200'}selected{/if} value='45660200'>45660200 Piezas repuesto m�quinas-herramienta</option>
                <option {if $itemCategory['concatCategoryName']==='45660300'}selected{/if} value='45660300'>45660300 Repuestos mec�nicos de pl�stico</option>
                <option {if $itemCategory['concatCategoryName']==='45660400'}selected{/if} value='45660400'>45660400 Repuestos mec�nicos de metal no f�rreo</option>
                <option {if $itemCategory['concatCategoryName']==='45669800'}selected{/if} value='45669800'>45669800 Construcci�n especial piezas</option>
                <option {if $itemCategory['concatCategoryName']==='45690200'}selected{/if} value='45690200'>45690200 Cintas abrasivas</option>
                <option {if $itemCategory['concatCategoryName']==='45699800'}selected{/if} value='45699800'>45699800 Materiales abrasivos/pulido </option>
                <option {if $itemCategory['concatCategoryName']==='45720200'}selected{/if} value='45720200'>45720200 Herramientas de mano</option>
                <option {if $itemCategory['concatCategoryName']==='45720300'}selected{/if} value='45720300'>45720300 Herramientas el�ctricas</option>
                <option {if $itemCategory['concatCategoryName']==='45720400'}selected{/if} value='45720400'>45720400 Herramientas de aire comprimido</option>
                <option {if $itemCategory['concatCategoryName']==='45720500'}selected{/if} value='45720500'>45720500 Herramientas hidr�ulicas</option>
                <option {if $itemCategory['concatCategoryName']==='45720600'}selected{/if} value='45720600'>45720600 Instrumentos de medici�n</option>
                <option {if $itemCategory['concatCategoryName']==='45729800'}selected{/if} value='45729800'>45729800 Herramientas sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='45750298'}selected{/if} value='45750298'>45750298 Herramientas arranque virutas</option>
                <option {if $itemCategory['concatCategoryName']==='45759800'}selected{/if} value='45759800'>45759800 Herramientas mec�nicas </option>
                <option {if $itemCategory['concatCategoryName']==='50090998'}selected{/if} value='50090998'>50090998 Compresores sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='50091000'}selected{/if} value='50091000'>50091000 Grupos electr�genos de emergencia</option>
                <option {if $itemCategory['concatCategoryName']==='50099800'}selected{/if} value='50099800'>50099800 M�quinas especiales</option>
                <option {if $itemCategory['concatCategoryName']==='50129800'}selected{/if} value='50129800'>50129800 T�cnica transporte/desplazamiento/elevaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='50212100'}selected{/if} value='50212100'>50212100 Ascensor, accionado por cable</option>
                <option {if $itemCategory['concatCategoryName']==='50212300'}selected{/if} value='50212300'>50212300 Ascensor, hidr�ulico</option>
                <option {if $itemCategory['concatCategoryName']==='50212900'}selected{/if} value='50212900'>50212900 Montacargas, accionado por cable</option>
                <option {if $itemCategory['concatCategoryName']==='50213100'}selected{/if} value='50213100'>50213100 Montacargas, hidr�ulico</option>
                <option {if $itemCategory['concatCategoryName']==='50213300'}selected{/if} value='50213300'>50213300 Pasarelas telesc�picas</option>
                <option {if $itemCategory['concatCategoryName']==='50213500'}selected{/if} value='50213500'>50213500 Escaleras mec�nicas / andenes rodantes</option>
                <option {if $itemCategory['concatCategoryName']==='50213900'}selected{/if} value='50213900'>50213900 Ascensor para casas</option>
                <option {if $itemCategory['concatCategoryName']==='50214100'}selected{/if} value='50214100'>50214100 Plataformas salvaescaleras verticales</option>
                <option {if $itemCategory['concatCategoryName']==='50214500'}selected{/if} value='50214500'>50214500 Montacargas de servicio </option>
                <option {if $itemCategory['concatCategoryName']==='50215200'}selected{/if} value='50215200'>50215200 Ascensor autom�tico</option>
                <option {if $itemCategory['concatCategoryName']==='50249800'}selected{/if} value='50249800'>50249800 Calefacci�n / aire acondicionado / ventilaci�n </option>
                <option {if $itemCategory['concatCategoryName']==='50279800'}selected{/if} value='50279800'>50279800 T�cnica de seguridad/instalaciones control </option>
                <option {if $itemCategory['concatCategoryName']==='55039800'}selected{/if} value='55039800'>55039800 Inmuebles / terrenos sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='55069800'}selected{/if} value='55069800'>55069800 Ingenier�a civil </option>
                <option {if $itemCategory['concatCategoryName']==='55099800'}selected{/if} value='55099800'>55099800 Servicio almacenamiento </option>
                <option {if $itemCategory['concatCategoryName']==='55129800'}selected{/if} value='55129800'>55129800 Maquinarias/equipamientos</option>
                <option {if $itemCategory['concatCategoryName']==='55159800'}selected{/if} value='55159800'>55159800 Art�culos de oficinas sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='55189800'}selected{/if} value='55189800'>55189800 Protecci�n accidentes del trabajo/vestimenta</option>
                <option {if $itemCategory['concatCategoryName']==='55219800'}selected{/if} value='55219800'>55219800 Materiales servicio/consumo </option>
                <option {if $itemCategory['concatCategoryName']==='60279800'}selected{/if} value='60279800'>60279800 Transportes multimodales </option>
                <option {if $itemCategory['concatCategoryName']==='60330698'}selected{/if} value='60330698'>60330698 Flete terrestre pesados</option>
                <option {if $itemCategory['concatCategoryName']==='60360798'}selected{/if} value='60360798'>60360798 Flete mar�timo Contenedores </option>
                <option {if $itemCategory['concatCategoryName']==='60399800'}selected{/if} value='60399800'>60399800 Flete a�reo sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='60429800'}selected{/if} value='60429800'>60429800 Correo/servicios cadeteria</option>
                <option {if $itemCategory['concatCategoryName']==='60489800'}selected{/if} value='60489800'>60489800 Costes de almac�n sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='60549800'}selected{/if} value='60549800'>60549800 Seguro de transporte sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='60579800'}selected{/if} value='60579800'>60579800 Aduanas / tasas / impuestos</option>
                <option {if $itemCategory['concatCategoryName']==='62030300'}selected{/if} value='62030300'>62030300 Cajas de madera</option>
                <option {if $itemCategory['concatCategoryName']==='62069800'}selected{/if} value='62069800'>62069800 Cartones y papel sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='62099800'}selected{/if} value='62099800'>62099800 Material embalaje pl�stico </option>
                <option {if $itemCategory['concatCategoryName']==='62129800'}selected{/if} value='62129800'>62129800 Material embalaje  metalico</option>
                <option {if $itemCategory['concatCategoryName']==='62149800'}selected{/if} value='62149800'>62149800 R�tulos, etiquetas sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='62159800'}selected{/if} value='62159800'>62159800 Cinta adhesiva sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='66039800'}selected{/if} value='66039800'>66039800 Hardware (compra) sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='66069800'}selected{/if} value='66069800'>66069800 Hardware (alquiler y leasing) </option>
                <option {if $itemCategory['concatCategoryName']==='66099800'}selected{/if} value='66099800'>66099800 Software sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='66109800'}selected{/if} value='66109800'>66109800 Hardware mant/servicio/instalaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='66120298'}selected{/if} value='66120298'>66120298 Hardware telef�nico (compra) </option>
                <option {if $itemCategory['concatCategoryName']==='66120700'}selected{/if} value='66120700'>66120700 Terminales m�viles (compra)</option>
                <option {if $itemCategory['concatCategoryName']==='66120898'}selected{/if} value='66120898'>66120898 Servicios de telefon�a/red </option>
                <option {if $itemCategory['concatCategoryName']==='66129800'}selected{/if} value='66129800'>66129800 Comunicaci�n y red sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='66189800'}selected{/if} value='66189800'>66189800 Servicios de TI sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='70039800'}selected{/if} value='70039800'>70039800 Prestaciones construcci�n</option>
                <option {if $itemCategory['concatCategoryName']==='70060698'}selected{/if} value='70060698'>70060698 Montaje de ascensores/escaleras mec�nicas </option>
                <option {if $itemCategory['concatCategoryName']==='70069900'}selected{/if} value='70069900'>70069900 Prestaciones de montaje Otros</option>
                <option {if $itemCategory['concatCategoryName']==='70099800'}selected{/if} value='70099800'>70099800 Prestaciones de ingenier�a y construcci�n </option>
                <option {if $itemCategory['concatCategoryName']==='70109800'}selected{/if} value='70109800'>70109800 Pruebas, recepciones, ex�menes y tests </option>
                <option {if $itemCategory['concatCategoryName']==='70129800'}selected{/if} value='70129800'>70129800 Prestaciones de servicio produccion</option>
                <option {if $itemCategory['concatCategoryName']==='70159800'}selected{/if} value='70159800'>70159800 Mantenimiento y reparaci�n de m�quinas </option>
                <option {if $itemCategory['concatCategoryName']==='70189800'}selected{/if} value='70189800'>70189800 Eliminaci�n de residuos </option>
                <option {if $itemCategory['concatCategoryName']==='70219800'}selected{/if} value='70219800'>70219800 Limpieza sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='70249800'}selected{/if} value='70249800'>70249800 Prestaciones de servicios edificios y f�bricas</option>
                <option {if $itemCategory['concatCategoryName']==='70279800'}selected{/if} value='70279800'>70279800 Prestaciones de servicio de seguridad </option>
                <option {if $itemCategory['concatCategoryName']==='70309800'}selected{/if} value='70309800'>70309800 Alquiler de personal externo </option>
                <option {if $itemCategory['concatCategoryName']==='70339800'}selected{/if} value='70339800'>70339800 Catering sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='70360200'}selected{/if} value='70360200'>70360200 Alquiler de veh�culos</option>
                <option {if $itemCategory['concatCategoryName']==='70360400'}selected{/if} value='70360400'>70360400 Compra de veh�culos</option>
                <option {if $itemCategory['concatCategoryName']==='70360700'}selected{/if} value='70360700'>70360700 Reparaci�n de veh�culos</option>
                <option {if $itemCategory['concatCategoryName']==='70360800'}selected{/if} value='70360800'>70360800 Tarjetas para abastecimiento de combustible</option>
                <option {if $itemCategory['concatCategoryName']==='70369800'}selected{/if} value='70369800'>70369800 Gesti�n de parque de veh�culos </option>
                <option {if $itemCategory['concatCategoryName']==='70399800'}selected{/if} value='70399800'>70399800 Gesti�n de viajes sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='70429800'}selected{/if} value='70429800'>70429800 Instrucci�n / cursos</option>
                <option {if $itemCategory['concatCategoryName']==='70489800'}selected{/if} value='70489800'>70489800 Asistencia y asesoramiento </option>
                <option {if $itemCategory['concatCategoryName']==='70549800'}selected{/if} value='70549800'>70549800 Seguros sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='70579800'}selected{/if} value='70579800'>70579800 Servicio financiero sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='70709800'}selected{/if} value='70709800'>70709800 Prestaciones administrativas </option>
                <option {if $itemCategory['concatCategoryName']==='71039800'}selected{/if} value='71039800'>71039800 Medios publicitarios sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='71069800'}selected{/if} value='71069800'>71069800 Ferias sin diferenciaci�n</option>
                <option {if $itemCategory['concatCategoryName']==='71099800'}selected{/if} value='71099800'>71099800 Insumos de Marketing sin diferenciaci�n</option>
            </select>
        </div>
    </div>
</div>
