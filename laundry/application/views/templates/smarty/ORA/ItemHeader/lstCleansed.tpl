<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="navbar-nav mr-auto">
            <h5 class="nav p-2"><i class="fas fa-lg fa-industry fa-fw m-2 text-stage"></i>&nbsp;<span class="m-2"><span class="flag-icon flag-icon-{strtolower($country)}"></span> {strtoupper($country)} STG Items (cleansed)</span></h5>
            <ul class="nav nav-pills p-2" id="pills-tab" role="tablist">
            </ul>
            <ul class="navbar-nav mr-auto">
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="self" href="{base_url()}Inventory/dirty/{$country}" data-toggle="tooltip" data-placement="top" title="dirty Items"><i class="fas fa-trash-alt fa-sm fa-fw"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_self" href="{base_url()}RefreshFF/Inventory/{$country}" data-toggle="tooltip" data-placement="top" title="Refresh datasets"><i class="fas fa-redo fa-sm fa-fw"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_self" href="{base_url()}DownloadFF/Inventory/{$country}" data-toggle="tooltip" data-placement="top" title="Download current dataset"><i class="fas fa-file-archive fa-sm fa-fw"></i>&nbsp;ORA Zip</a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_self" href="{base_url()}RefreshFF/InventoryTrans/{$country}" data-toggle="tooltip" data-placement="top" title="Refresh Inventory Trans"><i class="fas fa-redo fa-sm fa-fw"></i>&nbsp;Trans</a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_self" href="{base_url()}DownloadFF/InventoryTrans/{$country}" data-toggle="tooltip" data-placement="top" title="Download Inventory transactions"><i class="fas fa-file-archive fa-sm fa-fw"></i>&nbsp;Trans Zip</a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_self" href="{base_url()}Gallery/download" data-toggle="tooltip" data-placement="top" title="current Inventory gallery"><i class="fas fa-file-archive fa-sm fa-fw"></i> Gallery</a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-danger" target="_self" href="{base_url()}ManageFF/InventoryDiagnostics/{$country}" data-toggle="tooltip" data-placement="top" title="diagnostics"><i class="fas fa-ambulance fa-sm fa-fw"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-success" target="_blank" href="{base_url()}IDL/Navigate/navigateGroup/{$country}/Inventory" data-toggle="tooltip" data-placement="top" title="specification for Inventory group"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-success" target="_blank" href="{base_url()}scripts/00.COM/master/03.Items/01.IDL/TKE_LA_IDL_CD_01_INVENTORY_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="latest specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                </li>
            </ul>
        </div>
        <div class="dropdown">
            <button class="btn btn-success  dropleft dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-cloud-download-alt fa-sm fa-fw"></i> Inspect {$country} Datasets
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="{base_url()}DownloadFF/Inventory/{$country}"><i class="fas fa-file-archive fa-sm fa-fw m-1"></i> Download ORA ZIP</a>
                <a class="dropdown-item" href="{base_url()}InspectFF/Inventory/{$country}/TKE_LA_CD_01_ITEM_HEADER" target="_blank"><i class="fas fa-file-archive fa-sm fa-fw m-1"></i> Item Header</a>
                <a class="dropdown-item" href="{base_url()}InspectFF/Inventory/{$country}/TKE_LA_CD_01_ITEM_CATEGORY" target="_blank"><i class="fas fa-file-archive fa-sm fa-fw m-1"></i> Item Category</a>
                <a class="dropdown-item" href="{base_url()}InspectFF/Inventory/{$country}/TKE_LA_CD_01_ITEM_CROSS_REF" target="_blank"><i class="fas fa-file-archive fa-sm fa-fw m-1"></i> Item CrossRefence</a>
                <a class="dropdown-item" href="{base_url()}InspectFF/Inventory/{$country}/TKE_LA_CD_01_ITEM_MFG_PART_NUM" target="_blank"><i class="fas fa-file-archive fa-sm fa-fw m-1"></i> Item Mfg Part Number</a>
                <a class="dropdown-item" href="{base_url()}InspectFF/Inventory/{$country}/TKE_LA_CD_01_ITEM_TRANS_DEF_SUB_INV" target="_blank"><i class="fas fa-file-archive fa-sm fa-fw m-1"></i> Item Def Sub Inv</a>
                <a class="dropdown-item" href="{base_url()}InspectFF/Inventory/{$country}/TKE_LA_CD_01_ITEM_TRANS_DEF_LOC" target="_blank"><i class="fas fa-file-archive fa-sm fa-fw m-1"></i> Item Def Locators</a>
            </div>
        </div>
        <!--
        <div class="dropdown m-2">
            <button class="btn btn-success  dropleft dropdown-toggle mr-4" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Custom Reports <i class="fas fa-ruler fa-sm fa-fw ml-2 mr-2"></i>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="{base_url()}NamedQueries/InventoryAudits/{$country}"><i class="fas fa-file-archive fa-sm fa-fw m-1"></i>Audit Reports</a>
                <a class="dropdown-item" href="{base_url()}NamedQueries/InventorySCCFamilies/{$country}"><i class="fas fa-file-archive fa-sm fa-fw m-1"></i>SCC Families</a>
                <a class="dropdown-item" href="{base_url()}NamedQueries/InventoryAssociations/{$country}"><i class="fas fa-file-archive fa-sm fa-fw m-1"></i>Associations</a>
            </div>
        </div>
        -->
    </div>
</nav>
<br/>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade active show" id="pills-data" role="tabpanel" aria-labelledby="pills-home-tab">
        <ul class="pagination">
            <li>
                <small class="text-muted m-4"> by {$sortOrder}&nbsp;{$sortDirection}</small>
            </li>
            {if $isPaginationNeeded}
                <li class="page-item {if $currentPage==0}active{/if}">
                    <a class="page-link" href="{base_url()}{$listAction}0/{$sessionSelCol}/{if $sessionSelAsc}1{else}0{/if}" aria-label="First">
                        First
                    </a>
                </li>
                {for $foo=1 to ($lastPage-1)}
                    <li class="page-item {if $currentPage==$foo}active{/if}"><a class="page-link" href="{base_url()}{$listAction}{$foo}/{$sessionSelCol}/{if $sessionSelAsc}1{else}0{/if}">{$foo}</a></li>
                {/for}
                <li class="page-item {if $currentPage==$lastPage}active{/if}">
                    <a class="page-link" href="{base_url()}{$listAction}{$lastPage}/{$sessionSelCol}/{if $sessionSelAsc}1{else}0{/if}" aria-label="Last">
                        Last
                    </a>
                </li>
            {/if}
        </ul>
        <br/>
        <table class="table table-hover table-sm">
            <thead>
            <tr>
                <th style="width:50px;"></th>
                <th>#</th>
                <th>OrigItemNumber</th>
                <th>Item Number</th>
                <th>OBS (CODIGO INTERNO)</th>
                <th>Description (ESA)</th>
                <th>Descritpion</th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$cleansedVOs item=p}
                <tr class="{if ($p.dismissed)} dismissed {elseif (($p.locallyValidated)&&($p.crossRefValidated)) } alert-success {else} alert-warning {/if} clickable-row" style="cursor:pointer" data-href='{base_url()}Inventory/customEditCleansed/{$p.country}/{$p.id}'>
                    <td>
                        {if $p.hasPicture}<a href="{base_url()}Inventory/customEditCleansed/{$p.country}/{$p.id}" data-toggle="tooltip" data-placement="top" title="item {$p.origItemNumber} - {$p.itemNumber} has picture"><i class="{if ($p.dismissed)} dismissed {elseif (($p.locallyValidated)&&($p.crossRefValidated)) } text-success {else} text-warning {/if} fas fa-camera fa-sm fa-fw"></i></a>{/if}
                        {if !isset($p.mergedId)}{if !$p.childless}<i class="fas {if ($p.dismissed)} dismissed {elseif (($p.locallyValidated)&&($p.crossRefValidated)) } text-success {else} text-warning {/if} fa-sm fa-user-tie fa-fw text-stage" data-toggle="tooltip" data-placement="top" title="item {$p.origItemNumber} - {$p.itemNumber} is master"></i>{/if}{else}<i class="fas {if ($p.dismissed)} dismissed {elseif (($p.locallyValidated)&&($p.crossRefValidated)) } text-success {else} text-warning {/if} fa-sm fa-user-tag fa-fw text-stage" data-toggle="tooltip" data-placement="top" title="item {$p.origItemNumber} - {$p.itemNumber} is child"></i>{/if}
                    </td>
                    <td scope="row"><a href="{base_url()}Inventory/customEditCleansed/{$p.country}/{$p.id}" data-toggle="tooltip" data-placement="top" title="Item {$p.itemNumber} last updated at {$p.lastUpdated|date_format:"%d-%m-%Y %H:%M:%S"}" class="text-TKE"><small>{$p.id}</small></a></td>
                    <td >{$p.origItemNumber}</td>
                    <td >{$p.itemNumber}</td>
                    <td >{$p.observations|truncate:40}</td>
                    <td >{$p.descriptionEsa|truncate:60:' ... ':true:true}</td>
                    <td >{$p.description|truncate:20}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
        <br/>
    </div>
    <div class="tab-pane fade show" id="pills-stats" role="tabpanel" aria-labelledby="pills-stats-tab">
        <br/>
        <div class="row p-1">
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Locally Validated</h5>
                        <h6 class="card-subtitle mb-2 text-muted">matches all criteria for local validation</h6>
                        <h1 class="card-text">{$cleansedLocallyValidated}</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">CrossRef Validated</h5>
                        <h6 class="card-subtitle mb-2 text-muted">has no dependencies on other Groups</h6>
                        <h1 class="card-text">{$cleansedCrossRefValidated}</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Validated for Output</h5>
                        <h6 class="card-subtitle mb-2 text-muted">matches all criteria</h6>
                        <h1 class="card-text">{$cleansedValidatedForOutput}</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="row p-1">
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Cleansed</h5>
                        <h6 class="card-subtitle mb-2 text-muted">moved from pre-staging table to stage</h6>
                        <h1 class="card-text">{$total}</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Recommended</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Recommended records according to fixed functional criteria</h6>
                        <h1 class="card-text">{$recommendedCleansed}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<br/>
<br/>
<br/>
<br/>