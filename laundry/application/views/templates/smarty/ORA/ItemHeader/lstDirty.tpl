<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="navbar-nav mr-auto">
            <h5 class="nav p-2"><i class="fas fa-lg fa-trash-alt fa-fw m-2 text-pre-stage"></i>&nbsp;<span class="m-2"><span class="flag-icon flag-icon-{strtolower($country)}"></span> {strtoupper($country)} pre-STG Items (dirty)</span></h5>
            <ul class="nav nav-pills p-2" id="pills-tab" role="tablist">
            </ul>
            <ul class="navbar-nav mr-auto">
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_blank" href="{base_url()}Inventory/cleansed/{$country}/0/{$sessionSelCol}/{if $sessionSelAsc}1{else}0{/if}" data-toggle="tooltip" data-placement="top" title="cleansed Items"><i class="fas fa-industry fa-sm fa-fw"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-success" target="_blank" href="{base_url()}IDL/Navigate/navigateGroup/{$country}/Inventory" data-toggle="tooltip" data-placement="top" title="specification for Inventory group"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
                </li>

            </ul>
        </div>
        &nbsp;
    </div>
</nav>
<br/>
{if $isPaginationNeeded}
    <ul class="pagination">
        <li class="page-item {if $currentPage==0}active{/if}">
            <a class="page-link" href="{base_url()}{$listAction}0" aria-label="First">
                First
            </a>
        </li>
        {for $foo=1 to ($lastPage-1)}
            <li class="page-item {if $currentPage==$foo}active{/if}"><a class="page-link" href="{base_url()}{$listAction}{$foo}">{$foo}</a></li>
        {/for}
        <li class="page-item {if $currentPage==$lastPage}active{/if}">
            <a class="page-link" href="{base_url()}{$listAction}{$lastPage}" aria-label="Last">
                Last
            </a>
        </li>
    </ul>
{/if}
<br/>
<table class="table table-hover table-sm">
    <thead>
    <tr>
        <th>ranking</th>
        <th>OrigItemNumber</th>
        <th>Item Number</th>
        <th>OBS (CODIGO INTERNO)</th>
        <th>Description (ESA)</th>
        <th>Description</th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$dirtyVOs item=p}
        <tr class="{if ($p.recommended) } alert-success {else} alert-warning {/if} clickable-row" style="cursor:pointer" data-href='{base_url()}Inventory/editDirty/{$p.id}' title="inspect all fields from dirty Vendor {$p.description}">
            <td >{$p.ranking}</td>
            <td >{$p.origItemNumber}</td>
            <td >{$p.itemNumber}</td>
            <td >{$p.observations|truncate:80}</td>
            <td >{$p.descriptionEsa|truncate:80}</td>
            <td >{$p.description|truncate:20}</td>
        </tr>
    {/foreach}
    </tbody>
</table>
<br/>
<br/>
