<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2">{if !isset($mergedId)}{if $childless}<i class="fas fa-lg fa-user fa-fw m-2 text-stage"></i>{else}<i class="fas fa-lg fa-user-tie fa-fw m-2 text-stage"></i>{/if}{else}<i class="fas fa-lg fa-user-tag fa-fw m-2 text-stage"></i>{/if}&nbsp;<span class="m-2"><span class="flag-icon flag-icon-{strtolower($country)}"></span> {strtoupper($country)} {$title}</span></h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Inventory/editCleansed/{$id}" data-toggle="tooltip" data-placement="top" title="view ALL fields of item {$origItemNumber}"><i class="fas fa-edit fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Inventory/customEditCleansed/{$country}/{$id}" data-toggle="tooltip" data-placement="top" title="quick view of item {$origItemNumber} (NEW TAB)"><i class="fas fa-eye fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Inventory/validate/{$id}" data-toggle="tooltip" data-placement="top" title="trigger validation of item {$origItemNumber}"><i class="fas fa-check-circle fa-sm fa-fw"></i></a>
            </li>
            {if !isset($mergedId) && $childless}
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}ItemCrossRef/searchAssociationCleansedToCleansed/{$id}" data-toggle="tooltip" data-placement="top" title="search master association for item {$origItemNumber}"><i class="fas fa-link fa-sm fa-fw"></i></a>
            </li>
            {/if}
            {if !$sameCountryParent && $childless}
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-secondary" target="_self" href="{base_url()}Inventory/toggle/{$id}" data-toggle="tooltip" data-placement="top" title="{if ($dismissed)}Recall{else}Dismiss{/if} Item {$origItemNumber}"><i class="fas fa-minus-circle fa-sm fa-fw"></i></a>
            </li>
            {/if}
            {if $childless}
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-danger" target="_self" href="{base_url()}Inventory/discard/{$id}" data-toggle="modal" data-target="#discardModal" title="discard item {$origItemNumber} - completely remove from stage"><i class="fas fa-eraser fa-sm fa-fw"></i></a>
            </li>
            {/if}
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Inventory/uploadResource/{$id}" data-toggle="tooltip" data-placement="top" title="associate a file to {$itemNumber}"><i class="fas fa-file-upload fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-success" target="_blank" href="{base_url()}IDL/Navigate/navigate/{$country}/Inventory/ItemHeader" data-toggle="tooltip" data-placement="top" title="specification for {$country} item header"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-success" target="_blank" href="{base_url()}scripts/00.COM/master/03.Items/01.IDL/TKE_LA_IDL_CD_01_INVENTORY_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="latest specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Inventory/cleansed/{$country}/0/{$sessionSelCol}/{if $sessionSelAsc}1{else}0{/if}" data-toggle="tooltip" data-placement="top" title="cleansed Items for {$country}"><i class="fas fa-industry fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Inventory/dirty/{$country}" data-toggle="tooltip" data-placement="top" title="dirty Items for {$country}"><i class="fas fa-trash-alt fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-secondary" target="_self" href="{base_url()}{$cancelAction}" data-toggle="tooltip" data-placement="top" title="go back to cleansed items"><i class="fas fa-ban fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>
<div id="discardModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-danger" id="exampleModalLongTitle"><i class="fas fa-eraser fa-sm fa-fw"></i> Confirm Discard</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"/>
            </div>
            <div class="modal-body text-center p-2">
{if $childless}
                    <p class="alert-danger">You're about to discard Item {$origItemNumber}.</p>
                    <p><b class="alert-danger">This cannot be undone - No se puede deshacer esta acci�n</b></p>
{else}
                    <p class="alert-danger">Only childless items can be discarded</p>
                    <p><b class="alert-danger">Remove associations before trying to discard</b></p>
                    <p><b class="alert-danger">Tente remover as associa��es antes de tentar um descarte</b></p>
{/if}
            </div>
            <div class="modal-footer">
{if $childless}
                <a href="{base_url()}{$dirtyAction}" class="btn btn-danger"><i class="fas fa-eraser fa-sm fa-fw"></i> Yes, remove this item from the stage</a>
{/if}
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel <i class="fas fa-ban fa-sm fa-fw"></i></button>
            </div>
        </div>
    </div>
</div>

<div id="dismissModalLong" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="dismissModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="dismissModalLongLongTitle">Please Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"/>
            </div>
            <div class="modal-body">
                <p>
                    Please write down the reasons for {if ($dismissed)}recalling{else}dismissing{/if} this Vendor, please use the Observations.<br/>
                    This will automatically {if ($dismissed)}recall{else}dismiss{/if} all associated VendorSites, VendorSiteContacts, and VendorBankAccounts.<br/>
                    Are you sure?
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No, forget about it.</button>
                <a href="{base_url()}Vendors/toggle/{$id}" class="btn btn-danger">Yes, {if ($dismissed)}recall{else}dismiss{/if} this row</a>
            </div>
        </div>
    </div>
</div>
{if ($dismissed)}
<div class="row">
    <div class="col-sm-12 p-2">
        <p class="dismissed text-center p-2">dismissed row, not going to flat file</p>
    </div>
</div>
{else}
<div class="row">
    <div class="col-sm-4 p-2">
        <p class="alert-success text-center p-2">active row</p>
    </div>
    <div class="col-sm-4 p-2">
{if (isset($cleansingAction)&&($cleansingAction!=''))}
        <p class="alert-warning text-center p-2">{$cleansingAction}</p>
{else}
        <p class="alert-success text-center p-2">locally validated - no action required at header level</p>
{/if}
    </div>
    <div class="col-sm-4 p-2">
        {if ($crossRefValidated && $locallyValidated)}
            <p class="alert-success text-center p-2">no problems, going to flat file</p>
        {elseif ($locallyValidated)}
            <p class="alert-warning text-center p-2">problems in dependencies - check ItemMFGPartNum, ItemCrossRef, or ItemTransDefLoc</p>
        {/if}
    </div>
</div>
{/if}


<div class="row mb-2">
    <div class="col-sm-12 p-2">
        {if ($mergedId!=null)}
            <div class="alert-success text-center p-4">
                <p>Merged (child) to <span class="flag-icon flag-icon-{strtolower($parent['country'])}" style="margin-top:10px;"></span> {$parent['origItemNumber']}  - {$parent['itemNumber']}</p>
                <a class="btn text-light {if (($parent.locallyValidated)&&($parent.crossRefValidated)) }btn-success{else}btn-warning{/if}" target="_blank" href="{base_url()}Inventory/customEditCleansed/{$parent.country}/{$parent.id}" data-toggle="tooltip" data-placement="top" title="quick view of Item {$parent.itemNumber}"><i class="fas fa-link fa-sm fa-fw"></i></a>
            </div>
        {/if}
        {if !($childless)}
            {foreach from=$mergedChildren item=mi name=mi}
                <div class="alert-success text-center p-4">
                    <p>Master (parent) of <span class="flag-icon flag-icon-{strtolower($mi['country'])}" style="margin-top:10px;"></span> {$mi['origItemNumber']} - {$mi['itemNumber']}</p>
                    <a class="btn text-light {if $mi.dismissed}dismissed{elseif (($mi.locallyValidated)&&($mi.crossRefValidated)) }btn-success{else}btn-warning{/if}" target="_blank" href="{base_url()}Inventory/customEditCleansed/{$mi.country}/{$mi.id}" data-toggle="tooltip" data-placement="top" title="quick view of Item {$mi.origItemNumber} - {$mi.itemNumber}"><i class="fas fa-link fa-sm fa-fw"></i></a>
                </div>
            {/foreach}
        {/if}
    </div>
</div>

