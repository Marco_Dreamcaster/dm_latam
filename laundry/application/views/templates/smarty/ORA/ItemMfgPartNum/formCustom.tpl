<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorOrigItemNumber" class="form-text text-danger">&nbsp;{$errorOrigItemNumber}</small>
            <label class="sr-only" for="inlineFormInputOrigItemNumber">Orig Item Number</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Orig Item Number</div>
                </div>
                <input type="text" class="form-control" id="origItemNumber" name="origItemNumber" value="{$origItemNumber}" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorItemNumber" class="form-text text-danger">&nbsp;{$errorItemNumber}</small>
            <label class="sr-only" for="inlineFormInputItemNumber">Item Number</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Item Number</div>
                </div>
                <input type="text" class="form-control" id="itemNumber" name="itemNumber" value="{$itemNumber}" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorTemplateName" class="form-text text-danger">&nbsp;{$errorManufacturer}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Template Name</div>
                </div>
                <select class="form-control" id="manufacturer" name="manufacturer">
                    <option {if $manufacturer==='01'}selected{/if} value='01'>01 - TKE USA</option>
                    <option {if $manufacturer==='02'}selected{/if} value='02'>02 - TKE NORTE</option>
                    <option {if $manufacturer==='03'}selected{/if} value='03'>03 - TKE MANUFACTURING SPAIN</option>
                    <option {if $manufacturer==='04'}selected{/if} value='04'>04 - TKE ALEMANIA</option>
                    <option {if $manufacturer==='05'}selected{/if} value='05'>05 - TKE CHINA</option>
                    <option {if $manufacturer==='06'}selected{/if} value='06'>06 - TKE ACCESS</option>
                    <option {if $manufacturer==='07'}selected{/if} value='07'>07 - TKE KOREA</option>
                    <option {if $manufacturer==='08'}selected{/if} value='08'>08 - TKE AIRPORT SERVICES ESPA�A</option>
                    <option {if $manufacturer==='09'}selected{/if} value='09'>09 - TKE CANADA</option>
                    <option {if $manufacturer==='10'}selected{/if} value='10'>10 - OTIS</option>
                    <option {if $manufacturer==='11'}selected{/if} value='11'>11 - ATLAS-SCHINDLER</option>
                    <option {if $manufacturer==='12'}selected{/if} value='12'>12 - KONE</option>
                    <option {if $manufacturer==='13'}selected{/if} value='13'>13 - WITTUR</option>
                    <option {if $manufacturer==='14'}selected{/if} value='14'>14 - FERMATOR</option>
                    <option {if $manufacturer==='14'}selected{/if} value='14'>15 - TKE AIRPORT SERVICES CHINA</option>
                    <option {if $manufacturer==='98'}selected{/if} value='98'>98 - TKE BRASIL</option>
                    <option {if $manufacturer==='99'}selected{/if} value='99'>99 - OTROS FABRICANTES/MARCAS</option>
                    <option {if $manufacturer==='00'}selected{/if} value='00'>00 - SERVICIOS</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorPartNumber" class="form-text text-danger">&nbsp;{$errorPartNumber}</small>
            <label class="sr-only" for="inlineFormInputPartNumber">Part Number</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Part Number</div>
                </div>
                <input type="text" class="form-control" id="partNumber" name="partNumber" value="{$partNumber}" placeholder="Part Number">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorDescription" class="form-text text-danger">&nbsp;{$errorDescription}</small>
            <label class="sr-only" for="inlineFormInputDescription">Description</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Description</div>
                </div>
                <input type="text" class="form-control" id="description" name="description" value="{$description}" placeholder="Description">
            </div>
        </div>
    </div>
    <br/>
    <br/>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel} <i class="fas fa-check fa-sm fa-fw"></i></button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel <i class="fas fa-ban fa-sm fa-fw"></i></a>
    </div>

</form>
<br/>
<br/>
<br/>
