<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><span class="flag-icon flag-icon-{strtolower($country)}" style="margin-top:10px;"></span>&nbsp;<span class="mt-2">{strtoupper($country)} {$title}</span>&nbsp;</h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item p-1">
                <a class="btn btn-secondary" href="{base_url()}{$cancelAction}" title="cleansed Items for {$country}"><i class="fas fa-ban fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>
<div class="row mt-2">
    <div class="col-sm-6">
        {if ($dismissed)}
            <p class="dismissed text-center">dismissed row</p>
        {else}
            <p class="alert-success text-center">active row</p>
        {/if}
    </div>
    <div class="col-sm-6">
        {if (isset($cleansingAction)&&($cleansingAction!=''))}
            <p class="alert-warning text-center">{$cleansingAction}</p>
        {else}
            <p class="alert-success text-center">validated - no action required</p>
        {/if}
    </div>
</div>


