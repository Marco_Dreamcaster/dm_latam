<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorOrigItemNumber" class="form-text text-danger">&nbsp;{$errorOrigItemNumber}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Orig Item Number</div>
                </div>
                <input type="text" class="form-control" id="origItemNumber" name="origItemNumber" value="{$origItemNumber}" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorItemNumber" class="form-text text-danger">&nbsp;{$errorItemNumber}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Item Number</div>
                </div>
                <input type="text" class="form-control" id="itemNumber" name="itemNumber" value="{$itemNumber}" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorOrganizationCode" class="form-text text-danger">&nbsp;{$errorOrganizationCode}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Organization Code</div>
                </div>
                <input type="text" class="form-control" id="organizationCode" name="organizationCode" value="{$organizationCode}" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorSubinventoryName" class="form-text text-danger">&nbsp;{$errorSubinventoryName}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Subinventory Name</div>
                </div>
                <input type="text" class="form-control" id="subinventoryName" name="subinventoryName" value="{$subinventoryName}" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorSubinventoryName" class="form-text text-danger">&nbsp;{$errorLocatorName}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Locator Name</div>
                </div>
                <input type="text" class="form-control" id="locatorName" name="locatorName" value="{$locatorName}" placeholder="Locator Name">
            </div>
        </div>
    </div>
    <br/>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>
</form>