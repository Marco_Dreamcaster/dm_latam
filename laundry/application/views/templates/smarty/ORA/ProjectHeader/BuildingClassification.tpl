<div class="col-sm-4 my-1">
    <small id="errorBuildingClassification" class="form-text text-danger">&nbsp;{$errorBuildingClassification}</small>
    <div class="input-group">
        <div class="input-group-prepend">
            <div class="input-group-text">Building Classification</div>
        </div>
        <select class="form-control" id="buildingClassification" name="buildingClassification" aria-describedby="buildingClassificationHelp">
            <option {if $buildingClassification==='AIRPORT'}selected{/if}>AIRPORT</option>
            <option {if $buildingClassification==='AIRPORT/TERMINAL/TRANSPORT'}selected{/if}>AIRPORT/TERMINAL/TRANSPORT</option>
            <option {if $buildingClassification==='COMMERCIAL OFFICE - HIGH RISE'}selected{/if}>COMMERCIAL OFFICE - HIGH RISE</option>
            <option {if $buildingClassification==='COMMERCIAL OFFICE - LOW RISE'}selected{/if}>COMMERCIAL OFFICE - LOW RISE</option>
            <option {if $buildingClassification==='COMMERCIAL OFFICE - MID RISE'}selected{/if}>COMMERCIAL OFFICE - MID RISE</option>
            <option {if $buildingClassification==='COMMERCIAL/RESIDENTIAL MIX'}selected{/if}>COMMERCIAL/RESIDENTIAL MIX</option>
            <option {if $buildingClassification==='CONDOMINIUM - HIGH RISE'}selected{/if}>CONDOMINIUM - HIGH RISE</option>
            <option {if $buildingClassification==='CONDOMINIUM - LOW RISE'}selected{/if}>CONDOMINIUM - LOW RISE</option>
            <option {if $buildingClassification==='CONDOMINIUM - MID RISE'}selected{/if}>CONDOMINIUM - MID RISE</option>
            <option {if $buildingClassification==='EDUCATION / RELIGION'}selected{/if}>EDUCATION / RELIGION</option>
            <option {if $buildingClassification==='ENTERTAINMENT / LEISURE'}selected{/if}>ENTERTAINMENT / LEISURE</option>
            <option {if $buildingClassification==='FERRY TERMINAL'}selected{/if}>FERRY TERMINAL</option>
            <option {if $buildingClassification==='GOVERNMENT-MILITARY'}selected{/if}>GOVERNMENT-MILITARY</option>
            <option {if $buildingClassification==='GOVERNMENT-OTHER/RELIGIOUS'}selected{/if}>GOVERNMENT-OTHER/RELIGIOUS</option>
            <option {if $buildingClassification==='GOVERNMENT-TRANSPORTATION'}selected{/if}>GOVERNMENT-TRANSPORTATION</option>
            <option {if $buildingClassification==='HOSPITAL / HEALTHCARE'}selected{/if}>HOSPITAL / HEALTHCARE</option>
            <option {if $buildingClassification==='HOSPITAL/MEDICAL FACILITY'}selected{/if}>HOSPITAL/MEDICAL FACILITY</option>
            <option {if $buildingClassification==='HOTEL'}selected{/if}>HOTEL</option>
            <option {if $buildingClassification==='HOTEL / RESTAURANT'}selected{/if}>HOTEL / RESTAURANT</option>
            <option {if $buildingClassification==='INDUSTRIAL'}selected{/if}>INDUSTRIAL</option>
            <option {if $buildingClassification==='INDUSTRIAL - OTHER'}selected{/if}>INDUSTRIAL - OTHER</option>
            <option {if $buildingClassification==='INDUSTRIAL - PETRO/CHEMICAL'}selected{/if}>INDUSTRIAL - PETRO/CHEMICAL</option>
            <option {if $buildingClassification==='MARINE'}selected{/if}>MARINE</option>
            <option {if $buildingClassification==='METRO / SUBWAY'}selected{/if}>METRO / SUBWAY</option>
            <option {if $buildingClassification==='MIXED USE BUILDINGS'}selected{/if}>MIXED USE BUILDINGS</option>
            <option {if $buildingClassification==='OFFICE'}selected{/if}>OFFICE</option>
            <option {if $buildingClassification==='OTHER'}selected{/if}>OTHER</option>
            <option {if $buildingClassification==='PARKING GARAGE'}selected{/if}>PARKING GARAGE</option>
            <option {if $buildingClassification==='PRIVATE HOMES'}selected{/if}>PRIVATE HOMES</option>
            <option {if $buildingClassification==='PRIVATE RESIDENTIAL - COMFORT'}selected{/if}>PRIVATE RESIDENTIAL - COMFORT</option>
            <option {if $buildingClassification==='PRIVATE RESIDENTIAL - PREMIUM'}selected{/if}>PRIVATE RESIDENTIAL - PREMIUM</option>
            <option {if $buildingClassification==='PRIVATE RESIDENTIAL-FUNCTIONAL'}selected{/if}>PRIVATE RESIDENTIAL-FUNCTIONAL</option>
            <option {if $buildingClassification==='PUBLIC HOUSING AUTHORITY'}selected{/if}>PUBLIC HOUSING AUTHORITY</option>
            <option {if $buildingClassification==='PUBLIC/SEMI-PUBLIC RESIDENTIAL'}selected{/if}>PUBLIC/SEMI-PUBLIC RESIDENTIAL</option>
            <option {if $buildingClassification==='RAILWAY / BUS STATION'}selected{/if}>RAILWAY / BUS STATION</option>
            <option {if $buildingClassification==='RENTAL APARTMENT'}selected{/if}>RENTAL APARTMENT</option>
            <option {if $buildingClassification==='RETAIL'}selected{/if}>RETAIL</option>
            <option {if $buildingClassification==='RETIREMENT AND CARE HOUSING'}selected{/if}>RETIREMENT AND CARE HOUSING</option>
            <option {if $buildingClassification==='SCHOOLS/UNIVERSITIES'}selected{/if}>SCHOOLS/UNIVERSITIES</option>
            <option {if $buildingClassification==='SECURITY/COURT'}selected{/if}>SECURITY/COURT</option>
            <option {if $buildingClassification==='SHIPS/PLANES/TRAINS'}selected{/if}>SHIPS/PLANES/TRAINS</option>
            <option {if $buildingClassification==='SPECIAL EVENTS STADIUM/ARENA'}selected{/if}>SPECIAL EVENTS STADIUM/ARENA</option>
            <option {if $buildingClassification==='URBAN MOBILITY'}selected{/if}>URBAN MOBILITY</option>
        </select>
    </div>
</div>