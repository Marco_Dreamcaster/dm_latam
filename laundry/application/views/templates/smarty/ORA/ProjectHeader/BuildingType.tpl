<div class="col-sm-4 my-1">
    <small id="errorBuildingType" class="form-text text-danger">&nbsp;{$errorBuildingType}</small>
    <div class="input-group">
        <div class="input-group-prepend">
            <div class="input-group-text">Building Type </div>
        </div>
        <select class="form-control" id="buildingType" name="buildingType" aria-describedby="buildingTypeHelp">
            <option {if $buildingType===''}selected{/if}} value="">--</option>
            <option {if $buildingType==='4+ STAR HOTEL'}selected{/if}>4+ STAR HOTEL</option>
            <option {if $buildingType==='BUDGET HOTEL / MOTEL / HOSTEL'}selected{/if}>BUDGET HOTEL / MOTEL / HOSTEL</option>
            <option {if $buildingType==='BUSINESS OFFICE - HIGH/PREMIUM'}selected{/if}>BUSINESS OFFICE - HIGH/PREMIUM</option>
            <option {if $buildingType==='BUSINESS OFFICE-LOW/FUNCTIONAL'}selected{/if}>BUSINESS OFFICE-LOW/FUNCTIONAL</option>
            <option {if $buildingType==='BUSINESS OFFICE-MEDIUM/COMFORT'}selected{/if}>BUSINESS OFFICE-MEDIUM/COMFORT</option>
            <option {if $buildingType==='CARGO SHIP'}selected{/if}>CARGO SHIP</option>
            <option {if $buildingType==='CASINO'}selected{/if}>CASINO</option>
            <option {if $buildingType==='CINEMA COMPLEX'}selected{/if}>CINEMA COMPLEX</option>
            <option {if $buildingType==='CONVENTION / EXHIBITION CENTER'}selected{/if}>CONVENTION / EXHIBITION CENTER</option>
            <option {if $buildingType==='CRUISE LINER'}selected{/if}>CRUISE LINER</option>
            <option {if $buildingType==='CULTURAL CENTER'}selected{/if}>CULTURAL CENTER</option>
            <option {if $buildingType==='DAM'}selected{/if}>DAM</option>
            <option {if $buildingType==='FERRY'}selected{/if}>FERRY</option>
            <option {if $buildingType==='FUNCTIONAL DEPARTMENT STORE'}selected{/if}>FUNCTIONAL DEPARTMENT STORE</option>
            <option {if $buildingType==='FUNCTIONAL SHOPPING MALL'}selected{/if}>FUNCTIONAL SHOPPING MALL</option>
            <option {if $buildingType==='GOVERNMENT / PUBLIC OFFICE'}selected{/if}>GOVERNMENT / PUBLIC OFFICE</option>
            <option {if $buildingType==='IND. PARK / FACTORY / PLANT'}selected{/if}>IND. PARK / FACTORY / PLANT</option>
            <option {if $buildingType==='MED. CENTER / OFFICE / CLINIC'}selected{/if}>MED. CENTER / OFFICE / CLINIC</option>
            <option {if $buildingType==='MID-RANGE HOTEL'}selected{/if}>MID-RANGE HOTEL</option>
            <option {if $buildingType==='MINING FACILITY'}selected{/if}>MINING FACILITY</option>
            <option {if $buildingType==='MIXED USE - FUNCTIONAL'}selected{/if}>MIXED USE - FUNCTIONAL</option>
            <option {if $buildingType==='MIXED USE - PREMIUM'}selected{/if}>MIXED USE - PREMIUM</option>
            <option {if $buildingType==='MUSEUM'}selected{/if}>MUSEUM</option>
            <option {if $buildingType==='OIL / GAS FACILITY'}selected{/if}>OIL / GAS FACILITY</option>
            <option {if $buildingType==='OIL PLATFORM'}selected{/if}>OIL PLATFORM</option>
            <option {if $buildingType==='OWNED / RENTED APARTMENTS'}selected{/if}>OWNED / RENTED APARTMENTS</option>
            <option {if $buildingType==='PORT'}selected{/if}>PORT</option>
            <option {if $buildingType==='POWER STATION'}selected{/if}>POWER STATION</option>
            <option {if $buildingType==='PREMIUM DEPARTMENT STORE'}selected{/if}>PREMIUM DEPARTMENT STORE</option>
            <option {if $buildingType==='PREMIUM SHOPPING MALL'}selected{/if}>PREMIUM SHOPPING MALL</option>
            <option {if $buildingType==='PRIVATE HOSPITAL - FUNCTIONAL'}selected{/if}>PRIVATE HOSPITAL - FUNCTIONAL</option>
            <option {if $buildingType==='PRIVATE HOSPITAL - PREMIUM'}selected{/if}>PRIVATE HOSPITAL - PREMIUM</option>
            <option {if $buildingType==='PRIVATE SCHOOL'}selected{/if}>PRIVATE SCHOOL</option>
            <option {if $buildingType==='PRIVATE UNIVERSITY / COLLEGE'}selected{/if}>PRIVATE UNIVERSITY / COLLEGE</option>
            <option {if $buildingType==='PUBLIC HOSPITAL'}selected{/if}>PUBLIC HOSPITAL</option>
            <option {if $buildingType==='PUBLIC LIBRARY'}selected{/if}>PUBLIC LIBRARY</option>
            <option {if $buildingType==='PUBLIC SCHOOL'}selected{/if}>PUBLIC SCHOOL</option>
            <option {if $buildingType==='PUBLIC UNIVERSITY / COLLEGE'}selected{/if}>PUBLIC UNIVERSITY / COLLEGE</option>
            <option {if $buildingType==='REHABILITATION CENTER'}selected{/if}>REHABILITATION CENTER</option>
            <option {if $buildingType==='RELIGIOUS CENTER'}selected{/if}>RELIGIOUS CENTER</option>
            <option {if $buildingType==='RESTAURANT / BAR / CLUB'}selected{/if}>RESTAURANT / BAR / CLUB</option>
            <option {if $buildingType==='RETIREMENT / NURSING HOME'}selected{/if}>RETIREMENT / NURSING HOME</option>
            <option {if $buildingType==='SCIENCE PARK / LAB’S'}selected{/if}>SCIENCE PARK / LAB’S</option>
            <option {if $buildingType==='SINGLE FAMILY HOMES'}selected{/if}>SINGLE FAMILY HOMES</option>
            <option {if $buildingType==='SPORTS COMPLEX'}selected{/if}>SPORTS COMPLEX</option>
            <option {if $buildingType==='SUPERMARKET / HYPERMARKET'}selected{/if}>SUPERMARKET / HYPERMARKET</option>
            <option {if $buildingType==='SUPPLY SHIP'}selected{/if}>SUPPLY SHIP</option>
            <option {if $buildingType==='THEME PARK'}selected{/if}>THEME PARK</option>
            <option {if $buildingType==='WAR SHIP'}selected{/if}>WAR SHIP</option>
            <option {if $buildingType==='WAREHOUSE'}selected{/if}>WAREHOUSE</option>
            <option {if $buildingType==='WIND MILL / WIND PARK'}selected{/if}>WIND MILL / WIND PARK</option>
        </select>
    </div>
</div>