<div class="form-row align-items-center">
    <div class="col-sm-12 my-1">
        <small id="errorOrganizationCode" class="form-text text-danger">&nbsp;{$errorOrganizationCode}</small>
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text">Organization Code</div>
            </div>

           <select class="form-control" id="organizationCode" name="organizationCode" aria-describedby="organizationCodeHelp">
                {foreach from=$Locales item=org}
                    <option {if $organizationCode==={$org['localOracle']}} selected{/if} value="{$org['localOracle']}">{$org['localOracle']} - {$org['descrip']}</option>
                {/foreach}
            </select>
        </div>
    </div>
</div>
