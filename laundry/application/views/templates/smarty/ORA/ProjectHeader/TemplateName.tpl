<div class="col-sm-4 my-1">
    <small id="errorTemplateName" class="form-text text-danger">&nbsp;{$errorTemplateName}</small>
    <div class="input-group">
        <div class="input-group-prepend">
            <div class="input-group-text">Template Name</div>
        </div>
        <select class="form-control" id="templateName" name="templateName" aria-describedby="templateNameHelp" placeholder="Template Name">
            <option {if $templateName== $NITemplateName}selected{/if} value="DM NI TEMPLATE {$country}">DM NI TEMPLATE {$country}</option>
            <option {if $templateName== $MODTemplateName}selected{/if} value="DM MOD TEMPLATE {$country}">DM MOD TEMPLATE {$country}</option>
        </select>
    </div>
</div>