<h3>{$title}</h3>
<br/>
<div class="row align-items-center">
    <div class="col-12">
        <p class="alert-danger p-2 text-center">{$message['PT']}</p>
        <p class="alert-danger p-2 text-center">{$message['EN']}</p>
    </div>

    <div class="col-12 alert-info p-5">
        <p class="text-left">{$projectName}</p>
        <pre class="text-left">
SELECT ID,* FROM O_STG_PROJECT_HEADER WHERE PROJECT_NAME LIKE '%{$projectName}%'
SELECT ID,* FROM O_STG_PROJECT_TASKS WHERE PROJECT_NAME LIKE '%{$projectName}%'
SELECT ID,* FROM O_STG_PROJECT_CLASSIFICATIONS WHERE PROJECT_NAME LIKE '%{$projectName}%'
SELECT ID,* FROM O_STG_PROJECT_RET_RULES WHERE PROJECT_NAME LIKE '%{$projectName}%'

/**
DELETE FROM O_STG_PROJECT_HEADER WHERE ID IN ({foreach from=$headers item=i name=fooHeaders}{$i.id}{if not $smarty.foreach.fooHeaders.last},{/if}{/foreach})
DELETE FROM O_STG_PROJECT_TASKS WHERE ID IN ({foreach from=$tasks item=i name=fooTasks}{$i.id}{if not $smarty.foreach.fooTasks.last},{/if}{/foreach})
DELETE FROM O_STG_PROJECT_CLASSIFICATIONS WHERE ID IN ({foreach from=$classifications item=i name=fooClassifications}{$i.id}{if not $smarty.foreach.fooClassifications.last},{/if}{/foreach})
DELETE FROM O_STG_PROJECT_RET_RULES WHERE ID IN ({foreach from=$retrules item=i name=fooRetRules}{$i.id}{if not $smarty.foreach.fooRetRules.last},{/if}{/foreach})
**/
        </pre>
    </div>

</div>
<br/>
<br/>
<br/>
<br/>
<br/>
