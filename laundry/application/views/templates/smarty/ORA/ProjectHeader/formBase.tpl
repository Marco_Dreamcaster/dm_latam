<br/>
<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>

    <div class="form-group">
        <label for="pmProjectReference">Pm Project Reference <small id="pmProjectReferenceHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="pmProjectReference" name="pmProjectReference" aria-describedby="pmProjectReferenceHelp" value="{$pmProjectReference}" placeholder="Pm Project Reference...">
        <small id="errorPmProjectReference" class="form-text text-danger">{$errorPmProjectReference}</small>
    </div>
    <div class="form-group">
        <label for="projectName">Project Name <small id="projectNameHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="projectName" name="projectName" aria-describedby="projectNameHelp" value="{$projectName}" placeholder="Project Name...">
        <small id="errorProjectName" class="form-text text-danger">{$errorProjectName}</small>
    </div>
    <div class="form-group">
        <label for="projectType">Project Type <small id="projectTypeHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="projectType" name="projectType" aria-describedby="projectTypeHelp" value="{$projectType}" placeholder="Project Type...">
        <small id="errorProjectType" class="form-text text-danger">{$errorProjectType}</small>
    </div>
    <div class="form-group">
        <label for="projectStatus">Project Status <small id="projectStatusHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="projectStatus" name="projectStatus" aria-describedby="projectStatusHelp" value="{$projectStatus}" placeholder="Project Status...">
        <small id="errorProjectStatus" class="form-text text-danger">{$errorProjectStatus}</small>
    </div>
    <div class="form-group">
        <label for="templateName">Template Name <small id="templateNameHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="templateName" name="templateName" aria-describedby="templateNameHelp" value="{$templateName}" placeholder="Template Name...">
        <small id="errorTemplateName" class="form-text text-danger">{$errorTemplateName}</small>
    </div>
    <div class="form-group">
        <label for="startDate">Start Date <small id="startDateHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input class="form-control" id="startDate" name="startDate" aria-describedby="startDateHelp" width="276" />
        <script>
            $('#startDate').datepicker({
                uiLibrary: 'bootstrap4',
                format:'dd-mm-yyyy',
                value: '{$startDate|date_format:"%d-%m-%Y"}'
            });
        </script>
        <small id="errorStartDate" class="form-text text-danger">{$errorStartDate}</small>
    </div>
    <div class="form-group">
        <label for="completionDate">Completion Date <small id="completionDateHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input class="form-control" id="completionDate" name="completionDate" aria-describedby="completionDateHelp" width="276" />
        <script>
            $('#completionDate').datepicker({
                uiLibrary: 'bootstrap4',
                format:'dd-mm-yyyy',
                value: '{$completionDate|date_format:"%d-%m-%Y"}'
            });
        </script>
        <small id="errorCompletionDate" class="form-text text-danger">{$errorCompletionDate}</small>
    </div>
    <div class="form-group">
        <label for="description">Description <small id="descriptionHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="description" name="description" aria-describedby="descriptionHelp" value="{$description}" placeholder="Description...">
        <small id="errorDescription" class="form-text text-danger">{$errorDescription}</small>
    </div>
    <div class="form-group">
        <label for="organizationCode">Organization Code <small id="organizationCodeHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="organizationCode" name="organizationCode" aria-describedby="organizationCodeHelp" value="{$organizationCode}" placeholder="Organization Code...">
        <small id="errorOrganizationCode" class="form-text text-danger">{$errorOrganizationCode}</small>
    </div>
    <div class="form-group">
        <label for="customerNumber">Customer Number <small id="customerNumberHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="customerNumber" name="customerNumber" aria-describedby="customerNumberHelp" value="{$customerNumber}" placeholder="Customer Number...">
        <small id="errorCustomerNumber" class="form-text text-danger">{$errorCustomerNumber}</small>
    </div>
    <div class="form-group">
        <label for="billToAddressRef">Bill To Address Ref <small id="billToAddressRefHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="billToAddressRef" name="billToAddressRef" aria-describedby="billToAddressRefHelp" value="{$billToAddressRef}" placeholder="Bill To Address Ref...">
        <small id="errorBillToAddressRef" class="form-text text-danger">{$errorBillToAddressRef}</small>
    </div>
    <div class="form-group">
        <label for="shipToAddressRef">Ship To Address Ref <small id="shipToAddressRefHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="shipToAddressRef" name="shipToAddressRef" aria-describedby="shipToAddressRefHelp" value="{$shipToAddressRef}" placeholder="Ship To Address Ref...">
        <small id="errorShipToAddressRef" class="form-text text-danger">{$errorShipToAddressRef}</small>
    </div>
    <div class="form-group">
        <label for="contactRef">Contact Ref <small id="contactRefHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="contactRef" name="contactRef" aria-describedby="contactRefHelp" value="{$contactRef}" placeholder="Contact Ref...">
        <small id="errorContactRef" class="form-text text-danger">{$errorContactRef}</small>
    </div>
    <div class="form-group">
        <label for="projectManagerRef">Project Manager Ref <small id="projectManagerRefHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="projectManagerRef" name="projectManagerRef" aria-describedby="projectManagerRefHelp" value="{$projectManagerRef}" placeholder="Project Manager Ref...">
        <small id="errorProjectManagerRef" class="form-text text-danger">{$errorProjectManagerRef}</small>
    </div>
    <div class="form-group">
        <label for="creditReceiverRef">Credit Receiver Ref <small id="creditReceiverRefHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="creditReceiverRef" name="creditReceiverRef" aria-describedby="creditReceiverRefHelp" value="{$creditReceiverRef}" placeholder="Credit Receiver Ref...">
        <small id="errorCreditReceiverRef" class="form-text text-danger">{$errorCreditReceiverRef}</small>
    </div>
    <div class="form-group">
        <label for="salesRepresentativeRef">Sales Representative Ref <small id="salesRepresentativeRefHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="salesRepresentativeRef" name="salesRepresentativeRef" aria-describedby="salesRepresentativeRefHelp" value="{$salesRepresentativeRef}" placeholder="Sales Representative Ref...">
        <small id="errorSalesRepresentativeRef" class="form-text text-danger">{$errorSalesRepresentativeRef}</small>
    </div>
    <div class="form-group">
        <label for="dummyKeyMemberRef">Dummy Key Member Ref <small id="dummyKeyMemberRefHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="dummyKeyMemberRef" name="dummyKeyMemberRef" aria-describedby="dummyKeyMemberRefHelp" value="{$dummyKeyMemberRef}" placeholder="Dummy Key Member Ref...">
        <small id="errorDummyKeyMemberRef" class="form-text text-danger">{$errorDummyKeyMemberRef}</small>
    </div>
    <div class="form-group">
        <label for="attribute2">Attribute2 <small id="attribute2Help" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="attribute2" name="attribute2" aria-describedby="attribute2Help" value="{$attribute2}" placeholder="Attribute2...">
        <small id="errorAttribute2" class="form-text text-danger">{$errorAttribute2}</small>
    </div>
    <div class="form-group">
        <label for="attribute6">Attribute6 <small id="attribute6Help" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="attribute6" name="attribute6" aria-describedby="attribute6Help" value="{$attribute6}" placeholder="Attribute6...">
        <small id="errorAttribute6" class="form-text text-danger">{$errorAttribute6}</small>
    </div>
    <div class="form-group">
        <label for="attribute8">Attribute8 <small id="attribute8Help" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="attribute8" name="attribute8" aria-describedby="attribute8Help" value="{$attribute8}" placeholder="Attribute8...">
        <small id="errorAttribute8" class="form-text text-danger">{$errorAttribute8}</small>
    </div>
    <div class="form-group">
        <label for="bookingPkgRcvd">Booking Pkg Rcvd <small id="bookingPkgRcvdHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input class="form-control" id="bookingPkgRcvd" name="bookingPkgRcvd" aria-describedby="bookingPkgRcvdHelp" width="276" />
        <script>
            $('#bookingPkgRcvd').datepicker({
                uiLibrary: 'bootstrap4',
                format:'dd-mm-yyyy',
                value: '{$bookingPkgRcvd|date_format:"%d-%m-%Y"}'
            });
        </script>
        <small id="errorBookingPkgRcvd" class="form-text text-danger">{$errorBookingPkgRcvd}</small>
    </div>
    <div class="form-group">
        <label for="completePckgRcvdDate">Complete Pckg Rcvd Date <small id="completePckgRcvdDateHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input class="form-control" id="completePckgRcvdDate" name="completePckgRcvdDate" aria-describedby="completePckgRcvdDateHelp" width="276" />
        <script>
            $('#completePckgRcvdDate').datepicker({
                uiLibrary: 'bootstrap4',
                format:'dd-mm-yyyy',
                value: '{$completePckgRcvdDate|date_format:"%d-%m-%Y"}'
            });
        </script>
        <small id="errorCompletePckgRcvdDate" class="form-text text-danger">{$errorCompletePckgRcvdDate}</small>
    </div>
    <div class="form-group">
        <label for="contractBookedDate">Contract Booked Date <small id="contractBookedDateHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input class="form-control" id="contractBookedDate" name="contractBookedDate" aria-describedby="contractBookedDateHelp" width="276" />
        <script>
            $('#contractBookedDate').datepicker({
                uiLibrary: 'bootstrap4',
                format:'dd-mm-yyyy',
                value: '{$contractBookedDate|date_format:"%d-%m-%Y"}'
            });
        </script>
        <small id="errorContractBookedDate" class="form-text text-danger">{$errorContractBookedDate}</small>
    </div>
    <div class="form-group">
        <label for="contractDate">Contract Date <small id="contractDateHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input class="form-control" id="contractDate" name="contractDate" aria-describedby="contractDateHelp" width="276" />
        <script>
            $('#contractDate').datepicker({
                uiLibrary: 'bootstrap4',
                format:'dd-mm-yyyy',
                value: '{$contractDate|date_format:"%d-%m-%Y"}'
            });
        </script>
        <small id="errorContractDate" class="form-text text-danger">{$errorContractDate}</small>
    </div>
    <div class="form-group">
        <label for="packageAndPoSales">Package And Po Sales <small id="packageAndPoSalesHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input class="form-control" id="packageAndPoSales" name="packageAndPoSales" aria-describedby="packageAndPoSalesHelp" width="276" />
        <script>
            $('#packageAndPoSales').datepicker({
                uiLibrary: 'bootstrap4',
                format:'dd-mm-yyyy',
                value: '{$packageAndPoSales|date_format:"%d-%m-%Y"}'
            });
        </script>
        <small id="errorPackageAndPoSales" class="form-text text-danger">{$errorPackageAndPoSales}</small>
    </div>
    <div class="form-group">
        <label for="executionAndReturnDate">Execution And Return Date <small id="executionAndReturnDateHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input class="form-control" id="executionAndReturnDate" name="executionAndReturnDate" aria-describedby="executionAndReturnDateHelp" width="276" />
        <script>
            $('#executionAndReturnDate').datepicker({
                uiLibrary: 'bootstrap4',
                format:'dd-mm-yyyy',
                value: '{$executionAndReturnDate|date_format:"%d-%m-%Y"}'
            });
        </script>
        <small id="errorExecutionAndReturnDate" class="form-text text-danger">{$errorExecutionAndReturnDate}</small>
    </div>
    <div class="form-group">
        <label for="contractTypeTke">ContractTypeTke</label><br/>
        <small id="contractTypeTkeHelp" class="form-text text-muted">check eXceL specs for further details</small>
        <input type="checkbox" {($contractTypeTke==true)?'checked':''} id="contractTypeTke" name="contractTypeTke">
    </div>
    <div class="form-group">
        <label for="customerPoNumber">Customer Po Number <small id="customerPoNumberHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="customerPoNumber" name="customerPoNumber" aria-describedby="customerPoNumberHelp" value="{$customerPoNumber}" placeholder="Customer Po Number...">
        <small id="errorCustomerPoNumber" class="form-text text-danger">{$errorCustomerPoNumber}</small>
    </div>
    <div class="form-group">
        <label for="outputTaxCode">Output Tax Code <small id="outputTaxCodeHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="outputTaxCode" name="outputTaxCode" aria-describedby="outputTaxCodeHelp" value="{$outputTaxCode}" placeholder="Output Tax Code...">
        <small id="errorOutputTaxCode" class="form-text text-danger">{$errorOutputTaxCode}</small>
    </div>
    <div class="form-group">
        <label for="orgName">Org Name <small id="orgNameHelp" class="form-text text-muted">check eXceL specs for further details</small></label>
        <input type="text" class="form-control" id="orgName" name="orgName" aria-describedby="orgNameHelp" value="{$orgName}" placeholder="Org Name...">
        <small id="errorOrgName" class="form-text text-danger">{$errorOrgName}</small>
    </div>
    <br/>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>
</form>
<br/>
<br/>
<br/>
<br/>
<br/>
