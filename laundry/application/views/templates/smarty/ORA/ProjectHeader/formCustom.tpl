<form class="" action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorProjectName" class="form-text text-danger">&nbsp;{$errorProjectName}</small>
            <label class="sr-only" for="inlineFormInputProjectName">Project Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Project Name </div>
                </div>
                <input type="text" class="form-control" id="projectName" name="projectName" value="{$projectName}" placeholder="Project Name" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorOrganizationCode" class="form-text text-danger">&nbsp;{$errorOrganizationCode}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Organization Code</div>
                </div>
                <select class="form-control" id="organizationCode" name="organizationCode" aria-describedby="organizationCodeHelp">
                    <option value="">--</option>
                    <option {if $organizationCode==='501001'}selected{/if} value="501001">501001 - CPA</option>
                    <option {if $organizationCode==='501030'}selected{/if} value="501030">501030 - DISTRIBUIDORES</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-4 my-1">
            <small id="errorTemplateName" class="form-text text-danger">&nbsp;{$errorTemplateName}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Template Name</div>
                </div>
                <select class="form-control" id="templateName" name="templateName" aria-describedby="templateNameHelp" placeholder="Template Name">
                    <option value="">--</option>
                    <option {if $templateName==='DM NI TEMPLATE PA'}selected{/if}>DM NI TEMPLATE PA</option>
                    <option {if $templateName==='DM MOD TEMPLATE PA'}selected{/if}>DM MOD TEMPLATE PA</option>
                    <option {if $templateName==='SERV TEMPLATE PA'}selected{/if}>SERV TEMPLATE PA</option>
                </select>
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <small id="errorBuildingType" class="form-text text-danger">&nbsp;{$errorBuildingType}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Building Type </div>
                </div>
                <select class="form-control" id="buildingType" name="buildingType" aria-describedby="buildingTypeHelp">
                    <option {if $buildingType===''}selected{/if}} value="">--</option>
                    <option {if $buildingType==='4+ STAR HOTEL'}selected{/if}>4+ STAR HOTEL</option>
                    <option {if $buildingType==='BUDGET HOTEL / MOTEL / HOSTEL'}selected{/if}>BUDGET HOTEL / MOTEL / HOSTEL</option>
                    <option {if $buildingType==='BUSINESS OFFICE - HIGH/PREMIUM'}selected{/if}>BUSINESS OFFICE - HIGH/PREMIUM</option>
                    <option {if $buildingType==='BUSINESS OFFICE-LOW/FUNCTIONAL'}selected{/if}>BUSINESS OFFICE-LOW/FUNCTIONAL</option>
                    <option {if $buildingType==='BUSINESS OFFICE-MEDIUM/COMFORT'}selected{/if}>BUSINESS OFFICE-MEDIUM/COMFORT</option>
                    <option {if $buildingType==='CARGO SHIP'}selected{/if}>CARGO SHIP</option>
                    <option {if $buildingType==='CASINO'}selected{/if}>CASINO</option>
                    <option {if $buildingType==='CINEMA COMPLEX'}selected{/if}>CINEMA COMPLEX</option>
                    <option {if $buildingType==='CONVENTION / EXHIBITION CENTER'}selected{/if}>CONVENTION / EXHIBITION CENTER</option>
                    <option {if $buildingType==='CRUISE LINER'}selected{/if}>CRUISE LINER</option>
                    <option {if $buildingType==='CULTURAL CENTER'}selected{/if}>CULTURAL CENTER</option>
                    <option {if $buildingType==='DAM'}selected{/if}>DAM</option>
                    <option {if $buildingType==='FERRY'}selected{/if}>FERRY</option>
                    <option {if $buildingType==='FUNCTIONAL DEPARTMENT STORE'}selected{/if}>FUNCTIONAL DEPARTMENT STORE</option>
                    <option {if $buildingType==='FUNCTIONAL SHOPPING MALL'}selected{/if}>FUNCTIONAL SHOPPING MALL</option>
                    <option {if $buildingType==='GOVERNMENT / PUBLIC OFFICE'}selected{/if}>GOVERNMENT / PUBLIC OFFICE</option>
                    <option {if $buildingType==='IND. PARK / FACTORY / PLANT'}selected{/if}>IND. PARK / FACTORY / PLANT</option>
                    <option {if $buildingType==='MED. CENTER / OFFICE / CLINIC'}selected{/if}>MED. CENTER / OFFICE / CLINIC</option>
                    <option {if $buildingType==='MID-RANGE HOTEL'}selected{/if}>MID-RANGE HOTEL</option>
                    <option {if $buildingType==='MINING FACILITY'}selected{/if}>MINING FACILITY</option>
                    <option {if $buildingType==='MIXED USE - FUNCTIONAL'}selected{/if}>MIXED USE - FUNCTIONAL</option>
                    <option {if $buildingType==='MIXED USE - PREMIUM'}selected{/if}>MIXED USE - PREMIUM</option>
                    <option {if $buildingType==='MUSEUM'}selected{/if}>MUSEUM</option>
                    <option {if $buildingType==='OIL / GAS FACILITY'}selected{/if}>OIL / GAS FACILITY</option>
                    <option {if $buildingType==='OIL PLATFORM'}selected{/if}>OIL PLATFORM</option>
                    <option {if $buildingType==='OWNED / RENTED APARTMENTS'}selected{/if}>OWNED / RENTED APARTMENTS</option>
                    <option {if $buildingType==='PORT'}selected{/if}>PORT</option>
                    <option {if $buildingType==='POWER STATION'}selected{/if}>POWER STATION</option>
                    <option {if $buildingType==='PREMIUM DEPARTMENT STORE'}selecte
                            {/if}>PREMIUM DEPARTMENT STORE</option>
                    <option {if $buildingType==='PREMIUM SHOPPING MALL'}selected{/if}>PREMIUM SHOPPING MALL</option>
                    <option {if $buildingType==='PRIVATE HOSPITAL - FUNCTIONAL'}selected{/if}>PRIVATE HOSPITAL - FUNCTIONAL</option>
                    <option {if $buildingType==='PRIVATE HOSPITAL - PREMIUM'}selected{/if}>PRIVATE HOSPITAL - PREMIUM</option>
                    <option {if $buildingType==='PRIVATE SCHOOL'}selected{/if}>PRIVATE SCHOOL</option>
                    <option {if $buildingType==='PRIVATE UNIVERSITY / COLLEGE'}selected{/if}>PRIVATE UNIVERSITY / COLLEGE</option>
                    <option {if $buildingType==='PUBLIC HOSPITAL'}selected{/if}>PUBLIC HOSPITAL</option>
                    <option {if $buildingType==='PUBLIC LIBRARY'}selected{/if}>PUBLIC LIBRARY</option>
                    <option {if $buildingType==='PUBLIC SCHOOL'}selected{/if}>PUBLIC SCHOOL</option>
                    <option {if $buildingType==='PUBLIC UNIVERSITY / COLLEGE'}selected{/if}>PUBLIC UNIVERSITY / COLLEGE</option>
                    <option {if $buildingType==='REHABILITATION CENTER'}selected{/if}>REHABILITATION CENTER</option>
                    <option {if $buildingType==='RELIGIOUS CENTER'}selected{/if}>RELIGIOUS CENTER</option>
                    <option {if $buildingType==='RESTAURANT / BAR / CLUB'}selected{/if}>RESTAURANT / BAR / CLUB</option>
                    <option {if $buildingType==='RETIREMENT / NURSING HOME'}selected{/if}>RETIREMENT / NURSING HOME</option>
                    <option {if $buildingType==='SCIENCE PARK / LAB’S'}selected{/if}>SCIENCE PARK / LAB’S</option>
                    <option {if $buildingType==='SINGLE FAMILY HOMES'}selected{/if}>SINGLE FAMILY HOMES</option>
                    <option {if $buildingType==='SPORTS COMPLEX'}selected{/if}>SPORTS COMPLEX</option>
                    <option {if $buildingType==='SUPERMARKET / HYPERMARKET'}selected{/if}>SUPERMARKET / HYPERMARKET</option>
                    <option {if $buildingType==='SUPPLY SHIP'}selected{/if}>SUPPLY SHIP</option>
                    <option {if $buildingType==='THEME PARK'}selected{/if}>THEME PARK</option>
                    <option {if $buildingType==='WAR SHIP'}selected{/if}>WAR SHIP</option>
                    <option {if $buildingType==='WAREHOUSE'}selected{/if}>WAREHOUSE</option>
                    <option {if $buildingType==='WIND MILL / WIND PARK'}selected{/if}>WIND MILL / WIND PARK</option>
                </select>
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <small id="errorBuildingClassification" class="form-text text-danger">&nbsp;{$errorBuildingClassification}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Building Classification</div>
                </div>
                <select class="form-control" id="buildingClassification" name="buildingClassification" aria-describedby="buildingClassificationHelp">
                    <option value="">--</option>
                    <option {if $buildingClassification==='AIRPORT'}selected{/if}>AIRPORT</option>
                    <option {if $buildingClassification==='AIRPORT/TERMINAL/TRANSPORT'}selected{/if}>AIRPORT/TERMINAL/TRANSPORT</option>
                    <option {if $buildingClassification==='COMMERCIAL OFFICE - HIGH RISE'}selected{/if}>COMMERCIAL OFFICE - HIGH RISE</option>
                    <option {if $buildingClassification==='COMMERCIAL OFFICE - LOW RISE'}selected{/if}>COMMERCIAL OFFICE - LOW RISE</option>
                    <option {if $buildingClassification==='COMMERCIAL OFFICE - MID RISE'}selected{/if}>COMMERCIAL OFFICE - MID RISE</option>
                    <option {if $buildingClassification==='COMMERCIAL/RESIDENTIAL MIX'}selected{/if}>COMMERCIAL/RESIDENTIAL MIX</option>
                    <option {if $buildingClassification==='CONDOMINIUM - HIGH RISE'}selected{/if}>CONDOMINIUM - HIGH RISE</option>
                    <option {if $buildingClassification==='CONDOMINIUM - LOW RISE'}selected{/if}>CONDOMINIUM - LOW RISE</option>
                    <option {if $buildingClassification==='CONDOMINIUM - MID RISE'}selected{/if}>CONDOMINIUM - MID RISE</option>
                    <option {if $buildingClassification==='EDUCATION / RELIGION'}selected{/if}>EDUCATION / RELIGION</option>
                    <option {if $buildingClassification==='ENTERTAINMENT / LEISURE'}selected{/if}>ENTERTAINMENT / LEISURE</option>
                    <option {if $buildingClassification==='FERRY TERMINAL'}selected{/if}>FERRY TERMINAL</option>
                    <option {if $buildingClassification==='GOVERNMENT-MILITARY'}selected{/if}>GOVERNMENT-MILITARY</option>
                    <option {if $buildingClassification==='GOVERNMENT-OTHER/RELIGIOUS'}selected{/if}>GOVERNMENT-OTHER/RELIGIOUS</option>
                    <option {if $buildingClassification==='GOVERNMENT-TRANSPORTATION'}selected{/if}>GOVERNMENT-TRANSPORTATION</option>
                    <option {if $buildingClassification==='HOSPITAL / HEALTHCARE'}selected{/if}>HOSPITAL / HEALTHCARE</option>
                    <option {if $buildingClassification==='HOSPITAL/MEDICAL FACILITY'}selected{/if}>HOSPITAL/MEDICAL FACILITY</option>
                    <option {if $buildingClassification==='HOTEL'}selected{/if}>HOTEL</option>
                    <option {if $buildingClassification==='HOTEL / RESTAURANT'}selected{/if}>HOTEL / RESTAURANT</option>
                    <option {if $buildingClassification==='INDUSTRIAL'}selected{/if}>INDUSTRIAL</option>
                    <option {if $buildingClassification==='INDUSTRIAL - OTHER'}selected{/if}>INDUSTRIAL - OTHER</option>
                    <option {if $buildingClassification==='INDUSTRIAL - PETRO/CHEMICAL'}selected{/if}>INDUSTRIAL - PETRO/CHEMICAL</option>
                    <option {if $buildingClassification==='MARINE'}selected{/if}>MARINE</option>
                    <option {if $buildingClassification==='METRO / SUBWAY'}selected{/if}>METRO / SUBWAY</option>
                    <option {if $buildingClassification==='MIXED USE BUILDINGS'}selected{/if}>MIXED USE BUILDINGS</option>
                    <option {if $buildingClassification==='OFFICE'}selected{/if}>OFFICE</option>
                    <option {if $buildingClassification==='OTHER'}selected{/if}>OTHER</option>
                    <option {if $buildingClassification==='PARKING GARAGE'}selected{/if}>PARKING GARAGE</option>
                    <option {if $buildingClassification==='PRIVATE HOMES'}selected{/if}>PRIVATE HOMES</option>
                    <option {if $buildingClassification==='PRIVATE RESIDENTIAL - COMFORT'}selected{/if}>PRIVATE RESIDENTIAL - COMFORT</option>
                    <option {if $buildingClassification==='PRIVATE RESIDENTIAL - PREMIUM'}selected{/if}>PRIVATE RESIDENTIAL - PREMIUM</option>
                    <option {if $buildingClassification==='PRIVATE RESIDENTIAL-FUNCTIONAL'}selected{/if}>PRIVATE RESIDENTIAL-FUNCTIONAL</option>
                    <option {if $buildingClassification==='PUBLIC HOUSING AUTHORITY'}selected{/if}>PUBLIC HOUSING AUTHORITY</option>
                    <option {if $buildingClassification==='PUBLIC/SEMI-PUBLIC RESIDENTIAL'}selected{/if}>PUBLIC/SEMI-PUBLIC RESIDENTIAL</option>
                    <option {if $buildingClassification==='RAILWAY / BUS STATION'}selected{/if}>RAILWAY / BUS STATION</option>
                    <option {if $buildingClassification==='RENTAL APARTMENT'}selected{/if}>RENTAL APARTMENT</option>
                    <option {if $buildingClassification==='RETAIL'}selected{/if}>RETAIL</option>
                    <option {if $buildingClassification==='RETIREMENT AND CARE HOUSING'}selected{/if}>RETIREMENT AND CARE HOUSING</option>
                    <option {if $buildingClassification==='SCHOOLS/UNIVERSITIES'}selected{/if}>SCHOOLS/UNIVERSITIES</option>
                    <option {if $buildingClassification==='SECURITY/COURT'}selected{/if}>SECURITY/COURT</option>
                    <option {if $buildingClassification==='SHIPS/PLANES/TRAINS'}selected{/if}>SHIPS/PLANES/TRAINS</option>
                    <option {if $buildingClassification==='SPECIAL EVENTS STADIUM/ARENA'}selected{/if}>SPECIAL EVENTS STADIUM/ARENA</option>
                    <option {if $buildingClassification==='URBAN MOBILITY'}selected{/if}>URBAN MOBILITY</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-4 my-1">
            <small id="errorCustomer" class="form-text text-danger">&nbsp;{$errorCustomer}</small>
            <div class="input-group">
                {if isset($customer)}
                    <div class="input-group-prepend">
                        <a class="btn text-light {if (($customer.locallyValidated)&&($customer.crossRefValidated)) }btn-success{else}btn-warning{/if}" target="_blank" href="{base_url()}Customers/customEditCleansed/{$country}/{$customer.id}" data-toggle="tooltip" data-placement="top" title="quick view of Customer {$customer.customerName}"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                    </div>
                {/if}
                <div class="input-group-prepend">
                    <div class="input-group-text">Customer Number</div>
                </div>
                <input type="text" class="form-control" id="customerNumber" name="customerNumber" value="{$customerNumber}">
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <small id="errorBillToAddressRef" class="form-text text-danger">&nbsp;{$errorBillToAddressRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Bill To</div>
                </div>
                <input type="text" class="form-control" id="billToAddressRef" name="billToAddressRef" value="{$billToAddressRef}">
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <small id="errorShipToAddressRef" class="form-text text-danger">&nbsp;{$errorShipToAddressRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Ship To</div>
                </div>
                <input type="text" class="form-control" id="shipToAddressRef" name="shipToAddressRef" value="{$shipToAddressRef}">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-4 offset-4 my-1">
            <small id="errorBillToAddressRef" class="form-text text-danger">&nbsp;{$errorContactRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Contact Ref</div>
                </div>
                <input type="text" class="form-control" id="contactRef" name="contactRef" value="{$contactRef}">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-4 my-1">
            <small id="errorProjectManagerRef" class="form-text text-danger">&nbsp;{$errorProjectManagerRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Project Manager</div>
                </div>
                <input class="form-control" id="projectManagerRef" name="projectManagerRef" aria-describedby="projectManagerRefHelp" value="{$projectManagerRef}">
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <small id="errorSalesRepresentativeRef" class="form-text text-danger">&nbsp;{$errorSalesRepresentativeRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Sales Representative</div>
                </div>
                <input class="form-control" id="salesRepresentativeRef" name="salesRepresentativeRef" aria-describedby="salesRepresentativeRef" value="{$salesRepresentativeRef}">
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <small id="errorServiceManagerRef" class="form-text text-danger">&nbsp;{$errorServiceManagerRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Service Manager</div>
                </div>
                <input class="form-control" id="serviceManagerRef" name="serviceManagerRef" aria-describedby="serviceManagerRefHelp" value="{$serviceManagerRef}">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorStartDate" class="form-text text-danger">&nbsp;{$errorStartDate}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Start Date</div>
                </div>
                <input class="form-control" id="startDate" name="startDate" aria-describedby="startDateHelp"/>
                <script>
                    $('#startDate').datepicker({
                        uiLibrary: 'bootstrap4',
                        format:'dd-mm-yyyy',
                        value: '{$startDate|date_format:"%d-%m-%Y"}'
                    });
                </script>
            </div>
            <small id="errorStartDate" class="form-text text-danger">{$errorStartDate}</small>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorAliases" class="form-text text-danger">&nbsp;{$errorCompletionDate}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Completion Date</div>
                </div>
                <input class="form-control" id="completionDate" name="completionDate" aria-describedby="completionDateHelp"/>
                <script>
                    $('#completionDate').datepicker({
                        uiLibrary: 'bootstrap4',
                        format:'dd-mm-yyyy',
                        value: '{$completionDate|date_format:"%d-%m-%Y"}'
                    });
                </script>
            </div>
            <small id="errorCompletionDate" class="form-text text-danger">{$errorCompletionDate}</small>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorObservations" class="form-text text-danger">&nbsp;{$errorObservations}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Observations</div>
                </div>
                <textarea type="text" class="form-control" id="observations" name="observations" value="" placeholder="Observations" rows="5">{$observations}</textarea>
            </div>
            <label class="alert-info text-info"><i class="fas fa-bug fa-sm fa-fw"></i> Please help us kill bugs.  Use this observation field to annotate special cases, cleansing decisions, and other relevant issues related to this record.</label>
        </div>
    </div>
    <br/>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">{$actionLabel}</button>
                <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
            </div>
        </div>
    </div>
</form>
<br>
<h4>Top Tasks</h4>
{if isset($tasks) && (sizeof($tasks)>0)}
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th scope="col"></th>
            <th>#</th>
            <th>Task Number</th>
            <th>Description</th>
            <th>Start Date</th>
            <th>Finish Date</th>
            <th>action</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$tasks item=t}
            <tr class="{if ($t.dismissed)} dismissed {elseif (($t.locallyValidated)&&($t.crossRefValidated)) } alert-success {else} alert-warning {/if} clickable-row" style="cursor:pointer" data-href='{base_url()}ProjectTasks/customEditCleansed/{$t.country}/{$t.id}'>
                <th scope="row">
                    <a href="{base_url()}ProjectTasks/editCleansed/{$t.id}" data-toggle="tooltip" data-placement="top" title="inspect all fields from task {$t.taskName}"><i class="text-primary fas fa-edit fa-sm fa-fw"></i></a>
                    &nbsp;
                    <a href="{base_url()}ProjectTasks/customEditCleansed/{$t.country}/{$t.id}" data-toggle="tooltip" data-placement="top" title="quick view of task {$t.taskName}"><i class="text-primary fas fa-eye fa-sm fa-fw"></i></a>
                </th>
                <th scope="row">{$t.id}</th>
                <td>{$t.taskNumber}</td>
                <td>{$t.description}</td>
                <td>{$t.startDate|date_format:"%d-%m-%Y"}</td>
                <td>{$t.finishDate|date_format:"%d-%m-%Y"}</td>
                <td>{$t.action}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{/if}
<br/>
<br/>
<br/>
<br/>
<br/>