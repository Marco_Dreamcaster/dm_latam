<form class="" action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorColName" class="form-text text-danger">&nbsp;{$errorProjectName}</small>
            <label class="sr-only" for="inlineFormInputProjectName">Project Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Project Name </div>
                </div>
                <input type="text" class="form-control" id="projectName" name="projectName" value="{$projectName}" placeholder="Project Name" disabled>
            </div>
        </div>
    </div>

    {include file='./OrganizationName.tpl'}

    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">

            <small id="errorCustomer" class="form-text text-danger">&nbsp;{$errorCustomer}</small>
            <div class="input-group">
                {if isset($customer)}
                    <div class="input-group-prepend">
                        <a class="btn text-light {if (($customer.locallyValidated)&&($customer.crossRefValidated)) }btn-success{else}btn-warning{/if}" target="_blank" href="{base_url()}Customers/customEditCleansed/{$country}/{$customer.id}" data-toggle="tooltip" data-placement="top" title="quick view of Customer {$customer.customerName}"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                    </div>
                {/if}
                <div class="input-group-prepend">
                    <div class="input-group-text">Customer Number</div>
                </div>
                <input type="text" class="form-control" id="customerNumber" name="customerNumber" value="{$customerNumber}">
            </div>
        </div>
    </div>

    <div class="form-row align-items-center">
        <div class="col-sm-4 my-1">
            <small id="errorBillToAddressRef" class="form-text text-danger">&nbsp;{$errorBillToAddressRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Bill To</div>
                </div>
                <input type="text" class="form-control" id="billToAddressRef" name="billToAddressRef" value="{$billToAddressRef}">
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <small id="errorShipToAddressRef" class="form-text text-danger">&nbsp;{$errorShipToAddressRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Ship To</div>
                </div>
                <input type="text" class="form-control" id="shipToAddressRef" name="shipToAddressRef" value="{$shipToAddressRef}">
            </div>
        </div>

        <div class="col-sm-4 my-1">
            <small id="errorBillToAddressRef" class="form-text text-danger">&nbsp;{$errorContactRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Contact Ref</div>
                </div>
                <input type="text" class="form-control" id="contactRef" name="contactRef" value="{$contactRef}">
            </div>
        </div>
    </div>

    <div class="form-row align-items-center">
        {include file='./TemplateName.tpl'}
        {include file='./BuildingType.tpl'}
        {include file='./BuildingClassification.tpl'}
    </div>

    <div class="form-row align-items-center">
        <div class="col-sm-4 my-1">
            <small id="errorProjectManagerRef" class="form-text text-danger">&nbsp;{$errorProjectManagerRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Project Manager</div>
                </div>
                <input class="form-control" id="projectManagerRef" name="projectManagerRef" aria-describedby="projectManagerRefHelp" value="{$projectManagerRef}">
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <small id="errorSalesRepresentativeRef" class="form-text text-danger">&nbsp;{$errorSalesRepresentativeRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Sales Representative</div>
                </div>
                <input class="form-control" id="salesRepresentativeRef" name="salesRepresentativeRef" aria-describedby="salesRepresentativeRef" value="{$salesRepresentativeRef}">
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <small id="errorServiceManagerRef" class="form-text text-danger">&nbsp;{$errorServiceManagerRef}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Service Manager</div>
                </div>
                <input class="form-control" id="serviceManagerRef" name="serviceManagerRef" aria-describedby="serviceManagerRefHelp" value="{$serviceManagerRef}">
            </div>
        </div>
    </div>

    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorStartDate" class="form-text text-danger">&nbsp;{$errorStartDate}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Start Date</div>
                    <input class="form-control" id="startDate" name="startDate" aria-describedby="startDateHelp"/>
                    <script>
                        $('#startDate').datepicker({
                            uiLibrary: 'bootstrap4',
                            format:'dd-mm-yyyy',
                            value: '{$startDate|date_format:"%d-%m-%Y"}'
                        });
                    </script>
                </div>
            </div>
            <small id="errorStartDate" class="form-text text-danger">{$errorStartDate}</small>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorAliases" class="form-text text-danger">&nbsp;{$errorCompletionDate}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Completion Date</div>

                    <input class="form-control" id="completionDate" name="completionDate" aria-describedby="completionDateHelp"/>
                    <script>
                        $('#completionDate').datepicker({
                            uiLibrary: 'bootstrap4',
                            format:'dd-mm-yyyy',
                            value: '{$completionDate|date_format:"%d-%m-%Y"}'
                        });
                    </script>
                </div>
            </div>
            <small id="errorCompletionDate" class="form-text text-danger">{$errorCompletionDate}</small>
        </div>
    </div>

    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorObservations" class="form-text text-danger">&nbsp;{$errorObservations}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Observations</div>
                </div>
                <textarea type="text" class="form-control" id="observations" name="observations" value="" placeholder="Observations" rows="5">{$observations}</textarea>
            </div>
            <label class="alert-info text-info"><i class="fas fa-bug fa-sm fa-fw"></i> Please help us kill bugs.  Use this observation field to annotate special cases, cleansing decisions, and other relevant issues related to this record.</label>
        </div>
    </div>
    <br/>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">{$actionLabel}</button>
                <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
            </div>
        </div>
    </div>
</form>
<br>
<h4>Top Tasks</h4>
{if isset($tasks) && (sizeof($tasks)>0)}
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th scope="col"></th>
            <th>#</th>
            <th>Task Number</th>
            <th>Start Date</th>
            <th>Finish Date</th>
            <th>action</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$tasks item=t}
            <tr class="{if ($t.dismissed)} dismissed {elseif (($t.locallyValidated)&&($t.crossRefValidated)) } alert-success {else} alert-warning {/if} clickable-row" style="cursor:pointer" data-href='{base_url()}ProjectTasks/customEditCleansed/{$t.country}/{$t.id}'>
                <th scope="row">
                    <!--
                    <a href="{base_url()}ProjectTasks/editCleansed/{$t.id}" data-toggle="tooltip" data-placement="top" title="inspect all fields from task {$t.taskName}"><i class="text-primary fas fa-edit fa-sm fa-fw"></i></a>
                    -->
                </th>
                <th scope="row">{$t.id}</th>
                <td>{$t.taskNumber}</td>
                <td>{$t.startDate|date_format:"%d-%m-%Y"}</td>
                <td>{$t.finishDate|date_format:"%d-%m-%Y"}</td>
                <td>{$t.action}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{/if}
<br/>
<br/>
<br/>
<br/>
<br/>

