<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade active show" id="pills-data" role="tabpanel" aria-labelledby="pills-home-tab">
        <ul class="pagination">
            <li>
                <small class="text-muted m-4"> by {$sortOrder}&nbsp;{$sortDirection}</small>
            </li>
            {if $isPaginationNeeded}
                <li class="page-item {if $currentPage==0}active{/if}">
                    <a class="page-link" href="{base_url()}{$listAction}0/{$sessionSelCol}/{if $sessionSelAsc}1{else}0{/if}" aria-label="First">
                        First
                    </a>
                </li>
                {for $foo=1 to ($lastPage-1)}
                    <li class="page-item {if $currentPage==$foo}active{/if}"><a class="page-link" href="{base_url()}{$listAction}{$foo}/{$sessionSelCol}/{if $sessionSelAsc}1{else}0{/if}">{$foo}</a></li>
                {/for}
                <li class="page-item {if $currentPage==$lastPage}active{/if}">
                    <a class="page-link" href="{base_url()}{$listAction}{$lastPage}/{$sessionSelCol}/{if $sessionSelAsc}1{else}0{/if}" aria-label="Last">
                        Last
                    </a>
                </li>
            {/if}
        </ul>
        <br/>
        <table class="table table-hover table-sm">
            <thead>
            <tr>
                <th></th>
                <th scope="col">#</th>
                <th>PM PROJECT REFERENCE</th>
                <th>ProjectName</th>
                <th>TemplateName</th>
                <th>Start Date</th>
                <th>Completion Date</th>
                <th>action</th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$cleansedVOs item=p}
                <tr class="{if ($p.dismissed)} dismissed {elseif (($p.locallyValidated)&&($p.crossRefValidated)) } alert-success {else} alert-warning {/if} clickable-row" style="cursor:pointer" data-href='{base_url()}Projects/customEditCleansed/{$p.country}/{$p.id}'>
                    <td scope="row" style="width: 70px;">
                        <a href="{base_url()}Projects/editCleansed/{$p.id}" data-toggle="tooltip" data-placement="top" title="inspect ALL fields from {$p.projectName}"><i class="text-primary fas fa-edit fa-sm fa-fw"></i></a>
                        {if (strlen($p.observations)>0)}
                            <i class="text-normal fas fa-sticky-note fa-sm fa-fw" title='OBS: {$p.observations|truncate:200}'></i>
                        {/if}
                        {if ($p.dismissed)}
                            <i class="text-muted fas fa-thumbs-down fa-sm fa-fw"></i>
                        {elseif $p.action!=''}
                        {elseif (($p.locallyValidated)&&($p.crossRefValidated)) }
                        {else}
                            <i data-toggle="tooltip" data-placement="top" title='problem in a dependency' class="text-warning fas fa-leaf fa-sm fa-fw"></i>
                        {/if}
                    </td>
                    <td><a href="{base_url()}Projects/customEditCleansed/{$p.country}/{$p.id}" data-toggle="tooltip" data-placement="top" title="Project {$p.projectName} last updated at {$p.lastUpdated|date_format:"%d-%m-%Y %H:%M:%S"}" class="text-TKE"><small>{$p.id}</small></a></td>
                    <td>{$p.pmProjectReference}</td>
                    <td>{$p.projectName}</td>
                    <td>{$p.templateName}</td>
                    <td>{$p.startDate|date_format:"%d-%m-%Y"}</td>
                    <td>{$p.completionDate|date_format:"%d-%m-%Y"}</td>
                    <td>
                        {if $p.action!=''}
                            <a href="{base_url()}Projects/customEditCleansed/{$p.country}/{$p.id}" data-toggle="tooltip" data-placement="top" title="{$p.action} - updated at {$p.lastUpdated|date_format:"%d-%m-%Y %H:%M:%S"}" class="text-TKE text-warning"><small>{$p.action|truncate:20}</small></a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
        <br/>
    </div>
    <div class="tab-pane fade show" id="pills-stats" role="tabpanel" aria-labelledby="pills-stats-tab">
        <br/>
        <div class="row p-1">
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Locally Validated</h5>
                        <h6 class="card-subtitle mb-2 text-muted">matches all criteria for local validation</h6>
                        <h1 class="card-text">{$cleansedLocallyValidated}</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">CrossRef Validated</h5>
                        <h6 class="card-subtitle mb-2 text-muted">has no dependencies on other Groups</h6>
                        <h1 class="card-text">{$cleansedCrossRefValidated}</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Validated for Output</h5>
                        <h6 class="card-subtitle mb-2 text-muted">matches all criteria</h6>
                        <h1 class="card-text">{$cleansedValidatedForOutput}</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="row p-1">
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Cleansed</h5>
                        <h6 class="card-subtitle mb-2 text-muted">moved from pre-staging table to stage</h6>
                        <h1 class="card-text">{$total}</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Recommended</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Recommended records according to fixed functional criteria</h6>
                        <h1 class="card-text">{$recommendedCleansed}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<br/>
<br/>
<br/>
<br/>
