<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade active show" id="pills-data" role="tabpanel" aria-labelledby="pills-data-tab">
        <br/>
        {if $isPaginationNeeded}
            <ul class="pagination">
                <li class="page-item {if $currentPage==0}active{/if}">
                    <a class="page-link" href="{base_url()}{$listAction}0" aria-label="First">
                        First
                    </a>
                </li>
                {for $foo=1 to ($lastPage-1)}
                    <li class="page-item {if $currentPage==$foo}active{/if}"><a class="page-link" href="{base_url()}{$listAction}{$foo}">{$foo}</a></li>
                {/for}
                <li class="page-item {if $currentPage==$lastPage}active{/if}">
                    <a class="page-link" href="{base_url()}{$listAction}{$lastPage}" aria-label="Last">
                        Last
                    </a>
                </li>
            </ul>
        {/if}
        <br/>
        <table class="table table-hover table-sm">
            <thead>
            <tr>
                <th></th>
                <th scope="col">#</th>
                <th>PM PROJECT REFERENCE</th>
                <th>ProjectName</th>
                <th>TemplateName</th>
                <th>Start Date</th>
                <th>Completion Date</th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$dirtyVOs item=p}
                <tr class="{if ($p.recommended) } alert-success {else} alert-warning {/if} clickable-row" style="cursor:pointer" data-href='{base_url()}Repairs/editDirty/{$p.id}'>
                    <th><a href="{base_url()}Repairs/editDirty/{$p.id}" data-toggle="tooltip" data-placement="top" title="inspect all fields from dirty project {$p.projectName}"><i class="text-primary fas fa-edit fa-sm fa-fw"></i></a></th>
                    <th scope="row">{$p.id}</th>
                    <td>{$p.pmProjectReference}</td>
                    <td>{$p.projectName}</td>
                    <td>{$p.templateName}</td>
                    <td>{$p.startDate|date_format:"%d-%m-%Y"}</td>
                    <td>{$p.completionDate|date_format:"%d-%m-%Y"}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
        <br/>
    </div>
    <div class="tab-pane fade show" id="pills-stats" role="tabpanel" aria-labelledby="pills-stats-tab">
        <div class="row p-1">
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Total</h5>
                        <h6 class="card-subtitle mb-2 text-muted">All records from pre-staging table</h6>
                        <h1 class="card-text">{$total}</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Dirty</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Available to be cleansed</h6>
                        <h1 class="card-text">{$dirtyDirty}</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Cleansed</h5>
                        <h6 class="card-subtitle mb-2 text-muted">With matching records on staging table</h6>
                        <h1 class="card-text">{$cleansedDirty}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<br/>
<br/>
<br/>
<br/>
