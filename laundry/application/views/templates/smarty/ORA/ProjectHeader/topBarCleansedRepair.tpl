<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><i class="fas fa-lg fa-industry fa-fw m-2 text-stage"></i><i class="fas fa-lg fa-ambulance fa-fw m-2 text-stage"></i><span class="m-2"><span class="flag-icon flag-icon-{strtolower($country)}"></span> {strtoupper($country)} {$title}</span></h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Repairs/customEditCleansed/{$country}/{$id}" data-toggle="tooltip" data-placement="top" title="quick view of project {$projectName} (NEW TAB)"><i class="fas fa-eye fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Repairs/validate/{$id}" data-toggle="tooltip" data-placement="top" title="trigger validation of project {$projectName}"><i class="fas fa-check-circle fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}ProjectTasks/createNew/{$id}" data-toggle="tooltip" data-placement="top" title="create new Project Task for {$projectName}"><i class="fas fa-plus-circle fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-secondary" target="_self" href="{base_url()}Repairs/toggle/{$id}" data-toggle="tooltip" data-placement="top" title="{if ($dismissed)}Recall{else}Dismiss{/if} Project {$projectName}"><i class="fas fa-minus-circle fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-danger" target="_self" href="{base_url()}Repairs/discard/{$id}" data-toggle="modal" data-target="#discardModal" title="discard project {$projectName} - completely remove from stage"><i class="fas fa-eraser fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-success" target="_blank" href="{base_url()}IDL/Navigate/navigate/{$country}/Projects/ProjectHeader" data-toggle="tooltip" data-placement="top" title="specification for {$country} Project group"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-success" target="_blank" href="{base_url()}scripts/00.COM/master/08.Projects/01.IDL/TKE_LA_IDL_CD_02_PROJECT_HEADER_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="latest specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Repairs/cleansed/{$country}/0/{$sessionSelCol}/{if $sessionSelAsc}1{else}0{/if}" data-toggle="tooltip" data-placement="top" title="cleansed Project for {$country}"><i class="fas fa-industry fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Repairs/dirty/{$country}" data-toggle="tooltip" data-placement="top" title="dirty Projects for {$country}"><i class="fas fa-trash-alt fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-secondary" target="_self" href="{base_url()}{$cancelAction}" data-toggle="tooltip" data-placement="top" title="go back to cleansed projects"><i class="fas fa-ban fa-sm fa-fw"></i></a>
            </li>


        </ul>
    </div>
</nav>

<div id="discardModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-danger" id="exampleModalLongTitle"><i class="fas fa-eraser fa-sm fa-fw"></i> Confirm Discard</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"/>
            </div>
            <div class="modal-body text-center p-2">
                <p class="alert-danger">You're about to discard Repair Project {$projectName}.</p>
                <p><b class="alert-danger">This cannot be undone - No se puede deshacer esta acci�n</b></p>
            </div>
            <div class="modal-footer">
                <a href="{base_url()}{$dirtyAction}" class="btn btn-danger"><i class="fas fa-eraser fa-sm fa-fw"></i> Yes, remove this item from the stage</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel <i class="fas fa-ban fa-sm fa-fw"></i></button>
            </div>
        </div>
    </div>
</div>

{if ($dismissed)}
    <div class="row">
        <div class="col-sm-12 p-2">
            <p class="dismissed text-center p-2">dismissed row, not going to flat file</p>
        </div>
    </div>
{else}
    <div class="row">
        <div class="col-sm-12 p-2">
            <p class="alert-success text-center p-2">active row</p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 p-2">
            {if (isset($cleansingAction)&&($cleansingAction!=''))}
                <p class="alert-warning text-center p-2">{$cleansingAction}</p>
            {else}
                <p class="alert-success text-center p-2">locally validated - no action required at header level</p>
            {/if}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 p-2">
            {if !($crossRefValidated)}
                <p class="alert-warning text-center p-2">problems in dependencies - check Project Tasks</p>
            {else}
                <p class="alert-success text-center p-2">no problems, going to flat file</p>
            {/if}
        </div>
    </div>
{/if}
