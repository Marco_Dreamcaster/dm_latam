<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="navbar-nav mr-auto">
            <h5 class="nav p-2"><i class="fas fa-lg fa-industry fa-fw m-2 text-stage"></i>&nbsp;<i class="fas fa-lg fa-ambulance fa-fw m-2 text-stage"></i> <span class="m-2"><span class="flag-icon flag-icon-{strtolower($country)}"></span> {strtoupper($country)} STG {$title}</span></h5>
            <ul class="navbar-nav mr-auto">
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_blank" href="{base_url()}Repairs/dirty/{$country}" data-toggle="tooltip" data-placement="top" title="dirty Repairs"><i class="fas fa-trash-alt fa-sm fa-fw"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_blank" href="{base_url()}Projects/Maps/PFF" data-toggle="tooltip" data-placement="top" title="Type Line Configuration Flex Field"><i class="fas fa-table fa-sm fa-fw"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_blank" href="{base_url()}Bundled/Repairs/{$country}" data-toggle="tooltip" data-placement="top" title="Current dataset"><i class="fas fa-file-archive fa-sm fa-fw"></i>&nbsp;ORA Zip</a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-success" target="_blank" href="{base_url()}IDL/Navigate/navigateGroup/{$country}/Projects" data-toggle="tooltip" data-placement="top" title="specification for projects group"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-success" target="_blank" href="{base_url()}scripts/00.COM/master/08.Projects/01.IDL/TKE_LA_IDL_CD_02_PROJECT_HEADER_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="latest specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                </li>

            </ul>
        </div>
        <div class="dropdown">
            <button class="btn btn-success  dropleft dropdown-toggle mr-4" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {$country} FlatFiles <i class="fas fa-cloud-download-alt fa-sm fa-fw ml-2 mr-2"></i>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="{base_url()}Bundled/Repairs/{$country}"><i class="fas fa-file-archive fa-sm fa-fw m-1"></i>Download ZIP</a>
                <a class="dropdown-item" href="{base_url()}Inspect/Repairs/{$country}/TKE_LA_CD_02_REPAIR_HEADER" target="_blank"><i class="fas fa-eye fa-sm fa-fw m-1"></i>RepairHeader</a>
                <a class="dropdown-item" href="{base_url()}Inspect/Repairs/{$country}/TKE_LA_CD_02_REPAIR_TASKS" target="_blank"><i class="fas fa-eye fa-sm fa-fw m-1"></i>RepairTasks</a>
                <a class="dropdown-item" href="{base_url()}Inspect/Repairs/{$country}/TKE_LA_CD_02_REPAIR_CLASSIFICATIONS" target="_blank"><i class="fas fa-eye fa-sm fa-fw m-1"></i>RepairClassifications</a>
                <a class="dropdown-item" href="{base_url()}Inspect/Repairs/{$country}/TKE_LA_CD_02_REPAIR_RET_RULES" target="_blank"><i class="fas fa-eye fa-sm fa-fw m-1"></i>RepairRetRules</a>
            </div>
        </div>
        &nbsp;
    </div>
</nav>
<br/>
