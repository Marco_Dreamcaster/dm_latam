<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="navbar-nav mr-auto">
            <h5 class="nav p-2"><i class="fas fa-lg fa-trash-alt fa-fw m-2 text-pre-stage"></i><i class="fas fa-lg fa-ambulance fa-fw m-2 text-pre-stage"></i><span class="m-2"><span class="flag-icon flag-icon-{strtolower($country)}"></span> {strtoupper($country)} {$title}</span></h5>
            <ul class="navbar-nav mr-auto">
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_blank" href="{base_url()}Repairs/cleansed/{$country}/0/{$sessionSelCol}/{if $sessionSelAsc}1{else}0{/if}" data-toggle="tooltip" data-placement="top" title="cleansed Repairs"><i class="fas fa-industry fa-sm fa-fw"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_blank" href="{base_url()}Projects/Maps/PFF" data-toggle="tooltip" data-placement="top" title="Type Line Configuration Flex Field"><i class="fas fa-table fa-sm fa-fw"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-success" target="_blank" href="{base_url()}IDL/Navigate/navigateGroup/{$country}/Projects" data-toggle="tooltip" data-placement="top" title="specification for projects group"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                </li>
            </ul>
        </div>
        &nbsp;
    </div>
</nav>
<br/>