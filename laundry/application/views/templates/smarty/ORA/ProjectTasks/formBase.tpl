{if (isset($cleansingAction)&&($cleansingAction!=''))}
    <p class="alert-warning text-center">{$cleansingAction}</p>
{else}
    <p class="alert-success text-center">validated - no action required</p>
{/if}
<br/>
<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>
    <div class="form-group">
        <label for="projectName">Project Name <small id="projectNameHelp" class="form-text text-muted">(Same as specified in the Project Flat File)</small></label>
        <input type="text" class="form-control" id="projectName" name="projectName" aria-describedby="projectNameHelp" value="{$projectName}" placeholder="Project Name...">
        <small id="errorProjectName" class="form-text text-danger">{$errorProjectName}</small>
    </div>
    <div class="form-group">
        <label for="taskNumber">Task Number <small id="taskNumberHelp" class="form-text text-muted">(Unit Number for Top Task, WBS structure can be...)</small></label>
        <input type="text" class="form-control" id="taskNumber" name="taskNumber" aria-describedby="taskNumberHelp" value="{$taskNumber}" placeholder="Task Number...">
        <small id="errorTaskNumber" class="form-text text-danger">{$errorTaskNumber}</small>
    </div>
    <div class="form-group">
        <label for="taskName">Task Name <small id="taskNameHelp" class="form-text text-muted">(Name of the Task)</small></label>
        <input type="text" class="form-control" id="taskName" name="taskName" aria-describedby="taskNameHelp" value="{$taskName}" placeholder="Task Name...">
        <small id="errorTaskName" class="form-text text-danger">{$errorTaskName}</small>
    </div>
    <div class="form-group">
        <label for="parentTaskNumber">Parent Task Number <small id="parentTaskNumberHelp" class="form-text text-muted">("Parent Task Number associated with the record...)</small></label>
        <input type="text" class="form-control" id="parentTaskNumber" name="parentTaskNumber" aria-describedby="parentTaskNumberHelp" value="{$parentTaskNumber}" placeholder="Parent Task Number...">
        <small id="errorParentTaskNumber" class="form-text text-danger">{$errorParentTaskNumber}</small>
    </div>
    <div class="form-group">
        <label for="description">Description <small id="descriptionHelp" class="form-text text-muted">(Short description of the task)</small></label>
        <input type="text" class="form-control" id="description" name="description" aria-describedby="descriptionHelp" value="{$description}" placeholder="Description...">
        <small id="errorDescription" class="form-text text-danger">{$errorDescription}</small>
    </div>
    <div class="form-group">
        <label for="startDate">Start Date <small id="startDateHelp" class="form-text text-muted">(Task Start Date,  Must be = to or after...)</small></label>
        <input class="form-control" id="startDate" name="startDate" aria-describedby="startDateHelp" width="276" />
        <script>
            $('#startDate').datepicker({
                uiLibrary: 'bootstrap4',
                format:'dd-mm-yyyy',
                value: '{$startDate|date_format:"%d-%m-%Y"}'
            });
        </script>
        <small id="errorStartDate" class="form-text text-danger">{$errorStartDate}</small>
    </div>
    <div class="form-group">
        <label for="finishDate">Finish Date <small id="finishDateHelp" class="form-text text-muted">(Planned task finish date. Must be = to or...)</small></label>
        <input class="form-control" id="finishDate" name="finishDate" aria-describedby="finishDateHelp" width="276" />
        <script>
            $('#finishDate').datepicker({
                uiLibrary: 'bootstrap4',
                format:'dd-mm-yyyy',
                value: '{$finishDate|date_format:"%d-%m-%Y"}'
            });
        </script>
        <small id="errorFinishDate" class="form-text text-danger">{$errorFinishDate}</small>
    </div>
    <div class="form-group">
        <label for="serviceType">Service Type <small id="serviceTypeHelp" class="form-text text-muted">("Service Type from top task is used to...)</small></label>
        <input type="text" class="form-control" id="serviceType" name="serviceType" aria-describedby="serviceTypeHelp" value="{$serviceType}" placeholder="Service Type...">
        <small id="errorServiceType" class="form-text text-danger">{$errorServiceType}</small>
    </div>
    <div class="form-group">
        <label for="allowCharge">AllowCharge</label><br/>
        <small id="allowChargeHelp" class="form-text text-muted">(Allow charge flag (use Y for all lowest level...)</small>
        <input type="checkbox" {($allowCharge==true)?'checked':''} id="allowCharge" name="allowCharge">
    </div>
    <div class="form-group">
        <label for="attribute1">Attribute1 <small id="attribute1Help" class="form-text text-muted">(Group Number-Only for Top Task)</small></label>
        <input type="text" class="form-control" id="attribute1" name="attribute1" aria-describedby="attribute1Help" value="{$attribute1}" placeholder="Attribute1...">
        <small id="errorAttribute1" class="form-text text-danger">{$errorAttribute1}</small>
    </div>
    <div class="form-group">
        <label for="attribute2">Attribute2 <small id="attribute2Help" class="form-text text-muted">("Product Type-Only for Top Task
                Please use one...)</small></label>
        <input type="text" class="form-control" id="attribute2" name="attribute2" aria-describedby="attribute2Help" value="{$attribute2}" placeholder="Attribute2...">
        <small id="errorAttribute2" class="form-text text-danger">{$errorAttribute2}</small>
    </div>
    <div class="form-group">
        <label for="attribute3">Attribute3 <small id="attribute3Help" class="form-text text-muted">(Product Configuration-Only for Top Task,...)</small></label>
        <input type="text" class="form-control" id="attribute3" name="attribute3" aria-describedby="attribute3Help" value="{$attribute3}" placeholder="Attribute3...">
        <small id="errorAttribute3" class="form-text text-danger">{$errorAttribute3}</small>
    </div>
    <div class="form-group">
        <label for="attribute4">Attribute4 <small id="attribute4Help" class="form-text text-muted">("Application-Only for Top Task. Choose one of...)</small></label>
        <input type="text" class="form-control" id="attribute4" name="attribute4" aria-describedby="attribute4Help" value="{$attribute4}" placeholder="Attribute4...">
        <small id="errorAttribute4" class="form-text text-danger">{$errorAttribute4}</small>
    </div>
    <div class="form-group">
        <label for="attribute5">Attribute5 <small id="attribute5Help" class="form-text text-muted">("Turnover Date-Only for Top Task - Default to...)</small></label>
        <input type="text" class="form-control" id="attribute5" name="attribute5" aria-describedby="attribute5Help" value="{$attribute5}" placeholder="Attribute5...">
        <small id="errorAttribute5" class="form-text text-danger">{$errorAttribute5}</small>
    </div>
    <div class="form-group">
        <label for="attribute6">Attribute6</label><br/>
        <small id="attribute6Help" class="form-text text-muted">(Final Customer Acceptance-Only for Top Task:  Y...)</small>
        <input type="checkbox" {($attribute6==true)?'checked':''} id="attribute6" name="attribute6">
    </div>
    <div class="form-group">
        <label for="attribute7">Attribute7 <small id="attribute7Help" class="form-text text-muted">("Product Line - Only for Top Task
                Please use...)</small></label>
        <input type="text" class="form-control" id="attribute7" name="attribute7" aria-describedby="attribute7Help" value="{$attribute7}" placeholder="Attribute7...">
        <small id="errorAttribute7" class="form-text text-danger">{$errorAttribute7}</small>
    </div>
    <div class="form-group">
        <label for="attribute8">Attribute8 <small id="attribute8Help" class="form-text text-muted">("Installed Base-Only for Top Task -...)</small></label>
        <input type="text" class="form-control" id="attribute8" name="attribute8" aria-describedby="attribute8Help" value="{$attribute8}" placeholder="Attribute8...">
        <small id="errorAttribute8" class="form-text text-danger">{$errorAttribute8}</small>
    </div>
    <div class="form-group">
        <label for="noOfFloors">No Of Floors <small id="noOfFloorsHelp" class="form-text text-muted">("No of Floors - Applicable for Top Task Only...)</small></label>
        <input type="text" class="form-control" id="noOfFloors" name="noOfFloors" aria-describedby="noOfFloorsHelp" value="{$noOfFloors}" placeholder="No Of Floors...">
        <small id="errorNoOfFloors" class="form-text text-danger">{$errorNoOfFloors}</small>
    </div>
    <div class="form-group">
        <label for="speed">Speed <small id="speedHelp" class="form-text text-muted">("Speed Information - Applicable for Top Task...)</small></label>
        <input type="text" class="form-control" id="speed" name="speed" aria-describedby="speedHelp" value="{$speed}" placeholder="Speed...">
        <small id="errorSpeed" class="form-text text-danger">{$errorSpeed}</small>
    </div>
    <div class="form-group">
        <label for="capacity">Capacity <small id="capacityHelp" class="form-text text-muted">("Capacity Information Applicable for Top Task...)</small></label>
        <input type="text" class="form-control" id="capacity" name="capacity" aria-describedby="capacityHelp" value="{$capacity}" placeholder="Capacity...">
        <small id="errorCapacity" class="form-text text-danger">{$errorCapacity}</small>
    </div>
    <div class="form-group">
        <label for="orgName">Org Name <small id="orgNameHelp" class="form-text text-muted">(Operating Unit Name)</small></label>
        <input type="text" class="form-control" id="orgName" name="orgName" aria-describedby="orgNameHelp" value="{$orgName}" placeholder="Org Name...">
        <small id="errorOrgName" class="form-text text-danger">{$errorOrgName}</small>
    </div>


</form>
<br/>
<br/>
<br/>
<br/>
<br/>
