<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <label class="sr-only" for="inlineFormInputProjectName">Project Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Project Name <small id="errorProjectName" class="form-text text-danger">&nbsp;{$errorProjectName}</small></div>
                </div>
                <input type="text" class="form-control" id="projectName" name="projectName" value="{$projectName}" placeholder="Project Number" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <label class="sr-only" for="inlineFormInputTaskNumber">Task Number</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Task Number <small id="errorTaskNumber" class="form-text text-danger">&nbsp;{$errorTaskNumber}</small></div>
                </div>
                <input type="text" class="form-control" id="taskNumber" name="taskNumber" value="{$taskNumber}" placeholder="Task Number">
            </div>
        </div>
    </div>
    <br/>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <label class="sr-only" for="inlineFormInputTaskName">Task Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Task Name</div>
                </div>
                <input type="text" class="form-control" id="taskName" name="taskName" value="{$taskName}" placeholder="Task Name">
            </div>
            <small id="errorTaskName" class="form-text text-danger">&nbsp;{$errorTaskName}</small>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <label class="sr-only" for="inlineFormInputDescription">Description</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Description <small id="errorDescription" class="form-text text-danger">&nbsp;{$errorDescription}</small></div>
                </div>
                <input type="text" class="form-control" id="description" name="description" value="{$description}" placeholder="Description">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorStartDate" class="form-text text-danger">&nbsp;{$errorStartDate}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Start Date</div>

                <input class="form-control" id="startDate" name="startDate" aria-describedby="startDateHelp"/>
                <script>
                    $('#startDate').datepicker({
                        uiLibrary: 'bootstrap4',
                        format:'dd-mm-yyyy',
                        value: '{$startDate|date_format:"%d-%m-%Y"}'
                    });
                </script>
                </div>
            </div>
            <small id="errorStartDate" class="form-text text-danger">{$errorStartDate}</small>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorStartDate" class="form-text text-danger">&nbsp;{$errorFinishDate}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Finish Date</div>

                <input class="form-control" id="finishDate" name="finishDate" aria-describedby="finishDateHelp"/>
                <script>
                    $('#finishDate').datepicker({
                        uiLibrary: 'bootstrap4',
                        format:'dd-mm-yyyy',
                        value: '{$finishDate|date_format:"%d-%m-%Y"}'
                    });
                </script>
                </div>
            </div>
            <small id="errorFinishDate" class="form-text text-danger">{$errorFinishDate}</small>
        </div>
    </div>

    <div class="form-row align-items-center">
        <div class="col-sm-4 my-1">
            <small id="errorAttribute2" class="form-text text-danger">&nbsp;{$errorAttribute2}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Type</div>
                </div>
                <select class="form-control" id="attribute2" name="attribute2">
                    <option value="">--</option>
                    {foreach from=$types item=tp}
                        <option value="{$tp['TP']}" {$tp['selected']}>{str_replace('_', ' ', $tp['TP'])}</option>
                    {/foreach}
                </select>
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <small id="errorAttribute7" class="form-text text-danger">&nbsp;{$errorAttribute7}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Line</div>
                </div>
                <select class="form-control" id="attribute7" name="attribute7">
                    <option value="">--</option>
                    {foreach from=$lines item=ln}
                        <option value="{$ln['LN']}" {$ln['selected']} data-chained="{$ln['TP']}">{str_replace('_', ' ', $ln['LN'])}</option>
                    {/foreach}
                </select>
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <small id="errorAttribute3" class="form-text text-danger">&nbsp;{$errorAttribute3}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Configuration</div>
                </div>
                <select class="form-control" id="attribute3" name="attribute3">
                    <option value="">--</option>
                    {foreach from=$configurations item=co}
                        <option value="{$co['CO']}" {$co['selected']} data-chained="{$co['LN']}">{$co['CO']}</option>
                    {/foreach}
                </select>
            </div>
        </div>
    </div>
    <script>
        $("#attribute3").chained("#attribute7");
        $("#attribute7").chained("#attribute2");
    </script>
    <br>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">{$actionLabel}</button>
                <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
            </div>
        </div>
    </div>
</form>
<br/>
<br/>
<br/>
<br/>
<br/>

