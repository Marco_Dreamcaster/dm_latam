<br/>
<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>
    <div class="form-group">
        <label for="vendorNumber">Vendor Number <small id="vendorNumberHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="vendorNumber" name="vendorNumber" aria-describedby="vendorNumberHelp" value="{$vendorNumber}" placeholder="Vendor Number...">
        <small id="errorVendorNumber" class="form-text text-danger">{$errorVendorNumber}</small>
    </div>
    <div class="form-group">
        <label for="bankName">Bank Name <small id="bankNameHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="bankName" name="bankName" aria-describedby="bankNameHelp" value="{$bankName}" placeholder="Bank Name...">
        <small id="errorBankName" class="form-text text-danger">{$errorBankName}</small>
    </div>
    <div class="form-group">
        <label for="branchName">Branch Name <small id="branchNameHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="branchName" name="branchName" aria-describedby="branchNameHelp" value="{$branchName}" placeholder="Branch Name...">
        <small id="errorBranchName" class="form-text text-danger">{$errorBranchName}</small>
    </div>
    <div class="form-group">
        <label for="bankNumber">Bank Number <small id="bankNumberHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="bankNumber" name="bankNumber" aria-describedby="bankNumberHelp" value="{$bankNumber}" placeholder="Bank Number...">
        <small id="errorBankNumber" class="form-text text-danger">{$errorBankNumber}</small>
    </div>
    <div class="form-group">
        <label for="branchNumber">Branch Number <small id="branchNumberHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="branchNumber" name="branchNumber" aria-describedby="branchNumberHelp" value="{$branchNumber}" placeholder="Branch Number...">
        <small id="errorBranchNumber" class="form-text text-danger">{$errorBranchNumber}</small>
    </div>
    <div class="form-group">
        <label for="bankHomeCountry">Bank Home Country <small id="bankHomeCountryHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="bankHomeCountry" name="bankHomeCountry" aria-describedby="bankHomeCountryHelp" value="{$bankHomeCountry}" placeholder="Bank Home Country...">
        <small id="errorBankHomeCountry" class="form-text text-danger">{$errorBankHomeCountry}</small>
    </div>
    <div class="form-group">
        <label for="taxPayerId">Tax Payer Id <small id="taxPayerIdHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="taxPayerId" name="taxPayerId" aria-describedby="taxPayerIdHelp" value="{$taxPayerId}" placeholder="Tax Payer Id...">
        <small id="errorTaxPayerId" class="form-text text-danger">{$errorTaxPayerId}</small>
    </div>
    <div class="form-group">
        <label for="bankAccountName">Bank Account Name <small id="bankAccountNameHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="bankAccountName" name="bankAccountName" aria-describedby="bankAccountNameHelp" value="{$bankAccountName}" placeholder="Bank Account Name...">
        <small id="errorBankAccountName" class="form-text text-danger">{$errorBankAccountName}</small>
    </div>
    <div class="form-group">
        <label for="bankAccountNum">Bank Account Num <small id="bankAccountNumHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="bankAccountNum" name="bankAccountNum" aria-describedby="bankAccountNumHelp" value="{$bankAccountNum}" placeholder="Bank Account Num...">
        <small id="errorBankAccountNum" class="form-text text-danger">{$errorBankAccountNum}</small>
    </div>
    <div class="form-group">
        <label for="currencyCode">Currency Code <small id="currencyCodeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="currencyCode" name="currencyCode" aria-describedby="currencyCodeHelp" value="{$currencyCode}" placeholder="Currency Code...">
        <small id="errorCurrencyCode" class="form-text text-danger">{$errorCurrencyCode}</small>
    </div>
    <div class="form-group">
        <label for="startDate">Start Date <small id="startDateHelp" class="form-text text-muted">()</small></label>
        <input class="form-control" id="startDate" name="startDate" aria-describedby="startDateHelp" width="276" />
        <script>
            $('#startDate').datepicker({
                uiLibrary: 'bootstrap4',
                format:'dd-mm-yyyy',
                value: '{$startDate|date_format:"%d-%m-%Y"}'
            });
        </script>
        <small id="errorStartDate" class="form-text text-danger">{$errorStartDate}</small>
    </div>
    <div class="form-group">
        <label for="endDate">End Date <small id="endDateHelp" class="form-text text-muted">()</small></label>
        <input class="form-control" id="endDate" name="endDate" aria-describedby="endDateHelp" width="276" />
        <script>
            $('#endDate').datepicker({
                uiLibrary: 'bootstrap4',
                format:'dd-mm-yyyy',
                value: '{$endDate|date_format:"%d-%m-%Y"}'
            });
        </script>
        <small id="errorEndDate" class="form-text text-danger">{$errorEndDate}</small>
    </div>
    <div class="form-group">
        <label for="orgName">Org Name <small id="orgNameHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="orgName" name="orgName" aria-describedby="orgNameHelp" value="{$orgName}" placeholder="Org Name...">
        <small id="errorOrgName" class="form-text text-danger">{$errorOrgName}</small>
    </div>

    <br/>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>
</form>
<br/>
<br/>
<br/>
<br/>
<br/>
