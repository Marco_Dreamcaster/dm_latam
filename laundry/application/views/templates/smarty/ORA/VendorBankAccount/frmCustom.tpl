<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorVendorNumber" class="form-text text-danger">&nbsp;{$errorVendorNumber}</small>
            <label class="sr-only" for="inlineFormInputVendorNumber">Vendor Number</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Vendor Number</div>
                </div>
                <input type="text" class="form-control" id="vendorNumber" name="vendorNumber" value="{$vendorNumber}"
                       placeholder="Vendor Number" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorTaxPayerId" class="form-text text-danger">&nbsp;{$errorTaxPayerId}</small>
            <label class="sr-only" for="inlineFormInputTaxPayerId">Tax Payer Id</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Tax Payer Id</div>
                </div>
                <input type="text" class="form-control" id="taxPayerId" name="taxPayerId" value="{$taxPayerId}"
                       placeholder="Tax Payer Id">
            </div>
        </div>

    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorBankAccountName" class="form-text text-danger">&nbsp;{$errorBankAccountName}</small>
            <label class="sr-only" for="inlineFormInputBankAccountName">Bank Account Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Bank Account Name</div>
                </div>
                <input type="text" class="form-control" id="bankAccountName" name="bankAccountName"
                       value="{$bankAccountName}" placeholder="Bank Account Name">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorBankNumber" class="form-text text-danger">&nbsp;{$errorBankNumber}</small>
            <label class="sr-only" for="inlineFormInputBankNumber">Bank Number</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Bank Number</div>
                </div>
                <input type="text" class="form-control" id="bankNumber" name="bankNumber" value="{$bankNumber}"
                       placeholder="Bank Number">
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorBankName" class="form-text text-danger">&nbsp;{$errorBankName}</small>
            <label class="sr-only" for="inlineFormInputBankName">Bank Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Bank Name</div>
                </div>
                <input type="text" class="form-control" id="bankName" name="bankName" value="{$bankName}"
                       placeholder="Bank Name">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorBankHomeCountry" class="form-text text-danger">&nbsp;{$errorBankHomeCountry}</small>
            <label class="sr-only" for="inlineFormInputBankHomeCountry">Bank Home Country</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Bank Home Country</div>
                </div>
                <select class="form-control" id="bankHomeCountry" name="bankHomeCountry"
                        aria-describedby="$descriptor.shortNameHelp" placeholder="Bank Home Country">
                    <option value="">--</option>
                    {foreach from=$countryCodes item=cc}
                        <option value="{$cc['CC']}" {$cc['selected']} value="{$cc['CC']}">{$cc['CC']}</option>
                    {/foreach}
                </select>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorCurrencyCode" class="form-text text-danger">&nbsp;{$errorCurrencyCode}</small>
            <label class="sr-only" for="inlineFormInputCurrencyCode">Currency Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Currency Code</div>
                </div>
                <select class="form-control" id="currencyCode" name="currencyCode"
                        aria-describedby="$descriptor.shortNameHelp" placeholder="Currency Code">
                    <option {if $currencyCode==='USD'}selected{/if}>USD</option>
                    <option {if $currencyCode==='EUR'}selected{/if}>EUR</option>
                    <option {if $currencyCode==='USD'}selected{/if}>USD</option>
                    <option {if $currencyCode==='EUR'}selected{/if}>EUR</option>
                    <option {if $currencyCode==='ARS'}selected{/if}>ARS</option>
                    <option {if $currencyCode==='CAD'}selected{/if}>CAD</option>
                    <option {if $currencyCode==='COP'}selected{/if}>COP</option>
                    <option {if $currencyCode==='CLP'}selected{/if}>CLP</option>
                    <option {if $currencyCode==='CRC'}selected{/if}>CRC</option>
                    <option {if $currencyCode==='GTQ'}selected{/if}>GTQ</option>
                    <option {if $currencyCode==='HNL'}selected{/if}>HNL</option>
                    <option {if $currencyCode==='MXN'}selected{/if}>MXN</option>
                    <option {if $currencyCode==='NIO'}selected{/if}>NIO</option>
                    <option {if $currencyCode==='PAB'}selected{/if}>PAB</option>
                    <option {if $currencyCode==='PEN'}selected{/if}>PEN</option>
                    <option {if $currencyCode==='PYG'}selected{/if}>PYG</option>
                    <option {if $currencyCode==='UYU'}selected{/if}>UYU</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorBranchNumber" class="form-text text-danger">&nbsp;{$errorBranchNumber}</small>
            <label class="sr-only" for="inlineFormInputBranchNumber">Branch Number</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Branch Number</div>
                </div>
                <input type="text" class="form-control" id="branchNumber" name="branchNumber" value="{$branchNumber}"
                       placeholder="Branch Number">
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorBranchName" class="form-text text-danger">&nbsp;{$errorBranchName}</small>
            <label class="sr-only" for="inlineFormInputBranchName">Branch Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Branch Name</div>
                </div>
                <input type="text" class="form-control" id="branchName" name="branchName" value="{$branchName}"
                       placeholder="Branch Name">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorBankAccountNum" class="form-text text-danger">&nbsp;{$errorBankAccountNum}</small>
            <label class="sr-only" for="inlineFormInputBankAccountNum">Bank Account Num</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Bank Account Num</div>
                </div>
                <input type="text" class="form-control" id="bankAccountNum" name="bankAccountNum"
                       value="{$bankAccountNum}" placeholder="Bank Account Num">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorIban" class="form-text text-danger">&nbsp;{$errorIban}</small>
            <label class="sr-only" for="inlineFormInputIban">IBAN</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-warning">IBAN</div>
                </div>
                <input type="text" class="form-control" id="iban" name="iban"
                       value="{$iban}" placeholder="IBAN">
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorBankAccountNum" class="form-text text-danger">&nbsp;{$errorBIC}</small>
            <label class="sr-only" for="inlineFormInputBankAccountNum">BIC</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-warning">BIC</div>
                </div>
                <input type="text" class="form-control" id="bic" name="bic"
                       value="{$bic}" placeholder="BIC">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorType" class="form-text text-danger">&nbsp;{$errorType}</small>
            <label class="sr-only" for="inlineFormInputType">Type</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-warning">Type</div>
                </div>
                <input type="text" class="form-control" id="type" name="type"
                       value="{$type}" placeholder="type">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorStartDate" class="form-text text-danger">&nbsp;{$errorStartDate}</small>
            <label class="sr-only" for="inlineFormInputStartDate">Start Date</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Start Date</div>

                <input class="form-control" id="startDate" name="startDate" aria-describedby="startDateHelp"/>
                <script>
                    $('#startDate').datepicker({
                        uiLibrary: 'bootstrap4',
                        format: 'dd-mm-yyyy',
                        value: '{$startDate|date_format:"%d-%m-%Y"}'
                    });
                </script>
                </div>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorEndDate" class="form-text text-danger">&nbsp;{$errorEndDate}</small>
            <label class="sr-only" class="text-danger" for="inlineFormInputEndDate">End Date (has been disabled)</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" class="text-danger">End Date (disabled)</div>

                <input class="form-control" id="endDate" name="endDate" aria-describedby="endDateHelp" disabled/>
                <!--script>
                    $('#endDate').datepicker({
                        uiLibrary: 'bootstrap4',
                        format: 'dd-mm-yyyy',
                        value: '{$endDate|date_format:"%d-%m-%Y"}'
                    });
                </script-->
                </div>
            </div>
        </div>
    </div>
    <br/>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>

</form>
<br/>
<br/>
<br/>
<br/>
<br/>
