<br/>
<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>
    <div class="form-group">
        <label for="vendorName">Vendor Name <small id="vendorNameHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="vendorName" name="vendorName" aria-describedby="vendorNameHelp" value="{$vendorName}" placeholder="Vendor Name...">
        <small id="errorVendorName" class="form-text text-danger">{$errorVendorName}</small>
    </div>
    <div class="form-group">
        <label for="segment1">Segment1 <small id="segment1Help" class="form-text text-muted">(aka VENDOR_NUMBER)</small></label>
        <input type="text" class="form-control" id="segment1" name="segment1" aria-describedby="segment1Help" value="{$segment1}" placeholder="Segment1...">
        <small id="errorSegment1" class="form-text text-danger">{$errorSegment1}</small>
    </div>
    <div class="form-group">
        <label for="vendorNameAlt">Vendor Name Alt <small id="vendorNameAltHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="vendorNameAlt" name="vendorNameAlt" aria-describedby="vendorNameAltHelp" value="{$vendorNameAlt}" placeholder="Vendor Name Alt...">
        <small id="errorVendorNameAlt" class="form-text text-danger">{$errorVendorNameAlt}</small>
    </div>
    <div class="form-group">
        <label for="vendorTypeLookupCode">Vendor Type Lookup Code <small id="vendorTypeLookupCodeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="vendorTypeLookupCode" name="vendorTypeLookupCode" aria-describedby="vendorTypeLookupCodeHelp" value="{$vendorTypeLookupCode}" placeholder="Vendor Type Lookup Code...">
        <small id="errorVendorTypeLookupCode" class="form-text text-danger">{$errorVendorTypeLookupCode}</small>
    </div>
    <div class="form-group">
        <label for="_8id"> 8id <small id="_8idHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="_8id" name="_8id" aria-describedby="_8idHelp" value="{$_8id}" placeholder=" 8id...">
        <small id="error_8id" class="form-text text-danger">{$error_8id}</small>
    </div>
    <div class="form-group">
        <label for="vatRegistrationNum">Vat Registration Num <small id="vatRegistrationNumHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="vatRegistrationNum" name="vatRegistrationNum" aria-describedby="vatRegistrationNumHelp" value="{$vatRegistrationNum}" placeholder="Vat Registration Num...">
        <small id="errorVatRegistrationNum" class="form-text text-danger">{$errorVatRegistrationNum}</small>
    </div>
    <div class="form-group">
        <label for="num1099">Num 1099 <small id="num1099Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="num1099" name="num1099" aria-describedby="num1099Help" value="{$num1099}" placeholder="Num 1099...">
        <small id="errorNum1099" class="form-text text-danger">{$errorNum1099}</small>
    </div>
    <div class="form-group">
        <label for="federalReportableFlag">FederalReportableFlag</label><br/>
        <small id="federalReportableFlagHelp" class="form-text text-muted">()</small>
        <input type="checkbox" {($federalReportableFlag==true)?'checked':''} id="federalReportableFlag" name="federalReportableFlag">
    </div>
    <div class="form-group">
        <label for="type1099">Type 1099 <small id="type1099Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="type1099" name="type1099" aria-describedby="type1099Help" value="{$type1099}" placeholder="Type 1099...">
        <small id="errorType1099" class="form-text text-danger">{$errorType1099}</small>
    </div>
    <div class="form-group">
        <label for="stateReportableFlag">StateReportableFlag</label><br/>
        <small id="stateReportableFlagHelp" class="form-text text-muted">()</small>
        <input type="checkbox" {($stateReportableFlag==true)?'checked':''} id="stateReportableFlag" name="stateReportableFlag">
    </div>
    <div class="form-group">
        <label for="allowAwtFlag">AllowAwtFlag</label><br/>
        <small id="allowAwtFlagHelp" class="form-text text-muted">()</small>
        <input type="checkbox" {($allowAwtFlag==true)?'checked':''} id="allowAwtFlag" name="allowAwtFlag">
    </div>
    <div class="form-group">
        <label for="matchOptionLevel">Match Option Level <small id="matchOptionLevelHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="matchOptionLevel" name="matchOptionLevel" aria-describedby="matchOptionLevelHelp" value="{$matchOptionLevel}" placeholder="Match Option Level...">
        <small id="errorMatchOptionLevel" class="form-text text-danger">{$errorMatchOptionLevel}</small>
    </div>
    <div class="form-group">
        <label for="paymentMethodLookupCode">Payment Method Lookup Code <small id="paymentMethodLookupCodeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="paymentMethodLookupCode" name="paymentMethodLookupCode" aria-describedby="paymentMethodLookupCodeHelp" value="{$paymentMethodLookupCode}" placeholder="Payment Method Lookup Code...">
        <small id="errorPaymentMethodLookupCode" class="form-text text-danger">{$errorPaymentMethodLookupCode}</small>
    </div>
    <div class="form-group">
        <label for="globalFlag">GlobalFlag</label><br/>
        <small id="globalFlagHelp" class="form-text text-muted">()</small>
        <input type="checkbox" {($globalFlag==true)?'checked':''} id="globalFlag" name="globalFlag">
    </div>
    <div class="form-group">
        <label for="sensitiveVendor">SensitiveVendor</label><br/>
        <small id="sensitiveVendorHelp" class="form-text text-muted">()</small>
        <input type="checkbox" {($sensitiveVendor==true)?'checked':''} id="sensitiveVendor" name="sensitiveVendor">
    </div>
    <div class="form-group">
        <label for="globalAttribute1">Global Attribute1 <small id="globalAttribute1Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="globalAttribute1" name="globalAttribute1" aria-describedby="globalAttribute1Help" value="{$globalAttribute1}" placeholder="Global Attribute1...">
        <small id="errorGlobalAttribute1" class="form-text text-danger">{$errorGlobalAttribute1}</small>
    </div>
    <div class="form-group">
        <label for="globalAttribute8">Global Attribute8 <small id="globalAttribute8Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="globalAttribute8" name="globalAttribute8" aria-describedby="globalAttribute8Help" value="{$globalAttribute8}" placeholder="Global Attribute8...">
        <small id="errorGlobalAttribute8" class="form-text text-danger">{$errorGlobalAttribute8}</small>
    </div>
    <div class="form-group">
        <label for="globalAttribute9">Global Attribute9 <small id="globalAttribute9Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="globalAttribute9" name="globalAttribute9" aria-describedby="globalAttribute9Help" value="{$globalAttribute9}" placeholder="Global Attribute9...">
        <small id="errorGlobalAttribute9" class="form-text text-danger">{$errorGlobalAttribute9}</small>
    </div>
    <div class="form-group">
        <label for="globalAttribute10">Global Attribute10 <small id="globalAttribute10Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="globalAttribute10" name="globalAttribute10" aria-describedby="globalAttribute10Help" value="{$globalAttribute10}" placeholder="Global Attribute10...">
        <small id="errorGlobalAttribute10" class="form-text text-danger">{$errorGlobalAttribute10}</small>
    </div>
    <div class="form-group">
        <label for="globalAttribute12">Global Attribute12 <small id="globalAttribute12Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="globalAttribute12" name="globalAttribute12" aria-describedby="globalAttribute12Help" value="{$globalAttribute12}" placeholder="Global Attribute12...">
        <small id="errorGlobalAttribute12" class="form-text text-danger">{$errorGlobalAttribute12}</small>
    </div>
    <div class="form-group">
        <label for="globalAttribute15">Global Attribute15 <small id="globalAttribute15Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="globalAttribute15" name="globalAttribute15" aria-describedby="globalAttribute15Help" value="{$globalAttribute15}" placeholder="Global Attribute15...">
        <small id="errorGlobalAttribute15" class="form-text text-danger">{$errorGlobalAttribute15}</small>
    </div>
    <div class="form-group">
        <label for="globalAttribute16">Global Attribute16 <small id="globalAttribute16Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="globalAttribute16" name="globalAttribute16" aria-describedby="globalAttribute16Help" value="{$globalAttribute16}" placeholder="Global Attribute16...">
        <small id="errorGlobalAttribute16" class="form-text text-danger">{$errorGlobalAttribute16}</small>
    </div>
    <div class="form-group">
        <label for="attributeCategory">Attribute Category <small id="attributeCategoryHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attributeCategory" name="attributeCategory" aria-describedby="attributeCategoryHelp" value="{$attributeCategory}" placeholder="Attribute Category...">
        <small id="errorAttributeCategory" class="form-text text-danger">{$errorAttributeCategory}</small>
    </div>
    <div class="form-group">
        <label for="attribute1">Attribute1 <small id="attribute1Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute1" name="attribute1" aria-describedby="attribute1Help" value="{$attribute1}" placeholder="Attribute1...">
        <small id="errorAttribute1" class="form-text text-danger">{$errorAttribute1}</small>
    </div>
    <div class="form-group">
        <label for="attribute2">Attribute2 <small id="attribute2Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute2" name="attribute2" aria-describedby="attribute2Help" value="{$attribute2}" placeholder="Attribute2...">
        <small id="errorAttribute2" class="form-text text-danger">{$errorAttribute2}</small>
    </div>
    <div class="form-group">
        <label for="attribute3">Attribute3 <small id="attribute3Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute3" name="attribute3" aria-describedby="attribute3Help" value="{$attribute3}" placeholder="Attribute3...">
        <small id="errorAttribute3" class="form-text text-danger">{$errorAttribute3}</small>
    </div>
    <div class="form-group">
        <label for="attribute4">Attribute4 <small id="attribute4Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute4" name="attribute4" aria-describedby="attribute4Help" value="{$attribute4}" placeholder="Attribute4...">
        <small id="errorAttribute4" class="form-text text-danger">{$errorAttribute4}</small>
    </div>
    <div class="form-group">
        <label for="attribute5">Attribute5 <small id="attribute5Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute5" name="attribute5" aria-describedby="attribute5Help" value="{$attribute5}" placeholder="Attribute5...">
        <small id="errorAttribute5" class="form-text text-danger">{$errorAttribute5}</small>
    </div>
    <div class="form-group">
        <label for="attribute6">Attribute6 <small id="attribute6Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute6" name="attribute6" aria-describedby="attribute6Help" value="{$attribute6}" placeholder="Attribute6...">
        <small id="errorAttribute6" class="form-text text-danger">{$errorAttribute6}</small>
    </div>
    <div class="form-group">
        <label for="attribute7">Attribute7 <small id="attribute7Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute7" name="attribute7" aria-describedby="attribute7Help" value="{$attribute7}" placeholder="Attribute7...">
        <small id="errorAttribute7" class="form-text text-danger">{$errorAttribute7}</small>
    </div>
    <div class="form-group">
        <label for="attribute8">Attribute8 <small id="attribute8Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute8" name="attribute8" aria-describedby="attribute8Help" value="{$attribute8}" placeholder="Attribute8...">
        <small id="errorAttribute8" class="form-text text-danger">{$errorAttribute8}</small>
    </div>
    <div class="form-group">
        <label for="attribute9">Attribute9 <small id="attribute9Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute9" name="attribute9" aria-describedby="attribute9Help" value="{$attribute9}" placeholder="Attribute9...">
        <small id="errorAttribute9" class="form-text text-danger">{$errorAttribute9}</small>
    </div>
    <div class="form-group">
        <label for="attribute10">Attribute10 <small id="attribute10Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute10" name="attribute10" aria-describedby="attribute10Help" value="{$attribute10}" placeholder="Attribute10...">
        <small id="errorAttribute10" class="form-text text-danger">{$errorAttribute10}</small>
    </div>
    <div class="form-group">
        <label for="orgName">Org Name <small id="orgNameHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="orgName" name="orgName" aria-describedby="orgNameHelp" value="{$orgName}" placeholder="Org Name...">
        <small id="errorOrgName" class="form-text text-danger">{$errorOrgName}</small>
    </div>

    <br/>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>
</form>
<br/>
<br/>
<br/>
<br/>
<br/>
