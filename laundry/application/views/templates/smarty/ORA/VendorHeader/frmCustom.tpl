<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorSegment1" class="form-text text-danger">&nbsp;{$errorSegment1}</small>
            <label class="sr-only" for="inlineFormInputSegment1">Vendor Number (Segment1)</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Vendor Number (Segment1)</div>
                </div>
                <input type="text" class="form-control" id="segment1" name="segment1" value="{$segment1}" placeholder="Segment1" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorVendorNameAlt" class="form-text text-danger">&nbsp;{$errorVendorNameAlt}</small>
            <label class="sr-only" for="inlineFormInputVendorNameAlt">Vendor Name Alt</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Vendor Name Alt</div>
                </div>
                <input type="text" class="form-control" id="vendorNameAlt" name="vendorNameAlt" value="{$vendorNameAlt}" placeholder="Vendor Name Alt" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-8 my-1">
            <small id="errorVendorName" class="form-text text-danger">&nbsp;{$errorVendorName}</small>
            <label class="sr-only" for="inlineFormInputVendorName">Vendor Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Vendor Name</div>
                </div>
                <input type="text" class="form-control" id="vendorName" name="vendorName" value="{$vendorName}" placeholder="Vendor Name">
            </div>
        </div>
        <div class="col-4 my-1">
            <small id="errorVendorTypeLookupCode" class="form-text text-danger">&nbsp;{$errorVendorTypeLookupCode}</small>
            <label class="sr-only" for="inlineFormInputVendorTypeLookupCode">Vendor Type Lookup Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Vendor Type Lookup Code</div>
                </div>
                <select class="form-control" id="vendorTypeLookupCode" name="vendorTypeLookupCode" aria-describedby="$descriptor.shortNameHelp" placeholder="Vendor Type Lookup Code">
                    <option {if $vendorTypeLookupCode==='VENDOR'}selected{/if}>VENDOR</option>
                    <option {if $vendorTypeLookupCode==='CUSTOMER'}selected{/if}>CUSTOMER</option>
                    <option {if $vendorTypeLookupCode==='TKE'}selected{/if}>TKE</option>
                    <option {if $vendorTypeLookupCode==='TAX AUTHORITY'}selected{/if}>TAX AUTHORITY</option>
                    <option {if $vendorTypeLookupCode==='EMPLOYEE'}selected{/if}>EMPLOYEE</option>
                    <option {if $vendorTypeLookupCode==='PRODUCTION'}selected{/if}>PRODUCTION</option>
                    <option {if $vendorTypeLookupCode==='SUB CONTRACT'}selected{/if}>SUB CONTRACT</option>
                    <option {if $vendorTypeLookupCode==='IT'}selected{/if}>IT</option>
                    <option {if $vendorTypeLookupCode==='SECURITY'}selected{/if}>SECURITY</option>
                    <option {if $vendorTypeLookupCode==='COMMOM CARRIER'}selected{/if}>COMMOM CARRIER</option>
                    <option {if $vendorTypeLookupCode==='PARTNERSHIP'}selected{/if}>PARTNERSHIP</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorVatRegistrationNum" class="form-text text-danger">&nbsp;{$errorVatRegistrationNum}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Vat Registration Num
                        <i class="fas fa-globe fa-sm fa-fw text-TKE m-2"></i>
                    </div>
                </div>
                <input type="text" class="form-control" id="vatRegistrationNum" name="vatRegistrationNum" value="{$vatRegistrationNum}">
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorNum1099" class="form-text text-danger">&nbsp;{$errorNum1099}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Tax Payer ID (NUM_1099)
                        <i class="fas fa-globe fa-sm fa-fw text-TKE m-2"></i>
                    </div>
            </div>
                <input type="text" class="form-control" id="num1099" name="num1099" value="{$num1099}" readonly>
            </div>
        </div>
        {if ( $country=='AR' || $country=='CL' || $country=='CO')}
        <div class="col-sm-8 my-1">
            <small id="errorCoExtAttribute8" class="form-text text-danger">&nbsp;{$errorCoExtAttribute8}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Tax Payer ID (no digit) (CO_EXT_ATTRIBUTE8)
                        <span class="flag-icon flag-icon-co mr-1 ml-1" title="CO"></span>
                        <span class="flag-icon flag-icon-ar mr-1 ml-1" title="AR"></span>
                        <span class="flag-icon flag-icon-cl mr-1 ml-1" title="CL"></span>
                    </div>
                </div>
                <input type="text" class="form-control" id="coExtAttribute8" name="coExtAttribute8" value="{$coExtAttribute8}" readonly>
            </div>
        </div>
        <div class="col-sm-4 my-1">
            <small id="errorGlobalAttribute12" class="form-text text-danger">&nbsp;{$errorGlobalAttribute12}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Tax Payer ID Digit (GLOBAL_ATTRIBUTE12)&nbsp;
                        <span class="flag-icon flag-icon-co mr-1 ml-1" title="CO"></span>
                        <span class="flag-icon flag-icon-ar mr-1 ml-1" title="AR"></span>
                        <span class="flag-icon flag-icon-cl mr-1 ml-1" title="CL"></span>
                    </div>
                </div>
                <input type="text" class="form-control" id="globalAttribute12" name="globalAttribute12" value="{$globalAttribute12}" readonly>
            </div>
        </div>

        {if ($country=='AR' || $country=='CO' )}
            <div class="col-sm-12 my-1">
                <small id="errorGlobalAttribute9" class="form-text text-danger">&nbsp;{$errorGlobalAttribute9}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Origin
                            <span class="flag-icon flag-icon-ar mr-2 ml-2" title="AR only"></span>
                            <span class="flag-icon flag-icon-co mr-2 ml-2" title="CO only"></span>
                        </div>
                    </div>
                    <select class="form-control" id="globalAttribute9" name="globalAttribute9">
                        <option value="">--</option>
                        <option {if $globalAttribute9==='DOMESTIC_ORIGIN'}selected{/if}>DOMESTIC_ORIGIN</option>
                        <option {if $globalAttribute9==='FOREIGN_ORIGIN'}selected{/if}>FOREIGN_ORIGIN</option>
                    </select>
                </div>
            </div>
        {/if}

        {if ($country=='AR')}
            <div class="col-4 my-1">
                <small id="errorGlobalAttribute10" class="form-text text-danger">&nbsp;{$errorGlobalAttribute10}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Primary ID Type <span class="flag-icon flag-icon-ar mr-2 ml-2" title="AR only"></span></div>
                    </div>
                    <select class="form-control" id="globalAttribute10" name="globalAttribute10">
                        <option {if $globalAttribute10==='80'}selected{/if} value="80">Domestic Corporation or Foreign Business Entity</option>
                        <option {if $globalAttribute10==='82'}selected{/if} value="82">Employee</option>
                        <option {if $globalAttribute10==='96'}selected{/if} value="96">Individual</option>
                    </select>
                </div>
            </div>
            <div class="col-4 my-1">
                <small id="errorGlobalAttribute15" class="form-text text-danger">&nbsp;{$errorGlobalAttribute15}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Multi Lateral Contributor<span class="flag-icon flag-icon-ar mr-2 ml-2" title="AR only"></span></div>
                    </div>
                    <select class="form-control" id="globalAttribute15" name="globalAttribute15">
                        <option {if $globalAttribute15==='Y'}selected{/if} value="Y">Y</option>
                        <option {if $globalAttribute15==='N'}selected{/if} value="N">N</option>
                    </select>
                </div>
            </div>
            <div class="col-4 my-1">
                <small id="errorGlobalAttribute16" class="form-text text-danger">&nbsp;{$errorGlobalAttribute16}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Transaction Letter <span class="flag-icon flag-icon-ar mr-2 ml-2" title="AR only"></span></div>
                    </div>
                    <select class="form-control" id="globalAttribute16" name="globalAttribute16">
                        <option {if $globalAttribute16==='A'}selected{/if} value="A">A</option>
                        <option {if $globalAttribute16==='B'}selected{/if} value="B">B</option>
                        <option {if $globalAttribute16==='C'}selected{/if} value="C">C</option>
                        <option {if $globalAttribute16==='E'}selected{/if} value="E">E</option>
                        <option {if $globalAttribute16==='X'}selected{/if} value="X">X</option>
                    </select>
                </div>
            </div>
        {/if}

        {if ($country=='CL')}
            <div class="col-6 my-1">
                <small id="errorGlobalAttribute10" class="form-text text-danger">&nbsp;{$errorGlobalAttribute10}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Primary ID Type <span class="flag-icon flag-icon-cl mr-2 ml-2" title="CL only"></span></div>
                    </div>
                    <select class="form-control" id="globalAttribute10" name="globalAttribute10">
                        <option {if $globalAttribute10==='DOMESTIC_ORIGIN'}selected{/if}>DOMESTIC_ORIGIN</option>
                        <option {if $globalAttribute10==='FOREIGN_ORIGIN'}selected{/if}>FOREIGN_ORIGIN</option>
                    </select>
                </div>
            </div>
            <div class="col-6 my-1">
                <small id="errorAttribute5" class="form-text text-danger">&nbsp;{$errorAttribute5}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">GIRO <span class="flag-icon flag-icon-cl mr-2 ml-2" title="CL only"></span></div>
                    </div>
                    <select class="form-control" id="attribute5" name="attribute5">
                        <option {if $attribute5==='ACONDICIONAMIENTO DE EDIFICIOS'}selected{/if} value="ACONDICIONAMIENTO DE EDIFICIOS">ACONDICIONAMIENTO DE EDIFICIOS</option>
                        <option {if $attribute5==='ACTIVIDADES DE ASESORAMIENTO EMPRESARIAL Y EN MATE'}selected{/if} value="ACTIVIDADES DE ASESORAMIENTO EMPRESARIAL Y EN MATE">ACTIVIDADES DE ASESORAMIENTO EMPRESARIAL Y EN MATE</option>
                        <option {if $attribute5==='ACTIVIDADES DE BIBLIOTECAS Y ARCHIVOS'}selected{/if} value="ACTIVIDADES DE BIBLIOTECAS Y ARCHIVOS">ACTIVIDADES DE BIBLIOTECAS Y ARCHIVOS</option>
                        <option {if $attribute5==='ACTIVIDADES DE CASINO DE JUEGOS'}selected{/if} value="ACTIVIDADES DE CASINO DE JUEGOS">ACTIVIDADES DE CASINO DE JUEGOS</option>
                        <option {if $attribute5==='ACTIVIDADES DE ORGANIZACIONES RELIGIOSAS'}selected{/if} value="ACTIVIDADES DE ORGANIZACIONES RELIGIOSAS">ACTIVIDADES DE ORGANIZACIONES RELIGIOSAS</option>
                        <option {if $attribute5==='ADMINISTRACION'}selected{/if} value="ADMINISTRACION">ADMINISTRACION</option>
                        <option {if $attribute5==='ADMINISTRACION DE ACTIVOS'}selected{/if} value="ADMINISTRACION DE ACTIVOS">ADMINISTRACION DE ACTIVOS</option>
                        <option {if $attribute5==='ADMINISTRACION DE BIENES INMUEBLES'}selected{/if} value="ADMINISTRACION DE BIENES INMUEBLES">ADMINISTRACION DE BIENES INMUEBLES</option>
                        <option {if $attribute5==='ADMINISTRACION DE CENTROS COMERCIALES'}selected{/if} value="ADMINISTRACION DE CENTROS COMERCIALES">ADMINISTRACION DE CENTROS COMERCIALES</option>
                        <option {if $attribute5==='ADMINISTRACION DE INMUEBLES Y ASESORIAS DE NEGOCIO'}selected{/if} value="ADMINISTRACION DE INMUEBLES Y ASESORIAS DE NEGOCIO">ADMINISTRACION DE INMUEBLES Y ASESORIAS DE NEGOCIO</option>
                        <option {if $attribute5==='ADMINISTRACION DE SERVICIOS DE EDUCACION Y SALUD T'}selected{/if} value="ADMINISTRACION DE SERVICIOS DE EDUCACION Y SALUD T">ADMINISTRACION DE SERVICIOS DE EDUCACION Y SALUD T</option>
                        <option {if $attribute5==='ADMINISTRACION FONDOS DE PENSIONES'}selected{/if} value="ADMINISTRACION FONDOS DE PENSIONES">ADMINISTRACION FONDOS DE PENSIONES</option>
                        <option {if $attribute5==='ADMINISTRACION PUBLICA'}selected{/if} value="ADMINISTRACION PUBLICA">ADMINISTRACION PUBLICA</option>
                        <option {if $attribute5==='ADMINISTRACION SERVICIOS DE VIVIENDA'}selected{/if} value="ADMINISTRACION SERVICIOS DE VIVIENDA">ADMINISTRACION SERVICIOS DE VIVIENDA</option>
                        <option {if $attribute5==='ADMINISTRACION Y CAPACITACION DE RECURSOS'}selected{/if} value="ADMINISTRACION Y CAPACITACION DE RECURSOS">ADMINISTRACION Y CAPACITACION DE RECURSOS</option>
                        <option {if $attribute5==='ADMINSTRADORA DE  ESTABLECIMIENTOS COMERCIALES'}selected{/if} value="ADMINSTRADORA DE  ESTABLECIMIENTOS COMERCIALES">ADMINSTRADORA DE  ESTABLECIMIENTOS COMERCIALES</option>
                        <option {if $attribute5==='ADQUISICION ENAJ COMERC Y EXP DE BIENES RAICES'}selected{/if} value="ADQUISICION ENAJ COMERC Y EXP DE BIENES RAICES">ADQUISICION ENAJ COMERC Y EXP DE BIENES RAICES</option>
                        <option {if $attribute5==='AEROPUERTO'}selected{/if} value="AEROPUERTO">AEROPUERTO</option>
                        <option {if $attribute5==='AGENCIA'}selected{/if} value="AGENCIA">AGENCIA</option>
                        <option {if $attribute5==='AGENCIA DE NAVES'}selected{/if} value="AGENCIA DE NAVES">AGENCIA DE NAVES</option>
                        <option {if $attribute5==='ALIMENTACION'}selected{/if} value="ALIMENTACION">ALIMENTACION</option>
                        <option {if $attribute5==='ALMACENAMIENTO Y LOGISTICA'}selected{/if} value="ALMACENAMIENTO Y LOGISTICA">ALMACENAMIENTO Y LOGISTICA</option>
                        <option {if $attribute5==='ALMACENES MEDIANOS (VENTA DE ALIMENTOS)'}selected{/if} value="ALMACENES MEDIANOS (VENTA DE ALIMENTOS)">ALMACENES MEDIANOS (VENTA DE ALIMENTOS)</option>
                        <option {if $attribute5==='ARR. Y EXPL. DE BIE. INMUEBLES'}selected{/if} value="ARR. Y EXPL. DE BIE. INMUEBLES">ARR. Y EXPL. DE BIE. INMUEBLES</option>
                        <option {if $attribute5==='ARRDO. Y EXPLOT.BS INMUEBLES, CONST. VIVENDAS, OBR'}selected{/if} value="ARRDO. Y EXPLOT.BS INMUEBLES, CONST. VIVENDAS, OBR">ARRDO. Y EXPLOT.BS INMUEBLES, CONST. VIVENDAS, OBR</option>
                        <option {if $attribute5==='ARRIENDO DE BIENES RAICES SIN AMOBLAR'}selected{/if} value="ARRIENDO DE BIENES RAICES SIN AMOBLAR">ARRIENDO DE BIENES RAICES SIN AMOBLAR</option>
                        <option {if $attribute5==='ARRIENDO DE INMUEBLES'}selected{/if} value="ARRIENDO DE INMUEBLES">ARRIENDO DE INMUEBLES</option>
                        <option {if $attribute5==='ARRIENDO DE INMUEBLES AMOBLADOS'}selected{/if} value="ARRIENDO DE INMUEBLES AMOBLADOS">ARRIENDO DE INMUEBLES AMOBLADOS</option>
                        <option {if $attribute5==='ARRIENDO DE INMUEBLES SIN AMOBLAR Y AMOBLADO CON I'}selected{/if} value="ARRIENDO DE INMUEBLES SIN AMOBLAR Y AMOBLADO CON I">ARRIENDO DE INMUEBLES SIN AMOBLAR Y AMOBLADO CON I</option>
                        <option {if $attribute5==='ARRIENDO DE LOCALES'}selected{/if} value="ARRIENDO DE LOCALES">ARRIENDO DE LOCALES</option>
                        <option {if $attribute5==='ARRIENDO DE MAQUINARIA Y EQUIPOS'}selected{/if} value="ARRIENDO DE MAQUINARIA Y EQUIPOS">ARRIENDO DE MAQUINARIA Y EQUIPOS</option>
                        <option {if $attribute5==='ARRIENDO Y EXPLOTACION DE BIENES INMUEBLES'}selected{/if} value="ARRIENDO Y EXPLOTACION DE BIENES INMUEBLES">ARRIENDO Y EXPLOTACION DE BIENES INMUEBLES</option>
                        <option {if $attribute5==='ARTICULOS ELECTRICOS'}selected{/if} value="ARTICULOS ELECTRICOS">ARTICULOS ELECTRICOS</option>
                        <option {if $attribute5==='ASESORIAS E INVERSIONES'}selected{/if} value="ASESORIAS E INVERSIONES">ASESORIAS E INVERSIONES</option>
                        <option {if $attribute5==='ASOCIACION DE DIALIZADOS Y TRASPLANTADOS DE CHILE'}selected{/if} value="ASOCIACION DE DIALIZADOS Y TRASPLANTADOS DE CHILE">ASOCIACION DE DIALIZADOS Y TRASPLANTADOS DE CHILE</option>
                        <option {if $attribute5==='ASOCIACION GREMIAL'}selected{/if} value="ASOCIACION GREMIAL">ASOCIACION GREMIAL</option>
                        <option {if $attribute5==='BOLSA DE COMERCIO'}selected{/if} value="BOLSA DE COMERCIO">BOLSA DE COMERCIO</option>
                        <option {if $attribute5==='CASINO'}selected{/if} value="CASINO">CASINO</option>
                        <option {if $attribute5==='CCAF LOS ANDES'}selected{/if} value="CCAF LOS ANDES">CCAF LOS ANDES</option>
                        <option {if $attribute5==='CENTRO COMERCIAL                                  '}selected{/if} value="CENTRO COMERCIAL                                  ">CENTRO COMERCIAL                                  </option>
                        <option {if $attribute5==='CENTRO MEDICO'}selected{/if} value="CENTRO MEDICO">CENTRO MEDICO</option>
                        <option {if $attribute5==='CLINICA HOSPITAL'}selected{/if} value="CLINICA HOSPITAL">CLINICA HOSPITAL</option>
                        <option {if $attribute5==='COMERCIAL'}selected{/if} value="COMERCIAL">COMERCIAL</option>
                        <option {if $attribute5==='COMERCIALIZACION DE PRODUCTOS TEXTILES'}selected{/if} value="COMERCIALIZACION DE PRODUCTOS TEXTILES">COMERCIALIZACION DE PRODUCTOS TEXTILES</option>
                        <option {if $attribute5==='COMERCIALIZADORA DE ARTICULOS DE VESTIR'}selected{/if} value="COMERCIALIZADORA DE ARTICULOS DE VESTIR">COMERCIALIZADORA DE ARTICULOS DE VESTIR</option>
                        <option {if $attribute5==='COMERCIALIZADORA DE LOCALES'}selected{/if} value="COMERCIALIZADORA DE LOCALES">COMERCIALIZADORA DE LOCALES</option>
                        <option {if $attribute5==='COMERCIALIZADORA DE LOCALES Y SERV. DEL CDPA'}selected{/if} value="COMERCIALIZADORA DE LOCALES Y SERV. DEL CDPA">COMERCIALIZADORA DE LOCALES Y SERV. DEL CDPA</option>
                        <option {if $attribute5==='COMPANIA DE SEGUROS'}selected{/if} value="COMPANIA DE SEGUROS">COMPANIA DE SEGUROS</option>
                        <option {if $attribute5==='COMPRA - VENTA - ARRIENDO INMUEBLES ARRENDADOS Y S'}selected{/if} value="COMPRA - VENTA - ARRIENDO INMUEBLES ARRENDADOS Y S">COMPRA - VENTA - ARRIENDO INMUEBLES ARRENDADOS Y S</option>
                        <option {if $attribute5==='COMPRA VENTA Y ALQUILER DE INMUEBLES'}selected{/if} value="COMPRA VENTA Y ALQUILER DE INMUEBLES">COMPRA VENTA Y ALQUILER DE INMUEBLES</option>
                        <option {if $attribute5==='COMPRA, VENTA Y ALQUILER'}selected{/if} value="COMPRA, VENTA Y ALQUILER">COMPRA, VENTA Y ALQUILER</option>
                        <option {if $attribute5==='COMPRA, VENTA Y ALQUILER (EXCEPTO AMOBLADOS) '}selected{/if} value="COMPRA, VENTA Y ALQUILER (EXCEPTO AMOBLADOS) ">COMPRA, VENTA Y ALQUILER (EXCEPTO AMOBLADOS) </option>
                        <option {if $attribute5==='COMPRA, VENTA Y ARRIENDO DE BIENES INMUEBLES'}selected{/if} value="COMPRA, VENTA Y ARRIENDO DE BIENES INMUEBLES">COMPRA, VENTA Y ARRIENDO DE BIENES INMUEBLES</option>
                        <option {if $attribute5==='COMUNICACIONES'}selected{/if} value="COMUNICACIONES">COMUNICACIONES</option>
                        <option {if $attribute5==='COMUNIDAD ADMINISTRACION'}selected{/if} value="COMUNIDAD ADMINISTRACION">COMUNIDAD ADMINISTRACION</option>
                        <option {if $attribute5==='COMUNIDAD COMERCIAL'}selected{/if} value="COMUNIDAD COMERCIAL">COMUNIDAD COMERCIAL</option>
                        <option {if $attribute5==='COMUNIDAD CONDOMINIO'}selected{/if} value="COMUNIDAD CONDOMINIO">COMUNIDAD CONDOMINIO</option>
                        <option {if $attribute5==='COMUNIDAD EDIFICIO'}selected{/if} value="COMUNIDAD EDIFICIO">COMUNIDAD EDIFICIO</option>
                        <option {if $attribute5==='COMUNIDAD ELECTRICA'}selected{/if} value="COMUNIDAD ELECTRICA">COMUNIDAD ELECTRICA</option>
                        <option {if $attribute5==='COMUNIDAD HABITACIONAL'}selected{/if} value="COMUNIDAD HABITACIONAL">COMUNIDAD HABITACIONAL</option>
                        <option {if $attribute5==='COMUNIDAD INMOBILIARIA'}selected{/if} value="COMUNIDAD INMOBILIARIA">COMUNIDAD INMOBILIARIA</option>
                        <option {if $attribute5==='COMUNIDAD INVERSIONES'}selected{/if} value="COMUNIDAD INVERSIONES">COMUNIDAD INVERSIONES</option>
                        <option {if $attribute5==='COMUNIDAD JARDINERIA'}selected{/if} value="COMUNIDAD JARDINERIA">COMUNIDAD JARDINERIA</option>
                        <option {if $attribute5==='COMUNIDAD MEDICOS'}selected{/if} value="COMUNIDAD MEDICOS">COMUNIDAD MEDICOS</option>
                        <option {if $attribute5==='COMUNIDAD TIENDAS'}selected{/if} value="COMUNIDAD TIENDAS">COMUNIDAD TIENDAS</option>
                        <option {if $attribute5==='COMUNIDAD UNIVERSIDAD'}selected{/if} value="COMUNIDAD UNIVERSIDAD">COMUNIDAD UNIVERSIDAD</option>
                        <option {if $attribute5==='CONCEJO DE ADMINISTRACION'}selected{/if} value="CONCEJO DE ADMINISTRACION">CONCEJO DE ADMINISTRACION</option>
                        <option {if $attribute5==='CONCESIONARIA'}selected{/if} value="CONCESIONARIA">CONCESIONARIA</option>
                        <option {if $attribute5==='CONDOMINIO'}selected{/if} value="CONDOMINIO">CONDOMINIO</option>
                        <option {if $attribute5==='CONDOMINIO HABITACIONAL'}selected{/if} value="CONDOMINIO HABITACIONAL">CONDOMINIO HABITACIONAL</option>
                        <option {if $attribute5==='CONGREGACION PROVINCIAL'}selected{/if} value="CONGREGACION PROVINCIAL">CONGREGACION PROVINCIAL</option>
                        <option {if $attribute5==='CONJUNTO HABITACIONAL'}selected{/if} value="CONJUNTO HABITACIONAL">CONJUNTO HABITACIONAL</option>
                        <option {if $attribute5==='CONSEJO DE ADMINISTRACION DE EDIFICIOS Y CONDOMIN'}selected{/if} value="CONSEJO DE ADMINISTRACION DE EDIFICIOS Y CONDOMIN">CONSEJO DE ADMINISTRACION DE EDIFICIOS Y CONDOMIN</option>
                        <option {if $attribute5==='CONSTRUCCION'}selected{/if} value="CONSTRUCCION">CONSTRUCCION</option>
                        <option {if $attribute5==='CONSTRUCCION DE EDIFICIOS - VIVIENDAS - HOTELES'}selected{/if} value="CONSTRUCCION DE EDIFICIOS - VIVIENDAS - HOTELES">CONSTRUCCION DE EDIFICIOS - VIVIENDAS - HOTELES</option>
                        <option {if $attribute5==='CONSTRUCCION DE EDIFICIOS COMPLETOS O DE PARTES DE'}selected{/if} value="CONSTRUCCION DE EDIFICIOS COMPLETOS O DE PARTES DE">CONSTRUCCION DE EDIFICIOS COMPLETOS O DE PARTES DE</option>
                        <option {if $attribute5==='CONSTRUCCION DE EDIFICIOS COMPLETOS O DE PARTES DE'}selected{/if} value="CONSTRUCCION DE EDIFICIOS COMPLETOS O DE PARTES DE">CONSTRUCCION DE EDIFICIOS COMPLETOS O DE PARTES DE</option>
                        <option {if $attribute5==='CONTRATISTA DE SERV. PARA LA ADM. '}selected{/if} value="CONTRATISTA DE SERV. PARA LA ADM. ">CONTRATISTA DE SERV. PARA LA ADM. </option>
                        <option {if $attribute5==='CORREDORES DE PROPIEDADES'}selected{/if} value="CORREDORES DE PROPIEDADES">CORREDORES DE PROPIEDADES</option>
                        <option {if $attribute5==='CULTIVOS Y VENTA DE SALMONES'}selected{/if} value="CULTIVOS Y VENTA DE SALMONES">CULTIVOS Y VENTA DE SALMONES</option>
                        <option {if $attribute5==='DESARROLLO Y PARTC DE HOTELERIA - GASTRONOMIA Y AC'}selected{/if} value="DESARROLLO Y PARTC DE HOTELERIA - GASTRONOMIA Y AC">DESARROLLO Y PARTC DE HOTELERIA - GASTRONOMIA Y AC</option>
                        <option {if $attribute5==='DISCOTECA, SALA DE BAILE, CABARET'}selected{/if} value="DISCOTECA, SALA DE BAILE, CABARET">DISCOTECA, SALA DE BAILE, CABARET</option>
                        <option {if $attribute5==='DIST. MAY. Y MINOR. DE PROD.'}selected{/if} value="DIST. MAY. Y MINOR. DE PROD.">DIST. MAY. Y MINOR. DE PROD.</option>
                        <option {if $attribute5==='DISTRIBUCION DE GAS'}selected{/if} value="DISTRIBUCION DE GAS">DISTRIBUCION DE GAS</option>
                        <option {if $attribute5==='DISTRIBUCION Y VENTA DE ENERGIA DECTRICA Y VENTA D'}selected{/if} value="DISTRIBUCION Y VENTA DE ENERGIA DECTRICA Y VENTA D">DISTRIBUCION Y VENTA DE ENERGIA DECTRICA Y VENTA D</option>
                        <option {if $attribute5==='EDIFICIO'}selected{/if} value="EDIFICIO">EDIFICIO</option>
                        <option {if $attribute5==='EDUCACION'}selected{/if} value="EDUCACION">EDUCACION</option>
                        <option {if $attribute5==='EJECUCION Y CONSTRUCCION OBRAS CIVILES'}selected{/if} value="EJECUCION Y CONSTRUCCION OBRAS CIVILES">EJECUCION Y CONSTRUCCION OBRAS CIVILES</option>
                        <option {if $attribute5==='ELABORACION DE ALIMENTOS PREPARADOS PARA ANIMALES'}selected{/if} value="ELABORACION DE ALIMENTOS PREPARADOS PARA ANIMALES">ELABORACION DE ALIMENTOS PREPARADOS PARA ANIMALES</option>
                        <option {if $attribute5==='ENSENANZA PRIMARIA, SECUNDARIA CIENTIFICO HUMANIST'}selected{/if} value="ENSENANZA PRIMARIA, SECUNDARIA CIENTIFICO HUMANIST">ENSENANZA PRIMARIA, SECUNDARIA CIENTIFICO HUMANIST</option>
                        <option {if $attribute5==='ENSENANZA SUPERIOR EN UNIVERSIDADES PUBLICAS'}selected{/if} value="ENSENANZA SUPERIOR EN UNIVERSIDADES PUBLICAS">ENSENANZA SUPERIOR EN UNIVERSIDADES PUBLICAS</option>
                        <option {if $attribute5==='ENTRETENCION'}selected{/if} value="ENTRETENCION">ENTRETENCION</option>
                        <option {if $attribute5==='ESTABLECIMIENTO MEDICO'}selected{/if} value="ESTABLECIMIENTO MEDICO">ESTABLECIMIENTO MEDICO</option>
                        <option {if $attribute5==='ESTABLECIMIENTOS DE ENSENANZA PREUNIVERSITARIA'}selected{/if} value="ESTABLECIMIENTOS DE ENSENANZA PREUNIVERSITARIA">ESTABLECIMIENTOS DE ENSENANZA PREUNIVERSITARIA</option>
                        <option {if $attribute5==='ESTABLECIMIENTOS DE ENSENANZA PRIMARIA'}selected{/if} value="ESTABLECIMIENTOS DE ENSENANZA PRIMARIA">ESTABLECIMIENTOS DE ENSENANZA PRIMARIA</option>
                        <option {if $attribute5==='ESTABLECIMIENTOS MEDICOS DE ATENCION AMBULATORIA'}selected{/if} value="ESTABLECIMIENTOS MEDICOS DE ATENCION AMBULATORIA">ESTABLECIMIENTOS MEDICOS DE ATENCION AMBULATORIA</option>
                        <option {if $attribute5==='ESTACIONAMIENTO DE VEHICULOS Y PARQUIMETROS'}selected{/if} value="ESTACIONAMIENTO DE VEHICULOS Y PARQUIMETROS">ESTACIONAMIENTO DE VEHICULOS Y PARQUIMETROS</option>
                        <option {if $attribute5==='ESTACIONAMIENTOS'}selected{/if} value="ESTACIONAMIENTOS">ESTACIONAMIENTOS</option>
                        <option {if $attribute5==='EXPLOTACION DE AEROPUERTO'}selected{/if} value="EXPLOTACION DE AEROPUERTO">EXPLOTACION DE AEROPUERTO</option>
                        <option {if $attribute5==='EXPLOTACION DE AEROPUERTOS Y ESTACIONAMIENTOS'}selected{/if} value="EXPLOTACION DE AEROPUERTOS Y ESTACIONAMIENTOS">EXPLOTACION DE AEROPUERTOS Y ESTACIONAMIENTOS</option>
                        <option {if $attribute5==='EXPLOTACION DE OTRAS MINAS Y CANTERAS      '}selected{/if} value="EXPLOTACION DE OTRAS MINAS Y CANTERAS      ">EXPLOTACION DE OTRAS MINAS Y CANTERAS      </option>
                        <option {if $attribute5==='FAB PROD PRIMARIOS DE MET PRECIOSOS Y OTROS NO'}selected{/if} value="FAB PROD PRIMARIOS DE MET PRECIOSOS Y OTROS NO">FAB PROD PRIMARIOS DE MET PRECIOSOS Y OTROS NO</option>
                        <option {if $attribute5==='FABR. Y DIST. PROD. LACTEOS VTAS. INSUMOS AGRICOLA'}selected{/if} value="FABR. Y DIST. PROD. LACTEOS VTAS. INSUMOS AGRICOLA">FABR. Y DIST. PROD. LACTEOS VTAS. INSUMOS AGRICOLA</option>
                        <option {if $attribute5==='FABRICA DE PANELES DE MADERA, PRODUCCION , VENTA'}selected{/if} value="FABRICA DE PANELES DE MADERA, PRODUCCION , VENTA">FABRICA DE PANELES DE MADERA, PRODUCCION , VENTA</option>
                        <option {if $attribute5==='FABRICACION  DE PULPA DE MADERA'}selected{/if} value="FABRICACION  DE PULPA DE MADERA">FABRICACION  DE PULPA DE MADERA</option>
                        <option {if $attribute5==='FABRICACION DE PAPELES Y CARTONES'}selected{/if} value="FABRICACION DE PAPELES Y CARTONES">FABRICACION DE PAPELES Y CARTONES</option>
                        <option {if $attribute5==='FABRICACION DE PLASTICOS EN FORMAS PRIMARIAS Y DE '}selected{/if} value="FABRICACION DE PLASTICOS EN FORMAS PRIMARIAS Y DE ">FABRICACION DE PLASTICOS EN FORMAS PRIMARIAS Y DE </option>
                        <option {if $attribute5==='FABRICACION DE PRODUCTOS DE REFINACION DE PETROLEO'}selected{/if} value="FABRICACION DE PRODUCTOS DE REFINACION DE PETROLEO">FABRICACION DE PRODUCTOS DE REFINACION DE PETROLEO</option>
                        <option {if $attribute5==='FABRICACION Y VENTA DE CELULOSA Y PAPEL'}selected{/if} value="FABRICACION Y VENTA DE CELULOSA Y PAPEL">FABRICACION Y VENTA DE CELULOSA Y PAPEL</option>
                        <option {if $attribute5==='FABRICACION,REPARACION,ELEBORACION ,COMERCIALIZACI'}selected{/if} value="FABRICACION,REPARACION,ELEBORACION ,COMERCIALIZACI">FABRICACION,REPARACION,ELEBORACION ,COMERCIALIZACI</option>
                        <option {if $attribute5==='FISCAL'}selected{/if} value="FISCAL">FISCAL</option>
                        <option {if $attribute5==='FONDO DE INVERSION'}selected{/if} value="FONDO DE INVERSION">FONDO DE INVERSION</option>
                        <option {if $attribute5==='FONDOS Y SOCIEDADES DE INVERSION Y ENTIDADES FINAN'}selected{/if} value="FONDOS Y SOCIEDADES DE INVERSION Y ENTIDADES FINAN">FONDOS Y SOCIEDADES DE INVERSION Y ENTIDADES FINAN</option>
                        <option {if $attribute5==='GENERACION DE ELECTRICIDAD'}selected{/if} value="GENERACION DE ELECTRICIDAD">GENERACION DE ELECTRICIDAD</option>
                        <option {if $attribute5==='GENERACION EN OTRAS CENTRALES TERMOELECTRICA'}selected{/if} value="GENERACION EN OTRAS CENTRALES TERMOELECTRICA">GENERACION EN OTRAS CENTRALES TERMOELECTRICA</option>
                        <option {if $attribute5==='GENERACION, DISTRIBUCION Y TRANSMISION DE ENERGIA ELECTRICA'}selected{/if} value="GENERACION, DISTRIBUCION Y TRANSMISION DE ENERGIA ELECTRICA">GENERACION, DISTRIBUCION Y TRANSMISION DE ENERGIA ELECTRICA</option>
                        <option {if $attribute5==='GOBIERNO CENTRAL '}selected{/if} value="GOBIERNO CENTRAL ">GOBIERNO CENTRAL </option>
                        <option {if $attribute5==='GRANDES ESTABLECIMIENTOS (VENTAS DE ALIMENTOS) HIP'}selected{/if} value="GRANDES ESTABLECIMIENTOS (VENTAS DE ALIMENTOS) HIP">GRANDES ESTABLECIMIENTOS (VENTAS DE ALIMENTOS) HIP</option>
                        <option {if $attribute5==='GRANDES TIENDAS'}selected{/if} value="GRANDES TIENDAS">GRANDES TIENDAS</option>
                        <option {if $attribute5==='HIPODROMOS'}selected{/if} value="HIPODROMOS">HIPODROMOS</option>
                        <option {if $attribute5==='HOSPITAL'}selected{/if} value="HOSPITAL">HOSPITAL</option>
                        <option {if $attribute5==='HOSPITAL - CLINICAS Y OTROS'}selected{/if} value="HOSPITAL - CLINICAS Y OTROS">HOSPITAL - CLINICAS Y OTROS</option>
                        <option {if $attribute5==='HOSPITALES Y CLINICAS'}selected{/if} value="HOSPITALES Y CLINICAS">HOSPITALES Y CLINICAS</option>
                        <option {if $attribute5==='HOTELERIA'}selected{/if} value="HOTELERIA">HOTELERIA</option>
                        <option {if $attribute5==='HOTELERIA Y TURISMO'}selected{/if} value="HOTELERIA Y TURISMO">HOTELERIA Y TURISMO</option>
                        <option {if $attribute5==='HOTELES, ASESORIAS, RESTORANTES, BAR, INVERSIONES'}selected{/if} value="HOTELES, ASESORIAS, RESTORANTES, BAR, INVERSIONES">HOTELES, ASESORIAS, RESTORANTES, BAR, INVERSIONES</option>
                        <option {if $attribute5==='HOTELES, RESTAURANTES Y OTROS'}selected{/if} value="HOTELES, RESTAURANTES Y OTROS">HOTELES, RESTAURANTES Y OTROS</option>
                        <option {if $attribute5==='IMPORTACIONES'}selected{/if} value="IMPORTACIONES">IMPORTACIONES</option>
                        <option {if $attribute5==='IMPORTADORA DE ACEROS'}selected{/if} value="IMPORTADORA DE ACEROS">IMPORTADORA DE ACEROS</option>
                        <option {if $attribute5==='INDUSTRIAL'}selected{/if} value="INDUSTRIAL">INDUSTRIAL</option>
                        <option {if $attribute5==='INGENIERIA EN CONSTRUCCION'}selected{/if} value="INGENIERIA EN CONSTRUCCION">INGENIERIA EN CONSTRUCCION</option>
                        <option {if $attribute5==='INGENIERIA Y CONSTRUCCION'}selected{/if} value="INGENIERIA Y CONSTRUCCION">INGENIERIA Y CONSTRUCCION</option>
                        <option {if $attribute5==='INMOBILIARIA                                      '}selected{/if} value="INMOBILIARIA                                      ">INMOBILIARIA                                      </option>
                        <option {if $attribute5==='INMOBILIARIA CONSTRUCCIONES -  ASES INMOBILIARIAS'}selected{/if} value="INMOBILIARIA CONSTRUCCIONES -  ASES INMOBILIARIAS">INMOBILIARIA CONSTRUCCIONES -  ASES INMOBILIARIAS</option>
                        <option {if $attribute5==='INMOBILIARIA E INVERSIONES '}selected{/if} value="INMOBILIARIA E INVERSIONES ">INMOBILIARIA E INVERSIONES </option>
                        <option {if $attribute5==='INMOBILIARIA Y HOTELERA'}selected{/if} value="INMOBILIARIA Y HOTELERA">INMOBILIARIA Y HOTELERA</option>
                        <option {if $attribute5==='INMUEBLES AMOBLADOS'}selected{/if} value="INMUEBLES AMOBLADOS">INMUEBLES AMOBLADOS</option>
                        <option {if $attribute5==='INSTALACIONES'}selected{/if} value="INSTALACIONES">INSTALACIONES</option>
                        <option {if $attribute5==='INSTITUCION FINANCIERA'}selected{/if} value="INSTITUCION FINANCIERA">INSTITUCION FINANCIERA</option>
                        <option {if $attribute5==='INSTITUCION PUBLICA'}selected{/if} value="INSTITUCION PUBLICA">INSTITUCION PUBLICA</option>
                        <option {if $attribute5==='INSTITUTO PROFESIONAL'}selected{/if} value="INSTITUTO PROFESIONAL">INSTITUTO PROFESIONAL</option>
                        <option {if $attribute5==='INVERSIONES'}selected{/if} value="INVERSIONES">INVERSIONES</option>
                        <option {if $attribute5==='INVERSIONES E INMOBILIARIA'}selected{/if} value="INVERSIONES E INMOBILIARIA">INVERSIONES E INMOBILIARIA</option>
                        <option {if $attribute5==='INVERSIONES EN BIENES'}selected{/if} value="INVERSIONES EN BIENES">INVERSIONES EN BIENES</option>
                        <option {if $attribute5==='INVESTIGACIONES Y DESARROLLO EXPERIMENTAL CIENCIAS'}selected{/if} value="INVESTIGACIONES Y DESARROLLO EXPERIMENTAL CIENCIAS">INVESTIGACIONES Y DESARROLLO EXPERIMENTAL CIENCIAS</option>
                        <option {if $attribute5==='LABORATORIO CLINICO'}selected{/if} value="LABORATORIO CLINICO">LABORATORIO CLINICO</option>
                        <option {if $attribute5==='LABORATORIO FARMACEUTICO'}selected{/if} value="LABORATORIO FARMACEUTICO">LABORATORIO FARMACEUTICO</option>
                        <option {if $attribute5==='LINEA AEREA'}selected{/if} value="LINEA AEREA">LINEA AEREA</option>
                        <option {if $attribute5==='MINERIA'}selected{/if} value="MINERIA">MINERIA</option>
                        <option {if $attribute5==='MINISTERIO BIENES NACIONALES'}selected{/if} value="MINISTERIO BIENES NACIONALES">MINISTERIO BIENES NACIONALES</option>
                        <option {if $attribute5==='MUNICIPALIDAD'}selected{/if} value="MUNICIPALIDAD">MUNICIPALIDAD</option>
                        <option {if $attribute5==='MUTUAL'}selected{/if} value="MUTUAL">MUTUAL</option>
                        <option {if $attribute5==='OBRAS CIVILES Y TRANSPORTE DE CARGA'}selected{/if} value="OBRAS CIVILES Y TRANSPORTE DE CARGA">OBRAS CIVILES Y TRANSPORTE DE CARGA</option>
                        <option {if $attribute5==='OBRAS DE INGENIERIA'}selected{/if} value="OBRAS DE INGENIERIA">OBRAS DE INGENIERIA</option>
                        <option {if $attribute5==='OBRAS MENORES DE CONSTRUCCION'}selected{/if} value="OBRAS MENORES DE CONSTRUCCION">OBRAS MENORES DE CONSTRUCCION</option>
                        <option {if $attribute5==='OBRAS MENORES EN CONSTRUCCION (CONTRATISTAS, ALBAN'}selected{/if} value="OBRAS MENORES EN CONSTRUCCION (CONTRATISTAS, ALBAN">OBRAS MENORES EN CONSTRUCCION (CONTRATISTAS, ALBAN</option>
                        <option {if $attribute5==='OPTICA'}selected{/if} value="OPTICA">OPTICA</option>
                        <option {if $attribute5==='ORDEN RELIGIOSA'}selected{/if} value="ORDEN RELIGIOSA">ORDEN RELIGIOSA</option>
                        <option {if $attribute5==='ORGANIZACION INTERNACIONAL'}selected{/if} value="ORGANIZACION INTERNACIONAL">ORGANIZACION INTERNACIONAL</option>
                        <option {if $attribute5==='OTRAS ACTIVIDADES DE SERVICIOS DE APOYO A LAS EMPR'}selected{/if} value="OTRAS ACTIVIDADES DE SERVICIOS DE APOYO A LAS EMPR">OTRAS ACTIVIDADES DE SERVICIOS DE APOYO A LAS EMPR</option>
                        <option {if $attribute5==='OTRAS ACTIVIDADES EMPRESARIALES N.C.P.'}selected{/if} value="OTRAS ACTIVIDADES EMPRESARIALES N.C.P.">OTRAS ACTIVIDADES EMPRESARIALES N.C.P.</option>
                        <option {if $attribute5==='OTROS CULTIVOS N.C.P.'}selected{/if} value="OTROS CULTIVOS N.C.P.">OTROS CULTIVOS N.C.P.</option>
                        <option {if $attribute5==='OTROS'}selected{/if} value="OTROS">OTROS</option>
                        <option {if $attribute5==='PELUQUERIAS Y SALONES DE BELLEZA'}selected{/if} value="PELUQUERIAS Y SALONES DE BELLEZA">PELUQUERIAS Y SALONES DE BELLEZA</option>
                        <option {if $attribute5==='PLANES DE SEGUROS DE VIDA'}selected{/if} value="PLANES DE SEGUROS DE VIDA">PLANES DE SEGUROS DE VIDA</option>
                        <option {if $attribute5==='PLANES DE SEGUROS GENERALES'}selected{/if} value="PLANES DE SEGUROS GENERALES">PLANES DE SEGUROS GENERALES</option>
                        <option {if $attribute5==='PRESTACION DE SERVICIOS'}selected{/if} value="PRESTACION DE SERVICIOS">PRESTACION DE SERVICIOS</option>
                        <option {if $attribute5==='PRODUCCION DE EVENTOS'}selected{/if} value="PRODUCCION DE EVENTOS">PRODUCCION DE EVENTOS</option>
                        <option {if $attribute5==='PUBLICIDAD Y PRODUCCIONES DE EVENTOS'}selected{/if} value="PUBLICIDAD Y PRODUCCIONES DE EVENTOS">PUBLICIDAD Y PRODUCCIONES DE EVENTOS</option>
                        <option {if $attribute5==='RELIGIOSO'}selected{/if} value="RELIGIOSO">RELIGIOSO</option>
                        <option {if $attribute5==='REPARACION DE AERONAVES, ARRIENDO DE INSTALACIONES'}selected{/if} value="REPARACION DE AERONAVES, ARRIENDO DE INSTALACIONES">REPARACION DE AERONAVES, ARRIENDO DE INSTALACIONES</option>
                        <option {if $attribute5==='REPARTICION ESTATAL'}selected{/if} value="REPARTICION ESTATAL">REPARTICION ESTATAL</option>
                        <option {if $attribute5==='REPARTICION FISCAL'}selected{/if} value="REPARTICION FISCAL">REPARTICION FISCAL</option>
                        <option {if $attribute5==='REPARTICION PUBLICA'}selected{/if} value="REPARTICION PUBLICA">REPARTICION PUBLICA</option>
                        <option {if $attribute5==='RESTAURANT'}selected{/if} value="RESTAURANT">RESTAURANT</option>
                        <option {if $attribute5==='SALUD'}selected{/if} value="SALUD">SALUD</option>
                        <option {if $attribute5==='SERV INMOB - CORRETAJE - ASESORIAS FINANCIERAS '}selected{/if} value="SERV INMOB - CORRETAJE - ASESORIAS FINANCIERAS ">SERV INMOB - CORRETAJE - ASESORIAS FINANCIERAS </option>
                        <option {if $attribute5==='SERVICIO DE ALIMENTACION ASEO LAVANDERIA Y MANTENC'}selected{/if} value="SERVICIO DE ALIMENTACION ASEO LAVANDERIA Y MANTENC">SERVICIO DE ALIMENTACION ASEO LAVANDERIA Y MANTENC</option>
                        <option {if $attribute5==='SERVICIO DE DESARROLLO SOCIAL'}selected{/if} value="SERVICIO DE DESARROLLO SOCIAL">SERVICIO DE DESARROLLO SOCIAL</option>
                        <option {if $attribute5==='SERVICIOS'}selected{/if} value="SERVICIOS">SERVICIOS</option>
                        <option {if $attribute5==='SERVICIOS DE ADMINISTRACION'}selected{/if} value="SERVICIOS DE ADMINISTRACION">SERVICIOS DE ADMINISTRACION</option>
                        <option {if $attribute5==='SERVICIOS DE ALIMENTACION'}selected{/if} value="SERVICIOS DE ALIMENTACION">SERVICIOS DE ALIMENTACION</option>
                        <option {if $attribute5==='SERVICIOS DE ASEO, JARDINERIA, MANTENCION DE AREAS'}selected{/if} value="SERVICIOS DE ASEO, JARDINERIA, MANTENCION DE AREAS">SERVICIOS DE ASEO, JARDINERIA, MANTENCION DE AREAS</option>
                        <option {if $attribute5==='SERVICIOS DE MANTENCION'}selected{/if} value="SERVICIOS DE MANTENCION">SERVICIOS DE MANTENCION</option>
                        <option {if $attribute5==='SERVICIOS DE SALUD DENTAL'}selected{/if} value="SERVICIOS DE SALUD DENTAL">SERVICIOS DE SALUD DENTAL</option>
                        <option {if $attribute5==='SERVICIOS GERENCIALES YADMINISTRATIVOS'}selected{/if} value="SERVICIOS GERENCIALES YADMINISTRATIVOS">SERVICIOS GERENCIALES YADMINISTRATIVOS</option>
                        <option {if $attribute5==='SERVICIOS INMOBILIARIOS Y ASESORIAS'}selected{/if} value="SERVICIOS INMOBILIARIOS Y ASESORIAS">SERVICIOS INMOBILIARIOS Y ASESORIAS</option>
                        <option {if $attribute5==='SERVICIOS MEDICOS'}selected{/if} value="SERVICIOS MEDICOS">SERVICIOS MEDICOS</option>
                        <option {if $attribute5==='SERVICIOS PROFESIONALES INDEPENDIENTES'}selected{/if} value="SERVICIOS PROFESIONALES INDEPENDIENTES">SERVICIOS PROFESIONALES INDEPENDIENTES</option>
                        <option {if $attribute5==='SOCIEDAD DE INVERSIONES'}selected{/if} value="SOCIEDAD DE INVERSIONES">SOCIEDAD DE INVERSIONES</option>
                        <option {if $attribute5==='SOCIEDADES DE INVERSION Y RENTISTA'}selected{/if} value="SOCIEDADES DE INVERSION Y RENTISTA">SOCIEDADES DE INVERSION Y RENTISTA</option>
                        <option {if $attribute5==='SUPERMERCADOS'}selected{/if} value="SUPERMERCADOS">SUPERMERCADOS</option>
                        <option {if $attribute5==='TERMINAL DE BUSES'}selected{/if} value="TERMINAL DE BUSES">TERMINAL DE BUSES</option>
                        <option {if $attribute5==='TRANSMISION Y DISTRIBUCION DE ENERGIA ELECTRICA'}selected{/if} value="TRANSMISION Y DISTRIBUCION DE ENERGIA ELECTRICA">TRANSMISION Y DISTRIBUCION DE ENERGIA ELECTRICA</option>
                        <option {if $attribute5==='TRANSPORTE'}selected{/if} value="TRANSPORTE">TRANSPORTE</option>
                        <option {if $attribute5==='TRANSPORTE AEREO'}selected{/if} value="TRANSPORTE AEREO">TRANSPORTE AEREO</option>
                        <option {if $attribute5==='TRANSPORTE DE CARGA'}selected{/if} value="TRANSPORTE DE CARGA">TRANSPORTE DE CARGA</option>
                        <option {if $attribute5==='TRANSPORTE FERROVIARIO DE PASAJEROS'}selected{/if} value="TRANSPORTE FERROVIARIO DE PASAJEROS">TRANSPORTE FERROVIARIO DE PASAJEROS</option>
                        <option {if $attribute5==='TRANSPORTE URBANO DE PASAJEROS'}selected{/if} value="TRANSPORTE URBANO DE PASAJEROS">TRANSPORTE URBANO DE PASAJEROS</option>
                        <option {if $attribute5==='TRANSPORTE VERTICAL'}selected{/if} value="TRANSPORTE VERTICAL">TRANSPORTE VERTICAL</option>
                        <option {if $attribute5==='UNIVERSIDAD'}selected{/if} value="UNIVERSIDAD">UNIVERSIDAD</option>
                        <option {if $attribute5==='VENTA AL POR MAYOR DE MATERIALES DE CONSTRUCCION'}selected{/if} value="VENTA AL POR MAYOR DE MATERIALES DE CONSTRUCCION">VENTA AL POR MAYOR DE MATERIALES DE CONSTRUCCION</option>
                        <option {if $attribute5==='VENTA AL POR MAYOR DE PRODUCTOS TEXTILES, PRENDAS'}selected{/if} value="VENTA AL POR MAYOR DE PRODUCTOS TEXTILES, PRENDAS">VENTA AL POR MAYOR DE PRODUCTOS TEXTILES, PRENDAS</option>
                        <option {if $attribute5==='VENTA AL POR MENOR EN COMERCIOS DE ALIME'}selected{/if} value="VENTA AL POR MENOR EN COMERCIOS DE ALIME">VENTA AL POR MENOR EN COMERCIOS DE ALIME</option>
                        <option {if $attribute5==='VENTA DE ALIMENTOS'}selected{/if} value="VENTA DE ALIMENTOS">VENTA DE ALIMENTOS</option>
                        <option {if $attribute5==='VENTA DE AUTOMOVILES Y PARTES'}selected{/if} value="VENTA DE AUTOMOVILES Y PARTES">VENTA DE AUTOMOVILES Y PARTES</option>
                        <option {if $attribute5==='VENTA DE PRENDAS DE VESTIR'}selected{/if} value="VENTA DE PRENDAS DE VESTIR">VENTA DE PRENDAS DE VESTIR</option>
                        <option {if $attribute5==='VENTA DE REPUESTOS DE ASCENSORES'}selected{/if} value="VENTA DE REPUESTOS DE ASCENSORES">VENTA DE REPUESTOS DE ASCENSORES</option>
                        <option {if $attribute5==='VENTAS DE SERVICIOS Y MAQUINARIAS'}selected{/if} value="VENTAS DE SERVICIOS Y MAQUINARIAS">VENTAS DE SERVICIOS Y MAQUINARIAS</option>
                    </select>
                </div>
            </div>
        {/if}


        {if ($country=='CO')}
        <div class="col-6">
            <small id="errorCoExtAttribute9" class="form-text text-danger">&nbsp;{$errorCoExtAttribute9}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Nature (coExtAttribute9)
                        <span class="flag-icon flag-icon-co mr-1 ml-1" title="CO"></span>
                    </div>
                </div>
                <select class="form-control" id="coExtAttribute9" name="coExtAttribute9">
                    <option {if $coExtAttribute9==='J'}selected{/if} value="J">LEGAL ENTITY</option>
                    <option {if $coExtAttribute9==='N'}selected{/if} value="N">NATURAL PERSON</option>
                </select>
            </div>
        </div>
        <div class="col-6 my-1">
            <small id="errorGlobalAttribute10" class="form-text text-danger">&nbsp;{$errorGlobalAttribute10}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Primary ID Type (globalAttribute10)
                        <span class="flag-icon flag-icon-co mr-1 ml-1" title="CO"></span>
                    </div>
                </div>
                <select class="form-control" id="globalAttribute10" name="globalAttribute10">
                    <option value="">--</option>
                    <option {if $globalAttribute10==='FOREIGN_ENTITY'}selected{/if}>FOREIGN_ENTITY</option>
                    <option {if $globalAttribute10==='INDIVIDUAL'}selected{/if}>INDIVIDUAL</option>
                    <option {if $globalAttribute10==='LEGAL_ENTITY'}selected{/if}>LEGAL_ENTITY</option>
                    <option {if $globalAttribute10==='MERCHANT'}selected{/if}>MERCHANT</option>

                </select>
            </div>
        </div>
        <div class="col-6 my-1">
            <small id="errorCoExtAttribute1" class="form-text text-danger">&nbsp;{$errorCoExtAttribute1}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Supplier Tax Regime Type (coExtAttribute1)
                        <span class="flag-icon flag-icon-co mr-1 ml-1" title="CO"></span>
                    </div>
                </div>
                <select class="form-control" id="coExtAttribute1" name="coExtAttribute1">
                    <option {if $coExtAttribute1==='Simplified Regime'}selected{/if} value='Simplified Regime'>Simplified Regime</option>
                    <option {if $coExtAttribute1==='No Responsible'}selected{/if} value='No Responsible'>No Responsible</option>
                </select>
            </div>
        </div>
        <div class="col-6 my-1">
            <small id="errorGlobalAttribute10" class="form-text text-danger">&nbsp;{$errorCoExtAttribute2}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Document Type (CO) (coExtAttribute2)
                        <span class="flag-icon flag-icon-co mr-1 ml-1" title="CO"></span>
                    </div>
                </div>
                <select class="form-control" id="coExtAttribute2" name="coExtAttribute2">
                    <option {if $coExtAttribute2==='31'}selected{/if} value='31'>31 - NIT</option>
                    <option {if $coExtAttribute2==='11'}selected{/if} value='11'>11 - Birth Civil Registration</option>
                    <option {if $coExtAttribute2==='14'}selected{/if} value='14'>14 - Certificate of the Registrar for illiquid companies</option>
                    <option {if $coExtAttribute2==='13'}selected{/if} value='13'>13 - Citizenship Identification</option>
                    <option {if $coExtAttribute2==='46'}selected{/if} value='46'>46 - Diplomatic passport</option>
                    <option {if $coExtAttribute2==='43'}selected{/if} value='43'>43 - Document defined for external information</option>
                    <option {if $coExtAttribute2==='21'}selected{/if} value='21'>21 - Foreign Card</option>
                    <option {if $coExtAttribute2==='42'}selected{/if} value='42'>42 - Foreign Document Type</option>
                    <option {if $coExtAttribute2==='22'}selected{/if} value='22'>22 - Foreign Identification</option>
                    <option {if $coExtAttribute2==='44'}selected{/if} value='44'>44 - Foreign legal entity identification document</option>
                    <option {if $coExtAttribute2==='12'}selected{/if} value='12'>12 - Identification Card</option>
                    <option {if $coExtAttribute2==='33'}selected{/if} value='33'>33 - Identification of different foreign NIT assigned DIAN</option>
                    <option {if $coExtAttribute2==='15'}selected{/if} value='15'>15 - Inheritance illiquid issued by the notary</option>
                    <option {if $coExtAttribute2==='41'}selected{/if} value='41'>41 - Passport</option>
                </select>
            </div>
        </div>
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorCoExtAttribute6" class="form-text text-danger">&nbsp;{$errorCoExtAttribute6}</small>
                <label class="sr-only" for="inlineFormCoExtAttribute8">Simplified Regime Start (coExtAttribute6)</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Simplified Regime Start (coExtAttribute6)</div>
                    </div>
                    <input class="form-control" id="coExtAttribute6" name="coExtAttribute6" value="01-01-2018" readonly/>
                </div>
            </div>
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorCoExtAttribute7" class="form-text text-danger">&nbsp;{$errorCoExtAttribute7}</small>
                <label class="sr-only" for="inlineFormCoExtAttribute7">Simplified Regime End (coExtAttribute7)</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Simplified Regime End (coExtAttribute7)</div>
                    </div>
                    <input class="form-control" id="coExtAttribute7" name="coExtAttribute7" value="31-12-2049" readonly/>
                </div>
            </div>

        {/if}


        {/if}
    </div>

    {if ($country=='PE')}
    <div id="nameFieldsPE" class="form-row align-items-center">
        <div class="col-3 my-1">
            <small id="errorFirstNamePE" class="form-text text-danger">&nbsp;{$errorFirstNamePE}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">First Name&nbsp;
                        <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                    </div>
                </div>
                <input class="form-control" id="firstNamePE" name="firstNamePE" value="{$firstNamePE}"/>
            </div>
        </div>
        <div class="col-3 my-1">
            <small id="errorMiddleNamePE" class="form-text text-danger">&nbsp;{$errorMiddleNamePE}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Middle Name&nbsp;
                        <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                    </div>
                </div>
                <input class="form-control" id="middleNamePE" name="middleNamePE" value="{$middleNamePE}"/>
            </div>
        </div>
        <div class="col-3 my-1">
            <small id="errorFirstLastNamePE" class="form-text text-danger">&nbsp;{$errorFirstLastNamePE}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">First Last Name&nbsp;
                        <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                    </div>
                </div>
                <input class="form-control" id="lasNamePE" name="lasNamePE" value="{$lasNamePE}"/>
            </div>
        </div>
        <div class="col-3 my-1">
            <small id="errorSecondLasNamePE" class="form-text text-danger">&nbsp;{$errorSecondLasNamePE}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Second Last Name&nbsp;
                        <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                    </div>
                </div>
                <input class="form-control" id="secondLasNamePE" name="secondLasNamePE" value="{$secondLasNamePE}"/>
            </div>
        </div>
        <div class="col-6 my-1">
            <small id="errorServiceLenderPE" class="form-text text-danger">&nbsp;{$errorServiceLenderPE}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Service Lender&nbsp;
                        <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                    </div>
                </div>
                <select class="form-control" id="serviceLenderPE" name="serviceLenderPE">
                    <option {if $serviceLenderPE==='Y'}selected{/if} value='Y'>YES</option>
                    <option {if $serviceLenderPE==='N'}selected{/if} value='N'>NO</option>
                </select>
            </div>
        </div>
        <div class="col-6 my-1">
            <small id="errorConventionPE" class="form-text text-danger">&nbsp;{$errorConventionPE}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Convention&nbsp;
                        <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                    </div>
                </div>
                <select class="form-control" id="conventionPE" name="conventionPE">
                    <option {if $conventionPE==='0'}selected{/if} value='0'>None</option>
                    <option {if $conventionPE==='1'}selected{/if} value='1'>Canada</option>
                    <option {if $conventionPE==='2'}selected{/if} value='2'>Chile</option>
                    <option {if $conventionPE==='3'}selected{/if} value='3'>Can</option>
                    <option {if $conventionPE==='4'}selected{/if} value='4'>Brasil</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-6 my-1">
            <small id="errorPersonCategoryPE" class="form-text text-danger">&nbsp;{$errorPersonCategoryPE}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Person Category&nbsp;
                        <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                    </div>
                </div>
                <select class="form-control" id="personCategoryPE" name="personCategoryPE">
                    <option {if $personCategoryPE==='Legal Person'}selected{/if} value='Legal Person'>Legal Person</option>
                    <option {if $personCategoryPE==='Natural Person'}selected{/if} value='Natural Person'>Natural Person</option>
                </select>
            </div>
        </div>
        <div class="col-6 my-1">
            <small id="errorPersonTypePE" class="form-text text-danger">&nbsp;{$errorPersonTypePE}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Person Type&nbsp;
                        <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                    </div>
                </div>
                <select class="form-control" id="personTypePE" name="personTypePE">
                </select>
            </div>
        </div>
        <div class="col-6 my-1">
            <small id="errorDocumentTypePE" class="form-text text-danger">&nbsp;{$errorDocumentTypePE}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Document Type&nbsp;
                        <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                    </div>
                </div>
                <select class="form-control" id="documentTypePE" name="documentTypePE">
                </select>
            </div>
        </div>
        <div class="col-6 my-1">
            <small id="errorDocumentNumberPE" class="form-text text-danger">&nbsp;{$errorDocumentNumberPE}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Document Number&nbsp;
                        <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                    </div>
                </div>
                <input class="form-control" id="documentNumberPE" name="documentNumberPE" value="{$documentNumberPE}"/>
            </div>
        </div>
        <div class="col-12 my-1">
            <small id="errorNotResidentDocumentTypePE" class="form-text text-danger">&nbsp;{$errorNotResidentDocumentTypePE}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Not Resident Document Type
                        <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                    </div>
                </div>
                <select class="form-control" id="notResidentDocumentTypePE" name="notResidentDocumentTypePE">
                    <option {if $notResidentDocumentTypePE===''}selected{/if} value=''>leave empty, does not apply</option>
                    <option {if $notResidentDocumentTypePE==='01'}selected{/if} value='01'>01: TIN Tax identification number of PPNN Non Resident</option>
                    <option {if $notResidentDocumentTypePE==='02'}selected{/if} value='02'>02: IN Tax Identification Number of PPJJ Non Resident </option>
                    <option {if $notResidentDocumentTypePE==='03'}selected{/if} value='03'>03: Doc. Local Identification of the origin country, identifies the Non Resident per</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-6 my-1">
            <small id="errorSupplierConditionPE" class="form-text text-danger">&nbsp;{$errorSupplierConditionPE}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Supplier Condition&nbsp;
                        <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                    </div>
                </div>
                <select class="form-control" id="supplierConditionPE" name="supplierConditionPE">
                    <option {if $supplierConditionPE==='Y'}selected{/if} value='Y'>YES</option>
                    <option {if $supplierConditionPE==='N'}selected{/if} value='N'>NO</option>
                </select>
            </div>
        </div>
        <div class="col-6 my-1">
            <small id="errorWithholdingSupplierPE" class="form-text text-danger">&nbsp;{$errorWithholdingSupplierPE}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Withholding Supplier&nbsp;
                        <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                    </div>
                </div>
                <select class="form-control" id="withholdingSupplierPE" name="withholdingSupplierPE">
                    <option {if $withholdingSupplierPE==='Y'}selected{/if} value='Y'>YES</option>
                    <option {if $withholdingSupplierPE==='N'}selected{/if} value='N'>NO</option>
                </select>
            </div>
        </div>
    </div>


        <div class="form-row align-items-center">
            <div class="col-6 my-1">
                <small id="errorCountryCodePE" class="form-text text-danger">&nbsp;{$errorCountryCodePE}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Country Code&nbsp;
                            <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                        </div>
                    </div>
                    <select class="form-control" id="countryCodePE" name="countryCodePE">
                        <option {if $countryCodePE==='6469'}selected{/if} value='6469'>MARIANAS DEL NORTE, ISLAS</option>
                        <option {if $countryCodePE==='9001'}selected{/if} value='9001'>BOUVET ISLAND</option>
                        <option {if $countryCodePE==='9002'}selected{/if} value='9002'>COTE D IVOIRE</option>
                        <option {if $countryCodePE==='9003'}selected{/if} value='9003'>FALKLAND ISLANDS (MALVINAS)</option>
                        <option {if $countryCodePE==='9004'}selected{/if} value='9004'>FRANCE, METROPOLITAN</option>
                        <option {if $countryCodePE==='9005'}selected{/if} value='9005'>FRENCH SOUTHERN TERRITORIES</option>
                        <option {if $countryCodePE==='9006'}selected{/if} value='9006'>HEARD AND MC DONALD ISLANDS</option>
                        <option {if $countryCodePE==='9007'}selected{/if} value='9007'>MAYOTTE</option>
                        <option {if $countryCodePE==='9008'}selected{/if} value='9008'>SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS</option>
                        <option {if $countryCodePE==='9009'}selected{/if} value='9009'>SVALBARD AND JAN MAYEN ISLANDS</option>
                        <option {if $countryCodePE==='9010'}selected{/if} value='9010'>UNITED STATES MINOR OUTLYING ISLANDS</option>
                        <option {if $countryCodePE==='9011'}selected{/if} value='9011'>OTROS PAISES O LUGARES</option>
                        <option {if $countryCodePE==='9013'}selected{/if} value='9013'>AFGANISTAN</option>
                        <option {if $countryCodePE==='9017'}selected{/if} value='9017'>ALBANIA</option>
                        <option {if $countryCodePE==='9019'}selected{/if} value='9019'>ALDERNEY</option>
                        <option {if $countryCodePE==='9023'}selected{/if} value='9023'>ALEMANIA</option>
                        <option {if $countryCodePE==='9026'}selected{/if} value='9026'>ARMENIA</option>
                        <option {if $countryCodePE==='9027'}selected{/if} value='9027'>ARUBA</option>
                        <option {if $countryCodePE==='9028'}selected{/if} value='9028'>ASCENCION</option>
                        <option {if $countryCodePE==='9029'}selected{/if} value='9029'>BOSNIA-HERZEGOVINA</option>
                        <option {if $countryCodePE==='9031'}selected{/if} value='9031'>BURKINA FASO</option>
                        <option {if $countryCodePE==='9037'}selected{/if} value='9037'>ANDORRA</option>
                        <option {if $countryCodePE==='9040'}selected{/if} value='9040'>ANGOLA</option>
                        <option {if $countryCodePE==='9041'}selected{/if} value='9041'>ANGUILLA</option>
                        <option {if $countryCodePE==='9043'}selected{/if} value='9043'>ANTIGUA Y BARBUDA</option>
                        <option {if $countryCodePE==='9047'}selected{/if} value='9047'>ANTILLAS HOLANDESAS</option>
                        <option {if $countryCodePE==='9053'}selected{/if} value='9053'>ARABIA SAUDITA</option>
                        <option {if $countryCodePE==='9059'}selected{/if} value='9059'>ARGELIA</option>
                        <option {if $countryCodePE==='9063'}selected{/if} value='9063'>ARGENTINA</option>
                        <option {if $countryCodePE==='9069'}selected{/if} value='9069'>AUSTRALIA</option>
                        <option {if $countryCodePE==='9072'}selected{/if} value='9072'>AUSTRIA</option>
                        <option {if $countryCodePE==='9074'}selected{/if} value='9074'>AZERBAIJAN</option>
                        <option {if $countryCodePE==='9077'}selected{/if} value='9077'>BAHAMAS</option>
                        <option {if $countryCodePE==='9080'}selected{/if} value='9080'>BAHREIN</option>
                        <option {if $countryCodePE==='9081'}selected{/if} value='9081'>BANGLADESH</option>
                        <option {if $countryCodePE==='9083'}selected{/if} value='9083'>BARBADOS</option>
                        <option {if $countryCodePE==='9087'}selected{/if} value='9087'>BELGICA</option>
                        <option {if $countryCodePE==='9088'}selected{/if} value='9088'>BELICE</option>
                        <option {if $countryCodePE==='9090'}selected{/if} value='9090'>BERMUDAS</option>
                        <option {if $countryCodePE==='9091'}selected{/if} value='9091'>BELARUS</option>
                        <option {if $countryCodePE==='9093'}selected{/if} value='9093'>MYANMAR</option>
                        <option {if $countryCodePE==='9097'}selected{/if} value='9097'>BOLIVIA</option>
                        <option {if $countryCodePE==='9101'}selected{/if} value='9101'>BOTSWANA</option>
                        <option {if $countryCodePE==='9105'}selected{/if} value='9105'>BRASIL</option>
                        <option {if $countryCodePE==='9108'}selected{/if} value='9108'>BRUNEI DARUSSALAM</option>
                        <option {if $countryCodePE==='9111'}selected{/if} value='9111'>BULGARIA</option>
                        <option {if $countryCodePE==='9115'}selected{/if} value='9115'>BURUNDI</option>
                        <option {if $countryCodePE==='9119'}selected{/if} value='9119'>BUTAN</option>
                        <option {if $countryCodePE==='9127'}selected{/if} value='9127'>CABO VERDE</option>
                        <option {if $countryCodePE==='9137'}selected{/if} value='9137'>CAIMAN,ISLAS</option>
                        <option {if $countryCodePE==='9141'}selected{/if} value='9141'>CAMBOYA</option>
                        <option {if $countryCodePE==='9145'}selected{/if} value='9145'>CAMERUN,REPUBLICA UNIDA DEL</option>
                        <option {if $countryCodePE==='9147'}selected{/if} value='9147'>CAMPIONE D TALIA</option>
                        <option {if $countryCodePE==='9149'}selected{/if} value='9149'>CANADA</option>
                        <option {if $countryCodePE==='9155'}selected{/if} value='9155'>CANAL (NORMANDAS), ISLAS</option>
                        <option {if $countryCodePE==='9157'}selected{/if} value='9157'>CANTON Y ENDERBURRY</option>
                        <option {if $countryCodePE==='9159'}selected{/if} value='9159'>SANTA SEDE</option>
                        <option {if $countryCodePE==='9165'}selected{/if} value='9165'>COCOS (KEELING),ISLAS</option>
                        <option {if $countryCodePE==='9169'}selected{/if} value='9169'>COLOMBIA</option>
                        <option {if $countryCodePE==='9173'}selected{/if} value='9173'>COMORAS</option>
                        <option {if $countryCodePE==='9177'}selected{/if} value='9177'>CONGO</option>
                        <option {if $countryCodePE==='9183'}selected{/if} value='9183'>COOK, ISLAS</option>
                        <option {if $countryCodePE==='9187'}selected{/if} value='9187'>COREA (NORTE), REPUBLICA POPULAR DEMOCRATICA DE</option>
                        <option {if $countryCodePE==='9190'}selected{/if} value='9190'>COREA (SUR), REPUBLICA DE</option>
                        <option {if $countryCodePE==='9193'}selected{/if} value='9193'>COSTA DE MARFIL</option>
                        <option {if $countryCodePE==='9196'}selected{/if} value='9196'>COSTA RICA</option>
                        <option {if $countryCodePE==='9198'}selected{/if} value='9198'>CROACIA</option>
                        <option {if $countryCodePE==='9199'}selected{/if} value='9199'>CUBA</option>
                        <option {if $countryCodePE==='9203'}selected{/if} value='9203'>CHAD</option>
                        <option {if $countryCodePE==='9207'}selected{/if} value='9207'>CHECOSLOVAQUIA</option>
                        <option {if $countryCodePE==='9211'}selected{/if} value='9211'>CHILE</option>
                        <option {if $countryCodePE==='9215'}selected{/if} value='9215'>CHINA</option>
                        <option {if $countryCodePE==='9218'}selected{/if} value='9218'>TAIWAN (FORMOSA)</option>
                        <option {if $countryCodePE==='9221'}selected{/if} value='9221'>CHIPRE</option>
                        <option {if $countryCodePE==='9229'}selected{/if} value='9229'>BENIN</option>
                        <option {if $countryCodePE==='9232'}selected{/if} value='9232'>DINAMARCA</option>
                        <option {if $countryCodePE==='9235'}selected{/if} value='9235'>DOMINICA</option>
                        <option {if $countryCodePE==='9239'}selected{/if} value='9239'>ECUADOR</option>
                        <option {if $countryCodePE==='9240'}selected{/if} value='9240'>EGIPTO</option>
                        <option {if $countryCodePE==='9242'}selected{/if} value='9242'>EL SALVADOR</option>
                        <option {if $countryCodePE==='9243'}selected{/if} value='9243'>ERITREA</option>
                        <option {if $countryCodePE==='9244'}selected{/if} value='9244'>EMIRATOS ARABES UNIDOS</option>
                        <option {if $countryCodePE==='9245'}selected{/if} value='9245'>ESPANA</option>
                        <option {if $countryCodePE==='9246'}selected{/if} value='9246'>ESLOVAQUIA</option>
                        <option {if $countryCodePE==='9247'}selected{/if} value='9247'>ESLOVENIA</option>
                        <option {if $countryCodePE==='9249'}selected{/if} value='9249'>ESTADOS UNIDOS</option>
                        <option {if $countryCodePE==='9251'}selected{/if} value='9251'>ESTONIA</option>
                        <option {if $countryCodePE==='9253'}selected{/if} value='9253'>ETIOPIA</option>
                        <option {if $countryCodePE==='9259'}selected{/if} value='9259'>FEROE, ISLAS</option>
                        <option {if $countryCodePE==='9267'}selected{/if} value='9267'>FILIPINAS</option>
                        <option {if $countryCodePE==='9271'}selected{/if} value='9271'>FINLANDIA</option>
                        <option {if $countryCodePE==='9275'}selected{/if} value='9275'>FRANCIA</option>
                        <option {if $countryCodePE==='9281'}selected{/if} value='9281'>GABON</option>
                        <option {if $countryCodePE==='9285'}selected{/if} value='9285'>GAMBIA</option>
                        <option {if $countryCodePE==='9286'}selected{/if} value='9286'>GAZA Y JERICO</option>
                        <option {if $countryCodePE==='9287'}selected{/if} value='9287'>GEORGIA</option>
                        <option {if $countryCodePE==='9289'}selected{/if} value='9289'>GHANA</option>
                        <option {if $countryCodePE==='9293'}selected{/if} value='9293'>GIBRALTAR</option>
                        <option {if $countryCodePE==='9297'}selected{/if} value='9297'>GRANADA</option>
                        <option {if $countryCodePE==='9301'}selected{/if} value='9301'>GRECIA</option>
                        <option {if $countryCodePE==='9305'}selected{/if} value='9305'>GROENLANDIA</option>
                        <option {if $countryCodePE==='9309'}selected{/if} value='9309'>GUADALUPE</option>
                        <option {if $countryCodePE==='9313'}selected{/if} value='9313'>GUAM</option>
                        <option {if $countryCodePE==='9317'}selected{/if} value='9317'>GUATEMALA</option>
                        <option {if $countryCodePE==='9325'}selected{/if} value='9325'>GUAYANA FRANCESA</option>
                        <option {if $countryCodePE==='9327'}selected{/if} value='9327'>GUERNSEY</option>
                        <option {if $countryCodePE==='9329'}selected{/if} value='9329'>GUINEA</option>
                        <option {if $countryCodePE==='9331'}selected{/if} value='9331'>GUINEA ECUATORIAL</option>
                        <option {if $countryCodePE==='9334'}selected{/if} value='9334'>GUINEA-BISSAU</option>
                        <option {if $countryCodePE==='9337'}selected{/if} value='9337'>GUYANA</option>
                        <option {if $countryCodePE==='9341'}selected{/if} value='9341'>HAITI</option>
                        <option {if $countryCodePE==='9345'}selected{/if} value='9345'>HONDURAS</option>
                        <option {if $countryCodePE==='9348'}selected{/if} value='9348'>HONDURAS BRITANICAS</option>
                        <option {if $countryCodePE==='9351'}selected{/if} value='9351'>HONG KONG</option>
                        <option {if $countryCodePE==='9355'}selected{/if} value='9355'>HUNGRIA</option>
                        <option {if $countryCodePE==='9361'}selected{/if} value='9361'>INDIA</option>
                        <option {if $countryCodePE==='9365'}selected{/if} value='9365'>INDONESIA</option>
                        <option {if $countryCodePE==='9369'}selected{/if} value='9369'>IRAK</option>
                        <option {if $countryCodePE==='9372'}selected{/if} value='9372'>IRAN, REPUBLICA ISLAMICA DEL</option>
                        <option {if $countryCodePE==='9375'}selected{/if} value='9375'>IRLANDA (EIRE)</option>
                        <option {if $countryCodePE==='9377'}selected{/if} value='9377'>ISLA AZORES</option>
                        <option {if $countryCodePE==='9378'}selected{/if} value='9378'>ISLA DEL MAN</option>
                        <option {if $countryCodePE==='9379'}selected{/if} value='9379'>ISLANDIA</option>
                        <option {if $countryCodePE==='9380'}selected{/if} value='9380'>ISLAS CANARIAS</option>
                        <option {if $countryCodePE==='9381'}selected{/if} value='9381'>ISLAS DE CHRISTMAS</option>
                        <option {if $countryCodePE==='9382'}selected{/if} value='9382'>ISLAS QESHM</option>
                        <option {if $countryCodePE==='9383'}selected{/if} value='9383'>ISRAEL</option>
                        <option {if $countryCodePE==='9386'}selected{/if} value='9386'>ITALIA</option>
                        <option {if $countryCodePE==='9391'}selected{/if} value='9391'>JAMAICA</option>
                        <option {if $countryCodePE==='9395'}selected{/if} value='9395'>JONSTON, ISLAS</option>
                        <option {if $countryCodePE==='9399'}selected{/if} value='9399'>JAPON</option>
                        <option {if $countryCodePE==='9401'}selected{/if} value='9401'>JERSEY</option>
                        <option {if $countryCodePE==='9403'}selected{/if} value='9403'>JORDANIA</option>
                        <option {if $countryCodePE==='9406'}selected{/if} value='9406'>KAZAJSTAN</option>
                        <option {if $countryCodePE==='9410'}selected{/if} value='9410'>KENIA</option>
                        <option {if $countryCodePE==='9411'}selected{/if} value='9411'>KIRIBATI</option>
                        <option {if $countryCodePE==='9412'}selected{/if} value='9412'>KIRGUIZISTAN</option>
                        <option {if $countryCodePE==='9413'}selected{/if} value='9413'>KUWAIT</option>
                        <option {if $countryCodePE==='9418'}selected{/if} value='9418'>LABUN</option>
                        <option {if $countryCodePE==='9420'}selected{/if} value='9420'>LAOS, REPUBLICA POPULAR DEMOCRATICA DE</option>
                        <option {if $countryCodePE==='9426'}selected{/if} value='9426'>LESOTHO</option>
                        <option {if $countryCodePE==='9429'}selected{/if} value='9429'>LETONIA</option>
                        <option {if $countryCodePE==='9431'}selected{/if} value='9431'>LIBANO</option>
                        <option {if $countryCodePE==='9434'}selected{/if} value='9434'>LIBERIA</option>
                        <option {if $countryCodePE==='9438'}selected{/if} value='9438'>LIBIA</option>
                        <option {if $countryCodePE==='9440'}selected{/if} value='9440'>LIECHTENSTEIN</option>
                        <option {if $countryCodePE==='9443'}selected{/if} value='9443'>LITUANIA</option>
                        <option {if $countryCodePE==='9445'}selected{/if} value='9445'>LUXEMBURGO</option>
                        <option {if $countryCodePE==='9447'}selected{/if} value='9447'>MACAO</option>
                        <option {if $countryCodePE==='9448'}selected{/if} value='9448'>MACEDONIA</option>
                        <option {if $countryCodePE==='9450'}selected{/if} value='9450'>MADAGASCAR</option>
                        <option {if $countryCodePE==='9453'}selected{/if} value='9453'>MADEIRA</option>
                        <option {if $countryCodePE==='9455'}selected{/if} value='9455'>MALAYSIA</option>
                        <option {if $countryCodePE==='9458'}selected{/if} value='9458'>MALAWI</option>
                        <option {if $countryCodePE==='9461'}selected{/if} value='9461'>MALDIVAS</option>
                        <option {if $countryCodePE==='9464'}selected{/if} value='9464'>MALI</option>
                        <option {if $countryCodePE==='9467'}selected{/if} value='9467'>MALTA</option>
                        <option {if $countryCodePE==='9469'}selected{/if} value='9469'>MARIANAS DEL NORTE, ISLAS</option>
                        <option {if $countryCodePE==='9472'}selected{/if} value='9472'>MARSHALL, ISLAS</option>
                        <option {if $countryCodePE==='9474'}selected{/if} value='9474'>MARRUECOS</option>
                        <option {if $countryCodePE==='9477'}selected{/if} value='9477'>MARTINICA</option>
                        <option {if $countryCodePE==='9485'}selected{/if} value='9485'>MAURICIO</option>
                        <option {if $countryCodePE==='9488'}selected{/if} value='9488'>MAURITANIA</option>
                        <option {if $countryCodePE==='9493'}selected{/if} value='9493'>MEXICO</option>
                        <option {if $countryCodePE==='9494'}selected{/if} value='9494'>MICRONESIA, ESTADOS FEDERADOS DE</option>
                        <option {if $countryCodePE==='9495'}selected{/if} value='9495'>MIDWAY ISLAS</option>
                        <option {if $countryCodePE==='9496'}selected{/if} value='9496'>MOLDAVIA</option>
                        <option {if $countryCodePE==='9497'}selected{/if} value='9497'>MONGOLIA</option>
                        <option {if $countryCodePE==='9498'}selected{/if} value='9498'>MONACO</option>
                        <option {if $countryCodePE==='9501'}selected{/if} value='9501'>MONTSERRAT, ISLA</option>
                        <option {if $countryCodePE==='9505'}selected{/if} value='9505'>MOZAMBIQUE</option>
                        <option {if $countryCodePE==='9507'}selected{/if} value='9507'>NAMIBIA</option>
                        <option {if $countryCodePE==='9508'}selected{/if} value='9508'>NAURU</option>
                        <option {if $countryCodePE==='9511'}selected{/if} value='9511'>NAVIDAD (CHRISTMAS), ISLA</option>
                        <option {if $countryCodePE==='9517'}selected{/if} value='9517'>NEPAL</option>
                        <option {if $countryCodePE==='9521'}selected{/if} value='9521'>NICARAGUA</option>
                        <option {if $countryCodePE==='9525'}selected{/if} value='9525'>NIGER</option>
                        <option {if $countryCodePE==='9528'}selected{/if} value='9528'>NIGERIA</option>
                        <option {if $countryCodePE==='9531'}selected{/if} value='9531'>NIUE, ISLA</option>
                        <option {if $countryCodePE==='9535'}selected{/if} value='9535'>NORFOLK, ISLA</option>
                        <option {if $countryCodePE==='9538'}selected{/if} value='9538'>NORUEGA</option>
                        <option {if $countryCodePE==='9542'}selected{/if} value='9542'>NUEVA CALEDONIA</option>
                        <option {if $countryCodePE==='9545'}selected{/if} value='9545'>PAPUASIA NUEVA GUINEA</option>
                        <option {if $countryCodePE==='9548'}selected{/if} value='9548'>NUEVA ZELANDA</option>
                        <option {if $countryCodePE==='9551'}selected{/if} value='9551'>VANUATU</option>
                        <option {if $countryCodePE==='9556'}selected{/if} value='9556'>OMAN</option>
                        <option {if $countryCodePE==='9566'}selected{/if} value='9566'>PACIFICO, ISLAS DEL</option>
                        <option {if $countryCodePE==='9573'}selected{/if} value='9573'>PAISES BAJOS</option>
                        <option {if $countryCodePE==='9576'}selected{/if} value='9576'>PAKISTAN</option>
                        <option {if $countryCodePE==='9578'}selected{/if} value='9578'>PALAU, ISLAS</option>
                        <option {if $countryCodePE==='9579'}selected{/if} value='9579'>TERRITORIO AUTONOMO DE PALESTINA</option>
                        <option {if $countryCodePE==='9580'}selected{/if} value='9580'>PANAMA</option>
                        <option {if $countryCodePE==='9586'}selected{/if} value='9586'>PARAGUAY</option>
                        <option {if $countryCodePE==='9589'}selected{/if} value='9589'>PERU</option>
                        <option {if $countryCodePE==='9593'}selected{/if} value='9593'>PITCAIRN, ISLA</option>
                        <option {if $countryCodePE==='9599'}selected{/if} value='9599'>POLINESIA FRANCESA</option>
                        <option {if $countryCodePE==='9603'}selected{/if} value='9603'>POLONIA</option>
                        <option {if $countryCodePE==='9607'}selected{/if} value='9607'>PORTUGAL</option>
                        <option {if $countryCodePE==='9611'}selected{/if} value='9611'>PUERTO RICO</option>
                        <option {if $countryCodePE==='9618'}selected{/if} value='9618'>QATAR</option>
                        <option {if $countryCodePE==='9628'}selected{/if} value='9628'>REINO UNIDO</option>
                        <option {if $countryCodePE==='9629'}selected{/if} value='9629'>ESCOCIA</option>
                        <option {if $countryCodePE==='9633'}selected{/if} value='9633'>REPUBLICA ARABE UNIDA</option>
                        <option {if $countryCodePE==='9640'}selected{/if} value='9640'>REPUBLICA CENTROAFRICANA</option>
                        <option {if $countryCodePE==='9644'}selected{/if} value='9644'>REPUBLICA CHECA</option>
                        <option {if $countryCodePE==='9645'}selected{/if} value='9645'>REPUBLICA DE SWAZILANDIA</option>
                        <option {if $countryCodePE==='9646'}selected{/if} value='9646'>REPUBLICA DE TUNEZ</option>
                        <option {if $countryCodePE==='9647'}selected{/if} value='9647'>REPUBLICA DOMINICANA</option>
                        <option {if $countryCodePE==='9660'}selected{/if} value='9660'>REUNION</option>
                        <option {if $countryCodePE==='9665'}selected{/if} value='9665'>ZIMBABWE</option>
                        <option {if $countryCodePE==='9670'}selected{/if} value='9670'>RUMANIA</option>
                        <option {if $countryCodePE==='9675'}selected{/if} value='9675'>RUANDA</option>
                        <option {if $countryCodePE==='9676'}selected{/if} value='9676'>RUSIA</option>
                        <option {if $countryCodePE==='9677'}selected{/if} value='9677'>SALOMON, ISLAS</option>
                        <option {if $countryCodePE==='9685'}selected{/if} value='9685'>SAHARA OCCIDENTAL</option>
                        <option {if $countryCodePE==='9687'}selected{/if} value='9687'>SAMOA OCCIDENTAL</option>
                        <option {if $countryCodePE==='9690'}selected{/if} value='9690'>SAMOA NORTEAMERICANA</option>
                        <option {if $countryCodePE==='9695'}selected{/if} value='9695'>SAN CRISTOBAL Y NIEVES</option>
                        <option {if $countryCodePE==='9697'}selected{/if} value='9697'>SAN MARINO</option>
                        <option {if $countryCodePE==='9700'}selected{/if} value='9700'>SAN PEDRO Y MIQUELON</option>
                        <option {if $countryCodePE==='9705'}selected{/if} value='9705'>SAN VICENTE Y LAS GRANADINAS</option>
                        <option {if $countryCodePE==='9710'}selected{/if} value='9710'>SANTA ELENA</option>
                        <option {if $countryCodePE==='9715'}selected{/if} value='9715'>SANTA LUCIA</option>
                        <option {if $countryCodePE==='9720'}selected{/if} value='9720'>SANTO TOME Y PRINCIPE</option>
                        <option {if $countryCodePE==='9728'}selected{/if} value='9728'>SENEGAL</option>
                        <option {if $countryCodePE==='9731'}selected{/if} value='9731'>SEYCHELLES</option>
                        <option {if $countryCodePE==='9735'}selected{/if} value='9735'>SIERRA LEONA</option>
                        <option {if $countryCodePE==='9741'}selected{/if} value='9741'>SINGAPUR</option>
                        <option {if $countryCodePE==='9744'}selected{/if} value='9744'>SIRIA, REPUBLICA ARABE DE</option>
                        <option {if $countryCodePE==='9748'}selected{/if} value='9748'>SOMALIA</option>
                        <option {if $countryCodePE==='9750'}selected{/if} value='9750'>SRI LANKA</option>
                        <option {if $countryCodePE==='9756'}selected{/if} value='9756'>SUDAFRICA, REPUBLICA DE</option>
                        <option {if $countryCodePE==='9759'}selected{/if} value='9759'>SUDAN</option>
                        <option {if $countryCodePE==='9764'}selected{/if} value='9764'>SUECIA</option>
                        <option {if $countryCodePE==='9767'}selected{/if} value='9767'>SUIZA</option>
                        <option {if $countryCodePE==='9770'}selected{/if} value='9770'>SURINAM</option>
                        <option {if $countryCodePE==='9773'}selected{/if} value='9773'>SAWSILANDIA</option>
                        <option {if $countryCodePE==='9774'}selected{/if} value='9774'>TADJIKISTAN</option>
                        <option {if $countryCodePE==='9776'}selected{/if} value='9776'>TAILANDIA</option>
                        <option {if $countryCodePE==='9780'}selected{/if} value='9780'>TANZANIA, REPUBLICA UNIDA DE</option>
                        <option {if $countryCodePE==='9783'}selected{/if} value='9783'>DJIBOUTI</option>
                        <option {if $countryCodePE==='9786'}selected{/if} value='9786'>TERRITORIO ANTARTICO BRITANICO</option>
                        <option {if $countryCodePE==='9787'}selected{/if} value='9787'>TERRITORIO BRITANICO DEL OCEANO INDICO</option>
                        <option {if $countryCodePE==='9788'}selected{/if} value='9788'>TIMOR DEL ESTE</option>
                        <option {if $countryCodePE==='9800'}selected{/if} value='9800'>TOGO</option>
                        <option {if $countryCodePE==='9805'}selected{/if} value='9805'>TOKELAU</option>
                        <option {if $countryCodePE==='9810'}selected{/if} value='9810'>TONGA</option>
                        <option {if $countryCodePE==='9815'}selected{/if} value='9815'>TRINIDAD Y TOBAGO</option>
                        <option {if $countryCodePE==='9816'}selected{/if} value='9816'>TRISTAN DA CUNHA</option>
                        <option {if $countryCodePE==='9820'}selected{/if} value='9820'>TUNICIA</option>
                        <option {if $countryCodePE==='9823'}selected{/if} value='9823'>TURCAS Y CAICOS, ISLAS</option>
                        <option {if $countryCodePE==='9825'}selected{/if} value='9825'>TURKMENISTAN</option>
                        <option {if $countryCodePE==='9827'}selected{/if} value='9827'>TURQUIA</option>
                        <option {if $countryCodePE==='9828'}selected{/if} value='9828'>TUVALU</option>
                        <option {if $countryCodePE==='9830'}selected{/if} value='9830'>UCRANIA</option>
                        <option {if $countryCodePE==='9833'}selected{/if} value='9833'>UGANDA</option>
                        <option {if $countryCodePE==='9840'}selected{/if} value='9840'>URSS</option>
                        <option {if $countryCodePE==='9845'}selected{/if} value='9845'>URUGUAY</option>
                        <option {if $countryCodePE==='9850'}selected{/if} value='9850'>VENEZUELA</option>
                        <option {if $countryCodePE==='9855'}selected{/if} value='9855'>VIET NAM</option>
                        <option {if $countryCodePE==='9858'}selected{/if} value='9858'>VIETNAM (DEL NORTE)</option>
                        <option {if $countryCodePE==='9863'}selected{/if} value='9863'>VIRGENES, ISLAS (BRITANICAS)</option>
                        <option {if $countryCodePE==='9866'}selected{/if} value='9866'>VIRGENES, ISLAS (NORTEAMERICANAS)</option>
                        <option {if $countryCodePE==='9870'}selected{/if} value='9870'>FIJI</option>
                        <option {if $countryCodePE==='9873'}selected{/if} value='9873'>WAKE, ISLA</option>
                        <option {if $countryCodePE==='9875'}selected{/if} value='9875'>WALLIS Y FORTUNA, ISLAS</option>
                        <option {if $countryCodePE==='9880'}selected{/if} value='9880'>YEMEN</option>
                        <option {if $countryCodePE==='9885'}selected{/if} value='9885'>YUGOSLAVIA</option>
                        <option {if $countryCodePE==='9888'}selected{/if} value='9888'>ZAIRE</option>
                        <option {if $countryCodePE==='9890'}selected{/if} value='9890'>ZAMBIA</option>
                        <option {if $countryCodePE==='9895'}selected{/if} value='9895'>ZONA DEL CANAL DE PANAMA</option>
                        <option {if $countryCodePE==='9896'}selected{/if} value='9896'>ZONA LIBRE OSTRAVA</option>
                        <option {if $countryCodePE==='9897'}selected{/if} value='9897'>ZONA NEUTRAL (PALESTINA)</option>
                    </select>
                </div>
            </div>
            <div class="col-6 my-1">
                <small id="errorAgreementAvoidDoubleTaxPE" class="form-text text-danger">&nbsp;{$errorAgreementAvoidDoubleTaxPE}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Agreement Avoid Double Tax&nbsp;
                            <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                        </div>
                    </div>
                    <select class="form-control" id="agreementAvoidDoubleTaxPE" name="agreementAvoidDoubleTaxPE">
                        <option {if $agreementAvoidDoubleTaxPE==='00'}selected{/if} value='00'>NINGUNO</option>
                        <option {if $agreementAvoidDoubleTaxPE==='01'}selected{/if} value='01'>CANADA</option>
                        <option {if $agreementAvoidDoubleTaxPE==='02'}selected{/if} value='02'>CHILE</option>
                        <option {if $agreementAvoidDoubleTaxPE==='03'}selected{/if} value='03'>COMUNIDAD ANDINA DE NACIONES (CAN)</option>
                        <option {if $agreementAvoidDoubleTaxPE==='04'}selected{/if} value='04'>BRASIL</option>
                        <option {if $agreementAvoidDoubleTaxPE==='05'}selected{/if} value='05'>ESTADOS UNIDOS MEXICANOS</option>
                        <option {if $agreementAvoidDoubleTaxPE==='06'}selected{/if} value='06'>REPUBLICA DE COREA</option>
                        <option {if $agreementAvoidDoubleTaxPE==='07'}selected{/if} value='07'>CONFEDERACION SUIZA</option>
                        <option {if $agreementAvoidDoubleTaxPE==='08'}selected{/if} value='08'>PORTUGAL</option>
                        <option {if $agreementAvoidDoubleTaxPE==='09'}selected{/if} value='09'>OTROS</option>
                    </select>
                </div>
            </div>
        </div>



        <script type="application/javascript">

        var setValueForPersonType = false;
        var setValueForDocumentType = false;

        $("#personCategoryPE").ready(function() {
            var personCategoryPE = '{$personCategoryPE}';

            if (personCategoryPE=='')
                personCategoryPE ='Legal Person';

            $("#personCategoryPE").val(personCategoryPE);
            $("#personCategoryPE").trigger('change');
        });


        $( "#personCategoryPE" ).change(function() {
            if ($(this).val()=='Legal Person'){
                $('#nameFieldsPE').hide();
            }else{
                $('#nameFieldsPE').show();
            }

        });

        $("#documentTypePE").remoteChained({
            parents : "#personCategoryPE,#personTypePE",
            url : "{base_url()}/Vendors/getDocumentTypePE",
            data: function (json) {
                var documentTypePE = '{$documentTypePE}';

                if (setValueForPersonType&&!setValueForDocumentType&&(documentTypePE!='')){
                    setTimeout(function(){
                        $("#documentTypePE").val(documentTypePE).change();
                    }, 250);
                    setValueForDocumentType = true;
                }

                return json;
            }
        });


        $("#personTypePE").remoteChained({
            parents : "#personCategoryPE",
            url : "{base_url()}/Vendors/getPersonTypePE",
            data: function (json) {
                var personTypePE = '{$personTypePE}';

                if (!setValueForPersonType&&(personTypePE!='')){
                    setTimeout(function(){
                        $("#personTypePE").val(personTypePE).change();
                    }, 250);

                    setValueForPersonType = true;

                }

                return json;
            }
        });

    </script>
    {/if}

    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorMatchOptionLevel" class="form-text text-danger">&nbsp;{$errorMatchOptionLevel}</small>
            <label class="sr-only" for="inlineFormInputMatchOptionLevel">Match Option Level</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Match Option Level</div>
                </div>
                <select class="form-control" id="matchOptionLevel" name="matchOptionLevel" aria-describedby="$descriptor.shortNameHelp" placeholder="Match Option Level">
                    <option {if $matchOptionLevel==='2-WAY'}selected{/if}>2-WAY</option>
                    <option {if $matchOptionLevel==='3-WAY'}selected{/if}>3-WAY</option>

                </select>

            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorPaymentMethodLookupCode" class="form-text text-danger">&nbsp;{$errorPaymentMethodLookupCode}</small>
            <label class="sr-only" for="inlineFormInputPaymentMethodLookupCode">Payment Method Lookup Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Payment Method Lookup Code </div>
                </div>
                <select class="form-control" id="paymentMethodLookupCode" name="paymentMethodLookupCode" aria-describedby="$descriptor.shortNameHelp" placeholder="Payment Method Lookup Code">
                    <option {if $paymentMethodLookupCode==='WIRE'}selected{/if}>WIRE</option>
                    <option {if $paymentMethodLookupCode==='CHECK'}selected{/if}>CHECK</option>
                    <option {if $paymentMethodLookupCode==='EFT'}selected{/if}>EFT</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-2 my-1">
            <small id="errorFederalReportableFlag" class="form-text text-danger">&nbsp;{$errorFederalReportableFlag}</small>
            <label class="sr-only" for="inlineFormInputFederalReportableFlag">Federal Reportable Flag</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Federal Reportable Flag </div>
                </div>
                <input style="margin:10px;" type="checkbox" {($federalReportableFlag==true)?'checked':''} id="federalReportableFlag" name="federalReportableFlag">
            </div>
        </div>
        <div class="col-sm-2 my-1">
            <small id="errorStateReportableFlag" class="form-text text-danger">&nbsp;{$errorStateReportableFlag}</small>
            <label class="sr-only" for="inlineFormInputStateReportableFlag">State Reportable Flag</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">State Reportable Flag</div>
                </div>
                <input style="margin:10px;" type="checkbox" {($stateReportableFlag==true)?'checked':''} id="stateReportableFlag" name="stateReportableFlag">
            </div>
        </div>
        <div class="col-sm-2 my-1">
            <small id="errorAllowAwtFlag" class="form-text text-danger">&nbsp;{$errorAllowAwtFlag}</small>
            <label class="sr-only" for="inlineFormInputAllowAwtFlag">Allow Awt Flag</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Allow Awt Flag </div>
                </div>
                <input style="margin:10px;" type="checkbox" {($allowAwtFlag==true)?'checked':''} id="allowAwtFlag" name="allowAwtFlag">
            </div>
        </div>
        <div class="col-sm-2 my-1">
            <small id="errorGlobalFlag" class="form-text text-danger">&nbsp;{$errorGlobalFlag}</small>
            <label class="sr-only" for="inlineFormInputGlobalFlag">Global Flag</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Global Flag </div>
                </div>
                <input style="margin:10px;" type="checkbox" {($globalFlag==true)?'checked':''} id="globalFlag" name="globalFlag">
            </div>
        </div>
        <div class="col-sm-2 my-1">
            <small id="errorSensitiveVendor" class="form-text text-danger">&nbsp;{$errorSensitiveVendor}</small>
            <label class="sr-only" for="inlineFormInputSensitiveVendor">Sensitive Vendor</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Sensitive Vendor </div>
                </div>
                <input style="margin:10px;" type="checkbox" {($sensitiveVendor==true)?'checked':''} id="sensitiveVendor" name="sensitiveVendor">
            </div>
        </div>
    </div>

    {if $country=='NI'}
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small class="form-text text-danger">&nbsp;{$errorSupplierTypeNi}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Supplier Type &nbsp;<span class="flag-icon flag-icon-ni mr-2 ml-2" title="NI only"></span></div>
                </div>
                <select class="form-control" id="supplierTypeNi" name="supplierTypeNi">
                    <option {if $supplierTypeNi==='1'}selected{/if} value="1">1 : Legal Entity</option>
                    <option {if $supplierTypeNi==='2'}selected{/if} value="2">2 : Foreign Entity</option>
                    <option {if $supplierTypeNi==='3'}selected{/if} value="3">3 : Individual</option>
                </select>
            </div>
        </div>
    </div>
    {/if}
    {if $country=='GT'}
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small class="form-text text-danger">&nbsp;{$errorOrderNumberGt}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Order Number&nbsp;<span class="flag-icon flag-icon-gt mr-2 ml-2" title="GT only"></span></div>
                </div>
                <input type="text" class="form-control" id="orderNumberGt" name="orderNumberGt" value="{$orderNumberGt}" placeholder="Order Number of the cedula">
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small class="form-text text-danger">&nbsp;{$errorRegisterNumberGt}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Register Number&nbsp;<span class="flag-icon flag-icon-gt mr-2 ml-2" title="GT only"></span></div>
                </div>
                <input type="text" class="form-control" id="registerNumberGt" name="registerNumberGt" value="{$registerNumberGt}" placeholder="Register Number of the cedula">
            </div>
        </div>
    </div>
    {/if}
    {if $country=='UY'}
        <div class="form-row align-items-center">
            <div class="col-sm-12 my-1">
                <small class="form-text text-danger">&nbsp;{$errorContributorTypeUY}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Document Type&nbsp;<span class="flag-icon flag-icon-uy mr-2 ml-2" title="SV only"></span></div>
                    </div>
                    <select class="form-control" id="contributorTypeUY" name="contributorTypeUY">
                        <option {if $contributorTypeUY==='01'}selected{/if} value="01">01: Small Company and Others</option>
                        <option {if $contributorTypeUY==='02'}selected{/if} value="02">02: IRPF Contributor</option>
                        <option {if $contributorTypeUY==='05'}selected{/if} value="05">05: Foreign Contributor - Not Importation</option>
                        <option {if $contributorTypeUY==='06'}selected{/if} value="06">06: Company of Public Law</option>
                        <option {if $contributorTypeUY==='07'}selected{/if} value="07">07: Production Cooperative</option>
                        <option {if $contributorTypeUY==='08'}selected{/if} value="08">08: Business Groups</option>
                        <option {if $contributorTypeUY==='09'}selected{/if} value="09">09: Foreign Contributor - Importation</option>
                        <option {if $contributorTypeUY==='10'}selected{/if} value="10">10: Education Companies</option>
                        <option {if $contributorTypeUY==='11'}selected{/if} value="11">11: Banks</option>
                        <option {if $contributorTypeUY==='98'}selected{/if} value="98">98: Expenses of the Process of costs and / or Salaries</option>
                        <option {if $contributorTypeUY==='99'}selected{/if} value="99">99: Not Real Suppliers</option>
                        <option {if $contributorTypeUY==='00'}selected{/if} value="00">00: Not Assigned</option>
                    </select>
                </div>
            </div>
        </div>
    {/if}
    {if $country=='SV'}
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small class="form-text text-danger">&nbsp;{$errorSupplierTypeSv}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Supplier Type&nbsp;<span class="flag-icon flag-icon-sv mr-2 ml-2" title="SV only"></span></div>
                </div>
                <input type="text" class="form-control" id="supplierTypeSv" name="supplierTypeSv" value="{$supplierTypeSv}" placeholder="Supplier Type SV">
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small class="form-text text-danger">&nbsp;{$errorDocumentTypeSV}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Document Type&nbsp;<span class="flag-icon flag-icon-sv mr-2 ml-2" title="SV only"></span></div>
                </div>
                <select class="form-control" id="documentTypeSv" name="documentTypeSv">
                    <option {if $documentTypeSv==='1'}selected{/if} value="1">1 : NIT</option>
                    <option {if $documentTypeSv==='3'}selected{/if} value="3">3 : DUI</option>
                    <option {if $documentTypeSv==='4'}selected{/if} value="4">4 : Passport</option>
                    <option {if $documentTypeSv==='5'}selected{/if} value="5">5 : Residence Card</option>
                    <option {if $documentTypeSv==='6'}selected{/if} value="6">6 : Others</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small class="form-text text-danger">&nbsp;{$errorContributorRegistrationSv}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Contributor Registration &nbsp;<span class="flag-icon flag-icon-sv mr-2 ml-2" title="SV only"></span></div>
                </div>
                <input type="text" class="form-control" id="contributorRegistrationSv" name="contributorRegistrationSv" value="{$contributorRegistrationSv}" placeholder="Contributor Registration SV">
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small class="form-text text-danger">&nbsp;{$errorLegalRepresentativeSv}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Legal Representative &nbsp;<span class="flag-icon flag-icon-sv mr-2 ml-2" title="SV only"></span></div>
                </div>
                <input type="text" class="form-control" id="legalRepresentativeSv" name="legalRepresentativeSv" value="{$legalRepresentativeSv}" placeholder="Legal Representative SV">
            </div>
        </div>
    </div>
    {/if}

    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorAliases" class="form-text text-danger">&nbsp;{$errorAliases}</small>
            <label class="sr-only" for="inlineFormInputAliases">Aliases</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Aliases</div>
                </div>
                <textarea type="text" class="form-control" id="aliases" name="aliases" value="" placeholder="Aliases">{$aliases}</textarea>
            </div>
            <label class="alert-info text-info"><i class="fas fa-id-card fa-sm fa-fw"></i> An alias is an alternative ID fot this item.  Use this field to unify records to be migrated to ORACLE. Use a comma-separated list, with no spaces (e.g. PA1000,PA1002,CO1003)</label>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorAliases" class="form-text text-danger">&nbsp;{$errorObservations}</small>
            <label class="sr-only" for="inlineFormInputObservations">Observations </label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Observations</div>
                </div>
                <textarea type="text" class="form-control" id="observations" name="observations" value="" placeholder="Observations" rows="5">{$observations}</textarea>
            </div>
            <label class="alert-info text-info"><i class="fas fa-bug fa-sm fa-fw"></i> Please help us kill bugs.  Use this observation field to annotate special cases, cleansing decisions, and other relevant issues related to this record.</label>
        </div>
    </div>
    <br/>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">{$actionLabel}</button>
                <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
            </div>
        </div>
    </div>
</form>
<br>
{if isset($children1) && (sizeof($children1)>0)}
    <h4>Vendor Sites <a data-toggle="tooltip" data-placement="top" title="add a new Site to {$segment1}" href="{base_url()}VendorSite/createNew/{$id}" style="cursor:pointer"><small><i class="text-primary fas fa-plus-circle fa-sm fa-fw"></i></small></a> </h4>
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th scope="col" style="width:90px"></th>
            <th style="width:90px">#</th>
            <th>Action</th>
            <th>Vendor Site Code</th>
            <th>Address Line 1</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$children1 item=c1}
            <tr class="{if ($c1.dismissed)} dismissed {elseif (($c1.locallyValidated)&&($c1.crossRefValidated)) } alert-success {else} alert-warning {/if} clickable-row" style="cursor:pointer" data-href='{base_url()}VendorSite/customEditCleansed/{$c1.country}/{$c1.id}'>
                <th scope="row">
                    <a href="{base_url()}VendorSite/editCleansed/{$c1.id}" data-toggle="tooltip" data-placement="top" title="quick view of address {$c1.vendorSiteCode}"><i class="text-primary fas fa-edit fa-sm fa-fw"></i></a>
                    <a href="{base_url()}VendorSite/toggle/{$c1.id}" data-toggle="tooltip" data-placement="top" title="{if ($c1.dismissed)}Recall{else}Dismiss{/if} {$c1.vendorSiteCode}"><i class="text-primary fas fa-minus-circle fa-sm fa-fw"></i></a>
                </th>
                <th scope="row" style="width:90px">{$c1.id}</th>
                <td>{$c1.action|truncate:40}</td>
                <td>{$c1.vendorSiteCode}</td>
                <td>{$c1.addressLine1}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{/if}
<br/>
{if isset($children3) && (sizeof($children3)>0)}
    <h4>Vendor Bank Accounts <a data-toggle="tooltip" data-placement="top" title="add a new Bank Account to {$segment1}" href="{base_url()}VendorBankAccount/createNew/{$id}" style="cursor:pointer"><small><i class="text-primary fas fa-plus-circle fa-sm fa-fw"></i></small></a></h4>
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th scope="col" style="width:90px"></th>
            <th style="width:90px">#</th>
            <th>Action</th>
            <th>Bank Name</th>
            <th>Bank Account Num</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$children3 item=c3}
            <tr class="{if ($c3.dismissed)} dismissed {elseif (($c3.locallyValidated)&&($c3.crossRefValidated)) } alert-success {else} alert-warning {/if} clickable-row" style="cursor:pointer" data-href='{base_url()}VendorBankAccount/customEditCleansed/{$c3.country}/{$c3.id}'>
                <th scope="row">
                    <a href="{base_url()}VendorBankAccount/editCleansed/{$c3.id}" data-toggle="tooltip" data-placement="top" title="quick view of bank account {$c3.bankAccountNum}"><i class="text-primary fas fa-edit fa-sm fa-fw"></i></a>
                    <a href="{base_url()}VendorBankAccount/toggle/{$c3.id}" data-toggle="tooltip" data-placement="top" title="{if ($c3.dismissed)}Recall{else}Dismiss{/if} bank account {$c3.bankAccountNum}"><i class="text-primary fas fa-minus-circle fa-sm fa-fw"></i></a>
                </th>
                <th scope="row" style="width:90px">{$c3.id}</th>
                <td>{$c3.action|truncate:40}</td>
                <td>{$c3.bankName}</td>
                <td>{$c3.bankAccountNum}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{/if}
<br/>
<br/>
<br/>
<br/>
<br/>