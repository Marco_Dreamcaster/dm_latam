<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="navbar-nav mr-auto mt-3">
            <h5 class="nav p-2"><i class="fas fa-lg fa-trash-alt fa-fw m-2  text-pre-stage"></i>&nbsp;<span class="m-2"><span class="flag-icon flag-icon-{strtolower($country)}"></span> {strtoupper($country)} pre-STG Vendors (dirty)</span></h5>
            <ul class="navbar-nav mr-auto">
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_blank" href="{base_url()}Vendors/cleansed/{$country}" data-toggle="tooltip" data-placement="top" title="cleansed Vendors"><i class="fas fa-industry fa-sm fa-fw"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-primary" target="_blank" href="{base_url()}Maps/ORAGeoCodes" data-toggle="tooltip" data-placement="top" title="Oracle Geo Codes"><i class="fas fa-map-marker-alt fa-sm fa-fw"></i></a>
                </li>
                <li class="nav-item pt-2 p-1">
                    <a class="btn btn-success" target="_blank" href="{base_url()}IDL/Navigate/navigateGroup/{$country}/Vendors" data-toggle="tooltip" data-placement="top" title="specification for {$country} Vendors group"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                </li>
            </ul>
        </div>
        &nbsp;
    </div>
</nav>
<br/>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade active show" id="pills-data" role="tabpanel" aria-labelledby="pills-data-tab">
        {if $isPaginationNeeded}
        <ul class="pagination">
            <li class="page-item {if $currentPage==0}active{/if}">
                <a class="page-link" href="{base_url()}{$listAction}0" aria-label="First">
                    First
                </a>
            </li>
            {for $foo=1 to ($lastPage-1)}
                <li class="page-item {if $currentPage==$foo}active{/if}"><a class="page-link" href="{base_url()}{$listAction}{$foo}">{$foo}</a></li>
            {/for}
            <li class="page-item {if $currentPage==$lastPage}active{/if}">
                <a class="page-link" href="{base_url()}{$listAction}{$lastPage}" aria-label="Last">
                    Last
                </a>
            </li>
        </ul>
        {/if}
        <br/>
        <table class="table table-hover table-sm">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th>Vendor Name</th>
                <th>Vendor Number</th>
                <th>action</th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$dirtyVOs item=p}
                <tr class="{if ($p.recommended) } alert-success {else} alert-warning {/if} clickable-row" style="cursor:pointer" data-href='{base_url()}Vendors/editDirty/{$p.id}' title="inspect all fields from dirty Vendor {$p.vendorName}">
                    <td scope="row">{$p.id}</td>
                    <td>{$p.vendorName}</td>
                    <td>{$p.segment1}</td>
                    <td>{$p.action}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
    <div class="tab-pane fade show" id="pills-stats" role="tabpanel" aria-labelledby="pills-stats-tab">
        <div class="row p-1">
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Dirty</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Available to be cleansed</h6>
                        <h1 class="card-text">{$dirtyDirty}</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Cleansed</h5>
                        <h6 class="card-subtitle mb-2 text-muted">With matching records on staging table</h6>
                        <h1 class="card-text">{$cleansedDirty}</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Total</h5>
                        <h6 class="card-subtitle mb-2 text-muted">All records from pre-staging table</h6>
                        <h1 class="card-text">{$total}</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="row p-1">
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Recommended</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Recommended records according to fixed functional criteria</h6>
                        <h1 class="card-text">{$recommendedDirty}</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Not Recommended</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Other records on pre-staging table</h6>
                        <h1 class="card-text">{$unrecommendedDirty}</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Multiple Aliases</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Records that are referred by more than one alias ID</h6>
                        <h1 class="card-text">??</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<br/>
<br/>
<br/>
<br/>
