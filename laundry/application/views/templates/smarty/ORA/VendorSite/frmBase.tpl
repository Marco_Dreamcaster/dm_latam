<br/>
<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>
    <div class="form-group">
        <label for="vendorNumber">Vendor Number <small id="vendorNumberHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="vendorNumber" name="vendorNumber" aria-describedby="vendorNumberHelp" value="{$vendorNumber}" placeholder="Vendor Number...">
        <small id="errorVendorNumber" class="form-text text-danger">{$errorVendorNumber}</small>
    </div>
    <div class="form-group">
        <label for="vendorSiteCode">Vendor Site Code <small id="vendorSiteCodeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="vendorSiteCode" name="vendorSiteCode" aria-describedby="vendorSiteCodeHelp" value="{$vendorSiteCode}" placeholder="Vendor Site Code...">
        <small id="errorVendorSiteCode" class="form-text text-danger">{$errorVendorSiteCode}</small>
    </div>
    <div class="form-group">
        <label for="vendorSiteCodeAlt">Vendor Site Code Alt <small id="vendorSiteCodeAltHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="vendorSiteCodeAlt" name="vendorSiteCodeAlt" aria-describedby="vendorSiteCodeAltHelp" value="{$vendorSiteCodeAlt}" placeholder="Vendor Site Code Alt...">
        <small id="errorVendorSiteCodeAlt" class="form-text text-danger">{$errorVendorSiteCodeAlt}</small>
    </div>
    <div class="form-group">
        <label for="addressLine1">Address Line1 <small id="addressLine1Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="addressLine1" name="addressLine1" aria-describedby="addressLine1Help" value="{$addressLine1}" placeholder="Address Line1...">
        <small id="errorAddressLine1" class="form-text text-danger">{$errorAddressLine1}</small>
    </div>
    <div class="form-group">
        <label for="addressLine2">Address Line2 <small id="addressLine2Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="addressLine2" name="addressLine2" aria-describedby="addressLine2Help" value="{$addressLine2}" placeholder="Address Line2...">
        <small id="errorAddressLine2" class="form-text text-danger">{$errorAddressLine2}</small>
    </div>
    <div class="form-group">
        <label for="addressLine3">Address Line3 <small id="addressLine3Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="addressLine3" name="addressLine3" aria-describedby="addressLine3Help" value="{$addressLine3}" placeholder="Address Line3...">
        <small id="errorAddressLine3" class="form-text text-danger">{$errorAddressLine3}</small>
    </div>
    <div class="form-group">
        <label for="addressLine4">Address Line4 <small id="addressLine4Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="addressLine4" name="addressLine4" aria-describedby="addressLine4Help" value="{$addressLine4}" placeholder="Address Line4...">
        <small id="errorAddressLine4" class="form-text text-danger">{$errorAddressLine4}</small>
    </div>
    <div class="form-group">
        <label for="addressLineAlt">Address Line Alt <small id="addressLineAltHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="addressLineAlt" name="addressLineAlt" aria-describedby="addressLineAltHelp" value="{$addressLineAlt}" placeholder="Address Line Alt...">
        <small id="errorAddressLineAlt" class="form-text text-danger">{$errorAddressLineAlt}</small>
    </div>
    <div class="form-group">
        <label for="city">City <small id="cityHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="city" name="city" aria-describedby="cityHelp" value="{$city}" placeholder="City...">
        <small id="errorCity" class="form-text text-danger">{$errorCity}</small>
    </div>
    <div class="form-group">
        <label for="county">County <small id="countyHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="county" name="county" aria-describedby="countyHelp" value="{$county}" placeholder="County...">
        <small id="errorCounty" class="form-text text-danger">{$errorCounty}</small>
    </div>
    <div class="form-group">
        <label for="state">State <small id="stateHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="state" name="state" aria-describedby="stateHelp" value="{$state}" placeholder="State...">
        <small id="errorState" class="form-text text-danger">{$errorState}</small>
    </div>
    <div class="form-group">
        <label for="province">Province <small id="provinceHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="province" name="province" aria-describedby="provinceHelp" value="{$province}" placeholder="Province...">
        <small id="errorProvince" class="form-text text-danger">{$errorProvince}</small>
    </div>
    <div class="form-group">
        <label for="zip">Zip <small id="zipHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="zip" name="zip" aria-describedby="zipHelp" value="{$zip}" placeholder="Zip...">
        <small id="errorZip" class="form-text text-danger">{$errorZip}</small>
    </div>
    <div class="form-group">
        <label for="country">Country <small id="countryHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="country" name="country" aria-describedby="countryHelp" value="{$country}" placeholder="Country...">
        <small id="errorCountry" class="form-text text-danger">{$errorCountry}</small>
    </div>
    <div class="form-group">
        <label for="areaCode">Area Code <small id="areaCodeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="areaCode" name="areaCode" aria-describedby="areaCodeHelp" value="{$areaCode}" placeholder="Area Code...">
        <small id="errorAreaCode" class="form-text text-danger">{$errorAreaCode}</small>
    </div>
    <div class="form-group">
        <label for="phone">Phone <small id="phoneHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="phone" name="phone" aria-describedby="phoneHelp" value="{$phone}" placeholder="Phone...">
        <small id="errorPhone" class="form-text text-danger">{$errorPhone}</small>
    </div>
    <div class="form-group">
        <label for="faxAreaCode">Fax Area Code <small id="faxAreaCodeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="faxAreaCode" name="faxAreaCode" aria-describedby="faxAreaCodeHelp" value="{$faxAreaCode}" placeholder="Fax Area Code...">
        <small id="errorFaxAreaCode" class="form-text text-danger">{$errorFaxAreaCode}</small>
    </div>
    <div class="form-group">
        <label for="fax">Fax <small id="faxHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="fax" name="fax" aria-describedby="faxHelp" value="{$fax}" placeholder="Fax...">
        <small id="errorFax" class="form-text text-danger">{$errorFax}</small>
    </div>
    <div class="form-group">
        <label for="paymentMethodLookupCode">Payment Method Lookup Code <small id="paymentMethodLookupCodeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="paymentMethodLookupCode" name="paymentMethodLookupCode" aria-describedby="paymentMethodLookupCodeHelp" value="{$paymentMethodLookupCode}" placeholder="Payment Method Lookup Code...">
        <small id="errorPaymentMethodLookupCode" class="form-text text-danger">{$errorPaymentMethodLookupCode}</small>
    </div>
    <div class="form-group">
        <label for="vatCode">Vat Code <small id="vatCodeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="vatCode" name="vatCode" aria-describedby="vatCodeHelp" value="{$vatCode}" placeholder="Vat Code...">
        <small id="errorVatCode" class="form-text text-danger">{$errorVatCode}</small>
    </div>
    <div class="form-group">
        <label for="acctsPayAccount">Accts Pay Account <small id="acctsPayAccountHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="acctsPayAccount" name="acctsPayAccount" aria-describedby="acctsPayAccountHelp" value="{$acctsPayAccount}" placeholder="Accts Pay Account...">
        <small id="errorAcctsPayAccount" class="form-text text-danger">{$errorAcctsPayAccount}</small>
    </div>
    <div class="form-group">
        <label for="prepayAccount">Prepay Account <small id="prepayAccountHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="prepayAccount" name="prepayAccount" aria-describedby="prepayAccountHelp" value="{$prepayAccount}" placeholder="Prepay Account...">
        <small id="errorPrepayAccount" class="form-text text-danger">{$errorPrepayAccount}</small>
    </div>
    <div class="form-group">
        <label for="payGroupLookupCode">Pay Group Lookup Code <small id="payGroupLookupCodeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="payGroupLookupCode" name="payGroupLookupCode" aria-describedby="payGroupLookupCodeHelp" value="{$payGroupLookupCode}" placeholder="Pay Group Lookup Code...">
        <small id="errorPayGroupLookupCode" class="form-text text-danger">{$errorPayGroupLookupCode}</small>
    </div>
    <div class="form-group">
        <label for="invoiceCurrencyCode">Invoice Currency Code <small id="invoiceCurrencyCodeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="invoiceCurrencyCode" name="invoiceCurrencyCode" aria-describedby="invoiceCurrencyCodeHelp" value="{$invoiceCurrencyCode}" placeholder="Invoice Currency Code...">
        <small id="errorInvoiceCurrencyCode" class="form-text text-danger">{$errorInvoiceCurrencyCode}</small>
    </div>
    <div class="form-group">
        <label for="language">Language <small id="languageHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="language" name="language" aria-describedby="languageHelp" value="{$language}" placeholder="Language...">
        <small id="errorLanguage" class="form-text text-danger">{$errorLanguage}</small>
    </div>
    <div class="form-group">
        <label for="termsName">Terms Name <small id="termsNameHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="termsName" name="termsName" aria-describedby="termsNameHelp" value="{$termsName}" placeholder="Terms Name...">
        <small id="errorTermsName" class="form-text text-danger">{$errorTermsName}</small>
    </div>
    <div class="form-group">
        <label for="emailAddress">Email Address <small id="emailAddressHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="emailAddress" name="emailAddress" aria-describedby="emailAddressHelp" value="{$emailAddress}" placeholder="Email Address...">
        <small id="errorEmailAddress" class="form-text text-danger">{$errorEmailAddress}</small>
    </div>
    <div class="form-group">
        <label for="purchasingSiteFlag">PurchasingSiteFlag</label><br/>
        <small id="purchasingSiteFlagHelp" class="form-text text-muted">()</small>
        <input type="checkbox" {($purchasingSiteFlag==true)?'checked':''} id="purchasingSiteFlag" name="purchasingSiteFlag">
    </div>
    <div class="form-group">
        <label for="paySiteFlag">PaySiteFlag</label><br/>
        <small id="paySiteFlagHelp" class="form-text text-muted">()</small>
        <input type="checkbox" {($paySiteFlag==true)?'checked':''} id="paySiteFlag" name="paySiteFlag">
    </div>
    <div class="form-group">
        <label for="vatRegistrationNum">Vat Registration Num <small id="vatRegistrationNumHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="vatRegistrationNum" name="vatRegistrationNum" aria-describedby="vatRegistrationNumHelp" value="{$vatRegistrationNum}" placeholder="Vat Registration Num...">
        <small id="errorVatRegistrationNum" class="form-text text-danger">{$errorVatRegistrationNum}</small>
    </div>
    <div class="form-group">
        <label for="shipToLocationCode">Ship To Location Code <small id="shipToLocationCodeHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="shipToLocationCode" name="shipToLocationCode" aria-describedby="shipToLocationCodeHelp" value="{$shipToLocationCode}" placeholder="Ship To Location Code...">
        <small id="errorShipToLocationCode" class="form-text text-danger">{$errorShipToLocationCode}</small>
    </div>
    <div class="form-group">
        <label for="primaryPaySiteFlag">PrimaryPaySiteFlag</label><br/>
        <small id="primaryPaySiteFlagHelp" class="form-text text-muted">()</small>
        <input type="checkbox" {($primaryPaySiteFlag==true)?'checked':''} id="primaryPaySiteFlag" name="primaryPaySiteFlag">
    </div>
    <div class="form-group">
        <label for="gaplessInvNumFlag">GaplessInvNumFlag</label><br/>
        <small id="gaplessInvNumFlagHelp" class="form-text text-muted">()</small>
        <input type="checkbox" {($gaplessInvNumFlag==true)?'checked':''} id="gaplessInvNumFlag" name="gaplessInvNumFlag">
    </div>
    <div class="form-group">
        <label for="offsetTaxFlag">OffsetTaxFlag</label><br/>
        <small id="offsetTaxFlagHelp" class="form-text text-muted">()</small>
        <input type="checkbox" {($offsetTaxFlag==true)?'checked':''} id="offsetTaxFlag" name="offsetTaxFlag">
    </div>
    <div class="form-group">
        <label for="alwaysTakeDiscFlag">AlwaysTakeDiscFlag</label><br/>
        <small id="alwaysTakeDiscFlagHelp" class="form-text text-muted">()</small>
        <input type="checkbox" {($alwaysTakeDiscFlag==true)?'checked':''} id="alwaysTakeDiscFlag" name="alwaysTakeDiscFlag">
    </div>
    <div class="form-group">
        <label for="excludeFreightFromDiscount">ExcludeFreightFromDiscount</label><br/>
        <small id="excludeFreightFromDiscountHelp" class="form-text text-muted">()</small>
        <input type="checkbox" {($excludeFreightFromDiscount==true)?'checked':''} id="excludeFreightFromDiscount" name="excludeFreightFromDiscount">
    </div>
    <div class="form-group">
        <label for="globalAttribute17">GlobalAttribute17</label><br/>
        <small id="globalAttribute17Help" class="form-text text-muted">()</small>
        <input type="checkbox" {($globalAttribute17==true)?'checked':''} id="globalAttribute17" name="globalAttribute17">
    </div>
    <div class="form-group">
        <label for="globalAttribute18">GlobalAttribute18</label><br/>
        <small id="globalAttribute18Help" class="form-text text-muted">()</small>
        <input type="checkbox" {($globalAttribute18==true)?'checked':''} id="globalAttribute18" name="globalAttribute18">
    </div>
    <div class="form-group">
        <label for="globalAttribute19">Global Attribute19 <small id="globalAttribute19Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="globalAttribute19" name="globalAttribute19" aria-describedby="globalAttribute19Help" value="{$globalAttribute19}" placeholder="Global Attribute19...">
        <small id="errorGlobalAttribute19" class="form-text text-danger">{$errorGlobalAttribute19}</small>
    </div>
    <div class="form-group">
        <label for="globalAttribute20">Global Attribute20 <small id="globalAttribute20Help" class="form-text text-muted">()</small></label>
        <input class="form-control" id="globalAttribute20" name="globalAttribute20" aria-describedby="globalAttribute20Help" width="276" />
        <script>
            $('#globalAttribute20').datepicker({
                uiLibrary: 'bootstrap4',
                format:'dd-mm-yyyy',
                value: '{$globalAttribute20|date_format:"%d-%m-%Y"}'
            });
        </script>
        <small id="errorGlobalAttribute20" class="form-text text-danger">{$errorGlobalAttribute20}</small>
    </div>
    <div class="form-group">
        <label for="attributeCategory">Attribute Category <small id="attributeCategoryHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attributeCategory" name="attributeCategory" aria-describedby="attributeCategoryHelp" value="{$attributeCategory}" placeholder="Attribute Category...">
        <small id="errorAttributeCategory" class="form-text text-danger">{$errorAttributeCategory}</small>
    </div>
    <div class="form-group">
        <label for="attribute1">Attribute1 <small id="attribute1Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute1" name="attribute1" aria-describedby="attribute1Help" value="{$attribute1}" placeholder="Attribute1...">
        <small id="errorAttribute1" class="form-text text-danger">{$errorAttribute1}</small>
    </div>
    <div class="form-group">
        <label for="attribute2">Attribute2 <small id="attribute2Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute2" name="attribute2" aria-describedby="attribute2Help" value="{$attribute2}" placeholder="Attribute2...">
        <small id="errorAttribute2" class="form-text text-danger">{$errorAttribute2}</small>
    </div>
    <div class="form-group">
        <label for="attribute3">Attribute3 <small id="attribute3Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute3" name="attribute3" aria-describedby="attribute3Help" value="{$attribute3}" placeholder="Attribute3...">
        <small id="errorAttribute3" class="form-text text-danger">{$errorAttribute3}</small>
    </div>
    <div class="form-group">
        <label for="attribute4">Attribute4 <small id="attribute4Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute4" name="attribute4" aria-describedby="attribute4Help" value="{$attribute4}" placeholder="Attribute4...">
        <small id="errorAttribute4" class="form-text text-danger">{$errorAttribute4}</small>
    </div>
    <div class="form-group">
        <label for="attribute5">Attribute5 <small id="attribute5Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute5" name="attribute5" aria-describedby="attribute5Help" value="{$attribute5}" placeholder="Attribute5...">
        <small id="errorAttribute5" class="form-text text-danger">{$errorAttribute5}</small>
    </div>
    <div class="form-group">
        <label for="attribute6">Attribute6 <small id="attribute6Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute6" name="attribute6" aria-describedby="attribute6Help" value="{$attribute6}" placeholder="Attribute6...">
        <small id="errorAttribute6" class="form-text text-danger">{$errorAttribute6}</small>
    </div>
    <div class="form-group">
        <label for="attribute7">Attribute7 <small id="attribute7Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute7" name="attribute7" aria-describedby="attribute7Help" value="{$attribute7}" placeholder="Attribute7...">
        <small id="errorAttribute7" class="form-text text-danger">{$errorAttribute7}</small>
    </div>
    <div class="form-group">
        <label for="attribute8">Attribute8 <small id="attribute8Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute8" name="attribute8" aria-describedby="attribute8Help" value="{$attribute8}" placeholder="Attribute8...">
        <small id="errorAttribute8" class="form-text text-danger">{$errorAttribute8}</small>
    </div>
    <div class="form-group">
        <label for="attribute9">Attribute9 <small id="attribute9Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute9" name="attribute9" aria-describedby="attribute9Help" value="{$attribute9}" placeholder="Attribute9...">
        <small id="errorAttribute9" class="form-text text-danger">{$errorAttribute9}</small>
    </div>
    <div class="form-group">
        <label for="attribute10">Attribute10 <small id="attribute10Help" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="attribute10" name="attribute10" aria-describedby="attribute10Help" value="{$attribute10}" placeholder="Attribute10...">
        <small id="errorAttribute10" class="form-text text-danger">{$errorAttribute10}</small>
    </div>
    <div class="form-group">
        <label for="orgName">Org Name <small id="orgNameHelp" class="form-text text-muted">()</small></label>
        <input type="text" class="form-control" id="orgName" name="orgName" aria-describedby="orgNameHelp" value="{$orgName}" placeholder="Org Name...">
        <small id="errorOrgName" class="form-text text-danger">{$errorOrgName}</small>
    </div>

    <br/>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>
</form>
<br/>
<br/>
<br/>
<br/>
<br/>
