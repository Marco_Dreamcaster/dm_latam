<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorVendorNumber" class="form-text text-danger">&nbsp;{$errorVendorNumber}</small>
            <label class="sr-only" for="inlineFormInputVendorNumber">Vendor Number</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Vendor Number</div>
                </div>
                <input type="text" class="form-control" id="vendorNumber" name="vendorNumber" value="{$vendorNumber}" placeholder="Vendor Number" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorVendorName" class="form-text text-danger">&nbsp;{$errorVendorName}</small>
            <label class="sr-only" for="inlineFormInputVendorName">Vendor Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Vendor Name</div>
                </div>
                <input type="text" class="form-control" id="vendorName" name="vendorName" value="{$vendorName}" placeholder="Vendor Name" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorVendorSiteCode" class="form-text text-danger">&nbsp;{$errorVendorSiteCode}</small>
            <label class="sr-only" for="inlineFormInputVendorSiteCode">Vendor Site Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Vendor Site Code </div>
                </div>
                <input type="text" class="form-control" id="vendorSiteCode" name="vendorSiteCode" value="{$vendorSiteCode}" placeholder="Vendor Site Code" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorVendorSiteCodeAlt" class="form-text text-danger">&nbsp;{$errorVendorSiteCodeAlt}</small>
            <label class="sr-only" for="inlineFormInputVendorSiteCodeAlt">Vendor Site Code Alternative</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-warning"><a title="preencher com c�digo que ir� para flat file">Vendor Site Code Alternative</a></div>
                </div>
                <input type="text" class="form-control" id="vendorSiteCodeAlt" name="vendorSiteCodeAlt" value="{$vendorSiteCodeAlt}" placeholder="code on Sha's flat file">
            </div>
        </div>
    </div>

    {include file='../../MAPS/ORAGeoCodes/AddressStyleAwareWidget.tpl'}

    <div class="form-row align-items-center">
        <div class="col-sm-10 my-1">
            <small id="errorAddressLine1" class="form-text text-danger">&nbsp;{$errorAddressLine1}</small>
            <label class="sr-only" for="inlineFormInputAddressLine1">Address Line1</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Address Line1 </div>
                </div>
                <input type="text" class="form-control" id="addressLine1" name="addressLine1" value="{$addressLine1}" placeholder="Address Line1">
            </div>
        </div>
        <div class="col-sm-2 my-1">
            <small id="errorZip" class="form-text text-danger">&nbsp;{$errorZip}</small>
            <label class="sr-only" for="inlineFormInputZip">Zip</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Zip</div>
                </div>
                <input type="text" class="form-control" id="zip" name="zip" value="{$zip}" placeholder="Zip">
            </div>
        </div>
    </div>

    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorClassificationCode " class="form-text text-danger">&nbsp;{$errorClassificationTypeCode}</small>
            <label class="sr-only" for="inlineFormInputFiscalClassificationTypeCode">Fiscal Classification Type Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Type Code</div>
                </div>
                <select class="form-control" id="classificationTypeCode" name="classificationTypeCode">
                    <option value="">--</option>
                    <option {if $classificationTypeCode ==='TKE_LATAM_PA_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_PA_CONTRIBUTOR">TKE LATAM PA CONTRIBUTOR</option>
                </select>
            </div>
        </div>
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorClassificationCode " class="form-text text-danger">&nbsp;{$errorClassificationCode}</small>
            <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Code</div>
                </div>
                <select class="form-control" id="classificationCode" name="classificationCode" placeholder="Classification Code ">
                    <option value="">--</option>
                    <option {if $classificationCode ==='AUTORETENEDOR'}selected{/if} value="AUTORETENEDOR">AUTORETENEDOR</option>
                    <option {if $classificationCode ==='CONTRIBUYENTE'}selected{/if} value="CONTRIBUYENTE">CONTRIBUYENTE</option>
                    <option {if $classificationCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO</option>

                </select>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorClassificationStartDate" class="form-text text-danger">&nbsp;{$errorClassificationStartDate}</small>
            <label class="sr-only" for="inlineFormClassificationStartDate">Fiscal Classification Start Date</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Start Date</div>
                </div>
                <input class="form-control" id="classificationStartDate" name="classificationStartDate" aria-describedby="classificationStartDateHelp" />
                <script>
                    $('#classificationStartDate').datepicker({
                        uiLibrary: 'bootstrap4',
                        format:'dd-mm-yyyy',
                        value: '{$classificationStartDate|date_format:"%d-%m-%Y"}'
                    });
                </script>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="form-row align-items-center">
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorPurchasingSiteFlag class="form-text text-danger">&nbsp;{$errorPurchasingSiteFlag}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Purchasing Site</div>
                    </div>
                    <input type="checkbox" {($purchasingSiteFlag==true)?'checked':''} style="margin:10px;" id="purchasingSiteFlag" name="purchasingSiteFlag">
                </div>
            </div>
        </div>
    </div>

    <div class="form-row align-items-center">
        <div class="form-row align-items-center">
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorPaySiteFlag" class="form-text text-danger">&nbsp;{$errorPaySiteFlag}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Pay Site</div>
                    </div>
                    <input type="checkbox" {($paySite==true)?'checked':''} style="margin:10px;" id="paySite" name="primaryFlag">
                </div>
            </div>
        </div>
    </div>
    <br/>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">{$actionLabel}</button>
                <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
            </div>
        </div>
    </div>

</form>

<br>
{if isset($contacts) && (sizeof($contacts)>0)}
    <h4>Vendor Site Contacts  <a data-toggle="tooltip" data-placement="top" title="add a new Contact to {$vendorSiteCode}" href="{base_url()}VendorSiteContact/createNew/{$id}" style="cursor:pointer"><small><i class="text-primary fas fa-plus-circle fa-sm fa-fw"></i></small></a> </h4>
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th scope="col"></th>
            <th>#</th>
            <th>Action</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Email Address</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$contacts item=c}
            <tr class="{if ($c.dismissed)} dismissed {elseif (($c.locallyValidated)&&($c.crossRefValidated)) } alert-success {else} alert-warning {/if} clickable-row" style="cursor:pointer" data-href='{base_url()}VendorSiteContact/customEditCleansed/{$c.country}/{$c.id}'>
                <th scope="row">
                    <a href="{base_url()}VendorSiteContact/editCleansed/{$c.id}" data-toggle="tooltip" data-placement="top" title="inspect all fields from contact {$c.lastName}"><i class="text-primary fas fa-edit fa-sm fa-fw"></i></a>
                    <a href="{base_url()}VendorSiteContact/toggle/{$c.id}" data-toggle="tooltip" data-placement="top" title="{if ($c.dismissed)}Recall{else}Dismiss{/if} {$c.lastName}"><i class="text-primary fas fa-minus-circle fa-sm fa-fw"></i></a>
                </th>
                <th scope="row">{$c.id}</th>
                <td>{$c.action}</td>
                <td>{$c.firstName}</td>
                <td>{$c.lastName}</td>
                <td>{$c.phoneNumber}</td>
                <td>{$c.emailAddress}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{/if}
<br/>
<br/>
<br/>
<br/>
<br/>

