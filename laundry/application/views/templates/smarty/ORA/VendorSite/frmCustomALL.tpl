<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorVendorNumber" class="form-text text-danger">&nbsp;{$errorVendorNumber}</small>
            <label class="sr-only" for="inlineFormInputVendorNumber">Vendor Number</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Vendor Number</div>
                </div>
                <input type="text" class="form-control" id="vendorNumber" name="vendorNumber" value="{$vendorNumber}" placeholder="Vendor Number" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorVendorName" class="form-text text-danger">&nbsp;{$errorVendorName}</small>
            <label class="sr-only" for="inlineFormInputVendorName">Vendor Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Vendor Name</div>
                </div>
                <input type="text" class="form-control" id="vendorName" name="vendorName" value="{$vendorName}" placeholder="Vendor Name" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorVendorSiteCode" class="form-text text-danger">&nbsp;{$errorVendorSiteCode}</small>
            <label class="sr-only" for="inlineFormInputVendorSiteCode">Vendor Site Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Vendor Site Code </div>
                </div>
                <input type="text" class="form-control" id="vendorSiteCode" name="vendorSiteCode" value="{$vendorSiteCode}" placeholder="Vendor Site Code" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorVendorSiteCodeAlt" class="form-text text-danger">&nbsp;{$errorVendorSiteCodeAlt}</small>
            <label class="sr-only" for="inlineFormInputVendorSiteCodeAlt">Vendor Site Code Alternative</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-warning"><a title="preencher com c�digo que ir� para flat file">Vendor Site Code Alternative</a></div>
                </div>
                <input type="text" class="form-control" id="vendorSiteCodeAlt" name="vendorSiteCodeAlt" value="{$vendorSiteCodeAlt}" placeholder="code on Sha's flat file">
            </div>
        </div>
    </div>

    {if ($country=='CO')}
        <div class="form-row align-items-center">
            <div class="col-sm-1 col-md-12 my-1">
                <label class="sr-only" for="inlineFormInputShipToLocationCode">ShipTo Location</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text alert-warning">ShipTo Location</div>
                    </div>
                    <select class="form-control" id="shipToLocationCode" name="shipToLocationCode">
                        <option value="">--</option>
                        <option {if $shipToLocationCode==='CO - BARRANQUILLA'}selected{/if} value='CO - BARRANQUILLA'>CO - BARRANQUILLA</option>
                        <option {if $shipToLocationCode==='CO - BOGOTA'}selected{/if} value='CO - BOGOTA'>CO - BOGOTA</option>
                        <option {if $shipToLocationCode==='CO - BUCARAMANGA'}selected{/if} value='CO - BUCARAMANGA'>CO - BUCARAMANGA</option>
                        <option {if $shipToLocationCode==='CO - CALI'}selected{/if} value='CO - CALI'>CO - CALI</option>
                        <option {if $shipToLocationCode==='CO - CARTAGENA'}selected{/if} value='CO - CARTAGENA'>CO - CARTAGENA</option>
                        <option {if $shipToLocationCode==='CO - MEDELLIN'}selected{/if} value='CO - MEDELLIN'>CO - MEDELLIN</option>
                        <option {if $shipToLocationCode==='CO - PEREIRA'}selected{/if} value='CO - PEREIRA'>CO - PEREIRA</option>
                    </select>
                </div>
            </div>
        </div>
    {/if}

    {if ($country=='PE')}
        <div class="form-row align-items-center">
            <div class="col-6 my-1">
                <small id="errorExchangePaymentPE" class="form-text text-danger">&nbsp;{$errorExchangePaymentPE}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">ExchangePayment&nbsp;
                            <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                        </div>
                    </div>
                    <input class="form-control" id="exchangePaymentPE" name="exchangePaymentPE" value="{$exchangePaymentPE}"/>
                </div>
            </div>
            <div class="col-6 my-1">
                <small id="errorPrimarySitePE" class="form-text text-danger">&nbsp;{$errorPrimarySitePE}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Primary Site&nbsp;
                            <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                        </div>
                    </div>
                    <select class="form-control" id="primarySitePE" name="primarySitePE">
                        <option {if $primarySitePE==='Y'}selected{/if} value='Y'>YES</option>
                        <option {if $primarySitePE==='N'}selected{/if} value='N'>NO</option>
                    </select>
                </div>
            </div>
            <div class="col-12 my-1">
                <small id="errorNonResidentAddressTypePE" class="form-text text-danger">&nbsp;{$errorNonResidentAddressTypePE}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Non Resident Address Type&nbsp;
                            <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                        </div>
                    </div>
                    <select class="form-control" id="nonResidentAddressTypePE" name="nonResidentAddressTypePE">
                        <option {if $nonResidentAddressTypePE==='01'}selected{/if} value='01'>01 - The residence address and the activity address matches</option>
                        <option {if $nonResidentAddressTypePE==='02'}selected{/if} value='02'>02 - The residence address matches</option>
                        <option {if $nonResidentAddressTypePE==='03'}selected{/if} value='03'>03 - The activity address matches</option>
                        <option {if $nonResidentAddressTypePE==='04'}selected{/if} value='04'>04 - Address of the company or other address</option>
                    </select>
                </div>
            </div>
            <div class="col-12 my-1">
                <small id="errorNonResidentCertNumberPE" class="form-text text-danger">&nbsp;{$errorNonResidentCertNumberPE}</small>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Non Resident&nbsp;Cert Number
                            <span class="flag-icon flag-icon-pe mr-1 ml-1" title="PE"></span>
                        </div>
                    </div>
                    <input class="form-control" id="nonResidentCertNumberPE" name="nonResidentCertNumberPE" value="{$nonResidentCertNumberPE}"/>
                </div>
            </div>
        </div>
    {/if}


    {if ($country=='MX')}
        <div class="form-row align-items-center">
            <div class="col-sm-1 col-md-12 my-1">
                <small id="errorShipToLocationCode" class="form-text text-danger">&nbsp;{$errorShipToLocationCode}</small>
                <label class="sr-only" for="inlineFormInputShipToLocationCode">ShipTo Location</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">ShipTo Location
                            <span class="flag-icon flag-icon-mx ml-2"></div>
                    </div>
                    <select class="form-control" id="shipToLocationCode" name="shipToLocationCode">
                        <option value="">--</option>
                        <option {if $shipToLocationCode==='MX - CIUDAD DE MEXICO'}selected{/if} value='MX - CIUDAD DE MEXICO'>MX - CIUDAD DE MEXICO</option>
                        <option {if $shipToLocationCode==='MX - GUADALAJARA'}selected{/if} value='MX - GUADALAJARA'>MX - GUADALAJARA</option>
                        <option {if $shipToLocationCode==='MX - MONTERREY'}selected{/if} value='MX - MONTERREY'>MX - MONTERREY</option>
                        <option {if $shipToLocationCode==='MX - QUERETARO'}selected{/if} value='MX - QUERETARO'>MX - QUERETARO</option>
                        <option {if $shipToLocationCode==='MX - TIJUANA'}selected{/if} value='MX - TIJUANA'>MX - TIJUANA</option>
                    </select>
                </div>
            </div>
        </div>
    {/if}

    {include file='../../MAPS/ORAGeoCodes/AddressStyleAwareWidget.tpl'}

    {if ($country=='CR')}
        <div class="form-row align-items-center">
            <div class="col-6 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">ShipTo Location
                            <span class="flag-icon flag-icon-cr ml-2"></span>
                        </div>
                    </div>
                    <select class="form-control" id="shipToLocationCode" name="shipToLocationCode">
                        <option {if $shipToLocationCode==='CR - COSTA RICA'}selected{/if} value='CR - COSTA RICA'>CR - COSTA RICA</option>
                    </select>
                </div>
            </div>
            <div class="col-6 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Concept Code &nbsp;<span class="flag-icon flag-icon-cr"></span></div>
                    </div>
                    <select class="form-control" id="conceptCode" name="conceptCode">
                        <option {if $conceptCode==='M'}selected{/if} value="M">M : Expenses by commissions of all type</option>
                        <option {if $conceptCode==='I'}selected{/if} value="I">I : Expenses by interests</option>
                        <option {if $conceptCode==='SP'}selected{/if} value="SP">SP : Expenses by professional services</option>
                        <option {if $conceptCode==='A'}selected{/if} value="A">A : Expenses by rents</option>
                        <option {if $conceptCode==='C'}selected{/if} value="C">C : Purchases (to suppliers)</option>
                    </select>

                </div>
            </div>
        </div>
    {/if}

    {if ($country=='SV')}
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">ShipTo Location
                        <span class="flag-icon flag-icon-sv ml-2"></span>
                    </div>
                </div>
                <select class="form-control" id="shipToLocationCode" name="shipToLocationCode">
                    <option {if $shipToLocationCode==='SV - EL SALVADOR'}selected{/if} value='SV - EL SALVADOR'>SV - EL SALVADOR</option>
                </select>
            </div>
        </div>
        <div class="col-sm-1 col-md-6 my-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Principal Sites
                        <span class="flag-icon flag-icon-sv ml-2"></span>
                    </div>
                </div>
                <select class="form-control" id="principalSitesSV" name="principalSitesSV">
                    <option {if $principalSitesSV==='Y'}selected{/if} value='Y'>Yes</option>
                    <option {if $principalSitesSV==='N'}selected{/if} value='N'>No</option>
                </select>
            </div>
        </div>
    </div>
    {/if}

    {if ($country=='HN')}
        <div class="form-row align-items-center">
            <div class="col-sm-1 col-md-12 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">ShipTo Location
                            <span class="flag-icon flag-icon-hn ml-2"></span>
                        </div>
                    </div>
                    <select class="form-control" id="shipToLocationCode" name="shipToLocationCode">
                        <option value="">--</option>
                        <option {if $shipToLocationCode==='HN - HONDURAS'}selected{/if} value='HN - HONDURAS'>HN - HONDURAS</option>
                    </select>
                </div>
            </div>
        </div>
    {/if}

    {if ($country=='GT')}
        <div class="form-row align-items-center">
            <div class="col-sm-1 col-md-12 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">ShipTo Location
                            <span class="flag-icon flag-icon-gt ml-2"></span>
                        </div>
                    </div>
                    <select class="form-control" id="shipToLocationCode" name="shipToLocationCode">
                        <option {if $shipToLocationCode==='GT - GUATEMALA'}selected{/if} value='GT - GUATEMALA'>GT - GUATEMALA</option>
                    </select>
                </div>
            </div>
        </div>
    {/if}

    {if ($country=='NI')}
        <div class="form-row align-items-center">
            <div class="col-sm-1 col-md-12 my-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">ShipTo Location
                            <span class="flag-icon flag-icon-ni ml-2"></span>
                        </div>
                    </div>
                    <select class="form-control" id="shipToLocationCode" name="shipToLocationCode">
                        <option {if $shipToLocationCode==='NI - NICARAGUA'}selected{/if} value='NI - NICARAGUA'>NI - NICARAGUA</option>
                    </select>
                </div>
            </div>
        </div>
    {/if}

    <div class="form-row align-items-center">
        <div class="col-sm-9 my-1">
            <small id="errorAddressLine1" class="form-text text-danger">&nbsp;{$errorAddressLine1}</small>
            <label class="sr-only" for="inlineFormInputAddressLine1">Address Line1</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Address Line1</div>
                </div>
                <input type="text" class="form-control" id="addressLine1" name="addressLine1" value="{$addressLine1}"
                       placeholder="Address Line1">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <small id="errorZip" class="form-text text-danger">&nbsp;{$errorZip}</small>
            <label class="sr-only" for="inlineFormInputZip">Zip</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Zip</div>
                </div>
                <input type="text" class="form-control" id="zip" name="zip" value="{$zip}" placeholder="Zip">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-6 my-1">
            <small id="errorAddressLine2" class="form-text text-danger">&nbsp;{$errorAddressLine2}</small>
            <label class="sr-only" for="inlineFormInputAddressLine2">Address Line2 (only for CO)</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Address Line2</div>
                </div>
                <input type="text" class="form-control" id="addressLine2" name="addressLine2" value="{$addressLine2}"
                       placeholder="Address Line2">
            </div>
        </div>
        <div class="col-2 my-1">
            <small id="errorPurchasingSiteFlag" class="form-text text-danger">&nbsp;{$errorPurchasingSiteFlag}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Purchase Site Flag</div>
                </div>
                <input style="margin:10px;" type="checkbox" {($purchasingSiteFlag==true)?'checked':''} id="purchasingSiteFlag" name="purchasingSiteFlag">
            </div>
        </div>
        <div class="col-2 my-1">
            <small id="errorPaySiteFlag" class="form-text text-danger">&nbsp;{$errorPaySiteFlag}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Pay Site Flag</div>
                </div>
                <input style="margin:10px;" type="checkbox" {($paySiteFlag==true)?'checked':''} id="paySiteFlag" name="paySiteFlag">
            </div>
        </div>
        <div class="col-2 my-1">
            <small id="errorGlobalAttribute17" class="form-text text-danger">&nbsp;{$errorGlobalAttribute17}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Legal Address (GA17)</div>
                </div>
                <input style="margin:10px;" type="checkbox" {($globalAttribute17==true)?'checked':''} id="globalAttribute17" name="globalAttribute17">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorAddressLineAlt" class="form-text text-danger">&nbsp;{$errorAddressLineAlt}</small>
            <label class="sr-only" for="inlineFormInputAddressLineAlt">Address Line Alt (Phonetic, only CO)</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Address Line Alt (Phonetic, only CO)</div>
                </div>
                <input type="text" class="form-control" id="addressLineAlt" name="addressLineAlt"
                       value="{$addressLineAlt}" placeholder="Address Line Alt">
            </div>
        </div>
    </div>

    <div class="form-row align-items-center">
        <div class="col-sm-2 my-1">
            <small class="form-text text-danger">&nbsp;{$errorPurchasingSiteFlag}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Purchasing Site </div>
                </div>
                <input style="margin:10px;" type="checkbox" {($purchasingSiteFlag==true)?'checked':''} id="purchasingSiteFlag" name="purchasingSiteFlag">
            </div>
        </div>
        <div class="col-sm-2 my-1">
            <small class="form-text text-danger">&nbsp;{$errorPaySiteFlag}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Pay Site </div>
                </div>
                <input style="margin:10px;" type="checkbox" {($paySiteFlag==true)?'checked':''} id="paySiteFlag" name="paySiteFlag">
            </div>
        </div>
    </div>

    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorClassificationCode " class="form-text text-danger">&nbsp;{$errorClassificationTypeCode}</small>
            <label class="sr-only" for="inlineFormInputFiscalClassificationTypeCode">Fiscal Classification Type Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Type Code</div>
                </div>
                <select class="form-control" id="classificationTypeCode" name="classificationTypeCode">
                    <option value="">--</option>
                    <option {if $classificationTypeCode ==='TKE_LATAM_MX_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_MX_CONTRIBUTOR">TKE LATAM MX CONTRIBUTOR</option>
                    <option {if $classificationTypeCode ==='TKE_LATAM_HN_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_HN_CONTRIBUTOR">TKE LATAM HN CONTRIBUTOR</option>
                    <option {if $classificationTypeCode ==='TKE_LATAM_NI_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_NI_CONTRIBUTOR">TKE LATAM NI CONTRIBUTOR</option>
                    <option {if $classificationTypeCode ==='TKE_LATAM_CR_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_CR_CONTRIBUTOR">TKE LATAM CR CONTRIBUTOR</option>
                    <option {if $classificationTypeCode ==='TKE_LATAM_GT_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_GT_CONTRIBUTOR">TKE LATAM GT CONTRIBUTOR</option>
                    <option {if $classificationTypeCode ==='TKE_LATAM_SV_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_SV_CONTRIBUTOR">TKE LATAM SV CONTRIBUTOR</option>
                    <option {if $classificationTypeCode ==='TKE_LATAM_PE_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_PE_CONTRIBUTOR">TKE LATAM PE CONTRIBUTOR</option>
                    <option {if $classificationTypeCode ==='TKE_LATAM_CL_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_CL_CONTRIBUTOR">TKE LATAM CL CONTRIBUTOR</option>
                    <option {if $classificationTypeCode ==='TKE_LATAM_AR_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_AR_CONTRIBUTOR">TKE LATAM AR CONTRIBUTOR</option>
                    <option {if $classificationTypeCode ==='TKE_LATAM_UY_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_UY_CONTRIBUTOR">TKE LATAM UY CONTRIBUTOR</option>
                    <option {if $classificationTypeCode ==='TKE_LATAM_PY_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_PY_CONTRIBUTOR">TKE LATAM PY CONTRIBUTOR</option>
                </select>
            </div>
        </div>
        {if $country=='MX'}
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorClassificationCode " class="form-text text-danger">&nbsp;{$errorClassificationCode}</small>
            <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Code</div>
                </div>
                <select class="form-control" id="classificationCode" name="classificationCode" placeholder="Classification Code ">
                    <option {if $classificationCode ==='AUTORETENEDOR'}selected{/if} value="AUTORETENEDOR">AUTORETENEDOR</option>
                    <option {if $classificationCode ==='CONTRIBUYENTE'}selected{/if} value="CONTRIBUYENTE">CONTRIBUYENTE</option>
                    <option {if $classificationCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO</option>
                    <option {if $classificationCode ==='ZONA FRONTERIZA NORTE'}selected{/if} value="ZONA FRONTERIZA NORTE">ZONA FRONTERIZA NORTE</option>
                </select>
            </div>
        </div>
        {elseif $country=='GT'}
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorClassificationCode " class="form-text text-danger">&nbsp;{$errorClassificationCode}</small>
            <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Code</div>
                </div>
                <select class="form-control" id="classificationCode" name="classificationCode" placeholder="Classification Code ">
                    <option {if $classificationCode ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL  &ndash; 12%</option>
                    <option {if $classificationCode ==='CONTRIBUYENTE PEQUENO'}selected{/if} value="CONTRIBUYENTE PEQUENO">CONTRIBUYENTE PEQUENO  &ndash; 6%</option>
                    <option {if $classificationCode ==='CONTRIBUYENTE SIN FACTURA'}selected{/if} value="CONTRIBUYENTE SIN FACTURA">CONTRIBUYENTE SIN FACTURA  &ndash; 12%</option>
                    <option {if $classificationCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO  &ndash; 0%</option>
                    <option {if $classificationCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR  &ndash; 0%</option>
                </select>
            </div>
        </div>
        {elseif $country=='HN'}
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorClassificationCode " class="form-text text-danger">&nbsp;{$errorClassificationCode}</small>
                <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="classificationCode" name="classificationCode" placeholder="Classification Code ">
                        <option {if $classificationCode ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL  &ndash; 15%</option>
                        <option {if $classificationCode ==='CONTRIBUYENTE SPECIAL'}selected{/if} value="CONTRIBUYENTE SPECIAL">CONTRIBUYENTE SPECIAL  &ndash; 18%</option>
                        <option {if $classificationCode ==='CONTRIBUYENTE HOTELES'}selected{/if} value="CONTRIBUYENTE HOTELES">CONTRIBUYENTE HOTELES  &ndash; 19%</option>
                        <option {if $classificationCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO  &ndash; 0%</option>
                        <option {if $classificationCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR  &ndash; 0%</option>
                    </select>
                </div>
            </div>
        {elseif $country=='SV'}
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorClassificationCode " class="form-text text-danger">&nbsp;{$errorClassificationCode}</small>
                <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="classificationCode" name="classificationCode" placeholder="Classification Code ">
                        <option {if $classificationCode ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL  &ndash; 13%</option>
                        <option {if $classificationCode ==='CONTRIBUYENTE MEDIUM'}selected{/if} value="CONTRIBUYENTE MEDIUM">CONTRIBUYENTE MEDIUM  &ndash; 13%</option>
                        <option {if $classificationCode ==='CONTRIBUYENTE PEQUENO'}selected{/if} value="CONTRIBUYENTE PEQUENO">CONTRIBUYENTE PEQUENO  &ndash; 13%</option>
                        <option {if $classificationCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO &ndash; 0%</option>
                        <option {if $classificationCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR &ndash; 0%</option>
                    </select>
                </div>
            </div>
        {elseif $country=='NI'}
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorClassificationCode " class="form-text text-danger">&nbsp;{$errorClassificationCode}</small>
                <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="classificationCode" name="classificationCode" placeholder="Classification Code ">
                        <option {if $classificationCode ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL  &ndash; 15%</option>
                        <option {if $classificationCode ==='DIPLOMATICOS'}selected{/if} value="DIPLOMATICOS">DIPLOMATICOS  &ndash; 0%</option>
                        <option {if $classificationCode ==='CONSTANCIA DE EXONERACION'}selected{/if} value="CONSTANCIA DE EXONERACION">CONSTANCIA DE EXONERACION &ndash; 0%</option>
                        <option {if $classificationCode ==='ENERGIA ELECTRICA'}selected{/if} value="ENERGIA ELECTRICA">ENERGIA ELECTRICA &ndash; 7%</option>
                        <option {if $classificationCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO  &ndash; 0%</option>
                        <option {if $classificationCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR  &ndash; 0%</option>
                    </select>
                </div>
            </div>
        {elseif $country=='CR'}
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorClassificationCode " class="form-text text-danger">&nbsp;{$errorClassificationCode}</small>
                <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="classificationCode" name="classificationCode" placeholder="Classification Code ">
                        <option {if $classificationCode ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL  &ndash; 13%</option>
                        <option {if $classificationCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO  &ndash; 0%</option>
                        <option {if $classificationCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR  &ndash; 0%</option>
                    </select>
                </div>
            </div>
        {elseif $country=='AR'}
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorClassificationCode " class="form-text text-danger">&nbsp;{$errorClassificationCode}</small>
                <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="classificationCode" name="classificationCode" placeholder="Classification Code ">
                        <option {if $classificationCode ==='RESPONSABLES INSCRIPTOS'}selected{/if} value="RESPONSABLES INSCRIPTOS">RESPONSABLES INSCRIPTOS</option>
                        <option {if $classificationCode ==='RESPONSABLES NO INSCRIPTOS'}selected{/if} value="RESPONSABLES NO INSCRIPTOS">RESPONSABLES NO INSCRIPTOS</option>
                        <option {if $classificationCode ==='EXENTOS'}selected{/if} value="EXENTOS">EXENTOS</option>
                        <option {if $classificationCode ==='CONSUMIDOR FINAL'}selected{/if} value="CONSUMIDOR FINAL">CONSUMIDOR FINAL</option>
                        <option {if $classificationCode ==='MONOTRIBUTO'}selected{/if} value="MONOTRIBUTO">MONOTRIBUTO</option>
                        <option {if $classificationCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR</option>
                    </select>
                </div>
            </div>
        {elseif $country=='PE'}
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorClassificationCode " class="form-text text-danger">&nbsp;{$errorClassificationCode}</small>
                <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="classificationCode" name="classificationCode" placeholder="Classification Code ">
                        <option {if $classificationCode ==='GRABADAS DEL IGV'}selected{/if} value="GRABADAS DEL IGV">GRABADAS DEL IGV</option>
                        <option {if $classificationCode ==='NO GRABADAS DEL IGV'}selected{/if} value="NO GRABADAS DEL IGV">NO GRABADAS DEL IGV</option>
                        <option {if $classificationCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO</option>
                    </select>
                </div>
            </div>
        {elseif $country=='CL'}
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorClassificationCode " class="form-text text-danger">&nbsp;{$errorClassificationCode}</small>
                <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="classificationCode" name="classificationCode" placeholder="Classification Code ">
                        <option {if $classificationCode ==='AFECTAS DEL IVA'}selected{/if} value="AFECTAS DEL IVA">AFECTAS DEL IVA</option>
                        <option {if $classificationCode ==='EXENTAS DEL IVA'}selected{/if} value="EXENTAS DEL IVAV">EXENTAS DEL IVAV</option>
                    </select>
                </div>
            </div>
        {elseif $country=='PY'}
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorClassificationCode " class="form-text text-danger">&nbsp;{$errorClassificationCode}</small>
                <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="classificationCode" name="classificationCode" placeholder="Classification Code ">
                        <option {if $classificationCode ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL</option>
                        <option {if $classificationCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO</option>
                        <option {if $classificationCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR</option>
                        <option {if $classificationCode ==='ZONA FRANCA'}selected{/if} value="ZONA FRANCA">ZONA FRANCA</option>
                    </select>
                </div>
            </div>
        {elseif $country=='UY'}
            <div class="col-sm-1 col-md-6 my-1">
                <small id="errorClassificationCode " class="form-text text-danger">&nbsp;{$errorClassificationCode}</small>
                <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Fiscal Classification Code</div>
                    </div>
                    <select class="form-control" id="classificationCode" name="classificationCode" placeholder="Classification Code ">
                        <option {if $classificationCode ==='CONTRIBUYENTE GENERAL'}selected{/if} value="CONTRIBUYENTE GENERAL">CONTRIBUYENTE GENERAL</option>
                        <option {if $classificationCode ==='EXENTO'}selected{/if} value="EXENTO">EXENTO</option>
                        <option {if $classificationCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR</option>
                    </select>
                </div>
            </div>
        {/if}
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorClassificationStartDate" class="form-text text-danger">&nbsp;{$errorClassificationStartDate}</small>
            <label class="sr-only" for="inlineFormClassificationStartDate">Fiscal Classification Start Date</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Start Date</div>
                </div>
                <input class="form-control" id="classificationStartDate" name="classificationStartDate" aria-describedby="classificationStartDateHelp" width="276" />
                <script>
                    $('#classificationStartDate').datepicker({
                        uiLibrary: 'bootstrap4',
                        format:'dd-mm-yyyy',
                        value: '{$classificationStartDate|date_format:"%d-%m-%Y"}'
                    });
                </script>
            </div>
        </div>
    </div>
    <br/>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>

</form>

<br>
<h4>Vendor Site Contacts <a data-toggle="tooltip" data-placement="top"
                            title="add a new Contact to {$vendorSiteCode}"
                            href="{base_url()}VendorSiteContact/createNew/{$id}" style="cursor:pointer">
        <small><i class="text-primary fas fa-plus-circle fa-sm fa-fw"></i></small>
    </a></h4>
{if isset($contacts) && (sizeof($contacts)>0)}
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th scope="col"></th>
            <th>#</th>
            <th>Action</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Email Address</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$contacts item=c}
            <tr class="{if ($c.dismissed)} dismissed {elseif (($c.locallyValidated)&&($c.crossRefValidated)) } alert-success {else} alert-warning {/if} clickable-row"
                style="cursor:pointer"
                data-href='{base_url()}VendorSiteContact/customEditCleansed/{$c.country}/{$c.id}'>
                <th scope="row">
                    <a href="{base_url()}VendorSiteContact/editCleansed/{$c.id}" data-toggle="tooltip"
                       data-placement="top" title="inspect all fields from contact {$c.lastName}"><i
                                class="text-primary fas fa-edit fa-sm fa-fw"></i></a>
                    <a href="{base_url()}VendorSiteContact/toggle/{$c.id}" data-toggle="tooltip" data-placement="top"
                       title="{if ($c.dismissed)}Recall{else}Dismiss{/if} {$c.lastName}"><i
                                class="text-primary fas fa-minus-circle fa-sm fa-fw"></i></a>
                </th>
                <th scope="row">{$c.id}</th>
                <td>{$c.action}</td>
                <td>{$c.firstName}</td>
                <td>{$c.lastName}</td>
                <td>{$c.phoneNumber}</td>
                <td>{$c.emailAddress}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{/if}
<br/>
<br/>
<br/>
<br/>
<br/>
