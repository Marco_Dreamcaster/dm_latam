<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorVendorNumber" class="form-text text-danger">&nbsp;{$errorVendorNumber}</small>
            <label class="sr-only" for="inlineFormInputVendorNumber">Vendor Number</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Vendor Number</div>
                </div>
                <input type="text" class="form-control" id="vendorNumber" name="vendorNumber" value="{$vendorNumber}" placeholder="Vendor Number" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorVendorName" class="form-text text-danger">&nbsp;{$errorVendorName}</small>
            <label class="sr-only" for="inlineFormInputVendorName">Vendor Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Vendor Name</div>
                </div>
                <input type="text" class="form-control" id="vendorName" name="vendorName" value="{$vendorName}" placeholder="Vendor Name" disabled>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorVendorSiteCode" class="form-text text-danger">&nbsp;{$errorVendorSiteCode}</small>
            <label class="sr-only" for="inlineFormInputVendorSiteCode">Vendor Site Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Vendor Site Code </div>
                </div>
                <input type="text" class="form-control" id="vendorSiteCode" name="vendorSiteCode" value="{$vendorSiteCode}" placeholder="Vendor Site Code" disabled>
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorVendorSiteCodeAlt" class="form-text text-danger">&nbsp;{$errorVendorSiteCodeAlt}</small>
            <label class="sr-only" for="inlineFormInputVendorSiteCodeAlt">Vendor Site Code Alternative</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-warning"><a title="preencher com c�digo que ir� para flat file">Vendor Site Code Alternative</a></div>
                </div>
                <input type="text" class="form-control" id="vendorSiteCodeAlt" name="vendorSiteCodeAlt" value="{$vendorSiteCodeAlt}" placeholder="code on Sha's flat file">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-12 my-1">
            <small id="errorShipToLocationCode" class="form-text text-danger">&nbsp;{$errorShipToLocationCode}</small>
            <label class="sr-only" for="inlineFormInputShipToLocationCode">ShipTo Location</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text alert-warning">ShipTo Location</div>
                </div>
                <select class="form-control" id="shipToLocationCode" name="shipToLocationCode">
                    <option value="">--</option>
                    <option {if $shipToLocationCode==='CO - BARRANQUILLA'}selected{/if} value='CO - BARRANQUILLA'>CO - BARRANQUILLA</option>
                    <option {if $shipToLocationCode==='CO - BOGOTA'}selected{/if} value='CO - BOGOTA'>CO - BOGOTA</option>
                    <option {if $shipToLocationCode==='CO - BUCARAMANGA'}selected{/if} value='CO - BUCARAMANGA'>CO - BUCARAMANGA</option>
                    <option {if $shipToLocationCode==='CO - CALI'}selected{/if} value='CO - CALI'>CO - CALI</option>
                    <option {if $shipToLocationCode==='CO - CARTAGENA'}selected{/if} value='CO - CARTAGENA'>CO - CARTAGENA</option>
                    <option {if $shipToLocationCode==='CO - MEDELLIN'}selected{/if} value='CO - MEDELLIN'>CO - MEDELLIN</option>
                    <option {if $shipToLocationCode==='CO - PEREIRA'}selected{/if} value='CO - PEREIRA'>CO - PEREIRA</option>
                </select>
            </div>
        </div>
    </div>

    {include file='../../MAPS/ORAGeoCodes/AddressStyleAwareWidget.tpl'}

    <div class="form-row align-items-center">
        <div class="col-sm-9 my-1">
            <small id="errorAddressLine1" class="form-text text-danger">&nbsp;{$errorAddressLine1}</small>
            <label class="sr-only" for="inlineFormInputAddressLine1">Address Line1</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Address Line1</div>
                </div>
                <input type="text" class="form-control" id="addressLine1" name="addressLine1" value="{$addressLine1}"
                       placeholder="Address Line1">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <small id="errorZip" class="form-text text-danger">&nbsp;{$errorZip}</small>
            <label class="sr-only" for="inlineFormInputZip">Zip</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Zip</div>
                </div>
                <input type="text" class="form-control" id="zip" name="zip" value="{$zip}" placeholder="Zip">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-6 my-1">
            <small id="errorAddressLine2" class="form-text text-danger">&nbsp;{$errorAddressLine2}</small>
            <label class="sr-only" for="inlineFormInputAddressLine2">Address Line2 (only for CO)</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Address Line2</div>
                </div>
                <input type="text" class="form-control" id="addressLine2" name="addressLine2" value="{$addressLine2}"
                       placeholder="Address Line2">
            </div>
        </div>
        <div class="col-2 my-1">
            <small id="errorPurchasingSiteFlag" class="form-text text-danger">&nbsp;{$errorPurchasingSiteFlag}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Purchase Site Flag</div>
                </div>
                <input style="margin:10px;" type="checkbox" {($purchasingSiteFlag==true)?'checked':''} id="purchasingSiteFlag" name="purchasingSiteFlag">
            </div>
        </div>
        <div class="col-2 my-1">
            <small id="errorPaySiteFlag" class="form-text text-danger">&nbsp;{$errorPaySiteFlag}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Pay Site Flag</div>
                </div>
                <input style="margin:10px;" type="checkbox" {($paySiteFlag==true)?'checked':''} id="paySiteFlag" name="paySiteFlag">
            </div>
        </div>
        <div class="col-2 my-1">
            <small id="errorGlobalAttribute17" class="form-text text-danger">&nbsp;{$errorGlobalAttribute17}</small>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Legal Address (GA17)</div>
                </div>
                <input style="margin:10px;" type="checkbox" {($globalAttribute17==true)?'checked':''} id="globalAttribute17" name="globalAttribute17">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorAddressLineAlt" class="form-text text-danger">&nbsp;{$errorAddressLineAlt}</small>
            <label class="sr-only" for="inlineFormInputAddressLineAlt">Address Line Alt (Phonetic, only CO)</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Address Line Alt (Phonetic, only CO)</div>
                </div>
                <input type="text" class="form-control" id="addressLineAlt" name="addressLineAlt"
                       value="{$addressLineAlt}" placeholder="Address Line Alt">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorCoExtAttribute1" class="form-text text-danger">&nbsp;{$errorCoExtAttribute1}</small>
            <label class="sr-only" for="inlineFormInputCoExtAttribute1">Economic Activity (CO_EXT1)</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Economic Activity (CO_EXT1)</div>
                </div>
                <select class="form-control" id="coExtAttribute1" name="coExtAttribute1">
                    <option {if $coExtAttribute1==='0000'}selected{/if} value="0000">0000 - pendiente de informaci�n</option>
                    <option {if $coExtAttribute1==='0111'}selected{/if} value="0111">0111 - Cultivo de cereales (excepto arroz), legumbres y semillas oleaginosas</option>
                    <option {if $coExtAttribute1==='0112'}selected{/if} value="0112">0112 - Cultivo de arroz</option>
                    <option {if $coExtAttribute1==='0113'}selected{/if} value="0113">0113 - Cultivo de hortalizas, ra?ces y tub?rculos</option>
                    <option {if $coExtAttribute1==='0114'}selected{/if} value="0114">0114 - Cultivo de tabaco</option>
                    <option {if $coExtAttribute1==='0115'}selected{/if} value="0115">0115 - Cultivo de plantas textiles</option>
                    <option {if $coExtAttribute1==='0119'}selected{/if} value="0119">0119 - Otros cultivos transitorios n.c.p.</option>
                    <option {if $coExtAttribute1==='0121'}selected{/if} value="0121">0121 - Cultivo de frutas tropicales y subtropicales</option>
                    <option {if $coExtAttribute1==='0122'}selected{/if} value="0122">0122 - Cultivo de pl?tano y banano</option>
                    <option {if $coExtAttribute1==='0123'}selected{/if} value="0123">0123 - Cultivo de caf?</option>
                    <option {if $coExtAttribute1==='0124'}selected{/if} value="0124">0124 - Cultivo de ca?a de az?car</option>
                    <option {if $coExtAttribute1==='0125'}selected{/if} value="0125">0125 - Cultivo de flor de corte</option>
                    <option {if $coExtAttribute1==='0126'}selected{/if} value="0126">0126 - Cultivo de palma para aceite (palma africana) y otros frutos oleaginosos</option>
                    <option {if $coExtAttribute1==='0127'}selected{/if} value="0127">0127 - Cultivo de plantas con las que se preparan bebidas</option>
                    <option {if $coExtAttribute1==='0128'}selected{/if} value="0128">0128 - Cultivo de especias y de plantas arom?ticas y medicinales</option>
                    <option {if $coExtAttribute1==='0129'}selected{/if} value="0129">0129 - Otros cultivos permanentes n.c.p.</option>
                    <option {if $coExtAttribute1==='0130'}selected{/if} value="0130">0130 - Propagaci?n de plantas (actividades de los viveros, excepto viveros forestales)</option>
                    <option {if $coExtAttribute1==='0141'}selected{/if} value="0141">0141 - Cr?a de ganado bovino y bufalino</option>
                    <option {if $coExtAttribute1==='0142'}selected{/if} value="0142">0142 - Cr?a de caballos y otros equinos</option>
                    <option {if $coExtAttribute1==='0143'}selected{/if} value="0143">0143 - Cr?a de ovejas y cabras</option>
                    <option {if $coExtAttribute1==='0144'}selected{/if} value="0144">0144 - Cr?a de ganado porcino</option>
                    <option {if $coExtAttribute1==='0145'}selected{/if} value="0145">0145 - Cr?a de aves de corral</option>
                    <option {if $coExtAttribute1==='0149'}selected{/if} value="0149">0149 - Cr?a de otros animales n.c.p.</option>
                    <option {if $coExtAttribute1==='0150'}selected{/if} value="0150">0150 - Explotaci?n mixta (agr?cola y pecuaria)</option>
                    <option {if $coExtAttribute1==='0161'}selected{/if} value="0161">0161 - Actividades de apoyo a la agricultura</option>
                    <option {if $coExtAttribute1==='0162'}selected{/if} value="0162">0162 - Actividades de apoyo a la ganader?a</option>
                    <option {if $coExtAttribute1==='0163'}selected{/if} value="0163">0163 - Actividades posteriores a la cosecha</option>
                    <option {if $coExtAttribute1==='0164'}selected{/if} value="0164">0164 - Tratamiento de semillas para propagaci?n</option>
                    <option {if $coExtAttribute1==='0170'}selected{/if} value="0170">0170 - Caza ordinaria y mediante trampas y actividades de servicios conexas</option>
                    <option {if $coExtAttribute1==='0210'}selected{/if} value="0210">0210 - Silvicultura y otras actividades forestales</option>
                    <option {if $coExtAttribute1==='0220'}selected{/if} value="0220">0220 - Extracci?n de madera</option>
                    <option {if $coExtAttribute1==='0230'}selected{/if} value="0230">0230 - Recolecci?n de productos forestales diferentes a la madera</option>
                    <option {if $coExtAttribute1==='0240'}selected{/if} value="0240">0240 - Servicios de apoyo a la silvicultura</option>
                    <option {if $coExtAttribute1==='0311'}selected{/if} value="0311">0311 - Pesca mar?tima</option>
                    <option {if $coExtAttribute1==='0312'}selected{/if} value="0312">0312 - Pesca de agua dulce</option>
                    <option {if $coExtAttribute1==='0321'}selected{/if} value="0321">0321 - Acuicultura mar?tima</option>
                    <option {if $coExtAttribute1==='0322'}selected{/if} value="0322">0322 - Acuicultura de agua dulce</option>
                    <option {if $coExtAttribute1==='0510'}selected{/if} value="0510">0510 - Extracci?n de hulla (carb?n de piedra)</option>
                    <option {if $coExtAttribute1==='0520'}selected{/if} value="0520">0520 - Extracci?n de carb?n lignito</option>
                    <option {if $coExtAttribute1==='0610'}selected{/if} value="0610">0610 - Extracci?n de petr?leo crudo</option>
                    <option {if $coExtAttribute1==='0620'}selected{/if} value="0620">0620 - Extracci?n de gas natural</option>
                    <option {if $coExtAttribute1==='0710'}selected{/if} value="0710">0710 - Extracci?n de minerales de hierro</option>
                    <option {if $coExtAttribute1==='0721'}selected{/if} value="0721">0721 - Extracci?n de minerales de uranio y de torio</option>
                    <option {if $coExtAttribute1==='0722'}selected{/if} value="0722">0722 - Extracci?n de oro y otros metales preciosos</option>
                    <option {if $coExtAttribute1==='0723'}selected{/if} value="0723">0723 - Extracci?n de minerales de n?quel</option>
                    <option {if $coExtAttribute1==='0729'}selected{/if} value="0729">0729 - Extracci?n de otros minerales metal?feros no ferrosos n.c.p.</option>
                    <option {if $coExtAttribute1==='0811'}selected{/if} value="0811">0811 - Extracci?n de piedra, arena, arcillas comunes, yeso y anhidrita</option>
                    <option {if $coExtAttribute1==='0812'}selected{/if} value="0812">0812 - Extracci?n de arcillas de uso industrial, caliza, caol?n y bentonitas</option>
                    <option {if $coExtAttribute1==='0820'}selected{/if} value="0820">0820 - Extracci?n de esmeraldas, piedras preciosas y semipreciosas</option>
                    <option {if $coExtAttribute1==='0891'}selected{/if} value="0891">0891 - Extracci?n de minerales para la fabricaci?n de abonos y productos qu?micos</option>
                    <option {if $coExtAttribute1==='0892'}selected{/if} value="0892">0892 - Extracci?n de halita (sal)</option>
                    <option {if $coExtAttribute1==='0899'}selected{/if} value="0899">0899 - Extracci?n de otros minerales no met?licos n.c.p.</option>
                    <option {if $coExtAttribute1==='0910'}selected{/if} value="0910">0910 - Actividades de apoyo para la extracci?n de petr?leo y de gas natural</option>
                    <option {if $coExtAttribute1==='0990'}selected{/if} value="0990">0990 - Actividades de apoyo para otras actividades de explotaci?n de minas y canteras</option>
                    <option {if $coExtAttribute1==='1011'}selected{/if} value="1011">1011 - Procesamiento y conservaci?n de carne y productos c?rnicos</option>
                    <option {if $coExtAttribute1==='1012'}selected{/if} value="1012">1012 - Procesamiento y conservaci?n de pescados, crust?ceos y moluscos</option>
                    <option {if $coExtAttribute1==='1020'}selected{/if} value="1020">1020 - Procesamiento y conservaci?n de frutas, legumbres, hortalizas y tub?rculos</option>
                    <option {if $coExtAttribute1==='1030'}selected{/if} value="1030">1030 - Elaboraci?n de aceites y grasas de origen vegetal y animal</option>
                    <option {if $coExtAttribute1==='1040'}selected{/if} value="1040">1040 - Elaboraci?n de productos l?cteos</option>
                    <option {if $coExtAttribute1==='1051'}selected{/if} value="1051">1051 - Elaboraci?n de productos de moliner?a</option>
                    <option {if $coExtAttribute1==='1052'}selected{/if} value="1052">1052 - Elaboraci?n de almidones y productos derivados del almid?n</option>
                    <option {if $coExtAttribute1==='1061'}selected{/if} value="1061">1061 - Trilla de caf?</option>
                    <option {if $coExtAttribute1==='1062'}selected{/if} value="1062">1062 - Descafeinado, tosti?n y molienda del caf?</option>
                    <option {if $coExtAttribute1==='1063'}selected{/if} value="1063">1063 - Otros derivados del caf?</option>
                    <option {if $coExtAttribute1==='1071'}selected{/if} value="1071">1071 - Elaboraci?n y refinaci?n de az?car</option>
                    <option {if $coExtAttribute1==='1072'}selected{/if} value="1072">1072 - Elaboraci?n de panela</option>
                    <option {if $coExtAttribute1==='1081'}selected{/if} value="1081">1081 - Elaboraci?n de productos de panader?a</option>
                    <option {if $coExtAttribute1==='1082'}selected{/if} value="1082">1082 - Elaboraci?n de cacao, chocolate y productos de confiter?a</option>
                    <option {if $coExtAttribute1==='1083'}selected{/if} value="1083">1083 - Elaboraci?n de macarrones, fideos, alcuzcuz y productos farin?ceos similares</option>
                    <option {if $coExtAttribute1==='1084'}selected{/if} value="1084">1084 - Elaboraci?n de comidas y platos preparados</option>
                    <option {if $coExtAttribute1==='1089'}selected{/if} value="1089">1089 - Elaboraci?n de otros productos alimenticios n.c.p.</option>
                    <option {if $coExtAttribute1==='1090'}selected{/if} value="1090">1090 - Elaboraci?n de alimentos preparados para animales</option>
                    <option {if $coExtAttribute1==='11001-0140'}selected{/if} value="11001-0140">11001-0140 - Activity of Services Agricolas and Ganaders - Veterinary</option>
                    <option {if $coExtAttribute1==='1101'}selected{/if} value="1101">1101 - Destilaci?n, rectificaci?n y mezcla de bebidas alcoh?licas</option>
                    <option {if $coExtAttribute1==='1102'}selected{/if} value="1102">1102 - Elaboraci?n de bebidas fermentadas no destiladas</option>
                    <option {if $coExtAttribute1==='1103'}selected{/if} value="1103">1103 - Producci?n de malta, elaboraci?n de cervezas y otras bebidas malteadas</option>
                    <option {if $coExtAttribute1==='1104'}selected{/if} value="1104">1104 - Elaboraci?n de bebidas no alcoh?licas, producci?n de aguas minerales y de otras aguas embotelladas</option>
                    <option {if $coExtAttribute1==='1200'}selected{/if} value="1200">1200 - Elaboraci?n de productos de tabaco</option>
                    <option {if $coExtAttribute1==='1311'}selected{/if} value="1311">1311 - Preparaci?n e hilatura de fibras textiles</option>
                    <option {if $coExtAttribute1==='1312'}selected{/if} value="1312">1312 - Tejedur?a de productos textiles</option>
                    <option {if $coExtAttribute1==='1313'}selected{/if} value="1313">1313 - Acabado de productos textiles</option>
                    <option {if $coExtAttribute1==='1391'}selected{/if} value="1391">1391 - Fabricaci?n de tejidos de punto y ganchillo</option>
                    <option {if $coExtAttribute1==='1392'}selected{/if} value="1392">1392 - Confecci?n de art?culos con materiales textiles, excepto prendas de vestir</option>
                    <option {if $coExtAttribute1==='1393'}selected{/if} value="1393">1393 - Fabricaci?n de tapetes y alfombras para pisos</option>
                    <option {if $coExtAttribute1==='1394'}selected{/if} value="1394">1394 - Fabricaci?n de cuerdas, cordeles, cables, bramantes y redes</option>
                    <option {if $coExtAttribute1==='1399'}selected{/if} value="1399">1399 - Fabricaci?n de otros art?culos textiles n.c.p.</option>
                    <option {if $coExtAttribute1==='1410'}selected{/if} value="1410">1410 - Confecci?n de prendas de vestir, excepto prendas de piel</option>
                    <option {if $coExtAttribute1==='1420'}selected{/if} value="1420">1420 - Fabricaci?n de art?culos de piel</option>
                    <option {if $coExtAttribute1==='1430'}selected{/if} value="1430">1430 - Fabricaci?n de art?culos de punto y ganchillo</option>
                    <option {if $coExtAttribute1==='1511'}selected{/if} value="1511">1511 - Curtido y recurtido de cueros; recurtido y te?ido de pieles</option>
                    <option {if $coExtAttribute1==='1512'}selected{/if} value="1512">1512 - Fabricaci?n de art?culos de viaje, bolsos de mano y art?culos similares elaborados en cuero, y fabricaci?n de art?culos de talabarter?a y guarnicioner?a</option>
                    <option {if $coExtAttribute1==='1513'}selected{/if} value="1513">1513 - Fabricaci?n de art?culos de viaje, bolsos de mano y art?culos similares; art?culos de talabarter?a y guarnicioner?a elaborados en otros materiales</option>
                    <option {if $coExtAttribute1==='1521'}selected{/if} value="1521">1521 - Fabricaci?n de calzado de cuero y piel, con cualquier tipo de suela</option>
                    <option {if $coExtAttribute1==='1522'}selected{/if} value="1522">1522 - Fabricaci?n de otros tipos de calzado, excepto calzado de cuero y piel</option>
                    <option {if $coExtAttribute1==='1523'}selected{/if} value="1523">1523 - Fabricaci?n de partes del calzado</option>
                    <option {if $coExtAttribute1==='1610'}selected{/if} value="1610">1610 - Aserrado, acepillado e impregnaci?n de la madera</option>
                    <option {if $coExtAttribute1==='1620'}selected{/if} value="1620">1620 - Fabricaci?n de hojas de madera para enchapado; fabricaci?n de tableros contrachapados, tableros laminados, tableros de part?culas y otros tableros y paneles</option>
                    <option {if $coExtAttribute1==='1630'}selected{/if} value="1630">1630 - Fabricaci?n de partes y piezas de madera, de carpinter?a y ebanister?a para la construcci?n</option>
                    <option {if $coExtAttribute1==='1640'}selected{/if} value="1640">1640 - Fabricaci?n de recipientes de madera</option>
                    <option {if $coExtAttribute1==='1690'}selected{/if} value="1690">1690 - Fabricaci?n de otros productos de madera; fabricaci?n de art?culos de corcho, cester?a y esparter?a</option>
                    <option {if $coExtAttribute1==='1701'}selected{/if} value="1701">1701 - Fabricaci?n de pulpas (pastas) celul?sicas; papel y cart?n</option>
                    <option {if $coExtAttribute1==='1702'}selected{/if} value="1702">1702 - Fabricaci?n de papel y cart?n ondulado (corrugado); fabricaci?n de envases, empaques y de embalajes de papel y cart?n</option>
                    <option {if $coExtAttribute1==='1709'}selected{/if} value="1709">1709 - Fabricaci?n de otros art?culos de papel y cart?n</option>
                    <option {if $coExtAttribute1==='1811'}selected{/if} value="1811">1811 - Actividades de impresi?n</option>
                    <option {if $coExtAttribute1==='1812'}selected{/if} value="1812">1812 - Actividades de servicios relacionados con la impresi?n</option>
                    <option {if $coExtAttribute1==='1820'}selected{/if} value="1820">1820 - Producci?n de copias a partir de grabaciones originales</option>
                    <option {if $coExtAttribute1==='1910'}selected{/if} value="1910">1910 - Fabricaci?n de productos de hornos de coque</option>
                    <option {if $coExtAttribute1==='1921'}selected{/if} value="1921">1921 - Fabricaci?n de productos de la refinaci?n del petr?leo</option>
                    <option {if $coExtAttribute1==='1922'}selected{/if} value="1922">1922 - Actividad de mezcla de combustibles</option>
                    <option {if $coExtAttribute1==='2011'}selected{/if} value="2011">2011 - Fabricaci?n de sustancias y productos qu?micos b?sicos</option>
                    <option {if $coExtAttribute1==='2012'}selected{/if} value="2012">2012 - Fabricaci?n de abonos y compuestos inorg?nicos nitrogenados</option>
                    <option {if $coExtAttribute1==='2013'}selected{/if} value="2013">2013 - Fabricaci?n de pl?sticos en formas primarias</option>
                    <option {if $coExtAttribute1==='2014'}selected{/if} value="2014">2014 - Fabricaci?n de caucho sint?tico en formas primarias</option>
                    <option {if $coExtAttribute1==='2021'}selected{/if} value="2021">2021 - Fabricaci?n de plaguicidas y otros productos qu?micos de uso agropecuario</option>
                    <option {if $coExtAttribute1==='2022'}selected{/if} value="2022">2022 - Fabricaci?n de pinturas, barnices y revestimientos similares, tintas para impresi?n y masillas</option>
                    <option {if $coExtAttribute1==='2023'}selected{/if} value="2023">2023 - Fabricaci?n de jabones y detergentes, preparados para limpiar y pulir; perfumes y preparados de tocador</option>
                    <option {if $coExtAttribute1==='2029'}selected{/if} value="2029">2029 - Fabricaci?n de otros productos qu?micos n.c.p.</option>
                    <option {if $coExtAttribute1==='2030'}selected{/if} value="2030">2030 - Fabricaci?n de fibras sint?ticas y artificiales</option>
                    <option {if $coExtAttribute1==='2100'}selected{/if} value="2100">2100 - Fabricaci?n de productos farmac?uticos, sustancias qu?micas medicinales y productos bot?nicos de uso farmac?utico</option>
                    <option {if $coExtAttribute1==='2211'}selected{/if} value="2211">2211 - Fabricaci?n de llantas y neum?ticos de caucho</option>
                    <option {if $coExtAttribute1==='2212'}selected{/if} value="2212">2212 - Reencauche de llantas usadas</option>
                    <option {if $coExtAttribute1==='2219'}selected{/if} value="2219">2219 - Fabricaci?n de formas b?sicas de caucho y otros productos de caucho n.c.p.</option>
                    <option {if $coExtAttribute1==='2221'}selected{/if} value="2221">2221 - Fabricaci?n de formas b?sicas de pl?stico</option>
                    <option {if $coExtAttribute1==='2229'}selected{/if} value="2229">2229 - Fabricaci?n de art?culos de pl?stico n.c.p.</option>
                    <option {if $coExtAttribute1==='2310'}selected{/if} value="2310">2310 - Fabricaci?n de vidrio y productos de vidrio</option>
                    <option {if $coExtAttribute1==='2391'}selected{/if} value="2391">2391 - Fabricaci?n de productos refractarios</option>
                    <option {if $coExtAttribute1==='2392'}selected{/if} value="2392">2392 - Fabricaci?n de materiales de arcilla para la construcci?n</option>
                    <option {if $coExtAttribute1==='2393'}selected{/if} value="2393">2393 - Fabricaci?n de otros productos de cer?mica y porcelana</option>
                    <option {if $coExtAttribute1==='2394'}selected{/if} value="2394">2394 - Fabricaci?n de cemento, cal y yeso</option>
                    <option {if $coExtAttribute1==='2395'}selected{/if} value="2395">2395 - Fabricaci?n de art?culos de hormig?n, cemento y yeso</option>
                    <option {if $coExtAttribute1==='2396'}selected{/if} value="2396">2396 - Corte, tallado y acabado de la piedra</option>
                    <option {if $coExtAttribute1==='2399'}selected{/if} value="2399">2399 - Fabricaci?n de otros productos minerales no met?licos n.c.p.</option>
                    <option {if $coExtAttribute1==='2410'}selected{/if} value="2410">2410 - Industrias b?sicas de hierro y de acero</option>
                    <option {if $coExtAttribute1==='2421'}selected{/if} value="2421">2421 - Industrias b?sicas de metales preciosos</option>
                    <option {if $coExtAttribute1==='2429'}selected{/if} value="2429">2429 - Industrias b?sicas de otros metales no ferrosos</option>
                    <option {if $coExtAttribute1==='2431'}selected{/if} value="2431">2431 - Fundici?n de hierro y de acero</option>
                    <option {if $coExtAttribute1==='2432'}selected{/if} value="2432">2432 - Fundici?n de metales no ferrosos</option>
                    <option {if $coExtAttribute1==='2511'}selected{/if} value="2511">2511 - Fabricaci?n de productos met?licos para uso estructural</option>
                    <option {if $coExtAttribute1==='2512'}selected{/if} value="2512">2512 - Fabricaci?n de tanques, dep?sitos y recipientes de metal, excepto los utilizados para el envase o transporte de mercanc?as</option>
                    <option {if $coExtAttribute1==='2513'}selected{/if} value="2513">2513 - Fabricaci?n de generadores de vapor, excepto calderas de agua caliente para calefacci?n central</option>
                    <option {if $coExtAttribute1==='2520'}selected{/if} value="2520">2520 - Fabricaci?n de armas y municiones</option>
                    <option {if $coExtAttribute1==='2591'}selected{/if} value="2591">2591 - Forja, prensado, estampado y laminado de metal; pulvimetalurgia</option>
                    <option {if $coExtAttribute1==='2592'}selected{/if} value="2592">2592 - Tratamiento y revestimiento de metales; mecanizado</option>
                    <option {if $coExtAttribute1==='2593'}selected{/if} value="2593">2593 - Fabricaci?n de art?culos de cuchiller?a, herramientas de mano y art?culos de ferreter?a</option>
                    <option {if $coExtAttribute1==='2599'}selected{/if} value="2599">2599 - Fabricaci?n de otros productos elaborados de metal n.c.p.</option>
                    <option {if $coExtAttribute1==='2610'}selected{/if} value="2610">2610 - Fabricaci?n de componentes y tableros electr?nicos</option>
                    <option {if $coExtAttribute1==='2620'}selected{/if} value="2620">2620 - Fabricaci?n de computadoras y de equipo perif?rico</option>
                    <option {if $coExtAttribute1==='2630'}selected{/if} value="2630">2630 - Fabricaci?n de equipos de comunicaci?n</option>
                    <option {if $coExtAttribute1==='2640'}selected{/if} value="2640">2640 - Fabricaci?n de aparatos electr?nicos de consumo</option>
                    <option {if $coExtAttribute1==='2651'}selected{/if} value="2651">2651 - Fabricaci?n de equipo de medici?n, prueba, navegaci?n y control</option>
                    <option {if $coExtAttribute1==='2652'}selected{/if} value="2652">2652 - Fabricaci?n de relojes</option>
                    <option {if $coExtAttribute1==='2660'}selected{/if} value="2660">2660 - Fabricaci?n de equipo de irradiaci?n y equipo electr?nico de uso m?dico y terap?utico</option>
                    <option {if $coExtAttribute1==='2670'}selected{/if} value="2670">2670 - Fabricaci?n de instrumentos ?pticos y equipo fotogr?fico</option>
                    <option {if $coExtAttribute1==='2680'}selected{/if} value="2680">2680 - Fabricaci?n de medios magn?ticos y ?pticos para almacenamiento de datos</option>
                    <option {if $coExtAttribute1==='2711'}selected{/if} value="2711">2711 - Fabricaci?n de motores, generadores y transformadores el?ctricos</option>
                    <option {if $coExtAttribute1==='2712'}selected{/if} value="2712">2712 - Fabricaci?n de aparatos de distribuci?n y control de la energ?a el?ctrica</option>
                    <option {if $coExtAttribute1==='2720'}selected{/if} value="2720">2720 - Fabricaci?n de pilas, bater?as y acumuladores el?ctricos</option>
                    <option {if $coExtAttribute1==='2731'}selected{/if} value="2731">2731 - Fabricaci?n de hilos y cables el?ctricos y de fibra ?ptica</option>
                    <option {if $coExtAttribute1==='2732'}selected{/if} value="2732">2732 - Fabricaci?n de dispositivos de cableado</option>
                    <option {if $coExtAttribute1==='2740'}selected{/if} value="2740">2740 - Fabricaci?n de equipos el?ctricos de iluminaci?n</option>
                    <option {if $coExtAttribute1==='2750'}selected{/if} value="2750">2750 - Fabricaci?n de aparatos de uso dom?stico</option>
                    <option {if $coExtAttribute1==='2790'}selected{/if} value="2790">2790 - Fabricaci?n de otros tipos de equipo el?ctrico n.c.p.</option>
                    <option {if $coExtAttribute1==='2811'}selected{/if} value="2811">2811 - Fabricaci?n de motores, turbinas, y partes para motores de combusti?n interna</option>
                    <option {if $coExtAttribute1==='2812'}selected{/if} value="2812">2812 - Fabricaci?n de equipos de potencia hidr?ulica y neum?tica</option>
                    <option {if $coExtAttribute1==='2813'}selected{/if} value="2813">2813 - Fabricaci?n de otras bombas, compresores, grifos y v?lvulas</option>
                    <option {if $coExtAttribute1==='2814'}selected{/if} value="2814">2814 - Fabricaci?n de cojinetes, engranajes, trenes de engranajes y piezas de transmisi?n</option>
                    <option {if $coExtAttribute1==='2815'}selected{/if} value="2815">2815 - Fabricaci?n de hornos, hogares y quemadores industriales</option>
                    <option {if $coExtAttribute1==='2816'}selected{/if} value="2816">2816 - Fabricaci?n de equipo de elevaci?n y manipulaci?n</option>
                    <option {if $coExtAttribute1==='2817'}selected{/if} value="2817">2817 - Fabricaci?n de maquinaria y equipo de oficina (excepto computadoras y equipo perif?rico)</option>
                    <option {if $coExtAttribute1==='2818'}selected{/if} value="2818">2818 - Fabricaci?n de herramientas manuales con motor</option>
                    <option {if $coExtAttribute1==='2819'}selected{/if} value="2819">2819 - Fabricaci?n de otros tipos de maquinaria y equipo de uso general n.c.p.</option>
                    <option {if $coExtAttribute1==='2821'}selected{/if} value="2821">2821 - Fabricaci?n de maquinaria agropecuaria y forestal</option>
                    <option {if $coExtAttribute1==='2822'}selected{/if} value="2822">2822 - Fabricaci?n de m?quinas formadoras de metal y de m?quinas herramienta</option>
                    <option {if $coExtAttribute1==='2823'}selected{/if} value="2823">2823 - Fabricaci?n de maquinaria para la metalurgia</option>
                    <option {if $coExtAttribute1==='2824'}selected{/if} value="2824">2824 - Fabricaci?n de maquinaria para explotaci?n de minas y canteras y para obras de construcci?n</option>
                    <option {if $coExtAttribute1==='2825'}selected{/if} value="2825">2825 - Fabricaci?n de maquinaria para la elaboraci?n de alimentos, bebidas y tabaco</option>
                    <option {if $coExtAttribute1==='2826'}selected{/if} value="2826">2826 - Fabricaci?n de maquinaria para la elaboraci?n de productos textiles, prendas de vestir y cueros</option>
                    <option {if $coExtAttribute1==='2829'}selected{/if} value="2829">2829 - Fabricaci?n de otros tipos de maquinaria y equipo de uso especial n.c.p.</option>
                    <option {if $coExtAttribute1==='2910'}selected{/if} value="2910">2910 - Fabricaci?n de veh?culos automotores y sus motores</option>
                    <option {if $coExtAttribute1==='2920'}selected{/if} value="2920">2920 - Fabricaci?n de carrocer?as para veh?culos automotores; fabricaci?n de remolques y semirremolques</option>
                    <option {if $coExtAttribute1==='2930'}selected{/if} value="2930">2930 - Fabricaci?n de partes, piezas (autopartes) y accesorios (lujos) para veh?culos automotores</option>
                    <option {if $coExtAttribute1==='3011'}selected{/if} value="3011">3011 - Construcci?n de barcos y de estructuras flotantes</option>
                    <option {if $coExtAttribute1==='3012'}selected{/if} value="3012">3012 - Construcci?n de embarcaciones de recreo y deporte</option>
                    <option {if $coExtAttribute1==='3020'}selected{/if} value="3020">3020 - Fabricaci?n de locomotoras y de material rodante para ferrocarriles</option>
                    <option {if $coExtAttribute1==='3030'}selected{/if} value="3030">3030 - Fabricaci?n de aeronaves, naves espaciales y de maquinaria conexa</option>
                    <option {if $coExtAttribute1==='3040'}selected{/if} value="3040">3040 - Fabricaci?n de veh?culos militares de combate</option>
                    <option {if $coExtAttribute1==='3091'}selected{/if} value="3091">3091 - Fabricaci?n de motocicletas</option>
                    <option {if $coExtAttribute1==='3092'}selected{/if} value="3092">3092 - Fabricaci?n de bicicletas y de sillas de ruedas para personas con discapacidad</option>
                    <option {if $coExtAttribute1==='3099'}selected{/if} value="3099">3099 - Fabricaci?n de otros tipos de equipo de transporte n.c.p.</option>
                    <option {if $coExtAttribute1==='3110'}selected{/if} value="3110">3110 - Fabricaci?n de muebles</option>
                    <option {if $coExtAttribute1==='3120'}selected{/if} value="3120">3120 - Fabricaci?n de colchones y somieres</option>
                    <option {if $coExtAttribute1==='3210'}selected{/if} value="3210">3210 - Fabricaci?n de joyas, bisuter?a y art?culos conexos</option>
                    <option {if $coExtAttribute1==='3220'}selected{/if} value="3220">3220 - Fabricaci?n de instrumentos musicales</option>
                    <option {if $coExtAttribute1==='3230'}selected{/if} value="3230">3230 - Fabricaci?n de art?culos y equipo para la pr?ctica del deporte</option>
                    <option {if $coExtAttribute1==='3240'}selected{/if} value="3240">3240 - Fabricaci?n de juegos, juguetes y rompecabezas</option>
                    <option {if $coExtAttribute1==='3250'}selected{/if} value="3250">3250 - Fabricaci?n de instrumentos, aparatos y materiales m?dicos y odontol?gicos (incluido mobiliario)</option>
                    <option {if $coExtAttribute1==='3290'}selected{/if} value="3290">3290 - Otras industrias manufactureras n.c.p.</option>
                    <option {if $coExtAttribute1==='3311'}selected{/if} value="3311">3311 - Mantenimiento y reparaci?n especializado de productos elaborados en metal</option>
                    <option {if $coExtAttribute1==='3312'}selected{/if} value="3312">3312 - Mantenimiento y reparaci?n especializado de maquinaria y equipo</option>
                    <option {if $coExtAttribute1==='3313'}selected{/if} value="3313">3313 - Mantenimiento y reparaci?n especializado de equipo electr?nico y ?ptico</option>
                    <option {if $coExtAttribute1==='3314'}selected{/if} value="3314">3314 - Mantenimiento y reparaci?n especializado de equipo el?ctrico</option>
                    <option {if $coExtAttribute1==='3315'}selected{/if} value="3315">3315 - Mantenimiento y reparaci?n especializado de equipo de transporte, excepto los veh?culos automotores, motocicletas y bicicletas</option>
                    <option {if $coExtAttribute1==='3319'}selected{/if} value="3319">3319 - Mantenimiento y reparaci?n de otros tipos de equipos y sus componentes n.c.p.</option>
                    <option {if $coExtAttribute1==='3320'}selected{/if} value="3320">3320 - Instalaci?n especializada de maquinaria y equipo industrial</option>
                    <option {if $coExtAttribute1==='3511'}selected{/if} value="3511">3511 - Generaci?n de energ?a el?ctrica</option>
                    <option {if $coExtAttribute1==='3512'}selected{/if} value="3512">3512 - Transmisi?n de energ?a el?ctrica</option>
                    <option {if $coExtAttribute1==='3513'}selected{/if} value="3513">3513 - Distribuci?n de energ?a el?ctrica</option>
                    <option {if $coExtAttribute1==='3514'}selected{/if} value="3514">3514 - Comercializaci?n de energ?a el?ctrica</option>
                    <option {if $coExtAttribute1==='3520'}selected{/if} value="3520">3520 - Producci?n de gas; distribuci?n de combustibles gaseosos por tuber?as</option>
                    <option {if $coExtAttribute1==='3530'}selected{/if} value="3530">3530 - Suministro de vapor y aire acondicionado</option>
                    <option {if $coExtAttribute1==='3600'}selected{/if} value="3600">3600 - Captaci?n, tratamiento y distribuci?n de agua</option>
                    <option {if $coExtAttribute1==='3700'}selected{/if} value="3700">3700 - Evacuaci?n y tratamiento de aguas residuales</option>
                    <option {if $coExtAttribute1==='3811'}selected{/if} value="3811">3811 - Recolecci?n de desechos no peligrosos</option>
                    <option {if $coExtAttribute1==='3812'}selected{/if} value="3812">3812 - Recolecci?n de desechos peligrosos</option>
                    <option {if $coExtAttribute1==='3821'}selected{/if} value="3821">3821 - Tratamiento y disposici?n de desechos no peligrosos</option>
                    <option {if $coExtAttribute1==='3822'}selected{/if} value="3822">3822 - Tratamiento y disposici?n de desechos peligrosos</option>
                    <option {if $coExtAttribute1==='3830'}selected{/if} value="3830">3830 - Recuperaci?n de materiales</option>
                    <option {if $coExtAttribute1==='3900'}selected{/if} value="3900">3900 - Actividades de saneamiento ambiental y otros servicios de gesti?n de desechos</option>
                    <option {if $coExtAttribute1==='4111'}selected{/if} value="4111">4111 - Construcci?n de edificios residenciales</option>
                    <option {if $coExtAttribute1==='4112'}selected{/if} value="4112">4112 - Construcci?n de edificios no residenciales</option>
                    <option {if $coExtAttribute1==='4210'}selected{/if} value="4210">4210 - Construcci?n de carreteras y v?as de ferrocarril</option>
                    <option {if $coExtAttribute1==='4220'}selected{/if} value="4220">4220 - Construcci?n de proyectos de servicio p?blico</option>
                    <option {if $coExtAttribute1==='4290'}selected{/if} value="4290">4290 - Construcci?n de otras obras de ingenier?a civil</option>
                    <option {if $coExtAttribute1==='4311'}selected{/if} value="4311">4311 - Demolici?n</option>
                    <option {if $coExtAttribute1==='4312'}selected{/if} value="4312">4312 - Preparaci?n del terreno</option>
                    <option {if $coExtAttribute1==='4321'}selected{/if} value="4321">4321 - Instalaciones el?ctricas</option>
                    <option {if $coExtAttribute1==='4322'}selected{/if} value="4322">4322 - Instalaciones de fontaner?a, calefacci?n y aire acondicionado</option>
                    <option {if $coExtAttribute1==='4329'}selected{/if} value="4329">4329 - Otras instalaciones especializadas</option>
                    <option {if $coExtAttribute1==='4330'}selected{/if} value="4330">4330 - Terminaci?n y acabado de edificios y obras de ingenier?a civil</option>
                    <option {if $coExtAttribute1==='4390'}selected{/if} value="4390">4390 - Otras actividades especializadas para la construcci?n de edificios y obras de ingenier?a civil</option>
                    <option {if $coExtAttribute1==='4511'}selected{/if} value="4511">4511 - Comercio de veh?culos automotores nuevos</option>
                    <option {if $coExtAttribute1==='4512'}selected{/if} value="4512">4512 - Comercio de veh?culos automotores usados</option>
                    <option {if $coExtAttribute1==='4520'}selected{/if} value="4520">4520 - Mantenimiento y reparaci?n de veh?culos automotores</option>
                    <option {if $coExtAttribute1==='4530'}selected{/if} value="4530">4530 - Comercio de partes, piezas (autopartes) y accesorios (lujos) para veh?culos automotores</option>
                    <option {if $coExtAttribute1==='4541'}selected{/if} value="4541">4541 - Comercio de motocicletas y de sus partes, piezas y accesorios</option>
                    <option {if $coExtAttribute1==='4542'}selected{/if} value="4542">4542 - Mantenimiento y reparaci?n de motocicletas y de sus partes y piezas</option>
                    <option {if $coExtAttribute1==='4610'}selected{/if} value="4610">4610 - Comercio al por mayor a cambio de una retribuci?n o por contrata</option>
                    <option {if $coExtAttribute1==='4620'}selected{/if} value="4620">4620 - Comercio al por mayor de materias primas agropecuarias; animales vivos</option>
                    <option {if $coExtAttribute1==='4631'}selected{/if} value="4631">4631 - Comercio al por mayor de productos alimenticios</option>
                    <option {if $coExtAttribute1==='4632'}selected{/if} value="4632">4632 - Comercio al por mayor de bebidas y tabaco</option>
                    <option {if $coExtAttribute1==='4641'}selected{/if} value="4641">4641 - Comercio al por mayor de productos textiles, productos confeccionados para uso dom?stico</option>
                    <option {if $coExtAttribute1==='4642'}selected{/if} value="4642">4642 - Comercio al por mayor de prendas de vestir</option>
                    <option {if $coExtAttribute1==='4643'}selected{/if} value="4643">4643 - Comercio al por mayor de calzado</option>
                    <option {if $coExtAttribute1==='4644'}selected{/if} value="4644">4644 - Comercio al por mayor de aparatos y equipo de uso dom?stico</option>
                    <option {if $coExtAttribute1==='4645'}selected{/if} value="4645">4645 - Comercio al por mayor de productos farmac?uticos, medicinales, cosm?ticos y de tocador</option>
                    <option {if $coExtAttribute1==='4649'}selected{/if} value="4649">4649 - Comercio al por mayor de otros utensilios dom?sticos n.c.p.</option>
                    <option {if $coExtAttribute1==='4651'}selected{/if} value="4651">4651 - Comercio al por mayor de computadores, equipo perif?rico y programas de inform?tica</option>
                    <option {if $coExtAttribute1==='4652'}selected{/if} value="4652">4652 - Comercio al por mayor de equipo, partes y piezas electr?nicos y de telecomunicaciones</option>
                    <option {if $coExtAttribute1==='4653'}selected{/if} value="4653">4653 - Comercio al por mayor de maquinaria y equipo agropecuarios</option>
                    <option {if $coExtAttribute1==='4659'}selected{/if} value="4659">4659 - Comercio al por mayor de otros tipos de maquinaria y equipo n.c.p.</option>
                    <option {if $coExtAttribute1==='4661'}selected{/if} value="4661">4661 - Comercio al por mayor de combustibles s?lidos, l?quidos, gaseosos y productos conexos</option>
                    <option {if $coExtAttribute1==='4662'}selected{/if} value="4662">4662 - Comercio al por mayor de metales y productos metal?feros</option>
                    <option {if $coExtAttribute1==='4663'}selected{/if} value="4663">4663 - Comercio al por mayor de materiales de construcci?n, art?culos de ferreter?a, pinturas, productos</option>
                    <option {if $coExtAttribute1==='4664'}selected{/if} value="4664">4664 - Comercio al por mayor de productos qu?micos b?sicos, cauchos y pl?sticos en formas primarias y productos qu?micos de uso agropecuario</option>
                    <option {if $coExtAttribute1==='4665'}selected{/if} value="4665">4665 - Comercio al por mayor de desperdicios, desechos y chatarra</option>
                    <option {if $coExtAttribute1==='4669'}selected{/if} value="4669">4669 - Comercio al por mayor de otros productos n.c.p.</option>
                    <option {if $coExtAttribute1==='4690'}selected{/if} value="4690">4690 - Comercio al por mayor no especializado</option>
                    <option {if $coExtAttribute1==='4711'}selected{/if} value="4711">4711 - Comercio al por menor en establecimientos no especializados con surtido compuesto principalmente por alimentos, bebidas o tabaco</option>
                    <option {if $coExtAttribute1==='4719'}selected{/if} value="4719">4719 - Comercio al por menor en establecimientos no especializados, con surtido compuesto principalmente por productos diferentes de alimentos (v?veres en general), bebidas y tabaco</option>
                    <option {if $coExtAttribute1==='4721'}selected{/if} value="4721">4721 - Comercio al por menor de productos agr?colas para el consumo en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4722'}selected{/if} value="4722">4722 - Comercio al por menor de leche, productos l?cteos y huevos, en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4723'}selected{/if} value="4723">4723 - Comercio al por menor de carnes (incluye aves de corral), productos c?rnicos, pescados y productos de mar, en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4724'}selected{/if} value="4724">4724 - Comercio al por menor de bebidas y productos del tabaco, en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4729'}selected{/if} value="4729">4729 - Comercio al por menor de otros productos alimenticios n.c.p., en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4731'}selected{/if} value="4731">4731 - Comercio al por menor de combustible para automotores</option>
                    <option {if $coExtAttribute1==='4732'}selected{/if} value="4732">4732 - Comercio al por menor de lubricantes (aceites, grasas), aditivos y productos de limpieza para veh?culos automotores</option>
                    <option {if $coExtAttribute1==='4741'}selected{/if} value="4741">4741 - Comercio al por menor de computadores, equipos perif?ricos, programas de inform?tica y equipos de telecomunicaciones en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4742'}selected{/if} value="4742">4742 - Comercio al por menor de equipos y aparatos de sonido y de video, en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4751'}selected{/if} value="4751">4751 - Comercio al por menor de productos textiles en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4752'}selected{/if} value="4752">4752 - Comercio al por menor de art?culos de ferreter?a, pinturas y productos de vidrio en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4753'}selected{/if} value="4753">4753 - Comercio al por menor de tapices, alfombras y cubrimientos para paredes y pisos en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4754'}selected{/if} value="4754">4754 - Comercio al por menor de electrodom?sticos y gasodom?sticos de uso dom?stico, muebles y equipos de iluminaci?n</option>
                    <option {if $coExtAttribute1==='4755'}selected{/if} value="4755">4755 - Comercio al por menor de art?culos y utensilios de uso dom?stico</option>
                    <option {if $coExtAttribute1==='4759'}selected{/if} value="4759">4759 - Comercio al por menor de otros art?culos dom?sticos en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4761'}selected{/if} value="4761">4761 - Comercio al por menor de libros, peri?dicos, materiales y art?culos de papeler?a y escritorio, en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4762'}selected{/if} value="4762">4762 - Comercio al por menor de art?culos deportivos, en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4769'}selected{/if} value="4769">4769 - Comercio al por menor de otros art?culos culturales y de entretenimiento n.c.p. en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4771'}selected{/if} value="4771">4771 - Comercio al por menor de prendas de vestir y sus accesorios (incluye art?culos de piel) en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4772'}selected{/if} value="4772">4772 - Comercio al por menor de todo tipo de calzado y art?culos de cuero y suced?neos del cuero en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4773'}selected{/if} value="4773">4773 - Comercio al por menor de productos farmac?uticos y medicinales, cosm?ticos y art?culos de tocador en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4774'}selected{/if} value="4774">4774 - Comercio al por menor de otros productos nuevos en establecimientos especializados</option>
                    <option {if $coExtAttribute1==='4775'}selected{/if} value="4775">4775 - Comercio al por menor de art?culos de segunda mano</option>
                    <option {if $coExtAttribute1==='4781'}selected{/if} value="4781">4781 - Comercio al por menor de alimentos, bebidas y tabaco, en puestos de venta m?viles</option>
                    <option {if $coExtAttribute1==='4782'}selected{/if} value="4782">4782 - Comercio al por menor de productos textiles, prendas de vestir y calzado, en puestos de venta m?viles</option>
                    <option {if $coExtAttribute1==='4789'}selected{/if} value="4789">4789 - Comercio al por menor de otros productos en puestos de venta m?viles</option>
                    <option {if $coExtAttribute1==='4791'}selected{/if} value="4791">4791 - Comercio al por menor realizado a trav?s de internet</option>
                    <option {if $coExtAttribute1==='4792'}selected{/if} value="4792">4792 - Comercio al por menor realizado a trav?s de casas de venta o por correo</option>
                    <option {if $coExtAttribute1==='4799'}selected{/if} value="4799">4799 - Otros tipos de comercio al por menor no realizado en establecimientos, puestos de venta o mercados</option>
                    <option {if $coExtAttribute1==='4911'}selected{/if} value="4911">4911 - Transporte f?rreo de pasajeros</option>
                    <option {if $coExtAttribute1==='4912'}selected{/if} value="4912">4912 - Transporte f?rreo de carga</option>
                    <option {if $coExtAttribute1==='4921'}selected{/if} value="4921">4921 - Transporte de pasajeros</option>
                    <option {if $coExtAttribute1==='4922'}selected{/if} value="4922">4922 - Transporte mixto</option>
                    <option {if $coExtAttribute1==='4923'}selected{/if} value="4923">4923 - Transporte de carga por carretera</option>
                    <option {if $coExtAttribute1==='4930'}selected{/if} value="4930">4930 - Transporte por tuber?as</option>
                    <option {if $coExtAttribute1==='5011'}selected{/if} value="5011">5011 - Transporte de pasajeros mar?timo y de cabotaje</option>
                    <option {if $coExtAttribute1==='5012'}selected{/if} value="5012">5012 - Transporte de carga mar?timo y de cabotaje</option>
                    <option {if $coExtAttribute1==='5021'}selected{/if} value="5021">5021 - Transporte fluvial de pasajeros</option>
                    <option {if $coExtAttribute1==='5022'}selected{/if} value="5022">5022 - Transporte fluvial de carga</option>
                    <option {if $coExtAttribute1==='5111'}selected{/if} value="5111">5111 - Transporte a?reo nacional de pasajeros</option>
                    <option {if $coExtAttribute1==='5112'}selected{/if} value="5112">5112 - Transporte a?reo internacional de pasajeros</option>
                    <option {if $coExtAttribute1==='5121'}selected{/if} value="5121">5121 - Transporte a?reo nacional de carga</option>
                    <option {if $coExtAttribute1==='5122'}selected{/if} value="5122">5122 - Transporte a?reo internacional de carga</option>
                    <option {if $coExtAttribute1==='5210'}selected{/if} value="5210">5210 - Almacenamiento y dep?sito</option>
                    <option {if $coExtAttribute1==='5221'}selected{/if} value="5221">5221 - Actividades de estaciones, v?as y servicios complementarios para el transporte terrestre</option>
                    <option {if $coExtAttribute1==='5222'}selected{/if} value="5222">5222 - Actividades de puertos y servicios complementarios para el transporte acu?tico</option>
                    <option {if $coExtAttribute1==='5223'}selected{/if} value="5223">5223 - Actividades de aeropuertos, servicios de navegaci?n a?rea y dem?s actividades conexas al transporte a?reo</option>
                    <option {if $coExtAttribute1==='5224'}selected{/if} value="5224">5224 - Manipulaci?n de carga</option>
                    <option {if $coExtAttribute1==='5229'}selected{/if} value="5229">5229 - Otras actividades complementarias al transporte</option>
                    <option {if $coExtAttribute1==='5310'}selected{/if} value="5310">5310 - Actividades postales nacionales</option>
                    <option {if $coExtAttribute1==='5320'}selected{/if} value="5320">5320 - Actividades de mensajer?a</option>
                    <option {if $coExtAttribute1==='5511'}selected{/if} value="5511">5511 - Alojamiento en hoteles</option>
                    <option {if $coExtAttribute1==='5512'}selected{/if} value="5512">5512 - Alojamiento en apartahoteles</option>
                    <option {if $coExtAttribute1==='5513'}selected{/if} value="5513">5513 - Alojamiento en centros vacacionales</option>
                    <option {if $coExtAttribute1==='5514'}selected{/if} value="5514">5514 - Alojamiento rural</option>
                    <option {if $coExtAttribute1==='5519'}selected{/if} value="5519">5519 - Otros tipos de alojamientos para visitantes</option>
                    <option {if $coExtAttribute1==='5520'}selected{/if} value="5520">5520 - Actividades de zonas de camping y parques para veh?culos recreacionales</option>
                    <option {if $coExtAttribute1==='5530'}selected{/if} value="5530">5530 - Servicio por horas</option>
                    <option {if $coExtAttribute1==='5590'}selected{/if} value="5590">5590 - Otros tipos de alojamiento n.c.p.</option>
                    <option {if $coExtAttribute1==='5611'}selected{/if} value="5611">5611 - Expendio a la mesa de comidas preparadas</option>
                    <option {if $coExtAttribute1==='5612'}selected{/if} value="5612">5612 - Expendio por autoservicio de comidas preparadas</option>
                    <option {if $coExtAttribute1==='5613'}selected{/if} value="5613">5613 - Expendio de comidas preparadas en cafeter?as</option>
                    <option {if $coExtAttribute1==='5619'}selected{/if} value="5619">5619 - Otros tipos de expendio de comidas preparadas n.c.p.</option>
                    <option {if $coExtAttribute1==='5621'}selected{/if} value="5621">5621 - Catering para eventos</option>
                    <option {if $coExtAttribute1==='5629'}selected{/if} value="5629">5629 - Actividades de otros servicios de comidas</option>
                    <option {if $coExtAttribute1==='5630'}selected{/if} value="5630">5630 - Expendio de bebidas alcoh?licas para el consumo dentro del establecimiento</option>
                    <option {if $coExtAttribute1==='5811'}selected{/if} value="5811">5811 - Edici?n de libros</option>
                    <option {if $coExtAttribute1==='5812'}selected{/if} value="5812">5812 - Edici?n de directorios y listas de correo</option>
                    <option {if $coExtAttribute1==='5813'}selected{/if} value="5813">5813 - Edici?n de peri?dicos, revistas y otras publicaciones peri?dicas</option>
                    <option {if $coExtAttribute1==='5819'}selected{/if} value="5819">5819 - Otros trabajos de edici?n</option>
                    <option {if $coExtAttribute1==='5820'}selected{/if} value="5820">5820 - Edici?n de programas de inform?tica (software)</option>
                    <option {if $coExtAttribute1==='5911'}selected{/if} value="5911">5911 - Actividades de producci?n de pel?culas cinematogr?ficas, videos, programas, anuncios y comerciales de televisi?n</option>
                    <option {if $coExtAttribute1==='5912'}selected{/if} value="5912">5912 - Actividades de posproducci?n de pel?culas cinematogr?ficas, videos, programas, anuncios y comerciales de televisi?n</option>
                    <option {if $coExtAttribute1==='5913'}selected{/if} value="5913">5913 - Actividades de distribuci?n de pel?culas cinematogr?ficas, videos, programas, anuncios y comerciales de televisi?n</option>
                    <option {if $coExtAttribute1==='5914'}selected{/if} value="5914">5914 - Actividades de exhibici?n de pel?culas cinematogr?ficas y videos</option>
                    <option {if $coExtAttribute1==='5920'}selected{/if} value="5920">5920 - Actividades de grabaci?n de sonido y edici?n de m?sica</option>
                    <option {if $coExtAttribute1==='6010'}selected{/if} value="6010">6010 - Actividades de programaci?n y transmisi?n en el servicio de radiodifusi?n sonora</option>
                    <option {if $coExtAttribute1==='6020'}selected{/if} value="6020">6020 - Actividades de programaci?n y transmisi?n de televisi?n</option>
                    <option {if $coExtAttribute1==='6110'}selected{/if} value="6110">6110 - Actividades de telecomunicaciones al?mbricas</option>
                    <option {if $coExtAttribute1==='6120'}selected{/if} value="6120">6120 - Actividades de telecomunicaciones inal?mbricas</option>
                    <option {if $coExtAttribute1==='6130'}selected{/if} value="6130">6130 - Actividades de telecomunicaci?n satelital</option>
                    <option {if $coExtAttribute1==='6190'}selected{/if} value="6190">6190 - Otras actividades de telecomunicaciones</option>
                    <option {if $coExtAttribute1==='6201'}selected{/if} value="6201">6201 - Actividades de desarrollo de sistemas inform?ticos (planificaci?n, an?lisis, dise?o, programaci?n, pruebas)</option>
                    <option {if $coExtAttribute1==='6202'}selected{/if} value="6202">6202 - Actividades de consultor?a inform?tica y actividades de administraci?n de instalaciones inform?ticas</option>
                    <option {if $coExtAttribute1==='6209'}selected{/if} value="6209">6209 - Otras actividades de tecnolog?as de informaci?n y actividades de servicios inform?ticos</option>
                    <option {if $coExtAttribute1==='6311'}selected{/if} value="6311">6311 - Procesamiento de datos, alojamiento (hosting) y actividades relacionadas</option>
                    <option {if $coExtAttribute1==='6312'}selected{/if} value="6312">6312 - Portales web</option>
                    <option {if $coExtAttribute1==='6391'}selected{/if} value="6391">6391 - Actividades de agencias de noticias</option>
                    <option {if $coExtAttribute1==='6399'}selected{/if} value="6399">6399 - Otras actividades de servicio de informaci?n n.c.p.</option>
                    <option {if $coExtAttribute1==='6411'}selected{/if} value="6411">6411 - Banco Central</option>
                    <option {if $coExtAttribute1==='6412'}selected{/if} value="6412">6412 - Bancos comerciales</option>
                    <option {if $coExtAttribute1==='6421'}selected{/if} value="6421">6421 - Actividades de las corporaciones financieras</option>
                    <option {if $coExtAttribute1==='6422'}selected{/if} value="6422">6422 - Actividades de las compa??as de financiamiento</option>
                    <option {if $coExtAttribute1==='6423'}selected{/if} value="6423">6423 - Banca de segundo piso</option>
                    <option {if $coExtAttribute1==='6424'}selected{/if} value="6424">6424 - Actividades de las cooperativas financieras</option>
                    <option {if $coExtAttribute1==='6431'}selected{/if} value="6431">6431 - Fideicomisos, fondos y entidades financieras similares</option>
                    <option {if $coExtAttribute1==='6432'}selected{/if} value="6432">6432 - Fondos de cesant?as</option>
                    <option {if $coExtAttribute1==='6491'}selected{/if} value="6491">6491 - Leasing financiero (arrendamiento financiero)</option>
                    <option {if $coExtAttribute1==='6492'}selected{/if} value="6492">6492 - Actividades financieras de fondos de empleados y otras formas asociativas del sector solidario</option>
                    <option {if $coExtAttribute1==='6493'}selected{/if} value="6493">6493 - Actividades de compra de cartera o factoring</option>
                    <option {if $coExtAttribute1==='6494'}selected{/if} value="6494">6494 - Otras actividades de distribuci?n de fondos</option>
                    <option {if $coExtAttribute1==='6495'}selected{/if} value="6495">6495 - Instituciones especiales oficiales</option>
                    <option {if $coExtAttribute1==='6499'}selected{/if} value="6499">6499 - Otras actividades de servicio financiero, excepto las de seguros y pensiones n.c.p.</option>
                    <option {if $coExtAttribute1==='6511'}selected{/if} value="6511">6511 - Seguros generales</option>
                    <option {if $coExtAttribute1==='6512'}selected{/if} value="6512">6512 - Seguros de vida</option>
                    <option {if $coExtAttribute1==='6513'}selected{/if} value="6513">6513 - Reaseguros</option>
                    <option {if $coExtAttribute1==='6514'}selected{/if} value="6514">6514 - Capitalizaci?n</option>
                    <option {if $coExtAttribute1==='6521'}selected{/if} value="6521">6521 - Servicios de seguros sociales de salud</option>
                    <option {if $coExtAttribute1==='6522'}selected{/if} value="6522">6522 - Servicios de seguros sociales de riesgos profesionales</option>
                    <option {if $coExtAttribute1==='6531'}selected{/if} value="6531">6531 - R?gimen de prima media con prestaci?n definida (RPM)</option>
                    <option {if $coExtAttribute1==='6532'}selected{/if} value="6532">6532 - R?gimen de ahorro individual (RAI)</option>
                    <option {if $coExtAttribute1==='6611'}selected{/if} value="6611">6611 - Administraci?n de mercados financieros</option>
                    <option {if $coExtAttribute1==='6612'}selected{/if} value="6612">6612 - Corretaje de valores y de contratos de productos b?sicos</option>
                    <option {if $coExtAttribute1==='6613'}selected{/if} value="6613">6613 - Otras actividades relacionadas con el mercado de valores</option>
                    <option {if $coExtAttribute1==='6614'}selected{/if} value="6614">6614 - Actividades de las casas de cambio</option>
                    <option {if $coExtAttribute1==='6615'}selected{/if} value="6615">6615 - Actividades de los profesionales de compra y venta de divisas</option>
                    <option {if $coExtAttribute1==='6619'}selected{/if} value="6619">6619 - Otras actividades auxiliares de las actividades de servicios financieros n.c.p.</option>
                    <option {if $coExtAttribute1==='6621'}selected{/if} value="6621">6621 - Actividades de agentes y corredores de seguros</option>
                    <option {if $coExtAttribute1==='6629'}selected{/if} value="6629">6629 - Evaluaci?n de riesgos y da?os, y otras actividades de servicios auxiliares</option>
                    <option {if $coExtAttribute1==='6630'}selected{/if} value="6630">6630 - Actividades de administraci?n de fondos</option>
                    <option {if $coExtAttribute1==='6810'}selected{/if} value="6810">6810 - Actividades inmobiliarias realizadas con bienes propios o arrendados</option>
                    <option {if $coExtAttribute1==='6820'}selected{/if} value="6820">6820 - Actividades inmobiliarias realizadas a cambio de una retribuci?n o por contrata</option>
                    <option {if $coExtAttribute1==='6910'}selected{/if} value="6910">6910 - Actividades jur?dicas</option>
                    <option {if $coExtAttribute1==='6920'}selected{/if} value="6920">6920 - Actividades de contabilidad, tenedur?a de libros, auditor?a financiera y asesor?a tributaria</option>
                    <option {if $coExtAttribute1==='7010'}selected{/if} value="7010">7010 - Actividades de administraci?n empresarial</option>
                    <option {if $coExtAttribute1==='7020'}selected{/if} value="7020">7020 - Actividades de consultar?a de gesti?n</option>
                    <option {if $coExtAttribute1==='7110'}selected{/if} value="7110">7110 - Actividades de arquitectura e ingenier?a y otras actividades conexas de consultor?a t?cnica</option>
                    <option {if $coExtAttribute1==='7120'}selected{/if} value="7120">7120 - Ensayos y an?lisis t?cnicos</option>
                    <option {if $coExtAttribute1==='7210'}selected{/if} value="7210">7210 - Investigaciones y desarrollo experimental en el campo de las ciencias naturales y la ingenier?a</option>
                    <option {if $coExtAttribute1==='7220'}selected{/if} value="7220">7220 - Investigaciones y desarrollo experimental en el campo de las ciencias sociales y las humanidades</option>
                    <option {if $coExtAttribute1==='7310'}selected{/if} value="7310">7310 - Publicidad</option>
                    <option {if $coExtAttribute1==='7320'}selected{/if} value="7320">7320 - Estudios de mercado y realizaci?n de encuestas de opini?n p?blica</option>
                    <option {if $coExtAttribute1==='7410'}selected{/if} value="7410">7410 - Actividades especializadas de dise?o</option>
                    <option {if $coExtAttribute1==='7420'}selected{/if} value="7420">7420 - Actividades de fotograf?a</option>
                    <option {if $coExtAttribute1==='7490'}selected{/if} value="7490">7490 - Otras actividades profesionales, cient?ficas y t?cnicas n.c.p.</option>
                    <option {if $coExtAttribute1==='7499'}selected{/if} value="7499">7499 - Other business activities</option>
                    <option {if $coExtAttribute1==='7500'}selected{/if} value="7500">7500 - Actividades veterinarias</option>
                    <option {if $coExtAttribute1==='7515'}selected{/if} value="7515">7515 - Auxiliary service activities</option>
                    <option {if $coExtAttribute1==='7530'}selected{/if} value="7530">7530 - Activities of social security</option>
                    <option {if $coExtAttribute1==='7710'}selected{/if} value="7710">7710 - Alquiler y arrendamiento de veh?culos automotores</option>
                    <option {if $coExtAttribute1==='7721'}selected{/if} value="7721">7721 - Alquiler y arrendamiento de equipo recreativo y deportivo</option>
                    <option {if $coExtAttribute1==='7722'}selected{/if} value="7722">7722 - Alquiler de videos y discos</option>
                    <option {if $coExtAttribute1==='7729'}selected{/if} value="7729">7729 - Alquiler y arrendamiento de otros efectos personales y enseres dom?sticos n.c.p.</option>
                    <option {if $coExtAttribute1==='7730'}selected{/if} value="7730">7730 - Alquiler y arrendamiento de otros tipos de maquinaria, equipo y bienes tangibles n.c.p.</option>
                    <option {if $coExtAttribute1==='7740'}selected{/if} value="7740">7740 - Arrendamiento de propiedad intelectual y productos similares, excepto obras protegidas por derechos de autor</option>
                    <option {if $coExtAttribute1==='7810'}selected{/if} value="7810">7810 - Actividades de agencias de empleo</option>
                    <option {if $coExtAttribute1==='7820'}selected{/if} value="7820">7820 - Actividades de agencias de empleo temporal</option>
                    <option {if $coExtAttribute1==='7830'}selected{/if} value="7830">7830 - Otras actividades de suministro de recurso humano</option>
                    <option {if $coExtAttribute1==='7911'}selected{/if} value="7911">7911 - Actividades de las agencias de viaje</option>
                    <option {if $coExtAttribute1==='7912'}selected{/if} value="7912">7912 - Actividades de operadores tur?sticos</option>
                    <option {if $coExtAttribute1==='7990'}selected{/if} value="7990">7990 - Otros servicios de reserva y actividades relacionadas</option>
                    <option {if $coExtAttribute1==='8010'}selected{/if} value="8010">8010 - Actividades de seguridad privada</option>
                    <option {if $coExtAttribute1==='8020'}selected{/if} value="8020">8020 - Actividades de servicios de sistemas de seguridad</option>
                    <option {if $coExtAttribute1==='8022'}selected{/if} value="8022">8022 - Average education academic</option>
                    <option {if $coExtAttribute1==='8030'}selected{/if} value="8030">8030 - Actividades de detectives e investigadores privados</option>
                    <option {if $coExtAttribute1==='8110'}selected{/if} value="8110">8110 - Actividades combinadas de apoyo a instalaciones</option>
                    <option {if $coExtAttribute1==='8121'}selected{/if} value="8121">8121 - Limpieza general interior de edificios</option>
                    <option {if $coExtAttribute1==='8129'}selected{/if} value="8129">8129 - Otras actividades de limpieza de edificios e instalaciones industriales</option>
                    <option {if $coExtAttribute1==='8130'}selected{/if} value="8130">8130 - Actividades de paisajismo y servicios de mantenimiento conexos</option>
                    <option {if $coExtAttribute1==='8211'}selected{/if} value="8211">8211 - Actividades combinadas de servicios administrativos de oficina</option>
                    <option {if $coExtAttribute1==='8219'}selected{/if} value="8219">8219 - Fotocopiado, preparaci?n de documentos y otras actividades especializadas de apoyo a oficina</option>
                    <option {if $coExtAttribute1==='8220'}selected{/if} value="8220">8220 - Actividades de centros de llamadas (Call center)</option>
                    <option {if $coExtAttribute1==='8230'}selected{/if} value="8230">8230 - Organizaci?n de convenciones y eventos comerciales</option>
                    <option {if $coExtAttribute1==='8291'}selected{/if} value="8291">8291 - Actividades de agencias de cobranza y oficinas de calificaci?n crediticia</option>
                    <option {if $coExtAttribute1==='8292'}selected{/if} value="8292">8292 - Actividades de envase y empaque</option>
                    <option {if $coExtAttribute1==='8299'}selected{/if} value="8299">8299 - Otras actividades de servicio de apoyo a las empresas n.c.p.</option>
                    <option {if $coExtAttribute1==='8411'}selected{/if} value="8411">8411 - Actividades legislativas de la administraci?n p?blica</option>
                    <option {if $coExtAttribute1==='8412'}selected{/if} value="8412">8412 - Actividades ejecutivas de la administraci?n p?blica</option>
                    <option {if $coExtAttribute1==='8413'}selected{/if} value="8413">8413 - Regulaci?n de las actividades de organismos que prestan servicios de salud, educativos, culturales y otros servicios sociales, excepto servicios de seguridad social</option>
                    <option {if $coExtAttribute1==='8414'}selected{/if} value="8414">8414 - Actividades reguladoras y facilitadoras de la actividad econ?mica</option>
                    <option {if $coExtAttribute1==='8415'}selected{/if} value="8415">8415 - Actividades de los otros ?rganos de control</option>
                    <option {if $coExtAttribute1==='8421'}selected{/if} value="8421">8421 - Relaciones exteriores</option>
                    <option {if $coExtAttribute1==='8422'}selected{/if} value="8422">8422 - Actividades de defensa</option>
                    <option {if $coExtAttribute1==='8423'}selected{/if} value="8423">8423 - Orden p?blico y actividades de seguridad</option>
                    <option {if $coExtAttribute1==='8424'}selected{/if} value="8424">8424 - Administraci?n de justicia</option>
                    <option {if $coExtAttribute1==='8430'}selected{/if} value="8430">8430 - Actividades de planes de seguridad social de afiliaci?n obligatoria</option>
                    <option {if $coExtAttribute1==='8511'}selected{/if} value="8511">8511 - Educaci?n de la primera infancia</option>
                    <option {if $coExtAttribute1==='8512'}selected{/if} value="8512">8512 - Educaci?n preescolar</option>
                    <option {if $coExtAttribute1==='8513'}selected{/if} value="8513">8513 - Educaci?n b?sica primaria</option>
                    <option {if $coExtAttribute1==='8521'}selected{/if} value="8521">8521 - Educaci?n b?sica secundaria</option>
                    <option {if $coExtAttribute1==='8522'}selected{/if} value="8522">8522 - Educaci?n media acad?mica</option>
                    <option {if $coExtAttribute1==='8523'}selected{/if} value="8523">8523 - Educaci?n media t?cnica y de formaci?n laboral</option>
                    <option {if $coExtAttribute1==='8530'}selected{/if} value="8530">8530 - Establecimientos que combinan diferentes niveles de educaci?n</option>
                    <option {if $coExtAttribute1==='8541'}selected{/if} value="8541">8541 - Educaci?n t?cnica profesional</option>
                    <option {if $coExtAttribute1==='8542'}selected{/if} value="8542">8542 - Educaci?n tecnol?gica</option>
                    <option {if $coExtAttribute1==='8543'}selected{/if} value="8543">8543 - Educaci?n de instituciones universitarias o de escuelas tecnol?gicas</option>
                    <option {if $coExtAttribute1==='8544'}selected{/if} value="8544">8544 - Educaci?n de universidades</option>
                    <option {if $coExtAttribute1==='8551'}selected{/if} value="8551">8551 - Formaci?n acad?mica no formal</option>
                    <option {if $coExtAttribute1==='8552'}selected{/if} value="8552">8552 - Ense?anza deportiva y recreativa</option>
                    <option {if $coExtAttribute1==='8553'}selected{/if} value="8553">8553 - Ense?anza cultural</option>
                    <option {if $coExtAttribute1==='8559'}selected{/if} value="8559">8559 - Otros tipos de educaci?n n.c.p.</option>
                    <option {if $coExtAttribute1==='8560'}selected{/if} value="8560">8560 - Actividades de apoyo a la educaci?n</option>
                    <option {if $coExtAttribute1==='8610'}selected{/if} value="8610">8610 - Actividades de hospitales y cl?nicas, con internaci?n</option>
                    <option {if $coExtAttribute1==='8621'}selected{/if} value="8621">8621 - Actividades de la pr?ctica m?dica, sin internaci?n</option>
                    <option {if $coExtAttribute1==='8622'}selected{/if} value="8622">8622 - Actividades de la pr?ctica odontol?gica</option>
                    <option {if $coExtAttribute1==='8691'}selected{/if} value="8691">8691 - Actividades de apoyo diagn?stico</option>
                    <option {if $coExtAttribute1==='8692'}selected{/if} value="8692">8692 - Actividades de apoyo terap?utico</option>
                    <option {if $coExtAttribute1==='8699'}selected{/if} value="8699">8699 - Otras actividades de atenci?n de la salud humana</option>
                    <option {if $coExtAttribute1==='8710'}selected{/if} value="8710">8710 - Actividades de atenci?n residencial medicalizada de tipo general</option>
                    <option {if $coExtAttribute1==='8720'}selected{/if} value="8720">8720 - Actividades de atenci?n residencial, para el cuidado de pacientes con retardo mental, enfermedad mental y consumo de sustancias psicoactivas</option>
                    <option {if $coExtAttribute1==='8730'}selected{/if} value="8730">8730 - Actividades de atenci?n en instituciones para el cuidado de personas mayores y/o discapacitadas</option>
                    <option {if $coExtAttribute1==='8790'}selected{/if} value="8790">8790 - Otras actividades de atenci?n en instituciones con alojamiento</option>
                    <option {if $coExtAttribute1==='8810'}selected{/if} value="8810">8810 - Actividades de asistencia social sin alojamiento para personas mayores y discapacitadas</option>
                    <option {if $coExtAttribute1==='8890'}selected{/if} value="8890">8890 - Otras actividades de asistencia social sin alojamiento</option>
                    <option {if $coExtAttribute1==='9001'}selected{/if} value="9001">9001 - Creaci?n literaria</option>
                    <option {if $coExtAttribute1==='9002'}selected{/if} value="9002">9002 - Creaci?n musical</option>
                    <option {if $coExtAttribute1==='9003'}selected{/if} value="9003">9003 - Creaci?n teatral</option>
                    <option {if $coExtAttribute1==='9004'}selected{/if} value="9004">9004 - Creaci?n audiovisual</option>
                    <option {if $coExtAttribute1==='9005'}selected{/if} value="9005">9005 - Artes pl?sticas y visuales</option>
                    <option {if $coExtAttribute1==='9006'}selected{/if} value="9006">9006 - Actividades teatrales</option>
                    <option {if $coExtAttribute1==='9007'}selected{/if} value="9007">9007 - Actividades de espect?culos musicales en vivo</option>
                    <option {if $coExtAttribute1==='9008'}selected{/if} value="9008">9008 - Otras actividades de espect?culos en vivo</option>
                    <option {if $coExtAttribute1==='9101'}selected{/if} value="9101">9101 - Actividades de bibliotecas y archivos</option>
                    <option {if $coExtAttribute1==='9102'}selected{/if} value="9102">9102 - Actividades y funcionamiento de museos, conservaci?n de edificios y sitios hist?ricos</option>
                    <option {if $coExtAttribute1==='9103'}selected{/if} value="9103">9103 - Actividades de jardines bot?nicos, zool?gicos y reservas naturales</option>
                    <option {if $coExtAttribute1==='9200'}selected{/if} value="9200">9200 - Actividades de juegos de azar y apuestas</option>
                    <option {if $coExtAttribute1==='9311'}selected{/if} value="9311">9311 - Gesti?n de instalaciones deportivas</option>
                    <option {if $coExtAttribute1==='9312'}selected{/if} value="9312">9312 - Actividades de clubes deportivos</option>
                    <option {if $coExtAttribute1==='9319'}selected{/if} value="9319">9319 - Otras actividades deportivas</option>
                    <option {if $coExtAttribute1==='9321'}selected{/if} value="9321">9321 - Actividades de parques de atracciones y parques tem?ticos</option>
                    <option {if $coExtAttribute1==='9329'}selected{/if} value="9329">9329 - Otras actividades recreativas y de esparcimiento n.c.p.</option>
                    <option {if $coExtAttribute1==='9411'}selected{/if} value="9411">9411 - Actividades de asociaciones empresariales y de empleadores</option>
                    <option {if $coExtAttribute1==='9412'}selected{/if} value="9412">9412 - Actividades de asociaciones profesionales</option>
                    <option {if $coExtAttribute1==='9420'}selected{/if} value="9420">9420 - Actividades de sindicatos de empleados</option>
                    <option {if $coExtAttribute1==='9491'}selected{/if} value="9491">9491 - Actividades de asociaciones religiosas</option>
                    <option {if $coExtAttribute1==='9492'}selected{/if} value="9492">9492 - Actividades de asociaciones pol?ticas</option>
                    <option {if $coExtAttribute1==='9499'}selected{/if} value="9499">9499 - Actividades de otras asociaciones n.c.p.</option>
                    <option {if $coExtAttribute1==='9511'}selected{/if} value="9511">9511 - Mantenimiento y reparaci?n de computadores y de equipo perif?rico</option>
                    <option {if $coExtAttribute1==='9512'}selected{/if} value="9512">9512 - Mantenimiento y reparaci?n de equipos de comunicaci?n</option>
                    <option {if $coExtAttribute1==='9521'}selected{/if} value="9521">9521 - Mantenimiento y reparaci?n de aparatos electr?nicos de consumo</option>
                    <option {if $coExtAttribute1==='9522'}selected{/if} value="9522">9522 - Mantenimiento y reparaci?n de aparatos y equipos dom?sticos y de jardiner?a</option>
                    <option {if $coExtAttribute1==='9523'}selected{/if} value="9523">9523 - Reparaci?n de calzado y art?culos de cuero</option>
                    <option {if $coExtAttribute1==='9524'}selected{/if} value="9524">9524 - Reparaci?n de muebles y accesorios para el hogar</option>
                    <option {if $coExtAttribute1==='9529'}selected{/if} value="9529">9529 - Mantenimiento y reparaci?n de otros efectos personales y enseres dom?sticos</option>
                    <option {if $coExtAttribute1==='9601'}selected{/if} value="9601">9601 - Lavado y limpieza, incluso la limpieza en seco, de productos textiles y de piel</option>
                    <option {if $coExtAttribute1==='9602'}selected{/if} value="9602">9602 - Peluquer?a y otros tratamientos de belleza</option>
                    <option {if $coExtAttribute1==='9603'}selected{/if} value="9603">9603 - Pompas f?nebres y actividades relacionadas</option>
                    <option {if $coExtAttribute1==='9609'}selected{/if} value="9609">9609 - Otras actividades de servicios personales n.c.p.</option>
                    <option {if $coExtAttribute1==='9700'}selected{/if} value="9700">9700 - Actividades de los hogares individuales como empleadores de personal dom?stico</option>
                    <option {if $coExtAttribute1==='9810'}selected{/if} value="9810">9810 - Actividades no diferenciadas de los hogares individuales como productores de bienes para uso pr?prio</option>
                    <option {if $coExtAttribute1==='9820'}selected{/if} value="9820">9820 - Actividades no diferenciadas de los hogares individuales como productores de servicios para uso pr?prio</option>
                    <option {if $coExtAttribute1==='9900'}selected{/if} value="9900">9900 - Actividades de organizaciones y entidades extraterritoriales</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorClassificationCode " class="form-text text-danger">&nbsp;{$errorClassificationTypeCode}</small>
            <label class="sr-only" for="inlineFormInputFiscalClassificationTypeCode">Fiscal Classification Type Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Type Code</div>
                </div>
                <select class="form-control" id="classificationTypeCode" name="classificationTypeCode">
                    <option value="">--</option>
                    <option {if $classificationTypeCode ==='TKE_LATAM_CO_CONTRIBUTOR'}selected{/if} value="TKE_LATAM_CO_CONTRIBUTOR">TKE LATAM CO CONTRIBUTOR</option>
                </select>
            </div>
        </div>
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorClassificationCode " class="form-text text-danger">&nbsp;{$errorClassificationCode}</small>
            <label class="sr-only" for="inlineFormInputFiscalClassificationCode">Fiscal Classification Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Code</div>
                </div>
                <select class="form-control" id="classificationCode" name="classificationCode" placeholder="Classification Code ">
                    <option value="">--</option>
                    <option {if $classificationCode ==='ENTIDADES ESTATALES'}selected{/if} value="ENTIDADES ESTATALES">ENTIDADES ESTATALES</option>
                    <option {if $classificationCode ==='EXTERIOR'}selected{/if} value="EXTERIOR">EXTERIOR</option>
                    <option {if $classificationCode ==='GRAN CONTRIB. AUTORETENEDOR'}selected{/if} value="GRAN CONTRIB. AUTORETENEDOR">GRAN CONTRIB. AUTORETENEDOR</option>
                    <option {if $classificationCode ==='GRAN CONTRIBUYENTE'}selected{/if} value="GRAN CONTRIBUYENTE">GRAN CONTRIBUYENTE</option>
                    <option {if $classificationCode ==='REGIMEN COMUN'}selected{/if} value="REGIMEN COMUN">REGIMEN COMUN</option>
                    <option {if $classificationCode ==='REGIMEN COMUN AUTORRETENEDOR'}selected{/if} value="REGIMEN COMUN AUTORRETENEDOR">REGIMEN COMUN AUTORRETENEDOR</option>
                    <option {if $classificationCode ==='REGIMEN SIMPLIFICADO'}selected{/if} value="REGIMEN SIMPLIFICADO">REGIMEN SIMPLIFICADO</option>
                    <option {if $classificationCode ==='ZONA FRANCA'}selected{/if} value="ZONA FRANCA">ZONA FRANCA</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-1 col-md-6 my-1">
            <small id="errorClassificationStartDate" class="form-text text-danger">&nbsp;{$errorClassificationStartDate}</small>
            <label class="sr-only" for="inlineFormClassificationStartDate">Fiscal Classification Start Date</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fiscal Classification Start Date</div>
                </div>
                <input class="form-control" id="classificationStartDate" name="classificationStartDate" aria-describedby="classificationStartDateHelp"/>
                <script>
                    $('#classificationStartDate').datepicker({
                        uiLibrary: 'bootstrap4',
                        format:'dd-mm-yyyy',
                        value: '{$classificationStartDate|date_format:"%d-%m-%Y"}'
                    });
                </script>
            </div>
        </div>
    </div>
    <br/>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{$actionLabel}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
    </div>

</form>

<br>
<h4>Vendor Site Contacts <a data-toggle="tooltip" data-placement="top"
                            title="add a new Contact to {$vendorSiteCode}"
                            href="{base_url()}VendorSiteContact/createNew/{$id}" style="cursor:pointer">
        <small><i class="text-primary fas fa-plus-circle fa-sm fa-fw"></i></small>
    </a></h4>
{if isset($contacts) && (sizeof($contacts)>0)}
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th scope="col"></th>
            <th>#</th>
            <th>Action</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Email Address</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$contacts item=c}
            <tr class="{if ($c.dismissed)} dismissed {elseif (($c.locallyValidated)&&($c.crossRefValidated)) } alert-success {else} alert-warning {/if} clickable-row"
                style="cursor:pointer"
                data-href='{base_url()}VendorSiteContact/customEditCleansed/{$c.country}/{$c.id}'>
                <th scope="row">
                    <a href="{base_url()}VendorSiteContact/editCleansed/{$c.id}" data-toggle="tooltip"
                       data-placement="top" title="inspect all fields from contact {$c.lastName}"><i
                                class="text-primary fas fa-edit fa-sm fa-fw"></i></a>
                    <a href="{base_url()}VendorSiteContact/toggle/{$c.id}" data-toggle="tooltip" data-placement="top"
                       title="{if ($c.dismissed)}Recall{else}Dismiss{/if} {$c.lastName}"><i
                                class="text-primary fas fa-minus-circle fa-sm fa-fw"></i></a>
                </th>
                <th scope="row">{$c.id}</th>
                <td>{$c.action}</td>
                <td>{$c.firstName}</td>
                <td>{$c.lastName}</td>
                <td>{$c.phoneNumber}</td>
                <td>{$c.emailAddress}</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
{/if}
<br/>
<br/>
<br/>
<br/>
<br/>
