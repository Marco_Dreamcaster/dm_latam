<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-row align-items-center">
        <div class="col-sm-6 my-1">
            <small id="errorVendorNumber" class="form-text text-danger">&nbsp;{$errorVendorNumber}</small>
            <label class="sr-only" for="inlineFormInputVendorNumber">Vendor Number</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Vendor Number</div>
                </div>
                <input type="text" class="form-control" id="vendorNumber" name="vendorNumber" value="{$vendorNumber}" placeholder="Vendor Number">
            </div>
        </div>
        <div class="col-sm-6 my-1">
            <small id="errorVendorSiteCode" class="form-text text-danger">&nbsp;{$errorVendorSiteCode}</small>
            <label class="sr-only" for="inlineFormInputVendorSiteCode">Vendor Site Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Vendor Site Code</div>
                </div>
                <input type="text" class="form-control" id="vendorSiteCode" name="vendorSiteCode" value="{$vendorSiteCode}" placeholder="Vendor Site Code">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-2 my-1">
            <small id="errorPrefix" class="form-text text-danger">&nbsp;{$errorPrefix}</small>
            <label class="sr-only" for="inlineFormInputPrefix">Prefix</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Prefix</div>
                </div>
                <select class="form-control" id="prefix" name="prefix">
                    <option value="">--</option>
                    <option {if $prefix==='MR.'}selected{/if} value="MR.">MR.</option>
                    <option {if $prefix==='MRS.'}selected{/if} value="MRS.">MRS.</option>
                    <option {if $prefix==='SR.'}selected{/if} value="SR.">SR.</option>
                    <option {if $prefix==='SRA.'}selected{/if} value="SRA.">SRA.</option>
                    <option {if $prefix==='DR.'}selected{/if} value="DR.">DR.</option>
                </select>
            </div>
        </div>

        <div class="col-sm-4 my-1">
            <small id="errorFirstName" class="form-text text-danger">&nbsp;{$errorFirstName}</small>
            <label class="sr-only" for="inlineFormInputFirstName">First Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">First Name </div>
                </div>
                <input type="text" class="form-control" id="firstName" name="firstName" value="{$firstName}" placeholder="First Name">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <small id="errorMiddleName" class="form-text text-danger">&nbsp;{$errorMiddleName}</small>
            <label class="sr-only" for="inlineFormInputMiddleName">Middle Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Middle Name </div>
                </div>
                <input type="text" class="form-control" id="middleName" name="middleName" value="{$middleName}" placeholder="Middle Name">
            </div>
        </div>
        <div class="col-sm-3 my-1">
            <small id="errorLastName" class="form-text text-danger text-left">&nbsp;<br/>{$errorLastName}</small>
            <label class="sr-only" for="inlineFormInputLastName">Last Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Last Name</div>
                </div>
                <input type="text" class="form-control" id="lastName" name="lastName" value="{$lastName}" placeholder="Last Name">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorEmailAddress" class="form-text text-danger">&nbsp;<br/>{$errorEmailAddress}</small>
            <label class="sr-only" for="inlineFormInputEmailAddress">Email Address</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Email Address</div>
                </div>
                <input type="text" class="form-control" id="emailAddress" name="emailAddress" value="{$emailAddress}" placeholder="Email Address">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-3 my-1">
            <small id="errorPhoneAreaCode" class="form-text text-danger">&nbsp;{$errorPhoneAreaCode}</small>
            <label class="sr-only" for="inlineFormInputPhoneAreaCode">Phone Area Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Phone Area Code </div>
                </div>
                <input type="text" class="form-control" id="phoneAreaCode" name="phoneAreaCode" value="{$phoneAreaCode}" placeholder="Phone Area Code">
            </div>
        </div>
        <div class="col-sm-9 my-1">
            <small id="errorPhoneNumber" class="form-text text-danger">&nbsp;{$errorPhoneNumber}</small>
            <label class="sr-only" for="inlineFormInputPhoneNumber">Phone Number</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Phone Number </div>
                </div>
                <input type="text" class="form-control" id="phoneNumber" name="phoneNumber" value="{$phoneNumber}" placeholder="Phone Number">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-3 my-1">
            <small id="errorFaxAreaCode" class="form-text text-danger">&nbsp;{$errorFaxAreaCode}</small>
            <label class="sr-only" for="inlineFormInputFaxAreaCode">Fax Area Code</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fax Area Code </div>
                </div>
                <input type="text" class="form-control" id="faxAreaCode" name="faxAreaCode" value="{$faxAreaCode}" placeholder="Fax Area Code">
            </div>
        </div>
        <div class="col-sm-9 my-1">
            <small id="errorFaxNumber" class="form-text text-danger">&nbsp;{$errorFaxNumber}</small>
            <label class="sr-only" for="inlineFormInputFaxNumber">Fax Number</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Fax Number </div>
                </div>
                <input type="text" class="form-control" id="faxNumber" name="faxNumber" value="{$faxNumber}" placeholder="Fax Number">
            </div>
        </div>
    </div>
    <div class="form-row align-items-center">
        <div class="col-sm-12 my-1">
            <small id="errorDepartment" class="form-text text-danger">&nbsp;{$errorDepartment}</small>
            <label class="sr-only" for="inlineFormInputDepartment">Department</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Department </div>
                </div>
                <input type="text" class="form-control" id="department" name="department" value="{$department}" placeholder="Department">
            </div>
        </div>
    </div>
    <br/>
    <div class="form-row align-items-center">
        <div class="col-sm-3 my-1">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">{$actionLabel}</button>
                <a class="btn btn-secondary my-2 my-sm-0" href="{base_url()}{$cancelAction}">Cancel</a>
            </div>
        </div>
    </div>

</form>
<br/>
<br/>
<br/>
<br/>
<br/>
