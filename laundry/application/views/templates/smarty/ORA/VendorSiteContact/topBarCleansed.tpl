<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><span class="flag-icon flag-icon-{strtolower($country)}" style="margin-top:10px;"></span>&nbsp;<span class="mt-2">{strtoupper($country)} {$title}</span>&nbsp;</h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}VendorSiteContact/editCleansed/{$id}" data-toggle="tooltip" data-placement="top" title="view ALL fields of vendorSiteContact for {$vendorNumber}"><i class="fas fa-edit fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}VendorSiteContact/customEditCleansed/{$country}/{$id}" data-toggle="tooltip" data-placement="top" title="quick view of vendorSiteContact for {$vendorNumber}"><i class="fas fa-eye fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}VendorSiteContact/validate/{$id}" data-toggle="tooltip" data-placement="top" title="manual validation of vendorSiteContact for {$vendorNumber}"><i class="fas fa-check-circle fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-secondary" target="_self" href="{base_url()}VendorSiteContact/toggle/{$id}" data-toggle="tooltip" data-placement="top" title="{if ($dismissed)}recall{else}dismiss{/if} vendorSiteContact for {$vendorNumber}"><i class="fas fa-minus-circle fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-success" target="_blank" href="{base_url()}IDL/Navigate/navigate/{$country}/Vendors/VendorSiteContact" data-toggle="tooltip" data-placement="top" title="specification for Vendors VendorSiteContact"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-secondary" target="_self" href="{base_url()}{$cancelAction}" data-toggle="tooltip" data-placement="top" title="cancel, and go back"><i class="fas fa-ban fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>

{if ($dismissed)}
    <div class="row">
        <div class="col-sm-12 p-2">
            <p class="dismissed text-center p-2">dismissed row, not going to flat file</p>
        </div>
    </div>
{else}
    <div class="row">
        <div class="col-sm-12 p-2">
            <p class="alert-success text-center p-2">active row</p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 p-2">
            {if (isset($cleansingAction)&&($cleansingAction!=''))}
                <p class="alert-warning text-center p-2">{$cleansingAction}</p>
            {else}
                <p class="alert-success text-center p-2">locally validated - no action required at this level</p>
            {/if}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 p-2">
            {if !($crossRefValidated)}
                <p class="alert-warning text-center p-2">problems in dependencies - check VendorSite</p>
            {else}
                <p class="alert-success text-center p-2">no problems, going to flat file</p>
            {/if}
        </div>
    </div>
{/if}



