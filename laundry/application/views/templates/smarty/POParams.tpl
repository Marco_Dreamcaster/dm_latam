<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><span class="flag-icon flag-icon-{strtolower($country)} mt-2"></span>&nbsp;<span class="mt-2">{$title}</span>&nbsp;</h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Transactional/{$country}" data-toggle="tooltip" data-placement="top" title="view all Transactionals for {$country}"><i class="fas fa-money-bill-alt fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>
<div class="navbar navbar-expand-lg navbar-light bg-light col-md-4 ml-sm-auto col-lg-12">
    <div class="tabbable col-lg-12">
        <ul class="nav nav-pills mt-2 justify-content-center">
            <li class="nav-item active"><a class="nav-link {if $topActive=='tablas'}active{/if} show" href="#tablas" data-toggle="tab">Tablas</a></li>
            <li class="nav-item"><a class="nav-link {if $topActive=='params'}active{/if}" href="#params" data-toggle="tab">Params</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane {if $topActive=='tablas'}active{/if}" id="tablas">
                <div class="span8">
                    <div class="tabbable">
                        <ul class="nav nav-pills mt-2">
                            <li class="nav-item"><a class="nav-link {if $bottomActive=='TipCont'}active{/if}" href="#TipCont" data-toggle="tab">TipoCont</a></li>
                        </ul>
                        <div class="tab-pane {if $bottomActive=='TipoCont'}active{/if} mt-4" id="TipCont">
                            <div class="mt-2 mb-4">
                                <form id="updatePOTipoCont" class="inline align-top" action="{base_url()}POParams/updatePOTipoCont/{$country}" method="POST">
                                    <input type="hidden" id="updatePOTipoCont_id" name="id"/>

                                    <div class="form-row align-items-center">
                                        <div class="col-sm-2 my-1">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="updatePOTipoCont_tipo" name="tipcos" disabled placeholder="Tipo Cont. Sigla" required/>
                                            </div>
                                            <small id="errorTipoCont" class="form-text text-danger">{$errorTipoCont}</small>
                                        </div>
                                        <div class="col-sm-3 my-1">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="updatePOTipoCont_descrip" name="Description" disabled placeholder="Descripcion" required/>
                                            </div>
                                            <small id="errorDescripTipoCont" class="form-text text-danger">{$errorDescripTipoCont}</small>
                                        </div>
                                        <div class="col-sm-2 my-1">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="updatePOTipoCont_tipoOracle" name="tipoOracle" disabled placeholder="Tipo Cont. Oracle" required/>
                                            </div>
                                            <small id="errorTipoContOracle" class="form-text text-danger">{$errorTipoContOracle}</small>
                                        </div>
                                        <div class="col-sm-2 my-1">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="updatePOTipoCont_ctaSigla" name="ctaSigla" placeholder="Cuenta Sigla" required maxlength="8"/>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <button type="submit" id="btnPOTipoCont" class="btn btn-primary"><i class="fas fa-check-circle fa-sm fa-fw text-white"></i> Cambiar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <script>
                                function onClickUpdateFormTipoCont(id, tipo, descrip, tipoOracle, ctaSigla){
                                    $('input[id="updatePOTipoCont_id"]').val(id);
                                    $('input[id="updatePOTipoCont_tipo"]').val(tipo);
                                    $('input[id="updatePOTipoCont_descrip"]').val(descrip);
                                    $('input[id="updatePOTipoCont_tipoOracle"]').val(tipoOracle);
                                    $('input[id="updatePOTipoCont_ctaSigla"]').val(ctaSigla);
                                }
                            </script>
                            <table id="ccTableTipoCont" class="display">
                                <thead>
                                <tr>
                                    <th scope="col">Tipo Sigla</th>
                                    <th scope="col">Descripcion</th>
                                    <th scope="col">Tipo Oracle</th>
                                    <th scope="col">Cuenta Sigla</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach from=$TipoCont item=c}
                                    <tr onclick="onClickUpdateFormTipoCont({$c.id},'{$c.tipo|trim}','{$c.descrip|trim}','{$c.tipoOracle|trim}','{$c.ctaSigla}')">
                                        <td>{$c.tipo}</td>
                                        <td>{$c.descrip}</td>
                                        <td>{$c.tipoOracle}</td>
                                        <td>{$c.ctaSigla}</td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th scope="col">Tipo Sigla</th>
                                    <th scope="col">Descripcion</th>
                                    <th scope="col">Tipo Oracle</th>
                                    <th scope="col">Cuenta Sigla</th>
                                </tr>
                                </tfoot>
                            </table>
                            <script>
                                $(document).ready(function() {
                                    var table = $('#ccTableTipoCont').DataTable({
                                        "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                                    } );
                                    table.order( [ 0, 'asc' ] ).draw();
                                } );
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<br/>
<br/>
<br/>