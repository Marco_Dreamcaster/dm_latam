<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><i class="fas fa-lg fa-sliders-h fa-fw m-2 text-stage"></i>&nbsp;<span class="m-2"><span class="flag-icon flag-icon-{strtolower($country)}"></span> {strtoupper($country)} STG {$title}</span></h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-danger" target="_self" href="{base_url()}RefreshFF/GL_DIAG/{$country}" data-toggle="tooltip" data-placement="top" title="GL Diagnostics for {$country}"><i class="fas fa-ambulance fa-sm fa-fw"></i>&nbsp;GL DIAG</a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}GLMenu/{$country}" data-toggle="tooltip" data-placement="top" title="go to GL Control Center for {$country}">GL Control Center&nbsp;<i class="fas fa-dollar-sign fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}GLTables/{$country}" data-toggle="tooltip" data-placement="top" title="go to GL Tables for {$country}">GL Tables&nbsp;<i class="fas fa-dollar-sign fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}GLParams/{$country}" data-toggle="tooltip" data-placement="top" title="go to GL Params for {$country}">GL Params&nbsp;<i class="fas fa-dollar-sign fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}RefreshFF/GLParams/{$country}" data-toggle="tooltip" data-placement="top" title="download Migration params for {$country}"><i class="fas fa-file-archive fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-danger" target="_blank" href="{base_url()}RefreshFF/InventoryDiagnostics/{$country}" data-toggle="tooltip" data-placement="top" title="Inventory diagnostics for {$country}"><i class="fas fa-ambulance fa-sm fa-fw"></i> INV DIAG</a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-danger" target="_blank" href="{base_url()}InspectFF/InventoryDiagnostics/{$country}/TKE_LA_CD_01_ALMACENES" data-toggle="tooltip" data-placement="top" title="complete list of all SIGLA {$country} Almacenes"><i class="fas fa-eye fa-sm fa-fw"></i> Inv Alm</a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-danger" target="_blank" href="{base_url()}InspectFF/InventoryDiagnostics/{$country}/TKE_LA_CD_01_CONFIG_ALMACENES" data-toggle="tooltip" data-placement="top" title="SIGLA Almacenes to ORACLE subInventoryOrgCodes"><i class="fas fa-eye fa-sm fa-fw"></i> Inv Map</a>
            </li>
        </ul>
    </div>
</nav>
<div class="navbar navbar-expand-lg navbar-light bg-light ml-sm-auto">
    <div class="tabbable justify-content-center col">
        <ul class="nav nav-pills mt-2">
            <li class="nav-item"><a class="nav-link active" href="#Locales" data-toggle="tab">Locales</a></li>
            <li class="nav-item"><a class="nav-link" href="#ConfigEstandar" data-toggle="tab">Defaults</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active mt-4  col-12" id="Locales">
                <div class="mt-2 mb-4">
                    <form id="updateGLLocales" class="inline align-top" action="{base_url()}GLParams/updateGLLocales/{$country}" method="POST">
                        <input type="hidden" id="updateGLLocales_id" name="id"/>

                        <div class="form-row align-items-center">
                            <div class="col-sm-2 my-1">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="updateGLLocales_local" name="Local" disabled placeholder="Local" required/>
                                </div>
                                <small id="errorLocal" class="form-text text-danger">{$errorLocal}</small>
                            </div>
                            <div class="col-sm-3 my-1">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="updateGLLocales_descrip" name="descrip_locales" disabled placeholder="Descripcion" required/>
                                </div>
                                <small id="errorDescripLocales" class="form-text text-danger">{$errorDescripLocales}</small>
                            </div>
                            <div class="col-sm-1 my-1">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="updateGLLocales_localOracle" name="localOracle" value="{$localOracle}" maxlength="6" required placeholder="Local Oracle"/>
                                </div>
                            </div>
                            <div class="col-auto">
                                <button type="submit" id="btnLocales" class="btn btn-primary">Update&nbsp;<i class="fas fa-check-circle fa-sm fa-fw text-white"></i></button>
                            </div>
                            <small id="errorLocales" class="form-text text-danger">{$errorLocales}</small>
                        </div>
                        <div class="form-row align-items-center">
                            <div class="col-2 my-1">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="updateGLLocales_idAlmacen" name="idAlmacen" value="{$idAlmacen}"  placeholder="list of almacenes"/>
                                </div>
                            </div>
                            <div class="col-2 my-1">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="updateGLLocales_subInvName" name="subInventoryName" value="{$subInventoryName}" maxlength="10" placeholder="SubInv Name"/>
                                </div>
                            </div>
                            <div class="col-2 my-1">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="updateGLLocales_subInvOrgCode" name="subInventoryOrgCode" value="{$subInventoryOrgCode}" maxlength="3" placeholder="SubInv OrgCode"/>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <script>
                    function onClickUpdateFormLocales(id, local, descrip, localOracle, idAlmacen, subInvName, subInvOrgCode, subInventoryDistributionAccount){
                        $('input[id="updateGLLocales_id"]').val(id);
                        $('input[id="updateGLLocales_local"]').val(local);
                        $('input[id="updateGLLocales_descrip"]').val(descrip);
                        $('input[id="updateGLLocales_localOracle"]').val(localOracle);
                        $('input[id="updateGLLocales_idAlmacen"]').val(idAlmacen);
                        $('input[id="updateGLLocales_subInvName"]').val(subInvName);
                        $('input[id="updateGLLocales_subInvOrgCode"]').val(subInvOrgCode);
                        $('input[id="updateGLLocales_subInventoryDistributionAccount"]').val(subInventoryDistributionAccount);
                        $('small[id="errorLocales"]').html("");
                    }
                </script>
                <table id="ccTableLocales" class="display">
                    <thead>
                    <tr>
                        <th scope="col">Local</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Local Oracle</th>
                        <th scope="col">Associated Almacen IDs</th>
                        <th scope="col">SubInv Name</th>
                        <th scope="col">SubInv orgCode</th>
                        <th scope="col">Distribution Account</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$Locales item=c}
                        <tr onclick="onClickUpdateFormLocales({$c.id},'{$c.local|trim}','{$c.descrip|trim}','{$c.localOracle|trim}','{$c.idAlmacen}','{$c.subInventoryName}','{$c.subInventoryOrgCode}','{$c.subInventoryDistributionAccount}')">
                            <td><a href="{base_url()}GLParams/clearGLLocales/{$c.id}/{$country}"><i class="fas fa-eraser fa-sm fa-fw text-danger"></i></a>&nbsp; {$c.local}</td>
                            <td>{$c.descrip}</td>
                            <td>{$c.localOracle}</td>
                            <td>{$c.idAlmacen}</td>
                            <td>{$c.subInventoryName}</td>
                            <td>{$c.subInventoryOrgCode}</td>
                            <td>{$c.subInventoryDistributionAccount}</td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th scope="col">Local</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Local Oracle</th>
                        <th scope="col">Associated Almac�n IDs</th>
                        <th scope="col">SubInv Name</th>
                        <th scope="col">SubInv orgCode</th>
                        <th scope="col">Distribution Account</th>
                    </tr>
                    </tfoot>
                </table>
                <script>
                    $(document).ready(function() {
                        var table = $('#ccTableLocales').DataTable({
                            "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                        } );
                        table.order( [ 0, 'asc' ] ).draw();
                    } );
                </script>
            </div>
            <div class="tab-pane mt-4" id="ConfigEstandar">
                <div class="mt-2 mb-4">
                    <form class="inline align-top" action="{base_url()}/GLParams/updateGLConfigEstandar/{$country}" method="POST">
                        <input type="hidden" class="form-control" id="id" name="id" value="{$ConfigEstandar['id']}">
                        <div class="row">
                            <h4 class="col-12 mt-2">Regional Configurations</h4>
                        </div>
                        <div class="row">
                            <div class="col-4 my-1">
                                <small id="errorCutoffDate" class="form-text text-danger">&nbsp;{$errorCutoffDate}</small>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Cutoff Date <span class="flag-icon flag-icon-{strtolower($country)} m-2" title="$country"></span> </div>
                                    </div>
                                    <input class="form-control" id="cutoffDate" name="cutoffDate" aria-describedby="cutoffDateHelp"/>
                                    <script>
                                        $('#cutoffDate').datepicker({
                                            uiLibrary: 'bootstrap4',
                                            format:'dd-mm-yyyy',
                                            value: '{$ConfigEstandar['cutoffDate']|date_format:"%d-%m-%Y"}'
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 my-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Branch</div>
                                    </div>
                                    <input type="text" class="form-control" id="branch" name="branch" value="{$ConfigEstandar['branch']}" required maxlength="6">
                                </div>
                            </div>
                            <div class="col-6 my-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Company</div>
                                    </div>
                                    <input type="text" class="form-control" id="companyAlt" name="companyAlt" value="{$ConfigEstandar['companyAlt']}" required maxlength="6">
                                </div>
                            </div>
                            <div class="col-12 my-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Org Name</div>
                                    </div>
                                    <input type="text" class="form-control" id="org_name" name="org_name" value="{$ConfigEstandar['org_name']}" disabled maxlength="60">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="col-12 mt-2">GL Params</h4>
                            <div class="col-12 my-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Account List for <a target="_self" href="{base_url()}RefreshFF/GL_DIAG/{$country}" data-toggle="tooltip" data-placement="top" title="GL Diagnostics for {$country}">&nbsp;GL DIAG</a></div>
                                    </div>
                                    <input type="text" class="form-control" id="bAccList" name="bAccList" value="{$ConfigEstandar['bAccList']}">
                                </div>
                            </div>
                            <div class="col-sm-4 my-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Local Currency</div>
                                    </div>
                                    <input type="text" class="form-control" id="localcurrency" name="localcurrency" value="{$ConfigEstandar['localcurrency']}" maxlength="6">
                                </div>
                            </div>
                            <div class="col-sm-4 my-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Ref. 1</div>
                                    </div>
                                    <input type="text" class="form-control" id="ref1" name="ref1" value="{$ConfigEstandar['ref1']}" maxlength="100">
                                </div>
                            </div>
                            <div class="col-sm-4 my-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Ref. 2</div>
                                    </div>
                                    <input type="text" class="form-control" id="ref2" name="ref2" value="{$ConfigEstandar['ref2']}" maxlength="240">
                                </div>
                            </div>
                            <div class="col-sm-4 my-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Ref. 4</div>
                                    </div>
                                    <input type="text" class="form-control" id="ref4" name="ref4" value="{$ConfigEstandar['ref4']}" maxlength="100">
                                </div>
                            </div>
                            <div class="col-sm-4 my-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Ref. 5</div>
                                    </div>
                                    <input type="text" class="form-control" id="ref5" name="ref5" value="{$ConfigEstandar['ref5']}" maxlength="240">
                                </div>
                            </div>
                            <div class="col-sm-4 my-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Ref. 10</div>
                                    </div>
                                    <input type="text" class="form-control" id="ref10" name="ref10" value="{$ConfigEstandar['ref10']}" maxlength="240">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="col-12 mt-2">Projects</h4>
                            <div class="col-4 my-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Service Manager Ref.</div>
                                    </div>
                                    <input type="text" class="form-control" id="Smref" name="Smref" value="{$ConfigEstandar['smref']}" maxlength="8">
                                </div>
                            </div>

                            <div class="col-sm-4 my-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Project Manager Ref.</div>
                                    </div>
                                    <input type="text" class="form-control" id="Pmref" name="Pmref" value="{$ConfigEstandar['pmref']}" maxlength="8">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="col-12 mt-2">Inventory</h4>
                            <div class="col-6 my-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Distribution Account Sufix</div>
                                    </div>
                                    <input type="text" class="form-control" id="inventoryDistAccountPrefix" name="inventoryDistAccountPrefix" value="{$ConfigEstandar['inventoryDistAccountPrefix']}">
                                </div>
                                <small id="errorInventoryDistAccountPrefix" class="form-text text-danger">{$errorInventoryDistAccountPrefix}</small>
                            </div>
                            <div class="col-6 my-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Default Locator Name</div>
                                    </div>
                                    <input type="text" class="form-control" id="defaultLocatorName" name="defaultLocatorName" value="{$ConfigEstandar['defaultLocatorName']}">
                                </div>
                                <small id="errorDefaultLocatorName" class="form-text text-danger">{$errorDefaultLocatorName}</small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mt-2 text-right">
                                <button type="submit" id="btnConfiguracionEstandar" class="btn btn-primary">Update&nbsp;<i class="fas fa-check-circle fa-sm fa-fw text-white"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>

<br/>
<br/>
<br/>
