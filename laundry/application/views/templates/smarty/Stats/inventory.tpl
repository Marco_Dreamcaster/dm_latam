<h5 class="nav p-2"><span class="flag-icon flag-icon-{strtolower($country)}"></span>&nbsp;<span class="mt-2">{strtoupper($country)} Inventory Stats</span>&nbsp;</h5>
<br/>
<div class="row p-1 text-pre-stage">
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body text-center">
                <h5 class="card-title">Total (RFM)+(nRFM)</h5>
                <h6 class="card-subtitle mb-2">pre-stage - all of SIGLA</h6>
                <h1 class="card-text ">{$total}</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body text-center">
                <h5 class="card-title">Recommended (RFM)</h5>
                <h6 class="card-subtitle mb-2">ranking >= 5</h6>
                <h1 class="card-text">{$recommendedDirty}</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body text-center">
                <h5 class="card-title">Not Recommended (nRFM)</h5>
                <h6 class="card-subtitle mb-2">ranking < 5</h6>
                <h1 class="card-text">{$unrecommendedDirty}</h1>
            </div>
        </div>
    </div>
</div>
<div class="row p-1">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body text-center text-danger">
                <h5 class="card-title">Dirty and Recommended (dRFM)</h5>
                <h6 class="card-subtitle mb-2">fitting criteria and still needs to be cleansed</h6>
                <h1 class="card-text">{$dirtyRecommended}</h1>
            </div>
        </div>
    </div>
</div>
<div class="row p-1 text-stage">
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body text-center">
                <h5 class="card-title">Staged (C) = (cRFM) + (cnRFM)</h5>
                <h6 class="card-subtitle mb-2">all on Stage (cleansed)</h6>
                <h1 class="card-text">{$cleansedDirty}</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body text-center">
                <h5 class="card-title">Staged and Recommended (cRFM)</h5>
                <h6 class="card-subtitle mb-2">currently fitting criteria</h6>
                <h1 class="card-text">{$cleansedRecommended}</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body text-center">
                <h5 class="card-title">Staged but Not Recommended (cnRFM)</h5>
                <h6 class="card-subtitle mb-2">cleansed, but not on criteria anymore</h6>
                <h1 class="card-text">{$cleansedUnrecommended}</h1>
            </div>
        </div>
    </div>
</div>
<div class="row p-1">
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body text-center text-pre-stage">
                <h5 class="card-title">Total (C)+(D)</h5>
                <h6 class="card-subtitle mb-2">all on SIGLA</h6>
                <h1 class="card-text">{$total}</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body text-center text-stage">
                <h5 class="card-title">Staged (C)</h5>
                <h6 class="card-subtitle mb-2">moved to Stage (cleansed)</h6>
                <h1 class="card-text">{$cleansedDirty}</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body text-center text-pre-stage">
                <h5 class="card-title">Dirty (D)</h5>
                <h6 class="card-subtitle mb-2">all still dirty</h6>
                <h1 class="card-text">{$dirtyDirty}</h1>
            </div>
        </div>
    </div>
</div>
<br/>
<div class="row p-1">
    <div class="col-sm-12">
        <div class="card alert-info">
            <div class="card-body text-center">
                <h5 class="card-title">RFM Major Criteria (strong)</h5>
                <h6 class="card-subtitle mb-2 text-muted">SALDO > 0 at date</h6>
                <h1 class="card-text text-info">+5</h1>
            </div>
        </div>
    </div>
</div>
<div class="row p-1">
    <div class="col-sm-4">
        <div class="card alert-info">
            <div class="card-body text-center">
                <h5 class="card-title">RFM Minor Criteria 1 (weak)</h5>
                <h6 class="card-subtitle mb-2 text-muted">CODIGOINTERNO starting with L (recently entered into SIGLA)</h6>
                <h1 class="card-text text-info">+1</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card alert-info">
            <div class="card-body text-center">
                <h5 class="card-title">RFM Minor Criteria 2 (weak)</h5>
                <h6 class="card-subtitle mb-2 text-muted">similar ORIG_ITEM_NUMBER (similar SIGLA ID)</h6>
                <h1 class="card-text text-info">+1</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card alert-info">
            <div class="card-body text-center">
                <h5 class="card-title">RFM Minor Criteria 3 (weak)</h5>
                <h6 class="card-subtitle mb-2 text-muted">similar CODIGO INTERNO (AKA CODIGO MASSIVO)</h6>
                <h1 class="card-text text-info">+1</h1>
            </div>
        </div>
    </div>
</div>
<div class="row p-1">
    <div class="col-sm-12">
        <div class="card alert-danger">
            <div class="card-body text-center">
                <h5 class="card-title">Planned obsolescence (by KU)</h5>
                <h6 class="card-subtitle mb-2 text-muted">obsolescencia planificada - baja rotacion</h6>
                <h5 class="card-text text-danger">ranking = 3 (not RFM)</h5>
            </div>
        </div>
    </div>
</div>
<br/>
<div class="row p-1">
    <canvas id="histogramRFM" class="col-sm-12 col-md-6 offset-md-3"></canvas>
</div>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<script src="{base_url()}js/chartJS/Chart.bundle.js" charset="UTF-8"></script>
<script src="{base_url()}js/chartJS/utils.js" charset="UTF-8"></script>

<script>
    $.getJSON( "{base_url()}IDL/Stats/grabRankingFrequency/{$country}/Inventory/PreItemHeader", function( sampledData ) {

        var barChartData = {
            labels: sampledData.data.labels,
            datasets: [{
                label: 'pre-STG',
                backgroundColor: window.chartColors.blue,
                data: sampledData.data.dataRFM
            }, {
                label: 'STG',
                backgroundColor: window.chartColors.green,
                data: sampledData.data.dataCleansedRFM
            }]

        };

        var ctx = document.getElementById('histogramRFM').getContext('2d');
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'RFM Ranking Histogram'
                },
                tooltips: {
                    mode: 'index',
                    intersect: true
                },
                scales: {
                    yAxes: [{
                        type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: 'left',
                        id: 'y-axis-1',
                    }, {
                        type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: false,
                        position: 'right',
                        id: 'y-axis-2',
                        gridLines: {
                            drawOnChartArea: false
                        }
                    }],
                }
            }
        });

        console.log(config);

        //var ctx = document.getElementById('histogramRFM').getContext('2d');
        //window.myLine = new Chart(ctx, config)


    });


</script>
