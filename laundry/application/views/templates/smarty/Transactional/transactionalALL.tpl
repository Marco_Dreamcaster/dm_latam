<script src="{base_url()}js/chartJS/Chart.bundle.js" charset="UTF-8"></script>
<script src="{base_url()}js/chartJS/utils.js" charset="UTF-8"></script>
<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-4 ml-sm-auto col-lg-12">
    <div class="d-flex justify-content-center col-md-4 ml-sm-auto col-lg-12">
        <form id="transactionalForm" class="form-inline" method="post" target="_self">
            <div class="m-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary p-1" type="submit" target="_blank" title="go to Transactional page for country">&nbsp;Go to &nbsp;</button>
                    </div>
                    <input class="custom-select-country" type="text" id="transactionalsForCountry" name="transactionalsForCountry" readonly="true" />
                    <input type="hidden" id="transactionalsForCountry_code" name="transactionalsForCountry_code" />
                </div>
            </div>
            <div class="m-2">
                <div class="input-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="btn btn-primary p-2 text-white">Cutoff Date</div>
                        </div>
                        <input class="form-control" id="cutoffDate" name="cutoffDate" aria-describedby="startDateHelp"/>
                        <script>
                            $('#cutoffDate').datepicker({
                                uiLibrary: 'bootstrap4',
                                format:'dd-mm-yyyy',
                                value: '{$cutoffDate}'
                            });
                        </script>
                    </div>
                    <small id="errorCutoffDate" class="form-text text-danger">{$errorCutoffDate}</small>
                </div>
            </div>
        </form>
    </div>
</nav>

<script>
    $(function() {
        $("#transactionalsForCountry").countrySelect({
            defaultCountry: "{$sessionCountry}",
            onlyCountries: ['co','mx','sv','ni','hn','gt','cr','cl','pe','uy','py','ar','pa']
        });

        $('#transactionalsForCountry').on('change', function() {
            console.log($('#transactionalsForCountry_code').val());
            location.href = "{base_url()}Transactional/"+$('#transactionalsForCountry_code').val().toUpperCase();
        });
    });
</script>
<script>
    $(function() {
        $('#inspectGLBalance').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/GL/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_FD_01_GL_BALANCE');
            $('#transactionalForm').submit();
        });
        $('#inspectGLResult').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/GL/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_FD_01_GL_RESULT');
            $('#transactionalForm').submit();
        });
        $('#inspectGLHistResult').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/GL/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_FD_01_GL_HISTRESULT');
            $('#transactionalForm').submit();
        });
        $('#inspectAP').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/AP/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_FD_04_AP');
            $('#transactionalForm').submit();
        });
        $('#inspectAR').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/AR/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_FD_04_AR');
            $('#transactionalForm').submit();
        });
        $('#inspectAgreements').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/Projects/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_CD_03_AGREEMENT_HEADER');
            $('#transactionalForm').submit();
        });
        $('#inspectAgreementFundings').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/Projects/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_CD_03_AGREEMENT_FUNDING');
            $('#transactionalForm').submit();
        });
        $('#inspectAgreements').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/Projects/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_CD_03_AGREEMENT_HEADER');
            $('#transactionalForm').submit();
        });
        $('#inspectAgreementFundings').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/Projects/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_CD_03_AGREEMENT_FUNDING');
            $('#transactionalForm').submit();
        });
        $('#inspectProjectBudget').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/Projects/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_CD_04_PROJECT_BUDGET_HEADER');
            $('#transactionalForm').submit();
        });
        $('#inspectProjectBudgetLine').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/Projects/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_CD_04_PROJECT_BUDGET_LINE');
            $('#transactionalForm').submit();
        });
        $('#inspectProjectRevenueInvoice').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/Projects/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_CD_06_REVENUE_INVOICE');
            $('#transactionalForm').submit();
        });
        $('#inspectProjectCosts').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/Projects/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_CD_05_PROJECT_COST');
            $('#transactionalForm').submit();
        });
        $('#inspectItemTrans').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/InventoryTrans/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_CD_01_ITEM_TRANS');
            $('#transactionalForm').submit();
        });
        $('#inspectGLConfigEstandar').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/GLParams/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_GL_CONFIG_ESTANDAR');
            $('#transactionalForm').submit();
        });
        $('#inspectGLLocales').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/GLParams/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_GL_LOCALES');
            $('#transactionalForm').submit();
        });

        $('#inspectRCPContratadas').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/Repairs/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/RCP_CONTRATADAS');
            $('#transactionalForm').submit();
        });

        $('#inspectGLParams').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/InventoryTrans/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_GL_PARAMS');
            $('#transactionalForm').submit();
        });




    });
</script>

<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade active show" id="pills-summary" role="tabpanel" aria-labelledby="pills-summary-tab">
        <div class="row mt-3 p-1">
            <div class="col-sm-12 mb-2 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">GL Params</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Bundled/GLParams/{$country}" data-toggle="tooltip" data-placement="top" title="download {$country} GLParams bundled zip"><i class="fas fa-file-archive fa-sm fa-fw"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">GL ConfigEstandar</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectGLConfigEstandar" data-toggle="tooltip" data-placement="top" title="refresh {$country} GL ConfigEstandar"><i class="fas fa-redo-alt fa-sm fa-fw text-white"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">GL Locales</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectGLLocales" data-toggle="tooltip" data-placement="top" title="refresh {$country} GL Locales"><i class="fas fa-redo-alt fa-sm fa-fw text-white"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3 p-1">
            <div class="col-sm-12 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">GL</h5>
                        <h6 class="card-subtitle mb-2">
                            <button class="btn btn-primary m-1" id="inspectGLBalance" type="submit" data-toggle="tooltip" data-placement="top"  target="_blank" title="inspect {$country} GL Balance">Balance</button>
                            <button class="btn btn-primary m-1" id="inspectGLResult" type="submit" data-toggle="tooltip" data-placement="top"  target="_blank" title="inspect {$country} GL Result">Result</button>
                            <button class="btn btn-primary m-1" id="inspectGLHistResult" type="submit" data-toggle="tooltip" data-placement="top"  target="_blank" title="inspect {$country} GL Historical Result">Historical Result</button>
                            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Bundled/GL/{$country}" data-toggle="tooltip" data-placement="top" title="download {$country} GL bundled flat files (zip)"><i class="fas fa-file-archive fa-sm fa-fw"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3 p-1">
            <div class="col-sm-6 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">AR Invoice</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectAR" data-toggle="tooltip" data-placement="top" title="inspect {$country} AR"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Bundled/AR/{$country}" data-toggle="tooltip" data-placement="top" title="download {$country} AR bundled flat files (zip)"><i class="fas fa-file-archive fa-sm fa-fw"></i></a>
                            <a class="btn btn-success m-1" target="_self" href="{base_url()}scripts/00.COM/transactional/01.Clientes_AR/01.IDL/TKE_LA_IDL_FD_04_AR_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="latest specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                        </h6>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">AP Invoice</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectAP" data-toggle="tooltip" data-placement="top" title="inspect {$country} AP"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Bundled/AP/{$country}" data-toggle="tooltip" data-placement="top" title="download {$country} AP bundled flat files (zip)"><i class="fas fa-file-archive fa-sm fa-fw"></i></a>
                            <a class="btn btn-success m-1" target="_self" href="{base_url()}scripts/00.COM/transactional/02.Vendedores_AP/01.IDL/AP_OPEN_INVOICE_TEMPLATE.xls" data-toggle="tooltip" data-placement="top" title="latest specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3 p-1">
            <div class="col-sm-12 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Inventory Transactions</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectItemTrans" data-toggle="tooltip" data-placement="top" title="refresh {$country} Items Transactions"><i class="fas fa-redo-alt fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Bundled/InventoryTrans/{$country}" data-toggle="tooltip" data-placement="top" title="download {$country} InventoryTrans bundled zip"><i class="fas fa-file-archive fa-sm fa-fw"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3 p-1">
            <div class="col-sm-12 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Repairs (beta early version)</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectRCPContratadas" data-toggle="tooltip" data-placement="top" title="inspect {$country} RCP Contratadas"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Bundled/Repairs/{$country}" data-toggle="tooltip" data-placement="top" title="download {$country} Repairs bundled flat files (zip)"><i class="fas fa-file-archive fa-sm fa-fw"></i></a>
                            <a class="btn btn-success m-1" target="_self" href="{base_url()}scripts\00.COM\master\08.Projects\01.IDL\TKE_LA_IDL_CD_02_PROJECT_HEADER_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="latest Project Header specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                            <a class="btn btn-success m-1" target="_self" href="{base_url()}scripts\00.COM\master\08.Projects\01.IDL\TKE_LA_IDL_CD_02_PROJECT_CLASSIFICATIONS_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="latest Project Classifications specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                            <a class="btn btn-success m-1" target="_self" href="{base_url()}scripts\00.COM\master\08.Projects\01.IDL\TKE_LA_IDL_CD_02_PROJECT_TASKS_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="latest Project Tasks specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                            <a class="btn btn-success m-1" target="_self" href="{base_url()}scripts\00.COM\master\08.Projects\01.IDL\TKE_LA_IDL_CD_02_PROJECT_RET_RULES_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="latest Project RetRules specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3 p-1">
            <div class="col-sm-6 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Agreements Header</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectAgreements" data-toggle="tooltip" data-placement="top" title="inspect {$country} AR"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Bundled/Projects/{$country}" data-toggle="tooltip" data-placement="top" title="download {$country} Projects bundled flat files (zip)"><i class="fas fa-file-archive fa-sm fa-fw"></i></a>
                            <a class="btn btn-success m-1" target="_self" href="{base_url()}scripts\00.COM\transactional\04.ProjectAgreements\01.IDL\TKE_LA_IDL_CD_03_AGREEMENT_HEADER_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="latest specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                        </h6>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Agreements Funding</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectAgreementFundings" data-toggle="tooltip" data-placement="top" title="inspect {$country} AP"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Bundled/Projects/{$country}" data-toggle="tooltip" data-placement="top" title="download {$country} Projects bundled flat files (zip)"><i class="fas fa-file-archive fa-sm fa-fw"></i></a>
                            <a class="btn btn-success m-1" target="_self" href="{base_url()}scripts\00.COM\transactional\04.ProjectAgreements\01.IDL\TKE_LA_IDL_CD_03_AGREEMENT_FUNDING_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="latest specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3 p-1">
            <div class="col-sm-6 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Budget Header</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectProjectBudget" data-toggle="tooltip" data-placement="top" title="inspect {$country} Project Budget"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Bundled/Projects/{$country}" data-toggle="tooltip" data-placement="top" title="download {$country} Projects bundled flat files (zip)"><i class="fas fa-file-archive fa-sm fa-fw"></i></a>
                            <a class="btn btn-success m-1" target="_self" href="{base_url()}scripts\00.COM\transactional\05.ProjectBudget\01.IDL\TKE_LA_IDL_CD_04_BUDGET_HEADER_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="latest specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                        </h6>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Budget Line</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectProjectBudgetLine" data-toggle="tooltip" data-placement="top" title="inspect {$country} Project Budget Line"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Bundled/Projects/{$country}" data-toggle="tooltip" data-placement="top" title="download {$country} Projects bundled flat files (zip)"><i class="fas fa-file-archive fa-sm fa-fw"></i></a>
                            <a class="btn btn-success m-1" target="_self" href="{base_url()}scripts\00.COM\transactional\05.ProjectBudget\01.IDL\TKE_LA_IDL_CD_04_BUDGET_LINE_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="latest specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
        <br/>
    </div>
</div>
<br/>
<br/>
<br/>
