<script src="{base_url()}js/chartJS/Chart.bundle.js" charset="UTF-8"></script>
<script src="{base_url()}js/chartJS/utils.js" charset="UTF-8"></script>
<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-4 ml-sm-auto col-lg-12">
    <div class="d-flex justify-content-center col-md-4 ml-sm-auto col-lg-12">
        <form id="transactionalForm" class="form-inline" method="post" target="_blank">
            <div class="m-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary p-1" type="submit" target="_blank" title="go to Transactional page for country">&nbsp;Go to &nbsp;</button>
                    </div>
                    <input class="custom-select-country" type="text" id="transactionalsForCountry" name="transactionalsForCountry" readonly="true" />
                    <input type="hidden" id="transactionalsForCountry_code" name="transactionalsForCountry_code" />
                </div>
            </div>
            <div class="m-2">
                <div class="input-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="btn btn-primary p-2 text-white">Cutoff Date</div>
                        </div>
                        <input class="form-control" id="cutoffDate" name="cutoffDate" aria-describedby="startDateHelp"/>
                        <script>
                            $('#cutoffDate').datepicker({
                                uiLibrary: 'bootstrap4',
                                format:'dd-mm-yyyy',
                                value: '{$cutoffDate}'
                            });
                        </script>
                    </div>
                    <small id="errorCutoffDate" class="form-text text-danger">{$errorCutoffDate}</small>
                </div>
            </div>
        </form>
    </div>
</nav>
<script>
    $(function() {
        $("#transactionalsForCountry").countrySelect({
            defaultCountry: "{$sessionCountry}",
            onlyCountries: ['co','mx','sv','ni','hn','gt','cr','cl','ec','pe','uy','py','ar','pa']
        });

        $('#transactionalsForCountry').on('change', function() {
            console.log($('#transactionalsForCountry_code').val());
            location.href = "{base_url()}Transactional/"+$('#transactionalsForCountry_code').val().toUpperCase();
        });

    });

</script>

<script>
    $(function() {
        $('#inspectAP').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/AP/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_FD_04_AP');
            $('#transactionalForm').submit();

        });

        $('#inspectAP_ALT').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/AP/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_FD_04_AP_ALT');
            $('#transactionalForm').submit();

        });

        $('#inspectAR').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/AR/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_FD_04_AR');
            $('#transactionalForm').submit();
        });

        $('#inspectGLBalance').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/GL/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_FD_01_GL_BALANCE');
            $('#transactionalForm').submit();
        });
        $('#inspectGLResult').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/GL/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_FD_01_GL_RESULT');
            $('#transactionalForm').submit();
        });
        $('#inspectGLHistResult').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/GL/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_FD_01_GL_HISTRESULT');
            $('#transactionalForm').submit();
        });

        $('#inspectGLBalanceDetalle').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/GL/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_FD_01_GL_BALANCE_DETALLE');
            $('#transactionalForm').submit();
        });

        $('#inspectAgreements').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/Projects/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_CD_03_AGREEMENT_HEADER');
            $('#transactionalForm').submit();
        });
        $('#inspectAgreementFundings').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/Projects/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_CD_03_AGREEMENT_FUNDING');
            $('#transactionalForm').submit();
        });
        $('#inspectProjectBudget').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/Projects/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_CD_04_PROJECT_BUDGET_HEADER');
            $('#transactionalForm').submit();
        });
        $('#inspectProjectBudgetLine').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/Projects/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_CD_04_PROJECT_BUDGET_LINE');
            $('#transactionalForm').submit();
        });
        $('#inspectProjectRevenueInvoice').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/Projects/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_CD_06_REVENUE_INVOICE');
            $('#transactionalForm').submit();
        });
        $('#inspectProjectCosts').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/Projects/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_CD_05_PROJECT_COST');
            $('#transactionalForm').submit();
        });
        $('#inspectItemTrans').on('click', function() {
            $('#transactionalForm').attr('action', '{base_url()}Inspect/Inventory/'+$('#transactionalsForCountry_code').val().toUpperCase()+'/TKE_LA_CD_01_ITEM_TRANS');
            $('#transactionalForm').submit();
        });
    });

</script>

<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade active show" id="pills-summary" role="tabpanel" aria-labelledby="pills-summary-tab">
        <div class="row mt-3 p-1">
            <div class="col-sm-12 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">GL</h5>
                        <h6 class="card-subtitle mb-2">
                            <button class="btn btn-primary m-1" id="inspectGLBalance" type="submit" data-toggle="tooltip" data-placement="top"  target="_blank" title="inspect {$country} GL Balance">Balance</button>
                            <button class="btn btn-danger m-1" id="inspectGLBalanceDetalle" type="submit" data-toggle="tooltip" data-placement="top"  target="_blank" title="inspect {$country} GL Balance Detalle">Balance Detalle</button>
                            <button class="btn btn-primary m-1" id="inspectGLResult" type="submit" data-toggle="tooltip" data-placement="top"  target="_blank" title="inspect {$country} GL Result">Result</button>
                            <button class="btn btn-primary m-1" id="inspectGLHistResult" type="submit" data-toggle="tooltip" data-placement="top"  target="_blank" title="inspect {$country} GL Historical Result">Historical Result</button>
                            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Bundled/GL/{$country}" data-toggle="tooltip" data-placement="top" title="download {$country} GL bundled flat files (zip)"><i class="fas fa-file-archive fa-sm fa-fw"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3 p-1">
            <div class="col-sm-6 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">AR Invoice</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectAR" data-toggle="tooltip" data-placement="top" title="inspect {$country} AR"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Bundled/AR/{$country}" data-toggle="tooltip" data-placement="top" title="download {$country} AR bundled flat files (zip)"><i class="fas fa-file-archive fa-sm fa-fw"></i></a>
                            <a class="btn btn-success m-1" target="_self" href="{base_url()}scripts/00.COM/transactional/01.Clientes_AR/01.IDL/TKE_LA_IDL_FD_04_AR_FLAT_FILE.xlsx" data-toggle="tooltip" data-placement="top" title="latest specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                        </h6>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">AP Invoice</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectAP" data-toggle="tooltip" data-placement="top" title="inspect {$country} AP"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Bundled/AP/{$country}" data-toggle="tooltip" data-placement="top" title="download {$country} AP bundled flat files (zip)"><i class="fas fa-file-archive fa-sm fa-fw"></i></a>
                            <a class="btn btn-warning m-1" id="inspectAP_ALT" data-toggle="tooltip" data-placement="top" title="inspect {$country} AP Alternative"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-success m-1" target="_self" href="{base_url()}scripts/00.COM/transactional/02.Vendedores_AP/01.IDL/AP_OPEN_INVOICE_TEMPLATE.xls" data-toggle="tooltip" data-placement="top" title="latest specification in Xcel format"><i class="fas fa-file-excel fa-sm fa-fw"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3 p-1">
            <div class="col-sm-12 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">POC Analysis</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectPOCAnalisis" target="_self" href="{base_url()}Inspect/Projects/{$country}/TKE_LA_CD_02_PROJECT_POC_ANALYSIS" data-toggle="tooltip" data-placement="top" title="inspect PA analisis (setiembre) 2018-09"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-success m-1" id="downloadPOCAnalisis" target="_self" href="{base_url()}scripts/01.PAN/transactional/04.ProjectAgreements/PA_analisis_POC_Octubre.xlsx" data-toggle="tooltip" data-placement="top" title="download PA analisis (setiembre) 2018-09"><span class="flag-icon flag-icon-pa"></span>&nbsp;<i class="fas fa-file-excel fa-sm fa-fw text-white"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3 p-1">
            <div class="col-sm-6 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Agreements</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectAgreements" data-toggle="tooltip" data-placement="top" title="inspect {$country} Agreements"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Bundled/Projects/{$country}" data-toggle="tooltip" data-placement="top" title="download {$country} Project bundled flat files (zip)"><i class="fas fa-file-archive fa-sm fa-fw"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Agreement Fundings</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectAgreementFundings" data-toggle="tooltip" data-placement="top" title="inspect {$country} Agreement Fundings"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Bundled/Projects/{$country}" data-toggle="tooltip" data-placement="top" title="download {$country} Project bundled flat files (zip)"><i class="fas fa-file-archive fa-sm fa-fw"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3 p-1">
            <div class="col-sm-6 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Budget</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectProjectBudget" target="_self" data-toggle="tooltip" data-placement="top" title="inspect {$country} Budget"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-primary m-1" id="downloadProjectBudget" target="_self"  href="{base_url()}Bundled/Projects/{$country}"}" data-toggle="tooltip" data-placement="top" title="download flat file {$country} Revenue Invoices"><i class="fas fa-file-archive fa-sm fa-fw text-white"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Budget Line</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectProjectBudgetLine" target="_self" data-toggle="tooltip" data-placement="top" title="inspect {$country} Budget Line"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-primary m-1" id="downloadProjectBudgetLine" target="_self"  href="{base_url()}Bundled/Projects/{$country}"}" data-toggle="tooltip" data-placement="top" title="download flat file {$country} Revenue Invoices"><i class="fas fa-file-archive fa-sm fa-fw text-white"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3 p-1">
            <div class="col-sm-6 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Revenue Invoices</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectProjectRevenueInvoice" target="_self" data-toggle="tooltip" data-placement="top" title="inspect {$country} Revenue Invoices"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-primary m-1" id="downloadProjectRevenueInvoice" target="_self"  href="{base_url()}Bundled/Projects/{$country}"}" data-toggle="tooltip" data-placement="top" title="download flat file {$country} Revenue Invoices"><i class="fas fa-file-archive fa-sm fa-fw text-white"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Project Costs</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectProjectCosts" target="_self" data-toggle="tooltip" data-placement="top" title="inspect {$country} Revenue Invoices"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-primary m-1" id="downloadProjectCosts" target="_self"  href="{base_url()}Bundled/Projects/{$country}"}" data-toggle="tooltip" data-placement="top" title="download flat file {$country} Revenue Invoices"><i class="fas fa-file-archive fa-sm fa-fw text-white"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3 p-1">
            <div class="col-sm-12 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Inventory Transactions</h5>
                        <h6 class="card-subtitle mb-2">
                            <a class="btn btn-primary m-1" id="inspectItemTrans" data-toggle="tooltip" data-placement="top" title="inspect {$country} Items Transactions"><i class="fas fa-eye fa-sm fa-fw text-white"></i></a>
                            <a class="btn btn-primary m-1" id="bundledItemTrans" data-toggle="tooltip" data-placement="top" title="download {$country} Inventory bundled flat files (zip)"><i class="fas fa-file-archive fa-sm fa-fw text-white"></i></a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
        <br/>
    </div>
</div>
<br/>
<br/>
<br/>



