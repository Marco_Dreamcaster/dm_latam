<script src="{base_url()}js/chartJS/Chart.bundle.js" charset="UTF-8"></script>
<script src="{base_url()}js/chartJS/utils.js" charset="UTF-8"></script>
<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-4 ml-sm-auto col-lg-12">
    <div class="d-flex justify-content-center col-md-4 ml-sm-auto col-lg-12">
        <form id="transactionalForm" class="form-inline" method="post" target="_blank">
            <div class="m-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary p-1" type="submit" target="_blank" title="go to Transactional page for country">&nbsp;Go to &nbsp;</button>
                    </div>
                    <input class="custom-select-country" type="text" id="transactionalsForCountry" name="transactionalsForCountry" readonly="true" />
                    <input type="hidden" id="transactionalsForCountry_code" name="transactionalsForCountry_code" />
                </div>
            </div>
            <div class="m-2">
                <div class="input-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="btn btn-primary p-2 text-white">Cutoff Date</div>
                        </div>
                        <input class="form-control" id="cutoffDate" name="cutoffDate" aria-describedby="startDateHelp"/>
                        <script>
                            $('#cutoffDate').datepicker({
                                uiLibrary: 'bootstrap4',
                                format:'dd-mm-yyyy',
                                value: '{$cutoffDate}'
                            });
                        </script>
                    </div>
                    <small id="errorCutoffDate" class="form-text text-danger">{$errorCutoffDate}</small>
                </div>
            </div>
        </form>
    </div>
</nav>
<script>
    $(function() {
        $("#transactionalsForCountry").countrySelect({
            defaultCountry: "{$sessionCountry}",
            onlyCountries: ['co','mx','sv','ni','hn','gt','cr','cl','ec','pe','uy','py','ar','pa'],
        });

        $('#transactionalsForCountry').on('change', function() {
            console.log($('#transactionalsForCountry_code').val());
            location.href = "{base_url()}Transactional/"+$('#transactionalsForCountry_code').val().toUpperCase();
        });

    });

</script>

<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade active show" id="pills-summary" role="tabpanel" aria-labelledby="pills-summary-tab">
        <div class="row mt-3 p-1">
            <div class="col-sm-12 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Unavailable</h5>
                        <h6 class="card-subtitle mb-2">
                            Transactional files for {$country} are unavailable
                        </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>
<br/>
<br/>



