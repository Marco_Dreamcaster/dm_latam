<br/>
<form action="{ldelim}base_url(){rdelim}{ldelim}$action{rdelim}" method="post" accept-charset="UTF-8">
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{ldelim}$actionLabel{rdelim}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{ldelim}base_url(){rdelim}{ldelim}$cancelAction{rdelim}">Cancel</a>
    </div>
{foreach from=$fields item=descriptor name=foo}
{if ($descriptor.ORA_TYPE=='VARCHAR2(1)')}
    <div class="form-group">
        <label for="{$descriptor.shortName}">{ucfirst($descriptor.shortName)}</label><br/>
        <small id="{$descriptor.shortName}Help" class="form-text text-muted">({$descriptor.description|trim|truncate:50})</small>
        <input type="checkbox" {ldelim}(${$descriptor.shortName}==true)?'checked':''{rdelim} id="{$descriptor.shortName}" name="{$descriptor.shortName}">
    </div>
{elseif ($descriptor.ORA_TYPE=='DATE')}
    <div class="form-group">
        <label for="{$descriptor.shortName}">{$descriptor.longName} <small id="{$descriptor.shortName}Help" class="form-text text-muted">({$descriptor.description|trim|truncate:50})</small></label>
        <input class="form-control" id="{$descriptor.shortName}" name="{$descriptor.shortName}" aria-describedby="{$descriptor.shortName}Help" width="276" />
        <script>
            $('#{$descriptor.shortName}').datepicker({
                uiLibrary: 'bootstrap4',
                format:'dd-mm-yyyy',
                value: '{ldelim}${$descriptor.shortName}|date_format:"%d-%m-%Y"{rdelim}'
            });
        </script>
        <small id="error{ucfirst($descriptor.shortName)}" class="form-text text-danger">{ldelim}$error{ucfirst($descriptor.shortName)}{rdelim}</small>
    </div>
{else}
    <div class="form-group">
        <label for="{$descriptor.shortName}">{$descriptor.longName} <small id="{$descriptor.shortName}Help" class="form-text text-muted">({$descriptor.description|trim|truncate:50})</small></label>
        <input type="text" class="form-control" id="{$descriptor.shortName}" name="{$descriptor.shortName}" aria-describedby="{$descriptor.shortName}Help" value="{ldelim}${$descriptor.shortName}{rdelim}" placeholder="{$descriptor.longName}...">
        <small id="error{ucfirst($descriptor.shortName)}" class="form-text text-danger">{ldelim}$error{ucfirst($descriptor.shortName)}{rdelim}</small>
    </div>
{/if}
{/foreach}
    <br/>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">{ldelim}$actionLabel{rdelim}</button>
        <a class="btn btn-secondary my-2 my-sm-0" href="{ldelim}base_url(){rdelim}{ldelim}$cancelAction{rdelim}">Cancel</a>
    </div>
    <br/>
    <br/>
    <br/>
</form>