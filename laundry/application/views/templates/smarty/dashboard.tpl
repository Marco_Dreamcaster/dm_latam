<div class="d-flex justify-content-center col-12">
    <h4>DM ORACLE LATAM - Cleansing Status on {$timestamp}</h4>
</div>

<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-4 ml-sm-auto col-lg-12">
    <div class="d-flex justify-content-center col-md-4 ml-sm-auto col-lg-12">
        <ul class="nav nav-pills mt-2" id="pills-tab" role="tablist">
            {foreach from=$dashboard['data'] key=k item=v}
                <li class="nav-item">
                    <a class="nav-link {if $k=='wave3'}active{/if}" id="pills-dashboard-tab" data-toggle="pill" href="#pills-{$k}" role="tab" aria-controls="pills-all" aria-selected="false">{$k}</a>
                </li>
            {/foreach}
        </ul>
    </div>
</nav>


<div class="tab-content" id="pills-tabContent">
    {foreach from=$dashboard['data'] key=k item=stats}
        <div class="tab-pane fade {if $k=='wave3'}active show{/if}" id="pills-{$k}" role="tabpanel" aria-labelledby="pills-master-tab">

            <table id="dashboardTable_{$k}" class="display m-2">
                <thead>
                <tr>
                    <th class="text-center" scope="col">Object</th>
                    {foreach $stats['countries'] as $c}
                        <th class="text-center" scope="col"><span class="flag-icon flag-icon-{strtolower($c)}" title="{$c}"></span>&nbsp;{$c}</th>
                    {/foreach}
                </tr>
                </thead>
                <tbody>
                {foreach $stats['data'] as $object=>$data}
                    {if ($object=='03.INVENTORY'||$object=='04.CUSTOMER'||$object=='05.VENDOR')}
                    <tr>
                        <td class="text-center">{$object}</td>
                        {foreach $stats['countries'] as $c}
                            <td class="text-center p-2">
                                <a target="_self" href="{base_url()}KPI/{$c}"
                                   class="{if ($data[$c]['status']=='ok')}text-success
                                           {elseif ($data[$c]['status']=='warning')}text-warning
                                           {elseif ($data[$c]['status']=='nirvana')}text-TKE
                                            {else}text-danger{/if}"
                                    title="{$c} - {$data[$c]['done']}/{$data[$c]['target']}"
                                    style="text-decoration: none">
                                    <p class="lead">
                                        {$data[$c]['target']}&nbsp;<small>({$data[$c]['percent']})</small>
                                    </p>
                                    <p>
                                        {if ($data[$c]['status']=='ok')}<i class="fas fa-thumbs-up fa-sm fa-fw"></i>{elseif ($data[$c]['status']=='warning')}<i class="fas fa-exclamation-triangle fa-sm fa-fw"></i>{elseif ($data[$c]['status']=='nirvana')}<i class="fas fa-smile-beam fa-sm fa-fw"></i>{else}<i class="fas fa-exclamation-circle fa-sm fa-fw"></i>{/if}</small>
                                    </p>
                                </a>
                            </td>
                        {/foreach}
                    </tr>
                    {elseif ($object=='01.SNAPSHOT'||$object=='02.CUTOFF')}
                    <tr>
                        <td class="text-center">{$object}</td>
                        {foreach $stats['countries'] as $c}
                            <td class="text-center p-2">
                                <p class="lead">
                                    {$data[$c]['date']}
                                </p>
                                <p>
                                    &nbsp;<small>({$data[$c]['dminus']})</small>
                                </p>
                            </td>
                        {/foreach}
                    </tr>
                    {/if}
                {/foreach}
                </tbody>
                <tfoot>
                <tr>
                    <th class="text-center" scope="col">Object</th>
                    {foreach $stats['countries'] as $c}
                        <th class="text-center" scope="col"><span class="flag-icon flag-icon-{strtolower($c)}" title="{$c}"></span>&nbsp;{$c}</th>
                    {/foreach}
                </tr>
                </tfoot>
            </table>

            <script>
                $(document).ready(function() {
                    var table = $('#dashboardTable_{$k}').DataTable({
                        "paging": false,
                        "lengthMenu": [[50, 100, -1], [50, 100, "All"]]
                    } );
                    table.order( [ 0, 'asc' ] ).draw();
                } );
            </script>
            <br/>
            <br/>
            <br/>
            <br/>

        </div>
    {/foreach}
</div>

<br/>
<br/>
<br/>


