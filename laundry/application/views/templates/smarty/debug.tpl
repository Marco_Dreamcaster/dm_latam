<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><span class="mt-2"><i class="fas fa-bug fa-lg fa-fw m-2"></i>&nbsp;{$title}</span></h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-danger" target="_self" href="{base_url()}RefreshFF/Status/PA" data-toggle="tooltip" data-placement="top" title="run Status for PA"><i class="fas fa-thermometer-half fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}ManageFF" data-toggle="tooltip" data-placement="top" title="manage all bundles"><i class="fas fa-boxes fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}IDL/groups" data-toggle="tooltip" data-placement="top" title="view all IDLs by country"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Maps/ORAGeoCodes/testJSON" data-toggle="tooltip" data-placement="top" title="new JSON combos for ORA Geo Codes"><i class="fas fa-map-marker-alt fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-warning" target="_blank" href="{base_url()}Maps/ORAGeoCodes/list" data-toggle="tooltip" data-placement="top" title="renewed Oracle Geo Codes"><i class="fas fa-map-marker-alt fa-sm fa-fw text-white"></i></a>
            </li>
            <li class="nav-item pt-2 p-1">
                <a class="btn btn-primary" target="_blank" href="{base_url()}Maps/TKEFamilyLines/testJSON" data-toggle="tooltip" data-placement="top" title="new JSON combos for SCC Family Lines"><i class="fas fa-table fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>

<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-4 ml-sm-auto col-lg-12">
    <div class="d-flex justify-content-center col-md-4 ml-sm-auto col-lg-12">
        <ul class="nav nav-pills mt-2" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link show" id="pills-audit-tab" data-toggle="pill" href="#pills-audit" role="tab" aria-controls="pills-audit" aria-selected="true">Audit Logs</a>
            </li>
            <li class="nav-item">
                <a class="nav-link show" id="pills-quickEplode-tab" data-toggle="pill" href="#pills-quickExplode" role="tab" aria-controls="pills-quickExplode" aria-selected="false">Quick Explode</a>
            </li>
            <li class="nav-item">
                <a class="nav-link show" id="pills-setup-tab" data-toggle="pill" href="#pills-setup" role="tab" aria-controls="pills-setup" aria-selected="false">SQL Server Setup</a>
            </li>
        </ul>
    </div>
</nav>
<br/>

<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade {if $show=='logs'}active{/if} show" id="pills-audit" role="tabpanel" aria-labelledby="pills-audit-tab">
        <table id="auditTable" class="display" style="width:100%">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Country</th>
                <th scope="col">Stamp</th>
                <th scope="col">User</th>
                <th scope="col">Type</th>
                <th scope="col">Event</th>
                <th scope="col">Count</th>
                <th scope="col">Target</th>
                <th scope="col">Done</th>
                <th scope="col">Cutoff</th>
                <th scope="col">Snapshot</th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$logs item=l}
                <tr>
                    <th scope="row">{$l.id}</th>
                    <td>{$l.country}</td>
                    <td>{$l.stamp->format('Y-m-d H:i:s')}</td>
                    <td>{$l.user}</td>
                    <td>{$l.type}</td>
                    <td>{$l.event}</td>
                    <td>{$l.count}</td>
                    <td>{$l.target}</td>
                    <td>{$l.done}</td>
                    <td>{if isset($l.cutoffDate)}{$l.cutoffDate->format('Y-m-d H:i:s')}{else}-{/if}</td>
                    <td>{if isset($l.snapshotDate)}{$l.snapshotDate->format('Y-m-d H:i:s')}{else}-{/if}</td>
                </tr>
            {/foreach}
            </tbody>
            <tfoot>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Country</th>
                <th scope="col">Stamp</th>
                <th scope="col">User</th>
                <th scope="col">Type</th>
                <th scope="col">Event</th>
                <th scope="col">Count</th>
                <th scope="col">Target</th>
                <th scope="col">Done</th>
                <th scope="col">Cutoff</th>
                <th scope="col">Snapshot</th>
            </tr>
            </tfoot>
        </table>
        <script>
            $(document).ready(function() {
                var table = $('#auditTable').DataTable({
                    "lengthMenu": [[50, 100, -1], [50, 100, "All"]]
                } );
                table.order( [ 0, 'desc' ] ).draw();
            } );
        </script>
        <br/>
        <br/>
        <br/>
        <br/>
    </div>
    <div class="tab-pane fade {if $show=='explode'}active{/if} show" id="pills-quickExplode" role="tabpanel" aria-labelledby="pills-quickExplode-tab">
        <form action="{base_url()}{$action}" method="post">
            <div class="form-group">
                <label for="flatfileLine">Flatfile input line</label>
                <textarea class="form-control" id="flatfileLine" name="flatfileLine" rows="4"></textarea>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Explode</button>
                </div>
            </div>
        </form>
    </div>
    <div class="tab-pane fade show" id="pills-setup" role="tabpanel" aria-labelledby="pills-setup-tab">
        <div>
            <p>
            <h5>Master Data</h5>
            </p>
        </div>
        <div class="row p-1">
            <div class="col-12 text-center text-pre-stage m-4">
                <h3>00.COM</h3>
                <small class="text-muted">scripts common to all countries</small>
            </div>
            <div class="col-sm-2 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Inventory</h5>
                        <a class="btn btn-primary" target="_self" href="{base_url()}InspectSQLCMD/setupInventory" data-toggle="tooltip" data-placement="top" title="inspect Inventory scripts locations"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                        <a class="btn btn-success" target="_self" href="{base_url()}RunSQLCMD/setupInventory" data-toggle="tooltip" data-placement="top" title="run setup Inventory"><i class="fas fa-play fa-sm fa-fw"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Customers</h5>
                        <a class="btn btn-primary" target="_self" href="{base_url()}InspectSQLCMD/setupCustomers" data-toggle="tooltip" data-placement="top" title="inspect Customers scripts locations"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                        <a class="btn btn-success" target="_self" href="{base_url()}RunSQLCMD/setupCustomers" data-toggle="tooltip" data-placement="top" title="run setup Customers"><i class="fas fa-play fa-sm fa-fw"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-sm-2 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Vendors</h5>
                        <a class="btn btn-primary" target="_self" href="{base_url()}InspectSQLCMD/setupVendors" data-toggle="tooltip" data-placement="top" title="inspect Vendors scripts locations"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                        <a class="btn btn-success" target="_self" href="{base_url()}RunSQLCMD/setupVendors" data-toggle="tooltip" data-placement="top" title="run setup Vendors"><i class="fas fa-play fa-sm fa-fw"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-sm-2 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Projects</h5>
                        <a class="btn btn-primary" target="_self" href="{base_url()}InspectSQLCMD/setupProjects" data-toggle="tooltip" data-placement="top" title="inspect Projects scripts locations"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                        <a class="btn btn-success" target="_self" href="{base_url()}RunSQLCMD/setupProjects" data-toggle="tooltip" data-placement="top" title="run setup Projects"><i class="fas fa-play fa-sm fa-fw"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Services</h5>
                        <a class="btn btn-primary" target="_self" href="{base_url()}InspectSQLCMD/setupServices" data-toggle="tooltip" data-placement="top" title="inspect Services scripts locations"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                        <a class="btn btn-success" target="_self" href="{base_url()}RunSQLCMD/setupServices" data-toggle="tooltip" data-placement="top" title="run setup Services"><i class="fas fa-play fa-sm fa-fw"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 text-pre-stage">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title">Repairs</h5>
                        <a class="btn btn-primary" target="_self" href="{base_url()}InspectSQLCMD/setupRepairs" data-toggle="tooltip" data-placement="top" title="inspect Services scripts locations"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                        <a class="btn btn-success" target="_self" href="{base_url()}RunSQLCMD/setupRepairs" data-toggle="tooltip" data-placement="top" title="run setup Services"><i class="fas fa-play fa-sm fa-fw"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row p-1">
            <div class="col-12 text-center text-pre-stage m-4">
                <h3>By Country</h3>
                <small class="text-muted">major procedures for each country</small>
            </div>
            {foreach $countries as $country}
                <div class="col-4 text-pre-stage">
                    <div class="card m-2">
                        <div class="card-body text-center">
                            <h5 class="card-title"><span class="flag-icon flag-icon-{$country|lower} mr-1 ml-1" title="{$country}"></span></h5>
                            {if $country=='PA'}
                                <p class="text-muted">unavailable</p>
                            {else}
                                <a class="btn btn-success" target="_blank" href="{base_url()}RefreshFF/Load/{$country}" data-toggle="tooltip" data-placement="top" title="define load procedures for {$country}">Load <i class="fas fa-truck-loading fa-sm fa-fw"></i></a>
                                <a class="btn btn-warning text-white" target="_blank" href="{base_url()}RefreshFF/Sync/{$country}" data-toggle="tooltip" data-placement="top" title="synchronize with SIGLA snapshot from {$country}.  Don't overload the server!!">SIGLA to preSTG <i class="fas fa-upload fa-sm fa-fw"></i></a>
                            {/if}
                        </div>
                    </div>
                </div>
            {/foreach}

        </div>
        <div>
            <p><h5>Transactional</h5></p>
        </div>
        <div class="tab-pane fade show" id="pills-setup" role="tabpanel" aria-labelledby="pills-setup-tab">
            <div class="row p-1">
                <div class="col-sm-2 text-pre-stage">
                    <div class="card">
                        <div class="card-body text-center">
                            <h5 class="card-title">Customers AR</h5>
                            <a class="btn btn-primary" target="_self" href="{base_url()}InspectSQLCMD/setupAR" data-toggle="tooltip" data-placement="top" title="inspect AR scripts locations"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                            <a class="btn btn-success" target="_self" href="{base_url()}RunSQLCMD/setupAR" data-toggle="tooltip" data-placement="top" title="run setup AR"><i class="fas fa-play fa-sm fa-fw"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 text-pre-stage">
                    <div class="card">
                        <div class="card-body text-center">
                            <h5 class="card-title">Vendors AP</h5>
                            <a class="btn btn-primary" target="_self" href="{base_url()}InspectSQLCMD/setupAP" data-toggle="tooltip" data-placement="top" title="inspect AR scripts locations"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                            <a class="btn btn-success" target="_self" href="{base_url()}RunSQLCMD/setupAP" data-toggle="tooltip" data-placement="top" title="run setup AR"><i class="fas fa-play fa-sm fa-fw"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 text-pre-stage">
                    <div class="card">
                        <div class="card-body text-center">
                            <h5 class="card-title">General Ledger (GL)</h5>
                            <a class="btn btn-primary" target="_self" href="{base_url()}InspectSQLCMD/setupGL" data-toggle="tooltip" data-placement="top" title="inspect GL scripts locations"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                            <a class="btn btn-success" target="_self" href="{base_url()}RunSQLCMD/setupGL" data-toggle="tooltip" data-placement="top" title="run setup GL"><i class="fas fa-play fa-sm fa-fw"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 text-pre-stage">
                    <div class="card">
                        <div class="card-body text-center">
                            <h5 class="card-title">Purchase Orders (PO)</h5>
                            <a class="btn btn-primary" target="_self" href="{base_url()}InspectSQLCMD/setupPO" data-toggle="tooltip" data-placement="top" title="inspect PO scripts locations"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                            <a class="btn btn-success" target="_self" href="{base_url()}RunSQLCMD/setupPO" data-toggle="tooltip" data-placement="top" title="run setup PO"><i class="fas fa-play fa-sm fa-fw"></i></a>
                        </div>
                    </div>
                </div>
            <div>
        </div>
    </div>
</div>



<br/>
<br/>
<br/>
