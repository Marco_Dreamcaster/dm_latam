<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        </ul>
    </div>
</nav>

<h3>Quick inspect flat file output</h3>
<form action="{base_url()}{$action}" method="post" accept-charset="UTF-8">
    <div class="form-group">
        <label for="flatfileLine">Flatfile input line</label>
        <textarea class="form-control" id="flatfileLine" name="flatfileLine" rows="4" value="">{$originalLine}</textarea>
    </div>
    <div class="form-group row">
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Explode</button>
        </div>
    </div>
</form>


<h3>
    Exploded fields : {sizeof($values)}
    {if $missingFields}
        <small class="text-danger">(some fields are missing)</small>
    {else}
        <small class="text-success">(which seems ok)</small>
    {/if}
</h3>
<table class="table table-hover table-sm">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Key</th>
        <th scope="col">Value</th>
        <th scope="col">Value Length</th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$values key=k item=v name=foo}
        <tr>
            <th scope="row">{$smarty.foreach.foo.index}</th>
            <td>{$k}</td>
            <td>{if $v=='(not present)'}<small class="text-danger">not present</small>{elseif $v=='(empty)'}<small class="text-warning">empty</small>{else}{$v}{/if}</td>
            <td>{if $v=='(not present)'}<small class="text-danger">-</small>{elseif $v=='(empty)'}<small class="text-warning">empty</small>{else}{strlen($v)}{/if}</td>
        </tr>
    {/foreach}

    </tbody>
</table>

<br/>
<br/>
<br/>