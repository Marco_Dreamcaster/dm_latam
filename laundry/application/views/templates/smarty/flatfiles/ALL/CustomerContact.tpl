{foreach from=$VOs item=vo}
    {if (($vo.emailAddress!=null)&&(!strstr($vo.emailAddress,';')))}
        {$vo.origSystemCustomerRef}|
        {$vo.origSystemAdressReference}|
        {$vo.origSystemContactRef}|
        {$vo.origSystemContactRef}-E1|
        {$vo.contactFirstName}|
        {$vo.contactLastName}|
        EMAIL|
        |
        |
        |
        |
        {$vo.emailAddress}|
        {$vo.contactAttribute1}|
        {$vo.orgName}
        {$rn}
    {elseif (($vo.emailAddress!=null)&&(strstr($vo.emailAddress,';')))}
        {assign var=addresses value=";"|explode:$vo.emailAddress}
        {foreach from=$addresses item=address name=foo2}
            {if (isset($address)&& strlen(trim($address))!=0)}
                {$vo.origSystemCustomerRef}|
                {$vo.origSystemAdressReference}|
                {$vo.origSystemContactRef}|
                {$vo.origSystemContactRef}-E{$smarty.foreach.foo2.index+1}|
                {$vo.contactFirstName}|
                {$vo.contactLastName}|
                EMAIL|
                |
                |
                |
                |
                {$address|trim}|
                {$vo.contactAttribute1}|
                {$vo.orgName}
            {$rn}
            {/if}
        {/foreach}
    {/if}

    {if $vo.telephone!=null}
        {if !strstr($vo.telephone,' / ')}
            {$vo.origSystemCustomerRef}|
            {$vo.origSystemAdressReference}|
            {$vo.origSystemContactRef}|
            {$vo.origSystemContactRef}-T1|
            {$vo.contactFirstName}|
            {$vo.contactLastName}|
            PHONE|
            {$vo.phoneCountryCode}|
            {$vo.telephoneAreaCode}|
            {$vo.telephone}|
            {$vo.telephoneExtension}|
            |
            {$vo.contactAttribute1}|
            {$vo.orgName}
            {$rn}
        {else}
            {assign var=phones value=" / "|explode:$vo.telephone}
            {foreach from=$phones item=phone name=foo}
                {$vo.origSystemCustomerRef}|
                {$vo.origSystemAdressReference}|
                {$vo.origSystemContactRef}|
                {$vo.origSystemContactRef}-T{$smarty.foreach.foo.index+1}|
                {$vo.contactFirstName}|
                {$vo.contactLastName}|
                PHONE|
                {$vo.phoneCountryCode}|
                {$vo.telephoneAreaCode}|
                {$phone}|
                {$vo.telephoneExtension}|
                |
                {$vo.contactAttribute1}|
                {$vo.orgName}
                {$rn}
            {/foreach}
        {/if}
    {/if}


{/foreach}