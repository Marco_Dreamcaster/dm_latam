{foreach from=$VOs item=vo}
    {$vo.customerName|trim}|
    {$vo.origSystemPartyRef}|
    {$vo.origSystemCustomerRef}|
    {$vo.customerType}|
    {$vo.customerProfile}|
    {$vo.paymentMethodName}|
    {if ($vo.primaryFlag)}Y{else}N{/if}|
    {if ($vo.globalFlag)}Y{else}N{/if}|
    {$vo.globalAttribute9}|
    {$vo.globalAttribute10}|
    {$vo.globalAttribute12}|
    {$vo.taxPayerId}|
    {$vo.fiscalClassCategory}|
    {$vo.fiscalClassCode}|
    {if isset($vo.fiscalClassStartDate)}{$vo.fiscalClassStartDate|date_format:'%d-%m-%Y'}{/if}|
    {$vo.CODocumentType}|
    {$vo.COTaxPayerId}|{$vo.nature}|
    {$vo.regimen}|
    {if ($vo.specialCustomer)}Y{else}N{/if}|
    {$vo.orgName}|
    {$vo.customerClassCode}|
    {$vo.paymentTermName}|
    {$vo.orderNumberGt}|
    {$vo.registerNumberGt}|
    {$vo.documentTypeSv}|
    {$vo.contributorRegistrationSv}|
    {$vo.personCategoryPE}|
    {$vo.personTypePE}|
    {$vo.documentTypePE}|
    {$vo.documentNumberPE}|
    {$vo.lastNamePE}|
    {$vo.secondLastNamePE}|
    {$vo.firstNamePE}|
    {$vo.middleNamePE}|
    {$vo.originAll}|
    {$vo.natureAll}|
    {$vo.documentTypeUY}|
    {$vo.perceptionAgentPE}
    {$rn}
{/foreach}