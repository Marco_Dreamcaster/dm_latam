{foreach from=$VOs item=vo}
    {$vo.vendorName|trim}|
    {$vo.segment1|trim}|
    {$vo.vendorNameAlt|trim}|
    {$vo.vendorTypeLookupCode|trim}|
    {$vo.vatRegistrationNum|trim}|
    {$vo.num1099|replace:'-':''|replace:'/':''|replace:'.':''|trim}|
    {if ($vo.federalReportableFlag)}Y{else}N{/if}|
    {$vo.type1099|trim}|
    {if ($vo.stateReportableFlag)}Y{else}N{/if}|
    {if ($vo.allowAwtFlag)}Y{else}N{/if}|
    {$vo.matchOptionLevel|trim}|
    {$vo.paymentMethodLookupCode|trim}|
    {if ($vo.globalFlag)}Y{else}N{/if}|
    {if ($vo.sensitiveVendor)}Y{else}N{/if}|
    {$vo.globalAttribute1|trim}|
    {$vo.globalAttribute8|trim}|
    {$vo.globalAttribute9|trim}|
    {$vo.globalAttribute10|trim}|
    {$vo.globalAttribute12|trim}|
    {$vo.globalAttribute15|trim}|
    {$vo.globalAttribute16|trim}|
    {if ($vo.country=='AR'||$vo.country=='CL')||$vo.country=='UY'}TKE LA DYNAMICS{else}{$vo.attributeCategory|trim}{/if}|
    {$vo.attribute1|trim}|
    {$vo.attribute2|trim}|
    {$vo.attribute3|trim}|
    {$vo.attribute4|trim}|
    {$vo.attribute5|trim}|
    {$vo.attribute6|trim}|
    {$vo.attribute7|trim}|
    {$vo.attribute8|trim}|
    {$vo.attribute9|trim}|
    {$vo.attribute10|trim}|
    {$vo.paExtAttribute1|trim}|
    {$vo.coExtAttribute1|trim}|
    {$vo.coExtAttribute2|trim}|
    {$vo.coExtAttribute3|trim}|
    {$vo.coExtAttribute4|trim}|
    {$vo.coExtAttribute5|trim}|
    01-01-2018|
    31-12-2049|
    {$vo.coExtAttribute8|trim}|
    {$vo.coExtAttribute9|trim}|
    {$vo.orgName|trim}|
    {$vo.supplierTypeNi|trim}|
    {$vo.orderNumberGt|trim}|
    {$vo.registerNumberGt|trim}|
    {$vo.supplierTypeSv|trim}|
    {$vo.documentTypeSv|trim}|
    {$vo.contributorRegistrationSv|trim}|
    {$vo.legalRepresentativeSv|trim}|
    {$vo.personCategoryPE|trim}|
    {$vo.personTypePE|trim}|
    {$vo.documentTypePE|trim}|
    {$vo.documentNumberPE|trim}|
    {$vo.lasNamePE|trim}|
    {$vo.secondLasNamePE|trim}|
    {$vo.firstNamePE|trim}|
    {$vo.middleNamePE|trim}|
    {$vo.supplierConditionPE|trim}|
    {$vo.withholdingSupplierPE|trim}|
    {$vo.countryCodePE|trim}|
    {$vo.serviceLenderPE|trim}|
    {$vo.conventionPE|trim}|
    {$vo.notResidentDocumentTypePE|trim}|
    {$vo.agreementAvoidDoubleTaxPE|trim}|
    {$vo.contributorTypeUY|trim}
    {$rn}
{/foreach}


