{foreach from=$VOs item=vo}
    {$vo.customerName}|
    {$vo.origSystemPartyRef}|
    {$vo.origSystemCustomerRef}|
    {$vo.customerType}|
    {$vo.customerProfile}|
    {$vo.paymentMethodName}|
    {if ($vo.primaryFlag)}Y{else}N{/if}|
    {if ($vo.globalFlag)}Y{else}N{/if}|
    {$vo.globalAttribute9}|
    {$vo.globalAttribute10}|
    {$vo.globalAttribute12}|
    {$vo.taxPayerId}|
    |
    |
    {if isset($vo.fiscalClassStartDate)}{$vo.fiscalClassStartDate|date_format:'%d-%m-%Y'}{/if}|
    {$vo.CODocumentType}|
    {$vo.COTaxPayerId}|
    {$vo.nature}|
    {$vo.regimen}|
    {if ($vo.specialCustomer)}Y{else}N{/if}|
    {$vo.orgName}|
    {$vo.classCode}|
    {$vo.paymentTermName}
    {$rn}
{/foreach}