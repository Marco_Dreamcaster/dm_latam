{foreach from=$VOs item=vo}
    {trim($vo.vendorName)}|
    {trim($vo.segment1)}|
    {trim($vo.vendorNameAlt)}|
    {trim($vo.vendorTypeLookupCode)}|
    {trim($vo.vatRegistrationNum)}|
    |
    {if ($vo.federalReportableFlag)}Y{else}N{/if}|
    {trim($vo.type1099)}|
    {if ($vo.stateReportableFlag)}Y{else}N{/if}|
    {if ($vo.allowAwtFlag)}Y{else}N{/if}|
    {trim($vo.matchOptionLevel)}|
    {trim($vo.paymentMethodLookupCode)}|
    {if ($vo.globalFlag)}Y{else}N{/if}|
    {if ($vo.sensitiveVendor)}Y{else}N{/if}|
    {trim($vo.globalAttribute1)}|
    {trim($vo.globalAttribute8)}|
    {trim($vo.globalAttribute9)}|
    {trim($vo.globalAttribute10)}|
    {trim($vo.globalAttribute12)}|
    {trim($vo.globalAttribute15)}|
    {trim($vo.globalAttribute16)}|
    {trim($vo.attributeCategory)}|
    {trim($vo.attribute1)}|
    {trim($vo.attribute2)}|
    {trim($vo.attribute3)}|
    {trim($vo.attribute4)}|
    {trim($vo.attribute5)}|
    {trim($vo.attribute6)}|
    {trim($vo.attribute7)}|
    {trim($vo.attribute8)}|
    {trim($vo.attribute9)}|
    {trim($vo.attribute10)}|
    {trim($vo.paExtAttribute1)}|
    {trim($vo.coExtAttribute1)}|
    {trim($vo.coExtAttribute2)}|
    {trim($vo.coExtAttribute3)}|
    {trim($vo.coExtAttribute4)}|
    {trim($vo.coExtAttribute5)}|
    {$vo.coExtAttribute6|date_format:'%d-%m-%Y'}|
    {$vo.coExtAttribute7|date_format:'%d-%m-%Y'}|
    {trim($vo.vatRegistrationNum)}|
    {trim($vo.coExtAttribute9)}|
    {trim($vo.orgName)}
    {$rn}
{/foreach}