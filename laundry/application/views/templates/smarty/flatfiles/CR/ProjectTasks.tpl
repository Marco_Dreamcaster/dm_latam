{foreach from=$VOs item=vo}
    {$vo.projectName}|
    {$vo.taskNumber|trim}|
    {$vo.taskName|trim|truncate:20}|
    |
    {$vo.description}|
    {$vo.startDate|date_format:"%d-%m-%Y"}|
    {$vo.finishDate|date_format:"%d-%m-%Y"}|
    {$vo.serviceType}|
    N|
    {$vo.attribute1}|
    {$vo.attribute2}|
    {if ($vo.attribute3!='*NULL*')}{$vo.attribute3}{/if}|
    |
    31-12-2049|
    {if $vo.attribute6==0}N{else}Y{/if}|
    {if ($vo.attribute7!='*NULL*')}{$vo.attribute7}{/if}|
    |
    {$vo.noOfFloors}|
    {number_format($vo.speed,2,',','')}m/s|
    {$vo.capacity}kg|
    {$vo.orgName}{$rn}

    {$vo.projectName}|
    {str_replace("-", "_", $vo.taskNumber)|trim}.01|
    {str_replace("-", "_", $vo.taskName)|trim|truncate:20}|
    {str_replace("-", "_", $vo.taskNumber)|trim}|
    {str_replace("-", "_", $vo.taskName)|trim}.01.LABOR|
    {$vo.startDate|date_format:"%d-%m-%Y"}|
    {$vo.finishDate|date_format:"%d-%m-%Y"}|
    CHANGE ME|
    N|
    |||||||||||{$vo.orgName}{$rn}

    {$vo.projectName}|
    {str_replace("-", "_", $vo.taskNumber)|trim}.02|
    {str_replace("-", "_", $vo.taskName)|trim|truncate:20}|
    {str_replace("-", "_", $vo.taskNumber)|trim}|
    {str_replace("-", "_", $vo.taskName)|trim}.02.MATERIAL|
    {$vo.startDate|date_format:"%d-%m-%Y"}|
    {$vo.finishDate|date_format:"%d-%m-%Y"}|
    CHANGE ME|
    Y|
    |||||||||||{$vo.orgName}{$rn}

    {$vo.projectName}|
    {str_replace("-", "_", $vo.taskNumber)|trim}.03|
    {str_replace("-", "_", $vo.taskName)|trim|truncate:20}|
    {str_replace("-", "_", $vo.taskNumber)|trim}|
    {str_replace("-", "_", $vo.taskName)|trim}.03.MISCELLANEOUS|
    {$vo.startDate|date_format:"%d-%m-%Y"}|
    {$vo.finishDate|date_format:"%d-%m-%Y"}|
    CHANGE ME|
    Y|
    |||||||||||{$vo.orgName}{$rn}

    {$vo.projectName}|
    {str_replace("-", "_", $vo.taskNumber)|trim}.01.01|
    {str_replace("-", "_", $vo.taskName)|trim|truncate:20}|
    {str_replace("-", "_", $vo.taskNumber)|trim}.01|
    {str_replace("-", "_", $vo.taskName)|trim}.01.01.PRE-INSTALLATION|
    {$vo.startDate|date_format:"%d-%m-%Y"}|
    {$vo.finishDate|date_format:"%d-%m-%Y"}|
    CHANGE ME|
    Y|
    |||||||||||{$vo.orgName}{$rn}

    {$vo.projectName}|
    {str_replace("-", "_", $vo.taskNumber)|trim}.01.02|
    {str_replace("-", "_", $vo.taskName)|trim|truncate:20}|
    {str_replace("-", "_", $vo.taskNumber)|trim}.01|
    {str_replace("-", "_", $vo.taskName)|trim}.01.02.INSTALLATION|
    {$vo.startDate|date_format:"%d-%m-%Y"}|
    {$vo.finishDate|date_format:"%d-%m-%Y"}|
    CHANGE ME|
    Y|
    |||||||||||{$vo.orgName}{$rn}

    {$vo.projectName}|
    {str_replace("-", "_", $vo.taskNumber)|trim}.01.03|
    {str_replace("-", "_", $vo.taskName)|trim|truncate:20}|
    {str_replace("-", "_", $vo.taskNumber)|trim}.01|
    {str_replace("-", "_", $vo.taskName)|trim}.01.03.ADDITIONAL COST|
    {$vo.startDate|date_format:"%d-%m-%Y"}|
    {$vo.finishDate|date_format:"%d-%m-%Y"}|
    CHANGE ME|
    Y|
    |||||||||||{$vo.orgName}{$rn}

{/foreach}