{foreach from=$VOs item=vo}
    {if ($vo.siteUseCode=='ALL')}
        {$vo.customerName|trim}|
        {$vo.origSystemCustomerRef}|
        {$vo.origSystemAdressReference}|
        BILL_TO|
        {if ($vo.primarySiteUseFlag)}Y{else}N{/if}|
        {$vo.location}|
        |
        {$vo.addressLine1}|
        {$vo.addressLine2}|
        {$vo.addressLine3}|
        {$vo.addressLine4}|
        {$vo.addressLinesPhonetic}|
        {$vo.city}|
        {str_replace('_',' ',$vo.province)}|
        {str_replace('_',' ',$vo.state)}|
        {$vo.county}|
        {$vo.postalCode}|
        {$vo.addressCountry}|
        {$vo.recevablesAccount}|
        {$vo.revenueAccount}|
        {$vo.taxAccount}|
        {$vo.freightAccount}|
        {$vo.clearingAccount}|
        {$vo.unbilledReceivablesAccount}|
        {$vo.unearnedRevenueAccount}|
        {$vo.paymentMethodName}|
        {if ($vo.primaryFlag)}Y{else}N{/if}|
        {$vo.customerProfile}|
        {$vo.siteUseTaxReference}|
        {$vo.timeZone}|
        {$vo.language}|
        |
        |
        |
        |
        {$vo.economicActivity}|
        {$vo.orgName}{$rn}
        {$vo.customerName|trim}|
        {$vo.origSystemCustomerRef}|
        {$vo.origSystemAdressReference}|
        SHIP_TO|
        {if ($vo.primarySiteUseFlag)}Y{else}N{/if}|
        {$vo.location}|
        {$vo.origSystemAdressReference}|
        {$vo.addressLine1}|
        {$vo.addressLine2}|
        {$vo.addressLine3}|
        {$vo.addressLine4}|
        {$vo.addressLinesPhonetic}|
        {$vo.city}|
        {str_replace('_',' ',$vo.province)}|
        {str_replace('_',' ',$vo.state)}|
        {$vo.county}|
        {$vo.postalCode}|
        {$vo.addressCountry}|
        {$vo.recevablesAccount}|
        {$vo.revenueAccount}|
        {$vo.taxAccount}|
        {$vo.freightAccount}|
        {$vo.clearingAccount}|
        {$vo.unbilledReceivablesAccount}|
        {$vo.unearnedRevenueAccount}|
        {$vo.paymentMethodName}|
        {if ($vo.primaryFlag)}Y{else}N{/if}|
        {$vo.customerProfile}|
        {$vo.siteUseTaxReference}|
        {$vo.timeZone}|
        {$vo.language}|
        |
        |
        |
        |
        {$vo.economicActivity}|
        {$vo.orgName}{$rn}
        {$vo.customerName|trim}|
        {$vo.origSystemCustomerRef}|
        {$vo.origSystemAdressReference}|
        DUN|
        {if ($vo.primarySiteUseFlag)}Y{else}N{/if}|
        {$vo.location}|
        {$vo.origSystemAdressReference}|
        {$vo.addressLine1}|
        {$vo.addressLine2}|
        {$vo.addressLine3}|
        {$vo.addressLine4}|
        {$vo.addressLinesPhonetic}|
        {$vo.city}|
        {str_replace('_',' ',$vo.province)}|
        {str_replace('_',' ',$vo.state)}|
        {$vo.county}|
        {$vo.postalCode}|
        {$vo.addressCountry}|
        {$vo.recevablesAccount}|
        {$vo.revenueAccount}|
        {$vo.taxAccount}|
        {$vo.freightAccount}|
        {$vo.clearingAccount}|
        {$vo.unbilledReceivablesAccount}|
        {$vo.unearnedRevenueAccount}|
        {$vo.paymentMethodName}|
        {if ($vo.primaryFlag)}Y{else}N{/if}|
        {$vo.customerProfile}|
        {$vo.siteUseTaxReference}|
        {$vo.timeZone}|
        {$vo.language}|
        |
        |
        |
        |
        {$vo.economicActivity}|
        {$vo.orgName}{$rn}

    {else}
        {$vo.customerName|trim}|
        {$vo.origSystemCustomerRef}|
        {$vo.origSystemAdressReference}|
        {$vo.siteUseCode}|
        {if ($vo.primarySiteUseFlag)}Y{else}N{/if}|
        {$vo.location}|
        |
        {$vo.addressLine1}|
        {$vo.addressLine2}|
        {$vo.addressLine3}|
        {$vo.addressLine4}|
        {$vo.addressLinesPhonetic}|
        {$vo.city}|
        {str_replace('_',' ',$vo.province)}|
        {str_replace('_',' ',$vo.state)}|
        {$vo.county}|
        {$vo.postalCode}|
        {$vo.addressCountry}|
        {$vo.recevablesAccount}|
        {$vo.revenueAccount}|
        {$vo.taxAccount}|
        {$vo.freightAccount}|
        {$vo.clearingAccount}|
        {$vo.unbilledReceivablesAccount}|
        {$vo.unearnedRevenueAccount}|
        {$vo.paymentMethodName}|
        {if ($vo.primaryFlag)}Y{else}N{/if}|
        {$vo.customerProfile}|
        {$vo.siteUseTaxReference}|
        {$vo.timeZone}|
        {$vo.language}|
        |
        |
        |
        |
        {$vo.economicActivity}|
        {$vo.orgName}{$rn}

    {/if}
{/foreach}