{foreach from=$VOs item=vo}
    {if $vo.emailAddress!=null}
        {if !strstr($vo.emailAddress,';')}
            MULTIPLE
        {/if}
        {$vo.origSystemCustomerRef}|
        {$vo.origSystemAdressReference}|
        {$vo.origSystemContactRef}|
        {$vo.origSystemTelephoneRef}-E|
        {$vo.contactFirstName}|
        {$vo.contactLastName}|
        EMAIL|
        |
        |
        |
        |
        {$vo.emailAddress}|
        {$vo.contactAttribute1}|
        {$vo.orgName}
        {$rn}
    {/if}
    {if $vo.telephone!=null}
        {if !strstr($vo.telephone,' / ')}
            {$vo.origSystemCustomerRef}|
            {$vo.origSystemAdressReference}|
            {$vo.origSystemContactRef}|
            {$vo.origSystemTelephoneRef}-T1|
            {$vo.contactFirstName}|
            {$vo.contactLastName}|
            PHONE|
            {$vo.phoneCountryCode}|
            {$vo.telephoneAreaCode}|
            {$vo.telephone}|
            {$vo.telephoneExtension}|
            |
            {$vo.contactAttribute1}|
            {$vo.orgName}
            {$rn}
        {else}
            {assign var=phones value=" / "|explode:$vo.telephone}
            {foreach from=$phones item=phone name=foo}
                {$vo.origSystemCustomerRef}|
                {$vo.origSystemAdressReference}|
                {$vo.origSystemContactRef}|
                {$vo.origSystemTelephoneRef}-T{$smarty.foreach.foo.index+1}|
                {$vo.contactFirstName}|
                {$vo.contactLastName}|
                PHONE|
                {$vo.phoneCountryCode}|
                {$vo.telephoneAreaCode}|
                {$phone}|
                {$vo.telephoneExtension}|
                |
                {$vo.contactAttribute1}|
                {$vo.orgName}
                {$rn}
            {/foreach}
        {/if}
    {/if}

{/foreach}