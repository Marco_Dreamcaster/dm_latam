{foreach from=$VOs item=vo}
    {$vo.vendorNumber}|
    {$vo.vendorSiteCode}|
    {$vo.vendorSiteCodeAlt}|
    {$vo.addressLine1}|
    {$vo.addressLine2}|
    {$vo.addressLine3}|
    {$vo.addressLine4}|
    {$vo.addressLineAlt}|
    {str_replace('_',' ',trim($vo.city))}|
    {str_replace('_',' ',trim($vo.county))}|
    {str_replace('_',' ',trim($vo.state))}|
    {str_replace('_',' ',trim($vo.province))}|
    {$vo.zip}|
    {$vo.addressCountry}|
    {$vo.areaCode}|
    {$vo.phone}|
    {$vo.faxAreaCode}|
    {$vo.fax}|
    {$vo.paymentMethodLookupCode}|
    {$vo.vatCode}|
    {$vo.acctsPayAccount}|
    {$vo.prepayAccount}|
    {$vo.payGroupLookupCode}|
    {$vo.invoiceCurrencyCode}|
    {$vo.language}|
    {$vo.termsName}|
    {$vo.emailAddress}|
    {if ($vo.purchasingSiteFlag)}Y{else}N{/if}|
    {if ($vo.paySiteFlag)}Y{else}N{/if}|
    {$vo.vatRegistrationNum}|
    {$vo.shipToLocationCode}|
    {if ($vo.primaryPaySiteFlag)}Y{else}N{/if}|
    {if ($vo.gaplessInvNumFlag)}Y{else}N{/if}|
    {if ($vo.offsetTaxFlag)}Y{else}N{/if}|
    {if ($vo.alwaysTakeDiscFlag)}Y{else}N{/if}|
    {if ($vo.excludeFreightFromDiscount)}Y{else}N{/if}|
    {if ($vo.globalAttribute17)}Y{else}N{/if}|
    {if ($vo.globalAttribute18)}Y{else}N{/if}|
    {$vo.globalAttribute19}|
    {if $vo.globalAttribute20!=null}{$vo.globalAttribute20|
    date_format:'%d-%m-%Y'}{/if}|
    {$vo.attributeCategory}|
    {$vo.attribute1}|
    {$vo.attribute2}|
    {$vo.attribute3}|
    {$vo.attribute4}|
    {$vo.attribute5}|
    {$vo.attribute6}|
    {$vo.attribute7}|
    {$vo.attribute8}|
    {$vo.attribute9}|
    {$vo.attribute10}|
    {$vo.classificationTypeCode}|
    {$vo.classificationCode}|
    {$vo.classificationStartDate|
    date_format:'%d-%m-%Y'}|
    {$vo.coExtAttribute1}|
    {$vo.orgName}{$rn}
{/foreach}