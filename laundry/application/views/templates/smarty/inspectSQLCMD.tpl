<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><i class="fas fa-lg fa-bug fa-fw m-2 text-stage"></i>&nbsp;<span class="mt-2">{strtoupper($country)} {$title}</span>&nbsp;</h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Debug" data-toggle="tooltip" data-placement="top" title="debug and audit logs"><i class="fas fa-bug fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}IDL/groups" data-toggle="tooltip" data-placement="top" title="view all IDLs by country"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>

<div class="tab-content mt-2" id="pills-tabContent">
    <div class="tab-pane fade active show" id="pills-script" role="tabpanel" aria-labelledby="pills-script-tab">
        <table id="scriptsTable" class="display" style="width:100%">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">name</th>
                <th scope="col">location</th>
                <th scope="col">file</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$scriptLocations key=k item=i name=foo}
                <tr>
                    <td>{$smarty.foreach.foo.index + 1}</td>
                    <td>{$k}</td>
                    <td>{$i.location}</td>
                    <td><a href="{base_url()}scripts/{$i.location}/{$i.script}">{$i.script}</a></td>
                    <td>
                        <a class="btn btn-success" target="_self" href="{base_url()}RunSQLCMD/{$group}/{$k}" data-toggle="tooltip" data-placement="top" title="execute {$group} {$k}"><i class="fas fa-play fa-sm fa-fw"></i></a>
                    </td>
                </tr>
            {/foreach}
            </tbody>
            <tfoot>
            <tr>
                <th scope="col">#</th>
                <th scope="col">name</th>
                <th scope="col">location</th>
                <th scope="col">file</th>
                <th scope="col"></th>
            </tr>
            </tfoot>
        </table>
        <script>
            $(document).ready(function() {
                var table = $('#scriptsTable').DataTable({
                        "lengthMenu": [[50, 100, -1], [50, 100, "All"]]
                });
                table.order( [ 0, 'asc' ] ).draw();
            } );
        </script>
    </div>
</div>



<br/>
<br/>
<br/>
