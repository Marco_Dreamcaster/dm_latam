<script src="{base_url()}js/chartJS/Chart.bundle.js" charset="UTF-8"></script>
<script src="{base_url()}js/chartJS/utils.js" charset="UTF-8"></script>
<div class="alert-warning p-3">
    <div class="row">
        <div class="col-10">
            <p class="p-2">
                GoLive is on.  SIGLA is not quite so dead yet, ORACLE is coming alive.
            </p>
            <p class="p-2">
            <span class="flag-icon flag-icon-pa" title="PA"></span>
                (wave0) <i class="fa fas fa-check"></i>
            &nbsp;
            <span class="flag-icon flag-icon-co" title="CO"></span>
                (wave1) <i class="fa fas fa-check"></i>
            &nbsp;
            &nbsp;
            <span class="flag-icon flag-icon-mx" title="MX"></span>
            <span class="flag-icon flag-icon-sv" title="SV"></span>
            <span class="flag-icon flag-icon-gt" title="GT"></span>
            <span class="flag-icon flag-icon-cr" title="CR"></span>
            <span class="flag-icon flag-icon-ni" title="NI"></span>
            <span class="flag-icon flag-icon-hn" title="HN"></span>
                (wave2) <i class="fa fas fa-check"></i>
            &nbsp;
            &nbsp;
            <span class="flag-icon flag-icon-pe" title="PE"></span>
            <span class="flag-icon flag-icon-cl" title="CL"></span>
            <span class="flag-icon flag-icon-ar" title="AR"></span>
            <span class="flag-icon flag-icon-uy" title="UY"></span>
            <span class="flag-icon flag-icon-py" title="PY"></span>
                (wave3) <i class="fa fas fa-check"></i>
            </p>
        </div>
        <div class="col-2">
        </div>
    </div>
</div>

<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-4 ml-sm-auto col-lg-12">
    <div class="d-flex justify-content-center col-md-4 ml-sm-auto col-lg-12">
        <form class="form-inline">
            <div class="m-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary p-1" type="submit" target="_blank" title="go to country">&nbsp;Go to &nbsp;</button>
                    </div>
                    <input class="custom-select-country" type="text" id="kpisForCountry" name="kpisForCountry" readonly="true" />
                    <input type="hidden" id="kpisForCountry_code" name="kpisForCountry_code" />
                </div>
            </div>
            <div class="m-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Snapshot Date</div>
                    </div>
                    <input class="p-2" type="text" id="snapshotDate" name="snapshotDate" readonly="true" value="{$snapshotDate}"/>
                </div>
            </div>
            <div class="m-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">Cutoff Date</div>
                    </div>
                    <input class="p-2" type="text" id="cutoffDate" name="cutoffDate" readonly="true" value="{$cutoffDate}"/>
                </div>
            </div>
        </form>
    </div>
</nav>
<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-4 ml-sm-auto col-lg-12">
    <div class="d-flex justify-content-center col-md-4 ml-sm-auto col-lg-12">
        <ul class="nav nav-pills mt-2" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active show" id="pills-audit-tab" data-toggle="pill" href="#pills-inventory" role="tab" aria-controls="pills-inventory" aria-selected="true">Inventory</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-quickEplode-tab" data-toggle="pill" href="#pills-customers" role="tab" aria-controls="pills-customers" aria-selected="false">Customers</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-setup-tab" data-toggle="pill" href="#pills-vendors" role="tab" aria-controls="pills-vendors" aria-selected="false">Vendors</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-setup-tab" data-toggle="pill" href="#pills-projects" role="tab" aria-controls="pills-projects" aria-selected="false">Projects</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-setup-tab" data-toggle="pill" href="#pills-services" role="tab" aria-controls="pills-services" aria-selected="false">Services</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-setup-tab" data-toggle="pill" href="#pills-repairs" role="tab" aria-controls="pills-repairs" aria-selected="false">Repairs</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-setup-tab" data-toggle="pill" href="#pills-master" role="tab" aria-controls="pills-master" aria-selected="false">Master Data Load</a>
            </li>
        </ul>
    </div>
</nav>
<script>
    $(function() {
        $("#kpisForCountry").countrySelect({
            defaultCountry: "{$sessionCountry}",
            onlyCountries: ['co','mx','sv','ni','hn','gt','cr','cl','ar','pe','uy','py','pa']
        });

        $('#kpisForCountry').on('change', function() {
            console.log($('#kpisForCountry_code').val());
            location.href = "{base_url()}KPI/"+$('#kpisForCountry_code').val().toUpperCase();
        });

    });

</script>

<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade active show" id="pills-inventory" role="tabpanel" aria-labelledby="pills-summary-tab">
        <h5 class="nav p-2">
            <span class="flag-icon flag-icon-{strtolower($country)}"></span>&nbsp;<span class="mt-2">{strtoupper($country)} Inventory</span>&nbsp;
            &nbsp;
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Inventory/cleansed/{$country}" data-toggle="tooltip" data-placement="top" title="cleansed {$country} Inventory"><i class="fas fa-industry fa-sm fa-fw"></i></a>
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Inventory/dirty/{$country}" data-toggle="tooltip" data-placement="top" title="dirty {$country} Inventory"><i class="fas fa-trash-alt fa-sm fa-fw"></i></a>
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}KPI/Inventory/{$country}" data-toggle="tooltip" data-placement="top" title="view ALL stats for {$country} Inventory"><i class="fas fa-chart-line fa-sm fa-fw"></i></a>
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Setup/{$country}" data-toggle="tooltip" data-placement="top" title="setup for {$country} Inventory"><i class="fas fa-sliders-h fa-sm fa-fw"></i></a>
            <a class="btn btn-danger m-1" target="_self" href="{base_url()}RefreshFF/InventoryDiagnostics/{$country}" data-toggle="tooltip" data-placement="top" title="run Inventory Diagnostics for {$country}"><i class="fas fa-ambulance fa-sm fa-fw"></i></a>
            <a class="btn btn-danger m-1" target="_self" href="{base_url()}RefreshFF/Status/{$country}" data-toggle="tooltip" data-placement="top" title="run Status for {$country}"><i class="fas fa-thermometer-half fa-sm fa-fw"></i></a>
        </h5>

        <div class="row p-2">
            <div class="col-sm-6">
                <div class="card text-pre-stage">
                    <div class="card-body text-pre-stage text-center">
                        <h6 class="card-title">Amount on SIGLA (A)</h6>
                        <h4 class="card-text">{$inventory['siglaAmount']['displayAmount']}&nbsp;{$inventory['localCurrency']}</h4>
                        <h6 class="text-muted">${$inventory['siglaAmount']['dollarAmount']} USD</h6>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card text-success">
                    <div class="card-body text-success text-center">
                        <h6 class="card-title">Amount on ORACLE (B)</h6>
                        <h4 class="card-text">{$inventory['oracleAmount']['displayAmount']}&nbsp;{$inventory['localCurrency']}</h4>
                        <h6 class="text-muted">${$inventory['oracleAmount']['dollarAmount']} USD</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row p-2">
            <div class="col-sm-12">
                <div class="card text-danger">
                    <div class="card-body text-danger text-center">
                        <h6 class="card-title">SIGLA to ORACLE (A - B)</h6>
                        <h4 class="card-text">{$inventory['siglaToOracleDifference']['displayAmount']}&nbsp;{$inventory['localCurrency']}</h4>
                        <h6 class="text-muted">${$inventory['siglaToOracleDifference']['dollarAmount']} USD</h6>
                        <h6 class="text-muted">{$inventory['siglaToOracleDifference']['percentage']} %</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row p-2">
            <div class="col-sm-12">
                <div class="card text-danger">
                    <div class="card-body text-success text-center">
                        <h6 class="card-title">GL Exchange Rate</h6>
                        <h6 class="text-muted">$1 USD = {$inventory['dollarExchangeRate']} &nbsp;{$inventory['localCurrency']}</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row p-2">
            <div class="col-sm-6">
                <div class="card text-pre-stage">
                    <div class="card-body text-pre-stage text-center">
                        <h6 class="card-title">Dirty (preSTG)</h6>
                        <p class="card-subtitle mb-2">total records on SIGLA</p>
                        <h4 class="card-text">{$inventory['totalPreSTG']}</h4>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="card text-pre-stage">
                    <div class="card-body text-success text-center">
                        <h6 class="card-title">Migrated (STG)</h6>
                        <p class="card-subtitle mb-2">on flat file (cleansed and validated)</p>
                        <h4 class="card-text">{$inventory['totalValidated']}</h4>
                    </div>
                </div>
            </div>
        </div>


        <div class="row p-1">
            <div class="col-sm-12">
                <div class="card text-pre-stage">
                    <div class="card-body text-center">
                        <h6 class="card-title">Recommended (RFM)</h6>
                        <p class="card-subtitle mb-2">on pre-STG with ranking >= 5</p>
                        <h4 class="card-text">{$inventory['recommendedDirty']}</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="row p-1">
            <div class="col-sm-6">
                <div class="card text-danger">
                    <div class="card-body text-center">
                        <h6 class="card-title">Dirty and Recommended (dRFM)</h6>
                        <p class="card-subtitle mb-2">recommended, but only on preSTG</p>
                        <h4 class="card-text">{$inventory['dirtyRecommended']}</h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card text-stage">
                    <div class="card-body text-center">
                        <h6 class="card-title">Staged and Recommended (cRFM)</h6>
                        <p class="card-subtitle mb-2">on STG and recommended</p>
                        <h4 class="card-text">{$inventory['cleansedRecommended']}</h4>
                    </div>
                </div>
            </div>
        </div>

        <br/>
        <br/>
        <br/>
    </div>

    <div class="tab-pane fade" id="pills-customers" role="tabpanel" aria-labelledby="pills-summary-tab">
        <h5 class="nav p-2">
            <span class="flag-icon flag-icon-{strtolower($country)}"></span>&nbsp;<span class="mt-2">{strtoupper($country)} Customers</span>&nbsp;
            &nbsp;
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Customers/cleansed/{$country}" data-toggle="tooltip" data-placement="top" title="cleansed {$country} Customers"><i class="fas fa-industry fa-sm fa-fw"></i></a>
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Customers/dirty/{$country}" data-toggle="tooltip" data-placement="top" title="dirty {$country} Customers"><i class="fas fa-trash-alt fa-sm fa-fw"></i></a>
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}KPI/Customers/{$country}" data-toggle="tooltip" data-placement="top" title="view ALL stats for {$country} Customers"><i class="fas fa-chart-line fa-sm fa-fw"></i></a>
        </h5>

        <div class="row p-2">
            <div class="col-sm-6">
                <div class="card text-pre-stage">
                    <div class="card-body text-pre-stage text-center">
                        <h6 class="card-title">Dirty (preSTG)</h6>
                        <p class="card-subtitle mb-2">total records on SIGLA</p>
                        <h4 class="card-text">{$customers['totalPreSTG']}</h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card text-pre-stage">
                    <div class="card-body text-success text-center">
                        <h6 class="card-title">Migrated (STG)</h6>
                        <p class="card-subtitle mb-2">on flat file (cleansed and validated)</p>
                        <h4 class="card-text">{$customers['totalValidated']}</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="row p-1">
            <div class="col-sm-12">
                <div class="card text-pre-stage">
                    <div class="card-body text-center">
                        <h6 class="card-title">Recommended (RFM)</h6>
                        <p class="card-subtitle mb-2">on pre-STG with ranking >= 5</p>
                        <h4 class="card-text">{$customers['recommendedDirty']}</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="row p-1">
            <div class="col-sm-6">
                <div class="card text-danger">
                    <div class="card-body text-center">
                        <h6 class="card-title">Dirty and Recommended (dRFM)</h6>
                        <p class="card-subtitle mb-2">recommended, but only on preSTG</p>
                        <h4 class="card-text">{$customers['dirtyRecommended']}</h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card text-stage">
                    <div class="card-body text-center">
                        <h6 class="card-title">Staged and Recommended (cRFM)</h6>
                        <p class="card-subtitle mb-2">on STG and recommended</p>
                        <h4 class="card-text">{$customers['cleansedRecommended']}</h4>
                    </div>
                </div>
            </div>
        </div>

        <br/>
        <br/>
        <br/>
    </div>

    <div class="tab-pane fade" id="pills-vendors" role="tabpanel" aria-labelledby="pills-summary-tab">
        <h5 class="nav p-2">
            <span class="flag-icon flag-icon-{strtolower($country)}"></span>&nbsp;<span class="mt-2">{strtoupper($country)} Vendors</span>&nbsp;
            &nbsp;
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Vendors/cleansed/{$country}" data-toggle="tooltip" data-placement="top" title="cleansed {$country} Vendors"><i class="fas fa-industry fa-sm fa-fw"></i></a>
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Vendors/dirty/{$country}" data-toggle="tooltip" data-placement="top" title="dirty {$country} Vendors"><i class="fas fa-trash-alt fa-sm fa-fw"></i></a>
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}KPI/Vendors/{$country}" data-toggle="tooltip" data-placement="top" title="view ALL stats for {$country} Vendors"><i class="fas fa-chart-line fa-sm fa-fw"></i></a>
        </h5>

        <div class="row p-2">
            <div class="col-sm-6">
                <div class="card text-pre-stage">
                    <div class="card-body text-pre-stage text-center">
                        <h6 class="card-title">Dirty (preSTG)</h6>
                        <p class="card-subtitle mb-2">total records on SIGLA</p>
                        <h4 class="card-text">{$vendors['totalPreSTG']}</h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card text-pre-stage">
                    <div class="card-body text-success text-center">
                        <h6 class="card-title">Migrated (STG)</h6>
                        <p class="card-subtitle mb-2">on flat file (cleansed and validated)</p>
                        <h4 class="card-text">{$vendors['totalValidated']}</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="row p-1">
            <div class="col-sm-12">
                <div class="card text-pre-stage">
                    <div class="card-body text-center">
                        <h6 class="card-title">Recommended (RFM)</h6>
                        <p class="card-subtitle mb-2">on pre-STG with ranking >= 5</p>
                        <h4 class="card-text">{$vendors['recommendedDirty']}</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="row p-1">
            <div class="col-sm-6">
                <div class="card text-danger">
                    <div class="card-body text-center">
                        <h6 class="card-title">Dirty and Recommended (dRFM)</h6>
                        <p class="card-subtitle mb-2">recommended, but only on preSTG</p>
                        <h4 class="card-text">{$vendors['dirtyRecommended']}</h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card text-stage">
                    <div class="card-body text-center">
                        <h6 class="card-title">Staged and Recommended (cRFM)</h6>
                        <p class="card-subtitle mb-2">on STG and recommended</p>
                        <h4 class="card-text">{$vendors['cleansedRecommended']}</h4>
                    </div>
                </div>
            </div>
        </div>

        <br/>
        <br/>
        <br/>
    </div>

    <div class="tab-pane fade" id="pills-projects" role="tabpanel" aria-labelledby="pills-summary-tab">
        <h5 class="nav p-2">
            <span class="flag-icon flag-icon-{strtolower($country)}"></span>&nbsp;<span class="mt-2">{strtoupper($country)} Projects</span>&nbsp;
            &nbsp;
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Projects/cleansed/{$country}" data-toggle="tooltip" data-placement="top" title="cleansed {$country} Projects"><i class="fas fa-industry fa-sm fa-fw"></i></a>
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Projects/dirty/{$country}" data-toggle="tooltip" data-placement="top" title="dirty {$country} Projects"><i class="fas fa-trash-alt fa-sm fa-fw"></i></a>
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}KPI/Projects/{$country}" data-toggle="tooltip" data-placement="top" title="view ALL stats for {$country} Projects"><i class="fas fa-chart-line fa-sm fa-fw"></i></a>
        </h5>

        <div class="row p-2">
            <div class="col-sm-6">
                <div class="card text-pre-stage">
                    <div class="card-body text-pre-stage text-center">
                        <h6 class="card-title">Dirty (preSTG)</h6>
                        <p class="card-subtitle mb-2">total records on SIGLA</p>
                        <h4 class="card-text">{$projects['totalPreSTG']}</h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card text-pre-stage">
                    <div class="card-body text-success text-center">
                        <h6 class="card-title">Migrated (STG)</h6>
                        <p class="card-subtitle mb-2">on flat file (cleansed and validated)</p>
                        <h4 class="card-text">{$projects['totalValidated']}</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="row p-1">
            <div class="col-sm-12">
                <div class="card text-pre-stage">
                    <div class="card-body text-center">
                        <h6 class="card-title">Recommended (RFM)</h6>
                        <p class="card-subtitle mb-2">on pre-STG with ranking >= 5</p>
                        <h4 class="card-text">{$projects['recommendedDirty']}</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="row p-1">
            <div class="col-sm-6">
                <div class="card text-danger">
                    <div class="card-body text-center">
                        <h6 class="card-title">Dirty and Recommended (dRFM)</h6>
                        <p class="card-subtitle mb-2">recommended, but only on preSTG</p>
                        <h4 class="card-text">{$projects['dirtyRecommended']}</h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card text-stage">
                    <div class="card-body text-center">
                        <h6 class="card-title">Staged and Recommended (cRFM)</h6>
                        <p class="card-subtitle mb-2">on STG and recommended</p>
                        <h4 class="card-text">{$projects['cleansedRecommended']}</h4>
                    </div>
                </div>
            </div>
        </div>

        <br/>
        <br/>
        <br/>
    </div>

    <div class="tab-pane fade" id="pills-services" role="tabpanel" aria-labelledby="pills-summary-tab">
        <h5 class="nav p-2">
            <span class="flag-icon flag-icon-{strtolower($country)}"></span>&nbsp;<span class="mt-2">{strtoupper($country)} Services</span>&nbsp;
            &nbsp;
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Services/cleansed/{$country}" data-toggle="tooltip" data-placement="top" title="cleansed {$country} Services"><i class="fas fa-industry fa-sm fa-fw"></i></a>
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Services/dirty/{$country}" data-toggle="tooltip" data-placement="top" title="dirty {$country} Services"><i class="fas fa-trash-alt fa-sm fa-fw"></i></a>
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}KPI/Services/{$country}" data-toggle="tooltip" data-placement="top" title="view ALL stats for {$country} Services"><i class="fas fa-chart-line fa-sm fa-fw"></i></a>
        </h5>

        <div class="row p-2">
            <div class="col-sm-6">
                <div class="card text-pre-stage">
                    <div class="card-body text-pre-stage text-center">
                        <h6 class="card-title">Dirty (preSTG)</h6>
                        <p class="card-subtitle mb-2">total records on SIGLA</p>
                        <h4 class="card-text">{$services['totalPreSTG']}</h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card text-pre-stage">
                    <div class="card-body text-success text-center">
                        <h6 class="card-title">Migrated (STG)</h6>
                        <p class="card-subtitle mb-2">on flat file (cleansed and validated)</p>
                        <h4 class="card-text">{$services['totalValidated']}</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="row p-1">
            <div class="col-sm-12">
                <div class="card text-pre-stage">
                    <div class="card-body text-center">
                        <h6 class="card-title">Recommended (RFM)</h6>
                        <p class="card-subtitle mb-2">on pre-STG with ranking >= 5</p>
                        <h4 class="card-text">{$services['recommendedDirty']}</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="row p-1">
            <div class="col-sm-6">
                <div class="card text-danger">
                    <div class="card-body text-center">
                        <h6 class="card-title">Dirty and Recommended (dRFM)</h6>
                        <p class="card-subtitle mb-2">recommended, but only on preSTG</p>
                        <h4 class="card-text">{$services['dirtyRecommended']}</h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card text-stage">
                    <div class="card-body text-center">
                        <h6 class="card-title">Staged and Recommended (cRFM)</h6>
                        <p class="card-subtitle mb-2">on STG and recommended</p>
                        <h4 class="card-text">{$services['cleansedRecommended']}</h4>
                    </div>
                </div>
            </div>
        </div>

        <br/>
        <br/>
        <br/>
    </div>

    <div class="tab-pane fade" id="pills-repairs" role="tabpanel" aria-labelledby="pills-summary-tab">
        <h5 class="nav p-2">
            <span class="flag-icon flag-icon-{strtolower($country)}"></span>&nbsp;<span class="mt-2">{strtoupper($country)} Repairs</span>&nbsp;
            &nbsp;
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Repairs/cleansed/{$country}" data-toggle="tooltip" data-placement="top" title="cleansed {$country} Repairs"><i class="fas fa-industry fa-sm fa-fw"></i></a>
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}Repairs/dirty/{$country}" data-toggle="tooltip" data-placement="top" title="dirty {$country} Repairs"><i class="fas fa-trash-alt fa-sm fa-fw"></i></a>
            <a class="btn btn-primary m-1" target="_self" href="{base_url()}KPI/Repairs/{$country}" data-toggle="tooltip" data-placement="top" title="view ALL stats for {$country} Repairs"><i class="fas fa-chart-line fa-sm fa-fw"></i></a>
        </h5>

        <div class="row p-2">
            <div class="col-sm-6">
                <div class="card text-pre-stage">
                    <div class="card-body text-pre-stage text-center">
                        <h6 class="card-title">Dirty (preSTG)</h6>
                        <p class="card-subtitle mb-2">total records on SIGLA</p>
                        <h4 class="card-text">{$repairs['totalPreSTG']}</h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card text-pre-stage">
                    <div class="card-body text-success text-center">
                        <h6 class="card-title">Migrated (STG)</h6>
                        <p class="card-subtitle mb-2">on flat file (cleansed and validated)</p>
                        <h4 class="card-text">{$repairs['totalValidated']}</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row p-1">
            <div class="col-sm-12">
                <div class="card text-pre-stage">
                    <div class="card-body text-center">
                        <h6 class="card-title">Recommended (RFM)</h6>
                        <p class="card-subtitle mb-2">on pre-STG with ranking >= 5</p>
                        <h4 class="card-text">{$repairs['recommendedDirty']}</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row p-1">
            <div class="col-sm-6">
                <div class="card text-danger">
                    <div class="card-body text-center">
                        <h6 class="card-title">Dirty and Recommended (dRFM)</h6>
                        <p class="card-subtitle mb-2">recommended, but only on preSTG</p>
                        <h4 class="card-text">{$repairs['dirtyRecommended']}</h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card text-stage">
                    <div class="card-body text-center">
                        <h6 class="card-title">Staged and Recommended (cRFM)</h6>
                        <p class="card-subtitle mb-2">on STG and recommended</p>
                        <h4 class="card-text">{$repairs['cleansedRecommended']}</h4>
                    </div>
                </div>
            </div>
        </div>

        <br/>
        <br/>
        <br/>
    </div>

    <div class="tab-pane fade" id="pills-master" role="tabpanel" aria-labelledby="pills-master-tab">
        <h5 class="nav p-2">
            <span class="flag-icon flag-icon-{strtolower($country)}"></span>&nbsp;<span class="mt-2">{strtoupper($country)} Master Data Load Audit Logs</span>&nbsp;
            &nbsp;
        </h5>
        <table id="auditTable" class="display m-2">
            <thead>
            <tr>
                <th scope="col">Stamp</th>
                <th scope="col">Event</th>
                <th scope="col">Count</th>
                <th scope="col">Cutoff</th>
                <th scope="col">Snapshot</th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$logs item=l}
                <tr>
                    <td>{$l.stamp->format('Y-m-d H:i:s')}</td>
                    <td>{$l.event}</td>
                    <td>{$l.count}</td>
                    <td>{if isset($l.cutoffDate)}{$l.cutoffDate->format('Y-m-d H:i:s')}{else}-{/if}</td>
                    <td>{if isset($l.snapshotDate)}{$l.snapshotDate->format('Y-m-d H:i:s')}{else}-{/if}</td>
                </tr>
            {/foreach}
            </tbody>
            <tfoot>
            <tr>
                <th scope="col">Stamp</th>
                <th scope="col">Event</th>
                <th scope="col">Count</th>
                <th scope="col">Cutoff</th>
                <th scope="col">Snapshot</th>
            </tr>
            </tfoot>
        </table>

        <script>
            $(document).ready(function() {
                var table = $('#auditTable').DataTable({
                    "paging": false,
                    "lengthMenu": [[50, 100, -1], [50, 100, "All"]]
                } );
                table.order( [ 0, 'desc' ] ).draw();
            } );
        </script>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>


    </div>
</div>

