<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2">{if isset($country)}<span class="flag-icon flag-icon-{strtolower($country)}" style="margin-top:10px;"></span>{/if}<span class="mt-2"><i class="fas fa-boxes fa-lg fa-fw m-2"></i> {$title}</span>&nbsp;</h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Debug" data-toggle="tooltip" data-placement="top" title="debug and audit logs"><i class="fas fa-bug fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item mt-2 p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}IDL/groups" data-toggle="tooltip" data-placement="top" title="view all IDLs by country"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>

<div class="tab-content mt-2" id="pills-tabContent">
    <div class="tab-pane fade active show" id="pills-script" role="tabpanel" aria-labelledby="pills-manage-tab">
        <table id="bundlesTable" class="display">
            <thead>
            <tr>
                <th scope="col">Bundle</th>
                {foreach $countries as $country}
                    {if $country!=null}
                    <th scope="col"><span class="flag-icon flag-icon-{strtolower($country)}"></span></th>
                    {/if}
                {/foreach}
            </tr>
            </thead>
            <tbody>
            {foreach $bundles as $bundle=>$scope}
                <tr>
                    <td>{$bundle}</td>
                    {foreach $countries as $country}
                        {if $country!=null}
                            {if (in_array($country,$disabledCountries)||in_array($bundle,$disabledBundles))}
                            <td>
                                <a class="btn btn-secondary btn-sm disabled" target="_self" href="{base_url()}ManageFF" data-toggle="tooltip" data-placement="top" title="not available"><i class="fas fa-lock fa-sm fa-fw"></i></a>
                            </td>
                            {else}
                                <td>
                                    <a class="btn btn-primary btn-sm" target="_self" href="{base_url()}ManageFF/{$bundle}/{$country}" data-toggle="tooltip" data-placement="top" title="Manage {$bundle} {$country}"><i class="fas fa-boxes fa-sm fa-fw"></i></a>
                                </td>
                            {/if}
                        {/if}
                    {/foreach}
                </tr>
            {/foreach}
            </tbody>
            <tfoot>
            <tr>
                <th scope="col">Bundle</th>
                {foreach $countries as $country}
                    {if $country!=null}
                        <th scope="col">{$country}</th>
                    {/if}
                {/foreach}
            </tr>
            </tfoot>
        </table>
        <script>
            $(document).ready(function() {
                var table = $('#bundlesTable').DataTable({
                    "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                } );
                table.order( [ 0, 'asc' ] ).draw();
            } );
        </script>
    </div>
</div>



<br/>
<br/>
<br/>
