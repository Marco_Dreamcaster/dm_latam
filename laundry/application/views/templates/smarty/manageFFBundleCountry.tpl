<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav"><span class="p-2">{if isset($country)}<span class="flag-icon flag-icon-{strtolower($country)}"></span>{/if}<i class="fas fa-boxes fa-sm fa-fw m-4 text-TKE"></i>{$title}</span>&nbsp;</h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}ManageFF" data-toggle="tooltip" data-placement="top" title="manage all bundles"><i class="fas fa-boxes fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}RefreshFF/{$bundle}/{$country}" data-toggle="tooltip" data-placement="top" title="refresh all flatfiles on {$country} {$bundle}"><i class="fas fa-redo-alt fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}DownloadFF/{$bundle}/{$country}" data-toggle="tooltip" data-placement="top" title="download all flatfiles for {$country} {$bundle}">ORA Zip<i class="fas fa-archive fa-sm fa-fw ml-2"></i></a>
            </li>
            <li class="nav-item p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Debug" data-toggle="tooltip" data-placement="top" title="debug and audit logs"><i class="fas fa-bug fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}IDL/groups" data-toggle="tooltip" data-placement="top" title="view all IDLs by country"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>

<div class="tab-content mt-2" id="pills-tabContent">
    <div class="tab-pane fade active show" id="pills-script" role="tabpanel" aria-labelledby="pills-manage-tab">
        <table id="bundlesTable" class="display">
            <thead>
            <tr>
                <th scope="col">Bundle</th>
                <th scope="col">Type</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            {foreach $bundles as $ff}
                {if isset($ff['syncWithTemplate'])}
                <tr>
                    <td>{$ff['syncWithTemplate']['flatfileName']}</td>
                    <td>
                        <i class="fas fa-clock fa-sm fa-fw" title="synchronous"></i>
                        <i class="fas fa-palette fa-sm fa-fw" title="Smarty template"></i>
                    </td>
                    <td>
                        <a class="btn btn-primary btn-sm" target="_self" href="{base_url()}RefreshFF/{$bundle}/{$country}/{$ff['syncWithTemplate']['flatfileName']}" data-toggle="tooltip" data-placement="top" title="refresh {$bundle} {$country} {$ff['syncWithTemplate']['flatfileName']}"><i class="fas fa-redo fa-sm fa-fw"></i></a>
                        <a class="btn btn-primary btn-sm" target="_self" href="{base_url()}InspectFF/{$bundle}/{$country}/{$ff['syncWithTemplate']['flatfileName']}" data-toggle="tooltip" data-placement="top" title="inspect {$bundle} {$country} {$ff['syncWithTemplate']['flatfileName']}"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                    </td>
                </tr>
                {elseif isset($ff['asyncCMD'])}
                    <tr>
                        <td>{$ff['asyncCMD']['flatfileName']}</td>
                        <td>
                            <i class="fas fa-code-branch fa-sm fa-fw" title="asynchronous"></i>
                            <i class="fas fa-terminal fa-sm fa-fw" title="SQL CMD"></i>
                        </td>
                        <td>
                            <a class="btn btn-primary btn-sm" target="_self" href="{base_url()}RefreshFF/{$bundle}/{$country}/{$ff['asyncCMD']['flatfileName']}" data-toggle="tooltip" data-placement="top" title="refresh {$bundle} {$country} {$ff['asyncCMD']['flatfileName']}"><i class="fas fa-redo fa-sm fa-fw"></i></a>
                            <a class="btn btn-primary btn-sm" target="_self" href="{base_url()}InspectFF/{$bundle}/{$country}/{$ff['asyncCMD']['flatfileName']}" data-toggle="tooltip" data-placement="top" title="inspect {$bundle} {$country} {$ff['asyncCMD']['flatfileName']}"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                        </td>
                    </tr>
                {elseif isset($ff['asyncSQLCMD'])}
                    <tr>
                        <td>{$ff['asyncSQLCMD']['flatfileName']}</td>
                        <td>
                            <i class="fas fa-code-branch fa-sm fa-fw" title="asynchronous"></i>
                            <i class="fas fa-code fa-sm fa-fw" title="SQL Script"></i>
                        </td>
                        <td>
                            <a class="btn btn-primary btn-sm" target="_self" href="{base_url()}RefreshFF/{$bundle}/{$country}/{$ff['asyncSQLCMD']['flatfileName']}" data-toggle="tooltip" data-placement="top" title="refresh {$bundle} {$country} {$ff['asyncSQLCMD']['flatfileName']}"><i class="fas fa-redo fa-sm fa-fw"></i></a>
                            <a class="btn btn-primary btn-sm" target="_self" href="{base_url()}InspectFF/{$bundle}/{$country}/{$ff['asyncSQLCMD']['flatfileName']}" data-toggle="tooltip" data-placement="top" title="inpect {$bundle} {$country} {$ff['asyncSQLCMD']['flatfileName']}"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                        </td>
                    </tr>
                {/if}

            {/foreach}
            </tbody>
            <tfoot>
            <tr>
                <th scope="col">Bundle</th>
                <th scope="col">Type</th>
                <th scope="col">Actions</th>
            </tr>
            </tfoot>
        </table>
        <script>
            $(document).ready(function() {
                var table = $('#bundlesTable').DataTable({
                    "lengthMenu": [[100, 200, -1], [100, 200, "All"]]
                } );
                table.order( [ 0, 'asc' ] ).draw();
            } );
        </script>
    </div>
</div>



<br/>
<br/>
<br/>
