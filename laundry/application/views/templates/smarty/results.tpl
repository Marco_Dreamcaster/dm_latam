<h4 class="p-3 text-center">Search</h4>
<form id="search" class="form-inline d-flex justify-content-center" method="post" action="{base_url()}Search" accept-charset="UTF-8">
    <div class="form-group mx-sm-8 mb-0">
        <div class="input-group mb-0">
            <div class="input-group-prepend">
                <div class="input-group-text">%<small class="m-2 text-danger">{$errorKeywords}</small></div>
            </div>
            <input type="text" class="form-control form-control-sm p-2" id="keywords" name="keywords" size="60" value="{$keywords}">
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Search</button>
</form>

{if sizeof($results)>0}
    <table id="auditTable" class="display" style="width:100%">
        <thead>
        <tr>
            <th scope="col">Country</th>
            <th scope="col">Type</th>
            <th scope="col">Detail</th>
            <th scope="col">#</th>
        </tr>
        </thead>
        <tbody>
        {foreach from=$results item=r name=results}
            <tr>
                <td class="p-2"><span style="vertical-align: middle" class="flag-icon flag-icon-{strtolower($r.country)}"></span>&nbsp;<span>{strtoupper($r.country)}</td>
                <td>
                    {if strstr($r.type, 'pre-STG')}
                    <span class="text-pre-stage m-2">
                    <i class="fas fa-trash-alt fa-lg fa-fw" title="available for staging"></i>
                        {else}
                        <span class="text-stage m-2">
                    <i class="fas fa-industry fa-lg fa-fw" title="already staged"></i>
                            {/if}
                            {if strstr($r.type, 'Customer')}
                                <i class="fas fa-user-tie fa-lg fa-fw" title="Customer"></i>
                    {elseif strstr($r.type, 'Vendor')}
                    <i class="fas fa-comment-dots fa-lg fa-fw" title="Vendor"></i>
                    {elseif strstr($r.type, 'Item')}
                    <i class="fas fa-boxes fa-lg fa-fw" title="Item"></i>
                    {elseif strstr($r.type, 'Project')}
                    <i class="fas fa-project-diagram fa-lg fa-fw" title="Project"></i>
                    {elseif strstr($r.type, 'Service')}
                    <i class="fas fa-building fa-lg fa-fw" title="Service"></i>
                    {elseif strstr($r.type, 'Repair')}
                    <i class="fas fa-ambulance fa-lg fa-fw" title="Repair"></i>
                            {/if}

                </span>

                        {$r.type}
                </td>
                <td>{$r.detail}</td>
                <td>
                    <a class="btn btn-primary" target="_blank" href="{base_url()}{$r.actionUrl}" data-toggle="tooltip" data-placement="top" title="go to {$r.detail}"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                </td>
            </tr>
        {/foreach}
        </tbody>
        <tfoot>
        <tr>
            <th scope="col">Country</th>
            <th scope="col">Type</th>
            <th scope="col">Detail</th>
            <th scope="col">#</th>
        </tr>
        </tfoot>
    </table>
    <script>
        $(document).ready(function() {
            var table = $('#auditTable').DataTable({
                "lengthMenu": [[50, 100, -1], [50, 100, "All"]],
                "language": {
                    "search": "refine your search:"
                }
            } );
            table.order( [ 0, 'desc' ] ).draw();
        } );
    </script>

{/if}
<br/>
<br/>
<br/>
<br/>

