<nav class="navbar navbar-expand-lg navbar-light bg-light col-md-12 ml-sm-auto col-lg-12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <h5 class="nav p-2"><span class="flag-icon flag-icon-{strtolower($country)}" style="margin-top:10px;"></span>&nbsp;<span class="mt-2">{$country}&nbsp;{$title}</span>&nbsp;</h5>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}ManageFF" data-toggle="tooltip" data-placement="top" title="manage all bundles"><i class="fas fa-boxes fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}ManageFF/{$bundle}/{$country}" data-toggle="tooltip" data-placement="top" title="manage {$country} {$bundle}"><i class="fas fa-boxes fa-sm fa-fw"></i> <span class="flag-icon flag-icon-{strtolower($country)} ml-2"></span></a>
            </li>
            <li class="nav-item p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Setup/{$country}" data-toggle="tooltip" data-placement="top" title="Setup"><i class="fas fa-sliders-h fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}Debug" data-toggle="tooltip" data-placement="top" title="debug and audit logs"><i class="fas fa-bug fa-sm fa-fw"></i></a>
            </li>
            <li class="nav-item p-1">
                <a class="btn btn-primary" target="_self" href="{base_url()}IDL/groups" data-toggle="tooltip" data-placement="top" title="view all IDLs by country"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
            </li>
        </ul>
    </div>
</nav>

<div class="navbar navbar-expand-lg navbar-light bg-light col-md-4 ml-sm-auto col-lg-12 p-2">

    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        {foreach from=$flatfiles item=ff name=ffPills}
        <a class="nav-link {if $smarty.foreach.ffPills.iteration==1}active{/if}" id="v-pills-{$ff.flatfileName}-tab" data-toggle="pill" href="#v-pills-{$ff.flatfileName}" role="tab" aria-controls="v-pills-{$ff.flatfileName}" aria-selected="false">{$ff.flatfileName}</a>
        {/foreach}
    </div>
    <div class="tab-content p-2" id="v-pills-tabContent">
        {foreach from=$flatfiles item=ff name=ffTabs}
        <div class="tab-pane fade p-4 text-center {if $smarty.foreach.ffTabs.iteration==1}active show{/if}" id="v-pills-{$ff.flatfileName}" role="tabpanel" aria-labelledby="v-pills-{$ff.flatfileName}-tab">
            <h4 class="text-TKE">{$ff.outputFileName}</h4>
            <small class="p-2">{$ff.description}</small>
            <p class="p-2">{$ff.flatfileName}</p>
            {if ($ff.strategy=='syncWithTemplate')}
                <p>
                    <a class="btn btn-success" target="_self" href="{base_url()}DownloadFF/{$bundle}/{$country}" data-toggle="tooltip" data-placement="top" title="Dowload  bundle {$country} {$bundle}"><i class="fas fa-file-archive fa-sm fa-fw"></i>&nbsp;ORA Zip</a>
                    &nbsp;
                    <a class="btn btn-primary" target="_self" href="{base_url()}RefreshFF/{$bundle}/{$country}/{$ff.flatfileName}" data-toggle="tooltip" data-placement="top" title="Refresh flat file {$ff.flatfileName} from  bundle {$country} {$bundle}"><i class="fas fa-redo-alt fa-sm fa-fw"></i></a>
                    &nbsp;
                    <a class="btn btn-primary" target="_blank" href="{base_url()}InspectFF/{$bundle}/{$country}/{$ff.flatfileName}" data-toggle="tooltip" data-placement="top" title="Inspect flat file {$ff.flatfileName} from  bundle {$country} {$bundle}"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                    &nbsp;
                    <a class="btn btn-success" target="_blank" href="{base_url()}IDL/Navigate/navigateGroup/{$country}/{$bundle}" data-toggle="tooltip" data-placement="top" title="specification for {$bundle} group - use for debug!!"><i class="fas fa-object-group fa-sm fa-fw"></i></a>
                </p>
                <div class="alert-success text-success p-4 mb-2">
                    <div class="row">
                        <div class="col-12"><p>{$ff.strategy}</p></div>
                        <div class="col-12"><small><i class="fas fa-clock fa-sm fa-fw"></i> The new file is already available.  This does not mean that all files on the package are updated</small></div>
                    </div>
                    <p>{$ff.strategy}</p>
                    <p>O arquivo j� est� dipon�vel. Isso n�o significa que todos arquivos do pacote estejam atualizados.
                    </p>
                </div>
                <p><small>{$ff.strategy}</small> <b>{$ff.template}</b></p>
                <p>{$ff.cmd}</p>
                <p>{$ff.outputFileFullPath}</p>
            {elseif ($ff.strategy=='asyncCMD') || ($ff.strategy=='asyncSQLCMD')}
                <p>
                    <a class="btn btn-danger" target="_self" href="{base_url()}DownloadFF/{$bundle}/{$country}" data-toggle="tooltip" data-placement="top" title="Dowload  bundle {$country} {$bundle}"><i class="fas fa-file-archive fa-sm fa-fw"></i>&nbsp;ORA Zip</a>
                    &nbsp;
                    <a class="btn btn-danger" target="_blank" href="{base_url()}InspectFF/{$bundle}/{$country}/{$ff.flatfileName}" data-toggle="tooltip" data-placement="top" title="Inspect flat file {$ff.flatfileName} from  bundle {$country} {$bundle}"><i class="fas fa-eye fa-sm fa-fw"></i></a>
                </p>
                <div class="alert-danger text-danger p-4 mb-2">
                    <div class="row">
                        <div class="col-12"><p>{$ff.strategy}</p></div>
                        <div class="col-12"><small><i class="fas fa-clock fa-sm fa-fw"></i> You may have to wait until the script ends before downloading the zip (usually 1min for Master Data is enough)</small></div>
                    </div>
                </div>
                <p>{$ff.outputFileFullPath}</p>
                <p>{if isset($ff.encodeAs)}{$ff.encodeAs}{else}txt, no header, separated by pipes |{/if}</p>
                <p>{if isset($ff.cmd)}{$ff.cmd}{else}{$ff.inputScriptFullPath}{/if}</p>
                <p>cd {$ff.cwd}</p>
                <p>{$ff.cmdLine}</p>
            {/if}

        </div>
        {/foreach}
    </div>
</div>

<br/>
<br/>
<br/>
