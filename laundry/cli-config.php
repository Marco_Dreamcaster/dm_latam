<?php
use Doctrine\ORM\Tools\Console\ConsoleRunner;

$env = getenv('LAUNDRY_ENVIRONMENT');
define('LAUNDRY_ENVIRONMENT', isset($env) ? getenv('LAUNDRY_ENVIRONMENT') : 'development');

if(LAUNDRY_ENVIRONMENT == 'production') {
    define('BOOTSTRAP_PATH', 'C:\dm_latam\laundry\prototypes\bootstrap.php');
}else{
    define('BOOTSTRAP_PATH', '/var/www/dm_latam.demo/laundry/prototypes/bootstrap.php');
}

// replace with file to your own project bootstrap

require_once BOOTSTRAP_PATH;

return ConsoleRunner::createHelperSet($entityManager);