<?php
// bootstrap.php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

if((LAUNDRY_ENVIRONMENT == 'production')||(LAUNDRY_ENVIRONMENT == 'vm')) {
    define('APPATH', 'C:\dm_latam');
    define('AUTOLOAD_PATH', 'C:\dm_latam\laundry\vendor\autoload.php');
    define('MODEL1_PATH', 'C:\dm_latam\laundry\application\models\Commons');
    define('MODEL2_PATH', 'C:\dm_latam\laundry\application\models\Commons\ORA');
    define('MODEL3_PATH', 'C:\dm_latam\laundry\application\models\Commons\RAW');

    $conn = array(
        'driver' => 'sqlsrv',
        'user' => 'sa',
        'password' => 'Oracle2018',
        'host' => '10.148.1.44',
        'dbname' => 'DM_LATAM'
    );

    define('BASE_PATH', '');

}else{

    define('APPATH', '/var/www/dm_latam.demo');
    define('AUTOLOAD_PATH', '/var/www/dm_latam.demo/laundry/vendor/autoload.php');
    define('MODEL1_PATH', '/var/www/dm_latam.demo/laundry/application/models/Commons');
    define('MODEL2_PATH', '/var/www/dm_latam.demo/laundry/application/models/Commons/ORA');
    define('MODEL3_PATH', '/var/www/dm_latam.demo/laundry/application/models/Commons/RAW');

    $ini = parse_ini_file('/var/www/dm_latam.demo/laundry/DB.ini', true);
    $cUser = $ini['user'];
    $cPass = $ini['pass'];
    $cDb = $ini['db'];
    $cSrv = $ini['srv'];

    $conn = array(
        'driver' => 'sqlsrv',
        'user' => $cUser,
        'password' => $cPass,
        'host' => $cSrv,
        'dbname' => $cDb
    );

}

require(AUTOLOAD_PATH);

$isDevMode = true;
$useSimpleAnnotationReader = false;
$config = Setup::createAnnotationMetadataConfiguration(array(
    MODEL1_PATH,
    MODEL2_PATH,
    MODEL3_PATH), $isDevMode, $useSimpleAnnotationReader);



// obtaining the entity manager
$entityManager = EntityManager::create($conn, $config);


function getIDLFor($em, $country, $groupShortName, $idlShortName)
{
    $idlGroupRepository = $em->getRepository('Commons\\IDL\\IDLGroupDescriptor');
    $idlGroup = $idlGroupRepository->findOneBy(array('country' => $country, 'shortName' => $groupShortName));

    $idlRepository = $em->getRepository('Commons\\IDL\\IDLDescriptor');
    $idl = $idlRepository->findOneBy(array('shortName' => $idlShortName, 'group' => $idlGroup->getId()));
    return $idl;
}

function getGroupFor($em, $country, $groupShortName){
    $idlGroupRepository = $em->getRepository('Commons\\IDL\\IDLGroupDescriptor');
    $idlGroup = $idlGroupRepository->findOneBy(array('country' => $country, 'shortName' => $groupShortName));

    return $idlGroup;

}