<?php
// list_projectHeaders.php
require_once "shared.php";

echo 'fixing Manual PriceOverride for countries';

$implemented = array("PA","CO",'MX','SV','GT','CR','NI','HN');

$missingFields = array(
    "calculatedListPricePerUnit"=>['ORA_TYPE'=>'NUMBER', 'columName'=>'CALCULATED_LIST_PRICE_PER_UNIT'],
    "calculatedQuantity"=>['ORA_TYPE'=>'NUMBER', 'columName'=>'CALCULATED_QUANTITY'],
    "priceManualOverride"=>['ORA_TYPE'=>'VARCHAR2(1)', 'columName'=>'PRICE_MANUAL_OVERRIDE']
);

foreach($implemented as $country){

    foreach($missingFields as $missing=>$params){
        $qb = $entityManager->createQueryBuilder();

        $qb->select('f')
            ->from('Commons\\IDL\\IDLFieldDescriptor', 'f')
            ->leftJoin('f.descriptor', 'd')
            ->leftJoin('d.group', 'g')
            ->andWhere(
                $qb->expr()->eq('g.country', '?1'),
                $qb->expr()->eq('d.shortName',  '?2'),
                $qb->expr()->eq('f.shortName',  '?3')

            )
            ->setParameter(1, $country)
            ->setParameter(2, 'ItemHeader')
            ->setParameter(3, $missing);

        $fields = $qb->getQuery()->getResult();

        if (sizeof($fields)==0){
            //create it
            print_r ('creating field '.$missing.' on ItemHeader of '.$country);

            $qb = $entityManager->createQueryBuilder();
            $qb->select('d', 'g')
                ->from('Commons\\IDL\\IDLDescriptor', 'd')
                ->leftJoin('d.group', 'g')
                ->andWhere(
                    $qb->expr()->eq('g.country', '?1'),
                    $qb->expr()->eq('d.shortName',  '?2')

                )
                ->setParameter(1, $country)
                ->setParameter(2, 'ItemHeader');

            $group = $qb->getQuery()->getSingleResult();

            $field = new Commons\IDL\IDLFieldDescriptor();
            $field->setShortName($missing);
            $field->setORA_TYPE($params['ORA_TYPE']);
            $field->setColName($params['columName']);
            $field->setOutputPos(0);
            $field->setIsOutputValue(false);
            $field->setIsMandatory(false);
            $field->setDescriptor($group);

            $field->getDescription('automatically assigned via script, @comments on PA');

            $entityManager->persist($field);
            $entityManager->flush();

            $group->addField($field);
            $entityManager->merge($group);
            $entityManager->flush();




        }else{
            assert (sizeof($fields)==1);
        }

    }



}
