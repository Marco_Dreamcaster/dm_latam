<?php

// list_projectHeaders.php
require_once "shared.php";

$countries = ['PA','CO'];

foreach($countries as $country){
    echo 'checking Customers from '.$country;
    checkAllCustomerFor($entityManager, $country);

    echo 'checking Vendors from '.$country;
    checkAllVendorFor($entityManager, $country);

    echo 'checking Project from '.$country;
    checkAllProjectsFor($entityManager, $country);

}


