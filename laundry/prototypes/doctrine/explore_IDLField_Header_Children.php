<?php
/**
 * Created by PhpStorm.
 * User: Dreamcaster
 * Date: 3/23/2018
 * Time: 10:40 AM
 */

// list_projectHeaders.php
require_once "bootstrap.php";

Use Doctrine\ORM\Query;

Use Commons\IDL\IDLGroupDescriptor;
Use Commons\IDL\IDLDescriptor;
Use Commons\IDL\IDLFieldDescriptor;

function explore($em){
    $qb = $em->createQueryBuilder();
    $qb->select('f', 'd', 'g')
        ->from('Commons\\IDL\\IDLFieldDescriptor', 'f')
        ->leftJoin('f.descriptor', 'd')
        ->leftJoin('d.group', 'g')
        ->andWhere(
            $qb->expr()->eq('g.country', '?1'),
            $qb->expr()->eq('d.shortName',  '?2'),
            $qb->expr()->eq('f.shortName', '?3')
        );

    $instQuery = $qb->getQuery();
    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'CustomerHeader')
        ->setParameter(3, 'origSystemCustomerRef');

    $origSystemCustomerRefFromHeader = $instQuery->getSingleResult();
    assert($origSystemCustomerRefFromHeader!=null);

    $instQuery = $qb->getQuery();
    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'CustomerAddress')
        ->setParameter(3, 'origSystemCustomerRef');

    $origSystemCustomerRefFromChild_CustomerAddress = $instQuery->getSingleResult();
    assert($origSystemCustomerRefFromChild_CustomerAddress!=null);

    if ($origSystemCustomerRefFromChild_CustomerAddress->getHeader()==null){
        $origSystemCustomerRefFromChild_CustomerAddress->setHeader($origSystemCustomerRefFromHeader);
        $em->persist($origSystemCustomerRefFromHeader);
        $em->flush();
    }

    $instQuery = $qb->getQuery();
    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'CustomerContact')
        ->setParameter(3, 'origSystemCustomerRef');

    $origSystemCustomerRefFromChild_CustomerContact = $instQuery->getSingleResult();
    assert($origSystemCustomerRefFromChild_CustomerContact!=null);

    if ($origSystemCustomerRefFromChild_CustomerContact->getHeader()==null){
        $origSystemCustomerRefFromChild_CustomerContact->setHeader($origSystemCustomerRefFromHeader);
        $em->persist($origSystemCustomerRefFromHeader);
        $em->flush();
    }

    // get children fields
    //
    $qb = $em->createQueryBuilder();
    $qb->select('f', 'children', 'd', 'g')
        ->from('Commons\\IDL\\IDLFieldDescriptor', 'f')
        ->leftJoin('f.descriptor', 'd')
        ->leftJoin('d.group', 'g')
        ->leftJoin('f.children', 'children')
        ->andWhere(
            $qb->expr()->eq('g.country', '?1'),
            $qb->expr()->eq('d.shortName',  '?2')
        );

    $childrenQuery = $qb->getQuery();
    $childrenQuery->setParameter(1, 'PA')
        ->setParameter(2, 'CustomerHeader');

    $fields = $childrenQuery->getResult();

    $headerFields = array();
    $childrenFields = array();

    foreach($fields as $f){
        if ($f->getHeader()!=null){
            $headerFields[]=$f;
        }

        if (sizeof($f->getChildren())!=0){
            $children = $f->getChildren();
            foreach($children as $child){
                $childrenFields[]=$child;

            }

        }
    }

    assert(sizeof($headerFields)==0);
    assert(sizeof($childrenFields)==2);

    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'CustomerAddress')
        ->setParameter(3, 'origSystemAdressReference');

    $origSystemAdressReferenceFromHeader = $instQuery->getSingleResult();
    assert($origSystemAdressReferenceFromHeader!=null);


    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'CustomerContact')
        ->setParameter(3, 'origSystemAdressReference');

    $origSystemAdressReferenceFromCustomerContact = $instQuery->getSingleResult();
    assert($origSystemAdressReferenceFromCustomerContact!=null);

    if ($origSystemAdressReferenceFromCustomerContact->getHeader()==null){
        $origSystemAdressReferenceFromCustomerContact->setHeader($origSystemAdressReferenceFromHeader);
    }

    $em->merge($origSystemAdressReferenceFromHeader);
    $em->flush();

    // Projects

    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'ProjectHeader')
        ->setParameter(3, 'projectName');

    $projectNameFromHeader = $instQuery->getSingleResult();
    assert($projectNameFromHeader!=null);

    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'ProjectTasks')
        ->setParameter(3, 'projectName');

    $projectNameFromTasks = $instQuery->getSingleResult();
    assert($projectNameFromTasks!=null);

    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'ProjectClassifications')
        ->setParameter(3, 'projectName');

    $projectNameFromClassifications = $instQuery->getSingleResult();
    assert($projectNameFromClassifications!=null);

    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'ProjectRetRules')
        ->setParameter(3, 'projectName');

    $projectNameFromRetRules = $instQuery->getSingleResult();
    assert($projectNameFromRetRules!=null);

    if ($projectNameFromClassifications->getHeader()==null){
        $projectNameFromClassifications->setHeader($projectNameFromHeader);
    }

    if ($projectNameFromTasks->getHeader()==null){
        $projectNameFromTasks->setHeader($projectNameFromHeader);
    }

    if ($projectNameFromRetRules->getHeader()==null){
        $projectNameFromRetRules->setHeader($projectNameFromHeader);
    }

    $em->merge($projectNameFromHeader);
    $em->flush();


    // Vendors

    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'VendorHeader')
        ->setParameter(3, 'segment1');
    $segment1FromHeader = $instQuery->getSingleResult();
    assert($segment1FromHeader!=null);


    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'VendorSite')
        ->setParameter(3, 'vendorNumber');
    $vendorNumberFromSite = $instQuery->getSingleResult();
    assert($vendorNumberFromSite!=null);


    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'VendorSite')
        ->setParameter(3, 'vendorSiteCode');
    $vendorSiteCodeFromSite = $instQuery->getSingleResult();
    assert($vendorSiteCodeFromSite!=null);

    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'VendorSiteContact')
        ->setParameter(3, 'vendorNumber');
    $vendorNumberFromSiteContact = $instQuery->getSingleResult();
    assert($vendorNumberFromSiteContact!=null);

    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'VendorSiteContact')
        ->setParameter(3, 'vendorSiteCode');
    $vendorSiteCodeFromSiteContact = $instQuery->getSingleResult();
    assert($vendorSiteCodeFromSiteContact!=null);


    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'VendorBankAccount')
        ->setParameter(3, 'vendorNumber');

    $vendorNumberFromBankAccount = $instQuery->getSingleResult();
    assert($vendorNumberFromBankAccount!=null);

    if ($vendorNumberFromSite->getHeader()==null){
        $vendorNumberFromSite->setHeader($segment1FromHeader);
    }

    if ($vendorNumberFromSiteContact->getHeader()==null){
        $vendorNumberFromSiteContact->setHeader($segment1FromHeader);
    }

    if ($vendorSiteCodeFromSiteContact->getHeader()==null){
        $vendorSiteCodeFromSiteContact->setHeader($vendorSiteCodeFromSite);
    }

    if ($vendorNumberFromBankAccount->getHeader()==null){
        $vendorNumberFromBankAccount->setHeader($segment1FromHeader);
    }

    $em->merge($segment1FromHeader);
    $em->merge($vendorSiteCodeFromSite);
    $em->flush();



    // Inventory

    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'ItemHeader')
        ->setParameter(3, 'origItemNumber');
    $origItemNumberFromHeader = $instQuery->getSingleResult();
    assert($origItemNumberFromHeader!=null);

    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'ItemCategory')
        ->setParameter(3, 'origItemNumber');
    $origItemNumberFromCategory = $instQuery->getSingleResult();
    assert($origItemNumberFromCategory!=null);

    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'ItemCrossRef')
        ->setParameter(3, 'origItemNumber');
    $origItemNumberFromCrossRef = $instQuery->getSingleResult();
    assert($origItemNumberFromCrossRef!=null);

    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'ItemMFGPartNum')
        ->setParameter(3, 'origItemNumber');
    $origItemNumberFromMFGPartNum = $instQuery->getSingleResult();
    assert($origItemNumberFromMFGPartNum!=null);

    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'ItemTransDefLoc')
        ->setParameter(3, 'origItemNumber');
    $origItemNumberFromTransDefLoc = $instQuery->getSingleResult();
    assert($origItemNumberFromTransDefLoc!=null);

    $instQuery->setParameter(1, 'PA')
        ->setParameter(2, 'ItemTransDefSubInv')
        ->setParameter(3, 'origItemNumber');
    $origItemNumberFromTransDefSubInv = $instQuery->getSingleResult();
    assert($origItemNumberFromTransDefSubInv!=null);

    if ($origItemNumberFromCategory->getHeader()==null){
        $origItemNumberFromCategory->setHeader($origItemNumberFromHeader);
    }

    if ($origItemNumberFromCrossRef->getHeader()==null){
        $origItemNumberFromCrossRef->setHeader($origItemNumberFromHeader);
    }

    if ($origItemNumberFromMFGPartNum->getHeader()==null){
        $origItemNumberFromMFGPartNum->setHeader($origItemNumberFromHeader);
    }

    if ($origItemNumberFromTransDefLoc->getHeader()==null){
        $origItemNumberFromTransDefLoc->setHeader($origItemNumberFromHeader);
    }

    if ($origItemNumberFromTransDefSubInv->getHeader()==null){
        $origItemNumberFromTransDefSubInv->setHeader($origItemNumberFromHeader);
    }

    $em->merge($origItemNumberFromHeader);
    $em->flush();


}

explore($entityManager);




