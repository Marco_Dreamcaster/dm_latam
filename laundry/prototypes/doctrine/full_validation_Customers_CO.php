fu<?php
/**
 * Created by PhpStorm.
 * User: Dreamcaster
 * Date: 3/23/2018
 * Time: 10:40 AM
 */

// list_projectHeaders.php
require_once "../../bootstrap.php";

Use Doctrine\ORM\Query;

Use Commons\IDL\IDLGroupDescriptor;
Use Commons\IDL\IDLDescriptor;
Use Commons\IDL\IDLFieldDescriptor;

function localValidate($em, $className, $country){
    $qb = $em->createQueryBuilder();
    $qb->select('ch')
        ->from($className, 'ch')
        ->andWhere(
            $qb->expr()->eq('ch.country', '?1')
        );

    $instQuery = $qb->getQuery();
    $instQuery->setParameter(1, $country);

    $entityList = $instQuery->getResult();

    foreach($entityList as $e){
        print ('validate '. $className . ' - ' . $e->getId() . "\n");
        $e->validate($em);
        $em->merge($e);
        $em->flush();
    }
}

function validateCrossRef($em, $className, $country){
    $qb = $em->createQueryBuilder();
    $qb->select('ch')
        ->from($className, 'ch')
        ->andWhere(
            $qb->expr()->eq('ch.country', '?1')
        );

    $instQuery = $qb->getQuery();
    $instQuery->setParameter(1, $country);

    $entityList = $instQuery->getResult();

    foreach($entityList as $e){
        print ('crossvalidate '. $className . ' - ' . $e->getId() . "\n");
        $e->validateCrossRef($em);
        $em->merge($e);
        $em->flush();
    }
}


validateLocal($entityManager, 'Commons\\ORA\\CustomerContact', 'CO');
validateLocal($entityManager, 'Commons\\ORA\\CustomerAddress', 'CO');
validateLocal($entityManager, 'Commons\\ORA\\CustomerHeader', 'CO');

validateCrossRef($entityManager, 'Commons\\ORA\\CustomerAddress', 'CO');
validateCrossRef($entityManager, 'Commons\\ORA\\CustomerHeader', 'CO');



