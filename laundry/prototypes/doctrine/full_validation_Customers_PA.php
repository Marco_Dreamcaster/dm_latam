<?php
/**
 * Created by PhpStorm.
 * User: Dreamcaster
 * Date: 3/23/2018
 * Time: 10:40 AM
 */

// list_projectHeaders.php
require_once "../bootstrap.php";

Use Doctrine\ORM\Query;

Use Commons\IDL\IDLGroupDescriptor;
Use Commons\IDL\IDLDescriptor;
Use Commons\IDL\IDLFieldDescriptor;

function localValidate($em, $className, $country){
    $qb = $em->createQueryBuilder();
    $qb->select('ch')
        ->from($className, 'ch')
        ->andWhere(
            $qb->expr()->eq('ch.country', '?1')
        );

    $instQuery = $qb->getQuery();
    $instQuery->setParameter(1, $country);

    $entityList = $instQuery->getResult();
    $total = sizeof($entityList);

    for($i=0;$i<$total;$i++){
        printf("local validated %s - %d / %d \r\n", $className, $i, $total);
        $entityList[$i]->validate($em);
        $em->merge($entityList[$i]);
    }
    $em->flush();

}

function validateCrossRef($em, $className, $country){
    $qb = $em->createQueryBuilder();
    $qb->select('ch')
        ->from($className, 'ch')
        ->andWhere(
            $qb->expr()->eq('ch.country', '?1')
        );

    $instQuery = $qb->getQuery();
    $instQuery->setParameter(1, $country);

    $entityList = $instQuery->getResult();

    $total = sizeof($entityList);

    for($i=0;$i<$total;$i++){
        printf("crossRef validate %s - %d / %d \r\n", $className, $i, $total);
        $entityList[$i]->validateCrossRef($em, false);
        $em->merge($entityList[$i]);
    }
    $em->flush();
}

function checkAllFor($em, $country){
    validateLocal($em, 'Commons\\ORA\\CustomerContact', $country);
    validateLocal($em, 'Commons\\ORA\\CustomerAddress', $country);
    validateLocal($em, 'Commons\\ORA\\CustomerHeader', $country);

    validateCrossRef($em, 'Commons\\ORA\\CustomerAddress', $country);
    validateCrossRef($em, 'Commons\\ORA\\CustomerHeader', $country);

}

//checkAllFor($entityManager, 'PA');
checkAllFor($entityManager, 'CO');


