<?php
use Commons\ProjectHeader;

// list_projectHeaders.php

require_once "prototypes/bootstrap.php";

$projectHeaderRepository = $entityManager->getRepository('Commons\PA\ORA\ProjectHeader');
$projectHeaders = $projectHeaderRepository->findAll();

foreach ($projectHeaders as $p) {
    echo sprintf("-%s\n", $p->getDescription());
}