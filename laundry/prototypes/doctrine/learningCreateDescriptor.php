<?php
/*
 * http://docs.doctrine-project.org/en/latest/tutorials/getting-started.html
 *
 * .\vendor\bin\doctrine.bat orm:validate-schema
 *
 * [Mapping]  OK - The mapping files are correct.
 * [Database] OK - The database schema is in sync with the mapping files.
 *
 */
use Commons\IDLDescriptor;
use Commons\IDLGroupDescriptor;

// list_projectHeaders.php
require_once "../bootstrap.php";

$group = new IDLGroupDescriptor();
$group->setShortName("Project");
$group->setOutputPath("C:\Users\Dreamcaster\OneDrive\datasets");
$group->setCountry("PA");

$item = new IDLDescriptor();
$item->setShortName("pmProjectReference");
$item->setColName("PM_PROJECT_REFERENCE");

$item->setGroup($group);

$item = new IDLDescriptor();
$item->setShortName("projectName");
$item->setColName("PROJECT_NAME");

$item->setGroup($group);

$entityManager->persist($group);
$entityManager->flush();

$descriptorIDLGroupRepository = $entityManager->getRepository('Commons\IDLGroupDescriptor');
$groups = $descriptorIDLGroupRepository->findAll();

foreach ($groups as $g) {
    echo sprintf("%s\n", $g->getShortName());

    $currentDescriptors = $g->getDescriptors();

    foreach($currentDescriptors as $d){
        echo sprintf( "%s - ", $d->getColName());
    }
}