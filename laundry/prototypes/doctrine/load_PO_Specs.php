<?php
/**
 * Created by PhpStorm.
 * User: Dreamcaster
 * Date: 3/23/2018
 * Time: 10:40 AM
 */

// list_projectHeaders.php
require_once "../bootstrap.php";

Use Commons\IDL\IDLGroupDescriptor;
Use Commons\IDL\IDLDescriptor;
Use Commons\IDL\IDLFieldDescriptor;

function createIDL($em, $country, $groupName, $idlName, $group, $templateName, $tableName, $fileName, $isHeader, $isTransactional){

    $idl = new IDLDescriptor();
    $idl->setShortName($idlName);
    $idl->setTemplateName($templateName);
    $idl->setTableName($tableName);
    $idl->setGroup($group);
    $idl->setIsHeader($isHeader);
    $idl->setIsTransactional($isTransactional);

    $em->persist($idl);
    $em->flush();

    $json = file_get_contents($fileName);
    $fields = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json), true);

    $i=0;
    foreach($fields as $f){
        $fieldDescriptor = new IDLFieldDescriptor();
        $fieldDescriptor->setShortName($f['Source Field Name']);
        $fieldDescriptor->setIsMandatory($f['Mandatory']=='Y'?1:0);
        $fieldDescriptor->setORA_TYPE($f['Destination Type']);
        $fieldDescriptor->setColName($f['Destination Name']);
        $fieldDescriptor->setIsOutputValue(true);
        $fieldDescriptor->setDescription($f['Comments']);
        $fieldDescriptor->setOutputPos($i);
        $fieldDescriptor->setDescriptor($idl);
        $i++;

        $em->persist($fieldDescriptor);
        $em->flush();

    }

    $idl = getIDLFor($em,$country, $groupName, $idlName);

    assert($idl!=null);
    assert(sizeof($idl->getFields())!=0);
    assert(sizeof($idl->getFields())==sizeof($fields));


}

$group_PO = getGroupFor($entityManager, 'CO', 'PO');
if (isset($group_PO)){
    foreach($group_PO->getDescriptors() as $descriptor){
        foreach($descriptor->getFields() as $field){
            $entityManager->remove($field);
            $entityManager->flush();
        }

        $entityManager->remove($descriptor);
        $entityManager->flush();

    }
    $entityManager->remove($group_PO);
    $entityManager->flush();
}

$group_PO = new IDLGroupDescriptor();
$group_PO->setCountry('CO');
$group_PO->setShortName('PO');
$group_PO->setOutputPath('02.COL');

$entityManager->persist($group_PO);
$entityManager->flush();


createIDL($entityManager, 'CO', 'PO', 'POHeader', $group_PO, 'FD_06',  'PO_HEADER', 'D:\pub\dm_latam\laundry\prototypes\excel-as-json\xlIDLExtracts\TKE_LATAM_IDL_FD_06_PO_FLAT_FILE_header.json', true, true);
createIDL($entityManager, 'CO', 'PO', 'POLine', $group_PO, 'FD_06',  'PO_LINE', 'D:\pub\dm_latam\laundry\prototypes\excel-as-json\xlIDLExtracts\TKE_LATAM_IDL_FD_06_PO_FLAT_FILE_line.json', false, true);
createIDL($entityManager, 'CO', 'PO', 'POShip', $group_PO, 'FD_06',  'PO_SHIP', 'D:\pub\dm_latam\laundry\prototypes\excel-as-json\xlIDLExtracts\TKE_LATAM_IDL_FD_06_PO_FLAT_FILE_ship.json', false, true);
createIDL($entityManager, 'CO', 'PO', 'PODist', $group_PO, 'FD_06',  'PO_DIST', 'D:\pub\dm_latam\laundry\prototypes\excel-as-json\xlIDLExtracts\TKE_LATAM_IDL_FD_06_PO_FLAT_FILE_dist.json', false, true);

