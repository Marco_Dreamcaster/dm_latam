<?php
/**
 * Created by PhpStorm.
 * User: Dreamcaster
 * Date: 3/23/2018
 * Time: 10:40 AM
 */

// list_projectHeaders.php
require_once "../bootstrap.php";

Use Commons\IDL\IDLGroupDescriptor;
Use Commons\IDL\IDLDescriptor;
Use Commons\IDL\IDLFieldDescriptor;

function createIDL($em, $country, $groupName, $idlName, $group, $templateName, $tableName, $fileName, $isHeader, $isTransactional){

    $idl = new IDLDescriptor();
    $idl->setShortName($idlName);
    $idl->setTemplateName($templateName);
    $idl->setTableName($tableName);
    $idl->setGroup($group);
    $idl->setIsHeader($isHeader);
    $idl->setIsTransactional($isTransactional);

    $em->persist($idl);
    $em->flush();

    $json = file_get_contents($fileName);
    $fields = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json), true);

    foreach($fields as $f){
        $fieldDescriptor = new IDLFieldDescriptor();
        $fieldDescriptor->setShortName($f['field name']);
        $fieldDescriptor->setIsMandatory($f['mandatory']=='Y'?1:0);
        $fieldDescriptor->setORA_TYPE($f['ORA_type']);
        $fieldDescriptor->setLOV($f['LOV']);
        $fieldDescriptor->setColName($f['field name']);
        $fieldDescriptor->setConstant($f['constant']);
        $fieldDescriptor->setIsOutputValue($f['IsOutputValue']='Y'?1:0);
        $fieldDescriptor->setOutputPos($f['OutputPos']);
        $fieldDescriptor->setDescriptor($idl);

        $em->persist($fieldDescriptor);
        $em->flush();

    }

    $idl = getIDLFor($em,$country, $groupName, $idlName);

    assert($idl!=null);
    assert(sizeof($idl->getFields())!=0);
    assert(sizeof($idl->getFields())==sizeof($fields));


}

/**
 * Remove Vendors
 *
 */

$vendors = getGroupFor($entityManager, 'PA', 'Vendors');
if (isset($vendors)){
    foreach($vendors->getDescriptors() as $descriptor){
        foreach($descriptor->getFields() as $field){
            $entityManager->remove($field);
            $entityManager->flush();
        }

        $entityManager->remove($descriptor);
        $entityManager->flush();

    }
    $entityManager->remove($vendors);
    $entityManager->flush();
}

$vendors = new IDLGroupDescriptor();
$vendors->setCountry('PA');
$vendors->setShortName('Vendors');
$vendors->setOutputPath('01.PAN');

$entityManager->persist($vendors);
$entityManager->flush();


createIDL($entityManager, 'PA', 'Vendors', 'VendorHeader', $vendors, 'vendorHeader',  'VENDOR_HEADER', 'D:\pub\dm_latam\laundry\prototypes\excel-as-json\xlIDLExtracts\VendorHeader.json', true, false);
createIDL($entityManager, 'PA', 'Vendors', 'VendorSite', $vendors, 'vendorSite',  'VENDOR_SITE', 'D:\pub\dm_latam\laundry\prototypes\excel-as-json\xlIDLExtracts\VendorSite.json', false, false);
createIDL($entityManager, 'PA', 'Vendors', 'VendorSiteContact', $vendors, 'vendorSiteContact',  'VENDOR_SITE_CONTACT', 'D:\pub\dm_latam\laundry\prototypes\excel-as-json\xlIDLExtracts\VendorSiteContact.json', false, false);
createIDL($entityManager, 'PA', 'Vendors', 'VendorBankAccount', $vendors, 'vendorBankAccount',  'VENDOR_BANK_ACCOUNT', 'D:\pub\dm_latam\laundry\prototypes\excel-as-json\xlIDLExtracts\VendorBankAccount.json', false, false);



/**
 * Remove Inventory
 *
 */

$inventory = getGroupFor($entityManager, 'PA', 'Inventory');
if (isset($inventory)){
    foreach($inventory->getDescriptors() as $descriptor){
        foreach($descriptor->getFields() as $field){
            $entityManager->remove($field);
            $entityManager->flush();
        }

        $entityManager->remove($descriptor);
        $entityManager->flush();

    }
    $entityManager->remove($vendors);
    $entityManager->flush();
}

$inventory = new IDLGroupDescriptor();
$inventory->setCountry('PA');
$inventory->setShortName('Inventory');
$inventory->setOutputPath('01.PAN');

$entityManager->persist($inventory);
$entityManager->flush();


createIDL($entityManager, 'PA', 'Inventory', 'ItemHeader', $inventory, 'itemHeader',  'ITEM_HEADER', 'D:\pub\dm_latam\laundry\prototypes\excel-as-json\xlIDLExtracts\ItemHeader.json', true, false);
createIDL($entityManager, 'PA', 'Inventory', 'ItemCategory', $inventory, 'itemCategory',  'ITEM_CATEGORY', 'D:\pub\dm_latam\laundry\prototypes\excel-as-json\xlIDLExtracts\ItemCategory.json', false, false);
createIDL($entityManager, 'PA', 'Inventory', 'ItemCrossRef', $inventory, 'itemCrossRef',  'ITEM_CROSSREF', 'D:\pub\dm_latam\laundry\prototypes\excel-as-json\xlIDLExtracts\ItemCrossRef.json', false, false);
createIDL($entityManager, 'PA', 'Inventory', 'ItemMFGPartNum', $inventory, 'itemMFGPartNum',  'ITEM_MFG_PART_NUM', 'D:\pub\dm_latam\laundry\prototypes\excel-as-json\xlIDLExtracts\ItemMFGPartNum.json', false, false);
createIDL($entityManager, 'PA', 'Inventory', 'ItemTransDefLoc', $inventory, 'itemTransDefLoc',  'ITEM_TRANS_DEF_LOC', 'D:\pub\dm_latam\laundry\prototypes\excel-as-json\xlIDLExtracts\ItemTransDefLoc.json', false, false);
createIDL($entityManager, 'PA', 'Inventory', 'ItemTransDefSubInv', $inventory, 'itemTransDefSubInv',  'ITEM_TRANS_DEF_SUB_INV', 'D:\pub\dm_latam\laundry\prototypes\excel-as-json\xlIDLExtracts\ItemTransDefSubInv.json', false, false);




