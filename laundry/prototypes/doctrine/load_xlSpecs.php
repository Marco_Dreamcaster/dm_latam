<?php
/**
 * Created by PhpStorm.
 * User: Dreamcaster
 * Date: 3/23/2018
 * Time: 10:40 AM
 */

// list_projectHeaders.php
require_once "../bootstrap.php";

Use Commons\IDL\IDLGroupDescriptor;
Use Commons\IDL\IDLDescriptor;
Use Commons\IDL\IDLFieldDescriptor;

function createIDL($em, $country, $groupName, $idlName, $group, $templateName, $tableName, $fileName, $isHeader, $isTransactional){

    $idl = new IDLDescriptor();
    $idl->setShortName($idlName);
    $idl->setTemplateName($templateName);
    $idl->setTableName($tableName);
    $idl->setGroup($group);
    $idl->setIsHeader($isHeader);
    $idl->setIsTransactional($isTransactional);

    $em->persist($idl);
    $em->flush();

    $json = file_get_contents($fileName);
    $fields = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json), true);

    for($i=0;$i<sizeof($fields);$i++){
        $fieldDescriptor = new IDLFieldDescriptor();
        $fieldDescriptor->setORA_TYPE($fields[$i]['Type']);
        $fieldDescriptor->setDescription($fields[$i]['Comment'].' - '.$fields[$i]['Sample Data']);
        $fieldDescriptor->setIsMandatory($fields[$i]['Mand']=='Y'?1:0);
        $fieldDescriptor->setColName($fields[$i]['Destination Name']);
        $fieldDescriptor->setIsOutputValue(true);
        $fieldDescriptor->setOutputPos($i+1);
        $fieldDescriptor->setDescriptor($idl);

        $em->persist($fieldDescriptor);
        $em->flush();
    }

    $idl = getIDLFor($em,$country, $groupName, $idlName);

    assert($idl!=null);
    assert(sizeof($idl->getFields())!=0);
    assert(sizeof($idl->getFields())==sizeof($fields));


}

$spec = getGroupFor($entityManager, 'PA', 'General Ledger');
if (isset($spec)){
    foreach($spec->getDescriptors() as $descriptor){
        foreach($descriptor->getFields() as $field){
            $entityManager->remove($field);
            $entityManager->flush();
        }

        $entityManager->remove($descriptor);
        $entityManager->flush();

    }
    $entityManager->remove($spec);
    $entityManager->flush();
}


$spec = new IDLGroupDescriptor();
$spec->setCountry('PA');
$spec->setShortName('GeneralLedger');
$spec->setOutputPath('01.PAN');

$entityManager->persist($spec);
$entityManager->flush();


createIDL($entityManager,
    'PA',
    'GeneralLedger',
    'GeneralLedger',
    $spec,
    'GeneralLedger',
    'GENERAL_LEDGER',
    'D:\pub\dm_latam\laundry\prototypes\excel-as-json\TKE_LA_IDL_FD_01_GL_FLAT_FILE_PJ.json',
    false,
    true);
