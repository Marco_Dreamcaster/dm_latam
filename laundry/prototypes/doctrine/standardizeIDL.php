<?php
/**
 * Created by PhpStorm.
 * User: Dreamcaster
 * Date: 3/23/2018
 * Time: 10:40 AM
 */

// list_projectHeaders.php
require_once "../bootstrap.php";

Use Commons\IDL\IDLGroupDescriptor;
Use Commons\IDL\IDLDescriptor;
Use Commons\IDL\IDLFieldDescriptor;

function standardizeIDLFieldShortNames($em){

    $queryBuilder = $em->createQueryBuilder();

    $queryBuilder->select('fd')
        ->from('Commons\\IDL\\IDLFieldDescriptor', 'fd')
        ->orderBy('fd.outputPos', 'ASC');

    $fields = $queryBuilder->getQuery()->getResult();

    foreach($fields as $f){
        if ($f->getShortName()!=$f->getSuggestedShortName()){
            $f->setShortName($f->getSuggestedShortName());

        }
        $f->setORA_TYPE(str_replace('VARCHAR2 ', 'VARCHAR2', $f->getORA_TYPE()));
        print_r('........'.$f->getShortName().' - '.$f->getColName().' - '.$f->getSuggestedShortName().' - '.$f->getORA_TYPE());

        $em->persist($f);

    }
    $em->flush();

}

standardizeIDLFieldShortNames($entityManager);




