<?php

function cloneByReflection($source, $target){
    $srcRfx = new ReflectionClass($source);
    $targetRfx = new ReflectionClass($target);

    foreach($srcRfx->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED) as $prop){
        if (($prop->getName()!="id")&&
            ($prop->getName()!="header")&&
            ($prop->getName()!="recommended")&&
            ($prop->getName()!="action")&&
            ($prop->getName()!="cleansed")&&
            ($prop->getName()!="dismissed")&&
            ($prop->getName()!="ranking")&&
            ($prop->getName()!="locallyValidated")&&
            ($prop->getName()!="crossRefValidated")&&
            ($prop->getName()!="mergedId")&&
            ($prop->getName()!="sameCountryParent")&&
            ($prop->getName()!="childless")&&

            ($targetRfx->hasMethod("set".ucfirst($prop->getName())))){
            $getter = $srcRfx->getMethod("get".ucfirst($prop->getName()));
            $setter = $targetRfx->getMethod("set".ucfirst($prop->getName()));

            $setter->invoke($target, $getter->invoke($source));


        }
    }

    return $target;
}


function moveChildrenToStage($em, $childPreClass, $childClass, $parentKeyName, $parentKeyValue){
    $qb = $em->createQueryBuilder();
    $qb->select('src')
        ->from($childPreClass, 'src');

    foreach($parentKeyName as $key){
        $qb->where($qb->expr()->eq('src.'.$key, '?1'));
    }

    $i=1;
    foreach($parentKeyValue as $value){
        $qb->setParameter($i, $parentKeyValue);
        $i++;
    }

    $srcArray = $qb->getQuery()->getResult();

    foreach($srcArray as $src){
        $target = new $childClass;
        $target = cloneByReflection($src, $target);
        $target->setDismissed(false);

        $em->persist($target);
        $em->flush();

        $src->setCleansed(true);
        $em->persist($src);
        $em->flush();
    }


}

Use Commons\AuditLog;

function logToAudit($em, $country, $user, $event, $count = null, $type = null){
    echo $country.' - '.$user. ' - '.$type. ' - '.$event. "\r\n";

    $auditLog = new AuditLog($country,$user, $event, $count, $type);
    $em->persist($auditLog);
    $em->flush();
}


function patch($em)
{
    $bulkServices = array_map('str_getcsv', file('bulkServices.csv'));

    foreach ($bulkServices as $svc) {

        logToAudit($em, 'CO', 'patch', 'patch cleansing preSTG service for ' . $svc[0], null, 'INFO');

        try{
            $qb = $em->createQueryBuilder();
            $qb->select('pch')
                ->from('Commons\\RAW\\PreProjectHeader', 'pch')
                ->where($qb->expr()->eq('pch.siglaPrimaryKey', '?1'))
                ->setParameter(1, $svc[0]);

            $pre = $qb->getQuery()->getSingleResult();

            logToAudit($em, 'CO', 'patch', 'found preSTG service for ' . $svc[0] . ' -> ' . $pre->getId(), null, 'DEBUG');

            $qb = $em->createQueryBuilder();
            $qb->select('ph')
                ->from('Commons\\ORA\\ProjectHeader', 'ph')
                ->where($qb->expr()->eq('ph.siglaPrimaryKey', '?1'))
                ->setParameter(1, $svc[0]);

            $stg = $qb->getQuery()->getResult();

            if (sizeof($stg)>0){
                logToAudit($em, 'CO', 'patch', 'service ' . $svc[0] . ' already cleansed ', null, 'ERROR');

            }else{
                $stg = 'Commons\\ORA\\ProjectHeader';
                $stg = new $stg;
                $stg = cloneByReflection($pre, $stg);
                $stg->setDismissed(false);

                $em->persist($stg);
                $em->flush();

                $pre->setCleansed(true);
                $em->flush();

                moveChildrenToStage( $em, 'Commons\\RAW\\PreProjectTasks', 'Commons\\ORA\\ProjectTasks', array('projectName'), array($stg->getProjectName()) );
                moveChildrenToStage( $em, 'Commons\\RAW\\PreProjectClassifications', 'Commons\\ORA\\ProjectClassifications', array('projectName'), array($stg->getProjectName()) );
                moveChildrenToStage( $em, 'Commons\\RAW\\PreProjectRetRules', 'Commons\\ORA\\ProjectRetRules', array('projectName'), array($stg->getProjectName()) );

                logToAudit($em, 'CO', 'patch', 'cleansed service ' . $svc[0] . ' -> ' . $pre->getId(), null, 'CLEANSING');

            }


        }catch(\Exception $e){
            logToAudit($em, 'CO', 'patch', 'could not find preSTG service for ' . $svc[0] . ' - '.$e->getMessage(), null, 'ERROR');

        }



    }
}


$env = getenv('LAUNDRY_ENVIRONMENT');
define('LAUNDRY_ENVIRONMENT', isset($env) ? getenv('LAUNDRY_ENVIRONMENT') : 'development');

if(LAUNDRY_ENVIRONMENT == 'production') {
    define('BOOTSTRAP_PATH', 'C:\dm_latam\laundry\prototypes\bootstrap.php');
}else{
    define('BOOTSTRAP_PATH', 'D:\pub\dm_latam\laundry\prototypes\bootstrap.php');
}

require_once(BOOTSTRAP_PATH);

patch($entityManager);




