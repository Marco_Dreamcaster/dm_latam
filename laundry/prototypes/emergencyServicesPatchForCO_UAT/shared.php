<?php
$env = getenv('LAUNDRY_ENVIRONMENT');
define('LAUNDRY_ENVIRONMENT', isset($env) ? getenv('LAUNDRY_ENVIRONMENT') : 'development');

if(LAUNDRY_ENVIRONMENT == 'production') {
    define('BOOTSTRAP_PATH', 'C:\dm_latam\laundry\prototypes\bootstrap.php');
}else{
    define('BOOTSTRAP_PATH', 'D:\pub\dm_latam\laundry\prototypes\bootstrap.php');
}

require_once(BOOTSTRAP_PATH);

Use Doctrine\ORM\Query;

Use Commons\IDL\IDLGroupDescriptor;
Use Commons\IDL\IDLDescriptor;
Use Commons\IDL\IDLFieldDescriptor;
Use Commons\AuditLog;

function checkAllItemFor($em, $country, $flushOnlyOnChange = true){
    $completionMessage = 'bulk local validation triggered - ItemHeader' . ($flushOnlyOnChange?' - flush only on change':' - always flush');
    logToAudit($em, $country, 'scripted', $completionMessage);
    validateLocal($em, 'Commons\\ORA\\ItemHeader', $country,$flushOnlyOnChange);

    $completionMessage = 'bulk local validation triggered - ItemCategory' . ($flushOnlyOnChange?' - flush only on change':' - always flush');
    logToAudit($em, $country, 'scripted', $completionMessage);
    validateLocal($em, 'Commons\\ORA\\ItemCategory', $country,$flushOnlyOnChange);

    $completionMessage = 'bulk local validation triggered - ItemMFGPartNum' . ($flushOnlyOnChange?' - flush only on change':' - always flush');
    logToAudit($em, $country, 'scripted', $completionMessage);
    validateLocal($em, 'Commons\\ORA\\ItemMFGPartNum', $country,$flushOnlyOnChange);

    $completionMessage = 'bulk local validation triggered - ItemCrossRef' . ($flushOnlyOnChange?' - flush only on change':' - always flush');
    logToAudit($em, $country, 'scripted', $completionMessage);
    validateLocal($em, 'Commons\\ORA\\ItemCrossRef', $country,$flushOnlyOnChange);

    $completionMessage = 'bulk local validation triggered - ItemTransDefSubInv' . ($flushOnlyOnChange?' - flush only on change':' - always flush');
    logToAudit($em, $country, 'scripted', $completionMessage);
    validateLocal($em, 'Commons\\ORA\\ItemTransDefSubInv', $country,$flushOnlyOnChange);

    $completionMessage = 'bulk local validation triggered - ItemTransDefLoc' . ($flushOnlyOnChange?' - flush only on change':' - always flush');
    logToAudit($em, $country, 'scripted', $completionMessage);
    validateLocal($em, 'Commons\\ORA\\ItemTransDefLoc', $country,$flushOnlyOnChange);

    $completionMessage = 'bulk crossRef validation triggered - Items' . ($flushOnlyOnChange?' - flush only on change':' - always flush');
    logToAudit($em, $country, 'scripted', $completionMessage);
    validateCrossRef($em, 'Commons\\ORA\\ItemHeader', $country);

    $completionMessage = 'bulk validation completed - Items '. ($flushOnlyOnChange?' - flush only on change':' - always flush');
    logToAudit($em, $country, 'scripted', $completionMessage);
}

function checkAllProjectsFor($em, $country, $flushOnlyOnChange = true){

    $completionMessage = 'bulk local validation triggered - ProjectHeader' . ($flushOnlyOnChange?' - flush only on change':' - always flush');
    logToAudit($em, $country, 'scripted', $completionMessage);
    validateLocal($em, 'Commons\\ORA\\ProjectHeader', $country, $flushOnlyOnChange);

    $completionMessage = 'bulk local validation triggered - ProjectTasks' . ($flushOnlyOnChange?' - flush only on change':' - always flush');
    logToAudit($em, $country, 'scripted', $completionMessage);
    validateLocal($em, 'Commons\\ORA\\ProjectTasks', $country, $flushOnlyOnChange);

    $completionMessage = 'bulk local validation triggered - ProjectClassifications' . ($flushOnlyOnChange?' - flush only on change':' - always flush');
    logToAudit($em, $country, 'scripted', $completionMessage);
    validateLocal($em, 'Commons\\ORA\\ProjectClassifications', $country, $flushOnlyOnChange);

    $completionMessage = 'bulk local validation triggered - ProjectRetRules' . ($flushOnlyOnChange?' - flush only on change':' - always flush');
    logToAudit($em, $country, 'scripted', $completionMessage);
    validateLocal($em, 'Commons\\ORA\\ProjectRetRules', $country, $flushOnlyOnChange);

    $completionMessage = 'bulk crossRef validation triggered - Projects - flush only on change';
    logToAudit($em, $country, 'scripted', $completionMessage);
    validateCrossRef($em, 'Commons\\ORA\\ProjectHeader', $country);

    $completionMessage = 'bulk validation completed - Projects '. ($flushOnlyOnChange?' - flush only on change':' - always flush');
    logToAudit($em, $country, 'scripted', $completionMessage);
}

function checkCrossRefProjectFor($em, $country){
    validateCrossRef($em, 'Commons\\ORA\\ProjectHeader', $country);

}

function checkAllCustomerFor($em, $country, $flushOnlyOnChange = true){

    $completionMessage = 'bulk local validation triggered - CustomerContact' . ($flushOnlyOnChange?' - flush only on change':' - always flush');
    logToAudit($em, $country, 'scripted', $completionMessage);
    validateLocal($em, 'Commons\\ORA\\CustomerContact', $country, $flushOnlyOnChange);

    $completionMessage = 'bulk local validation triggered - CustomerAddress' . ($flushOnlyOnChange?' - flush only on change':' - always flush');
    logToAudit($em, $country, 'scripted', $completionMessage);
    validateLocal($em, 'Commons\\ORA\\CustomerAddress', $country, $flushOnlyOnChange);

    $completionMessage = 'bulk local validation triggered - CustomerHeader' . ($flushOnlyOnChange?' - flush only on change':' - always flush');
    logToAudit($em, $country, 'scripted', $completionMessage);
    validateLocal($em, 'Commons\\ORA\\CustomerHeader', $country, $flushOnlyOnChange);

    $completionMessage = 'bulk crossRef validation triggered - Customers - flush only on change';
    logToAudit($em, $country, 'scripted', $completionMessage);
    validateCrossRef($em, 'Commons\\ORA\\CustomerHeader', $country);

    $completionMessage = 'bulk validation completed - Customers '. ($flushOnlyOnChange?' - flush only on change':' - always flush');
    logToAudit($em, $country, 'scripted', $completionMessage);
}

function checkCrossRefCustomerFor($em, $country){
    validateCrossRef($em, 'Commons\\ORA\\CustomerHeader', $country);

}


function checkAllVendorFor($em, $country){
    validateLocal($em, 'Commons\\ORA\\VendorBankAccount', $country);
    validateLocal($em, 'Commons\\ORA\\VendorSiteContact', $country);
    validateLocal($em, 'Commons\\ORA\\VendorSite', $country);
    validateLocal($em, 'Commons\\ORA\\VendorHeader', $country);

    validateCrossRef($em, 'Commons\\ORA\\VendorSite', $country);
    validateCrossRef($em, 'Commons\\ORA\\VendorHeader', $country);

}

function checkCrossRefVendorFor($em, $country){
    validateCrossRef($em, 'Commons\\ORA\\VendorSite', $country);
    validateCrossRef($em, 'Commons\\ORA\\VendorHeader', $country);

}

function logToAudit($em, $country, $user, $event, $count = null){
    echo $country.' - '.$user. ' - '.$event;

    $auditLog = new AuditLog($country,$user, $event, $count);
    $em->persist($auditLog);
    $em->flush();
}


function validateLocal($em, $className, $country, $flushOnlyOnChange = true){
    $qb = $em->createQueryBuilder();
    $qb->select('ch')
        ->from($className, 'ch')
        ->andWhere(
            $qb->expr()->eq('ch.country', '?1'),
            $qb->expr()->eq('ch.dismissed', '?2')
        );

    $instQuery = $qb->getQuery();
    $instQuery->setParameter(1, $country);
    $instQuery->setParameter(2, false);

    $entityList = $instQuery->getResult();
    $total = sizeof($entityList);

    for($i=0;$i<$total;$i++){
        printf("local validated %s %s - %d / %d (%s - %s)\r\n", $country, $className, $i+1, $total, $entityList[$i]->getLocallyValidated() ? "true" : "false", $entityList[$i]->getAction());

        if ($flushOnlyOnChange){
            $previousState = $entityList[$i]->getLocallyValidated();
            if ($previousState!=$entityList[$i]->validate($em)){
                printf("flushing new validation states for %s %s - item %s\r\n", $country, $className, $i+1);
                $em->flush();
            }
        }else{
            $entityList[$i]->validate($em);
            printf("forcing flush of validation states for %s %s - item %s\r\n", $country, $className, $i+1);
            $em->flush();
        }

        $em->detach($entityList[$i]);

    }


}


function validateLocalWithFlush($em, $className, $country){
    $qb = $em->createQueryBuilder();
    $qb->select('ch')
        ->from($className, 'ch')
        ->andWhere(
            $qb->expr()->eq('ch.country', '?1'),
            $qb->expr()->eq('ch.dismissed', '?2')
        );

    $instQuery = $qb->getQuery();
    $instQuery->setParameter(1, $country);
    $instQuery->setParameter(2, false);

    $entityList = $instQuery->getResult();
    $total = sizeof($entityList);

    for($i=0;$i<$total;$i++){
        $entityList[$i]->validate($em);
        $em->merge($entityList[$i]);
        printf("local validated %s %s - %d / %d (%s - %s)\r\n", $country, $className, $i+1, $total, $entityList[$i]->getLocallyValidated() ? "true" : "false", $entityList[$i]->getAction());
    }

    $em->flush();

}


function validateCrossRef($em, $className, $country){
    $qb = $em->createQueryBuilder();
    $qb->select('ch')
        ->from($className, 'ch')
        ->andWhere(
            $qb->expr()->eq('ch.country', '?1'),
            $qb->expr()->eq('ch.dismissed', '?2')

        );

    $instQuery = $qb->getQuery();
    $instQuery->setParameter(1, $country);
    $instQuery->setParameter(2, false);

    $entityList = $instQuery->getResult();

    $total = sizeof($entityList);

    for($i=0;$i<$total;$i++){
        $previousState = $entityList[$i]->getCrossRefValidated();
        printf("crossRef validated %s %s - %d / %d \r\n", $country, $className, $i+1, $total);
        if ($previousState!=$entityList[$i]->validateCrossRef($em, false)){
            $em->merge($entityList[$i]);

            printf("flushing new validation states for %s %s - item %s\r\n", $country, $className, $i+1);
            $em->flush();

        }else{
            $em->detach($entityList[$i]);
        }
    }

}




