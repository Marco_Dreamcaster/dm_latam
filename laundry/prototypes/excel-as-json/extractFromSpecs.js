convertExcel = require('excel-as-json').processFile;

options = {
    sheet:'6',
    isColOriented: false,
    omitEmtpyFields: false

}

convertExcel('D:\\pub\\dm_latam\\laundry\\scripts\\00.COM\\transactional\\09.PurchaseOrders_PO\\01.IDL\\TKE_LATAM_IDL_FD_06_PO_FLAT_FILE.xlsx',
    'xlIDLExtracts\\TKE_LATAM_IDL_FD_06_PO_FLAT_FILE_header.json', options);


options = {
    sheet:'7',
    isColOriented: false,
    omitEmtpyFields: false

}

convertExcel('D:\\pub\\dm_latam\\laundry\\scripts\\00.COM\\transactional\\09.PurchaseOrders_PO\\01.IDL\\TKE_LATAM_IDL_FD_06_PO_FLAT_FILE.xlsx',
    'xlIDLExtracts\\TKE_LATAM_IDL_FD_06_PO_FLAT_FILE_line.json', options);

options = {
    sheet:'8',
    isColOriented: false,
    omitEmtpyFields: false

}

convertExcel('D:\\pub\\dm_latam\\laundry\\scripts\\00.COM\\transactional\\09.PurchaseOrders_PO\\01.IDL\\TKE_LATAM_IDL_FD_06_PO_FLAT_FILE.xlsx',
    'xlIDLExtracts\\TKE_LATAM_IDL_FD_06_PO_FLAT_FILE_ship.json', options);

options = {
    sheet:'9',
    isColOriented: false,
    omitEmtpyFields: false

}

convertExcel('D:\\pub\\dm_latam\\laundry\\scripts\\00.COM\\transactional\\09.PurchaseOrders_PO\\01.IDL\\TKE_LATAM_IDL_FD_06_PO_FLAT_FILE.xlsx',
    'xlIDLExtracts\\TKE_LATAM_IDL_FD_06_PO_FLAT_FILE_dist.json', options);