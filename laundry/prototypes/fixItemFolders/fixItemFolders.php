<?php
// list_projectHeaders.php
require_once "shared.php";

function traverseResourcesFolder($em){
    $path = realpath('D:\pub\dm_latam\laundry\itemFiles\resources');

    $objects = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path), \RecursiveIteratorIterator::SELF_FIRST);

    $myfile = fopen("fixFolders.bat", "w");
    foreach($objects as $name => $object){
        if (is_dir($object)){
            $baseFolder = basename($name);

            if(($baseFolder!='.')&&($baseFolder!='..')){
                if ((substr( $baseFolder, 0, 1 )!='X')&&
                    (substr( $baseFolder, 0, 2 )!='3W')&&
                    (substr( $baseFolder, 0, 2 )!='3X')&&
                    (substr( $baseFolder, 0, 2 )!='3Y')&&
                    (substr( $baseFolder, 0, 2 )!='3Z')&&
                    (substr( $baseFolder, 0, 2 )!='LA')){

                    $qb = $em->createQueryBuilder();
                    $qb->select('ih')
                        ->from('Commons\\ORA\\ItemHeader', 'ih')
                        ->andWhere(
                            $qb->expr()->like('ih.origItemNumber', '?1')
                        );

                    $instQuery = $qb->getQuery();
                    $instQuery->setParameter(1, '%'.$baseFolder.'%');

                    $entityList = $instQuery->getResult();

                    foreach($entityList as $e){
                        if (substr( $e->getItemNumber(), 0, 2 )=='LA'){
                            $renamedFolder = str_replace($baseFolder, $e->getItemNumber(), $name);

                            if (is_dir($name)){
                                fwrite($myfile, 'mv '.$name.' '.$renamedFolder. PHP_EOL);

                            }

                        }
                    }

                }
            }
        }
    }
    fclose($myfile);

}

traverseResourcesFolder($entityManager);