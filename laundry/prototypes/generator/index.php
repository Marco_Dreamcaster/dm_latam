<?php
Use ORADTO\ProjectTask;

// list_projectHeaders.php
require_once "../doctrine/bootstrap.php";

$projectTaskRepository = $entityManager->getRepository('ORADTO\\ProjectTask');
$projectTasks = $projectTaskRepository->findAll();

echo json_encode($projectTasks);