<?php
require_once "../bootstrap.php";

function deleteDirectory($dir) {
    if (!file_exists($dir)) {
        return true;
    }

    if (!is_dir($dir)) {
        return unlink($dir);
    }

    foreach (scandir($dir) as $item) {
        if ($item == '.' || $item == '..') {
            continue;
        }

        if (!deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
            return false;
        }

    }

    return rmdir($dir);
}

//function defination to convert array to xml
function array_to_xml($array, &$xml_user_info) {
    foreach($array as $key => $value) {
        if(is_array($value)) {
            if(!is_numeric($key)){
                $subnode = $xml_user_info->addChild("$key");
                array_to_xml($value, $subnode);
            }else{
                $subnode = $xml_user_info->addChild("item$key");
                array_to_xml($value, $subnode);
            }
        }else {
            $xml_user_info->addChild("$key",htmlspecialchars("$value"));
        }
    }
}

$fldOutput = './output';

if (!file_exists($fldOutput)) {
    mkdir($fldOutput, 0777, true);
}else{
    deleteDirectory($fldOutput);
    mkdir($fldOutput, 0777, true);
}


$groups = array(
/*[   'targetFile'=>'./setupCustomers.xml',
    'tag'=>'Customers',
    'originalFile'=>'./setupCustomers_old.xml',
    'defaultLocation'=>'00.COM/master/01.Customers/02.development'],*/

[   'targetFile'=>'./setupVendors.xml',
    'tag'=>'Vendors',
    'originalFile'=>'./setupVendors_old.xml',
    'defaultLocation'=>'00.COM/master/02.Vendors/02.development']
);

foreach($groups as $group){
    $originalSetup = array();

    $originalSetup = json_decode(json_encode(simplexml_load_file($group['originalFile'])),true);


    $fScriptArray = array();
    foreach($originalSetup as $key=>$steps){
        $fScript = fopen($fldOutput.'/'.$key.'.sql', "w");
        foreach ($steps['step'] as $step){
            fwrite($fScript, str_replace('&gt;','>',str_replace('&lt;','<',$step)));
            fwrite($fScript, "GO\n");

        }
        fwrite($fScript, "INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)\n");
        fwrite($fScript, "VALUES('ZZ', 'script', 'updated ".$key." procedure/function/table definition', GETDATE());\n");
        fwrite($fScript, "GO\n");
        fclose($fScript);

        $fScriptArray[$key]=array(
            'location'=>$group['defaultLocation'],
            'script'=>$key.'.sql'
        );
    }

    $xml_user_info = new SimpleXMLElement("<?xml version=\"1.0\"?><" . $group['tag']. "></". $group['tag'].">");

    array_to_xml($fScriptArray,$xml_user_info);

    $xml_file = $xml_user_info->asXML($fldOutput.$group['targetFile']);

}



