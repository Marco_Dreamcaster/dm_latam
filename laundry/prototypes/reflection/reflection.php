<?php
/**
 * Created by PhpStorm.
 * User: Dreamcaster
 * Date: 3/22/2018
 * Time: 8:27 PM
 */

Use Commons\PA\ORA\ProjectHeader;
Use Commons\PA\Raw\PreProjectHeader;

require('../../vendor/autoload.php');

assert_options(ASSERT_ACTIVE, 1);
assert_options(ASSERT_WARNING, 0);
assert_options(ASSERT_QUIET_EVAL, 1);

// Create a handler function
function my_assert_handler($file, $line, $code)
{
    echo "<hr>Assertion Failed:
        File '$file'<br />
        Line '$line'<br />
        Code '$code'<br /><hr />";
}

// Set up the callback
assert_options(ASSERT_CALLBACK, 'my_assert_handler');

$obj1 = new ProjectHeader();

$obj1->setTemplateName('teste1');

$obj1_vars = get_object_vars($obj1);

/**
 * http://php.net/manual/en/reflectionclass.hasmethod.php
 *
 * http://php.net/manual/en/function.assert.php
 *
 */

$rc = new ReflectionClass("Commons\PA\ORA\ProjectHeader");

assert($rc!=null);

$props   = $rc->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED);

foreach ($props as $prop) {
    print $prop->getName() . "\n";
}

$rc = new ReflectionClass($obj1);

assert($rc!=null);

$obj2 = new PreProjectHeader();

$reflect = new ReflectionClass($obj2);

foreach($reflect->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED) as $prop)
{
    print $prop->getName();

}

$pre = new PreProjectHeader();
$pre->setTemplateName("copy between PRE and STG entities");

$stg = new ProjectHeader();

$srcRfx = new ReflectionClass($pre);
$targetRfx = new ReflectionClass($stg);

foreach($srcRfx->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED) as $prop){
    if (($prop->getName()!="id")&&($targetRfx->hasMethod("set".ucfirst($prop->getName())))){
        $getter = $srcRfx->getMethod("get".ucfirst($prop->getName()));
        $setter = $targetRfx->getMethod("set".ucfirst($prop->getName()));

        $setter->invoke($stg, $getter->invoke($pre));

    }
}

assert($pre->getTemplateName()==$pre->getTemplateName());




?>