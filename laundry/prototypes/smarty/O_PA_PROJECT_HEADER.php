<?php
use Doctrine\ORM\Query;

require('../../vendor/autoload.php');

$smarty = new Smarty();

$smarty->setTemplateDir('./templates');
$smarty->setCompileDir('./templates_c');
$smarty->setCacheDir('./cache');
$smarty->setConfigDir('./configs');

require_once "../doctrine/bootstrap.php";

$query = $entityManager->createQuery('SELECT ph FROM ORADTO\\PreProjectHeader ph');
$projects = $query->getResult(Query::HYDRATE_ARRAY);


$smarty->assign('PFF', $projects);
$content =  $smarty->fetch('O_PA_PROJECT_HEADER.tpl');

date_default_timezone_set('America/Sao_Paulo');
$date = date('Ymd_His', time());

$outputPath = "/output/";
$fileTemplate = "TKE_PAN_CD_02_%timestamp%_ProjectHeader.txt";
$fileTemplate = str_replace("%timestamp%", $date, $fileTemplate);

$outputPath = "./output/".$fileTemplate;

$outputfile = fopen($outputPath, "w") or die("Unable to open file!");

fwrite($outputfile, utf8_encode ($content));
fclose($outputfile);

$smarty->display('O_PA_PROJECT_HEADER.tpl');