<?php
use Doctrine\ORM\Query;

require('../../vendor/autoload.php');

$smarty = new Smarty();

$smarty->setTemplateDir('./templates');
$smarty->setCompileDir('./templates_c');
$smarty->setCacheDir('./cache');
$smarty->setConfigDir('./configs');

require_once "../doctrine/bootstrap.php";

$descriptorIDLGroupRepository = $entityManager->getRepository('Commons\IDLGroupDescriptor');
$groups = $descriptorIDLGroupRepository->findAll();

foreach ($groups as $g) {
    echo sprintf("%s\n", $g->getShortName());

    $currentDescriptors = $g->getDescriptors();

    foreach($currentDescriptors as $d){
        echo sprintf( "%s - ", $d->getShortName());
    }
}

/**
$smarty->assign('rn', "\r\n");
$smarty->assign('projects', $projects);
$content =  $smarty->fetch('O_PA_PROJECT_HEADER.tpl');

date_default_timezone_set('America/Sao_Paulo');
$date = date('Ymd_His', time());

$outputPath = "/output/";
$fileTemplate = "TKE_PAN_CD_02_%timestamp%_ProjectHeader.txt";
$fileTemplate = str_replace("%timestamp%", $date, $fileTemplate);

$outputPath = "./output/".$fileTemplate;

$outputfile = fopen($outputPath, "w") or die("Unable to open file!");

fwrite($outputfile, utf8_encode ($content));
fclose($outputfile);

$smarty->display('O_PA_PROJECT_HEADER.tpl');
 **/