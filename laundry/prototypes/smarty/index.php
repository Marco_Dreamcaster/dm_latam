<?php

/**
 * https://www.smarty.net/docs/en/
 *
 */
require('../../vendor/autoload.php');

$smarty = new Smarty();

$smarty->setTemplateDir('./templates');
$smarty->setCompileDir('./templates_c');
$smarty->setCacheDir('./cache');
$smarty->setConfigDir('./configs');

$smarty->assign('name', 'TenetO');
$smarty->display('hello.tpl');

?>