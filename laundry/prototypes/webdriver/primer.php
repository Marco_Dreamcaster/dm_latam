<?php

// list_projectHeaders.php
require_once "../bootstrap.php";

Use Facebook\WebDriver\Remote\RemoteWebDriver;
Use Facebook\WebDriver\Remote\DesiredCapabilities;

// This would be the url of the host running the server-standalone.jar
$host = 'http://localhost:4444/wd/hub'; // this is the default

$driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());
$driver->get("http://www.google.com");

// The HTML Source code
$html = $driver->getPageSource();

println($html);
