IF OBJECT_ID('dbo.getLastGLParamChange', 'P') IS NOT NULL
DROP PROCEDURE dbo.getLastGLParamChange;
GO

CREATE PROCEDURE dbo.getLastGLParamChange(@country varchar(2))  
AS  
BEGIN  
	SELECT TOP 1 b.* FROM COM_AUDIT_LOGS b
	WHERE
	b.TYPE='GLP_CHANGE' AND
	b.COUNTRY=@country
	ORDER BY id DESC

	RETURN;

END;  
GO  

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated getLastGLParamChange procedure definition', GETDATE());
