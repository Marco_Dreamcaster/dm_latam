IF OBJECT_ID('dbo.udf_GetAveriasDate', 'P') IS NOT NULL
DROP PROCEDURE dbo.udf_GetAveriasDate;
GO

CREATE PROCEDURE dbo.udf_GetAveriasDate(@country varchar(2))  
AS  
BEGIN  
	If @country = 'PA'
		SELECT CONVERT(date,'31-10-2019',103) AS FECHA

	Else If @country = 'CO'
		SELECT CONVERT(date,'31-01-2019',103) AS FECHA

	Else If @country = 'MX'
		SELECT CONVERT(date,'06-05-2019',103) AS FECHA

	Else If @country = 'SV'
		SELECT CONVERT(date,'06-05-2019',103) AS FECHA

	Else If @country = 'NI'
		SELECT CONVERT(date,'06-05-2019',103) AS FECHA

	Else If @country = 'HN'
		SELECT CONVERT(date,'06-05-2019',103) AS FECHA

	Else If @country = 'GT'
		SELECT CONVERT(date,'06-05-2019',103) AS FECHA

	Else If @country = 'CR'
		SELECT CONVERT(date,'06-05-2019',103) AS FECHA

	Else If @country = 'AR'
		SELECT CONVERT(date,'26-07-2019',103) AS FECHA

	Else If @country = 'PE'
		SELECT CONVERT(date,'26-07-2019',103) AS FECHA

	Else If @country = 'CL'
		SELECT CONVERT(date,'26-07-2019',103) AS FECHA

	Else If @country = 'PY'
		SELECT CONVERT(date,'26-07-2019',103) AS FECHA

	Else If @country = 'UY'
		SELECT CONVERT(date,'26-07-2019',103) AS FECHA

	Else
		SELECT @country + ' NOT CONFIGURED' AS FECHA

	RETURN;

END;  
GO  

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated udf_GetAveriasDate procedure definition', GETDATE());


IF OBJECT_ID('dbo.udf_GetSnapshotDate', 'P') IS NOT NULL
DROP PROCEDURE dbo.udf_GetSnapshotDate;
GO

CREATE PROCEDURE dbo.udf_GetSnapshotDate(@country varchar(2))  
AS  
BEGIN  
	BEGIN TRY 
		EXEC dbo.udf_GetAveriasDate @country
	END TRY 
	BEGIN CATCH  
		SELECT ERROR_MESSAGE() AS FECHA;  
	END CATCH;  

	RETURN;

END;  
GO  

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated udf_GetSnapshotDate procedure definition', GETDATE());
