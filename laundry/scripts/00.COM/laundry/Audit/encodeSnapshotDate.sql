IF OBJECT_ID('dbo.udf_EncodeSnapshotDate', 'FN') IS NOT NULL
	DROP FUNCTION dbo.udf_EncodeSnapshotDate;
GO

CREATE FUNCTION dbo.udf_EncodeSnapshotDate ( @country VARCHAR(2) )
RETURNS VARCHAR(25)
AS
BEGIN

	DECLARE @snapshotDate DATE

	If @country = 'PA'
		SET @snapshotDate = CONVERT(date,'31-10-2019',103)

	Else If @country = 'CO'
		SET @snapshotDate = CONVERT(date,'31-01-2019',103)

	Else If @country = 'MX'
		SET @snapshotDate = CONVERT(date,'06-05-2019',103)

	Else If @country = 'SV'
		SET @snapshotDate = CONVERT(date,'06-05-2019',103)

	Else If @country = 'NI'
		SET @snapshotDate = CONVERT(date,'06-05-2019',103)

	Else If @country = 'GT'
		SET @snapshotDate = CONVERT(date,'06-05-2019',103)

	Else If @country = 'CR'
		SET @snapshotDate = CONVERT(date,'06-05-2019',103)

	Else If @country = 'HN'
		SET @snapshotDate = CONVERT(date,'06-05-2019',103)

	Else If @country = 'SV'
		SET @snapshotDate = CONVERT(date,'06-05-2019',103)

	Else If @country = 'CL'
		SELECT TOP 1 @snapshotDate=b.FECHA FROM TKCL_LATEST..AVERIAS b ORDER BY idAveria DESC
		
	Else If @country = 'AR'
		SELECT TOP 1 @snapshotDate=b.FECHA FROM TKAR_LATEST..AVERIAS b ORDER BY idAveria DESC
		
	Else If @country = 'PE'
		SELECT TOP 1 @snapshotDate=b.FECHA FROM TKPE_LATEST..AVERIAS b ORDER BY idAveria DESC	
		
	Else If @country = 'PY'
		SELECT TOP 1 @snapshotDate=b.FECHA FROM TKPY_LATEST..AVERIAS b ORDER BY idAveria DESC	

	Else If @country = 'UY'
		SELECT TOP 1 @snapshotDate=b.FECHA FROM TKUY_LATEST..AVERIAS b ORDER BY idAveria DESC	
	

	Else
		return cast('Unconfigured Country.' as varchar(25));


	RETURN 	convert(varchar(25), @snapshotDate, 120);

END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated udf_EncodeSnapshotDate function definition', GETDATE());

