-- run this script to update IDL definitions from PROD to DEV
-- make sure you're connected to DEV database 

BEGIN TRANSACTION

ALTER TABLE COM_IDL_FIELD NOCHECK CONSTRAINT all
ALTER TABLE COM_IDL NOCHECK CONSTRAINT all
ALTER TABLE COM_IDL_GROUP NOCHECK CONSTRAINT all

DELETE FROM COM_IDL_FIELD
DELETE FROM COM_IDL
DELETE FROM COM_IDL_GROUP

ALTER TABLE COM_IDL_FIELD WITH CHECK CHECK CONSTRAINT all
ALTER TABLE COM_IDL WITH CHECK CHECK CONSTRAINT all
ALTER TABLE COM_IDL_GROUP WITH CHECK CHECK CONSTRAINT all

COMMIT