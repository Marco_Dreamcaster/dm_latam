IF OBJECT_ID('dbo.runSampleKPI', 'P') IS NOT NULL
  DROP PROCEDURE dbo.runSampleKPI;
GO

IF OBJECT_ID('dbo.sampleKPI', 'P') IS NOT NULL
  DROP PROCEDURE dbo.sampleKPI;
GO

CREATE PROCEDURE dbo.sampleKPI
(
	@countryCode VARCHAR(2)
)
AS  
BEGIN
		Declare @sampledAt datetime = GETDATE();

		INSERT INTO COM_AUDIT_LOGS(
			STAMP,
			"EVENT",
			COUNTRY,
			"COUNT",
			"USER_ID")
		VALUES(
			@sampledAt,
			'sampleKPI - CustomerHeader',
			@countryCode,
			(SELECT COUNT(*) FROM O_STG_CUSTOMER_HEADER WHERE COUNTRY=@countryCode),
			'script')

		INSERT INTO COM_AUDIT_LOGS(
			STAMP,
			"EVENT",
			COUNTRY,
			"COUNT",
			"USER_ID")
		VALUES(
			@sampledAt,
			'sampleKPI - CustomerHeader - target',
			@countryCode,
			(SELECT COUNT(*) FROM O_PRE_CUSTOMER_HEADER WHERE COUNTRY=@countryCode AND RECOMMENDED=1),
			'script')

		INSERT INTO COM_AUDIT_LOGS(
			STAMP,
			"EVENT",
			COUNTRY,
			"COUNT",
			"USER_ID")
		VALUES(
			@sampledAt,
			'sampleKPI - VendorHeader',
			@countryCode,
			(SELECT COUNT(*) FROM O_STG_VENDOR_HEADER WHERE COUNTRY=@countryCode),
			'script')

		INSERT INTO COM_AUDIT_LOGS(
			STAMP,
			"EVENT",
			COUNTRY,
			"COUNT",
			"USER_ID")
		VALUES(
			@sampledAt,
			'sampleKPI - VendorHeader - target',
			@countryCode,
			(SELECT COUNT(*) FROM O_PRE_VENDOR_HEADER WHERE COUNTRY=@countryCode AND RECOMMENDED=1),
			'script')

		INSERT INTO COM_AUDIT_LOGS(
			STAMP,
			"EVENT",
			COUNTRY,
			"COUNT",
			"USER_ID")
		VALUES(
			@sampledAt,
			'sampleKPI - ProjectHeader',
			@countryCode,
			(SELECT COUNT(*) FROM O_STG_PROJECT_HEADER WHERE COUNTRY=@countryCode),
			'script')

		INSERT INTO COM_AUDIT_LOGS(
			STAMP,
			"EVENT",
			COUNTRY,
			"COUNT",
			"USER_ID")
		VALUES(
			@sampledAt,
			'sampleKPI - ProjectHeader - target',
			@countryCode,
			(SELECT COUNT(*) FROM O_PRE_PROJECT_HEADER WHERE COUNTRY=@countryCode AND RECOMMENDED=1),
			'script')

		INSERT INTO COM_AUDIT_LOGS(
			STAMP,
			"EVENT",
			COUNTRY,
			"COUNT",
			"USER_ID")
		VALUES(
			@sampledAt,
			'sampleKPI - ItemHeader',
			@countryCode,
			(SELECT COUNT(*) FROM O_STG_ITEM_HEADER WHERE COUNTRY=@countryCode),
			'script')

		INSERT INTO COM_AUDIT_LOGS(
			STAMP,
			"EVENT",
			COUNTRY,
			"COUNT",
			"USER_ID")
		VALUES(
			@sampledAt,
			'sampleKPI - ItemHeader - target',
			@countryCode,
			(SELECT COUNT(*) FROM O_PRE_ITEM_HEADER WHERE COUNTRY=@countryCode AND RECOMMENDED=1),
			'script')

		INSERT INTO COM_AUDIT_LOGS(
			STAMP,
			"EVENT",
			COUNTRY,
			"COUNT",
			"USER_ID")
		VALUES(
			@sampledAt,
			'sampleKPI - ItemHeader - dirtyRecommended AKA unstagedRecommended',
			@countryCode,
			(SELECT COUNT(*) FROM O_PRE_ITEM_HEADER WHERE COUNTRY=@countryCode AND RECOMMENDED=1 AND CLEANSED=0),
			'script')

		INSERT INTO COM_AUDIT_LOGS(
			STAMP,
			"EVENT",
			COUNTRY,
			"COUNT",
			"USER_ID")
		VALUES(
			@sampledAt,
			'sampleKPI - ItemHeader - cleansedRecommended AKA stagedRecommended',
			@countryCode,
			(SELECT COUNT(*) FROM O_PRE_ITEM_HEADER WHERE COUNTRY=@countryCode AND RECOMMENDED=1 AND CLEANSED=1),
			'script')


END;
GO

CREATE PROCEDURE dbo.runSampleKPI
AS  
BEGIN
	EXEC dbo.sampleKPI 'PA';
	EXEC dbo.sampleKPI 'CO';
	EXEC dbo.sampleKPI 'MX';
	EXEC dbo.sampleKPI 'SV';
	EXEC dbo.sampleKPI 'HN';
	EXEC dbo.sampleKPI 'NI';
	EXEC dbo.sampleKPI 'CV';
END;
GO

EXEC dbo.runSampleKPI
GO

SELECT TOP 16 * FROM COM_AUDIT_LOGS ORDER BY STAMP DESC
GO



