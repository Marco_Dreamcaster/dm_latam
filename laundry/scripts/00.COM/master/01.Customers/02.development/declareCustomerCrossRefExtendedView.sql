IF OBJECT_ID('O_STG_CUSTOMER_CROSS_REF_EXT', 'V') IS NOT NULL
DROP VIEW dbo.O_STG_CUSTOMER_CROSS_REF_EXT
GO

CREATE VIEW dbo.O_STG_CUSTOMER_CROSS_REF_EXT
AS
SELECT
a.ORIG_SYSTEM_CUSTOMER_REF AS ORIG_SYSTEM_CUSTOMER_REF,
a.ORACLE_CUSTOMER_REF AS ORACLE_CUSTOMER_REF,
dbo.udf_GetAdjustedCustomerRef(a.ORIG_SYSTEM_CUSTOMER_REF) AS MIGRATED_CUSTOMER_REF,
dbo.udf_GetAdjustedCustomerBillToRef(a.ORIG_SYSTEM_CUSTOMER_REF) AS MIGRATED_CUSTOMER_ADDRESS_BILL_TO_REF,
dbo.udf_GetAdjustedCustomerShipToRef(a.ORIG_SYSTEM_CUSTOMER_REF) AS MIGRATED_CUSTOMER_ADDRESS_SHIP_TO_REF,
dbo.udf_GetAdjustedCustomerDunRef(a.ORIG_SYSTEM_CUSTOMER_REF) AS MIGRATED_CUSTOMER_ADDRESS_DUN_REF,
a.CROSS_REFERENCE_TYPE,
a.CROSS_REFERENCE
FROM O_STG_CUSTOMER_CROSS_REF_ALT a
GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated O_STG_CUSTOMER_CROSS_REF_EXT view definition', GETDATE());
GO
