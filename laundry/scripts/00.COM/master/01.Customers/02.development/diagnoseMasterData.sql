--:setvar country MX
--:setvar cutoffDate 30-04-2019
--:setvar timestamp 201901292109

SET NOCOUNT ON;

DECLARE @cutoffDate DATE;
SET @cutoffDate=CONVERT(date,'$(cutoffDate)',103)

DECLARE @snapshotDate DATE;
SET @snapshotDate=dbo.udf_EncodeSnapshotDate('$(country)')

DECLARE @itemCount INT;

PRINT 'country = $(country)' + CHAR(13)+CHAR(10);
PRINT 'cutoffDate = $(cutoffDate)' +  CHAR(13)+CHAR(10);
PRINT 'snapshotDate = ' + CONVERT(varchar, @snapshotDate, 103) + CHAR(13)+CHAR(10);
PRINT 'timestamp = $(timestamp)';


:r "includes\generateOrgContacts.sql"

PRINT CHAR(13)+CHAR(10) + '---------------- CUSTOMERS WITH ORG_CONTACT EMAIL_ADDRESSES TRIMMED TO 255char ----------------------'+CHAR(13)+CHAR(10);

SELECT
a.ORIG_SYSTEM_CUSTOMER_REF,
a.EMAIL_ADDRESS 
FROM #O_STG_CUSTOMER_ORG_CONTACTS a
WHERE LEN(EMAIL_ADDRESS)>255
AND a.COUNTRY IN ('MX','GT','SV','CR','HN','NI')
