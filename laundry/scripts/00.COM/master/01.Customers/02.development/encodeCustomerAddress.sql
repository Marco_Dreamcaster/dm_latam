IF OBJECT_ID('dbo.udf_EncodeCustomerAddressReference', 'FN') IS NOT NULL
	DROP FUNCTION udf_EncodeCustomerAddressReference;
GO

CREATE FUNCTION dbo.udf_EncodeCustomerAddressReference ( @country VARCHAR(2), @cliente VARCHAR(10) )
RETURNS VARCHAR(25)
AS
BEGIN

DECLARE @customerAddressReference VARCHAR(25)

SET @customerAddressReference = UPPER(@country+RTRIM(LTRIM(@cliente))+'-CA');

RETURN @customerAddressReference
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated udf_EncodeCustomerAddressReference function definition', GETDATE());

