IF OBJECT_ID('dbo.udf_EncodeCustomerReference', 'FN') IS NOT NULL
	DROP FUNCTION dbo.udf_EncodeCustomerReference;
GO

CREATE FUNCTION dbo.udf_EncodeCustomerReference ( @country VARCHAR(2), @cliente VARCHAR(10) )
RETURNS VARCHAR(25)
AS
BEGIN

DECLARE @customerReference VARCHAR(25)

SET @customerReference = UPPER(@country+RTRIM(LTRIM(@cliente))+'-CC');

RETURN @customerReference
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated udf_EncodeCustomerReference function definition', GETDATE());

