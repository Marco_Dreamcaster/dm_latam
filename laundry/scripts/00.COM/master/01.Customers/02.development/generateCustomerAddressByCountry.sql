IF OBJECT_ID('.generateCustomerAddressByCountry', 'P') IS NOT NULL
DROP PROCEDURE dbo.generateCustomerAddressByCountry;
GO

CREATE PROCEDURE dbo.generateCustomerAddressByCountry
(
@country VARCHAR(2)
)
AS
BEGIN
SELECT
a.CUSTOMER_NAME							as customerName,
a.ORIG_SYSTEM_CUSTOMER_REF				as origSystemCustomerRef,
b.ORIG_SYSTEM_ADRESS_REFERENCE			as origSystemAdressReference,
b.SITE_USE_CODE							as siteUseCode,
b.PRIMARY_SITE_USE_FLAG					as primarySiteUseFlag,
b.LOCATION								as location,
b.ADDRESS_LINE1							as addressLine1,
b.ADDRESS_LINE2							as addressLine2,
b.ADDRESS_LINE3							as addressLine3,
b.ADDRESS_LINE4							as addressLine4,
b.ADDRESS_LINES_PHONETIC				as addressLinesPhonetic,
b.CITY									as city,
b.PROVINCE								as province,
b.STATE									as state,
b.COUNTY								as county,
b.POSTAL_CODE							as postalCode,
b.ADDRESS_COUNTRY						as addressCountry,
b.RECEVABLES_ACCOUNT			        as recevablesAccount,
b.REVENUE_ACCOUNT				        as revenueAccount,
b.TAX_ACCOUNT						    as taxAccount,
b.FREIGHT_ACCOUNT				        as freightAccount,
b.CLEARING_ACCOUNT				        as clearingAccount,
b.UNBILLED_RECEIVABLES_ACCOUNT	        as unbilledReceivablesAccount,
b.UNEARNED_REVENUE_ACCOUNT		        as unearnedRevenueAccount,
b.PAYMENT_METHOD_NAME			        as paymentMethodName,
b.PRIMARY_FLAG							as primaryFlag,
b.CUSTOMER_PROFILE						as customerProfile,
b.SITE_USE_TAX_REFERENCE		        as siteUseTaxReference,
b.TIME_ZONE						        as timeZone,
b.LANGUAGE						        as language,
a.FISCAL_CLASS_CODE						as gdfAddressAttribute8,
b.CO_EXT_ATTRIBUTE1						as economicActivity,
dbo.udf_EncodeOrgName (@country)	    as orgName,
b.PAYMENT_TERM_NAME						as paymentTermName,
b.CONCEPT_CODE							as conceptCode,
b.GIRO									as giro
FROM O_STG_CUSTOMER_HEADER a
INNER JOIN O_STG_CUSTOMER_ADDRESS b ON (a.ORIG_SYSTEM_CUSTOMER_REF=b.ORIG_SYSTEM_CUSTOMER_REF)
WHERE
a.COUNTRY=@country AND
a.DISMISSED=0 AND
b.DISMISSED=0 AND
a.LOCAL_VALIDATED=1 AND
a.CROSSREF_VALIDATED=1
ORDER BY
a.ORIG_SYSTEM_CUSTOMER_REF ASC,
b.ORIG_SYSTEM_ADRESS_REFERENCE ASC

END
GO
INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateCustomerAddressByCountry procedure definition', GETDATE());
GO
