IF OBJECT_ID('dbo.generateCustomerHeaderByCountry', 'P') IS NOT NULL
DROP PROCEDURE dbo.generateCustomerHeaderByCountry;

GO

CREATE PROCEDURE dbo.generateCustomerHeaderByCountry
(
@country VARCHAR(2)
)
AS
BEGIN
SELECT
a.CUSTOMER_NAME							as customerName,
a.ORIG_SYSTEM_PARTY_REF					as origSystemPartyRef,
a.ORIG_SYSTEM_CUSTOMER_REF				as origSystemCustomerRef,
a.CUSTOMER_TYPE							as customerType,
a.CUSTOMER_PROFILE						as customerProfile,
a.PAYMENT_METHOD_NAME				    as paymentMethodName,
a.PRIMARY_FLAG							as primaryFlag,
a.GLOBAL_FLAG							as globalFlag,
a.GLOBAL_ATTRIBUTE9						as globalAttribute9,
CASE
WHEN a.COUNTRY='CO' OR a.COUNTRY='CL' OR a.COUNTRY='AR' THEN a.GLOBAL_ATTRIBUTE10
ELSE '' 
END									    as globalAttribute10,
CASE
WHEN a.COUNTRY='CO' OR a.COUNTRY='CL' OR a.COUNTRY='AR' THEN a.GLOBAL_ATTRIBUTE12
ELSE '' 
END									    as globalAttribute12,
a.TAX_PAYER_ID							as taxPayerId,
a.FISCAL_CLASS_CODE					    as fiscalClassCode,
a.FISCAL_CLASS_CATEGORY				    as fiscalClassCategory,
a.FISCAL_CLASS_START_DATE			    as fiscalClassStartDate,
CASE
WHEN a.COUNTRY='CO' THEN a.[CO_EXT_ATTRIBUTE1]
ELSE '' 
END									    as CODocumentType,
CASE
WHEN a.COUNTRY='CO' THEN a.[CO_EXT_ATTRIBUTE2]
ELSE '' 
END									    as COTaxPayerId,
CASE
WHEN a.COUNTRY='CO' THEN a.[CO_EXT_ATTRIBUTE3]
ELSE '' 
END									    as nature,
CASE
WHEN a.COUNTRY='CO' THEN a.[CO_EXT_ATTRIBUTE4]
ELSE '' 
END									    as regimen,
a.[SPECIAL_CUSTOMER]					as specialCustomer,
a.[CLASS_CODE]							as classCode,
a.[CUSTOMER_CLASS_CODE]					as customerClassCode,
a.[PAYMENT_TERM_NAME]					as paymentTermName,
dbo.udf_EncodeOrgName (@country)	    as orgName,
a.ORDER_NUMBER_GT						as orderNumberGt,
a.REGISTER_NUMBER_GT					as registerNumberGt,
a.DOCUMENT_TYPE_SV						as documentTypeSv,
a.CONTRIBUTOR_REGISTRATION_SV			as contributorRegistrationSv,
a.PERSON_CATEGORY_PE					as personCategoryPE,
a.PERSON_TYPE_PE						as personTypePE,
a.DOCUMENT_TYPE_PE						as documentTypePE,
a.DOCUMENT_NUMBER_PE					as documentNumberPE,
a.LAST_NAME_PE							as lastNamePE,
a.SECOND_LAST_NAME_PE					as secondLastNamePE,
a.FIRST_NAME_PE							as firstNamePE,
a.MIDDLE_NAME_PE						as middleNamePE,
a.ORIGIN_ALL							as originAll,
a.NATURE_ALL							as natureAll,
a.DOCUMENT_TYPE_UY                      as documentTypeUY,
a.PERCEPTION_AGENT_PE                   as perceptionAgentPE
FROM O_STG_CUSTOMER_HEADER a
WHERE
a.COUNTRY=@country AND
a.DISMISSED=0 AND
a.LOCAL_VALIDATED=1 AND
a.CROSSREF_VALIDATED=1
ORDER BY
a.ORIG_SYSTEM_CUSTOMER_REF ASC

END
GO
INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateCustomerHeaderByCountry procedure definition', GETDATE());
GO


