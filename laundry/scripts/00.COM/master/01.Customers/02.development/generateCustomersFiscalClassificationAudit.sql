SET NOCOUNT ON;

SELECT
a.CUSTOMER_NAME							 as customerName,
a.ORIG_SYSTEM_PARTY_REF					 as origSystemPartyRef,
a.ORIG_SYSTEM_CUSTOMER_REF				 as origSystemCustomerRef,
SUBSTRING(a.ORIG_SYSTEM_CUSTOMER_REF, 3, LEN(a.ORIG_SYSTEM_CUSTOMER_REF)-5) as codigoLookup,
a.FISCAL_CLASS_CODE						 as fiscalClassificationCode,
a.FISCAL_CLASS_CATEGORY					 as fiscalClassificationCategory,
a.FISCAL_CLASS_START_DATE				 as fiscalClasstificationStartDate
FROM O_STG_CUSTOMER_HEADER a
WHERE
a.COUNTRY='$(country)' AND
a.DISMISSED=0 AND
a.LOCAL_VALIDATED=1 AND
a.CROSSREF_VALIDATED=1
ORDER BY
a.CUSTOMER_NAME ASC

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP, COUNT)
VALUES('$(country)', 'script', 'SQLCMD execution finished for $(country) generateCustomerFiscalClassificationAudit', GETDATE(), @@ROWCOUNT);
