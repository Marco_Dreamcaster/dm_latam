IF OBJECT_ID('dbo.udf_sanitizePhoneNumber', 'FN') IS NOT NULL
	DROP FUNCTION dbo.udf_sanitizePhoneNumber;
GO

CREATE FUNCTION dbo.udf_sanitizePhoneNumber ( @raw VARCHAR(MAX) )
RETURNS VARCHAR(40)
AS
BEGIN
	DECLARE @sanitized VARCHAR(40)
	
	SET @sanitized = REPLACE(@raw,CHAR(160),'');
	SET @sanitized = REPLACE(@sanitized,' ','');
	SET @sanitized = REPLACE(@sanitized,';',',');
	SET @sanitized = REPLACE(@sanitized,'/',',');
	SET @sanitized = LEFT(@sanitized,40);
	SET @sanitized = SUBSTRING(@sanitized,0, LEN(@sanitized)-CHARINDEX(',',REVERSE(LEFT(@sanitized,LEN(@sanitized))))+1)

	RETURN CAST (@sanitized AS VARCHAR(40))
END

GO

--:setvar country PE
--:setvar cutoffDate 30-06-2019
--:setvar timestamp 201901292109


:r "includes\generateOrgContacts.sql"

DECLARE @origSystemCustomerRef VARCHAR(MAX)
DECLARE @emailAddresses VARCHAR(MAX)
	
DECLARE LONG_MXCA_EMAILADDRESSES CURSOR FOR   
SELECT
a.ORIG_SYSTEM_CUSTOMER_REF,
a.EMAIL_ADDRESS 
FROM #O_STG_CUSTOMER_ORG_CONTACTS a
WHERE LEN(EMAIL_ADDRESS)>255
AND a.COUNTRY IN ('MX','GT','SV','CR','HN','NI')

	
OPEN LONG_MXCA_EMAILADDRESSES;

FETCH NEXT FROM LONG_MXCA_EMAILADDRESSES   
INTO @origSystemCustomerRef, @emailAddresses;

WHILE @@FETCH_STATUS = 0
BEGIN
	DECLARE @trimmedEmailAddresses VARCHAR(MAX);

	SET @trimmedEmailAddresses=REPLACE(SUBSTRING(@emailAddresses,0, 256-CHARINDEX(',',REVERSE(LEFT(@emailAddresses,256)))+1),';',',');

	UPDATE #O_STG_CUSTOMER_ORG_CONTACTS SET EMAIL_ADDRESS=@trimmedEmailAddresses WHERE ORIG_SYSTEM_CUSTOMER_REF=@origSystemCustomerRef

	FETCH NEXT FROM LONG_MXCA_EMAILADDRESSES   
	INTO @origSystemCustomerRef, @emailAddresses;

END;

CLOSE LONG_MXCA_EMAILADDRESSES;

DEALLOCATE LONG_MXCA_EMAILADDRESSES;




SELECT 
  a.ORIG_SYSTEM_CUSTOMER_REF,
  RIGHT(REPLACE(a.EMAIL_ADDRESS,';',','),2000),
  dbo.udf_sanitizePhoneNumber(a.PHONE_NUMBER),
  dbo.udf_EncodeOrgName ('$(country)')	    as ORG_NAME
FROM #O_STG_CUSTOMER_ORG_CONTACTS a
--WHERE a.ORIG_SYSTEM_CUSTOMER_REF='CL70995200-5-CC'
ORDER BY
a.ORIG_SYSTEM_CUSTOMER_REF ASC





