            SELECT
                a.ORIG_SYSTEM_CUSTOMER_REF AS MIGRATION_CODE,
                a.CLEANSED AS STAGED,
                b.DISMISSED AS DISMISSED_RECORD,
                b.CROSSREF_VALIDATED AS VALIDATED,
                a.RECOMMENDED AS RFM,
                a.CUSTOMER_NAME AS CUSTOMER_NAME_SIGLA,
                b.CUSTOMER_NAME AS CUSTOMER_NAME_ORACLE,
                b.ALIASES,

                REPLACE(REPLACE(b.OBSERVATION, CHAR(13), ''), CHAR(10), '') AS OBSERVATION
                --b.*
            FROM O_PRE_CUSTOMER_HEADER a
                LEFT JOIN O_STG_CUSTOMER_HEADER b ON a.ORIG_SYSTEM_CUSTOMER_REF=b.ORIG_SYSTEM_CUSTOMER_REF
            WHERE
                a.COUNTRY='PA'
            ORDER BY
                STAGED DESC,
                DISMISSED_RECORD ASC,
                VALIDATED DESC,
                RFM DESC,
                CUSTOMER_NAME_ORACLE ASC
