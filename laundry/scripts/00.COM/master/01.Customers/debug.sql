SELECT a.COUNTRY, a.ORIG_SYSTEM_CUSTOMER_REF, a.EMAIL_ADDRESS, SUBSTRING(a.EMAIL_ADDRESS, 0, LEN(a.EMAIL_ADDRESS))
FROM O_STG_CUSTOMER_CONTACT a
WHERE
SUBSTRING(a.EMAIL_ADDRESS, LEN(a.EMAIL_ADDRESS),1)=';'
ORDER BY
a.COUNTRY

UPDATE O_STG_CUSTOMER_CONTACT SET EMAIL_ADDRESS=SUBSTRING(EMAIL_ADDRESS, 0, LEN(EMAIL_ADDRESS))
WHERE
SUBSTRING(EMAIL_ADDRESS, LEN(EMAIL_ADDRESS),1)=';'




--UPDATE O_STG_CUSTOMER_HEADER SET PAYMENT_METHOD_NAME='HN - CITIBANK MANUAL' WHERE COUNTRY='HN' AND (PAYMENT_METHOD_NAME IS NULL OR LEN(PAYMENT_METHOD_NAME)=0)
--UPDATE O_STG_CUSTOMER_HEADER SET PAYMENT_METHOD_NAME='CR - CITIBANK - USD' WHERE COUNTRY='CR'

--UPDATE O_STG_CUSTOMER_ADDRESS SET PAYMENT_METHOD_NAME='HN - CITIBANK MANUAL' WHERE COUNTRY='HN' AND (PAYMENT_METHOD_NAME IS NULL OR LEN(PAYMENT_METHOD_NAME)=0)
--UPDATE O_STG_CUSTOMER_ADDRESS SET PAYMENT_METHOD_NAME='CR - CITIBANK - USD' WHERE COUNTRY='CR'
--SELECT DISTINCT FISCAL_CLASS_CATEGORY FROM O_STG_CUSTOMER_HEADER WHERE COUNTRY='CR'

--UPDATE O_STG_CUSTOMER_HEADER SET FISCAL_CLASS_CATEGORY='TKE_LATAM_CR_CONTRIBUTOR2' WHERE COUNTRY='CR'

/**
SELECT * FROM GLMONEDAS WHERE COUNTRY='PY'

INSERT INTO GLMONEDAS (Moneda, Descrip, Sigla, Country, Tasa)
VALUES ('G', 'GUARANIES (ISO)', 'PYG', 'PY', '0.0001617');


DELETE FROM GLMONEDAS WHERE ID=70
**/

/**
SELECT ORIG_ITEM_NUMBER, RECOMMENDED, CLEANSED FROM O_PRE_ITEM_HEADER WHERE COUNTRY='PE' ORDER BY RECOMMENDED, CLEANSED

SELECT CLEANSED, RECOMMENDED, * FROM O_PRE_ITEM_HEADER WHERE ORIG_ITEM_NUMBER='PE1000042932'

SELECT ORIG_ITEM_NUMBER,COUNT(*) FROM O_STG_ITEM_HEADER S WHERE S.COUNTRY='PY' GROUP BY ORIG_ITEM_NUMBER ORDER BY COUNT(*) DESC

SELECT * FROM O_STG_ITEM_HEADER WHERE ORIG_ITEM_NUMBER LIKE '%PYX08.038.179%' AND COUNTRY='PY'
SELECT * FROM O_STG_ITEM_CATEGORY WHERE ORIG_ITEM_NUMBER LIKE '%PYX08.038.179%' AND COUNTRY='PY'
SELECT * FROM O_STG_ITEM_MFG_PART_NUM WHERE ORIG_ITEM_NUMBER LIKE '%PYX08.038.179%' AND COUNTRY='PY'
SELECT * FROM O_STG_ITEM_TRANS_DEF_LOC WHERE ORIG_ITEM_NUMBER LIKE '%PYX08.038.179%' AND COUNTRY='PY'
SELECT * FROM O_STG_ITEM_TRANS_DEF_SUB_INV WHERE ORIG_ITEM_NUMBER LIKE '%PYX08.038.179%' AND COUNTRY='PY'

DELETE FROM O_STG_ITEM_HEADER WHERE ID IN (27493,27494)
DELETE FROM O_STG_ITEM_CATEGORY WHERE ID IN (15764,15765)
DELETE FROM O_STG_ITEM_MFG_PART_NUM WHERE ID IN (25973,25974)
DELETE FROM O_STG_ITEM_TRANS_DEF_LOC WHERE ID IN (53652,53653)
DELETE FROM O_STG_ITEM_TRANS_DEF_SUB_INV WHERE ID IN (61620,61621)
**/


--SELECT DISTINCT(PAYMENT_METHOD_NAME), COUNT(*) FROM O_STG_CUSTOMER_HEADER WHERE COUNTRY='PY' GROUP BY PAYMENT_METHOD_NAME

--UPDATE O_STG_CUSTOMER_HEADER SET PAYMENT_METHOD_NAME='PY - ITAU MANUAL' WHERE COUNTRY='PY'

--SELECT DISTINCT(PAYMENT_METHOD_NAME), COUNTRY, COUNT(*) FROM O_STG_CUSTOMER_HEADER GROUP BY PAYMENT_METHOD_NAME,COUNTRY ORDER BY COUNTRY

--SELECT DISTINCT(PAYMENT_METHOD_NAME), COUNTRY, COUNT(*) FROM O_STG_CUSTOMER_ADDRESS GROUP BY PAYMENT_METHOD_NAME,COUNTRY ORDER BY COUNTRY

--SELECT COUNTRY, PAYMENT_METHOD_NAME, COUNT(*) FROM FIX_PMN GROUP BY PAYMENT_METHOD_NAME,COUNTRY ORDER BY COUNTRY

--UPDATE O_STG_CUSTOMER_ADDRESS SET PAYMENT_METHOD_NAME='UY - SANTANDER MANUAL' WHERE COUNTRY='UY'
--UPDATE O_STG_CUSTOMER_ADDRESS SET PAYMENT_METHOD_NAME='PY - ITAU MANUAL' WHERE COUNTRY='PY'

/**
SELECT COUNTRY, PAYMENT_METHOD_NAME, COUNT(*) FROM O_STG_CUSTOMER_HEADER GROUP BY PAYMENT_METHOD_NAME,COUNTRY ORDER BY COUNTRY

UPDATE O_STG_CUSTOMER_HEADER SET PAYMENT_METHOD_NAME='PY - ITAU MANUAL' WHERE COUNTRY='PY'
UPDATE O_STG_CUSTOMER_HEADER SET PAYMENT_METHOD_NAME='CL - SANTANDER MANUAL' WHERE COUNTRY='CL'


MERGE INTO O_STG_CUSTOMER_HEADER T
   USING FIX_PMN S 
      ON T.ORIG_SYSTEM_CUSTOMER_REF = S.ORIG_SYSTEM_CUSTOMER_REF
WHEN MATCHED THEN
   UPDATE 
      SET T.PAYMENT_METHOD_NAME = S.PAYMENT_METHOD_NAME;

SELECT * FROM COM_ORA_GEO_CODES WHERE COUNTRY_CODE='AR'
**/

/**
SELECT * FROM AR_LA_IDL_XD_01_GEO WHERE C_TYPE='CABECERA'




SELECT * FROM AR_LA_IDL_XD_01_GEO WHERE C_TYPE='DEPARTMENT'


SELECT
DISTINCT 'ARGENTINA' AS COUNTRY_NAME,
'AR' as COUNTRY_CODE,
b.P_VALUE AS STATE,
LEFT(b.P_VALUE,10) AS STATE_CODE,
b.P_VALUE AS PROVINCE,
LEFT(b.P_VALUE,10) AS PROVINCE_CODE,
a.P_VALUE AS COUNTY,
LEFT(a.P_VALUE,10) AS COUNTY_CODE,
a.C_VALUE AS CITY,
LEFT(a.C_VALUE,10) as CITY_CODE,
GETDATE() AS LAST_UPDATED,
'AR_LA_IDL_XD_01 Lokesh' AS LAST_SOURCE
FROM AR_LA_IDL_XD_01_GEO a
INNER JOIN AR_LA_IDL_XD_01_GEO b
ON (b.C_VALUE=a.P_VALUE)
WHERE
a.C_TYPE='CABECERA' AND
b.C_TYPE='DEPARTMENT'

DELETE FROM COM_ORA_GEO_CODES WHERE COUNTRY_CODE='AR'

INSERT INTO COM_ORA_GEO_CODES
(
COUNTRY_NAME,
COUNTRY_CODE,
STATE,
STATE_CODE,
PROVINCE,
PROVINCE_CODE,
COUNTY,
COUNTY_CODE,
CITY,
CITY_CODE,
LAST_UPDATED,
LAST_SOURCE)
SELECT
DISTINCT 'ARGENTINA' AS COUNTRY_NAME,
'AR' as COUNTRY_CODE,
b.P_VALUE AS STATE,
LEFT(b.P_VALUE,10) AS STATE_CODE,
b.P_VALUE AS PROVINCE,
LEFT(b.P_VALUE,10) AS PROVINCE_CODE,
a.P_VALUE AS COUNTY,
LEFT(a.P_VALUE,10) AS COUNTY_CODE,
a.C_VALUE AS CITY,
LEFT(a.C_VALUE,10) as CITY_CODE,
GETDATE() AS LAST_UPDATED,
'AR_LA_IDL_XD_01 Lokesh' AS LAST_SOURCE
FROM AR_LA_IDL_XD_01_GEO a
INNER JOIN AR_LA_IDL_XD_01_GEO b
ON (b.C_VALUE=a.P_VALUE)
WHERE
a.C_TYPE='CABECERA' AND
b.C_TYPE='DEPARTMENT'

**/

/**
SELECT DISTINCT TIME_ZONE FROM O_STG_CUSTOMER_ADDRESS WHERE COUNTRY='AR'
SELECT DISTINCT TIME_ZONE FROM O_PRE_CUSTOMER_ADDRESS WHERE COUNTRY='AR'


SELECT DISTINCT TIME_ZONE FROM O_PRE_CUSTOMER_ADDRESS WHERE COUNTRY='CL'
SELECT DISTINCT TIME_ZONE FROM O_STG_CUSTOMER_ADDRESS WHERE COUNTRY='CL'


SELECT DISTINCT TIME_ZONE, COUNT(*) FROM O_STG_CUSTOMER_ADDRESS WHERE COUNTRY='CL' GROUP BY TIME_ZONE


UPDATE O_STG_CUSTOMER_ADDRESS SET TIME_ZONE='America/Santiago' WHERE TIME_ZONE='Chile/Continental'
**/

/**
ALTER TABLE O_STG_CUSTOMER_HEADER ADD DOCUMENT_TYPE_UY NVARCHAR(10);
ALTER TABLE O_STG_CUSTOMER_HEADER ADD PERCEPTION_AGENT_PE NVARCHAR(2);
ALTER TABLE O_PRE_CUSTOMER_HEADER ADD DOCUMENT_TYPE_UY NVARCHAR(10);
ALTER TABLE O_PRE_CUSTOMER_HEADER ADD PERCEPTION_AGENT_PE NVARCHAR(2);
**/


/**
EXEC dbo.generateCustomerHeaderByCountry 'MX'

UPDATE O_STG_CUSTOMER_HEADER
SET ORIGIN_ALL='Domestic Origin'
WHERE
COUNTRY IN ('MX','NI','GT','CR','SV','HN','PE','AR','CL','PY','UY')

UPDATE O_STG_CUSTOMER_HEADER
SET NATURE_ALL='Legal Entity'
WHERE
COUNTRY IN ('MX','NI','GT','CR','SV','HN','PE','AR','CL','PY','UY')
**/

/**
SELECT a.FISCAL_CLASS_CATEGORY, a.FISCAL_CLASS_CODE FROM O_STG_CUSTOMER_HEADER a WHERE a.COUNTRY='PA'
EXEC dbo.generateCustomerHeaderByCountry 'PA'
**/
/**
EXECUTE dbo.fixMultipleCustomers 'PA';

SELECT * FROM O_STG_CUSTOMER_CROSS_REF_ALT a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA3154-CC'
SELECT * FROM O_STG_CUSTOMER_CROSS_REF_ALT a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA5066-CC'
SELECT * FROM O_STG_CUSTOMER_CROSS_REF_ALT a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA5078-CC'
SELECT * FROM O_STG_CUSTOMER_CROSS_REF_ALT a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA5480-CC'
SELECT * FROM O_STG_CUSTOMER_CROSS_REF_ALT a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA5504-CC'

SELECT * FROM O_STG_CUSTOMER_CROSS_REF_ALT a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA5066-CC'

SELECT a.MERGED_ID FROM O_STG_CUSTOMER_HEADER a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA5132-CC'


SELECT * FROM O_STG_CUSTOMER_CROSS_REF_ALT a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA3154-CC'

SELECT a.MERGED_ID, a.DISMISSED FROM O_STG_CUSTOMER_HEADER a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA3154-CC'
SELECT a.MERGED_ID, a.DISMISSED FROM O_STG_CUSTOMER_HEADER a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA5066-CC'
SELECT a.MERGED_ID, a.DISMISSED FROM O_STG_CUSTOMER_HEADER a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA5078-CC'
SELECT a.MERGED_ID, a.DISMISSED FROM O_STG_CUSTOMER_HEADER a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA5480-CC'
SELECT a.MERGED_ID, a.DISMISSED FROM O_STG_CUSTOMER_HEADER a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA5504-CC'


SELECT
a.ALIASES,
a.ORIG_SYSTEM_CUSTOMER_REF,
a.id
FROM O_STG_CUSTOMER_HEADER a
WHERE COUNTRY='PA' AND
a.ALIASES IS NOT NULL AND
LEN(a.ALIASES)>0 AND
a.ORIG_SYSTEM_CUSTOMER_REF='PA5132-CC' AND
a.DISMISSED=0
ORDER BY
a.ALIASES DESC

**/

/**
EXEC dbo.generateProjectHeaderByCountry 'PA'
EXEC dbo.generateProjectClassificationsByCountry 'PA'
EXEC dbo.generateProjectRetRulesByCountry 'PA'
EXEC dbo.generateProjectTasksByCountry 'PA'

EXEC dbo.populateProjectRevenueForPA
EXEC dbo.generateProjectCostForPA
EXEC dbo.generateProjectBudgetHeaderForPA
EXEC dbo.generateProjectBudgetLineForPA
EXEC dbo.populateProjectAgreementHeaderForPA
EXEC dbo.generateProjectAgreementFundingForPA
EXEC dbo.generateProjectPOCAnalysisForPA

SELECT * FROM O_ONLY_STG_PROJECT_TASKS WHERE COUNTRY='PA'
SELECT * FROM O_ONLY_PRE_PROJECT_TASKS WHERE COUNTRY='PA'
SELECT * FROM O_ONLY_PRE_PROJECT_TASKS WHERE COUNTRY='PA'
**/
/**
EXEC dbo.fixMultipleCustomers 'PA';

SELECT * FROM O_STG_CUSTOMER_CROSS_REF_ALT a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA3405-CC'

SELECT a.MERGED_ID FROM O_STG_CUSTOMER_HEADER a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA3405-CC'

SELECT a.MERGED_ID FROM O_PRE_CUSTOMER_HEADER a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA3405-CC'


SELECT * FROM O_STG_CUSTOMER_CROSS_REF_ALT a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA3405-CC'

SELECT * FROM O_STG_CUSTOMER_CROSS_REF_EXT J WHERE j.ORIG_SYSTEM_CUSTOMER_REF='PA5100-CC'

SELECT * FROM O_STG_CUSTOMER_CROSS_REF_EXT J WHERE j.ORIG_SYSTEM_CUSTOMER_REF='PA3071-CC'

SELECT * FROM O_STG_CUSTOMER_CROSS_REF_EXT J WHERE j.ORIG_SYSTEM_CUSTOMER_REF='PA5131-CC'


SELECT a.MERGED_ID, a.* FROM O_PRE_CUSTOMER_HEADER a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA5100-CC'
SELECT a.MERGED_ID, a.* FROM O_PRE_CUSTOMER_HEADER a WHERE a.ORIG_SYSTEM_CUSTOMER_REF='PA3071-CC'
**/

