-- Customers standard loading routines


:setvar procMain loadCustomerMasterData_


GO
:on error exit

GO
USE [$(targetDatabaseName)];

IF OBJECT_ID('tempdb.dbo.#TMP_UNIQUE_$(targetCountryCode)', 'U') IS NOT NULL
  DROP TABLE dbo.#TMP_UNIQUE_$(targetCountryCode);

IF OBJECT_ID('$(targetDatabaseName).dbo.buildCustomerHeaderRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildCustomerHeaderRow_$(targetCountryCode);

IF OBJECT_ID('$(targetDatabaseName).dbo.buildCustomerContactRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildCustomerContactRow_$(targetCountryCode);

IF OBJECT_ID('$(targetDatabaseName).dbo.loadCustomerMasterData_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.loadCustomerMasterData_$(targetCountryCode);

IF OBJECT_ID('$(targetDatabaseName).dbo.rebuildPreStageCustomerIndex', 'P') IS NOT NULL
  DROP PROCEDURE dbo.rebuildPreStageCustomerIndex;
GO

CREATE PROCEDURE dbo.rebuildPreStageCustomerIndex
AS
BEGIN
	UPDATE O_PRE_CUSTOMER_HEADER SET CLEANSED=0;
	UPDATE O_PRE_CUSTOMER_HEADER SET CLEANSED=1 WHERE ORIG_SYSTEM_PARTY_REF IN (SELECT ORIG_SYSTEM_PARTY_REF FROM O_STG_CUSTOMER_HEADER)
END

GO

-- 1. Defines row building procedures for header and dependent objects
--

-- 1.a procedures have two parameters : @useCase and @itemCode
-- 1.b procedure should insert a single row
-- 1.c usually rows are marked as RECOMMENDED 'N' (not recommended)

CREATE PROCEDURE dbo.buildCustomerHeaderRow_$(targetCountryCode)  
(  
    @itemCode VARCHAR(16),
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN
INSERT INTO O_PRE_CUSTOMER_HEADER WITH (TABLOCK)
	(
	LAST_UPDATED,
	COUNTRY,
	LOCAL_VALIDATED,
	CROSSREF_VALIDATED,
	CLEANSED,
	RECOMMENDED,

	CUSTOMER_NAME,
	SPECIAL_CUSTOMER,
	PRIMARY_FLAG,
	ORIG_SYSTEM_PARTY_REF,
	ORIG_SYSTEM_CUSTOMER_REF,
	CUSTOMER_TYPE,
	TAX_PAYER_ID,
	CO_EXT_ATTRIBUTE2,
	CO_EXT_ATTRIBUTE4,
	FISCAL_CLASS_START_DATE,
	CUSTOMER_PROFILE,
	GLOBAL_FLAG,
	ORG_NAME
	)
SELECT
	GETDATE(),
	'$(targetCountryCode)',
	1,
	1,
	0,
	0,

	UPPER(CLIENTES.NOMBRE),
	0,
	1,
	'$(targetCountryCode)' + RTRIM(LTRIM(CAST(CLIENTES.CLIENTE AS varchar(30)))) + '-CC' AS ORIG_SYSTEM_PARTY_REF,
	'$(targetCountryCode)' + RTRIM(LTRIM(CAST(CLIENTES.CLIENTE AS varchar(30)))) + '-CC' AS ORIG_SYSTEM_CUSTOMER_REF,
	'EXTERNAL',
	LEFT(RTRIM(LTRIM(CLIENTES.RUC)),20) AS TAX_PAYER_ID,
	LEFT(RTRIM(LTRIM(CLIENTES.RUC)),20) AS CO_EXT_ATTRIBUTE2,
	'2' AS CO_EXT_ATTRIBUTE4,
	CONVERT(datetime, '01-01-2018', 105),
	'TKE LATAM STANDARD',
	0,
	'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
FROM
$(sourceDatabaseName).dbo.CLIENTES
WHERE $(sourceDatabaseName).dbo.CLIENTES.CLIENTE = @itemCode
END;
GO

IF OBJECT_ID('$(targetDatabaseName).dbo.buildCustomerAddressRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildCustomerAddressRow_$(targetCountryCode);

GO

CREATE PROCEDURE dbo.buildCustomerAddressRow_$(targetCountryCode)
(  
    @itemCode VARCHAR(16),
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN
INSERT INTO O_PRE_CUSTOMER_ADDRESS WITH (TABLOCK)
	(
	LAST_UPDATED,
	COUNTRY,
	LOCAL_VALIDATED,
	CROSSREF_VALIDATED,
	CLEANSED,
	RECOMMENDED,

	CUSTOMER_NAME,
	ORIG_SYSTEM_CUSTOMER_REF,
	ORIG_SYSTEM_ADRESS_REFERENCE,
	SITE_USE_CODE,
	PRIMARY_SITE_USE_FLAG,
	LOCATION,
	ADDRESS_LINE1,
	CITY,
	ADDRESS_COUNTRY,
	PROVINCE,
	CUSTOMER_PROFILE,
	TIME_ZONE,
	LANGUAGE,
	ORG_NAME
	)
SELECT
	GETDATE(),
	'$(targetCountryCode)',
	1,
	1,
	0,
	0,

	UPPER(c.NOMBRE) AS CUSTOMER_NAME,
	'$(targetCountryCode)' + RTRIM(LTRIM(CAST(c.CLIENTE AS varchar(30)))) + '-CC' AS ORIG_SYSTEM_CUSTOMER_REF,
	'$(targetCountryCode)' + RTRIM(LTRIM(CAST(c.CLIENTE AS varchar(30)))) + '-CA' AS ORIG_SYSTEM_ADRESS_REFERENCE,
	'ALL',
	1,
	UPPER(LEFT(c.DIRECCION,40)),
	UPPER(LEFT(c.DIRECCION,40)),
	cc.DESCRIP,
	'$(targetCountryCode)',
	d.DESCRIP,
	'DEFAULT',
	'$(timezoneName)',
	'ESA',
	'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
FROM
$(sourceDatabaseName).dbo.CLIENTES c
LEFT JOIN $(sourceDatabaseName).dbo.CIUDADES cc ON c.IdCiudad = cc.IdCiudad
LEFT JOIN $(sourceDatabaseName).dbo.DISTRITOS d ON c.IdDistrito = d.IdDistrito
WHERE c.CLIENTE = @itemCode
END;
GO


CREATE PROCEDURE dbo.buildCustomerContactRow_$(targetCountryCode)  
(  
    @itemCode VARCHAR(16),
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN

IF EXISTS(
SELECT TOP 1
	'$(targetCountryCode)',
	1,
	1,
	0,
	0,
	LEFT('$(targetCountryCode)' + RTRIM(LTRIM(CAST(c.CLIENTE AS varchar(30)))) + '-CC',240) AS ORIG_SYSTEM_CUSTOMER_REF,
	LEFT('$(targetCountryCode)' + RTRIM(LTRIM(CAST(c.CLIENTE AS varchar(30)))) + '-CA',30) AS ORIG_SYSTEM_ADRESS_REFERENCE,
	LEFT('$(targetCountryCode)' + RTRIM(LTRIM(CAST(c.CLIENTE AS varchar(30)))) + '-CC.',30) + CAST(@@ROWCOUNT AS varchar(3)) AS ORIG_SYSTEM_CONTACT_REF,
	LEFT('$(targetCountryCode)' + RTRIM(LTRIM(CAST(c.CLIENTE AS varchar(30)))) + '-CT',30) AS ORIG_SYSTEM_TELEPHONE_REFERENCE,
	UPPER(LEFT(SUBSTRING(ct.NOMBRE, 1, CHARINDEX(' ', ct.NOMBRE, 1)), 60)),
	UPPER(LEFT(SUBSTRING(ct.NOMBRE, CHARINDEX(' ', ct.NOMBRE, 1)+1, LEN(c.NOMBRE) - CHARINDEX(ct.NOMBRE, ' ', 1)), 60)),
	'EMAIL',
	LEFT(ISNULL(LEFT(ct.TELEFONO,25), ''),30),
	LEFT(ct.CorreoElectronico,240),
	LEFT(ct.Cargo,30),
	'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
FROM
	TKPA_LATEST.dbo.CLIENTES c,
	TKPA_LATEST.dbo.CLIENTECONTACTOS cc,
	TKPA_LATEST.dbo.CONTACTOS ct
WHERE
	c.CLIENTE = cc.Cliente AND
	ct.IdContacto = cc.IdContacto AND
	ct.Activo = 1 AND
	c.CLIENTE = @itemCode
)
	BEGIN

		INSERT INTO O_PRE_CUSTOMER_CONTACT WITH (TABLOCK)
			(
			LAST_UPDATED,
			COUNTRY,
			LOCAL_VALIDATED,
			CROSSREF_VALIDATED,
			CLEANSED,
			RECOMMENDED,
			ORIG_SYSTEM_CUSTOMER_REF,
			ORIG_SYSTEM_ADRESS_REFERENCE,
			ORIG_SYSTEM_CONTACT_REF,
			ORIG_SYSTEM_TELEPHONE_REF,
			CONTACT_FIRST_NAME,
			CONTACT_LAST_NAME,
			TELEPHONE_TYPE,
			TELEPHONE,
			EMAIL_ADDRESS,
			JOB_TITLE,
			ORG_NAME
			)
		SELECT
			GETDATE(),
			'$(targetCountryCode)',
			1,
			1,
			0,
			0,
			LEFT('$(targetCountryCode)' + RTRIM(LTRIM(CAST(c.CLIENTE AS varchar(30)))) + '-CC',240) AS ORIG_SYSTEM_CUSTOMER_REF,
			LEFT('$(targetCountryCode)' + RTRIM(LTRIM(CAST(c.CLIENTE AS varchar(30)))) + '-CA',30) AS ORIG_SYSTEM_ADRESS_REFERENCE,
			LEFT('$(targetCountryCode)' + RTRIM(LTRIM(CAST(c.CLIENTE AS varchar(30)))) + '-CC.',30) + CAST(ROW_NUMBER() OVER (ORDER BY ct.NOMBRE) AS varchar(3)) AS ORIG_SYSTEM_CONTACT_REF,
			LEFT('$(targetCountryCode)' + RTRIM(LTRIM(CAST(c.CLIENTE AS varchar(30)))) + '-CT',30) AS ORIG_SYSTEM_TELEPHONE_REFERENCE,
			ISNULL(UPPER(LEFT(SUBSTRING(ct.NOMBRE, 1, CHARINDEX(' ', ct.NOMBRE, 1)), 60)),'UNKNOWN'),
			ISNULL(UPPER(LEFT(SUBSTRING(ct.NOMBRE, CHARINDEX(' ', ct.NOMBRE, 1)+1, LEN(c.NOMBRE) - CHARINDEX(ct.NOMBRE, ' ', 1)), 60)),'UNKNOWN'),
			'EMAIL',
			LEFT(ISNULL(LEFT(ct.TELEFONO,25), ''),30),
			LEFT(ct.CorreoElectronico,240),
			LEFT(ct.Cargo,30),
			'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
		FROM
			$(sourceDatabaseName).dbo.CLIENTES c,
			$(sourceDatabaseName).dbo.CLIENTECONTACTOS cc,
			$(sourceDatabaseName).dbo.CONTACTOS ct
		WHERE
			c.CLIENTE = cc.Cliente AND
			ct.IdContacto = cc.IdContacto AND
			ct.Activo = 1 AND
			c.CLIENTE = @itemCode
	
	END
ELSE
	BEGIN

		INSERT INTO O_PRE_CUSTOMER_CONTACT WITH (TABLOCK)
			(
			LAST_UPDATED,
			COUNTRY,
			LOCAL_VALIDATED,
			CROSSREF_VALIDATED,
			CLEANSED,
			RECOMMENDED,
			ORIG_SYSTEM_CUSTOMER_REF,
			ORIG_SYSTEM_ADRESS_REFERENCE,
			ORIG_SYSTEM_CONTACT_REF,
			ORIG_SYSTEM_TELEPHONE_REF,
			CONTACT_FIRST_NAME,
			CONTACT_LAST_NAME,
			TELEPHONE_TYPE,
			TELEPHONE,
			EMAIL_ADDRESS,
			ORG_NAME
			)
		SELECT
		    GETDATE(),
			'$(targetCountryCode)',
			1,
			1,
			0,
			0,
			'$(targetCountryCode)' + RTRIM(LTRIM(CAST(CLIENTES.CLIENTE AS varchar(30)))) + '-CC' AS ORIG_SYSTEM_CUSTOMER_REF,
			'$(targetCountryCode)' + RTRIM(LTRIM(CAST(CLIENTES.CLIENTE AS varchar(30)))) + '-CA' AS ORIG_SYSTEM_ADRESS_REFERENCE,
			'$(targetCountryCode)' + RTRIM(LTRIM(CAST(CLIENTES.CLIENTE AS varchar(30)))) + '-CC.'  + CAST(ROW_NUMBER() OVER (ORDER BY CONTACTO) AS varchar(3)) AS ORIG_SYSTEM_CONTACT_REF,
			'$(targetCountryCode)' + RTRIM(LTRIM(CAST(CLIENTES.CLIENTE AS varchar(30)))) + '-CT' AS ORIG_SYSTEM_ADRESS_REFERENCE,
			ISNULL(UPPER(LEFT(SUBSTRING(CONTACTO, 1, CHARINDEX(' ', CONTACTO, 1)), 60)),'UNKNOWN'),
			ISNULL(UPPER(LEFT(SUBSTRING(CONTACTO, CHARINDEX(' ', CONTACTO, 1)+1, LEN(CONTACTO) - CHARINDEX(CONTACTO, ' ', 1)), 60)),'UNKNOWN'),
			'PHONE',
			ISNULL(LEFT(CLIENTES.TELEFONO,25), '<unknown'),
			CLIENTES.EMAIL,
			'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
		FROM
		$(sourceDatabaseName).dbo.CLIENTES
		WHERE CLIENTES.CLIENTE = @itemCode
	END


END;
GO

CREATE PROCEDURE dbo.loadCustomerMasterData_$(targetCountryCode)
AS
BEGIN
	DELETE FROM dbo.O_PRE_CUSTOMER_HEADER WHERE COUNTRY='$(targetCountryCode)';
	DELETE FROM dbo.O_PRE_CUSTOMER_ADDRESS WHERE COUNTRY='$(targetCountryCode)';
	DELETE FROM dbo.O_PRE_CUSTOMER_CONTACT WHERE COUNTRY='$(targetCountryCode)';
	
	DELETE FROM COM_AUDIT_LOGS WHERE EVENT LIKE '%Customers Master Data partial load%' AND COUNTRY='$(targetCountryCode)'

	-- 3. avoids duplicates on filtered group
	--
	select distinct CLIENTE INTO #TMP_UNIQUE_$(targetCountryCode) FROM $(sourceDatabaseName).dbo.CLIENTES

	DECLARE @unique_id VARCHAR(16)

	DECLARE @logThreshold int = 100
	DECLARE @count int = 0
	DECLARE @total int = 0
	
	DECLARE UNIQUE_FUNCTIONAL CURSOR FOR   
	SELECT CLIENTE  
	FROM #TMP_UNIQUE_$(targetCountryCode);

	SELECT @total=COUNT(CLIENTE) FROM #TMP_UNIQUE_$(targetCountryCode);
	
	OPEN UNIQUE_FUNCTIONAL;

	FETCH NEXT FROM UNIQUE_FUNCTIONAL   
	INTO @unique_id;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC dbo.buildCustomerHeaderRow_$(targetCountryCode) @unique_id
		EXEC dbo.buildCustomerAddressRow_$(targetCountryCode) @unique_id
		EXEC dbo.buildCustomerContactRow_$(targetCountryCode) @unique_id

		IF (@count%@logThreshold = 0)
		BEGIN
			INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,SNAPSHOT_DATE)
			VALUES('$(targetCountryCode)', 'script', 'Customers Master Data partial load ' + CAST(@count AS VARCHAR(max))+'/'+CAST(@total AS VARCHAR(max)), GETDATE(), @count, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));
		END
		
		SET @count = @count + 1

		FETCH NEXT FROM UNIQUE_FUNCTIONAL   
		INTO @unique_id;
	END;

	CLOSE UNIQUE_FUNCTIONAL;

	DEALLOCATE UNIQUE_FUNCTIONAL;

	EXEC dbo.rebuildPreStageCustomerIndex

	DELETE FROM COM_AUDIT_LOGS WHERE EVENT LIKE '%Customers Master Data partial load%' AND COUNTRY='$(targetCountryCode)'

	SELECT @count=COUNT(*) FROM O_PRE_CUSTOMER_HEADER WHERE COUNTRY='$(targetCountryCode)'

	INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,SNAPSHOT_DATE)
	VALUES('$(targetCountryCode)', 'script', 'Customers Master Data loaded from $(sourceDatabaseName) @ ' + dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'), GETDATE(), @count, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));

END
GO

IF OBJECT_ID('$(targetDatabaseName).dbo.addCustomerRecommendationsFor_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.addCustomerRecommendationsFor_$(targetCountryCode);

GO

CREATE PROCEDURE dbo.addCustomerRecommendationsFor_$(targetCountryCode)
AS
BEGIN
	UPDATE O_PRE_CUSTOMER_HEADER SET RECOMMENDED = 0 WHERE COUNTRY='$(targetCountryCode)';

	-- Customers from mantenimento
	UPDATE O_PRE_CUSTOMER_HEADER SET
	  RECOMMENDED = 1
	WHERE ORIG_SYSTEM_CUSTOMER_REF COLLATE DATABASE_DEFAULT IN
	(
	  SELECT
		DISTINCT '$(targetCountryCode)' + RTRIM(LTRIM(CAST(a.CLIENTE AS varchar(30)))) + '-CC' AS ORIG_SYSTEM_CUSTOMER_REF
	  FROM $(sourceDatabaseName).dbo.manten AS a
	  INNER JOIN $(sourceDatabaseName).dbo.titrenovac AS b
		ON (a.contrato = b.contrato AND b.RENOVACION=(
		  SELECT top 1 RENOVACION
		  FROM $(sourceDatabaseName).dbo.TITRENOVAC
		  WHERE CONTRATO=a.CONTRATO ORDER BY RENOVACION DESC)
		)
	  INNER JOIN $(sourceDatabaseName).dbo.DETTABAUX AS d ON (a.CLASIFICACION=d.Id and d.TABLA=6)
	  INNER JOIN $(sourceDatabaseName).dbo.PAGOS AS f ON (a.IDPAGO=f.IdPago)
	  LEFT JOIN $(sourceDatabaseName).dbo.USUARIOS AS c ON (a.USERVENDEDOR=c.USUARIO)
	  LEFT JOIN (
		SELECT MAX(fecha) AS fecha, contrato, MAX(ltrim(rtrim(glosa))) AS glosa
		FROM $(sourceDatabaseName).dbo.ObservacionesContrato
		GROUP BY contrato
		) AS e ON (a.CONTRATO=e.contrato)
	  LEFT JOIN (
		SELECT MAX(Fcancel) AS FCancel,codcont
		FROM $(sourceDatabaseName).dbo.CabCancelContMant
		GROUP BY codcont
		) AS h ON (a.CONTRATO=h.CodCont)

	  WHERE h.FCancel IS NULL
	)

	DECLARE @count int = 0

	SELECT @count=COUNT(*) FROM O_PRE_CUSTOMER_HEADER WHERE RECOMMENDED=1 AND COUNTRY='$(targetCountryCode)'

	INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,SNAPSHOT_DATE)
	VALUES('$(targetCountryCode)', 'script', 'Customers Master Data added recommendations from $(sourceDatabaseName) @ ' + dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'), GETDATE(), @count, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));

END
GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,SNAPSHOT_DATE)
VALUES('$(targetCountryCode)', 'script', 'Customers Master Data load procedures redefined', GETDATE(), dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));

