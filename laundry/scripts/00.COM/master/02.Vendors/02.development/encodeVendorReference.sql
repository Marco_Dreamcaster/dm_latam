IF OBJECT_ID('dbo.udf_EncodeVendorReference', 'FN') IS NOT NULL
	DROP FUNCTION dbo.udf_EncodeVendorReference;
GO

CREATE FUNCTION dbo.udf_EncodeVendorReference ( @country VARCHAR(2), @proveedor VARCHAR(10) )
RETURNS VARCHAR(25)
AS
BEGIN

DECLARE @vendorReference VARCHAR(25)

SET @vendorReference = UPPER(@country+RTRIM(LTRIM(@proveedor))+'-VE');

RETURN @vendorReference
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated udf_EncodeVendorReference function definition', GETDATE());

