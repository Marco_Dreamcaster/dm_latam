IF OBJECT_ID('dbo.generateVendorBankAccountByCountry', 'P') IS NOT NULL
DROP PROCEDURE dbo.generateVendorBankAccountByCountry;

GO

CREATE PROCEDURE dbo.generateVendorBankAccountByCountry
(
@country VARCHAR(2)
)
AS
BEGIN
SELECT
b.SEGMENT1 as vendorNumber,
a.BANK_NAME as bankName,
a.BRANCH_NAME as branchName,
a.BANK_NUMBER as bankNumber,
a.BRANCH_NUMBER as branchNumber,
a.BANK_HOME_COUNTRY as bankHomeCountry,
a.TAX_PAYER_ID as taxPayerId,
a.BANK_ACCOUNT_NAME as bankAccountName,
a.BANK_ACCOUNT_NUM as bankAccountNum,
a.CURRENCY_CODE as currencyCode,
a.START_DATE as startDate,
a.END_DATE as endDate,
dbo.udf_EncodeOrgName (@country) as orgName,
a.BIC as bic,
a.IBAN as iban,
a.TYPE as type
FROM
O_STG_VENDOR_BANK_ACCOUNT a INNER JOIN 
O_STG_VENDOR_HEADER b
ON b.SEGMENT1=a.VENDOR_NUMBER
WHERE
a.COUNTRY=@country AND
a.DISMISSED=0 AND
a.LOCAL_VALIDATED=1 AND
a.CROSSREF_VALIDATED=1 AND
b.DISMISSED=0 AND
b.LOCAL_VALIDATED=1 AND
b.CROSSREF_VALIDATED=1
ORDER BY 
b.SEGMENT1
END
GO
INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateVendorBankAccountByCountry procedure definition', GETDATE());
GO
