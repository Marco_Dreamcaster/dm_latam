IF OBJECT_ID('.generateVendorCrossRefAliasCountByCountry', 'P') IS NOT NULL
DROP PROCEDURE dbo.generateVendorCrossRefAliasCountByCountry;
GO

CREATE PROCEDURE dbo.generateVendorCrossRefAliasCountByCountry
(
@country VARCHAR(2)
)
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

EXEC dbo.fixMultipleVendors @country

SELECT b.[ORACLE_VENDOR_REF]              as vendorNumber
,COUNT(*)                                 as ALIASES
FROM DBO.[O_STG_VENDOR_HEADER] a
inner join DBO.[O_STG_VENDOR_CROSS_REF_ALT] b ON a.SEGMENT1=b.VENDOR_NUMBER_REF
WHERE
a.[COUNTRY]=@country
GROUP BY b.[ORACLE_VENDOR_REF]
ORDER BY ALIASES DESC

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

RETURN

END
GO
INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateVendorCrossRefAliasCountByCountry procedure definition', GETDATE());
GO
