IF OBJECT_ID('.generateVendorCrossRefByCountry', 'P') IS NOT NULL
DROP PROCEDURE dbo.generateVendorCrossRefByCountry;
GO

CREATE PROCEDURE dbo.generateVendorCrossRefByCountry
(
@country VARCHAR(2)
)
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

EXEC dbo.fixMultipleVendors @country

SELECT a.[SEGMENT1]                             as origVendorNumber
,b.[ORACLE_VENDOR_REF]                          as vendorNumber
,b.[CROSS_REFERENCE_TYPE]                       as crossReferenceType
,b.[CROSS_REFERENCE]                            as crossReference
,b.[DESCRIPTION]                                as description
,'L'+@country                                   as organizationCode
,dbo.udf_EncodeOrgName (@country)				as orgName
FROM DBO.[O_STG_VENDOR_HEADER] a
inner join DBO.[O_STG_VENDOR_CROSS_REF_ALT] b ON a.SEGMENT1=b.ORACLE_VENDOR_REF
WHERE
a.[COUNTRY]=@country
ORDER BY b.ORACLE_VENDOR_REF

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

RETURN
END
GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateVendorCrossRefByCountry procedure definition', GETDATE());
GO
