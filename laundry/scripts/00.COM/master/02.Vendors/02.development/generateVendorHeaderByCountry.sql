IF OBJECT_ID('dbo.generateVendorHeaderByCountry', 'P') IS NOT NULL
DROP PROCEDURE dbo.generateVendorHeaderByCountry;

GO

CREATE PROCEDURE dbo.generateVendorHeaderByCountry
(
@country VARCHAR(2)
)
AS
BEGIN
SELECT
a.VENDOR_NAME					as vendorName,
a.SEGMENT1						as segment1,
a.VENDOR_NAME_ALT				as vendorNameAlt,
a.VENDOR_TYPE_LOOKUP_CODE		as vendorTypeLookupCode,
a.VAT_REGISTRATION_NUM			as vatRegistrationNum,
a.NUM_1099						as num1099,
a.FEDERAL_REPORTABLE_FLAG		as federalReportableFlag,
a.TYPE_1099						as type1099,
a.STATE_REPORTABLE_FLAG			as stateReportableFlag,
a.ALLOW_AWT_FLAG				as allowAwtFlag,
a.MATCH_OPTION_LEVEL			as matchOptionLevel,
a.PAYMENT_METHOD_LOOKUP_CODE	as paymentMethodLookupCode,
a.GLOBAL_FLAG					as globalFlag,
a.SENSITIVE_VENDOR				as sensitiveVendor,
a.GLOBAL_ATTRIBUTE1				as globalAttribute1,
a.GLOBAL_ATTRIBUTE8				as globalAttribute8,
a.GLOBAL_ATTRIBUTE9				as globalAttribute9,
a.GLOBAL_ATTRIBUTE10			as globalAttribute10,
a.GLOBAL_ATTRIBUTE12			as globalAttribute12,
a.GLOBAL_ATTRIBUTE15			as globalAttribute15,
a.GLOBAL_ATTRIBUTE16			as globalAttribute16,
a.ATTRIBUTE_CATEGORY			as attributeCategory,
a.ATTRIBUTE1					as attribute1,
a.ATTRIBUTE2					as attribute2,
a.ATTRIBUTE3					as attribute3,
a.ATTRIBUTE4					as attribute4,
a.ATTRIBUTE5					as attribute5,
a.ATTRIBUTE6					as attribute6,
a.ATTRIBUTE7					as attribute7,
a.ATTRIBUTE8					as attribute8,
a.ATTRIBUTE9					as attribute9,
a.ATTRIBUTE10					as attribute10,
a.PA_EXT_ATTRIBUTE1				as paExtAttribute1,
a.CO_EXT_ATTRIBUTE1				as coExtAttribute1,
a.CO_EXT_ATTRIBUTE2				as coExtAttribute2,
a.CO_EXT_ATTRIBUTE3				as coExtAttribute3,
a.CO_EXT_ATTRIBUTE4				as coExtAttribute4,
a.CO_EXT_ATTRIBUTE5				as coExtAttribute5,
a.CO_EXT_ATTRIBUTE6				as coExtAttribute6,
a.CO_EXT_ATTRIBUTE7				as coExtAttribute7,
a.CO_EXT_ATTRIBUTE8				as coExtAttribute8,
a.CO_EXT_ATTRIBUTE9				as coExtAttribute9,
dbo.udf_EncodeOrgName (@country)	    as orgName,
a.SUPPLIER_TYPE_NI				as supplierTypeNi,
a.ORDER_NUMBER_GT				as orderNumberGt,
a.REGISTER_NUMBER_GT			as registerNumberGt,
a.SUPPLIER_TYPE_SV				as supplierTypeSv,
a.DOCUMENT_TYPE_SV				as documentTypeSv,
a.CONTRIBUTOR_REGISTRATION_SV   as contributorRegistrationSv,
a.LEGAL_REPRESENTATIVE_SV		as legalRepresentativeSv,
a.PE_EXT_ATTRIBUTE_CATEGORY		as personCategoryPE,
a.PE_EXT_ATTRIBUTE1				as personTypePE,
a.PE_EXT_ATTRIBUTE2				as documentTypePE,
a.PE_EXT_ATTRIBUTE3				as documentNumberPE,
a.PE_EXT_ATTRIBUTE4				as lasNamePE,
a.PE_EXT_ATTRIBUTE5				as secondLasNamePE,
a.PE_EXT_ATTRIBUTE6				as firstNamePE,
a.PE_EXT_ATTRIBUTE7				as middleNamePE,
a.PE_EXT_ATTRIBUTE8				as supplierConditionPE,
a.PE_EXT_ATTRIBUTE9				as withholdingSupplierPE,
a.PE_EXT_ATTRIBUTE10			as countryCodePE,
a.PE_EXT_ATTRIBUTE11			as serviceLenderPE,
a.PE_EXT_ATTRIBUTE12			as conventionPE,
a.PE_EXT_ATTRIBUTE13			as notResidentDocumentTypePE,
a.PE_EXT_ATTRIBUTE15			as agreementAvoidDoubleTaxPE,
a.UY_EXT_ATTRIBUTE1				as contributorTypeUY,
@country						as country


FROM
O_STG_VENDOR_HEADER a
WHERE 
a.COUNTRY=@country AND
a.DISMISSED=0 AND
a.LOCAL_VALIDATED=1 AND
a.CROSSREF_VALIDATED=1
ORDER BY 
a.VENDOR_NAME
END
GO
INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateVendorHeaderByCountry procedure definition', GETDATE());
GO
