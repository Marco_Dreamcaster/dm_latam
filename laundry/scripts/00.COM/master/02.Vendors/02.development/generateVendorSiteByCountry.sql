IF OBJECT_ID('dbo.generateVendorSiteByCountry', 'P') IS NOT NULL
DROP PROCEDURE dbo.generateVendorSiteByCountry;
GO

CREATE PROCEDURE dbo.generateVendorSiteByCountry
(
@country VARCHAR(2)
)
AS
BEGIN
SELECT
b.SEGMENT1 as vendorNumber,
a.VENDOR_SITE_CODE as vendorSiteCode,
a.VENDOR_SITE_CODE_ALT as vendorSiteCodeAlt,
a.ADDRESS_LINE1 as addressLine1,
a.ADDRESS_LINE2 as addressLine2,
a.ADDRESS_LINE3 as addressLine3,
a.ADDRESS_LINE4 as addressLine4,
a.ADDRESS_LINE_ALT as addressLineAlt,
a.CITY as city,
a.COUNTY as county,
a.STATE as state,
a.PROVINCE as province,
a.ZIP as zip,
a.ADDRESS_COUNTRY as addressCountry,
a.AREA_CODE as areaCode,
a.PHONE as phone,
a.FAX_AREA_CODE as faxAreaCode,
a.FAX as fax,
a.PAYMENT_METHOD_LOOKUP_CODE as paymentMethodLookupCode,
a.VAT_CODE as vatCode,
a.ACCTS_PAY_ACCOUNT as acctsPayAccount,
a.PREPAY_ACCOUNT as prepayAccount,
a.PAY_GROUP_LOOKUP_CODE as payGroupLookupCode,
a.INVOICE_CURRENCY_CODE as invoiceCurrencyCode,
a.LANGUAGE as language,
a.TERMS_NAME as termsName,
a.EMAIL_ADDRESS as emailAddress,
a.PURCHASING_SITE_FLAG as purchasingSiteFlag,
a.PAY_SITE_FLAG as paySiteFlag,
a.VAT_REGISTRATION_NUM as vatRegistrationNum,
a.SHIP_TO_LOCATION_CODE as shipToLocationCode,
a.PRIMARY_PAY_SITE_FLAG as primaryPaySiteFlag,
a.GAPLESS_INV_NUM_FLAG as gaplessInvNumFlag,
a.OFFSET_TAX_FLAG as offsetTaxFlag,
a.ALWAYS_TAKE_DISC_FLAG as alwaysTakeDiscFlag,
a.EXCLUDE_FREIGHT_FROM_DISCOUNT as excludeFreightFromDiscount,
a.GLOBAL_ATTRIBUTE17 as globalAttribute17,
a.GLOBAL_ATTRIBUTE18 as globalAttribute18,
a.GLOBAL_ATTRIBUTE19 as globalAttribute19,
a.GLOBAL_ATTRIBUTE20 as globalAttribute20,
a.ATTRIBUTE_CATEGORY as attributeCategory,
a.ATTRIBUTE1 as attribute1,
a.ATTRIBUTE2 as attribute2,
a.ATTRIBUTE3 as attribute3,
a.ATTRIBUTE4 as attribute4,
a.ATTRIBUTE5 as attribute5,
a.ATTRIBUTE6 as attribute6,
a.ATTRIBUTE7 as attribute7,
a.ATTRIBUTE8 as attribute8,
a.ATTRIBUTE9 as attribute9,
a.ATTRIBUTE10 as attribute10,
a.CLASSIFICATION_TYPE_CODE as classificationTypeCode,
a.CLASSIFICATION_CODE as classificationCode,
a.CLASSIFICATION_START_DATE as classificationStartDate,
a.CO_EXT_ATTRIBUTE1 as coExtAttribute1,
dbo.udf_EncodeOrgName (@country)	    as orgName
FROM
O_STG_VENDOR_SITE a INNER JOIN 
O_STG_VENDOR_HEADER b
ON b.SEGMENT1=a.VENDOR_NUMBER
WHERE
a.COUNTRY=@country AND
a.DISMISSED=0 AND
a.LOCAL_VALIDATED=1 AND
a.CROSSREF_VALIDATED=1 AND
b.DISMISSED=0 AND
b.LOCAL_VALIDATED=1 AND
b.CROSSREF_VALIDATED=1
ORDER BY 
b.VENDOR_NAME
END
GO
INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateVendorSiteByCountry procedure definition', GETDATE());
GO
