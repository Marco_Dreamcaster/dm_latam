IF OBJECT_ID('dbo.generateVendorSiteContactByCountry', 'P') IS NOT NULL
DROP PROCEDURE dbo.generateVendorSiteContactByCountry;

GO

CREATE PROCEDURE dbo.generateVendorSiteContactByCountry
(
@country VARCHAR(2)
)
AS
BEGIN
SELECT
c.SEGMENT1 as vendorNumber,
b.VENDOR_SITE_CODE as vendorSiteCode,
a.PREFIX as prefix,
a.FIRST_NAME as firstName,
a.MIDDLE_NAME as middleName,
a.LAST_NAME as lastName,
a.DEPARTMENT as department,
a.PHONE_AREA_CODE as phoneAreaCode,
a.PHONE_NUMBER as phoneNumber,
a.FAX_AREA_CODE as faxAreaCode,
a.FAX_NUMBER as faxNumber,
a.EMAIL_ADDRESS as emailAddress,
dbo.udf_EncodeOrgName (@country)	    as orgName
FROM
O_STG_VENDOR_SITE_CONTACT a 
INNER JOIN O_STG_VENDOR_SITE b
ON a.VENDOR_SITE_CODE=b.VENDOR_SITE_CODE
INNER JOIN O_STG_VENDOR_HEADER c 
ON c.SEGMENT1=b.VENDOR_NUMBER
WHERE
c.COUNTRY='PA' AND
c.DISMISSED=0 AND
c.LOCAL_VALIDATED=1 AND
c.CROSSREF_VALIDATED=1 AND
b.DISMISSED=0 AND
b.LOCAL_VALIDATED=1 AND
b.CROSSREF_VALIDATED=1 AND
a.DISMISSED=0 AND
a.LOCAL_VALIDATED=1 AND
a.CROSSREF_VALIDATED=1
ORDER BY 
c.VENDOR_NAME
END
GO
INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateVendorSiteContactByCountry procedure definition', GETDATE());
GO
