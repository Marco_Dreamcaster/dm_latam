-- Vendors standard loading routines

GO
:on error exit

GO
USE [$(TargetDatabaseName)];

IF OBJECT_ID('tempdb.dbo.#TMP_UNIQUE', 'U') IS NOT NULL
  DROP TABLE dbo.#TMP_UNIQUE;
  
IF OBJECT_ID('$(TargetDatabaseName).dbo.buildVendorHeaderRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildVendorHeaderRow_$(targetCountryCode);

IF OBJECT_ID('$(TargetDatabaseName).dbo.buildVendorSiteRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildVendorSiteRow_$(targetCountryCode);

IF OBJECT_ID('$(TargetDatabaseName).dbo.buildVendorSiteContactRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildVendorSiteContactRow_$(targetCountryCode);

IF OBJECT_ID('$(TargetDatabaseName).dbo.buildVendorBankAccountRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildVendorBankAccountRow_$(targetCountryCode);

IF OBJECT_ID('$(TargetDatabaseName).dbo.loadVendorMasterData_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.loadVendorMasterData_$(targetCountryCode);

IF OBJECT_ID('$(TargetDatabaseName).dbo.rebuildPreStageVendorIndex', 'P') IS NOT NULL
  DROP PROCEDURE dbo.rebuildPreStageVendorIndex;
GO


-- 1. Defines row building procedures for header and dependent objects
--

CREATE PROCEDURE dbo.rebuildPreStageVendorIndex
AS
BEGIN
	UPDATE O_PRE_VENDOR_HEADER SET CLEANSED=0;
	UPDATE O_PRE_VENDOR_HEADER SET CLEANSED=1 WHERE SEGMENT1 IN (SELECT SEGMENT1 FROM O_STG_VENDOR_HEADER)
END

GO

CREATE PROCEDURE dbo.buildVendorSiteRow_$(targetCountryCode)
(  
    @itemCode VARCHAR(16),
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN 
INSERT INTO O_PRE_VENDOR_SITE WITH (TABLOCK)
	(
	LAST_UPDATED,
	COUNTRY,
	LOCAL_VALIDATED,
	CROSSREF_VALIDATED,
	CLEANSED,
	RECOMMENDED,

	VENDOR_NUMBER,
	VENDOR_SITE_CODE,
	ADDRESS_LINE1,
	PAY_SITE_FLAG,
	PURCHASING_SITE_FLAG,
	CLASSIFICATION_START_DATE,
	CITY,
	STATE,
	PROVINCE,
	ADDRESS_COUNTRY,

	ORG_NAME
	)
SELECT
	TOP 1
    GETDATE(),
	'$(targetCountryCode)',
	1,
	1,
	0,
	0,
	'$(targetCountryCode)' + LTRIM(RTRIM(CAST([$(SourceDatabaseName)].dbo.PROVEEDOR.idproveedor AS varchar(30))))+'-VE' AS VENDOR_NUMBER,
	RIGHT('$(targetCountryCode)' + LTRIM(RTRIM(CAST([$(SourceDatabaseName)].dbo.PROVEEDOR.idproveedor AS varchar(30))))+'-VC',15) AS VENDOR_SITE_CODE,
	RIGHT([$(SourceDatabaseName)].dbo.PROVEEDOR.direccion,150) AS ADDRESS_LINE1,
	1,
	1,
	CONVERT(datetime, '01-01-2018', 105),
	ISNULL(CIUDADES.DESCRIP,'FIX ME') AS CITY,
	'FIX_ME' AS STATE,
	'FIX_ME' AS PROVINCE,
	'$(targetCountryCode)' AS ADDRESS_COUNTRY,
	'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
FROM [$(SourceDatabaseName)].dbo.PROVEEDOR
LEFT JOIN [$(SourceDatabaseName)].dbo.CIUDADES
ON
[$(SourceDatabaseName)].dbo.PROVEEDOR.CIUDAD=[$(SourceDatabaseName)].dbo.CIUDADES.IdCiudad
WHERE [$(SourceDatabaseName)].dbo.PROVEEDOR.idproveedor = @itemCode

END
GO


-- 2.a procedures have two parameters : @useCase and @itemCode
-- 2.b procedure should insert a single row
-- 2.c usually rows are marked as RECOMMENDED 'N' (not recommended)
CREATE PROCEDURE dbo.buildVendorHeaderRow_$(targetCountryCode)
(  
    @itemCode VARCHAR(16),
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN 
INSERT INTO O_PRE_VENDOR_HEADER WITH (TABLOCK)
	(
	LAST_UPDATED,
	COUNTRY,
	LOCAL_VALIDATED,
	CROSSREF_VALIDATED,
	CLEANSED,
	RECOMMENDED,

	CODIGO,
	GLOBAL_FLAG,
	VENDOR_NAME,
	SEGMENT1,
	VENDOR_NAME_ALT,
	VENDOR_TYPE_LOOKUP_CODE,
	VAT_REGISTRATION_NUM,
	MATCH_OPTION_LEVEL,
	PAYMENT_METHOD_LOOKUP_CODE,
	NUM_1099,
	STATE_REPORTABLE_FLAG,
	SENSITIVE_VENDOR,
	ALLOW_AWT_FLAG,
	FEDERAL_REPORTABLE_FLAG,
	CO_EXT_ATTRIBUTE6,
	CO_EXT_ATTRIBUTE7,
	ORG_NAME
	)
SELECT
    GETDATE(),
	'$(targetCountryCode)',
	1,
	1,
	0,
	0,

	LEFT(LTRIM(RTRIM(CAST([$(SourceDatabaseName)].dbo.PROVEEDOR.codigo AS varchar(max)))),20),
	0,
	RTRIM(LTRIM(UPPER(nombre))),
	'$(targetCountryCode)' + LTRIM(RTRIM(CAST([$(SourceDatabaseName)].dbo.PROVEEDOR.idproveedor AS varchar(30))))+'-VE' AS SEGMENT1,
	'$(targetCountryCode)' + LTRIM(RTRIM(CAST([$(SourceDatabaseName)].dbo.PROVEEDOR.idproveedor AS varchar(30))))+'-VE' AS VENDOR_NAME_ALT,
	'VENDOR',
	RUC,
	'3-WAY',
	CASE IDPAGO
		WHEN 0 THEN 'CHECK'
		WHEN 1 THEN 'WIRE'
		WHEN 2 THEN 'EFT'
	END,
	CASE EXTERIOR
		WHEN 'S' THEN 'Y'
		WHEN 'N' THEN 'N'
	END,
	1 AS STATE_REPORTABLE_FLAG,
	0 AS SENSITIVE_VENDOR,
	1 AS ALLOW_AWT_FLAG,
	1 AS FEDERAL_REPORTABLE_FLAG,
	CONVERT(datetime, '01-01-2018', 105),
	CONVERT(datetime, '31-12-2049', 105),
	'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
FROM [$(SourceDatabaseName)].dbo.PROVEEDOR
WHERE [$(SourceDatabaseName)].dbo.PROVEEDOR.idproveedor = @itemCode
END;
GO

CREATE PROCEDURE dbo.buildVendorSiteContactRow_$(targetCountryCode)
(  
    @itemCode VARCHAR(16),
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN 
INSERT INTO O_PRE_VENDOR_SITE_CONTACT WITH (TABLOCK)
	(
	LAST_UPDATED,
	COUNTRY,
	LOCAL_VALIDATED,
	CROSSREF_VALIDATED,
	CLEANSED,
	RECOMMENDED,
	VENDOR_NUMBER,
	VENDOR_SITE_CODE,
	LAST_NAME,
	EMAIL_ADDRESS,
	ORG_NAME
	)
SELECT
    GETDATE(),
	'$(targetCountryCode)',
	1,
	1,
	0,
	0,
	'$(targetCountryCode)' + RTRIM(LTRIM(CAST(PROVEEDOR.idproveedor AS varchar(30))))+'-VE' AS VENDOR_NUMBER,
	RIGHT('$(targetCountryCode)' + RTRIM(LTRIM(CAST(PROVEEDOR.idproveedor AS varchar(30))))+'-VC',15) AS VENDOR_SITE_CODE,
	ISNULL(RTRIM(LTRIM(PROVEEDOR.CONTACTO)), 'FIX ME'),
	RTRIM(LTRIM(PROVEEDOR.EMAIL)),
	'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
FROM [$(SourceDatabaseName)].dbo.PROVEEDOR
LEFT JOIN [$(SourceDatabaseName)].dbo.CIUDADES
ON
[$(SourceDatabaseName)].dbo.PROVEEDOR.CIUDAD=CIUDADES.IdCiudad
WHERE [$(SourceDatabaseName)].dbo.PROVEEDOR.idproveedor = @itemCode

END;
GO

CREATE PROCEDURE dbo.buildVendorBankAccountRow_$(targetCountryCode)  
(  
    @itemCode VARCHAR(16),
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN 
INSERT INTO O_PRE_VENDOR_BANK_ACCOUNT WITH (TABLOCK)
	(
	LAST_UPDATED,
	COUNTRY,
	LOCAL_VALIDATED,
	CROSSREF_VALIDATED,
	CLEANSED,
	RECOMMENDED,
	VENDOR_NUMBER,
	BANK_NAME,
	BRANCH_NAME,
	BANK_NUMBER,
	BRANCH_NUMBER,
	BANK_HOME_COUNTRY,
	TAX_PAYER_ID,
	BANK_ACCOUNT_NAME,
	BANK_ACCOUNT_NUM,
	CURRENCY_CODE,
	START_DATE,
	ORG_NAME
	)
SELECT
    GETDATE(),
	'$(targetCountryCode)',
	1,
	1,
	0,
	0,
	'$(targetCountryCode)' + RTRIM(LTRIM(CAST([$(SourceDatabaseName)].dbo.PROVEEDOR.idproveedor AS varchar(30))))+'-VE' AS VENDOR_NUMBER,
	ISNULL([$(SourceDatabaseName)].dbo.BANCOSCOB.DESCRIP,'FIX ME'),
	'<unknown>',	
	'<unknown>',	
	'<unknown>',	
	'PA',
	[$(SourceDatabaseName)].dbo.PROVEEDOR.RUC,	
	'<unknown>',
	CASE  
		WHEN [$(SourceDatabaseName)].dbo.PROVEEDOR.CTABANCO IS NULL OR [$(SourceDatabaseName)].dbo.PROVEEDOR.CTABANCO=''  THEN '<unknown>'
		ELSE CAST( [$(SourceDatabaseName)].dbo.PROVEEDOR.CTABANCO AS VARCHAR(30))
	END,
	'USD',
	GETDATE(),
	'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
FROM [$(SourceDatabaseName)].dbo.PROVEEDOR
LEFT JOIN [$(SourceDatabaseName)].dbo.BANCOSCOB
ON
[$(SourceDatabaseName)].dbo.PROVEEDOR.BANCO=BANCOSCOB.IdBanco
WHERE [$(SourceDatabaseName)].dbo.PROVEEDOR.idproveedor = @itemCode
END;
GO




CREATE PROCEDURE dbo.loadVendorMasterData_$(targetCountryCode)

AS
BEGIN
	DELETE FROM dbo.O_PRE_VENDOR_HEADER WHERE COUNTRY='$(targetCountryCode)';
	DELETE FROM dbo.O_PRE_VENDOR_SITE WHERE COUNTRY='$(targetCountryCode)';
	DELETE FROM dbo.O_PRE_VENDOR_SITE_CONTACT WHERE COUNTRY='$(targetCountryCode)';
	DELETE FROM dbo.O_PRE_VENDOR_BANK_ACCOUNT WHERE COUNTRY='$(targetCountryCode)';

	DELETE FROM COM_AUDIT_LOGS WHERE EVENT LIKE '%Vendors Master Data partial load%' AND COUNTRY='$(targetCountryCode)'

	DECLARE @logThreshold int = 100
	DECLARE @count int = 0
	DECLARE @total int = 0

	select distinct IDPROVEEDOR 
	INTO #TMP_RECOMMENDED
	from [$(SourceDatabaseName)].dbo.titorden as a
		inner join [$(SourceDatabaseName)].dbo.detorden as b on a.IDORDEN=b.IDORDEN
		where a.ESTADO in ('A','N') and b.ESTADORECEP in ('N','P')
	----TRANSACCIONES ABIERTAS - FACTURAS PENDIENTES 
	UNION ALL
	SELECT DISTINCT c.idproveedor
		FROM [$(SourceDatabaseName)].dbo.CTACTEPROV as a
		inner join [$(SourceDatabaseName)].dbo.TITCOMPRAS as b on a.IDCOMPRA=b.IDCOMPRA
		inner join [$(SourceDatabaseName)].dbo.PROVEEDOR as c on b.IDPROVEEDOR=c.idproveedor
	WHERE 
	a.FECHA<=GETDATE()
	AND TIPDOC IN (SELECT  TIPDOCCONT FROM[$(SourceDatabaseName)].dbo.TABDOCFACT WHERE COMPRA='S') 
	GROUP BY c.idproveedor,c.NOMBRE,a.IDCOMPRA,TIPDOC,SERDOC,NUMDOC,b.FECHA,VENCIM,MONEDA 
	HAVING ROUND(SUM(CARGO-ABONO),2)<>0

	/**
	----FACTURAS ULTIMOS 1 A�O
	UNION ALL
	select distinct IDPROVEEDOR 
	from [$(SourceDatabaseName)].dbo.TITCOMPRAS where PERIODO in (2017)
	----ORDENES DE COMPRA ULTIMOS 1 A�O
	UNION ALL
	select DISTINCT IDPROVEEDOR FROM [$(SourceDatabaseName)].dbo.TITORDEN 
	 where YEAR(FECHA) in (2017)
	----MOVIMIENTOS CONTABLES DE 1 A�O
	UNION ALL
	SELECT DISTINCT C.idproveedor 
	FROM [$(SourceDatabaseName)].dbo.COM AS A
		INNER JOIN [$(SourceDatabaseName)].dbo.DETTABLA AS B ON A.CLAVE=B.CODIGO AND B.TABLA=2
		INNER JOIN [$(SourceDatabaseName)].dbo.PROVEEDOR AS C ON A.CLAVE =C.CODIGO
	WHERE YEAR(A.FECHA) IN (2017) AND A.CLAVE <> ''
	**/

	-- 3. avoids duplicates on filtered group
	--
	select distinct IDPROVEEDOR INTO #TMP_UNIQUE FROM [$(SourceDatabaseName)].dbo.PROVEEDOR



	-- 4. create a cursor per distinct @itemCode group
	-- one cursor per temporary table, per use case
	-- calls associated row building procedures in sequence

	DECLARE @unique_id VARCHAR(16)

	DECLARE UNIQUE_FUNCTIONAL CURSOR FOR   
	SELECT IDPROVEEDOR  
	FROM #TMP_UNIQUE;

	SELECT @total=COUNT(IDPROVEEDOR) FROM #TMP_UNIQUE;

	OPEN UNIQUE_FUNCTIONAL;

	FETCH NEXT FROM UNIQUE_FUNCTIONAL   
	INTO @unique_id;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC buildVendorHeaderRow_$(targetCountryCode) @unique_id
		EXEC buildVendorSiteRow_$(targetCountryCode) @unique_id
		EXEC buildVendorSiteContactRow_$(targetCountryCode) @unique_id
		EXEC buildVendorBankAccountRow_$(targetCountryCode) @unique_id

		IF (@count%@logThreshold = 0)
		BEGIN
			INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,SNAPSHOT_DATE)
			VALUES('$(targetCountryCode)', 'script', 'Vendors Master Data partial load ' + CAST(@count AS VARCHAR(max))+'/'+CAST(@total AS VARCHAR(max)), GETDATE(), @count, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));
		END
		
		SET @count = @count + 1


		FETCH NEXT FROM UNIQUE_FUNCTIONAL   
		INTO @unique_id;
	END;

	CLOSE UNIQUE_FUNCTIONAL;

	DEALLOCATE UNIQUE_FUNCTIONAL;

	-- 5. run recommendations based on functional parameters
	UPDATE O_PRE_VENDOR_HEADER
	SET RECOMMENDED=1
	WHERE SEGMENT1 IN
	(
		select distinct '$(targetCountryCode)' + RTRIM(LTRIM(CAST(idproveedor AS varchar(30))))+'-VE' AS SEGMENT1
		from #TMP_RECOMMENDED 
	)

	UPDATE O_PRE_VENDOR_SITE
	SET RECOMMENDED=1
	WHERE VENDOR_NUMBER IN
	(
		select distinct '$(targetCountryCode)' + RTRIM(LTRIM(CAST(idproveedor AS varchar(30))))+'-VE' AS VENDOR_NUMBER
		from #TMP_RECOMMENDED 
	)

	UPDATE O_PRE_VENDOR_SITE_CONTACT
	SET RECOMMENDED=1
	WHERE VENDOR_NUMBER IN
	(
		select distinct '$(targetCountryCode)' + RTRIM(LTRIM(CAST(idproveedor AS varchar(30))))+'-VE' AS VENDOR_NUMBER
		from #TMP_RECOMMENDED 
	)

	UPDATE O_PRE_VENDOR_BANK_ACCOUNT
	SET RECOMMENDED=1
	WHERE VENDOR_NUMBER IN
	(
		select distinct '$(targetCountryCode)' + RTRIM(LTRIM(CAST(idproveedor AS varchar(30))))+'-VE' AS VENDOR_NUMBER
		from #TMP_RECOMMENDED 
	)


-- 5.1 identify possible employees by TIPPERSONA

	UPDATE O_PRE_VENDOR_HEADER
	SET VENDOR_TYPE_LOOKUP_CODE='EMPLOYEE'
	WHERE SEGMENT1 IN
	(
		select distinct '$(targetCountryCode)' + RTRIM(LTRIM(CAST(idproveedor AS varchar(30))))+'-VE' AS SEGMENT1
		FROM [$(SourceDatabaseName)].dbo.PROVEEDOR
		WHERE TIPPERSONA = 1
	)

	EXEC dbo.rebuildPreStageVendorIndex

	DELETE FROM COM_AUDIT_LOGS WHERE EVENT LIKE '%Vendors Master Data partial load%' AND COUNTRY='$(targetCountryCode)'

	SELECT @count=COUNT(*) FROM O_PRE_VENDOR_HEADER WHERE COUNTRY='$(targetCountryCode)'

	INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,SNAPSHOT_DATE)
	VALUES('$(targetCountryCode)', 'script', 'Vendors Master Data loaded from $(sourceDatabaseName) @ ' + dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'), GETDATE(), @count, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));

	SELECT @count=COUNT(*) FROM O_PRE_VENDOR_HEADER WHERE COUNTRY='$(targetCountryCode)' AND RECOMMENDED=1

	INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,SNAPSHOT_DATE)
	VALUES('$(targetCountryCode)', 'script', 'Vendors Master Data added recommendations from $(sourceDatabaseName) @ ' + dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'), GETDATE(), @count, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));

END
GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,SNAPSHOT_DATE)
VALUES('$(targetCountryCode)', 'script', 'Vendors Master Data load procedures redefined', GETDATE(), dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));




