--:setvar country MX
--:setvar cutoffDate 31-01-2019
--:setvar timestamp 201901292109

SET ANSI_WARNINGS OFF
GO

SET NOCOUNT ON;

DECLARE @cutoffDate DATE;
SET @cutoffDate=CONVERT(date,'$(cutoffDate)',103)

DECLARE @snapshotDate DATE;
SET @snapshotDate=dbo.udf_EncodeSnapshotDate('$(country)')

DECLARE @itemCount INT;

PRINT 'country = $(country)' + CHAR(13)+CHAR(10);
PRINT 'cutoffDate = $(cutoffDate)' +  CHAR(13)+CHAR(10);
PRINT 'snapshotDate = ' + CONVERT(varchar, @snapshotDate, 103) + CHAR(13)+CHAR(10);
PRINT 'timestamp = $(timestamp)';


:r "includes/generateItemHeader.sql"

PRINT CHAR(13)+CHAR(10) + '---------------- ITEM_RAW_HEADER ----------------------'+CHAR(13)+CHAR(10);


SELECT COUNT(*) AS ITEM_RAW_HEADER_COUNT FROM #ITEM_RAW_HEADER

PRINT CHAR(13)+CHAR(10)


PRINT CHAR(13)+CHAR(10) + '---------------- DUPLICATED ITEMS ----------------------'+CHAR(13)+CHAR(10);

SELECT itemNumber, COUNT(*) AS RECORDS_ON_ITEM_RAW_HEADER FROM #ITEM_RAW_HEADER GROUP BY itemNumber HAVING COUNT(*)>1 ORDER BY RECORDS_ON_ITEM_RAW_HEADER DESC

PRINT CHAR(13)+CHAR(10)


SELECT ORIG_ITEM_NUMBER,COUNT(*) FROM O_STG_ITEM_HEADER S WHERE S.COUNTRY='$(country)' GROUP BY ORIG_ITEM_NUMBER HAVING COUNT(*)>1 ORDER BY COUNT(*) DESC 


PRINT CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10) + '---------------- ITEM_HEADER ----------------------'+CHAR(13)+CHAR(10);

SELECT * FROM #ITEM_HEADER ORDER BY itemNumber, organizationCode

PRINT CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10) + '---------------- ITEM_CATEGORY (ALL BUT CO)----------------------'+CHAR(13)+CHAR(10);

:r "includes/generateItemCategory.sql"

SELECT COUNT(*) AS ITEM_CATEGORY_ALL_BUT_CO_COUNT FROM #ITEM_CATEGORY

SELECT * FROM #ITEM_CATEGORY

PRINT CHAR(13)+CHAR(10) + '---------------- ITEM_CATEGORY (ONLY CO)----------------------'+CHAR(13)+CHAR(10);

:r "includes/generateItemCategory_CO.sql"

SELECT COUNT(*) AS ITEM_CATEGORY_ONLY_CO_COUNT FROM #ITEM_CATEGORY

SELECT * FROM #ITEM_CATEGORY


PRINT CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10) + '---------------- ITEM_CROSS_REF ----------------------'+CHAR(13)+CHAR(10);

:r "includes/generateItemCrossRef.sql"

SELECT COUNT(*) AS #ITEM_CROSS_REF_COUNT FROM #ITEM_CROSS_REF

SELECT * FROM #ITEM_CROSS_REF ORDER BY itemNumber

PRINT CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10) + '---------------- ITEM_MFG_PART_NUM ----------------------'+CHAR(13)+CHAR(10);

:r "includes/generateItemMfgPartNum.sql"

SELECT COUNT(*) AS ITEM_MFG_PART_NUM_COUNT FROM #ITEM_MFG_PART_NUM

SELECT itemNumber, COUNT(*) AS RECORDS FROM #ITEM_MFG_PART_NUM GROUP BY itemNumber ORDER BY RECORDS DESC

SELECT manufacturer, COUNT(*) AS RECORDS FROM #ITEM_MFG_PART_NUM GROUP BY manufacturer ORDER BY RECORDS DESC

SELECT * FROM #ITEM_MFG_PART_NUM ORDER BY itemNumber

SELECT COUNT(*) AS ITEM_MFG_PART_NUM_NOT_IN_HEADER FROM #ITEM_MFG_PART_NUM a WHERE a.itemNumber NOT IN (SELECT itemNumber FROM #ITEM_HEADER)  


PRINT CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10) + '---------------- ITEM_TRANS_DEF_SUB_INV ----------------------'+CHAR(13)+CHAR(10);

:r "includes/generateItemTransDefSubInv.sql"

SELECT * FROM #ITEM_TRANS_DEF_SUB_INV ORDER BY itemNumber, subInvOrgCode

PRINT CHAR(13)+CHAR(10)

SELECT COUNT(*) AS ITEM_TRANS_DEF_SUB_INV_COUNT FROM #ITEM_TRANS_DEF_SUB_INV

SELECT COUNT(*) AS ITEM_TRANS_DEF_SUB_INV_NOT_IN_HEADER FROM #ITEM_TRANS_DEF_SUB_INV a WHERE a.itemNumber NOT IN (SELECT itemNumber FROM #ITEM_HEADER)  

SELECT itemNumber, COUNT(*) AS RECORDS FROM #ITEM_TRANS_DEF_SUB_INV GROUP BY itemNumber ORDER BY RECORDS DESC


PRINT CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10) + '---------------- ITEM_TRANS_DEF_LOC ----------------------'+CHAR(13)+CHAR(10);

:r "includes/generateItemTransDefLoc.sql"

SELECT * FROM #ITEM_TRANS_DEF_LOC ORDER BY itemNumber, subInvOrgCode

PRINT CHAR(13)+CHAR(10)

SELECT COUNT(*) AS ITEM_TRANS_DEF_LOC_COUNT FROM #ITEM_TRANS_DEF_LOC

SELECT COUNT(*) AS ITEM_TRANS_DEF_LOC_COUNT_NOT_IN_HEADER FROM #ITEM_TRANS_DEF_LOC a WHERE a.itemNumber NOT IN (SELECT itemNumber FROM #ITEM_HEADER)  

SELECT itemNumber, COUNT(*) AS RECORDS FROM #ITEM_TRANS_DEF_LOC GROUP BY itemNumber ORDER BY itemNumber, RECORDS DESC

SELECT itemNumber, locatorName, COUNT(*) AS RECORDS FROM #ITEM_TRANS_DEF_LOC GROUP BY itemNumber, locatorName ORDER BY itemNumber, locatorName, RECORDS DESC

PRINT CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10) + '---------------- DISTINCT LOCATORS WITH RECORD COUNT ----------------------'+CHAR(13)+CHAR(10);

SELECT locatorName, COUNT(*) AS RECORDS FROM #ITEM_TRANS_DEF_LOC GROUP BY locatorName ORDER BY RECORDS DESC 
