SET ANSI_WARNINGS OFF
GO

SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#ITEM_CATEGORY') IS NOT NULL DROP TABLE #ITEM_CATEGORY

SELECT
b.itemNumber									as itemNumber,
'TKE PO Item Category'							as categorySetName,
ISNULL(a.CONCAT_CATEGORY_NAME + '.0', '10039800.0')		as concatCategoryName,
'L$(country)'									as organizationCode,
dbo.udf_EncodeOrgName('$(country)')				as orgName
INTO #ITEM_CATEGORY
FROM
#ITEM_RAW_HEADER b
LEFT JOIN O_STG_ITEM_CATEGORY a
ON (b.origItemNumber=a.ORIG_ITEM_NUMBER)
