IF OBJECT_ID('tempdb..#ITEM_CROSS_REF') IS NOT NULL DROP TABLE #ITEM_CROSS_REF

SELECT
b.[ITEM_NUMBER]									as itemNumber
,b.[CROSS_REFERENCE_TYPE]                       as crossReferenceType
,b.[CROSS_REFERENCE]                            as crossReference
,b.[DESCRIPTION]                                as description
,b.[ORGANIZATION_CODE]                          as organizationCode
,dbo.udf_EncodeOrgName('$(country)')			as orgName
INTO #ITEM_CROSS_REF
FROM #ITEM_RAW_HEADER a
inner join DBO.[O_STG_ITEM_CROSS_REF_ALT] b ON a.origItemNumber=b.ORIG_ITEM_NUMBER
