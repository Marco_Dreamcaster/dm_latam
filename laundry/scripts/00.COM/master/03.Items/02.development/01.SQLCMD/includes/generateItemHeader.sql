--:setvar country CL

SET ANSI_WARNINGS OFF
GO

SET NOCOUNT ON;

IF '$(country)' IN ('PE','CL','AR','UY','PY') 
BEGIN
IF OBJECT_ID('tempdb..#FIX_ITEM_UOM_FOR_PCAUP') IS NOT NULL DROP TABLE #FIX_ITEM_UOM_FOR_PCAUP

SELECT
DISTINCT
d.ORIG_ITEM_NUMBER,
a.UNIMED,
c.SIGLA_UOM,
b.DESCRIP,
c.ORACLE_UOM
INTO #FIX_ITEM_UOM_FOR_PCAUP
FROM
TK$(country)_LATEST.dbo.PRODUCTOS a
INNER JOIN TK$(country)_LATEST.dbo.UNIDADMEDIDA b
ON a.UNIMED=b.ID
INNER JOIN O_STG_UOM_MAPPING c
ON b.VALOR=c.SIGLA_UOM AND c.COUNTRY='$(country)'
INNER JOIN O_STG_ITEM_HEADER d
ON d.ORIG_ITEM_NUMBER=dbo.udf_EncodeItemReference('$(country)',a.PRODUCTO)
WHERE
d.[COUNTRY]='$(country)' AND
d.[DISMISSED]=0 AND
d.[LOCAL_VALIDATED]=1 AND
d.[CROSSREF_VALIDATED]=1 AND
d.[MERGED_ID] IS NULL


MERGE INTO O_STG_ITEM_HEADER T
   USING #FIX_ITEM_UOM_FOR_PCAUP S 
      ON T.ORIG_ITEM_NUMBER COLLATE DATABASE_DEFAULT = S.ORIG_ITEM_NUMBER COLLATE DATABASE_DEFAULT AND
		 T.COUNTRY='$(country)'
WHEN MATCHED THEN
   UPDATE 
      SET
		T.UOM_CODE = S.ORACLE_UOM;


IF OBJECT_ID('tempdb..#FIX_PREVIOUSLY_MIGRATED_UOM') IS NOT NULL DROP TABLE #FIX_PREVIOUSLY_MIGRATED_UOM

SELECT
a.ID,
b.UOM_CODE
INTO #FIX_PREVIOUSLY_MIGRATED_UOM
FROM
O_STG_ITEM_HEADER a
INNER JOIN O_STG_ITEM_HEADER b
ON a.MERGED_ID=b.ID
WHERE
a.COUNTRY IN ('PE','CL','AR','UY','PY')
AND a.MERGED_ID IS NOT NULL
AND b.COUNTRY IN ('MX','HN','GT','CR','SV','NI','CO','PA')
AND a.UOM_CODE<>'ea'

SELECT * FROM #FIX_PREVIOUSLY_MIGRATED_UOM

MERGE INTO O_STG_ITEM_HEADER T
   USING #FIX_PREVIOUSLY_MIGRATED_UOM S 
      ON T.ID = S.ID
WHEN MATCHED THEN
   UPDATE 
      SET
		T.UOM_CODE = S.UOM_CODE;


END
 




IF OBJECT_ID('tempdb..#ITEM_RAW_HEADER') IS NOT NULL DROP TABLE #ITEM_RAW_HEADER

SELECT b.[id]									as id
,b.[ORIG_ITEM_NUMBER]							as origItemNumber
,c.[ITEM_NUMBER]								as itemNumber
,b.[DESCRIPTION]								as description
,b.[DESCRIPTION_ESA]							as descriptionEsa
,b.[ORGANIZATION_CODE]							as organizationCode
,b.[TEMPLATE_NAME]								as templateName
,CASE
WHEN (b.PRICE_MANUAL_OVERRIDE=1 AND b.LIST_PRICE_PER_UNIT>0.01) THEN b.LIST_PRICE_PER_UNIT
ELSE CALCULATED_LIST_PRICE_PER_UNIT END	 		as listPricePerUnit
,b.CALCULATED_QUANTITY							as quantity
,b.PRICE_MANUAL_OVERRIDE						as priceManualOverride
,b.[UOM_CODE]									as uomCode
,b.[WEIGHT_UOM_CODE]							as weightUomCode
,b.[UNIT_WEIGHT]								as unitWeight
,b.[VOLUME_UOM_CODE]							as volumeUomCode
,b.[UNIT_VOLUME]								as unitVolume
,b.[MIN_MINMAX_QUANTITY]						as minMinMaxQuantity
,b.[MAX_MINMAX_QUANTITY]						as maxMinMaxQuantity
,b.[FULL_LEAD_TIME]								as fullLeadTime
,b.[FIXED_LOT_MULTIPLIER]						as fixedLotMultiplier
,b.[FIXED_ORDER_QUANTITY]						as fixedOrderQuantity
,b.[MINIMUM_ORDER_QUANTITY]						as minOrderQuantity
,b.[MAXIMUM_ORDER_QUANTITY]						as maxOrderQuantity
,b.[LONG_DESCRIPTION]							as longDescription
,b.[ITEM_INV_APPLICATION]						as itemInvApplication
,b.[TRANSACTION_CONDITION_CODE]					as transactionConditionCode
,b.[ADJUSTMENT_ACCOUNT]							as adjustmentAccount
,b.[CORRECTION_ACCOUNT]							as correctionAccount
,b.[SALES_COST_ACCOUNT]							as salesCostAccount
,b.[FOUR_DIGIT_CODE]							as fourDigitCode
,dbo.udf_EncodeOrgName('$(country)')				as orgName
,b.[PE_EXT_ATTRIBUTE1]							as peExtAttribute1
,b.[PE_EXT_ATTRIBUTE2]							as peExtAttribute2
,b.[MERGED_ID]									as mergedId
,b.[CHILD_LESS]									as childLess
,b.[SAME_COUNTRY_PARENT]						as sameCountryParent
,b.[COUNTRY]									as originCountry
INTO #ITEM_RAW_HEADER
FROM DBO.[O_STG_ITEM_HEADER] b
INNER JOIN DBO.O_STG_ITEM_CROSS_REF_ALT c
ON b.ORIG_ITEM_NUMBER = c.ORIG_ITEM_NUMBER
WHERE
b.[COUNTRY]='$(country)' AND
b.[DISMISSED]=0 AND
b.[LOCAL_VALIDATED]=1 AND
b.[CROSSREF_VALIDATED]=1

--SELECT * FROM #ITEM_RAW_HEADER

IF OBJECT_ID('tempdb..#ITEM_HEADER') IS NOT NULL DROP TABLE #ITEM_HEADER

SELECT
b.itemNumber																as itemNumber
,RTRIM(LTRIM(b.description))												as description
,RTRIM(LTRIM(b.descriptionEsa))												as descriptionEsa
,'TKE'																		as organizationCode
,RTRIM(LTRIM(b.templateName))												as templateName
,CASE WHEN (b.listPricePerUnit IS NULL OR b.listPricePerUnit<0.01)
THEN '0.01'
ELSE RTRIM(LTRIM(STR(b.listPricePerUnit,10,2))) END 						as listPricePerUnit
,ISNULL(b.uomCode,'')														as uomCode
,''			 																as weightUomCode
,''																			as unitWeight
,''																			as volumeUomCode
,''																			as unitVolume
,''																			as minMinMaxQuantity
,''																			as maxMinMaxQuantity
,''																			as fullLeadTime
,''																			as fixedLotMultiplier
,''																			as fixedOrderQuantity
,''																			as minOrderQuantity
,''																			as maxOrderQuantity
,RTRIM(LTRIM(ISNULL(b.longDescription,'')))		as longDescription
,CASE
WHEN (b.originCountry='CO' OR b.originCountry='AR')
THEN 'INV'
ELSE '' END										as itemInvApplication
,CASE
WHEN (b.originCountry='CO' AND b.templateName LIKE '%LA00%') THEN 'CO_SERVICIOS_13'
WHEN (b.originCountry='CO' AND b.templateName NOT LIKE '%LA00%') THEN 'CO_BENS_27'
WHEN (b.originCountry='AR' AND b.templateName LIKE '%LA00%') THEN 'AR_SERVICIOS'
WHEN (b.originCountry='AR' AND b.templateName NOT LIKE '%LA00%') THEN 'AR_BIENES'
ELSE '' END	 									as transactionConditionCode
,ISNULL(b.adjustmentAccount,'')					as adjustmentAccount
,ISNULL(b.correctionAccount,'')					as correctionAccount
,ISNULL(b.salesCostAccount,'')					as salesCostAccount
,CASE
WHEN (b.originCountry='AR') THEN '0010'
ELSE ''
END												as fourDigitCode
,dbo.udf_EncodeOrgName('$(country)')				as orgName
,CASE
WHEN (b.originCountry='PE') THEN b.peExtAttribute1
ELSE '' END	 									as peExtAttribute1
,CASE
WHEN (b.originCountry='PE') THEN b.peExtAttribute2
ELSE '' END	 									as peExtAttribute2
INTO
#ITEM_HEADER
FROM
#ITEM_RAW_HEADER b

--SELECT * FROM #ITEM_RAW_HEADER



IF OBJECT_ID('tempdb..#CONFIGURED_SUB_INVS') IS NOT NULL DROP TABLE #CONFIGURED_SUB_INVS

SELECT DISTINCT SUB_INV_ORG_CODE INTO #CONFIGURED_SUB_INVS FROM GLLOCALES WHERE COUNTRY='$(country)' AND RTRIM(LTRIM(SUB_INV_ORG_CODE))<>''

IF NOT EXISTS( SELECT SUB_INV_ORG_CODE FROM #CONFIGURED_SUB_INVS WHERE SUB_INV_ORG_CODE='L$(country)' )
BEGIN
	INSERT INTO #CONFIGURED_SUB_INVS VALUES ('L$(country)')
END

DECLARE @subInvOrgCode varchar(3)

DECLARE CONFIGURED_SUB_INVS_CURSOR CURSOR FOR   
SELECT SUB_INV_ORG_CODE FROM #CONFIGURED_SUB_INVS

OPEN CONFIGURED_SUB_INVS_CURSOR;

FETCH NEXT FROM CONFIGURED_SUB_INVS_CURSOR   
INTO @subInvOrgCode;

WHILE @@FETCH_STATUS = 0
BEGIN

	INSERT INTO
	#ITEM_HEADER

	SELECT
	b.itemNumber									as itemNumber
	,RTRIM(LTRIM(b.description))					as description
	,RTRIM(LTRIM(b.descriptionEsa))					as descriptionEsa
	,@subInvOrgCode									as organizationCode
	,RTRIM(LTRIM(b.templateName))					as templateName
	,CASE WHEN (b.listPricePerUnit IS NULL OR b.listPricePerUnit<0.01)
	THEN '0.01'
	ELSE RTRIM(LTRIM(STR(b.listPricePerUnit,10,2))) END as listPricePerUnit
	,ISNULL(b.uomCode,'')							as uomCode
	,''			 									as weightUomCode
	,''												as unitWeight
	,''												as volumeUomCode
	,''												as unitVolume
	,''												as minMinMaxQuantity
	,''												as maxMinMaxQuantity
	,''												as fullLeadTime
	,''												as fixedLotMultiplier
	,''												as fixedOrderQuantity
	,''												as minOrderQuantity
	,''												as maxOrderQuantity
	,RTRIM(LTRIM(ISNULL(b.longDescription,'')))		as longDescription
	,CASE WHEN (b.originCountry='CO' OR b.originCountry='AR')
	THEN 'INV'
	ELSE '' END										as itemInvApplication
	,CASE
	WHEN (b.originCountry='CO' AND b.templateName LIKE '%LA00%') THEN 'CO_SERVICIOS_13'
	WHEN (b.originCountry='CO' AND b.templateName NOT LIKE '%LA00%') THEN 'CO_BENS_27'
	WHEN (b.originCountry='AR' AND b.templateName LIKE '%LA00%') THEN 'AR_SERVICIOS'
	WHEN (b.originCountry='AR' AND b.templateName NOT LIKE '%LA00%') THEN 'AR_BIENES'
	ELSE '' END	 									as transactionConditionCode
	,ISNULL(b.adjustmentAccount,'')					as adjustmentAccount
	,ISNULL(b.correctionAccount,'')					as correctionAccount
	,ISNULL(b.salesCostAccount,'')					as salesCostAccount
	,CASE
	WHEN (b.originCountry='AR') THEN '0010'
	ELSE ''
	END												as fourDigitCode
	,dbo.udf_EncodeOrgName('$(country)')				as orgName
	,CASE
	WHEN (b.originCountry='PE') THEN b.peExtAttribute1
	ELSE ''
	END												as peExtAttribute1
	,CASE
	WHEN (b.originCountry='PE') THEN b.peExtAttribute2
	ELSE ''
	END												as peExtAttribute2
	FROM
	#ITEM_RAW_HEADER b

FETCH NEXT FROM CONFIGURED_SUB_INVS_CURSOR   
INTO @subInvOrgCode;
END;

CLOSE CONFIGURED_SUB_INVS_CURSOR;

DEALLOCATE CONFIGURED_SUB_INVS_CURSOR;

--SELECT * FROM #ITEM_HEADER a WHERE a.itemNumber ='09.09.000/31'

UPDATE #ITEM_HEADER SET peExtAttribute1='', peExtAttribute2='' WHERE organizationCode='TKE'



