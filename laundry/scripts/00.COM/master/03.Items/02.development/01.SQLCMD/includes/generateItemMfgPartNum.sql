SET ANSI_WARNINGS OFF
GO

SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#ITEM_MFG') IS NOT NULL DROP TABLE #ITEM_MFG

SELECT b.[id]												as id
,b.origItemNumber											as origItemNumber
,b.itemNumber												as itemNumber
,LEFT(a.MANUFACTURER+' '+a.description,30)					as manufacturer
,a.DESCRIPTION												as description
,RTRIM(LTRIM(REPLACE(a.PART_NUMBER, '(FIXME)', '')))		as partNumber
,'L$(country)'												as organizationCode
,dbo.udf_EncodeOrgName('$(country)')						as orgName
,b.mergedId													as mergedId
,b.childLess												as childLess
,b.sameCountryParent										as sameCountryParent
INTO #ITEM_MFG
FROM 
#ITEM_RAW_HEADER b 
LEFT JOIN O_STG_ITEM_MFG_PART_NUM a
ON (b.origItemNumber=a.ORIG_ITEM_NUMBER)
WHERE b.mergedId IS NULL

UNION ALL

SELECT b.[id]												as id
,b.origItemNumber											as origItemNumber
,b.itemNumber												as itemNumber
,LEFT(a.MANUFACTURER+' '+a.description,30)					as manufacturer
,a.DESCRIPTION												as description
,RTRIM(LTRIM(REPLACE(a.PART_NUMBER, '(FIXME)', '')))		as partNumber
,'L$(country)'												as organizationCode
,dbo.udf_EncodeOrgName('$(country)')						as orgName
,b.mergedId													as mergedId
,b.childLess												as childLess
,b.sameCountryParent										as sameCountryParent
FROM
#ITEM_RAW_HEADER b
INNER JOIN O_STG_ITEM_HEADER c
ON (b.mergedId=c.ID)
LEFT JOIN O_STG_ITEM_MFG_PART_NUM a
ON (c.ORIG_ITEM_NUMBER=a.ORIG_ITEM_NUMBER)
WHERE b.mergedId IS NOT NULL
AND a.DISMISSED<>1



IF OBJECT_ID('tempdb..#ITEM_MFG_PART_NUM') IS NOT NULL DROP TABLE #ITEM_MFG_PART_NUM

SELECT
a.manufacturer,
a.description,
CASE WHEN RTRIM(LTRIM(a.partNumber))=''
THEN a.itemNumber
ELSE a.partNumber END 						as partNumber,
a.itemNumber,
a.orgName
INTO #ITEM_MFG_PART_NUM
FROM #ITEM_MFG a

