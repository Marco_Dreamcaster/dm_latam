SET ANSI_WARNINGS OFF
GO

SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#CONFIGURED_SUB_INVS') IS NOT NULL DROP TABLE #CONFIGURED_SUB_INVS

SELECT DISTINCT SUB_INV_ORG_CODE INTO #CONFIGURED_SUB_INVS FROM GLLOCALES WHERE COUNTRY='$(country)' AND RTRIM(LTRIM(SUB_INV_ORG_CODE))<>''

IF OBJECT_ID('tempdb..#ITEM_RAW_TRANS_DEF_LOC') IS NOT NULL DROP TABLE #ITEM_RAW_TRANS_DEF_LOC

DECLARE @defaultLocatorName varchar(max);
SELECT @defaultLocatorName=a.defaultLocatorName FROM GLConfigEstandar a	WHERE country='$(country)'

SELECT
b.origItemNumber											as origItemNumber,
b.itemNumber												as itemNumber
,a.SUB_INV_ORG_CODE											as subInvOrgCode
,'DEP01'													as subInventoryName
,@defaultLocatorName										as locatorName
,dbo.udf_EncodeOrgName('$(country)')				as orgName
INTO #ITEM_RAW_TRANS_DEF_LOC
FROM
#ITEM_RAW_HEADER b
INNER JOIN #CONFIGURED_SUB_INVS a
ON (1=1)

/**
FOR DEBUG USE

When MERGE fails due to more than one source source rows matching the given conditions

La instrucci�n MERGE intent� UPDATE o DELETE en la misma fila m�s de una vez.
Esto sucede cuando una fila de destino coincide con m�s de una fila de origen.
Una instrucci�n MERGE no puede UPDATE/DELETE la misma fila de la tabla de destino varias veces.
Refine la cl�usula ON para asegurarse de que la fila de destino coincide s�lo con una fila de origen, o utilice la cl�usula GROUP BY para agrupar las filas de origen.

SELECT ORIG_ITEM_NUMBER, T.subInvOrgCode, COUNT(*) FROM #ITEM_RAW_TRANS_DEF_LOC T
   JOIN O_STG_ITEM_TRANS_DEF_LOC S 
      ON T.origItemNumber = S.ORIG_ITEM_NUMBER AND
	     T.subInvOrgCode COLLATE DATABASE_DEFAULT = S.ORGANIZATION_CODE COLLATE DATABASE_DEFAULT AND
		 S.LOCATOR_NAME LIKE '[0-9][0-9][0-9][0-9].[0-9][0-9][0-9].[0-9][0-9][0-9]'
GROUP BY ORIG_ITEM_NUMBER,T.subInvOrgCode
HAVING COUNT(*)>1
**/


IF OBJECT_ID('tempdb..#UBICAPRODALM_LOC') IS NOT NULL DROP TABLE #UBICAPRODALM_LOC

SELECT 
DISTINCT
'$(country)'+ a.PRODUCTO as ORIG_ITEM_NUMBER,
MAX(a.UBICACION) AS LOCATOR_NAME,
c.subInvOrgCode AS ORGANIZATION_CODDE
INTO
#UBICAPRODALM_LOC
FROM TK$(country)_LATEST..UBICAPRODALM a,
TK$(country)_LATEST..PRODUCTOS b,
#CONFIG_ALMACEN c
WHERE
c.idAlmacen = a.IDALMACEN
AND b.PRODUCTO = a.PRODUCTO 
--AND a.PRODUCTO LIKE '%3W.0591.MJ.5%'
AND a.UBICACION LIKE '[0-9][0-9][0-9][0-9].[0-9][0-9][0-9].[0-9][0-9][0-9]'
GROUP BY 
a.PRODUCTO,
c.subInvOrgCode

--SELECT * FROM #UBICAPRODALM_LOC 

MERGE INTO #ITEM_RAW_TRANS_DEF_LOC T
   USING #UBICAPRODALM_LOC S 
      ON RTRIM(LTRIM(T.origItemNumber)) = RTRIM(LTRIM(S.ORIG_ITEM_NUMBER))
		 AND RTRIM(LTRIM(T.subInvOrgCode)) COLLATE DATABASE_DEFAULT = RTRIM(LTRIM(S.ORGANIZATION_CODDE)) COLLATE DATABASE_DEFAULT
WHEN MATCHED THEN
   UPDATE 
      SET T.locatorName = S.LOCATOR_NAME;

IF OBJECT_ID('tempdb..#ITEM_TRANS_DEF_LOC') IS NOT NULL DROP TABLE #ITEM_TRANS_DEF_LOC

SELECT
b.itemNumber												as itemNumber
,b.subInvOrgCode											as subInvOrgCode
,'DEP01'													as subInventoryName
,b.locatorName												as locatorName
,dbo.udf_EncodeOrgName('$(country)')						as orgName
INTO #ITEM_TRANS_DEF_LOC
FROM
#ITEM_RAW_TRANS_DEF_LOC b
