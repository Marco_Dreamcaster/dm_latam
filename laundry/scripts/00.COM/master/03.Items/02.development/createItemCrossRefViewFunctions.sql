IF OBJECT_ID('dbo.udf_GetCountriesFor', 'FN') IS NOT NULL
DROP FUNCTION dbo.udf_GetCountriesFor;
GO

CREATE FUNCTION dbo.udf_GetCountriesFor ( @itemNumber VARCHAR(240) )
RETURNS VARCHAR(100)
AS
BEGIN
DECLARE @ItemNumberAtCountry varchar(100)
SET
@ItemNumberAtCountry=STUFF(
(SELECT
DISTINCT
SUBSTRING(a.ORIG_ITEM_NUMBER,0,3) + ','
FROM O_STG_ITEM_CROSS_REF_ALT a
WHERE a.ITEM_NUMBER=@itemNumber FOR XML PATH('')),1,0,'')

SET @ItemNumberAtCountry = SUBSTRING(@ItemNumberAtCountry,0,LEN(@ItemNumberAtCountry))

RETURN @ItemNumberAtCountry

END;
GO

PRINT dbo.udf_GetCountriesFor('3Z.0598.DG.1')
