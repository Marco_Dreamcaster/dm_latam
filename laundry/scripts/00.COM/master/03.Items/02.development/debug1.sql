IF OBJECT_ID('DM_LATAM_DEV.dbo.execDebug1', 'P') IS NOT NULL
  DROP PROCEDURE dbo.execDebug1;
GO

CREATE PROCEDURE dbo.execDebug1
AS
BEGIN

	PRINT 'EXEC DEBUG1 @' + CAST(GETDATE() AS VARCHAR(max));

	DECLARE @latestDate datetime2

	SELECT TOP 1 @latestDate = FECHA FROM TKPA_LATEST..AVERIAS ORDER BY idAveria DESC

	INSERT INTO 
	dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
	VALUES
	('UN', 'script', 'exec debug1 TKPA_LATEST @ ' + convert(varchar(25), @latestDate, 120), GETDATE());


END
GO

EXEC dbo.execDebug1;

SELECT TOP 3 * FROM dbo.COM_AUDIT_LOGS ORDER BY ID DESC;

EXEC dbo.fixListPricePerUnitPA;
