SELECT ORIG_ITEM_NUMBER, ITEM_NUMBER, ORGANIZATION_CODE FROM O_STG_ITEM_HEADER WHERE ITEM_NUMBER='LA07ASE000047'

SELECT * FROM O_STG_ITEM_MFG_PART_NUM WHERE ITEM_NUMBER='LA07ASE000047'


SELECT ORIG_ITEM_NUMBER, ITEM_NUMBER, MERGED_ID, LOCAL_VALIDATED, CROSSREF_VALIDATED FROM O_STG_ITEM_HEADER WHERE COUNTRY='HN'

/**
    EXECUTE dbo.fixListPricePerUnitPA;

    SELECT DISTINCT
    max(ltrim(rtrim(d.ITEM_NUMBER)))											as Item_Number,
    'CPA'																		as Organization_Code,
    case when max(isnull(f.DESCRIP,'')) = 'ALMACEN PRINCIPAL' THEN 'DEP01'
    else 'DEPCAL'
    END																			as Subinventory_Name,
    round(sum(a.SALDO),4)														as Transaction_Quantity,
    '31-07-2018'																as Transaction_Date,
    'DATA MIGRATION PA'															as Transaction_Reference,
    max(ltrim(rtrim(e.UBICACION))) 												as Locator_Name,
    round(sum(a.COSTO * a.SALDO) / (SUM(a.saldo)) ,4)                           as Transaction_Cost,
    ''																			as Serial_Number_From,
    ''																			as Serial_Number_To	,
    '501020.12601002.0000.00.00.000000.000000.000000.000000.274705'				as Distribution_Account,
    'PA - THYSSENKRUPP ELEVADORES S.A.'											as Operating_Unit_Name,
	e.UBICACION,
	e.PRODUCTO
    from TKPA_LATEST..STOCK as a
    inner join TKPA_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
    inner join DBO.O_STG_ITEM_CROSS_REF_ALT as c on ltrim(rtrim(b.PRODUCTO))=ltrim(rtrim(c.CROSS_REFERENCE))
    inner join DBO.O_STG_ITEM_HEADER as d on c.ORIG_ITEM_NUMBER=d.ORIG_ITEM_NUMBER
    inner join TKPA_LATEST..UBICAPRODALM as e on b.PRODUCTO=e.PRODUCTO and a.IdAlmacen=e.IDALMACEN
    inner join TKPA_LATEST..ALMACEN as f on a.IdAlmacen=f.IdAlmacen
    WHERE
    A.PERIODO=2018
    and a.IdAlmacen=1
    AND A.MES=7
    and a.SALDO > 0
    --and e.UBICACION like '%.%'
    and d.LOCAL_VALIDATED = 1
    and d.CROSSREF_VALIDATED = 1
    and d.COUNTRY='PA'
	and c.ITEM_NUMBER='LA99ESM000308'
    group by c.ITEM_NUMBER, e.UBICACION, e.PRODUCTO


	SELECT e.UBICACION, a.* FROM TKPA_LATEST..STOCK as a
    inner join TKPA_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO 
	inner join TKPA_LATEST..UBICAPRODALM as e on a.PRODUCTO=e.PRODUCTO and a.IdAlmacen=e.IDALMACEN
	WHERE a.PRODUCTO='FML35-1000-5500'
	and A.PERIODO=2018
    and a.IdAlmacen=1
    AND A.MES=7
    and a.SALDO > 0
**/