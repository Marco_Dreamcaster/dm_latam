IF OBJECT_ID('dbo.udf_EncodeItemReference', 'FN') IS NOT NULL
	DROP FUNCTION dbo.udf_EncodeItemReference;
GO

CREATE FUNCTION dbo.udf_EncodeItemReference ( @country VARCHAR(2), @producto VARCHAR(10) )
RETURNS VARCHAR(25)
AS
BEGIN

DECLARE @itemReference VARCHAR(25)


SET @itemReference = RTRIM(LTRIM(UPPER(@country) + @producto));

RETURN @itemReference
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated udf_EncodeItemReference function definition', GETDATE());

