


IF OBJECT_ID('.generateItemCategoryByCountry', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateItemCategoryByCountry;

GO

CREATE PROCEDURE dbo.generateItemCategoryByCountry
(
@country VARCHAR(2)
)
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

SELECT b.[id]                                   as id
,b.[ORIG_ITEM_NUMBER]                           as origItemNumber
,b.[ITEM_NUMBER]                                as itemNumber
,a.CATEGORY_SET_NAME					        as categorySetName
,a.CONCAT_CATEGORY_NAME					        as concatCategoryName
,'L'+@country							        as organizationCode
,dbo.udf_EncodeOrgName(@country)					as orgName
,b.[MERGED_ID]                                  as mergedId
,b.[CHILD_LESS]                                 as childLess
,b.[SAME_COUNTRY_PARENT]                        as sameCountryParent
FROM DBO.[O_STG_ITEM_HEADER] b
inner join DBO.O_STG_ITEM_CATEGORY a ON b.ORIG_ITEM_NUMBER=a.ORIG_ITEM_NUMBER
WHERE
b.[COUNTRY]=@country AND
b.[DISMISSED]=0 AND
b.[LOCAL_VALIDATED]=1 AND
b.[CROSSREF_VALIDATED]=1 AND
a.[DISMISSED]=0

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateItemCategoryByCountry procedure definition', GETDATE());



