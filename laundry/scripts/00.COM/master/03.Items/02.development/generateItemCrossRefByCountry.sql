IF OBJECT_ID('.generateItemCrossRefByCountry', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateItemCrossRefByCountry;

GO
CREATE PROCEDURE dbo.generateItemCrossRefByCountry
(
@country VARCHAR(2)
)
AS
BEGIN
SELECT b.[ORIG_ITEM_NUMBER]                     as origItemNumber
,b.[ITEM_NUMBER]                                as itemNumber
,b.[CROSS_REFERENCE_TYPE]                       as crossReferenceType
,b.[CROSS_REFERENCE]                            as crossReference
,b.[DESCRIPTION]                                as description
,'L'+@country                                   as organizationCode
,dbo.udf_EncodeOrgName(@country)					as orgName
FROM DBO.[O_STG_ITEM_HEADER] a
inner join DBO.[O_STG_ITEM_CROSS_REF_ALT] b ON a.ORIG_ITEM_NUMBER=b.ORIG_ITEM_NUMBER
WHERE
a.[COUNTRY]=@country
and a.LOCAL_VALIDATED = 1
and a.CROSSREF_VALIDATED = 1
and a.DISMISSED=0
ORDER BY a.ITEM_NUMBER
END

GO
INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateItemCategoryByCountry procedure definition', GETDATE());
