IF OBJECT_ID('.generateItemHeaderByCountry', 'P') IS NOT NULL
DROP PROCEDURE dbo.generateItemHeaderByCountry;
GO

CREATE PROCEDURE dbo.generateItemHeaderByCountry
(
@country VARCHAR(2)
)
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

SELECT b.[id]                           as id
,b.[ORIG_ITEM_NUMBER]                   as origItemNumber
,b.[ITEM_NUMBER]                        as itemNumber
,b.[DESCRIPTION]                        as description
,b.[DESCRIPTION_ESA]                    as descriptionEsa
,b.[ORGANIZATION_CODE]                  as organizationCode
,b.[TEMPLATE_NAME]                      as templateName
,b.[LIST_PRICE_PER_UNIT]                as listPricePerUnit
,b.[UOM_CODE]                           as uomCode
,b.[WEIGHT_UOM_CODE]                    as weightUomCode
,b.[UNIT_WEIGHT]                        as unitWeight
,b.[VOLUME_UOM_CODE]                    as volumeUomCode
,b.[UNIT_VOLUME]                        as unitVolume
,b.[MIN_MINMAX_QUANTITY]				as minMinMaxQuantity
,b.[MAX_MINMAX_QUANTITY]				as maxMinMaxQuantity
,b.[FULL_LEAD_TIME]						as fullLeadTime
,b.[FIXED_LOT_MULTIPLIER]				as fixedLotMultiplier
,b.[FIXED_ORDER_QUANTITY]				as fixedOrderQuantity
,b.[MINIMUM_ORDER_QUANTITY]				as minOrderQuantity
,b.[MAXIMUM_ORDER_QUANTITY]				as maxOrderQuantity
,b.[LONG_DESCRIPTION]                   as longDescription
,b.[ITEM_INV_APPLICATION]               as itemInvApplication
,b.[TRANSACTION_CONDITION_CODE]         as transactionConditionCode
,b.[ADJUSTMENT_ACCOUNT]                 as adjustmentAccount
,b.[CORRECTION_ACCOUNT]                 as correctionAccount
,b.[SALES_COST_ACCOUNT]                 as salesCostAccount
,b.[FOUR_DIGIT_CODE]					as fourDigitCode
,dbo.udf_EncodeOrgName(@country)					as orgName
,b.[MERGED_ID]                          as mergedId
,b.[CHILD_LESS]                         as childLess
,b.[SAME_COUNTRY_PARENT]                as sameCountryParent
,c.COUNTRIES							as presentOnCountries
FROM DBO.[O_STG_ITEM_HEADER] b
INNER JOIN DBO.O_STG_ITEM_CROSS_REF_EXT c
ON b.ORIG_ITEM_NUMBER = c.ORIG_ITEM_NUMBER
WHERE
b.[COUNTRY]=@country AND
b.[DISMISSED]=0 AND
b.[LOCAL_VALIDATED]=1 AND
b.[CROSSREF_VALIDATED]=1

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateItemHeaderByCountry procedure definition', GETDATE());
