    SELECT DISTINCT
    max(ltrim(rtrim(d.ITEM_NUMBER)))											as Item_Number,
    'CPA'																		as Organization_Code,
    case when max(isnull(f.DESCRIP,'')) = 'ALMACEN PRINCIPAL' THEN 'DEP01'
    else 'DEPCAL'
    END																			as Subinventory_Name,
    round(sum(a.SALDO),4)														as Transaction_Quantity,
    '31-07-2018'																as Transaction_Date,
    'DATA MIGRATION PA'															as Transaction_Reference,
    max(ltrim(rtrim(e.UBICACION))) 												as Locator_Name,
    max(d.LIST_PRICE_PER_UNIT)													as Transaction_Cost,
    ''																			as Serial_Number_From,
    ''																			as Serial_Number_To	,
    '501020.12601002.0000.00.00.000000.000000.000000.000000.274705'				as Distribution_Account,
    'PA - THYSSENKRUPP ELEVADORES S.A.'											as Operating_Unit_Name
    from TKPA_LATEST..STOCK as a
    inner join TKPA_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
    inner join DM_LATAM_DEV..O_STG_ITEM_CROSS_REF_ALT as c on ltrim(rtrim(b.PRODUCTO))=ltrim(rtrim(c.CROSS_REFERENCE))
    inner join DM_LATAM_DEV..O_STG_ITEM_HEADER as d on c.ORIG_ITEM_NUMBER=d.ORIG_ITEM_NUMBER
    inner join TKPA_LATEST..UBICAPRODALM as e on b.PRODUCTO=e.PRODUCTO and a.IdAlmacen=e.IDALMACEN
    inner join TKPA_LATEST..ALMACEN as f on a.IdAlmacen=f.IdAlmacen
    WHERE
    A.PERIODO=2018
    and a.IdAlmacen=1
    AND A.MES=7
    and a.SALDO > 0
    and e.UBICACION like '%.%'
    and d.LOCAL_VALIDATED = 1
    and d.CROSSREF_VALIDATED = 1
    and d.PRICE_MANUAL_OVERRIDE=1
    and d.COUNTRY='PA'
	and d.ITEM_NUMBER='X26.101.095'
    group by c.ITEM_NUMBER

SELECT DISTINCT
max(ltrim(rtrim(d.ITEM_NUMBER)))											as Item_Number,
'CPA'																		as Organization_Code,
case when max(isnull(f.DESCRIP,'')) = 'ALMACEN PRINCIPAL' THEN 'DEP01'
else 'DEPCAL'
END																			as Subinventory_Name,
round(sum(a.SALDO),4)														as Transaction_Quantity,
'31-07-2018'																as Transaction_Date,
'DATA MIGRATION PA'															as Transaction_Reference,
max(ltrim(rtrim(e.UBICACION))) 												as Locator_Name,
CASE WHEN (round(sum(a.COSTO * a.SALDO) / (SUM(a.saldo)) ,4))>=1
THEN (round(sum(a.COSTO * a.SALDO) / (SUM(a.saldo)) ,4))
ELSE 1
END                                                                         as Transaction_Cost,
''																			as Serial_Number_From,
''																			as Serial_Number_To	,
'501020.12601002.0000.00.00.000000.000000.000000.000000.274705'				as Distribution_Account,
'PA - THYSSENKRUPP ELEVADORES S.A.'											as Operating_Unit_Name
from TKPA_LATEST..STOCK as a
inner join TKPA_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
inner join DM_LATAM_DEV..O_STG_ITEM_CROSS_REF_ALT as c on ltrim(rtrim(b.PRODUCTO))=ltrim(rtrim(c.CROSS_REFERENCE))
inner join DM_LATAM_DEV..O_STG_ITEM_HEADER as d on c.ORIG_ITEM_NUMBER=d.ORIG_ITEM_NUMBER
inner join TKPA_LATEST..UBICAPRODALM as e on b.PRODUCTO=e.PRODUCTO and a.IdAlmacen=e.IDALMACEN
inner join TKPA_LATEST..ALMACEN as f on a.IdAlmacen=f.IdAlmacen
WHERE
A.PERIODO=2018
and a.IdAlmacen=1
AND A.MES=7
and a.SALDO > 0
and e.UBICACION like '%.%'
and d.LOCAL_VALIDATED = 1
and d.CROSSREF_VALIDATED = 1
and (d.PRICE_MANUAL_OVERRIDE IS NULL OR d.PRICE_MANUAL_OVERRIDE=0)
and d.ITEM_NUMBER='X26.101.095'
and d.COUNTRY='PA'
group by c.ITEM_NUMBER


SELECT DISTINCT
ltrim(rtrim(d.ITEM_NUMBER))													as Item_Number,
'CPA'																		as Organization_Code,
case when isnull(f.DESCRIP,'') = 'ALMACEN PRINCIPAL' THEN 'DEP01'
else 'DEPCAL'
END																			as Subinventory_Name,
round(a.SALDO,4)															as Transaction_Quantity,
'31-07-2018'																as Transaction_Date,
'DATA MIGRATION PA'															as Transaction_Reference,
ltrim(rtrim(e.UBICACION)) 													as Locator_Name,
CASE WHEN (round(a.COSTO,4))>=1
THEN (round(a.COSTO,4))
ELSE 1
END                                                                         as Transaction_Cost,
''																			as Serial_Number_From,
''																			as Serial_Number_To	,
'501020.12601002.0000.00.00.000000.000000.000000.000000.274705'				as Distribution_Account,
'PA - THYSSENKRUPP ELEVADORES S.A.'											as Operating_Unit_Name
from TKPA_LATEST..STOCK as a
inner join TKPA_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
inner join DM_LATAM_DEV..O_STG_ITEM_CROSS_REF_ALT as c on ltrim(rtrim(b.PRODUCTO))=ltrim(rtrim(c.CROSS_REFERENCE))
inner join DM_LATAM_DEV..O_STG_ITEM_HEADER as d on c.ORIG_ITEM_NUMBER=d.ORIG_ITEM_NUMBER
inner join TKPA_LATEST..UBICAPRODALM as e on b.PRODUCTO=e.PRODUCTO and a.IdAlmacen=e.IDALMACEN
inner join TKPA_LATEST..ALMACEN as f on a.IdAlmacen=f.IdAlmacen
WHERE
A.PERIODO=2018
and a.IdAlmacen=1
AND A.MES=7
and a.SALDO > 0
and e.UBICACION like '%.%'
and d.LOCAL_VALIDATED = 1
and d.CROSSREF_VALIDATED = 1
and (d.PRICE_MANUAL_OVERRIDE IS NULL OR d.PRICE_MANUAL_OVERRIDE=0)
and d.ITEM_NUMBER='X26.101.095'
and d.COUNTRY='PA'

SELECT
* 
FROM O_STG_ITEM_CROSS_REF_ALT
WHERE ITEM_NUMBER='X26.101.095'

SELECT
a.PRODUCTO,
a.MES,
a.SALDO,
a.SALDO*a.COSTO AS DOLLAR_TOTAL
FROM TKPA_LATEST..STOCK a
WHERE a.PRODUCTO='X26.101.095' AND
A.PERIODO=2018
and a.IdAlmacen=1
AND A.MES=7
ORDER BY A.MES

 

SELECT * FROM O_STG_ITEM_HEADER WHERE ITEM_NUMBER='X26.101.095'
SELECT * FROM O_STG_ITEM_CROSS_REF_ALT WHERE ITEM_NUMBER='X26.101.095'


SELECT
a.ITEM_NUMBER,
a.ORIG_ITEM_NUMBER,
b.RECOMMENDED,
b.RANKING,
a.MERGED_ID,
a.DISMISSED
FROM O_STG_ITEM_HEADER a
INNER JOIN O_PRE_ITEM_HEADER b ON (a.ORIG_ITEM_NUMBER=b.ORIG_ITEM_NUMBER)
WHERE a.COUNTRY='PA' AND
a.DISMISSED=1 AND
a.MERGED_ID IS NULL
ORDER BY RECOMMENDED DESC


SELECT RECOMMENDED, RANKING, * FROM O_PRE_ITEM_HEADER a WHERE a.ORIG_ITEM_NUMBER='PAN2501'

/** **/


SELECT DISTINCT a.ORIG_ITEM_NUMBER,
a.RANKING
FROM O_PRE_ITEM_HEADER a
WHERE a.COUNTRY='PA' AND
a.RANKING>=3 AND
a.ORIG_ITEM_NUMBER NOT IN
(
	select distinct LEFT(RTRIM(LTRIM('PA' + a.PRODUCTO)),30) AS ORIG_ITEM_NUMBER
	from TKPA_LATEST.dbo.STOCK as a
		  inner join TKPA_LATEST.dbo.PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO  
	WHERE
		  a.PERIODO=YEAR(GETDATE())
		  AND a.MES=7
		  and a.SALDO > 0
		  and isnull(b.BLOQUEADO,'N') <> 'S'
		  and a.idalmacen=1
)


-- items which were marked for obsolecense, and have stock
--
SELECT DISTINCT a.ORIG_ITEM_NUMBER,
a.RANKING
FROM O_PRE_ITEM_HEADER a
WHERE a.COUNTRY='PA' AND
a.RANKING=3 AND
a.ORIG_ITEM_NUMBER IN
(
	select distinct LEFT(RTRIM(LTRIM('PA' + a.PRODUCTO)),30) AS ORIG_ITEM_NUMBER
	from TKPA_LATEST.dbo.STOCK as a
		  inner join TKPA_LATEST.dbo.PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO  
	WHERE
		  a.PERIODO=YEAR(GETDATE())
		  AND a.MES=7
		  and a.SALDO > 0
		  and isnull(b.BLOQUEADO,'N') <> 'S'
		  and a.idalmacen=1
)



SELECT DISTINCT a.ORIG_ITEM_NUMBER
FROM O_PRE_ITEM_HEADER a
WHERE a.COUNTRY='PA' AND
a.RANKING=3 AND
a.ORIG_ITEM_NUMBER NOT IN
(
	select distinct a.ORIG_ITEM_NUMBER
	from O_STG_ITEM_HEADER as a
	WHERE
		  a.COUNTRY='PA'
)

SELECT DISTINCT a.ORIG_ITEM_NUMBER,
a.RANKING,
a.RECOMMENDED,
a.CLEANSED
FROM O_PRE_ITEM_HEADER a
WHERE a.COUNTRY='PA' AND
a.RANKING>=3 AND
a.CLEANSED=0 AND
a.ORIG_ITEM_NUMBER IN
(
	select distinct LEFT(RTRIM(LTRIM('PA' + a.PRODUCTO)),30) AS ORIG_ITEM_NUMBER
	from TKPA_LATEST.dbo.STOCK as a
		  inner join TKPA_LATEST.dbo.PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO  
	WHERE
		  a.PERIODO=YEAR(GETDATE())
		  AND a.MES=7
		  and a.SALDO > 0
		  and isnull(b.BLOQUEADO,'N') <> 'S'
		  and a.idalmacen=1
)


SELECT DISTINCT a.ORIG_ITEM_NUMBER,
a.RANKING,
a.RECOMMENDED,
a.CLEANSED
FROM O_PRE_ITEM_HEADER a
WHERE a.COUNTRY='PA' AND
a.RANKING=3 AND
a.CLEANSED=1 AND
a.ORIG_ITEM_NUMBER IN
(
	select distinct LEFT(RTRIM(LTRIM('PA' + a.PRODUCTO)),30) AS ORIG_ITEM_NUMBER
	from TKPA_LATEST.dbo.STOCK as a
		  inner join TKPA_LATEST.dbo.PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO  
	WHERE
		  a.PERIODO=YEAR(GETDATE())
		  AND a.MES=7
		  and a.SALDO > 0
		  and isnull(b.BLOQUEADO,'N') <> 'S'
		  and a.idalmacen=1
)
