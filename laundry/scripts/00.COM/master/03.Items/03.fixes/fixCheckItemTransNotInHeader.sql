EXEC sp_configure 'show advanced options', 1
RECONFIGURE
GO
EXEC sp_configure 'ad hoc distributed queries', 1
RECONFIGURE
GO

IF OBJECT_ID('tempdb..#ITEM_HEADER_PA') IS NOT NULL DROP TABLE #ITEM_HEADER_PA
IF OBJECT_ID('tempdb..#ITEM_TRANS_PA') IS NOT NULL DROP TABLE #ITEM_TRANS_PA
IF OBJECT_ID('tempdb..#ITEM_CATEGORY_PA') IS NOT NULL DROP TABLE ITEM_CATEGORY_PA

-- 'Server=172.21.72.148;Database=DM_LATAM_DEV;Uid=tkedev;Pwd=tkedev;'
-- 'Server=10.148.1.47;Database=DM_LATAM;Uid=sa;Pwd=Tkelatam2017;'

SELECT * INTO #ITEM_HEADER_PA FROM 
OPENROWSET('SQLNCLI','Server=10.148.1.47;Database=DM_LATAM;Uid=sa;Pwd=Tkelatam2017;','EXEC dbo.generateItemHeaderByCountry ''PA'', ''2018-09-30''' )

SELECT * INTO #ITEM_TRANS_PA FROM 
OPENROWSET('SQLNCLI','Server=10.148.1.47;Database=DM_LATAM;Uid=sa;Pwd=Tkelatam2017;','EXEC  dbo.generateItemTransForPA ''2018-09-30''' )

SELECT * INTO #ITEM_CATEGORY_PA FROM 
OPENROWSET('SQLNCLI','Server=10.148.1.47;Database=DM_LATAM;Uid=sa;Pwd=Tkelatam2017;','EXEC dbo.generateItemCategoryByCountry_ALT ''PA''' )


SELECT b.itemNumber AS ITEM_CATEGORY_NOT_IN_HEADER FROM #ITEM_CATEGORY_PA b
EXCEPT
SELECT a.itemNumber FROM #ITEM_HEADER_PA a

SELECT a.itemNumber AS ITEM_HEADER_NOT_IN_CATEGORY FROM #ITEM_HEADER_PA a
EXCEPT
SELECT b.itemNumber FROM #ITEM_CATEGORY_PA 

SELECT COUNT(*) AS ITEM_TRANS_COUNT FROM #ITEM_TRANS_PA

SELECT a.Item_Number AS ITEM_TRANS_NOT_IN_HEADER FROM #ITEM_TRANS_PA a
EXCEPT
SELECT b.itemNumber FROM #ITEM_HEADER_PA b

SELECT b.itemNumber AS 
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																														
FROM #ITEM_HEADER_PA b
EXCEPT
SELECT a.Item_Number FROM #ITEM_TRANS_PA a

