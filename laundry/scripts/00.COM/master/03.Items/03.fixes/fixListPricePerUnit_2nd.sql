SELECT DISTINCT
max(ltrim(rtrim(d.ITEM_NUMBER)))											as Item_Number,
'CPA'																		as Organization_Code,
case when max(isnull(f.DESCRIP,'')) = 'ALMACEN PRINCIPAL' THEN 'DEP01'
else 'DEPCAL'
END																			as Subinventory_Name,
round(sum(a.SALDO),4)														as Transaction_Quantity,
'31-07-2018'																as Transaction_Date,
'DATA MIGRATION PA'															as Transaction_Reference,
max(ltrim(rtrim(e.UBICACION))) 												as Locator_Name,
max(d.LIST_PRICE_PER_UNIT)													as Transaction_Cost,
''																			as Serial_Number_From,
''																			as Serial_Number_To	,
'501020.12601002.0000.00.00.000000.000000.000000.000000.274705'				as Distribution_Account,
'PA - THYSSENKRUPP ELEVADORES S.A.'											as Operating_Unit_Name
from TKPA_LATEST..STOCK as a
inner join TKPA_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
inner join DBO.O_STG_ITEM_CROSS_REF_ALT as c on ltrim(rtrim(b.PRODUCTO))=ltrim(rtrim(c.CROSS_REFERENCE))
inner join DBO.O_STG_ITEM_HEADER as d on c.ORIG_ITEM_NUMBER=d.ORIG_ITEM_NUMBER
inner join TKPA_LATEST..UBICAPRODALM as e on b.PRODUCTO=e.PRODUCTO and a.IdAlmacen=e.IDALMACEN
inner join TKPA_LATEST..ALMACEN as f on a.IdAlmacen=f.IdAlmacen
WHERE
A.PERIODO=2018
and a.IdAlmacen=1
AND A.MES=7
and a.SALDO > 0
and e.UBICACION like '%.%'
and d.LOCAL_VALIDATED = 1
and d.CROSSREF_VALIDATED = 1
and d.PRICE_MANUAL_OVERRIDE=1
and d.COUNTRY='PA'
and d.PRICE_MANUAL_OVERRIDE=1
group by c.ITEM_NUMBER

UNION

SELECT DISTINCT
max(ltrim(rtrim(d.ITEM_NUMBER)))											as Item_Number,
'CPA'																		as Organization_Code,
case when max(isnull(f.DESCRIP,'')) = 'ALMACEN PRINCIPAL' THEN 'DEP01'
else 'DEPCAL'
END																			as Subinventory_Name,
round(sum(a.SALDO),4)														as Transaction_Quantity,
'31-07-2018'																as Transaction_Date,
'DATA MIGRATION PA'															as Transaction_Reference,
max(ltrim(rtrim(e.UBICACION))) 												as Locator_Name,
round(sum(a.COSTO * a.SALDO) / (SUM(a.saldo)) ,4)                           as Transaction_Cost,
''																			as Serial_Number_From,
''																			as Serial_Number_To	,
'501020.12601002.0000.00.00.000000.000000.000000.000000.274705'				as Distribution_Account,
'PA - THYSSENKRUPP ELEVADORES S.A.'											as Operating_Unit_Name
from TKPA_LATEST..STOCK as a
inner join TKPA_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
inner join DBO.O_STG_ITEM_CROSS_REF_ALT as c on ltrim(rtrim(b.PRODUCTO))=ltrim(rtrim(c.CROSS_REFERENCE))
inner join DBO.O_STG_ITEM_HEADER as d on c.ORIG_ITEM_NUMBER=d.ORIG_ITEM_NUMBER
inner join TKPA_LATEST..UBICAPRODALM as e on b.PRODUCTO=e.PRODUCTO and a.IdAlmacen=e.IDALMACEN
inner join TKPA_LATEST..ALMACEN as f on a.IdAlmacen=f.IdAlmacen
WHERE
A.PERIODO=2018
and a.IdAlmacen=1
AND A.MES=7
and a.SALDO > 0
and e.UBICACION like '%.%'
and d.LOCAL_VALIDATED = 1
and d.CROSSREF_VALIDATED = 1
and (d.PRICE_MANUAL_OVERRIDE IS NULL OR d.PRICE_MANUAL_OVERRIDE=0)
and d.COUNTRY='PA'
group by c.ITEM_NUMBER

-- working query for 3Y.6153.D.3

SELECT DISTINCT
max(ltrim(rtrim(d.ITEM_NUMBER)))											as Item_Number,
'CPA'																		as Organization_Code,
case when max(isnull(f.DESCRIP,'')) = 'ALMACEN PRINCIPAL' THEN 'DEP01'
else 'DEPCAL'
END																			as Subinventory_Name,
round(sum(a.SALDO),4)														as Transaction_Quantity,
'31-07-2018'																as Transaction_Date,
'DATA MIGRATION PA'															as Transaction_Reference,
max(ltrim(rtrim(e.UBICACION))) 												as Locator_Name,
round(sum(a.COSTO * a.SALDO) / (SUM(a.saldo)) ,4)                           as Transaction_Cost,
''																			as Serial_Number_From,
''																			as Serial_Number_To	,
'501020.12601002.0000.00.00.000000.000000.000000.000000.274705'				as Distribution_Account,
'PA - THYSSENKRUPP ELEVADORES S.A.'											as Operating_Unit_Name
from TKPA_LATEST..STOCK as a
inner join TKPA_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
inner join DBO.O_STG_ITEM_CROSS_REF_ALT as c on ltrim(rtrim(b.PRODUCTO))=ltrim(rtrim(c.CROSS_REFERENCE))
inner join DBO.O_STG_ITEM_HEADER as d on c.ORIG_ITEM_NUMBER=d.ORIG_ITEM_NUMBER
inner join TKPA_LATEST..UBICAPRODALM as e on b.PRODUCTO=e.PRODUCTO and a.IdAlmacen=e.IDALMACEN
inner join TKPA_LATEST..ALMACEN as f on a.IdAlmacen=f.IdAlmacen
WHERE
A.PERIODO=2018
and a.IdAlmacen=1
AND A.MES=7
and a.SALDO > 0
and e.UBICACION like '%.%'
and d.LOCAL_VALIDATED = 1
and d.CROSSREF_VALIDATED = 1
and (d.PRICE_MANUAL_OVERRIDE IS NULL OR d.PRICE_MANUAL_OVERRIDE=0)
and d.COUNTRY='PA'
and d.ORIG_ITEM_NUMBER='PA3Y.6153.D.3'
group by c.ITEM_NUMBER

-- reduced projection for 3Y.6153.D.3

SELECT DISTINCT
max(ltrim(rtrim(d.ITEM_NUMBER)))											as Item_Number
from TKPA_LATEST..STOCK as a
inner join TKPA_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
inner join DBO.O_STG_ITEM_CROSS_REF_ALT as c on ltrim(rtrim(b.PRODUCTO))=ltrim(rtrim(c.CROSS_REFERENCE))
inner join DBO.O_STG_ITEM_HEADER as d on c.ORIG_ITEM_NUMBER=d.ORIG_ITEM_NUMBER
inner join TKPA_LATEST..UBICAPRODALM as e on b.PRODUCTO=e.PRODUCTO and a.IdAlmacen=e.IDALMACEN
inner join TKPA_LATEST..ALMACEN as f on a.IdAlmacen=f.IdAlmacen
WHERE
A.PERIODO=2018
and a.IdAlmacen=1
AND A.MES=7
and a.SALDO > 0
and e.UBICACION like '%.%'
and d.LOCAL_VALIDATED = 1
and d.CROSSREF_VALIDATED = 1
and (d.PRICE_MANUAL_OVERRIDE IS NULL OR d.PRICE_MANUAL_OVERRIDE=0)
and d.COUNTRY='PA'
and d.ORIG_ITEM_NUMBER='PA3Y.6153.D.3'
group by c.ITEM_NUMBER



-- not working for item 439

SELECT DISTINCT
max(ltrim(rtrim(d.ITEM_NUMBER)))											as Item_Number
from TKPA_LATEST..STOCK as a
inner join TKPA_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
inner join DBO.O_STG_ITEM_CROSS_REF_ALT as c on ltrim(rtrim(b.PRODUCTO))=ltrim(rtrim(c.CROSS_REFERENCE))
inner join DBO.O_STG_ITEM_HEADER as d on c.ORIG_ITEM_NUMBER=d.ORIG_ITEM_NUMBER
inner join TKPA_LATEST..UBICAPRODALM as e on b.PRODUCTO=e.PRODUCTO and a.IdAlmacen=e.IDALMACEN
inner join TKPA_LATEST..ALMACEN as f on a.IdAlmacen=f.IdAlmacen
WHERE
A.PERIODO=2018
and a.IdAlmacen=1
AND A.MES=7
and a.SALDO > 0
and e.UBICACION like '%.%'
and d.LOCAL_VALIDATED = 1
and d.CROSSREF_VALIDATED = 1
and (d.PRICE_MANUAL_OVERRIDE IS NULL OR d.PRICE_MANUAL_OVERRIDE=0)
and d.COUNTRY='PA'
and d.ORIG_ITEM_NUMBER='PA439'
group by c.ITEM_NUMBER

-- debugging conditions

SELECT DISTINCT
max(ltrim(rtrim(d.ITEM_NUMBER)))											as Item_Number
from TKPA_LATEST..STOCK as a
inner join TKPA_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
inner join DBO.O_STG_ITEM_CROSS_REF_ALT as c on ltrim(rtrim(b.PRODUCTO))=ltrim(rtrim(c.CROSS_REFERENCE))
inner join DBO.O_STG_ITEM_HEADER as d on c.ORIG_ITEM_NUMBER=d.ORIG_ITEM_NUMBER
inner join TKPA_LATEST..UBICAPRODALM as e on b.PRODUCTO=e.PRODUCTO and a.IdAlmacen=e.IDALMACEN
inner join TKPA_LATEST..ALMACEN as f on a.IdAlmacen=f.IdAlmacen
WHERE
A.PERIODO=2018
and a.IdAlmacen=1
AND A.MES=7
and a.SALDO > 0
--and e.UBICACION like '%.%'
and d.LOCAL_VALIDATED = 1
and d.CROSSREF_VALIDATED = 1
and (d.PRICE_MANUAL_OVERRIDE IS NULL OR d.PRICE_MANUAL_OVERRIDE=0)
and d.COUNTRY='PA'
and d.ORIG_ITEM_NUMBER='PA439'
group by c.ITEM_NUMBER

SELECT * FROM TKPA_LATEST..UBICAPRODALM e WHERE e.PRODUCTO IN ('439')


/**




SELECT DISTINCT
max(ltrim(rtrim(d.ITEM_NUMBER)))											as Item_Number,
'CPA'																		as Organization_Code,
case when max(isnull(f.DESCRIP,'')) = 'ALMACEN PRINCIPAL' THEN 'DEP01'
else 'DEPCAL'
END																			as Subinventory_Name,
round(sum(a.SALDO),4)														as Transaction_Quantity,
'31-07-2018'																as Transaction_Date,
'DATA MIGRATION PA'															as Transaction_Reference,
max(ltrim(rtrim(e.UBICACION))) 												as Locator_Name,
CASE WHEN (round(sum(a.COSTO * a.SALDO) / (SUM(a.saldo)) ,4))>=1 
THEN (round(sum(a.COSTO * a.SALDO) / (SUM(a.saldo)) ,4))
ELSE 1
END                                                                         as Transaction_Cost,
''																			as Serial_Number_From,
''																			as Serial_Number_To	,
'501020.12601002.0000.00.00.000000.000000.000000.000000.274705'				as Distribution_Account,
'PA - THYSSENKRUPP ELEVADORES S.A.'											as Operating_Unit_Name
from TKPA_LATEST..STOCK as a
inner join TKPA_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
inner join DM_LATAM_DEV..O_STG_ITEM_CROSS_REF_ALT as c on ltrim(rtrim(b.PRODUCTO))=ltrim(rtrim(c.CROSS_REFERENCE))
inner join DM_LATAM_DEV..O_STG_ITEM_HEADER as d on c.ORIG_ITEM_NUMBER=d.ORIG_ITEM_NUMBER
inner join TKPA_LATEST..UBICAPRODALM as e on b.PRODUCTO=e.PRODUCTO and a.IdAlmacen=e.IDALMACEN
inner join TKPA_LATEST..ALMACEN as f on a.IdAlmacen=f.IdAlmacen
WHERE
A.PERIODO=2018
and a.IdAlmacen=1
AND A.MES=7
and a.SALDO > 0
and e.UBICACION like '%.%'
and d.LOCAL_VALIDATED = 1
and d.CROSSREF_VALIDATED = 1
and (d.PRICE_MANUAL_OVERRIDE IS NULL OR d.PRICE_MANUAL_OVERRIDE=0)
and d.COUNTRY='PA'
group by c.ITEM_NUMBER


SELECT a.LOCAL_VALIDATED, a.CROSSREF_VALIDATED, a.PRICE_MANUAL_OVERRIDE FROM O_STG_ITEM_HEADER a WHERE a.ORIG_ITEM_NUMBER='PA3Y.6153.D.3'





SELECT DISTINCT
max(ltrim(rtrim(d.ITEM_NUMBER)))											as Item_Number,
'CPA'																		as Organization_Code,
case when max(isnull(f.DESCRIP,'')) = 'ALMACEN PRINCIPAL' THEN 'DEP01'
else 'DEPCAL'
END																			as Subinventory_Name,
round(sum(a.SALDO),4)														as Transaction_Quantity,
'31-07-2018'																as Transaction_Date,
'DATA MIGRATION PA'															as Transaction_Reference,
max(ltrim(rtrim(e.UBICACION))) 												as Locator_Name,
CASE WHEN (round(sum(a.COSTO * a.SALDO) / (SUM(a.saldo)) ,4))>=1 
THEN (round(sum(a.COSTO * a.SALDO) / (SUM(a.saldo)) ,4))
ELSE 1
END                                                                         as Transaction_Cost,
''																			as Serial_Number_From,
''																			as Serial_Number_To	,
'501020.12601002.0000.00.00.000000.000000.000000.000000.274705'				as Distribution_Account,
'PA - THYSSENKRUPP ELEVADORES S.A.'											as Operating_Unit_Name
from TKPA_LATEST..STOCK as a
inner join TKPA_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
inner join DM_LATAM_DEV..O_STG_ITEM_CROSS_REF_ALT as c on ltrim(rtrim(b.PRODUCTO))=ltrim(rtrim(c.CROSS_REFERENCE))
inner join DM_LATAM_DEV..O_STG_ITEM_HEADER as d on c.ORIG_ITEM_NUMBER=d.ORIG_ITEM_NUMBER
inner join TKPA_LATEST..UBICAPRODALM as e on b.PRODUCTO=e.PRODUCTO and a.IdAlmacen=e.IDALMACEN
inner join TKPA_LATEST..ALMACEN as f on a.IdAlmacen=f.IdAlmacen
WHERE
A.PERIODO=2018
and a.IdAlmacen=1
AND A.MES=7
and a.SALDO > 0
and e.UBICACION like '%.%'
and d.LOCAL_VALIDATED = 1
and d.CROSSREF_VALIDATED = 1
and (d.PRICE_MANUAL_OVERRIDE IS NULL OR d.PRICE_MANUAL_OVERRIDE=0)
and d.COUNTRY='PA'
and d.ORIG_ITEM_NUMBER='PA411'
group by c.ITEM_NUMBER


SELECT DISTINCT
max(ltrim(rtrim(d.ITEM_NUMBER)))											as Item_Number,
'CPA'																		as Organization_Code,
case when max(isnull(f.DESCRIP,'')) = 'ALMACEN PRINCIPAL' THEN 'DEP01'
else 'DEPCAL'
END																			as Subinventory_Name,
round(sum(a.SALDO),4)														as Transaction_Quantity,
'31-07-2018'																as Transaction_Date,
'DATA MIGRATION PA'															as Transaction_Reference,
max(ltrim(rtrim(e.UBICACION))) 												as Locator_Name,
CASE WHEN (round(sum(a.COSTO * a.SALDO) / (SUM(a.saldo)) ,4))>=1 
THEN (round(sum(a.COSTO * a.SALDO) / (SUM(a.saldo)) ,4))
ELSE 1
END                                                                         as Transaction_Cost,
''																			as Serial_Number_From,
''																			as Serial_Number_To	,
'501020.12601002.0000.00.00.000000.000000.000000.000000.274705'				as Distribution_Account,
'PA - THYSSENKRUPP ELEVADORES S.A.'											as Operating_Unit_Name
from TKPA_LATEST..STOCK as a
inner join TKPA_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
inner join DM_LATAM_DEV..O_STG_ITEM_CROSS_REF_ALT as c on ltrim(rtrim(b.PRODUCTO))=ltrim(rtrim(c.CROSS_REFERENCE))
inner join DM_LATAM_DEV..O_STG_ITEM_HEADER as d on c.ORIG_ITEM_NUMBER=d.ORIG_ITEM_NUMBER
inner join TKPA_LATEST..UBICAPRODALM as e on b.PRODUCTO=e.PRODUCTO and a.IdAlmacen=e.IDALMACEN
inner join TKPA_LATEST..ALMACEN as f on a.IdAlmacen=f.IdAlmacen
WHERE
A.PERIODO=2018
and a.IdAlmacen=1
AND A.MES=7
and a.SALDO > 0
and e.UBICACION like '%.%'
and d.LOCAL_VALIDATED = 1
and d.CROSSREF_VALIDATED = 1
and (d.PRICE_MANUAL_OVERRIDE IS NULL OR d.PRICE_MANUAL_OVERRIDE=0)
and d.COUNTRY='PA'
and d.ORIG_ITEM_NUMBER='PA3Y.6153.D.3'
group by c.ITEM_NUMBER



SELECT * FROM O_STG_ITEM_CROSS_REF_ALT a WHERE a.ORIG_ITEM_NUMBER='PA411' 

**/