SELECT a.RANKING,
	   a.DESCRIPTION_ESA,
	   a.ITEM_NUMBER,
	   b.PART_NUMBER
FROM O_PRE_ITEM_HEADER a
INNER JOIN O_PRE_ITEM_MFG_PART_NUM b
ON a.ORIG_ITEM_NUMBER=b.ORIG_ITEM_NUMBER
WHERE a.COUNTRY='CO'
AND a.RANKING>0
ORDER BY RANKING DESC

 