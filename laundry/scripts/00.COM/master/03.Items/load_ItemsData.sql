GO
:on error exit

GO
USE [$(targetDatabaseName)];


-- 1. clean up previous work
--

IF OBJECT_ID('tempdb.dbo.#TMP_UNIQUE_VALID_CODIGOINTERNO', 'U') IS NOT NULL
  DROP TABLE dbo.#TMP_UNIQUE_VALID_CODIGOINTERNO;

IF OBJECT_ID('$(targetDatabaseName).dbo.buildItemHeaderRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildItemHeaderRow_$(targetCountryCode);

IF OBJECT_ID('$(targetDatabaseName).dbo.buildItemCategoryRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildItemCategoryRow_$(targetCountryCode);

IF OBJECT_ID('$(targetDatabaseName).dbo.buildItemCrossRefRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildItemCrossRefRow_$(targetCountryCode);

IF OBJECT_ID('$(targetDatabaseName).dbo.buildItemMfgPartNumRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildItemMfgPartNumRow_$(targetCountryCode);

IF OBJECT_ID('$(targetDatabaseName).dbo.buildItemTransDefSubInvRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildItemTransDefSubInvRow_$(targetCountryCode);

IF OBJECT_ID('$(targetDatabaseName).dbo.buildItemTransDefLocRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildItemTransDefLocRow_$(targetCountryCode);

IF OBJECT_ID('$(targetDatabaseName).dbo.loadItemMasterData_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.loadItemMasterData_$(targetCountryCode);

IF OBJECT_ID('$(targetDatabaseName).dbo.rebuildPreStageItemstIndex', 'P') IS NOT NULL
  DROP PROCEDURE dbo.rebuildPreStageItemstIndex;

GO

CREATE PROCEDURE dbo.buildItemHeaderRow_$(targetCountryCode)  
(  
    @itemCode VARCHAR(28),
    @userStory VARCHAR(28) = ''
)  
AS  
BEGIN 
	INSERT INTO O_PRE_ITEM_HEADER  WITH (TABLOCK)
	(
	COUNTRY,
	LOCAL_VALIDATED,
	CROSSREF_VALIDATED,
	CLEANSED,
	RECOMMENDED,
	LAST_UPDATED,

	ORIG_ITEM_NUMBER,
	ITEM_NUMBER,
	OBSERVATION,
	DESCRIPTION,
	DESCRIPTION_ESA,
	ORGANIZATION_CODE,
	ITEM_INV_APPLICATION,
	TEMPLATE_NAME,
	UOM_CODE,
	ORG_NAME
	)
	SELECT
	TOP 1
	'$(targetCountryCode)',
	1,
	1,
	0,
	0,
	GETDATE(),

	RTRIM(LTRIM('$(targetCountryCode)' + $(sourceDatabaseName).dbo.PRODUCTOS.PRODUCTO)) AS ORIG_ITEM_NUMBER,
	LEFT(UPPER(@itemCode), 16) AS ITEM_NUMBER,
	'$(targetCountryCode)' + PRODUCTOS.CODIGOINTERNO AS OBSERVATION,
	@userStory + ' ' + LEFT(RTRIM(PRODUCTOS.DESCRIP),240 - LEN(@userStory)) AS DESCRIPTION,
	@userStory + ' ' + LEFT(RTRIM(PRODUCTOS.DESCRIP),240 - LEN(@userStory)) AS DESCRIPTION_ESA,
	'L$(targetCountryCode)' AS ORGANIZATION_CODE,
	'' AS ITEM_INV_APPLICATION,
	'LA98' AS TEMPLATE_NAME,
	'ea' AS UOM_CODE,
	'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
	FROM $(sourceDatabaseName).dbo.PRODUCTOS, $(sourceDatabaseName).dbo.UNIDADMEDIDA
	WHERE
	PRODUCTOS.UNIMED = UNIDADMEDIDA.ID AND
	PRODUCTOS.PRODUCTO  = @itemCode;

END;
  
GO

CREATE PROCEDURE dbo.buildItemCategoryRow_$(targetCountryCode)
(  
    @itemCode VARCHAR(28),
    @userStory VARCHAR(28) = ''
)  
AS  
BEGIN 
	INSERT INTO O_PRE_ITEM_CATEGORY  WITH (TABLOCK)
	(
	COUNTRY,
	LOCAL_VALIDATED,
	CROSSREF_VALIDATED,
	CLEANSED,
	RECOMMENDED,
	LAST_UPDATED,

	ORIG_ITEM_NUMBER,
	ITEM_NUMBER,
	CATEGORY_SET_NAME,
	CONCAT_CATEGORY_NAME,
	ORGANIZATION_CODE,
	ORG_NAME
	)
	SELECT
	TOP 1
	'$(targetCountryCode)',
	1,
	1,
	0,
	0,
	GETDATE(),

	RTRIM(LTRIM('$(targetCountryCode)' + $(sourceDatabaseName).dbo.PRODUCTOS.PRODUCTO)) AS ORIG_ITEM_NUMBER,
	LEFT(UPPER(@itemCode), 16) AS ITEM_NUMBER,
	'TKE PO Item Category',
	'10039800',
	'L$(targetCountryCode)' AS ORGANIZATION_CODE,
	'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
	FROM $(sourceDatabaseName).dbo.PRODUCTOS
	WHERE
	PRODUCTOS.PRODUCTO  = @itemCode;

END;
  
GO

CREATE PROCEDURE dbo.buildItemCrossRefRow_$(targetCountryCode)
(  
    @itemCode VARCHAR(28),
    @userStory VARCHAR(28) = ''
)  
AS  
BEGIN 
	INSERT INTO O_PRE_ITEM_CROSSREF  WITH (TABLOCK)
	(
	COUNTRY,
	LOCAL_VALIDATED,
	CROSSREF_VALIDATED,
	CLEANSED,
	RECOMMENDED,
	LAST_UPDATED,

	ORIG_ITEM_NUMBER,
	ITEM_NUMBER,
	CROSS_REFERENCE_TYPE,
	CROSS_REFERENCE,
	DESCRIPTION,
	ORGANIZATION_CODE,
	ORG_NAME
	)
	SELECT
	TOP 1
	'$(targetCountryCode)',
	1,
	1,
	0,
	0,
	GETDATE(),

	RTRIM(LTRIM('$(targetCountryCode)' + $(sourceDatabaseName).dbo.PRODUCTOS.PRODUCTO)) AS ORIG_ITEM_NUMBER,
	LEFT(UPPER(@itemCode), 16) AS ITEM_NUMBER,
	'SIGLA' AS CROSS_REFERENCE_TYPE,
	PRODUCTOS.PRODUCTO AS CROSS_REFERENCE,
	LEFT(@userStory + ' ' + LTRIM(PRODUCTOS.PRODUCTO) + ' / ' + PRODUCTOS.CODIGOINTERNO,240) AS DESCRIPTION,
	'L$(targetCountryCode)' AS ORGANIZATION_CODE,
	'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
	FROM $(sourceDatabaseName).dbo.PRODUCTOS
	WHERE
	PRODUCTOS.PRODUCTO  = @itemCode;
END;
  
GO


CREATE PROCEDURE dbo.buildItemMfgPartNumRow_$(targetCountryCode)
(  
    @itemCode VARCHAR(28),
    @userStory VARCHAR(28) = ''
)  
AS  
BEGIN 
	INSERT INTO O_PRE_ITEM_MFG_PART_NUM  WITH (TABLOCK)
	(
	COUNTRY,
	LOCAL_VALIDATED,
	CROSSREF_VALIDATED,
	CLEANSED,
	RECOMMENDED,
	LAST_UPDATED,

	ORIG_ITEM_NUMBER,
	ITEM_NUMBER,
	MANUFACTURER,
	DESCRIPTION,
	PART_NUMBER,
	ORG_NAME
	)
	SELECT
	TOP 1
	'$(targetCountryCode)',
	1,
	1,
	0,
	0,
	GETDATE(),

	RTRIM(LTRIM('$(targetCountryCode)' + $(sourceDatabaseName).dbo.PRODUCTOS.PRODUCTO)) AS ORIG_ITEM_NUMBER,
	LEFT(UPPER(@itemCode), 16) AS ITEM_NUMBER,
	'98' AS MANUFACTURER,
	@userStory + 'TKE BRASIL' AS DESCRIPTION,
	ISNULL(CODIGOINTERNO,'') + '(FIXME)' AS PART_NUMBER,
	'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
	FROM $(sourceDatabaseName).dbo.PRODUCTOS
	WHERE
	PRODUCTOS.PRODUCTO  = @itemCode;
END;

GO

CREATE PROCEDURE dbo.buildItemTransDefSubInvRow_$(targetCountryCode)
(  
    @itemCode VARCHAR(28),
    @userStory VARCHAR(28) = ''
)  
AS  
BEGIN 
    -- insert for TKE org Code
	INSERT INTO O_PRE_ITEM_TRANS_DEF_SUB_INV  WITH (TABLOCK)
	(
	COUNTRY,
	LOCAL_VALIDATED,
	CROSSREF_VALIDATED,
	CLEANSED,
	RECOMMENDED,
	LAST_UPDATED,

	ORIG_ITEM_NUMBER,
	ITEM_NUMBER,
	ORGANIZATION_CODE,
	SUBINVENTORY_NAME,
	ORG_NAME
	)
	SELECT
	TOP 1
	'$(targetCountryCode)',
	1,
	1,
	0,
	0,
	GETDATE(),

	RTRIM(LTRIM('$(targetCountryCode)' + a.PRODUCTO)) AS ORIG_ITEM_NUMBER,
	LEFT(UPPER(@itemCode), 16) AS ITEM_NUMBER,
	'CPA' AS ORGANIZATION_CODE,
	'DEP01' AS SUBINVENTORY_NAME,
	'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME

	FROM $(sourceDatabaseName).dbo.PRODUCTOS as a
	left outer join  $(sourceDatabaseName).dbo.UBICAPRODALM as b on a.PRODUCTO=b.PRODUCTO
	left outer join  $(sourceDatabaseName).dbo.ALMACEN as c on b.IDALMACEN=c.IdAlmacen
	WHERE
	a.PRODUCTO  = @itemCode;


END;
  
GO

CREATE PROCEDURE dbo.buildItemTransDefLocRow_$(targetCountryCode)
(  
    @itemCode VARCHAR(28),
    @userStory VARCHAR(28) = ''
)  
AS  
BEGIN 
	INSERT INTO O_PRE_ITEM_TRANS_DEF_LOC  WITH (TABLOCK)
	(
	COUNTRY,
	LOCAL_VALIDATED,
	CROSSREF_VALIDATED,
	CLEANSED,
	RECOMMENDED,
	LAST_UPDATED,

	ORIG_ITEM_NUMBER,
	ITEM_NUMBER,
	ORGANIZATION_CODE,
	SUBINVENTORY_NAME,
	LOCATOR_NAME,
	ORG_NAME
	)
	SELECT
	TOP 1
	'$(targetCountryCode)',
	1,
	1,
	0,
	0,
	GETDATE(),

	RTRIM(LTRIM('$(targetCountryCode)' + a.PRODUCTO)) AS ORIG_ITEM_NUMBER,
	LEFT(UPPER(@itemCode), 16) AS ITEM_NUMBER,
	'CPA' AS ORGANIZATION_CODE,
	'DEP01' AS SUBINVENTORY_NAME,
	ISNULL(b.UBICACION, 'UNKNOWN') AS LOCATOR_NAME,
	'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME

	FROM $(sourceDatabaseName).dbo.PRODUCTOS as a
	left outer join  $(sourceDatabaseName).dbo.UBICAPRODALM as b on a.PRODUCTO=b.PRODUCTO
	left outer join  $(sourceDatabaseName).dbo.ALMACEN as c on b.IDALMACEN=c.IdAlmacen
	WHERE
	a.PRODUCTO  = @itemCode;

END;

GO

CREATE PROCEDURE dbo.rebuildPreStageItemstIndex
AS
BEGIN
	UPDATE O_PRE_ITEM_HEADER SET CLEANSED=0;
	UPDATE O_PRE_ITEM_HEADER SET CLEANSED=1 WHERE ORIG_ITEM_NUMBER IN (SELECT ORIG_ITEM_NUMBER FROM O_STG_ITEM_HEADER)

	UPDATE O_PRE_ITEM_CATEGORY SET CLEANSED=0;
	UPDATE O_PRE_ITEM_CATEGORY SET CLEANSED=1 WHERE ORIG_ITEM_NUMBER IN (SELECT ORIG_ITEM_NUMBER FROM O_STG_ITEM_CATEGORY)

	UPDATE O_PRE_ITEM_CROSSREF SET CLEANSED=0;
	UPDATE O_PRE_ITEM_CROSSREF SET CLEANSED=1 WHERE ORIG_ITEM_NUMBER IN (SELECT ORIG_ITEM_NUMBER FROM O_STG_ITEM_CROSSREF)

	UPDATE O_PRE_ITEM_MFG_PART_NUM SET CLEANSED=0;
	UPDATE O_PRE_ITEM_MFG_PART_NUM SET CLEANSED=1 WHERE ORIG_ITEM_NUMBER IN (SELECT ORIG_ITEM_NUMBER FROM O_STG_ITEM_MFG_PART_NUM)

	UPDATE O_PRE_ITEM_TRANS_DEF_SUB_INV SET CLEANSED=0;
	UPDATE O_PRE_ITEM_TRANS_DEF_SUB_INV SET CLEANSED=1 WHERE ORIG_ITEM_NUMBER IN (SELECT ORIG_ITEM_NUMBER FROM O_STG_ITEM_TRANS_DEF_SUB_INV)

	UPDATE O_PRE_ITEM_TRANS_DEF_LOC SET CLEANSED=0;
	UPDATE O_PRE_ITEM_TRANS_DEF_LOC SET CLEANSED=1 WHERE ORIG_ITEM_NUMBER IN (SELECT ORIG_ITEM_NUMBER FROM O_STG_ITEM_TRANS_DEF_LOC)

END
GO


CREATE PROCEDURE dbo.loadItemMasterData_$(targetCountryCode)
AS
BEGIN
	DELETE FROM O_PRE_ITEM_HEADER WHERE COUNTRY='$(targetCountryCode)';
	DELETE FROM O_PRE_ITEM_CATEGORY WHERE COUNTRY='$(targetCountryCode)';
	DELETE FROM O_PRE_ITEM_CROSSREF WHERE COUNTRY='$(targetCountryCode)';
	DELETE FROM O_PRE_ITEM_MFG_PART_NUM WHERE COUNTRY='$(targetCountryCode)';
	DELETE FROM O_PRE_ITEM_TRANS_DEF_SUB_INV WHERE COUNTRY='$(targetCountryCode)';
	DELETE FROM O_PRE_ITEM_TRANS_DEF_LOC WHERE COUNTRY='$(targetCountryCode)';

	DELETE FROM COM_AUDIT_LOGS WHERE EVENT LIKE '%Items Master Data partial load%' AND COUNTRY='$(targetCountryCode)'

	DECLARE @unique_id VARCHAR(28)

	DECLARE @logThreshold int = 100
	DECLARE @count int = 0
	DECLARE @total int = 0


	DECLARE UNIQUE_VALID_STOCK_CURSOR CURSOR FOR   
	SELECT DISTINCT LEFT(RTRIM(LTRIM(b.PRODUCTO)),30)  
	FROM $(sourceDatabaseName).dbo.PRODUCTOS as b
	WHERE LEFT(RTRIM(LTRIM(b.PRODUCTO)),30) IS NOT NULL;

	SELECT @total=COUNT(LEFT(RTRIM(LTRIM(b.PRODUCTO)),30))  
	FROM $(sourceDatabaseName).dbo.PRODUCTOS as b 
	WHERE LEFT(RTRIM(LTRIM(b.PRODUCTO)),30) IS NOT NULL;

	OPEN UNIQUE_VALID_STOCK_CURSOR;

	FETCH NEXT FROM UNIQUE_VALID_STOCK_CURSOR   
	INTO @unique_id;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC dbo.buildItemHeaderRow_$(targetCountryCode) @unique_id
		EXEC dbo.buildItemCategoryRow_$(targetCountryCode) @unique_id
		EXEC dbo.buildItemCrossRefRow_$(targetCountryCode) @unique_id, 'SIGLA $(targetCountryCode) ID -> '
		EXEC dbo.buildItemMfgPartNumRow_$(targetCountryCode) @unique_id
		EXEC dbo.buildItemTransDefSubInvRow_$(targetCountryCode) @unique_id
		EXEC dbo.buildItemTransDefLocRow_$(targetCountryCode) @unique_id


		IF (@count%@logThreshold = 0)
		BEGIN
			PRINT CAST(@count AS VARCHAR(max))+'/'+CAST(@total AS VARCHAR(max));

			INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,SNAPSHOT_DATE)
			VALUES('$(targetCountryCode)', 'script', 'Items Master Data partial load ' + CAST(@count AS VARCHAR(max))+'/'+CAST(@total AS VARCHAR(max)), GETDATE(), @count, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));
		END
		
		SET @count = @count + 1


		FETCH NEXT FROM UNIQUE_VALID_STOCK_CURSOR   
			INTO @unique_id;
	END;

	CLOSE UNIQUE_VALID_STOCK_CURSOR;

	DEALLOCATE UNIQUE_VALID_STOCK_CURSOR;

	EXEC $(targetDatabaseName).dbo.rebuildPreStageItemstIndex

	DELETE FROM COM_AUDIT_LOGS WHERE EVENT LIKE '%Items Master Data partial load%' AND COUNTRY='$(targetCountryCode)'

	INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,SNAPSHOT_DATE)
	VALUES('$(targetCountryCode)', 'script', 'Items Master Data loaded from $(sourceDatabaseName) @ ' + dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'), GETDATE(), @count, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));

END;
GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,SNAPSHOT_DATE)
VALUES('$(targetCountryCode)', 'script', 'Items Master Data load procedures redefined', GETDATE(), dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));


