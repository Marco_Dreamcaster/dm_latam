IF OBJECT_ID('dbo.udf_EncodeProjectReference', 'FN') IS NOT NULL
	DROP FUNCTION dbo.udf_EncodeProjectReference;
GO

CREATE FUNCTION dbo.udf_EncodeProjectReference ( @country VARCHAR(2), @contrato VARCHAR(10) )
RETURNS VARCHAR(25)
AS
BEGIN

DECLARE @pmProjectReference VARCHAR(25)

SET @pmProjectReference = UPPER(@country+'_'+replace(replace(RTRIM(LTRIM(@contrato)),'-','_'),' ','_'));

RETURN @pmProjectReference
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated udf_EncodeProjectReference function definition', GETDATE());

