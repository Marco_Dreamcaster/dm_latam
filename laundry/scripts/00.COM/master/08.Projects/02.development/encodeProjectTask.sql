IF OBJECT_ID('dbo.udf_EncodeProjectTask', 'FN') IS NOT NULL
	DROP FUNCTION dbo.udf_EncodeProjectTask;
GO

CREATE FUNCTION dbo.udf_EncodeProjectTask ( @country VARCHAR(2), @reference VARCHAR(10) )
RETURNS VARCHAR(25)
AS
BEGIN

DECLARE @projectTask VARCHAR(25)

SET @projectTask = UPPER(@country+'_'+replace(replace(RTRIM(LTRIM(@reference)),'-','_'),' ','_'));

RETURN @projectTask
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated udf_EncodeProjectTask function definition', GETDATE());

