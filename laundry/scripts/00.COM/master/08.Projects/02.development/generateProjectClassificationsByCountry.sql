IF OBJECT_ID('dbo.generateProjectClassificationsByCountry', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateProjectClassificationsByCountry;

GO

CREATE PROCEDURE dbo.generateProjectClassificationsByCountry
(
@country VARCHAR(2)
)
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRANSACTION

SELECT a.[PROJECT_CLASSIFICATIONS_ID] AS projectClassificationsID
,b.[PROJECT_NAME] AS projectName
,a.[CLASS_CATEGORY] AS classCategory
,a.[CLASS_CODE] AS classCode
,dbo.udf_EncodeOrgName(@country) as orgName
FROM [dbo].[O_STG_PROJECT_CLASSIFICATIONS] a JOIN
[dbo].[O_STG_PROJECT_HEADER] b
ON (a.[PROJECT_NAME] = b.[PROJECT_NAME])
WHERE
b.[COUNTRY]=@country AND
(b.[TEMPLATE_NAME] LIKE '%DM NI%' OR b.[TEMPLATE_NAME] LIKE '%DM MOD%') AND
b.[DISMISSED]=0 AND
b.[LOCAL_VALIDATED]=1 AND
b.[CROSSREF_VALIDATED]=1

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateProjectClassificationsByCountry procedure definition', GETDATE());