IF OBJECT_ID('dbo.generateProjectClassificationsByCountry_ALT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateProjectClassificationsByCountry_ALT;

GO

CREATE PROCEDURE dbo.generateProjectClassificationsByCountry_ALT
(
@country VARCHAR(2)
)
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRANSACTION

SELECT a.[PROJECT_CLASSIFICATIONS_ID] AS projectClassificationsID
,b.[PROJECT_NAME] AS projectName
,'TKE PO Item Category' AS classCategory
,b.LINE AS classCode
,dbo.udf_EncodeOrgName(@country) as orgName
FROM
[dbo].[O_STG_PROJECT_HEADER] b
WHERE
b.[COUNTRY]=@country AND
(b.[TEMPLATE_NAME] LIKE '%DM NI%' OR b.[TEMPLATE_NAME] LIKE '%DM MOD%') AND
b.DISMISSED=0 AND
b.[LOCAL_VALIDATED]=1 AND
b.[CROSSREF_VALIDATED]=1

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateProjectClassificationsByCountry_ALT procedure definition', GETDATE());