IF OBJECT_ID('dbo.generateProjectHeaderByCountry', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateProjectHeaderByCountry;

GO

CREATE PROCEDURE dbo.generateProjectHeaderByCountry
(
@country VARCHAR(2)
)
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRANSACTION

EXEC dbo.adjustProjectDates @country

SELECT
b.PM_PROJECT_REFERENCE							as pmProjectReference
,b.PROJECT_NAME		        as projectName
,b.PROJECT_TYPE       as projectType
,b.PROJECT_STATUS       as projectStatus
,b.TEMPLATE_NAME       as templateName
,b.START_DATE       as startDate
,b.COMPLETION_DATE       as completionDate
,b.DESCRIPTION       as description
,b.ORGANIZATION_CODE       as organizationCode
,b.CUSTOMER_NUMBER       as customerNumber
,b.BILL_TO_ADDRESS_REF       as billToAddressRef
,b.SHIP_TO_ADDRESS_REF       as shipToAddressRef
,b.CONTACT_REF       as contactRef
,b.PROJECT_MANAGER_REF       as projectManagerRef
,b.CREDIT_RECEIVER_REF       as creditReceiverRef
,b.SALES_REPRESENTATIVE_REF       as salesRepresentativeRef
,b.SERVICE_MANAGER_REF       as serviceManagerRef
,b.DUMMY_KEY_MEMBER_REF       as dummyKeyMemberRef
,b.ATTRIBUTE2       as attribute2
,b.ATTRIBUTE6       as attribute6
,b.ATTRIBUTE8       as attribute8
,b.BOOKING_PKG_RCVD       as bookingPkgRcvd
,b.COMPLETE_PCKG_RCVD_DATE       as completePckgRcvdDate
,b.CONTRACT_BOOKED_DATE       as contractBookedDate
,b.CONTRACT_DATE       as contractDate
,b.PACKAGE_AND_PO_SALES       as packageAndPoSales
,b.EXECUTION_AND_RETURN_DATE       as executionAndReturnDate
,b.CONTRACT_TYPE_TKE       as contractTypeTke
,b.CUSTOMER_PO_NUMBER       as customerPoNumber
,b.OUTPUT_TAX_CODE       as outputTaxCode
,dbo.udf_EncodeOrgName(@country) as orgName
,b.[MERGED_ID]                                  as mergedId
,b.[CHILD_LESS]                                 as childLess
,b.[SAME_COUNTRY_PARENT]                        as sameCountryParent

FROM O_STG_PROJECT_HEADER b
WHERE
b.[COUNTRY]=@country AND
(b.[TEMPLATE_NAME] LIKE '%DM NI%' OR b.[TEMPLATE_NAME] LIKE '%DM MOD%') AND
b.DISMISSED=0 AND
b.[LOCAL_VALIDATED]=1 AND
b.[CROSSREF_VALIDATED]=1


IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateProjectHeaderByCountry procedure definition', GETDATE());