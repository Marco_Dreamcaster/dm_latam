IF OBJECT_ID('dbo.generateProjectRetRulesByCountry', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateProjectRetRulesByCountry;

GO

CREATE PROCEDURE dbo.generateProjectRetRulesByCountry
(
@country VARCHAR(2)
)
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRANSACTION

SELECT b.[PROJECT_NAME]  AS projectName
,a.[RETENTION_PERCENTAGE] AS retentionPercentage
,a.[RETAINED_AMOUNT] AS retainedAmount
,dbo.udf_EncodeOrgName(@country) as orgName
FROM [dbo].[O_STG_PROJECT_RET_RULES] a JOIN
[dbo].[O_STG_PROJECT_HEADER] b
ON (a.[PROJECT_NAME] = b.[PROJECT_NAME])
WHERE
b.[COUNTRY]=@country AND
(b.[TEMPLATE_NAME] LIKE '%DM NI%' OR b.[TEMPLATE_NAME] LIKE '%DM MOD%') AND
b.DISMISSED=0 AND
b.[LOCAL_VALIDATED]=1 AND
b.[CROSSREF_VALIDATED]=1

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateProjectRetRulesByCountry procedure definition', GETDATE());