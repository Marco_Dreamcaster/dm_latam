GO
:on error exit

GO
USE [$(TargetDatabaseName)];

/** 1. Cleanup **/

/** O_PRE_PROJECT_HEADER loaders **/

IF OBJECT_ID('$(TargetDatabaseName).dbo.buildProjectHeaderRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildProjectHeaderRow_$(targetCountryCode);

IF OBJECT_ID('$(TargetDatabaseName).dbo.buildServiceHeaderRow_$(targetCountryCode)', 'P') IS NOT NULL
	DROP PROCEDURE dbo.buildServiceHeaderRow_$(targetCountryCode);

IF OBJECT_ID('$(TargetDatabaseName).dbo.buildRepairHeaderRow_$(targetCountryCode)', 'P') IS NOT NULL
	DROP PROCEDURE dbo.buildRepairHeaderRow_$(targetCountryCode);

/** O_PRE_PROJECT_CLASSIFICATIONS loaders **/

IF OBJECT_ID('$(TargetDatabaseName).dbo.buildProjectClassificationsRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildProjectClassificationsRow_$(targetCountryCode);

IF OBJECT_ID('$(TargetDatabaseName).dbo.buildServiceClassificationsRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildServiceClassificationsRow_$(targetCountryCode);

IF OBJECT_ID('$(TargetDatabaseName).dbo.buildRepairClassificationsRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildRepairClassificationsRow_$(targetCountryCode);


/** O_PRE_PROJECT_TASKS loaders **/

IF OBJECT_ID('$(TargetDatabaseName).dbo.buildProjectTasksRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildProjectTasksRow_$(targetCountryCode);

IF OBJECT_ID('$(TargetDatabaseName).dbo.buildServiceTasksRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildServiceTasksRow_$(targetCountryCode);

IF OBJECT_ID('$(TargetDatabaseName).dbo.buildRepairTasksRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildRepairTasksRow_$(targetCountryCode);

/** O_PRE_PROJECT_RET_RULES loaders **/

IF OBJECT_ID('$(TargetDatabaseName).dbo.buildProjectRetRulesRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildProjectRetRulesRow_$(targetCountryCode);

IF OBJECT_ID('$(TargetDatabaseName).dbo.buildServiceRetRulesRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildServiceRetRulesRow_$(targetCountryCode);

IF OBJECT_ID('$(TargetDatabaseName).dbo.buildRepairRetRulesRow_$(targetCountryCode)', 'P') IS NOT NULL
	DROP PROCEDURE dbo.buildRepairRetRulesRow_$(targetCountryCode);


/** loads Projects, Services and Repairs **/

IF OBJECT_ID('$(TargetDatabaseName).dbo.loadProjectsMasterData_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.loadProjectsMasterData_$(targetCountryCode);

IF OBJECT_ID('$(TargetDatabaseName).dbo.rebuildPreStageProjectIndex', 'P') IS NOT NULL
  DROP PROCEDURE dbo.rebuildPreStageProjectIndex;
GO

-- 1.1 define rowbuilders
-- maps SIGLA fields to ORACLE specs

---- PRE PROJECT HEADER

CREATE PROCEDURE dbo.buildProjectHeaderRow_$(targetCountryCode)  
(  
    @itemCode VARCHAR(16),  -- declarar o pmRef e o smRef
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN 
	INSERT INTO $(targetDatabaseName).dbo.O_PRE_PROJECT_HEADER  WITH (TABLOCK)
	(
		COUNTRY,
		LOCAL_VALIDATED,
		CROSSREF_VALIDATED,
		CLEANSED,
		RECOMMENDED,
		LAST_UPDATED,
		SIGLA_PRIMARY_KEY,

		PM_PROJECT_REFERENCE,
		PROJECT_NAME,
		PROJECT_TYPE,
		PROJECT_STATUS,
		TEMPLATE_NAME,
		START_DATE,
		COMPLETION_DATE,
		DESCRIPTION,
		ORGANIZATION_CODE,
		CUSTOMER_NUMBER,
		BILL_TO_ADDRESS_REF,
		SHIP_TO_ADDRESS_REF,
		PROJECT_MANAGER_REF,
		SERVICE_MANAGER_REF,
		SALES_REPRESENTATIVE_REF,
		DUMMY_KEY_MEMBER_REF,
		OBRA_CERRADA,
		OBRA_EN_CURSO,
		ORG_NAME
	)
	SELECT
		TOP 1
		'$(targetCountryCode)',
		0,
		0,
		0,
		0 AS RECOMMENDED,
		GETDATE(),
		REPLACE(REPLACE(LEFT(RTRIM(LTRIM('$(targetCountryCode)' + '_' + CAST(CONTRATO.CONTRATO AS VARCHAR))), 16),' ','_'),'-','_') AS SIGLA_PRIMARY_KEY,

		LEFT('$(targetCountryCode)' + '_' + CAST(CONTRATO.CONTRATO AS VARCHAR), 16) AS PM_PROJECT_REFERENCE,
		
		UPPER(
			REPLACE(
			REPLACE(
				LEFT( '$(targetCountryCode)' + '_' +
						RTRIM(LTRIM(CAST(CONTRATO.CONTRATO AS VARCHAR))) +	'_' +
						RTRIM(LTRIM(CONTRATO.OBRA))
					,30)
			,' ','_')
			,'-','_')
		) COLLATE sql_latin1_general_cp1251_ci_as AS PM_PROJECT_NAME,

	  'POC' AS PROJECT_TYPE, -- CC, POC (is it a percentage? e.g. 80%)
	  'Approved' AS PROJECT_STATUS, -- Approved, Closed, Completed (CC), Completed (POC)
	  CASE CONTRATO.TIPOCONT
		  WHEN 01 THEN 'DM NI TEMPLATE $(targetCountryCode)'
		  WHEN 10 THEN 'DM MOD TEMPLATE $(targetCountryCode)'
		  ELSE 'SERV TEMPLATE $(targetCountryCode)'
	  END AS TEMPLATE_NAME,
	  CONTRATO.FECHA AS START_DATE,
	  CONTRATO.FECHA AS COMPLETION_DATE,  -- just for building purposes, needs revision!
    LEFT(CONTRATO.OBRA, 250) AS DESCRIPTON,
	'' AS ORGANIZATION_CODE,
    '$(targetCountryCode)' + RTRIM(LTRIM(CAST(CLIENTES.CLIENTE AS varchar(30)))) + '-CC' AS CUSTOM_NUMBER,
    '$(targetCountryCode)' + RTRIM(LTRIM(CAST(CLIENTES.CLIENTE AS varchar(30)))) + '-CA' AS BILL_TO_ADDRESS_REF,
    '$(targetCountryCode)' + RTRIM(LTRIM(CAST(CLIENTES.CLIENTE AS varchar(30)))) + '-CA' AS SHIP_TO_ADDRESS_REF,
    '$(pmRef)' AS PROJECT_MANAGER_REF,   -- substituir as vari�veis
    '$(smRef)' AS SERVICE_MANAGER_REF,
    '$(salesRef)' AS SALES_REPRESENTATIVE_REF,
    '$(dummyRef)' AS DUMMY_KEY_MEMBER_REF,
	CASE ISNULL(CONTRATO.OBRACERRADA,'N') WHEN 'S' THEN 1 WHEN 'N' THEN 0 END,
	CASE ISNULL(CONTRATO.OBRAENCURSO,'S') WHEN 'S' THEN 1 WHEN 'N' THEN 0 END,
    '$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME

  FROM $(sourceDatabaseName).dbo.CONTRATO
  INNER JOIN $(sourceDatabaseName).dbo.CLIENTES ON (CONTRATO.CLIENTE = CLIENTES.CLIENTE)
  WHERE CONTRATO.CONTRATO = @itemCode;
END;
GO

-- service contracts

CREATE PROCEDURE dbo.buildServiceHeaderRow_$(targetCountryCode)  
(  
    @itemCode VARCHAR(16),
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN 
	INSERT INTO $(targetDatabaseName).dbo.O_PRE_PROJECT_HEADER  WITH (TABLOCK)
	(
		COUNTRY,
		LOCAL_VALIDATED,
		CROSSREF_VALIDATED,
		CLEANSED,
		RECOMMENDED,
		LAST_UPDATED,
		SIGLA_PRIMARY_KEY,

		PM_PROJECT_REFERENCE,
		PROJECT_NAME,
		PROJECT_TYPE,
		PROJECT_STATUS,
		TEMPLATE_NAME,
		START_DATE,
		COMPLETION_DATE,
		DESCRIPTION,
		ORGANIZATION_CODE,
		CUSTOMER_NUMBER,
		BILL_TO_ADDRESS_REF,
		SHIP_TO_ADDRESS_REF,
		PROJECT_MANAGER_REF,
		SERVICE_MANAGER_REF,
		SALES_REPRESENTATIVE_REF,
		DUMMY_KEY_MEMBER_REF,
		ORG_NAME
	)
	SELECT
		TOP 1
		'$(targetCountryCode)',
		0,
		0,
		0,
		0 AS RECOMMENDED,
		GETDATE(),
		REPLACE(REPLACE(LEFT(RTRIM(LTRIM('$(targetCountryCode)' + '-' + CAST(a.CONTRATO AS VARCHAR))), 16),' ','_'),'-','_') AS SIGLA_PRIMARY_KEY,

		LEFT('$(targetCountryCode)_' + CAST(a.CONTRATO AS VARCHAR), 16) AS PM_PROJECT_REFERENCE,
		UPPER(
			REPLACE(
			REPLACE(
				LEFT( '$(targetCountryCode)' + '_' +
						RTRIM(LTRIM(CAST(a.CONTRATO AS VARCHAR))) +	'_' +
						RTRIM(LTRIM(a.OBRA))
					,30)
			,' ','_')
			,'-','_')
		) COLLATE sql_latin1_general_cp1251_ci_as AS PM_PROJECT_NAME,

	  'SVC' AS PROJECT_TYPE, -- CC, POC (is it a percentage? e.g. 80%)
	  'Approved' AS PROJECT_STATUS, -- Approved, Closed, Completed (CC), Completed (POC)
	  'SERV TEMPLATE $(targetCountryCode)'  AS TEMPLATE_NAME,
	  C.INICIO AS START_DATE,
	  case when c.IDRENOVAC = 2 then C.TERMINO 
		else dateadd(year,5,c.TERMINO)
	  end AS COMPLETION_DATE,  -- just for building purposes, needs revision!
    LEFT(A.OBRA, 250) AS DESCRIPTON,
	'' AS ORGANIZATION_CODE,
    '$(targetCountryCode)' + RTRIM(LTRIM(CAST(B.CLIENTE AS varchar(30)))) + '-CC' AS CUSTOM_NUMBER,
    '$(targetCountryCode)' + RTRIM(LTRIM(CAST(B.CLIENTE AS varchar(30)))) + '-CA' AS BILL_TO_ADDRESS_REF,
    '$(targetCountryCode)' + RTRIM(LTRIM(CAST(B.CLIENTE AS varchar(30)))) + '-CA' AS SHIP_TO_ADDRESS_REF,
    '$(pmRef)' AS PROJECT_MANAGER_REF,
    '$(smRef)' AS SERVICE_MANAGER_REF,
    '$(salesRef)' AS SALES_REPRESENTATIVE_REF,
    '$(dummyRef)' AS DUMMY_KEY_MEMBER_REF,
    '$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
  FROM $(sourceDatabaseName).dbo.MANTEN AS A
	INNER JOIN $(sourceDatabaseName).dbo.CLIENTES AS B ON A.CLIENTE = B.CLIENTE
	INNER JOIN $(sourceDatabaseName).dbo.TITRENOVAC AS C ON A.CONTRATO=C.CONTRATO
	AND RENOVACION IN 
	(SELECT TOP 1 RENOVACION FROM $(sourceDatabaseName).dbo.TITRENOVAC WHERE CONTRATO=a.CONTRATO ORDER BY RENOVACION DESC)
	AND A.CONTRATO = @itemCode;
END;
GO


CREATE PROCEDURE dbo.buildProjectClassificationsRow_$(targetCountryCode) 
(  
    @itemCode VARCHAR(16),
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN
	INSERT INTO $(targetDatabaseName).dbo.O_PRE_PROJECT_CLASSIFICATIONS  WITH (TABLOCK)
	(
		COUNTRY,
		LOCAL_VALIDATED,
		CROSSREF_VALIDATED,
		CLEANSED,
		RECOMMENDED,
		LAST_UPDATED,

		PROJECT_CLASSIFICATIONS_ID,
		PROJECT_NAME,
		CLASS_CATEGORY,
		CLASS_CODE,
		ORG_NAME
	)
	SELECT
	  TOP 1
		'$(targetCountryCode)',
		0,
		0,
		0,
		0 AS RECOMMENDED,
		GETDATE(),

		UPPER(
			REPLACE(
			REPLACE(
				LEFT( '$(targetCountryCode)' + '_' +
						RTRIM(LTRIM(CAST(CONTRATO.CONTRATO AS VARCHAR))) +	'_' +
						RTRIM(LTRIM(CONTRATO.OBRA))
					,30)
			,' ','_')
			,'-','_')
		) + '.1' COLLATE sql_latin1_general_cp1251_ci_as AS PROJECT_CLASSIFICATIONS_ID,


		UPPER(
			REPLACE(
			REPLACE(
				LEFT( '$(targetCountryCode)' + '_' +
						RTRIM(LTRIM(CAST(CONTRATO.CONTRATO AS VARCHAR))) +	'_' +
						RTRIM(LTRIM(CONTRATO.OBRA))
					,30)
			,' ','_')
			,'-','_')
		) COLLATE sql_latin1_general_cp1251_ci_as AS PM_PROJECT_NAME,

		'BUILDING TYPE',
		'4+ STAR HOTEL' AS CLASS_CODE,
		'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
	FROM $(sourceDatabaseName).dbo.CONTRATO
	WHERE CONTRATO.CONTRATO = @itemCode;

	INSERT INTO $(targetDatabaseName).dbo.O_PRE_PROJECT_CLASSIFICATIONS  WITH (TABLOCK)
	(
		COUNTRY,
		LOCAL_VALIDATED,
		CROSSREF_VALIDATED,
		CLEANSED,
		RECOMMENDED,
		LAST_UPDATED,

		PROJECT_CLASSIFICATIONS_ID,
		PROJECT_NAME,
		CLASS_CATEGORY,
		CLASS_CODE,
		ORG_NAME
	)
	SELECT
	  TOP 1
		'$(targetCountryCode)',
		0,
		0,
		0,
		0 AS RECOMMENDED,
		GETDATE(),

		UPPER(
			REPLACE(
			REPLACE(
				LEFT( '$(targetCountryCode)' + '_' +
						RTRIM(LTRIM(CAST(CONTRATO.CONTRATO AS VARCHAR))) +	'_' +
						RTRIM(LTRIM(CONTRATO.OBRA))
					,30)
			,' ','_')
			,'-','_')
		) + '.2' COLLATE sql_latin1_general_cp1251_ci_as AS PROJECT_CLASSIFICATIONS_ID,

		UPPER(
			REPLACE(
			REPLACE(
				LEFT( '$(targetCountryCode)' + '_' +
						RTRIM(LTRIM(CAST(CONTRATO.CONTRATO AS VARCHAR))) +	'_' +
						RTRIM(LTRIM(CONTRATO.OBRA))
					,30)
			,' ','_')
			,'-','_')
		) COLLATE sql_latin1_general_cp1251_ci_as AS PM_PROJECT_NAME,

		'BUILDING CLASSIFICATION',
		'AIRPORT' AS CLASS_CODE,
		'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
	FROM $(sourceDatabaseName).dbo.CONTRATO 
	WHERE CONTRATO.CONTRATO = @itemCode;
END;
GO

CREATE PROCEDURE dbo.buildServiceClassificationsRow_$(targetCountryCode) 
(  
    @itemCode VARCHAR(16),
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN
	INSERT INTO $(targetDatabaseName).dbo.O_PRE_PROJECT_CLASSIFICATIONS  WITH (TABLOCK)
	(
		COUNTRY,
		LOCAL_VALIDATED,
		CROSSREF_VALIDATED,
		CLEANSED,
		RECOMMENDED,
		LAST_UPDATED,

		PROJECT_CLASSIFICATIONS_ID,
		PROJECT_NAME,
		CLASS_CATEGORY,
		CLASS_CODE,
		ORG_NAME
	)
	SELECT
	  TOP 1
		'$(targetCountryCode)',
		0,
		0,
		0,
		0 AS RECOMMENDED,
		GETDATE(),

		UPPER(
			REPLACE(
			REPLACE(
				LEFT( '$(targetCountryCode)' + '_' +
						RTRIM(LTRIM(CAST(a.CONTRATO AS VARCHAR))) +	'_' +
						RTRIM(LTRIM(a.OBRA))
					,30)
			,' ','_')
			,'-','_')
		) + '.1' COLLATE sql_latin1_general_cp1251_ci_as AS PROJECT_CLASSIFICATIONS_ID,


		UPPER(
			REPLACE(
			REPLACE(
				LEFT( '$(targetCountryCode)' + '_' +
						RTRIM(LTRIM(CAST(a.CONTRATO AS VARCHAR))) +	'_' +
						RTRIM(LTRIM(a.OBRA))
					,30)
			,' ','_')
			,'-','_')
		) COLLATE sql_latin1_general_cp1251_ci_as AS PM_PROJECT_NAME,

		'BUILDING TYPE',
		'4+ STAR HOTEL' AS CLASS_CODE,
		'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
	FROM $(sourceDatabaseName).dbo.MANTEN a
	WHERE a.CONTRATO = @itemCode;

	INSERT INTO $(targetDatabaseName).dbo.O_PRE_PROJECT_CLASSIFICATIONS  WITH (TABLOCK)
	(
		COUNTRY,
		LOCAL_VALIDATED,
		CROSSREF_VALIDATED,
		CLEANSED,
		RECOMMENDED,
		LAST_UPDATED,

		PROJECT_CLASSIFICATIONS_ID,
		PROJECT_NAME,
		CLASS_CATEGORY,
		CLASS_CODE,
		ORG_NAME
	)
	SELECT
	  TOP 1
		'$(targetCountryCode)',
		0,
		0,
		0,
		0 AS RECOMMENDED,
		GETDATE(),

		UPPER(
			REPLACE(
			REPLACE(
				LEFT( '$(targetCountryCode)' + '_' +
						RTRIM(LTRIM(CAST(a.CONTRATO AS VARCHAR))) +	'_' +
						RTRIM(LTRIM(a.OBRA))
					,30)
			,' ','_')
			,'-','_')
		) + '.2' COLLATE sql_latin1_general_cp1251_ci_as AS PROJECT_CLASSIFICATIONS_ID,

		UPPER(
			REPLACE(
			REPLACE(
				LEFT( '$(targetCountryCode)' + '_' +
						RTRIM(LTRIM(CAST(a.CONTRATO AS VARCHAR))) +	'_' +
						RTRIM(LTRIM(a.OBRA))
					,30)
			,' ','_')
			,'-','_')
		) COLLATE sql_latin1_general_cp1251_ci_as AS PM_PROJECT_NAME,

		'BUILDING CLASSIFICATION',
		'AIRPORT' AS CLASS_CODE,
		'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
	FROM $(sourceDatabaseName).dbo.MANTEN a 
	WHERE a.CONTRATO = @itemCode;

	INSERT INTO $(targetDatabaseName).dbo.O_PRE_PROJECT_CLASSIFICATIONS  WITH (TABLOCK)
	(
		COUNTRY,
		LOCAL_VALIDATED,
		CROSSREF_VALIDATED,
		CLEANSED,
		RECOMMENDED,
		LAST_UPDATED,

		PROJECT_CLASSIFICATIONS_ID,
		PROJECT_NAME,
		CLASS_CATEGORY,
		CLASS_CODE,
		ORG_NAME
	)
	SELECT
	  TOP 1
		'$(targetCountryCode)',
		0,
		0,
		0,
		0 AS RECOMMENDED,
		GETDATE(),

		UPPER(
			REPLACE(
			REPLACE(
				LEFT( '$(targetCountryCode)' + '_' +
						RTRIM(LTRIM(CAST(a.CONTRATO AS VARCHAR))) +	'_' +
						RTRIM(LTRIM(a.OBRA))
					,30)
			,' ','_')
			,'-','_')
		) + '.3' COLLATE sql_latin1_general_cp1251_ci_as AS PROJECT_CLASSIFICATIONS_ID,

		UPPER(
			REPLACE(
			REPLACE(
				LEFT( '$(targetCountryCode)' + '_' +
						RTRIM(LTRIM(CAST(a.CONTRATO AS VARCHAR))) +	'_' +
						RTRIM(LTRIM(a.OBRA))
					,30)
			,' ','_')
			,'-','_')
		) COLLATE sql_latin1_general_cp1251_ci_as AS PM_PROJECT_NAME,

		'CONTRACT TYPE',
		'SERVICE' AS CLASS_CODE,
		'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
	FROM $(sourceDatabaseName).dbo.MANTEN a 
	WHERE a.CONTRATO = @itemCode;


END;
GO


CREATE PROCEDURE dbo.buildProjectTasksRow_$(targetCountryCode)
(  
    @contractCode VARCHAR(16),
    @increment int
)  
AS  
BEGIN 
	DECLARE @equipo_id VARCHAR(16)

	DECLARE EQUIPOS_PER_CURSOR CURSOR FOR   
	  SELECT IdEquipo
		FROM $(sourceDatabaseName).dbo.DETCONT
		WHERE DETCONT.CONTRATO=@contractCode;

	OPEN EQUIPOS_PER_CURSOR;

	FETCH NEXT FROM EQUIPOS_PER_CURSOR   
	INTO @equipo_id;

	Declare @count int = 1

	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO $(targetDatabaseName).dbo.O_PRE_PROJECT_TASKS  WITH (TABLOCK)
		(
		COUNTRY,
		LOCAL_VALIDATED,
		CROSSREF_VALIDATED,
		CLEANSED,
		RECOMMENDED,
		LAST_UPDATED,
		SIGLA_PRIMARY_KEY,

		PROJECT_NAME,
		TASK_NUMBER,
		TASK_NAME,
		PARENT_TASK_NUMBER,
		DESCRIPTION,
		START_DATE,
		FINISH_DATE,
		SERVICE_TYPE,
		ALLOW_CHARGE,
		ATTRIBUTE1,
		ATTRIBUTE2,
		ATTRIBUTE3,
		ATTRIBUTE4,
		ATTRIBUTE5,
		ATTRIBUTE6,
		ATTRIBUTE7,
		ATTRIBUTE8,
		NO_OF_FLOORS,
		SPEED,
		CAPACITY,
		ORG_NAME
		)
		SELECT
		'$(targetCountryCode)',
		0,
		0,
		0,
		0 AS RECOMMENDED,
		GETDATE(),
		REPLACE(REPLACE(LEFT(RTRIM(LTRIM('$(targetCountryCode)' + '-' + CAST(a.CONTRATO AS VARCHAR))), 16),' ','_'),'-','_') AS SIGLA_PRIMARY_KEY,


		UPPER(
			REPLACE(
			REPLACE(
				LEFT( '$(targetCountryCode)' + '_' +
						RTRIM(LTRIM(CAST(b.CONTRATO AS VARCHAR))) +	'_' +
						RTRIM(LTRIM(b.OBRA))
					,30)
			,' ','_')
			,'-','_')
		) COLLATE sql_latin1_general_cp1251_ci_as AS PM_PROJECT_NAME,

		LEFT('$(targetCountryCode)' + '_' + CAST (c.IdEquipo AS VARCHAR(max)),20) AS TASK_NUMBER,
		LEFT('$(targetCountryCode)' + '_' + CAST (c.IdEquipo AS VARCHAR(max)),20) AS TASK_NAME,
		'' AS PARENT_TASK_NUMBER,
		LEFT(c.OE,255) AS DESCRIPTION,
		a.FCHMONTAJE AS START_DATE,
		a.FCHENTREGA AS FINISH_DATE,

		CASE WHEN b.TIPOCONT = '10' THEN 'MOD'
		ELSE 'NI'
		END AS SERVICE_TYPE,

		'Y' AS ALLOW_CHARGE,
		'' AS ATTRIBUTE1,
		LEFT(substring(ltrim(rtrim(d.DESCRIP)),1,3),40) AS ATTRIBUTE2,
		d.DESCRIP as ATTRIBUTE3,

		CASE WHEN LEFT(substring(ltrim(rtrim(d.DESCRIP)),4,20),35) IS NULL THEN LEFT(substring(ltrim(rtrim(d.DESCRIP)),4,20),35)
		ELSE ' '
		END AS ATTRIBUTE4,

		CASE WHEN ISDATE(c.HABILITACION)=1 THEN LEFT(CAST(c.HABILITACION AS VARCHAR),11)
		ELSE GETDATE()
		END AS Attribute5,

		'N' AS ATTRIBUTE6,
		LEFT(e.DESCRIP,40) AS ATTRIBUTE7,
		LEFT(c.OE,40) AS ATTRIBUTE8,
		c.PARADAS AS NO_OF_FLOORS,
		c.VELOCIDAD AS SPEED,
		c.CAPACIDAD AS CAPACITY,
		'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME

		FROM $(sourceDatabaseName).dbo.detcont a
		INNER JOIN $(sourceDatabaseName).dbo.CONTRATO b on (a.contrato=b.contrato)
		INNER JOIN $(sourceDatabaseName).dbo.equipos c on (a.IdEquipo=c.IdEquipo)
		INNER JOIN $(sourceDatabaseName).dbo.tipos d on (d.TIPO = c.TIPO)
		LEFT JOIN $(sourceDatabaseName).dbo.tipoequipo e on (c.IDTIPOEQUIPO=e.ID)

		WHERE b.CONTRATO = @contractCode
		AND a.IdEquipo = @equipo_id
		
		SET @count = @count + 1
		
		FETCH NEXT FROM EQUIPOS_PER_CURSOR   
		INTO @equipo_id;
	END;

	CLOSE EQUIPOS_PER_CURSOR;
	DEALLOCATE EQUIPOS_PER_CURSOR;
END;
GO

CREATE PROCEDURE dbo.buildServiceTasksRow_$(targetCountryCode)
(  
    @contractCode VARCHAR(16),
    @increment int
)  
AS  
BEGIN 
	INSERT INTO $(targetDatabaseName).dbo.O_PRE_PROJECT_TASKS  WITH (TABLOCK)
	(
	COUNTRY,
	LOCAL_VALIDATED,
	CROSSREF_VALIDATED,
	CLEANSED,
	RECOMMENDED,
	LAST_UPDATED,
	SIGLA_PRIMARY_KEY,

	PROJECT_NAME,
	TASK_NUMBER,
	TASK_NAME,
	PARENT_TASK_NUMBER,
	DESCRIPTION,
	START_DATE,
	FINISH_DATE,
	SERVICE_TYPE,
	ALLOW_CHARGE,
	ATTRIBUTE1,
	ATTRIBUTE2,
	ATTRIBUTE3,
	ATTRIBUTE4,
	ATTRIBUTE5,
	ATTRIBUTE6,
	ATTRIBUTE7,
	ORG_NAME
	)
	SELECT TOP 1
	'$(targetCountryCode)',
	0,
	0,
	0,
	0 AS RECOMMENDED,
	GETDATE(),
	REPLACE(REPLACE(LEFT(RTRIM(LTRIM('$(targetCountryCode)' + '-' + CAST(a.CONTRATO AS VARCHAR))), 16),' ','_'),'-','_') AS SIGLA_PRIMARY_KEY,

	UPPER(
		REPLACE(
		REPLACE(
			LEFT( '$(targetCountryCode)' + '_' +
					RTRIM(LTRIM(CAST(a.CONTRATO AS VARCHAR))) +	'_' +
					RTRIM(LTRIM(a.OBRA))
				,30)
		,' ','_')
		,'-','_')
	) COLLATE sql_latin1_general_cp1251_ci_as AS PM_PROJECT_NAME,

	LEFT('$(targetCountryCode)' + '_' + CAST (a.CONTRATO AS VARCHAR(max)),20) AS TASK_NUMBER,
	LEFT('$(targetCountryCode)' + '_' + CAST (a.CONTRATO AS VARCHAR(max)),20) AS TASK_NAME,
	'' AS PARENT_TASK_NUMBER,
	LEFT('$(targetCountryCode)' + '_' + CAST (a.CONTRATO AS VARCHAR(max)),20) AS DESCRIPTION,
	ISNULL(a.FCHFIRMA, GETDATE()) AS START_DATE,
	ISNULL(a.FCHFIRMA, GETDATE()) AS FINISH_DATE,
	'SVC' AS SERVICE_TYPE,
	'Y' AS ALLOW_CHARGE,
	'' AS ATTRIBUTE1,
	'OTHER' AS ATTRIBUTE2,
	'*NULL*' as ATTRIBUTE3,
	' ' AS ATTRIBUTE4,
	GETDATE() AS Attribute5,
	'N' AS ATTRIBUTE6,
	'NONE' AS ATTRIBUTE7,
	'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME

	FROM $(sourceDatabaseName).dbo.MANTEN AS a
	WHERE a.CONTRATO = @contractCode
END;
GO


CREATE PROCEDURE dbo.buildServiceRetRulesRow_$(targetCountryCode)  
(  
    @itemCode VARCHAR(16),
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN 
	INSERT INTO $(targetDatabaseName).dbo.O_PRE_PROJECT_RET_RULES  WITH (TABLOCK)
	(
		COUNTRY,
		LOCAL_VALIDATED,
		CROSSREF_VALIDATED,
		CLEANSED,
		RECOMMENDED,
		LAST_UPDATED,

		PROJECT_NAME,
		RETENTION_PERCENTAGE,
		RETAINED_AMOUNT,
		ORG_NAME
	)
	SELECT
    TOP 1
		'$(targetCountryCode)',
		0,
		0,
		0,
		0 AS RECOMMENDED,
		GETDATE(),

		UPPER(
			REPLACE(
			REPLACE(
				LEFT( '$(targetCountryCode)' + '_' +
						RTRIM(LTRIM(CAST(a.CONTRATO AS VARCHAR))) +	'_' +
						RTRIM(LTRIM(a.OBRA))
					,30)
			,' ','_')
			,'-','_')
		) COLLATE sql_latin1_general_cp1251_ci_as AS PM_PROJECT_NAME,
		0,
		0,
		'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
	FROM $(sourceDatabaseName).dbo.MANTEN a
	WHERE a.CONTRATO = @itemCode;
END;
GO



CREATE PROCEDURE dbo.buildProjectRetRulesRow_$(targetCountryCode)  
(  
    @itemCode VARCHAR(16),
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN 
	INSERT INTO $(targetDatabaseName).dbo.O_PRE_PROJECT_RET_RULES  WITH (TABLOCK)
	(
		COUNTRY,
		LOCAL_VALIDATED,
		CROSSREF_VALIDATED,
		CLEANSED,
		RECOMMENDED,
		LAST_UPDATED,

		PROJECT_NAME,
		RETENTION_PERCENTAGE,
		RETAINED_AMOUNT,
		ORG_NAME
	)
	SELECT
    TOP 1
		'$(targetCountryCode)',
		0,
		0,
		0,
		0 AS RECOMMENDED,
		GETDATE(),

		UPPER(
			REPLACE(
			REPLACE(
				LEFT( '$(targetCountryCode)' + '_' +
						RTRIM(LTRIM(CAST(CONTRATO.CONTRATO AS VARCHAR))) +	'_' +
						RTRIM(LTRIM(CONTRATO.OBRA))
					,30)
			,' ','_')
			,'-','_')
		) COLLATE sql_latin1_general_cp1251_ci_as AS PM_PROJECT_NAME,
		0,
		0,
		'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
	FROM $(sourceDatabaseName).dbo.CONTRATO
	WHERE CONTRATO.CONTRATO = @itemCode;
END;
GO

CREATE PROCEDURE dbo.rebuildPreStageProjectIndex
AS
BEGIN
	UPDATE O_PRE_PROJECT_HEADER SET CLEANSED=0;
	UPDATE O_PRE_PROJECT_HEADER SET CLEANSED=1 WHERE SIGLA_PRIMARY_KEY IN (SELECT SIGLA_PRIMARY_KEY FROM O_STG_PROJECT_HEADER)
END

GO

CREATE PROCEDURE dbo.loadProjectsMasterData_$(targetCountryCode)
AS
BEGIN
	DELETE FROM $(targetDatabaseName).dbo.O_PRE_PROJECT_HEADER WHERE COUNTRY='$(targetCountryCode)';
	DELETE FROM $(targetDatabaseName).dbo.O_PRE_PROJECT_TASKS WHERE COUNTRY='$(targetCountryCode)';
	DELETE FROM $(targetDatabaseName).dbo.O_PRE_PROJECT_CLASSIFICATIONS WHERE COUNTRY='$(targetCountryCode)';
	DELETE FROM $(targetDatabaseName).dbo.O_PRE_PROJECT_RET_RULES WHERE COUNTRY='$(targetCountryCode)';

	DELETE FROM COM_AUDIT_LOGS WHERE EVENT LIKE '%Projects Master Data partial load%' AND COUNTRY='$(targetCountryCode)'

	DECLARE @unique_id VARCHAR(16)

	DECLARE @logThreshold int = 100
	DECLARE @count int = 0
	DECLARE @total int = 0

	-- selecionar o pmRef e o smRef do ConfigEstandar

	DECLARE PROJECTS_CURSOR CURSOR FOR   
	SELECT DISTINCT CONTRATO  
	FROM $(sourceDatabaseName).dbo.CONTRATO;

	SELECT @total = COUNT(DISTINCT CONTRATO)  
	FROM $(sourceDatabaseName).dbo.CONTRATO;

	OPEN PROJECTS_CURSOR;

	FETCH NEXT FROM PROJECTS_CURSOR   
	INTO @unique_id;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC $(targetDatabaseName).dbo.buildProjectHeaderRow_$(targetCountryCode) @unique_id   -- passar o pmRef eo smRef como parametros
		EXEC $(targetDatabaseName).dbo.buildProjectTasksRow_$(targetCountryCode) @unique_id, @count
		EXEC $(targetDatabaseName).dbo.buildProjectClassificationsRow_$(targetCountryCode) @unique_id
		EXEC $(targetDatabaseName).dbo.buildProjectRetRulesRow_$(targetCountryCode) @unique_id

		IF (@count%@logThreshold = 0)
		BEGIN
			PRINT CAST(@count AS VARCHAR(max))+'/'+CAST(@total AS VARCHAR(max));

			INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,SNAPSHOT_DATE)
			VALUES('$(targetCountryCode)', 'script', 'Projects Master Data partial load ' + CAST(@count AS VARCHAR(max))+'/'+CAST(@total AS VARCHAR(max)), GETDATE(), @count, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));
		END
		
		SET @count = @count + 1
		
		FETCH NEXT FROM PROJECTS_CURSOR   
		INTO @unique_id;
	END;

	DELETE FROM COM_AUDIT_LOGS WHERE EVENT LIKE '%Projects Master Data partial load%' AND COUNTRY='$(targetCountryCode)'

	INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,SNAPSHOT_DATE)
	VALUES('$(targetCountryCode)', 'script', 'Projects Master Data loaded from $(sourceDatabaseName) @ ' + dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'), GETDATE(), @count, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));

	DELETE FROM COM_AUDIT_LOGS WHERE EVENT LIKE '%Services Master Data partial load%' AND COUNTRY='$(targetCountryCode)'


	DECLARE SERVICE_CURSOR CURSOR FOR   
	SELECT DISTINCT A.CONTRATO FROM $(sourceDatabaseName).dbo.MANTEN AS A
	INNER JOIN $(sourceDatabaseName).dbo.CLIENTES AS B ON A.CLIENTE = B.CLIENTE
	INNER JOIN $(sourceDatabaseName).dbo.TITRENOVAC AS C ON A.CONTRATO=C.CONTRATO
	AND RENOVACION IN 
	(SELECT TOP 1 RENOVACION FROM $(sourceDatabaseName).dbo.TITRENOVAC WHERE CONTRATO=a.CONTRATO ORDER BY RENOVACION DESC)

	SELECT @total=COUNT(DISTINCT A.CONTRATO) FROM $(sourceDatabaseName).dbo.MANTEN AS A
	INNER JOIN $(sourceDatabaseName).dbo.CLIENTES AS B ON A.CLIENTE = B.CLIENTE
	INNER JOIN $(sourceDatabaseName).dbo.TITRENOVAC AS C ON A.CONTRATO=C.CONTRATO
	AND RENOVACION IN 
	(SELECT TOP 1 RENOVACION FROM $(sourceDatabaseName).dbo.TITRENOVAC WHERE CONTRATO=a.CONTRATO ORDER BY RENOVACION DESC)


	OPEN SERVICE_CURSOR;

	FETCH NEXT FROM SERVICE_CURSOR   
	INTO @unique_id;

	SET @count = 0

	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC $(targetDatabaseName).dbo.buildServiceHeaderRow_$(targetCountryCode) @unique_id  -- se vai fazer pra Projects, tem que fazer pra Services tamb�m
		EXEC $(targetDatabaseName).dbo.buildServiceTasksRow_$(targetCountryCode) @unique_id, @count
		EXEC $(targetDatabaseName).dbo.buildServiceClassificationsRow_$(targetCountryCode) @unique_id
		EXEC $(targetDatabaseName).dbo.buildServiceRetRulesRow_$(targetCountryCode) @unique_id
		
		IF (@count%@logThreshold = 0)
		BEGIN
			PRINT CAST(@count AS VARCHAR(max))+'/'+CAST(@total AS VARCHAR(max));

			INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,SNAPSHOT_DATE)
			VALUES('$(targetCountryCode)', 'script', 'Services Master Data partial load ' + CAST(@count AS VARCHAR(max))+'/'+CAST(@total AS VARCHAR(max)), GETDATE(), @count, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));
		END

		SET @count = @count + 1
		
		FETCH NEXT FROM SERVICE_CURSOR   
		INTO @unique_id;
	END;

	CLOSE PROJECTS_CURSOR;
	DEALLOCATE PROJECTS_CURSOR;

	CLOSE SERVICE_CURSOR;
	DEALLOCATE SERVICE_CURSOR;


	DELETE FROM COM_AUDIT_LOGS WHERE EVENT LIKE '%Services Master Data partial load%' AND COUNTRY='$(targetCountryCode)'

	EXEC $(targetDatabaseName).dbo.rebuildPreStageProjectIndex

	INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,SNAPSHOT_DATE)
	VALUES('$(targetCountryCode)', 'script', 'Services Master Data loaded from $(sourceDatabaseName) @ ' + dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'), GETDATE(), @count, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));



END
GO

IF OBJECT_ID('$(targetDatabaseName).dbo.addProjectRecommendationsFor_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.addProjectRecommendationsFor_$(targetCountryCode);

GO

CREATE PROCEDURE dbo.addProjectRecommendationsFor_$(targetCountryCode)
AS
BEGIN
	DECLARE @count int = 0

	UPDATE O_PRE_PROJECT_HEADER SET
	  RECOMMENDED=0
	WHERE COUNTRY='$(targetCountryCode)';

	SELECT @count=COUNT(*) FROM O_PRE_PROJECT_HEADER WHERE RECOMMENDED=1 AND TEMPLATE_NAME LIKE '%DM%' AND COUNTRY='$(targetCountryCode)'

	DELETE FROM COM_AUDIT_LOGS WHERE EVENT LIKE '%Projects Master Data partial recommendation%' AND COUNTRY='$(targetCountryCode)'

	INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,SNAPSHOT_DATE)
	VALUES('$(targetCountryCode)', 'script', 'Projects Master Data added recommendations from $(sourceDatabaseName) @ ' + dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'), GETDATE(), @count, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));
END
GO

IF OBJECT_ID('$(targetDatabaseName).dbo.addServiceRecommendationsFor_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.addServiceRecommendationsFor_$(targetCountryCode);

GO

CREATE PROCEDURE dbo.addServiceRecommendationsFor_$(targetCountryCode)
AS
BEGIN
	DECLARE @count int = 0

	UPDATE O_PRE_PROJECT_HEADER SET
	  RECOMMENDED=0
	WHERE COUNTRY='$(targetCountryCode)';

	SELECT @count=COUNT(*) FROM O_PRE_PROJECT_HEADER WHERE RECOMMENDED=1 AND TEMPLATE_NAME LIKE '%SERV TEMPLATE%' AND COUNTRY='$(targetCountryCode)'

	DELETE FROM COM_AUDIT_LOGS WHERE EVENT LIKE '%Services Master Data partial recommendation%' AND COUNTRY='$(targetCountryCode)'

	INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,SNAPSHOT_DATE)
	VALUES('$(targetCountryCode)', 'script', 'Services Master Data added recommendations from $(sourceDatabaseName) @ ' + dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'), GETDATE(), @count, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));
END
GO



INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,SNAPSHOT_DATE)
VALUES('$(targetCountryCode)', 'script', 'Projects Master Data load procedures redefined', GETDATE(), dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,SNAPSHOT_DATE)
VALUES('$(targetCountryCode)', 'script', 'Services Master Data load procedures redefined', GETDATE(), dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));
