IF OBJECT_ID('dbo.generateServiceClassificationsByCountry', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateServiceClassificationsByCountry;
GO

CREATE PROCEDURE dbo.generateServiceClassificationsByCountry
(
@country VARCHAR(2)
)
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRANSACTION

SELECT a.[PROJECT_CLASSIFICATIONS_ID] AS projectClassificationsID
,b.[PROJECT_NAME] AS projectName
,a.[CLASS_CATEGORY] AS classCategory
,a.[CLASS_CODE] AS classCode
,b.[ORG_NAME] AS orgName
FROM [dbo].[O_STG_PROJECT_CLASSIFICATIONS] a JOIN
[dbo].[O_STG_PROJECT_HEADER] b
ON (a.[PROJECT_NAME] = b.[PROJECT_NAME])
WHERE
b.[COUNTRY]=@country AND
b.[TEMPLATE_NAME] LIKE '%SERV TEMPLATE%' AND
b.DISMISSED=0 AND
b.[LOCAL_VALIDATED]=1 AND
b.[CROSSREF_VALIDATED]=1

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateServiceClassificationsByCountry procedure definition', GETDATE());