IF OBJECT_ID('dbo.udf_EncodeRepairName', 'FN') IS NOT NULL
	DROP FUNCTION dbo.udf_EncodeRepairName;
GO

CREATE FUNCTION dbo.udf_EncodeRepairName ( @country VARCHAR(2), @rcp VARCHAR(10), @obra VARCHAR(120))
RETURNS VARCHAR(30)
AS
BEGIN

DECLARE @projectName VARCHAR(30)

SET @projectName = LEFT(UPPER(@country+'_RCP_'+replace(replace(RTRIM(LTRIM(@rcp)+'_'+RTRIM(LTRIM(@obra))),'-','_'),' ','_')),30);

RETURN @projectName
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated udf_EncodeRepairName function definition', GETDATE());

