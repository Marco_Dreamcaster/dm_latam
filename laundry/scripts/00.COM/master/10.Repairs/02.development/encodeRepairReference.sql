IF OBJECT_ID('dbo.udf_EncodeRepairReference', 'FN') IS NOT NULL
	DROP FUNCTION dbo.udf_EncodeRepairReference;
GO

CREATE FUNCTION dbo.udf_EncodeRepairReference ( @country VARCHAR(2), @rcp VARCHAR(10) )
RETURNS VARCHAR(25)
AS
BEGIN

DECLARE @pmProjectReference VARCHAR(25)

SET @pmProjectReference = UPPER(@country+'_RCP_'+replace(replace(RTRIM(LTRIM(@rcp)),'-','_'),' ','_'));

RETURN @pmProjectReference
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated udf_EncodeRepairReference function definition', GETDATE());

