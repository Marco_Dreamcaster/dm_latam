IF OBJECT_ID('DBO.generateRCPContratadas_$(targetCountryCode)') IS NOT NULL DROP PROCEDURE DBO.generateRCPContratadas_$(targetCountryCode)
GO

CREATE PROCEDURE DBO.generateRCPContratadas_$(targetCountryCode)(@FechaCorte Date)
AS BEGIN
    SET NOCOUNT ON;
    BEGIN TRANSACTION generateRCPContratadas_$(targetCountryCode)_Trans

    DELETE FROM DBO.O_PRE_RAW_RCP WHERE COUNTRY = '$(targetCountryCode)'

    EXEC DBO.populateRCPContratadas_$(targetCountryCode) @FechaCorte

    SELECT
        IDRCP, TIPOCONT, RCP, FECHA, FCHSOLICITUD, FCHAPROBAC,
        FCHAPROBACCLI,
        NOMBRE, MONEDA, IMPORTE, VALVTA, CONDVENTA, TIEESTIMADO, MARGEM,
        IDTECNICO, CLIENTE, EDIFICIO, VENDEDOR, USRAPROBAC,
        IMPORTENAC, DESCRIP, NOMTECNICO, MATERIALES, HORASTECNICO, IMPHORAS, USUARIO, USUARIOAPR,
        CONDPAGO, SUPERVISOR, VALVTALOCAL, MATERIALESLOCAL, IMPHORASLOCAL, OBRA, SIGLATOTAL, COUNTRY
    FROM DBO.O_PRE_RAW_RCP
    WHERE COUNTRY = '$(targetCountryCode)'

	IF @@ERROR != 0 ROLLBACK TRANSACTION generateRCPContratadas_$(targetCountryCode)_Trans
    ELSE COMMIT TRANSACTION generateRCPContratadas_$(targetCountryCode)_Trans

    RETURN
END
GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('$(targetCountryCode)', 'script', 'updated generateRawRCP_$(targetCountryCode) procedure definition', GETDATE());