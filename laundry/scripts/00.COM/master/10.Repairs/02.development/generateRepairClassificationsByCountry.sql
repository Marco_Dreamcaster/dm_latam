IF OBJECT_ID('dbo.generateRepairClassificationsByCountry', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateRepairClassificationsByCountry;

GO

CREATE PROCEDURE dbo.generateRepairClassificationsByCountry
(
@country VARCHAR(2)
)
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRANSACTION

INSERT INTO
dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT,STAMP,SNAPSHOT_DATE)
VALUES(@country, 'script', 'Repairs Master Data flatfile generated - Classifications',GETDATE(), dbo.udf_EncodeSnapshotDate(@country));

SELECT a.[PROJECT_CLASSIFICATIONS_ID] AS projectClassificationsID
,b.[PROJECT_NAME] AS projectName
,a.[CLASS_CATEGORY] AS classCategory
,a.[CLASS_CODE] AS classCode
,b.[ORG_NAME] AS orgName
FROM [dbo].[O_STG_PROJECT_CLASSIFICATIONS] a JOIN
[dbo].[O_STG_PROJECT_HEADER] b
ON (a.[PROJECT_NAME] = b.[PROJECT_NAME])
WHERE
b.[COUNTRY]=@country AND
b.[TEMPLATE_NAME] LIKE '%QR%' AND
b.[DISMISSED]=0 AND
b.[LOCAL_VALIDATED]=1 AND
b.[CROSSREF_VALIDATED]=1

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateProjectClassificationsByCountry procedure definition', GETDATE());