IF OBJECT_ID('dbo.generateRepairRetRulesByCountry', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateRepairRetRulesByCountry;

GO

CREATE PROCEDURE dbo.generateRepairRetRulesByCountry
(
@country VARCHAR(2)
)
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRANSACTION

INSERT INTO
dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT,STAMP,SNAPSHOT_DATE)
VALUES(@country, 'script', 'Repairs Master Data flatfile generated - RetRules',GETDATE(), dbo.udf_EncodeSnapshotDate(@country));

SELECT b.[PROJECT_NAME]  AS projectName
,a.[RETENTION_PERCENTAGE] AS retentionPercentage
,a.[RETAINED_AMOUNT] AS retainedAmount
,b.[ORG_NAME] AS orgName
FROM [dbo].[O_STG_PROJECT_RET_RULES] a JOIN
[dbo].[O_STG_PROJECT_HEADER] b
ON (a.[PROJECT_NAME] = b.[PROJECT_NAME])
WHERE
b.[COUNTRY]=@country AND
b.[TEMPLATE_NAME] LIKE '%QR%' AND
b.[DISMISSED]=0 AND
b.[LOCAL_VALIDATED]=1 AND
b.[CROSSREF_VALIDATED]=1

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateRepairRetRulesByCountry procedure definition', GETDATE());