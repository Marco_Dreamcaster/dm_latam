IF OBJECT_ID('dbo.generateRepairTasksByCountry', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateRepairTasksByCountry;

GO

CREATE PROCEDURE dbo.generateRepairTasksByCountry
(
@country VARCHAR(2)
)
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRANSACTION

INSERT INTO
dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT,STAMP,SNAPSHOT_DATE)
VALUES(@country, 'script', 'Repairs Master Data flatfile generated - Tasks',GETDATE(), dbo.udf_EncodeSnapshotDate(@country));

MERGE INTO O_STG_PROJECT_TASKS T
   USING O_PRE_PROJECT_TASKS S 
      ON T.PROJECT_NAME LIKE '%'+S.SIGLA_PRIMARY_KEY+'%'
         AND S.TASK_NUMBER = T.TASK_NUMBER
         AND S.COUNTRY = @country
WHEN MATCHED THEN
   UPDATE 
      SET SIGLA_PRIMARY_KEY = S.SIGLA_PRIMARY_KEY;

SELECT b.[PROJECT_NAME]  AS projectName
,a.[TASK_NUMBER] AS taskNumber
,a.[TASK_NAME] AS taskName
,a.[PARENT_TASK_NUMBER] AS parentTaskNumber
,a.[DESCRIPTION] AS description
,a.[START_DATE] AS startDate
,a.[FINISH_DATE] AS finishDate
,a.[SERVICE_TYPE] AS serviceType
,a.[ALLOW_CHARGE] AS allowCharge
,a.[ATTRIBUTE1] AS attribute1
,a.[ATTRIBUTE2] AS attribute2
,a.[ATTRIBUTE3] AS attribute3
,a.[ATTRIBUTE4] AS attribute4
,a.[ATTRIBUTE5] AS attribute5
,a.[ATTRIBUTE6] AS attribute6
,a.[ATTRIBUTE7] AS attribute7
,a.[ATTRIBUTE8] AS attribute8
,a.[NO_OF_FLOORS] AS noOfFloors
,a.[SPEED] AS speed
,a.[CAPACITY] as capacity
,b.[ORG_NAME] AS orgName
FROM [dbo].[O_STG_PROJECT_TASKS] a JOIN
[dbo].[O_STG_PROJECT_HEADER] b
ON (a.SIGLA_PRIMARY_KEY = b.SIGLA_PRIMARY_KEY)
WHERE
b.[COUNTRY]=@country AND
b.[TEMPLATE_NAME] LIKE '%QR%' AND
b.[DISMISSED]=0 AND
b.[LOCAL_VALIDATED]=1 AND
b.[CROSSREF_VALIDATED]=1

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateRepairTasksByCountry procedure definition', GETDATE());