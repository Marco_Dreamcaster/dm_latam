IF OBJECT_ID('DBO.fixRCPContratadas_CO') IS NOT NULL DROP PROCEDURE DBO.fixRCPContratadas_CO
GO

CREATE PROCEDURE DBO.fixRCPContratadas_CO(@FechaCorte Date, @Country nVarchar(2))
AS BEGIN

    SET NOCOUNT ON;
    BEGIN TRANSACTION fixRCPContratadas_CO_Trans

    UPDATE DBO.O_PRE_RAW_RCP SET
        DBO.O_PRE_RAW_RCP.USUARIO = TKCO_LATEST.DBO.USUARIOS.NOMBRE
    FROM TKCO_LATEST.DBO.USUARIOS
    INNER JOIN DBO.O_PRE_RAW_RCP ON (DBO.O_PRE_RAW_RCP.VENDEDOR = TKCO_LATEST.DBO.USUARIOS.USUARIO)
    WHERE DBO.O_PRE_RAW_RCP.COUNTRY = @Country

    UPDATE DBO.O_PRE_RAW_RCP SET
        DBO.O_PRE_RAW_RCP.CONDPAGO = TKCO_LATEST.DBO.CUOTASRCP.DESCRIP
    FROM TKCO_LATEST.DBO.CUOTASRCP
    INNER JOIN DBO.O_PRE_RAW_RCP ON (DBO.O_PRE_RAW_RCP.IDRCP = TKCO_LATEST.DBO.CUOTASRCP.IDRCP)
    WHERE DBO.O_PRE_RAW_RCP.COUNTRY = @Country

    UPDATE DBO.O_PRE_RAW_RCP SET
        DBO.O_PRE_RAW_RCP.SUPERVISOR = C.NOMBRE
    FROM DBO.O_PRE_RAW_RCP T
    INNER JOIN (
        SELECT A.IDTECNICO, B.IDSUPERVISOR, D.NOMBRE
        FROM  TKCO_LATEST.DBO.RUTAS A
        INNER JOIN TKCO_LATEST.DBO.GRUPOAVERIAS B ON (A.IDGRUPO = B.IDGRUPO)
        INNER JOIN TKCO_LATEST.DBO.TECNICOS D ON (B.IDSUPERVISOR = D.IDTECNICO)
        ) C
    ON (T.IDTECNICO = C.IDTECNICO)
    WHERE T.COUNTRY = @Country

    UPDATE DBO.O_PRE_RAW_RCP SET
        DBO.O_PRE_RAW_RCP.DESCRIP = TKCO_LATEST.DBO.DETRCP.CONCEPTO
    FROM TKCO_LATEST.DBO. DETRCP
    INNER JOIN DBO.O_PRE_RAW_RCP ON (DBO.O_PRE_RAW_RCP.IDRCP = TKCO_LATEST.DBO.DETRCP.IDRCP)
    WHERE DBO.O_PRE_RAW_RCP.COUNTRY = @Country

    UPDATE DBO.O_PRE_RAW_RCP SET
        DBO.O_PRE_RAW_RCP.NOMTECNICO = TKCO_LATEST.DBO.TECNICOS.NOMBRE
    FROM TKCO_LATEST.DBO.TECNICOS
    INNER JOIN DBO.O_PRE_RAW_RCP ON (DBO.O_PRE_RAW_RCP.IDTECNICO = TKCO_LATEST.DBO.TECNICOS.IDTECNICO)
    WHERE DBO.O_PRE_RAW_RCP.COUNTRY = @Country

    UPDATE DBO.O_PRE_RAW_RCP SET
        DBO.O_PRE_RAW_RCP.MATERIALES = (
            SELECT SUM(TKCO_LATEST.DBO.DETRCP.IMPORTE)
            FROM TKCO_LATEST.DBO.DETRCP
            WHERE TKCO_LATEST.DBO.DETRCP.IDRCP = DBO.O_PRE_RAW_RCP.IDRCP
        )
    WHERE DBO.O_PRE_RAW_RCP.COUNTRY = @Country

    UPDATE DBO.O_PRE_RAW_RCP SET
        DBO.O_PRE_RAW_RCP.HORASTECNICO = (
            SELECT SUM(TKCO_LATEST.DBO.HORASRCP.CANTIDAD)
            FROM TKCO_LATEST.DBO.HORASRCP
            WHERE TKCO_LATEST.DBO.HORASRCP.IDRCP = DBO.O_PRE_RAW_RCP.IDRCP
        )
    WHERE DBO.O_PRE_RAW_RCP.COUNTRY = @Country

    UPDATE DBO.O_PRE_RAW_RCP SET
        DBO.O_PRE_RAW_RCP.IMPHORAS = (
            SELECT SUM(TKCO_LATEST.DBO.HORASRCP.IMPORTE)
            FROM TKCO_LATEST.DBO.HORASRCP
            WHERE TKCO_LATEST.DBO.HORASRCP.IDRCP = DBO.O_PRE_RAW_RCP.IDRCP
        )
    WHERE DBO.O_PRE_RAW_RCP.COUNTRY = @Country

    UPDATE DBO.O_PRE_RAW_RCP SET
        DBO.O_PRE_RAW_RCP.MATERIALES = 0
    WHERE DBO.O_PRE_RAW_RCP.MATERIALES IS NULL
    AND DBO.O_PRE_RAW_RCP.COUNTRY = @Country

    UPDATE DBO.O_PRE_RAW_RCP SET
        DBO.O_PRE_RAW_RCP.HORASTECNICO = 0
    WHERE DBO.O_PRE_RAW_RCP.HORASTECNICO IS NULL
    AND DBO.O_PRE_RAW_RCP.COUNTRY = @Country

    UPDATE DBO.O_PRE_RAW_RCP SET
        DBO.O_PRE_RAW_RCP.IMPHORAS = 0
    WHERE DBO.O_PRE_RAW_RCP.IMPHORAS IS NULL
    AND DBO.O_PRE_RAW_RCP.COUNTRY = @Country

    UPDATE DBO.O_PRE_RAW_RCP SET
        IMPORTENAC = IMPORTE * TKCO_LATEST.DBO.CAMBIOS.VENTA,
        VALVTALOCAL = VALVTA * TKCO_LATEST.DBO.CAMBIOS.VENTA,
        MATERIALESLOCAL = MATERIALES * TKCO_LATEST.DBO.CAMBIOS.VENTA,
        IMPHORASLOCAL = IMPHORAS * TKCO_LATEST.DBO.CAMBIOS.VENTA
    FROM DBO.O_PRE_RAW_RCP
    INNER JOIN TKCO_LATEST.DBO.CAMBIOS ON (TKCO_LATEST.DBO.CAMBIOS.MONEDA = DBO.O_PRE_RAW_RCP.MONEDA)
    WHERE CAMBIOS.FECHA = Cast(CONVERT(NVARCHAR, @FechaCorte, 103) AS DATE)
    AND DBO.O_PRE_RAW_RCP.MONEDA <> 'P'
    AND DBO.O_PRE_RAW_RCP.COUNTRY = @Country

    UPDATE DBO.O_PRE_RAW_RCP SET
        DESCRIP = ''
    WHERE DESCRIP IS NULL
    AND DBO.O_PRE_RAW_RCP.COUNTRY = @Country

    UPDATE DBO.O_PRE_RAW_RCP SET
        NOMTECNICO = ''
    WHERE NOMTECNICO IS NULL
    AND DBO.O_PRE_RAW_RCP.COUNTRY = @Country

    IF @@ERROR != 0 ROLLBACK TRANSACTION fixRCPContratadas_CO_Trans
    ELSE COMMIT TRANSACTION fixRCPContratadas_CO_Trans
END
GO
