-- this script is never to be used directly, instead it should be included into the target country folder
-- @see 02.COL\master\10.Repairs\load_RepairsData.sql

IF OBJECT_ID('$(targetDatabase).dbo.buildRepairHeaderRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildRepairHeaderRow_$(targetCountryCode);
GO

CREATE PROCEDURE dbo.buildRepairHeaderRow_$(targetCountryCode)  
(  
    @rcpCode VARCHAR(10),
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN 
	INSERT INTO $(targetDatabase).dbo.O_PRE_PROJECT_HEADER  WITH (TABLOCK)
	(
		COUNTRY,
		LOCAL_VALIDATED,
		CROSSREF_VALIDATED,
		CLEANSED,
		RECOMMENDED,
		LAST_UPDATED,
		SIGLA_PRIMARY_KEY,

		PM_PROJECT_REFERENCE,
		PROJECT_NAME,
		PROJECT_TYPE,
		PROJECT_STATUS,
		TEMPLATE_NAME,
		START_DATE,
		COMPLETION_DATE,
		DESCRIPTION,
		ORGANIZATION_CODE,
		CUSTOMER_NUMBER,
		BILL_TO_ADDRESS_REF,
		SHIP_TO_ADDRESS_REF,
		PROJECT_MANAGER_REF,
		SERVICE_MANAGER_REF,
		SALES_REPRESENTATIVE_REF,
		DUMMY_KEY_MEMBER_REF,
		ORG_NAME
	)
	SELECT
		TOP 1
		'$(targetCountryCode)',
		0,
		0,
		0,
		0 AS RECOMMENDED,
		GETDATE(),
		dbo.udf_EncodeRepairReference('$(targetCountryCode)',@rcpCode) AS SIGLA_PRIMARY_KEY,
		dbo.udf_EncodeRepairReference('$(targetCountryCode)',@rcpCode) AS PM_PROJECT_REFERENCE,
		dbo.udf_EncodeRepairName('$(targetCountryCode)',@rcpCode,a.OBRA) AS PROJECT_NAME,
		'QR' AS PROJECT_TYPE, -- CC, POC (is it a percentage? e.g. 80%)
		'Approved' AS PROJECT_STATUS, -- Approved, Closed, Completed (CC), Completed (POC)
		'QR - $(targetCountryCode)'  AS TEMPLATE_NAME,
		ISNULL(d.FCHFIRMA, GETDATE()) AS START_DATE,
		dateadd(year,1,ISNULL(d.FCHFIRMA, GETDATE())) AS COMPLETION_DATE, 
		LEFT(a.OBRA, 250) AS DESCRIPTON,
		'' AS ORGANIZATION_CODE,
		dbo.udf_EncodeCustomerReference('$(targetCountryCode)', B.CLIENTE) AS CUSTOM_NUMBER,
		dbo.udf_EncodeCustomerAddressReference('$(targetCountryCode)', B.CLIENTE) AS BILL_TO_ADDRESS_REF,
		dbo.udf_EncodeCustomerAddressReference('$(targetCountryCode)', B.CLIENTE) AS SHIP_TO_ADDRESS_REF,
		'$(pmRef)' AS PROJECT_MANAGER_REF,
		'$(smRef)' AS SERVICE_MANAGER_REF,
		a.VENDEDOR AS SALES_REPRESENTATIVE_REF,
		'$(dummyRef)' AS DUMMY_KEY_MEMBER_REF,
		'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
  FROM $(sourceDatabase)..CONTRCP a
    INNER JOIN $(sourceDatabase).dbo.CLIENTES b ON (b.CLIENTE=a.CLIENTE)
    INNER JOIN $(sourceDatabase).dbo.MONEDAS c ON (c.MONEDA=a.MONEDA)
    LEFT JOIN $(sourceDatabase).dbo.MANTEN d ON (d.CONTRATO=a.CONTRATO)
  WHERE a.RCP = @rcpCode;
END;
GO

IF OBJECT_ID('$(targetDatabase).dbo.buildProjectClassificationsRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildProjectClassificationsRow_$(targetCountryCode);
GO

CREATE PROCEDURE dbo.buildProjectClassificationsRow_$(targetCountryCode) 
(  
    @rcpCode VARCHAR(16),
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN
	INSERT INTO $(targetDatabase).dbo.O_PRE_PROJECT_CLASSIFICATIONS  WITH (TABLOCK)
	(
		COUNTRY,
		LOCAL_VALIDATED,
		CROSSREF_VALIDATED,
		CLEANSED,
		RECOMMENDED,
		LAST_UPDATED,

		PROJECT_CLASSIFICATIONS_ID,
		PROJECT_NAME,
		CLASS_CATEGORY,
		CLASS_CODE,
		ORG_NAME
	)
	SELECT
	  TOP 1
		'$(targetCountryCode)',
		0,
		0,
		0,
		0 AS RECOMMENDED,
		GETDATE(),
		dbo.udf_EncodeRepairReference('$(targetCountryCode)',@rcpCode)+'.1' COLLATE sql_latin1_general_cp1251_ci_as AS PROJECT_CLASSIFICATIONS_ID,
		dbo.udf_EncodeRepairName('$(targetCountryCode)',@rcpCode,a.OBRA) AS PROJECT_NAME,
		'BUILDING TYPE',
		'4+ STAR HOTEL' AS CLASS_CODE,
		'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
	  FROM $(sourceDatabase)..CONTRCP a
		INNER JOIN $(sourceDatabase).dbo.CLIENTES b ON (b.CLIENTE=a.CLIENTE)
		INNER JOIN $(sourceDatabase).dbo.MONEDAS c ON (c.MONEDA=a.MONEDA)
		LEFT JOIN $(sourceDatabase).dbo.MANTEN d ON (d.CONTRATO=a.CONTRATO)
	  WHERE a.RCP = @rcpCode;

	INSERT INTO $(targetDatabase).dbo.O_PRE_PROJECT_CLASSIFICATIONS  WITH (TABLOCK)
	(
		COUNTRY,
		LOCAL_VALIDATED,
		CROSSREF_VALIDATED,
		CLEANSED,
		RECOMMENDED,
		LAST_UPDATED,

		PROJECT_CLASSIFICATIONS_ID,
		PROJECT_NAME,
		CLASS_CATEGORY,
		CLASS_CODE,
		ORG_NAME
	)
	SELECT
	  TOP 1
		'$(targetCountryCode)',
		0,
		0,
		0,
		0 AS RECOMMENDED,
		GETDATE(),
		dbo.udf_EncodeRepairReference('$(targetCountryCode)',@rcpCode)+'.2' COLLATE sql_latin1_general_cp1251_ci_as AS PROJECT_CLASSIFICATIONS_ID,
		dbo.udf_EncodeRepairName('$(targetCountryCode)',@rcpCode,a.OBRA) AS PROJECT_NAME,
		'BUILDING CLASSIFICATION',
		'AIRPORT' AS CLASS_CODE,
		'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
	  FROM $(sourceDatabase)..CONTRCP a
		INNER JOIN $(sourceDatabase).dbo.CLIENTES b ON (b.CLIENTE=a.CLIENTE)
		INNER JOIN $(sourceDatabase).dbo.MONEDAS c ON (c.MONEDA=a.MONEDA)
		LEFT JOIN $(sourceDatabase).dbo.MANTEN d ON (d.CONTRATO=a.CONTRATO)
	  WHERE a.RCP = @rcpCode;
END;
GO

IF OBJECT_ID('$(targetDatabase).dbo.buildRepairTasksRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildRepairTasksRow_$(targetCountryCode);
GO


CREATE PROCEDURE dbo.buildRepairTasksRow_$(targetCountryCode)
(  
    @rcpCode VARCHAR(16),
    @increment int
)  
AS  
BEGIN 
	INSERT INTO $(targetDatabase).dbo.O_PRE_PROJECT_TASKS  WITH (TABLOCK)
	(
	COUNTRY,
	LOCAL_VALIDATED,
	CROSSREF_VALIDATED,
	CLEANSED,
	RECOMMENDED,
	LAST_UPDATED,
	SIGLA_PRIMARY_KEY,

	PROJECT_NAME,
	TASK_NUMBER,
	TASK_NAME,
	PARENT_TASK_NUMBER,
	DESCRIPTION,
	START_DATE,
	FINISH_DATE,
	SERVICE_TYPE,
	ALLOW_CHARGE,
	ATTRIBUTE1,
	ATTRIBUTE2,
	ATTRIBUTE3,
	ATTRIBUTE4,
	ATTRIBUTE5,
	ATTRIBUTE6,
	ATTRIBUTE7,
	ORG_NAME
	)
	SELECT TOP 1
	'$(targetCountryCode)',
	0,
	0,
	0,
	0 AS RECOMMENDED,
	GETDATE(),
	dbo.udf_EncodeRepairReference('$(targetCountryCode)',@rcpCode) AS SIGLA_PRIMARY_KEY,
	dbo.udf_EncodeRepairName('$(targetCountryCode)',@rcpCode,a.OBRA) AS PROJECT_NAME,

	dbo.udf_EncodeProjectTask('$(targetCountryCode)',@rcpCode)  AS TASK_NUMBER,
	dbo.udf_EncodeProjectTask('$(targetCountryCode)',@rcpCode) AS TASK_NAME,
	'' AS PARENT_TASK_NUMBER,
	LEFT(a.OBRA,250) AS DESCRIPTION,
	ISNULL(d.FCHFIRMA, GETDATE()) AS START_DATE,
	ISNULL(dateadd(year,1,d.FCHFIRMA), dateadd(year,1,GETDATE())) AS FINISH_DATE, 
	'QR' AS SERVICE_TYPE,
	'Y' AS ALLOW_CHARGE,
	'' AS ATTRIBUTE1,
	'OTHER' AS ATTRIBUTE2,
	'*NULL*' as ATTRIBUTE3,
	' ' AS ATTRIBUTE4,
	GETDATE() AS Attribute5,
	'N' AS ATTRIBUTE6,
	'NONE' AS ATTRIBUTE7,
	'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME

	FROM $(sourceDatabase)..CONTRCP a
		INNER JOIN $(sourceDatabase).dbo.CLIENTES b ON (b.CLIENTE=a.CLIENTE)
		INNER JOIN $(sourceDatabase).dbo.MONEDAS c ON (c.MONEDA=a.MONEDA)
		LEFT JOIN $(sourceDatabase).dbo.MANTEN d ON (d.CONTRATO=a.CONTRATO)
	WHERE a.RCP = @rcpCode;
END;
GO

IF OBJECT_ID('$(targetDatabase).dbo.buildRepairRetRulesRow_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.buildRepairRetRulesRow_$(targetCountryCode);
GO

CREATE PROCEDURE dbo.buildRepairRetRulesRow_$(targetCountryCode)  
(  
    @rcpCode VARCHAR(16),
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN 
	INSERT INTO $(targetDatabase).dbo.O_PRE_PROJECT_RET_RULES  WITH (TABLOCK)
	(
		COUNTRY,
		LOCAL_VALIDATED,
		CROSSREF_VALIDATED,
		CLEANSED,
		RECOMMENDED,
		LAST_UPDATED,

		PROJECT_NAME,
		RETENTION_PERCENTAGE,
		RETAINED_AMOUNT,
		ORG_NAME
	)
	SELECT
    TOP 1
		'$(targetCountryCode)',
		0,
		0,
		0,
		0 AS RECOMMENDED,
		GETDATE(),
		dbo.udf_EncodeRepairName('$(targetCountryCode)',@rcpCode,a.OBRA) AS PROJECT_NAME,
		0,
		0,
		'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
	FROM $(sourceDatabase)..CONTRCP a
		INNER JOIN $(sourceDatabase).dbo.CLIENTES b ON (b.CLIENTE=a.CLIENTE)
		INNER JOIN $(sourceDatabase).dbo.MONEDAS c ON (c.MONEDA=a.MONEDA)
		LEFT JOIN $(sourceDatabase).dbo.MANTEN d ON (d.CONTRATO=a.CONTRATO)
	WHERE a.RCP = @rcpCode;
END;
GO


IF OBJECT_ID('$(targetDatabase).dbo.loadRepairsMasterData_$(targetCountryCode)', 'P') IS NOT NULL
  DROP PROCEDURE dbo.loadRepairsMasterData_$(targetCountryCode);
GO

CREATE PROCEDURE dbo.loadRepairsMasterData_$(targetCountryCode)(@cutoffDate Date)
AS
BEGIN
	DECLARE @unique_id VARCHAR(16)
	DECLARE @logThreshold int = 100
	DECLARE @count int = 0
	DECLARE @total int = 0

	DELETE FROM $(targetDatabase).dbo.O_PRE_PROJECT_RET_RULES WHERE PROJECT_NAME IN
	(SELECT PROJECT_NAME FROM $(targetDatabase).dbo.O_PRE_PROJECT_HEADER WHERE COUNTRY='$(targetCountryCode)' AND TEMPLATE_NAME LIKE '%CC QR TEMPLATE%')

	DELETE FROM $(targetDatabase).dbo.O_PRE_PROJECT_TASKS WHERE PROJECT_NAME IN
	(SELECT PROJECT_NAME FROM $(targetDatabase).dbo.O_PRE_PROJECT_HEADER WHERE COUNTRY='$(targetCountryCode)' AND TEMPLATE_NAME LIKE '%CC QR TEMPLATE%')

	DELETE FROM $(targetDatabase).dbo.O_PRE_PROJECT_CLASSIFICATIONS WHERE PROJECT_NAME IN
	(SELECT PROJECT_NAME FROM $(targetDatabase).dbo.O_PRE_PROJECT_HEADER WHERE COUNTRY='$(targetCountryCode)' AND TEMPLATE_NAME LIKE '%CC QR TEMPLATE%')

	DELETE FROM $(targetDatabase).dbo.O_PRE_PROJECT_HEADER WHERE COUNTRY='$(targetCountryCode)' AND TEMPLATE_NAME LIKE '%CC QR TEMPLATE%';

	EXEC dbo.generateRCPContratadas_$(targetCountryCode) @cutoffDate

	-- RCP is the uniqueID in this case
	DECLARE REPAIRS_CURSOR CURSOR FOR
	SELECT DISTINCT b.RCP AS UNIQUE_ID FROM $(sourceDatabase)..CONTRCP b
	WHERE b.RCP IN (SELECT a.RCP FROM O_PRE_RAW_RCP a WHERE COUNTRY='$(targetCountryCode)')

	SELECT @total=COUNT(DISTINCT b.RCP) FROM $(sourceDatabase)..CONTRCP b
	WHERE b.RCP IN (SELECT a.RCP FROM O_PRE_RAW_RCP a WHERE COUNTRY='$(targetCountryCode)')

	OPEN REPAIRS_CURSOR;

	FETCH NEXT FROM REPAIRS_CURSOR   
	INTO @unique_id;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC dbo.buildRepairHeaderRow_$(targetCountryCode) @unique_id, '';
		EXEC dbo.buildProjectClassificationsRow_$(targetCountryCode) @unique_id, '';
		EXEC dbo.buildRepairTasksRow_$(targetCountryCode) @unique_id, '';
		EXEC dbo.buildRepairRetRulesRow_$(targetCountryCode) @unique_id, '';

		IF (@count%@logThreshold = 0)
		BEGIN
			PRINT CAST(@count AS VARCHAR(max))+'/'+CAST(@total AS VARCHAR(max));

			INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,SNAPSHOT_DATE)
			VALUES('$(targetCountryCode)', 'script', 'Repairs Master Data partial load ' + CAST(@count AS VARCHAR(max))+'/'+CAST(@total AS VARCHAR(max)), GETDATE(), @count, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));
		END
		
		SET @count = @count + 1
		
		FETCH NEXT FROM REPAIRS_CURSOR   
		INTO @unique_id;
	END;

	EXEC $(targetDatabase).dbo.rebuildPreStageProjectIndex

	INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,CUTOFF_DATE,SNAPSHOT_DATE)
	VALUES('$(targetCountryCode)', 'script', 'Repairs Master Data loaded from $(sourceDatabase) @ ' + dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'), GETDATE(), @count, @cutoffDate, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));

	CLOSE REPAIRS_CURSOR;
	DEALLOCATE REPAIRS_CURSOR;

END;

GO

INSERT INTO
dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT,STAMP,SNAPSHOT_DATE)
VALUES('$(targetCountryCode)', 'script', 'Repairs Master Data loading procedures updated @ ' + dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'),
		GETDATE(), dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));

