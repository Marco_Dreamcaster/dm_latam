IF OBJECT_ID('dbo.udf_EncodeOrgName', 'FN') IS NOT NULL
	DROP FUNCTION dbo.udf_EncodeOrgName;
GO

CREATE FUNCTION dbo.udf_EncodeOrgName ( @country VARCHAR(2) )
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE @organizationName VARCHAR(MAX)

	IF ( @country = 'UY')
		SET @organizationName = 'UY - THYSSENKRUPP ELEVADORES S.R.L.'

	ELSE IF ( @country = 'PY')
		SET @organizationName = 'PY - THYSSENKRUPP ELEVADORES S.R.L.'

	ELSE IF ( @country = 'PE')
		SET @organizationName = 'PE - THYSSENKRUPP ELEVADORES S.A.C.'

	ELSE
		SET @organizationName = @country + ' - THYSSENKRUPP ELEVADORES S.A.'



	RETURN 	@organizationName;

END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated udf_EncodeOrgName function definition', GETDATE());

