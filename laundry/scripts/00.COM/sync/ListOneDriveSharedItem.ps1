<#
	The sample scripts are not supported under any Microsoft standard support 
	program or service. The sample scripts are provided AS IS without warranty  
	of any kind. Microsoft further disclaims all implied warranties including,  
	without limitation, any implied warranties of merchantability or of fitness for 
	a particular purpose. The entire risk arising out of the use or performance of  
	the sample scripts and documentation remains with you. In no event shall 
	Microsoft, its authors, or anyone Else involved in the creation, production, or 
	delivery of the scripts be liable for any damages whatsoever (including, 
	without limitation, damages for loss of business profits, business interruption, 
	loss of business information, or other pecuniary loss) arising out of the use 
	of or inability to use the sample scripts or documentation, even If Microsoft 
	has been advised of the possibility of such damages 
#>

$ClientId = "cebf80b7-a90f-46e0-b12d-65207554ee71" # your application clientid
$SecretKey = "anUOLQ625$_gebdrBZA81{?" # the secrect key for your application
$RedirectURI = "https://localhost" # the re-direct url of your local IIS website application (https is required, use a hosts file entry alias as localhost may not work)

Function List-SharedItem
{
	[CmdletBinding()]
	Param
	(
		[Parameter(Mandatory=$true)][String]$ClientId,
		[Parameter(Mandatory=$true)][String]$SecretKey,
		[Parameter(Mandatory=$true)][String]$RedirectURI
	)

	# import the utils module
	Import-Module ".\OneDriveAuthentication.psm1"

	# get token
	$Token = New-AccessTokenAndRefreshToken -ClientId $ClientId -RedirectURI $RedirectURI -SecretKey $SecretKey

	# you can store the token somewhere for the later usage, however the token will expired
	# if the token is expired, please call Update-AccessTokenAndRefreshToken to update token
	# e.g.
	# $RefreshedToken = Update-AccessTokenAndRefreshToken -ClientId $ClientId -RedirectURI $RedirectURI -RefreshToken $Token.RefreshToken -SecretKey $SecretKey
	
	# construct authentication header
	$Header = Get-AuthenticateHeader -AccessToken $Token.AccessToken

	# api root
	$ApiRootUrl = "https://api.onedrive.com/v1.0"

	# call api
	$Response = Invoke-RestMethod -Headers $Header -Method GET -Uri "$ApiRootUrl/drive/shared"

	RETURN $Response.value
}

# call method to do job
$Results = List-SharedItem -ClientId $ClientId -SecretKey $SecretKey -RedirectURI $RedirectURI

# print results
$Results | ForEach-Object {
	Write-Host "ID: $($_.id)"
	Write-Host "Name: $($_.name)"
	Write-Host "ParentReference: $($_.parentReference)"
	Write-Host "Size: $($_.size)"
	Write-Host "WebURL: $($_.webUrl)"
	Write-Host
}
