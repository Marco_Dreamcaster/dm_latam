﻿param (
    [string]$country = "NI"
)

Write-Host $country

$response = Invoke-WebRequest -Uri "http://10.148.1.44/laundry/RefreshFF/InventoryDiagnostics/$($country)"

if (!$response) {
    Write-Host "Inventory Diagnostics response is null" 
}

$response = Invoke-WebRequest -Uri "http://10.148.1.44/laundry/RefreshFF/LoadAP/$($country)"

if (!$response) {
    Write-Host "Load AP response is null"
}
