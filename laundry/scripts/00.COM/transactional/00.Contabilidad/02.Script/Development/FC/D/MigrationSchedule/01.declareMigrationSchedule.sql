IF OBJECT_ID('DBO.MS') IS NOT NULL
    DROP TABLE DBO.MS
GO

CREATE TABLE DBO.MS (
    [ID] NUMERIC(5) NOT NULL IDENTITY,
    COUNTRY CHAR(2) NOT NULL,
    ORDERMIGRATION NUMERIC(2) NOT NULL,
    PROJECT CHAR(1) NOT NULL,
    SERVICE CHAR(1) NOT NULL,
    REPAIR CHAR(1) NOT NULL,
    CUSTOMER CHAR(1) NOT NULL,
    VENDOR CHAR(1) NOT NULL,
    INVENTORY CHAR(1) NOT NULL,
    GLFC CHAR(1) NOT NULL,
    GLFG CHAR(1) NOT NULL
)
GO

INSERT INTO DBO.MS VALUES ('PA', 1, 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S');
INSERT INTO DBO.MS VALUES ('CO', 2, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N');
INSERT INTO DBO.MS VALUES ('MX', 3, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N');
INSERT INTO DBO.MS VALUES ('SV', 4, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N');
INSERT INTO DBO.MS VALUES ('NI', 5, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N');
INSERT INTO DBO.MS VALUES ('HN', 6, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N');
INSERT INTO DBO.MS VALUES ('GT', 7, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N');
INSERT INTO DBO.MS VALUES ('CR', 8, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N');
INSERT INTO DBO.MS VALUES ('CL', 9, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N');
INSERT INTO DBO.MS VALUES ('PE', 10, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N');
INSERT INTO DBO.MS VALUES ('UY', 11, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N');
INSERT INTO DBO.MS VALUES ('PY', 12, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N');
INSERT INTO DBO.MS VALUES ('AR', 13, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N');
GO