IF OBJECT_ID('DBO.generateGLLOG') IS NOT NULL DROP PROCEDURE DBO.generateGLLOG
GO

CREATE PROCEDURE DBO.generateGLLOG (@Country nVarchar(2), @TypeExec int)
AS BEGIN
    DECLARE @GroupRefCode Numeric(10);

    SET NOCOUNT ON;
    BEGIN TRANSACTION generateGLLOG_Trans

    SELECT @GroupRefCode = MAX(ISNULL(L.Group_Ref_Code, 0)) + 1
    FROM COM_AUDIT_LOGS L
    WHERE L.COUNTRY = '' + @Country + '';

    INSERT INTO COM_AUDIT_LOGS (COUNTRY, [USER_ID], [COUNT], [EVENT], STAMP, [TYPE], GROUP_REF_CODE)
    VALUES ('' + @Country + '', 'GL - LOG SYSTEM', null, 'Generate - GL Log System', GETDATE(), 'GL_LOG', @GroupRefCode);

    -- Tablas / *
    EXEC populateGLLOG_Tablas_Cuentas @Country, @GroupRefCode;
    EXEC populateGLLOG_Tablas_Locales @Country, @GroupRefCode;
    EXEC populateGLLOG_Tablas_TipCos @Country, @GroupRefCode;
    EXEC populateGLLOG_Tablas_TipoCont @Country, @GroupRefCode;
    EXEC populateGLLOG_Tablas_Diarios @Country, @GroupRefCode;

    -- Segmentos / Archivo
    EXEC populateGLLOG_Segment @Country, @GroupRefCode, @TypeExec

    -- Archivo / Nombre

    -- Errores / Archivo

    IF @@ERROR != 0 ROLLBACK TRANSACTION generateGLLOG_Trans
    ELSE COMMIT TRANSACTION generateGLLOG_Trans

    SELECT *
    FROM COM_AUDIT_LOGS
    WHERE GROUP_REF_CODE = @GroupRefCode

    RETURN
END