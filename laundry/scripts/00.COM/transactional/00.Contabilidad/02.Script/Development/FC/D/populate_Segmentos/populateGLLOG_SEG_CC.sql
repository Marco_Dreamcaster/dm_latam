IF OBJECT_ID('DBO.populateGLLOG_Seg_CC') IS NOT NULL DROP PROCEDURE DBO.populateGLLOG_Seg_CC
GO

CREATE PROCEDURE DBO.populateGLLOG_Seg_CC (@Country nVarchar(2), @GroupRefCode Numeric(10), @TypeExec int)
AS BEGIN
    DECLARE @QtdReg Numeric(10);

    SET NOCOUNT ON;
    BEGIN TRANSACTION GLLOG_Seg_CC_Trans;

    IF @TypeExec = 1
		BEGIN
			SELECT @QtdReg = Count(*)
			FROM GL_B A
			WHERE A.COUNTRY = '' + @Country + ''
			AND A.COST_CENTER_ORACLE BETWEEN A.COST_CENTER_PARAM1 AND A.COST_CENTER_PARAM2
		END
    ELSE IF @TypeExec = 2
		BEGIN
			SELECT @QtdReg = Count(*)
			FROM GL_R A
			WHERE A.COUNTRY = '' + @Country + ''
			AND A.COST_CENTER_ORACLE BETWEEN A.COST_CENTER_PARAM1 AND A.COST_CENTER_PARAM2
		END
    ELSE IF @TypeExec = 3
		BEGIN
			SELECT @QtdReg = Count(*)
			FROM GL_H A
			WHERE A.COUNTRY = '' + @Country + ''
			AND A.COST_CENTER_ORACLE BETWEEN A.COST_CENTER_PARAM1 AND A.COST_CENTER_PARAM2
		END

    INSERT INTO COM_AUDIT_LOGS (COUNTRY, [USER_ID], [COUNT], [EVENT], STAMP, [TYPE], GROUP_REF_CODE)
    VALUES ('' + @Country + '', 'GL - LOG SYSTEM', @QtdReg, 'Segmentos - Con Cost Center', GETDATE(), 'GL_LOG', @GroupRefCode);

    IF @TypeExec = 1
		BEGIN
			SELECT @QtdReg = Count(*)
			FROM GL_B A
			WHERE (NOT A.COST_CENTER_ORACLE BETWEEN A.Cost_Center_Param1 AND A.Cost_Center_Param2)
				OR (A.Cost_Center_Param1 IS NULL AND A.Cost_Center_Param2 IS NULL)
		END
    ELSE IF @TypeExec = 2
		BEGIN
			SELECT @QtdReg = Count(*)
			FROM GL_R A
			WHERE (NOT A.COST_CENTER_ORACLE BETWEEN A.Cost_Center_Param1 AND A.Cost_Center_Param2)
				OR (A.Cost_Center_Param1 IS NULL AND A.Cost_Center_Param2 IS NULL)
		END
    ELSE IF @TypeExec = 3
		BEGIN
			SELECT @QtdReg = Count(*)
			FROM GL_H A
			WHERE (NOT A.COST_CENTER_ORACLE BETWEEN A.Cost_Center_Param1 AND A.Cost_Center_Param2)
				OR (A.Cost_Center_Param1 IS NULL AND A.Cost_Center_Param2 IS NULL)
		END

    INSERT INTO COM_AUDIT_LOGS (COUNTRY, [USER_ID], [COUNT], [EVENT], STAMP, [TYPE], GROUP_REF_CODE)
    VALUES ('' + @Country + '', 'GL - LOG SYSTEM', @QtdReg, 'Segmentos - Sin Cost Center', GETDATE(), 'GL_LOG', @GroupRefCode);

    IF @@ERROR != 0 ROLLBACK TRANSACTION GLLOG_Seg_CC_Trans
    ELSE COMMIT TRANSACTION GLLOG_Seg_CC_Trans
END
GO