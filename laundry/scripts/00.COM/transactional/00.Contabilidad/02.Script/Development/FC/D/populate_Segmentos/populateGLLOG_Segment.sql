IF OBJECT_ID('DBO.populateGLLOG_Segment') IS NOT NULL DROP PROCEDURE DBO.populateGLLOG_Segment
GO

CREATE PROCEDURE DBO.populateGLLOG_Segment(@Country nVarchar(2), @GroupRefCode Numeric(10), @TypeExec int)
AS BEGIN
    SET NOCOUNT ON;
    BEGIN TRANSACTION GLLOG_Segment_Trans

    EXEC DBO.populateGLLOG_SEG_CC @Country, @GroupRefCode, @TypeExec -- Cost Center
    --EXEC DBO.populateGLLOG_SEG_PL @Country, @TypeExec, @GroupRefCode -- Product Line
    --EXEC DBO.populateGLLOG_SEG_LB @Country, @TypeExec, @GroupRefCode -- Line of Business
    --EXEC DBO.populateGLLOG_SEG_IC @Country, @TypeExec, @GroupRefCode -- Intercompany

    IF @@ERROR != 0 ROLLBACK TRANSACTION GLLOG_Segment_Trans
    ELSE COMMIT TRANSACTION GLLOG_Segment_Trans
END