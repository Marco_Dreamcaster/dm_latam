IF OBJECT_ID('DBO.populateGLLOG_Tablas_Cuentas') IS NOT NULL DROP PROCEDURE DBO.populateGLLOG_Tablas_Cuentas
GO

CREATE PROCEDURE DBO.populateGLLOG_Tablas_Cuentas (@Country nVarchar(2), @GroupRefCode Numeric(10))
AS BEGIN
    DECLARE @QtdReg NUMERIC(5);

    SET NOCOUNT ON;
    BEGIN TRANSACTION GLLOG_Tablas_Cuentas_Trans;

    SELECT @QtdReg = COUNT(*)
    FROM GLCUENTAS A
    WHERE NOT A.CTAORACLE IS NULL
    AND A.COUNTRY = '' + @Country + '';

    INSERT INTO COM_AUDIT_LOGS (COUNTRY, [USER_ID], [COUNT], [EVENT], STAMP, [TYPE], GROUP_REF_CODE)
    VALUES ('' + @Country + '', 'GL - LOG SYSTEM', @QtdReg, 'Tablas - Cuentas Configured', GETDATE(), 'GL_LOG', @GroupRefCode);

	SELECT @QtdReg = COUNT(*)
    FROM GLCUENTAS A
    WHERE A.CTAORACLE IS NULL
    AND A.COUNTRY = '' + @Country + '';

	INSERT INTO COM_AUDIT_LOGS (COUNTRY, [USER_ID], [COUNT], [EVENT], STAMP, [TYPE], GROUP_REF_CODE)
    VALUES ('' + @Country + '', 'GL - LOG SYSTEM', @QtdReg, 'Tablas - Cuentas NOT Configured', GETDATE(), 'GL_LOG', @GroupRefCode);

    IF @@ERROR != 0 ROLLBACK TRANSACTION GLLOG_Tablas_Cuentas_Trans
    ELSE COMMIT TRANSACTION GLLOG_Tablas_Cuentas_Trans
END
GO