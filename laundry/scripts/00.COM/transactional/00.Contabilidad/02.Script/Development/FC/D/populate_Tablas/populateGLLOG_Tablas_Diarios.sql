IF OBJECT_ID('DBO.populateGLLOG_Tablas_Diarios') IS NOT NULL DROP PROCEDURE DBO.populateGLLOG_Tablas_Diarios
GO

CREATE PROCEDURE DBO.populateGLLOG_Tablas_Diarios (@Country nVarchar(2), @GroupRefCode Numeric(10))
AS BEGIN
    DECLARE @QtdReg NUMERIC(5);

    SET NOCOUNT ON;
    BEGIN TRANSACTION GLLOG_Tablas_Diarios_Trans;

    SELECT @QtdReg = COUNT(*)
    FROM GLDiarios A
    WHERE A.Active = 1
    AND A.COUNTRY = '' + @Country + '';

    INSERT INTO COM_AUDIT_LOGS (COUNTRY, [USER_ID], [COUNT], [EVENT], STAMP, [TYPE], GROUP_REF_CODE)
    VALUES ('' + @Country + '', 'GL - LOG SYSTEM', @QtdReg, 'Tablas - Diarios Active', GETDATE(), 'GL_LOG', @GroupRefCode);

	SELECT @QtdReg = COUNT(*)
    FROM GLDiarios A
    WHERE A.Active = 0
    AND A.COUNTRY = '' + @Country + '';

	INSERT INTO COM_AUDIT_LOGS (COUNTRY, [USER_ID], [COUNT], [EVENT], STAMP, [TYPE], GROUP_REF_CODE)
    VALUES ('' + @Country + '', 'GL - LOG SYSTEM', @QtdReg, 'Tablas - Diarios NOT Active', GETDATE(), 'GL_LOG', @GroupRefCode);

    IF @@ERROR != 0 ROLLBACK TRANSACTION GLLOG_Tablas_Diarios_Trans
    ELSE COMMIT TRANSACTION GLLOG_Tablas_Diarios_Trans
END
GO