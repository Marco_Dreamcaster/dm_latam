IF OBJECT_ID('DBO.populateGLLOG_Tablas_TipCos') IS NOT NULL DROP PROCEDURE DBO.populateGLLOG_Tablas_TipCos
GO

CREATE PROCEDURE DBO.populateGLLOG_Tablas_TipCos (@Country nVarchar(2), @GroupRefCode Numeric(10))
AS BEGIN
    DECLARE @QtdReg NUMERIC(5);

    SET NOCOUNT ON;
    BEGIN TRANSACTION GLLOG_Tablas_TipCos_Trans;

    SELECT @QtdReg = COUNT(*)
    FROM GLTipCos A
    WHERE NOT A.TIPCOSORACLE IS NULL
    AND A.COUNTRY = '' + @Country + '';

    INSERT INTO COM_AUDIT_LOGS (COUNTRY, [USER_ID], [COUNT], [EVENT], STAMP, [TYPE], GROUP_REF_CODE)
    VALUES ('' + @Country + '', 'GL - LOG SYSTEM', @QtdReg, 'Tablas - Tip. Cos. Configured', GETDATE(), 'GL_LOG', @GroupRefCode);

	SELECT @QtdReg = COUNT(*)
    FROM GLTipCos A
    WHERE A.TIPCOSORACLE IS NULL
    AND A.COUNTRY = '' + @Country + '';

	INSERT INTO COM_AUDIT_LOGS (COUNTRY, [USER_ID], [COUNT], [EVENT], STAMP, [TYPE], GROUP_REF_CODE)
    VALUES ('' + @Country + '', 'GL - LOG SYSTEM', @QtdReg, 'Tablas - Tip. Cos. NOT Configured', GETDATE(), 'GL_LOG', @GroupRefCode);

    IF @@ERROR != 0 ROLLBACK TRANSACTION GLLOG_Tablas_TipCos_Trans
    ELSE COMMIT TRANSACTION GLLOG_Tablas_TipCos_Trans
END
GO