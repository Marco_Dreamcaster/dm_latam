IF OBJECT_ID('DBO.populateGLLOG_Tablas_TipoCont') IS NOT NULL DROP PROCEDURE DBO.populateGLLOG_Tablas_TipoCont
GO

CREATE PROCEDURE DBO.populateGLLOG_Tablas_TipoCont (@Country nVarchar(2), @GroupRefCode Numeric(10))
AS BEGIN
    DECLARE @QtdReg NUMERIC(5);

    SET NOCOUNT ON;
    BEGIN TRANSACTION GLLOG_Tablas_TipoCont_Trans;

    SELECT @QtdReg = COUNT(*)
    FROM GLTipoCont A
    WHERE NOT A.TIPOORACLE IS NULL
    AND A.COUNTRY = '' + @Country + '';

    INSERT INTO COM_AUDIT_LOGS (COUNTRY, [USER_ID], [COUNT], [EVENT], STAMP, [TYPE], GROUP_REF_CODE)
    VALUES ('' + @Country + '', 'GL - LOG SYSTEM', @QtdReg, 'Tablas - Tipo Cont. Configured', GETDATE(), 'GL_LOG', @GroupRefCode);

	SELECT @QtdReg = COUNT(*)
    FROM GLTipoCont A
    WHERE A.TIPOORACLE IS NULL
    AND A.COUNTRY = '' + @Country + '';

	INSERT INTO COM_AUDIT_LOGS (COUNTRY, [USER_ID], [COUNT], [EVENT], STAMP, [TYPE], GROUP_REF_CODE)
    VALUES ('' + @Country + '', 'GL - LOG SYSTEM', @QtdReg, 'Tablas - Tipo Cont. NOT Configured', GETDATE(), 'GL_LOG', @GroupRefCode);

    IF @@ERROR != 0 ROLLBACK TRANSACTION GLLOG_Tablas_TipoCont_Trans
    ELSE COMMIT TRANSACTION GLLOG_Tablas_TipoCont_Trans
END
GO