IF OBJECT_ID('DBO.GLCHECKRULES') IS NOT NULL
    DROP TABLE DBO.GLCHECKRULES
GO

CREATE TABLE DBO.GLCHECKRULES (
    [ID] Numeric(5) Not Null Identity,
    Country Varchar(2) Not Null,
    ctaOracle Varchar(8) Not Null,
    cc varchar(4) null,
    lob varchar(2) null,
    pl varchar(2) null,
    ic varchar(6) null
)
GO