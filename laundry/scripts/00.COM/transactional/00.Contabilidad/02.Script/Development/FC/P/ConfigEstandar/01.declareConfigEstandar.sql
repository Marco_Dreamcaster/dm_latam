IF OBJECT_ID('dbo.GLConfigEstandar') IS NOT NULL
    DROP TABLE dbo.GLConfigEstandar
GO

CREATE TABLE dbo.GLConfigEstandar (
    [ID] Numeric(5) not null identity,
    Country Varchar(2) Not Null,
    Branch Varchar(6) Not Null,
    Company Varchar(6) Not Null
    LocalCurrency Varchar(6) Null,
    Org_Name Varchar(60) Not Null,
    Reference1 Varchar(100) Null,
	Reference2 Varchar(240) Null,
	Reference4 Varchar(100) Null,
	Reference5 Varchar(240) Null,
	Reference10 Varchar(240) Null,
	SMRef Varchar(8) Null,
	PMRef Varchar(8) Null
)
GO


IF OBJECT_ID('dbo.GetGLConfigEstandar') IS NOT NULL
    DROP PROCEDURE dbo.GetGLConfigEstandar
GO

CREATE PROCEDURE dbo.GetGLConfigEstandar (@country varchar(2))
AS BEGIN
    SELECT ID, Branch, LocalCurrency, LocalCurrency, Org_Name, Reference1, Reference2, Reference4, Reference5, Reference10,
        ServiceManagerRef, ProjectManagerRef, Country
    FROM dbo.GLConfigEstandar
    WHERE Country = @country

    RETURN
END
GO