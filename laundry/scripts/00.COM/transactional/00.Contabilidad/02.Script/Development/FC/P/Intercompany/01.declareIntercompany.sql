IF OBJECT_ID('DBO.GLIntercompany') IS NOT NULL DROP TABLE DBO.GLIntercompany
GO

CREATE TABLE DBO.GLIntercompany (
    [ID] NUMERIC(5) NOT NULL IDENTITY,
	CTASIGLA NVARCHAR(8) NOT NULL,
	ICCOD NVARCHAR(6) NOT NULL,
	COUNTRY NVARCHAR(2) NOT NULL
)
GO