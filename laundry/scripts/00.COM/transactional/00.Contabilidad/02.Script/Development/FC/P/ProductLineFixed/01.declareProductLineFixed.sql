IF OBJECT_ID('DBO.GLProductLineFixed') IS NOT NULL
    DROP TABLE DBO.GLProductLineFixed
GO

CREATE TABLE DBO.GLProductLineFixed (
    [ID] Numeric(5) Not Null Identity,
    [COUNTRY] Varchar(2) Not Null,
    lob Varchar(2) Not Null,
    pl Varchar(2) Not Null
)
GO