IF OBJECT_ID('DBO.mapGLProductLineFixed') IS NOT NULL
    DROP PROCEDURE DBO.mapGLProductLineFixed
GO

CREATE PROCEDURE DBO.mapGLProductLineFixed(@Country as Varchar(2))
AS BEGIN
    SET NOCOUNT ON;
    BEGIN TRANSACTION mapGLProductLineFixed_Trans

    DELETE FROM DBO.GLProductLineFixed WHERE Country = '' + @Country + '';

    INSERT INTO DBO.GLProductLineFixed (LOB, PL, COUNTRY) VALUES ('10', '10', '' + @Country + '');
    INSERT INTO DBO.GLProductLineFixed (LOB, PL, COUNTRY) VALUES ('20', '10', '' + @Country + '');
    INSERT INTO DBO.GLProductLineFixed (LOB, PL, COUNTRY) VALUES ('40', '00', '' + @Country + '');
    INSERT INTO DBO.GLProductLineFixed (LOB, PL, COUNTRY) VALUES ('50', '00', '' + @Country + '');
    INSERT INTO DBO.GLProductLineFixed (LOB, PL, COUNTRY) VALUES ('70', '00', '' + @Country + '');

    IF @@ERROR != 0 ROLLBACK TRANSACTION mapGLProductLineFixed_Trans
    ELSE COMMIT TRANSACTION mapGLProductLineFixed_Trans
END
GO
