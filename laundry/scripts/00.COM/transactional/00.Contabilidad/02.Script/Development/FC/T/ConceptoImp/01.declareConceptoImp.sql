IF OBJECT_ID('dbo.GLConceptoImp') IS NOT NULL
    DROP TABLE dbo.GLConceptoImp
GO

CREATE TABLE dbo.GLConceptoImp (
    [ID] Numeric(5) not null identity,
    ConImputacion Numeric(4) not null,
    Descrip Varchar(50) null,
    Orden Numeric(4) null,
    Tipo Char(1) null,
    FechaVigencia DateTime null,
    PadConImputacion Numeric(4) null,
    ObraCurso Char(1) not null,
    Country Varchar(2) not null
)
GO

ALTER TABLE DBO.GLCONCEPTOIMP ADD CONSTRAINT PK_GLCONCEPTOIMP PRIMARY KEY (ID)
GO