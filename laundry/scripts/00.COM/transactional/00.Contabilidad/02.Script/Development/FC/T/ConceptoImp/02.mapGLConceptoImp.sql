IF OBJECT_ID('DBO.mapGLConceptoImp') IS NOT NULL DROP PROCEDURE DBO.mapGLConceptoImp
GO

CREATE PROCEDURE DBO.mapGLConceptoImp(@Country nVarchar(2))
AS
BEGIN
    DECLARE @SQL nVarchar(Max);
	DECLARE @sourceDataBaseName nVarchar(20);

	SET NOCOUNT ON
	BEGIN TRANSACTION mapGLConceptoImp_Trans

    SET @sourceDataBaseName = 'TK' + @Country + '_LATEST';

	DELETE FROM DBO.GLCostoLocal WHERE Country = '' + @Country + '';

	SET @SQL = '' ;
	SET @SQL = @SQL + ' INSERT INTO DBO.GLCONCEPTOIMPUTACION (CONIMPUTACION, DESCRIP, ORDEN, TIPO, FECHAVIGENCIA, PADCONIMPUTACION,  ';
	SET @SQL = @SQL + '     OBRACURSO, COUNTRY )';

    SET @SQL = @SQL + ' SELECT ID, DESCRIP, ORDEN, TIPO, FECHAVIGENCIA, PADCONIMPUTACION,  ';
	SET @SQL = @SQL + '     OBRACURSO, ';
    SET @SQL = @SQL + '     ''' +  @Country + '''';
    SET @SQL = @SQL + ' FROM ' + @sourceDataBaseName + '.dbo.ConceptoImputacion ';
    SET @SQL = @SQL + ' WHERE NOT EXISTS ( ';
    SET @SQL = @SQL + '     SELECT A.CONIMPUTACION FROM DBO.GLCONCEPTOIMPUTACION A ';
    SET @SQL = @SQL + '     WHERE A.CONIMPUTACION = ' + @sourceDataBaseName + '.dbo.ConceptoImputacion.CONIMPUTACION '
    SET @SQL = @SQL + ' )';
    EXEC (@SQL) ;

    SET @SQL = '';
    SET @SQL = @SQL + ' DELETE FROM DBO.GLCONCEPTOIMPUTACION ';
    SET @SQL = @SQL + ' WHERE NOT EXISTS ( ';
    SET @SQL = @SQL + '     SELECT A.ID ';
    SET @SQL = @SQL + '     FROM ' + @sourceDataBaseName + '.dbo.ConceptoImputacion A ';
    SET @SQL = @SQL + '     WHERE DBO.GLCONCEPTOIMPUTACION.CONIMPUTACION = A.ID ';
    SET @SQL = @SQL + ' )';
    SET @SQL = @SQL + ' AND COUNTRY = ''' + @Country + '''';
    EXEC (@SQL) ;

    --EXEC dbo.fixGLConceptoImputacion @Country;

	IF @@ERROR != 0 ROLLBACK TRANSACTION mapGLConceptoImp_Trans
	ELSE COMMIT TRANSACTION mapGLConceptoImp_Trans
END;
GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP) VALUES('CO', 'script', 'updated mapGLConceptoImp procedure definition', GETDATE()) ;