IF OBJECT_ID('dbo.GLCostoLocal') IS NOT NULL
    DROP TABLE dbo.GLCostoLocal
GO

CREATE TABLE dbo.GLCostoLocal (
    [ID] Numeric(5) not null identity,
    Id_Sigla char(4) not null,
    Country char(2) not null,
    Descrip varchar(100) null,
    Valor numeric(20,6) null,
    UnidMedida numeric(4) null,
    Moneda char(1) null,
    Procedencia char(3) null,
    Producto char(20) null,
    TipCos char(4) null,
    Formula varchar(5) null,
    Uso char(1) null,
    Montaje char(1) null,
    Clasificacion varchar(100) null,
    Expenditure varchar(100) null
)

GO
ALTER TABLE dbo.GLCostoLocal ADD CONSTRAINT PK_GLCOSTOLOCAL PRIMARY KEY ([ID])
GO