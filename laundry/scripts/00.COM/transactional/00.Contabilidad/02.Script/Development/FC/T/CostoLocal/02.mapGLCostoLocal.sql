IF OBJECT_ID('DBO.mapGLCostoLocal') IS NOT NULL DROP PROCEDURE DBO.mapGLCostoLocal
GO

CREATE PROCEDURE DBO.mapGLCostoLocal(@Country nVarchar(2))
AS
BEGIN
    DECLARE @SQL nVarchar(Max);
	DECLARE @sourceDataBaseName nVarchar(20);

	SET NOCOUNT ON
	BEGIN TRANSACTION mapGLCostoLocal_Trans

    SET @sourceDataBaseName = 'TK' + @Country + '_LATEST';

	SET @SQL = '' ;
	SET @SQL = @SQL + ' INSERT INTO DBO.GLCOSTOLOCAL (ID_SIGLA, DESCRIP, VALOR, UNIDMEDIDA, MONEDA, PROCEDENCIA, PRODUCTO, ';
	SET @SQL = @SQL + '     TIPCOS, FORMULA, USO, MONTAJE, COUNTRY )';

    SET @SQL = @SQL + ' SELECT ID, DESCRIP, VALOR, UNIDMEDIDA, MONEDA, PROCEDENCIA, PRODUCTO, ';
	SET @SQL = @SQL + '     TIPCOS, FORMULA, USO, MONTAJE, ';
    SET @SQL = @SQL + '     ''' +  @Country + '''';
    SET @SQL = @SQL + ' FROM ' + @sourceDataBaseName + '.dbo.CostoLocal ';
    SET @SQL = @SQL + ' WHERE NOT EXISTS ( ';
    SET @SQL = @SQL + '     SELECT A.ID_SIGLA ';
    SET @SQL = @SQL + '     FROM DBO.GLCOSTOLOCAL A ';
    SET @SQL = @SQL + '     WHERE A.ID_SIGLA = ' + @sourceDataBaseName + '.dbo.CostoLocal.ID ';
    SET @SQL = @SQL + '     AND A.COUNTRY = ''' + @Country + '''';
    SET @SQL = @SQL + ' )';
    EXEC (@SQL) ;

    SET @SQL = '';
    SET @SQL = @SQL + ' DELETE FROM DBO.GLCostoLocal ';
    SET @SQL = @SQL + ' WHERE NOT EXISTS ( ';
    SET @SQL = @SQL + '     SELECT A.ID ';
    SET @SQL = @SQL + '     FROM ' + @sourceDataBaseName + '.dbo.CostoLocal A ';
    SET @SQL = @SQL + '     WHERE DBO.GLCostoLocal.ID_SIGLA = A.ID ';
    SET @SQL = @SQL + ' )';
    SET @SQL = @SQL + ' AND COUNTRY = ''' + @Country + '''';
    EXEC (@SQL) ;

    EXEC dbo.fixGLCostoLocal @Country;

	IF @@ERROR != 0 ROLLBACK TRANSACTION mapGLCostoLocal_Trans
	ELSE COMMIT TRANSACTION mapGLCostoLocal_Trans
END;
GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP) VALUES('CO', 'script', 'updated mapGLCostoLocal procedure definition', GETDATE()) ;