IF OBJECT_ID('dbo.GLCuentas') IS NOT NULL DROP TABLE dbo.GLCuentas
GO

CREATE TABLE dbo.GLCuentas(
    [ID] NUMERIC(10) NOT NULL IDENTITY,
	Cta char(8) NOT NULL,
	Descrip varchar(100) NOT NULL,
	CC char(1) NOT NULL,
	Titulo char(1) NOT NULL,
	Acreedor char(1) NULL,
	Ajuste char(1) NOT NULL,
	CtaRei char(8) NULL,
	CtaDolar char(1) NULL,
	TipoTC char(1) NOT NULL,
	DifCam char(1) NOT NULL,
	Tipo char(1) NOT NULL,
	ReqContrato char(1) NOT NULL,
	DifCamDet char(1) NOT NULL,
	DescripI varchar(100) NOT NULL,
	Moneda char(1) NOT NULL,
	CtaMersy char(20) NOT NULL,
	Tercero char(1) NULL,
	Centro char(1) NULL,
	TipoSaldo char(1) NULL,
	ProvCompras char(1) NULL,
	RecibeGiroCheques char(1) NULL,
	BcoDiferida char(1) NULL,
	SoloAutomatica char(1) NULL,
	ObraNueva char(1) NULL,
	ProvAlmacen char(1) NULL,
	Presupuesto char(1) NOT NULL,
	ReqCimp char(1) NOT NULL,
	AjusteProvision char(1) NOT NULL,
	Compras char(1) NULL,
	BloqueoImputacion varchar(1) NULL,
	CodReductora bit NULL,
	Country CHAR(2) NOT NULL,
	CtaOracle NVARCHAR(20) NULL
)

ALTER TABLE dbo.GLCuentas ADD CONSTRAINT PK_GLCUENTAS PRIMARY KEY (CTA)
GO