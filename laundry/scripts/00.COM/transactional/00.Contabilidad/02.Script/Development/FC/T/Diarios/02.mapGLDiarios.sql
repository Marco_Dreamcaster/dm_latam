
IF OBJECT_ID('DBO.mapGLDiarios') IS NOT NULL DROP PROCEDURE DBO.mapGLDiarios
GO

CREATE PROCEDURE mapGLDiarios (@Country nVarchar(2))
AS
BEGIN
    DECLARE @SQL NVARCHAR(MAX);
	DECLARE @sourceDataBaseName NVARCHAR(20);

	SET @sourceDataBaseName = 'TK' + @Country + '_LATEST';

    SET NOCOUNT ON
    BEGIN TRANSACTION mapGLDiarios_Trans

    SET @SQL = '';
    SET @SQL = @SQL + ' INSERT INTO GLDiarios (DIARIO, DESCRIP, ACTIVE, COUNTRY )'
    SET @SQL = @SQL + ' SELECT DIARIO, DESCRIP, 1, ''' + @COUNTRY + '''';
    SET @SQL = @SQL + ' FROM ' + @sourceDataBaseName + '.DBO.DIARIOS ';
    SET @SQL = @SQL + ' WHERE NOT EXISTS ( ';
    SET @SQL = @SQL + '     SELECT A.DIARIO ';
    SET @SQL = @SQL + '     FROM DBO.GLDIARIOS A ';
    SET @SQL = @SQL + '     WHERE A.DIARIO = ' + @sourceDataBaseName + '.DBO.DIARIOS.DIARIO ';
    SET @SQL = @SQL + '     AND A.COUNTRY = ''' + @Country + '''';
    SET @SQL = @SQL + ' )';
    EXEC (@SQL);

    SET @SQL = '';
    SET @SQL = @SQL + ' DELETE FROM DBO.GLDiarios ';
    SET @SQL = @SQL + ' WHERE NOT EXISTS ( ';
    SET @SQL = @SQL + '     SELECT A.DIARIO ';
    SET @SQL = @SQL + '     FROM ' + @sourceDataBaseName + '.dbo.DIARIOS A ';
    SET @SQL = @SQL + '     WHERE DBO.GLDiarios.DIARIO = A.DIARIO ';
    SET @SQL = @SQL + ' )';
    SET @SQL = @SQL + ' AND COUNTRY = ''' + @Country + '''';
    EXEC (@SQL) ;
    
    IF @@ERROR != 0 ROLLBACK TRANSACTION mapGLDiarios_Trans
	ELSE COMMIT TRANSACTION mapGLDiarios_Trans
END
GO