IF OBJECT_ID('dbo.GLLocales') IS NOT NULL DROP TABLE dbo.GLLocales
GO

CREATE TABLE dbo.GLLocales(
    [ID] Numeric(10) NOT NULL IDENTITY,
	[Local] Char(3) NOT NULL,
	Descrip Varchar(50) NOT NULL,
	IDCiudad Int NULL,
	Country Char(2) NOT NULL,
	LocalOracle NVarchar(20) NULL
)

ALTER TABLE DBO.GLLocales ADD CONSTRAINT PK_GLLOCALES PRIMARY KEY ([LocaL])
GO

