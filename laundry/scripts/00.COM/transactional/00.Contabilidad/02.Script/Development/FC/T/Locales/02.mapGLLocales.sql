IF OBJECT_ID('DBO.mapGLLocales') IS NOT NULL DROP PROCEDURE DBO.mapGLLocales
GO

CREATE PROCEDURE DBO.mapGLLocales(@Country nVarchar(2))
AS
BEGIN
	DECLARE @SQL NVARCHAR(MAX);
	DECLARE @sourceDataBaseName NVARCHAR(20);
	
	SET @sourceDataBaseName = 'TK' + @Country + '_LATEST';
	
	SET NOCOUNT ON
	BEGIN TRANSACTION mapGLLocales_Trans

	DELETE FROM DBO.GLLOCALES WHERE COUNTRY = '' + @Country + '';

	SET @SQL = '';
	SET @SQL = @SQL + ' INSERT INTO DBO.GLLOCALES ([LOCAL], DESCRIP, IDCIUDAD, COUNTRY) ';
	SET @SQL = @SQL + ' SELECT [LOCAL], DESCRIP, IDCIUDAD, ''' + @Country + '''';
	SET @SQL = @SQL + ' FROM ' + @sourceDataBaseName + '.dbo.LOCALES ';
	SET @SQL = @SQL + ' WHERE NOT EXISTS ( ';
    SET @SQL = @SQL + '     SELECT A.LOCAL ';
    SET @SQL = @SQL + '     FROM DBO.GLLOCALES A ';
    SET @SQL = @SQL + '     WHERE A.LOCAL = ' + @sourceDataBaseName + '.dbo.LOCALES.LOCAL ';
    SET @SQL = @SQL + '     AND A.COUNTRY = ''' + @Country + '''';
    SET @SQL = @SQL + ' )';
	EXEC (@SQL);

	SET @SQL = '';
    SET @SQL = @SQL + ' DELETE FROM DBO.GLLOCALES ';
    SET @SQL = @SQL + ' WHERE NOT EXISTS ( ';
    SET @SQL = @SQL + '     SELECT A.LOCAL ';
    SET @SQL = @SQL + '     FROM ' + @sourceDataBaseName + '.dbo.LOCALES A ';
    SET @SQL = @SQL + '     WHERE DBO.GLLOCALES.LOCAL = A.LOCAL ';
    SET @SQL = @SQL + ' )';
    SET @SQL = @SQL + ' AND COUNTRY = ''' + @Country + '''';
    EXEC (@SQL) ;

    EXEC dbo.fixGLLocales @Country;

	IF @@ERROR != 0 ROLLBACK TRANSACTION mapGLLocales_Trans
	ELSE COMMIT TRANSACTION mapGLLocales_Trans
END;
GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP) VALUES('PA', 'script', 'updated mapGLLocales procedure definition', GETDATE());