IF OBJECT_ID('dbo.fixGLLocales') IS NOT NULL
    DROP PROCEDURE dbo.fixGLLocales
GO

CREATE PROCEDURE dbo.fixGLLocales (@Country nVarchar(2))
AS BEGIN
    SET NOCOUNT ON;
    BEGIN TRANSACTION fixGLLocales_Trans

    UPDATE dbo.GLLocales SET LocalOracle = '501001' WHERE [Local] = '001' AND Country = '' + @Country + '';
	UPDATE dbo.GLLocales SET LocalOracle = '501030' WHERE [Local] = '002' AND Country = '' + @Country + '';

    IF @@Error != 0 ROLLBACK TRANSACTION fixGLLocales_Trans
    ELSE COMMIT TRANSACTION fixGLLocales_Trans
END
GO