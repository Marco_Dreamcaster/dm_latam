IF OBJECT_ID('DBO.mapGLMonedas') IS NOT NULL DROP PROCEDURE DBO.mapGLMonedas
GO

CREATE PROCEDURE DBO.mapGLMonedas(@Country nVarchar(2))
AS
BEGIN
	DECLARE @SQL NVARCHAR(MAX);
	DECLARE @sourceDataBaseName NVARCHAR(20);
	
	SET @sourceDataBaseName = 'TK' + @Country + '_LATEST';
	
	SET NOCOUNT ON
	BEGIN TRANSACTION mapGLMonedas_Trans

	DELETE FROM DBO.GLMONEDAS WHERE COUNTRY = '' + @Country + '';

	SET @SQL = '';
	SET @SQL = @SQL + ' INSERT INTO DBO.GLMONEDAS (MONEDA, DESCRIP, SIGLA, COUNTRY) ';
	SET @SQL = @SQL + ' SELECT MONEDA, DESCRIP, SIGLA, ''' + @Country + '''';
	SET @SQL = @SQL + ' FROM ' + @sourceDataBaseName + '.dbo.MONEDAS ';
	SET @SQL = @SQL + ' WHERE NOT EXISTS ( ';
    SET @SQL = @SQL + '     SELECT A.MONEDA ';
    SET @SQL = @SQL + '     FROM DBO.GLMONEDAS A ';
    SET @SQL = @SQL + '     WHERE A.MONEDA = ' + @sourceDataBaseName + '.dbo.MONEDAS.MONEDA ';
    SET @SQL = @SQL + '     AND A.COUNTRY = ''' + @Country + '''';
    SET @SQL = @SQL + ' )';
	EXEC (@SQL);

	SET @SQL = '';
    SET @SQL = @SQL + ' DELETE FROM DBO.GLMONEDAS ';
    SET @SQL = @SQL + ' WHERE NOT EXISTS ( ';
    SET @SQL = @SQL + '     SELECT A.MONEDA ';
    SET @SQL = @SQL + '     FROM ' + @sourceDataBaseName + '.dbo.MONEDAS A ';
    SET @SQL = @SQL + '     WHERE DBO.GLMONEDAS.MONEDA = A.MONEDA ';
    SET @SQL = @SQL + ' )';
    SET @SQL = @SQL + ' AND COUNTRY = ''' + @Country + '''';
    EXEC (@SQL) ;

-- COLOMBIA
--    D	DOLAR	USD	3252.096070
--    E	EURO	EUR	3723.650000
--    P	PESO	COP	1.000000
--    R	REAL	BRL	837.905040
--    S	CORONAS SUECAS	SEK	363.112880

	IF @@ERROR != 0 ROLLBACK TRANSACTION mapGLMonedas_Trans
	ELSE COMMIT TRANSACTION mapGLMonedas_Trans
END;
GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP) VALUES('PA', 'script', 'updated mapGLMonedas procedure definition', GETDATE());