IF OBJECT_ID('DBO.mapGLTipCos') IS NOT NULL DROP PROCEDURE DBO.mapGLTipCos
GO

CREATE PROCEDURE DBO.mapGLTipCos(@Country nVarchar(2))
AS
BEGIN
	DECLARE @SQL NVARCHAR(MAX);
	DECLARE @sourceDataBaseName NVARCHAR(20);
	
	SET @sourceDataBaseName = 'TK' + @Country + '_LATEST';
	
	SET NOCOUNT ON
	BEGIN TRANSACTION mapGLTipCos_Trans

	DELETE FROM DBO.GLTIPCOS WHERE COUNTRY = '' + @Country + '';

	SET @SQL = '';
	SET @SQL = @SQL + ' INSERT INTO DBO.GLTIPCOS (TIPCOS, DESCRIP, PORCENTAJE, COUNTRY) ';
	SET @SQL = @SQL + ' SELECT TIPCOS, DESCRIP, PORCENTAJE, ''' + @Country  + '''';
	SET @SQL = @SQL + ' FROM ' + @sourceDataBaseName + '.dbo.TIPCOS ';
	SET @SQL = @SQL + ' WHERE NOT EXISTS (';
	SET @SQL = @SQL + '     SELECT A.TIPCOS ';
	SET @SQL = @SQL + '     FROM DBO.GLTIPCOS A ';
	SET @SQL = @SQL + '     WHERE A.TIPCOS = ' + @sourceDataBaseName + '.DBO.TIPCOS.TIPCOS ';
	SET @SQL = @SQL + '     AND A.COUNTRY = ''' + @Country + '''';
	SET @SQL = @SQL + ' )';
	EXEC (@SQL);

    SET @SQL = '';
    SET @SQL = @SQL + ' DELETE FROM DBO.GLTIPCOS ';
    SET @SQL = @SQL + ' WHERE NOT EXISTS ( ';
    SET @SQL = @SQL + '     SELECT A.TIPCOS ';
    SET @SQL = @SQL + '     FROM ' + @sourceDataBaseName + '.dbo.TIPCOS A ';
    SET @SQL = @SQL + '     WHERE DBO.GLTIPCOS.TIPCOS = A.TIPCOS ';
    SET @SQL = @SQL + ' )';
    SET @SQL = @SQL + ' AND COUNTRY = ''' + @Country + '''';
    EXEC (@SQL) ;

    EXEC dbo.fixGLTipCos @Country;

	IF @@ERROR != 0	ROLLBACK TRANSACTION mapGLTipCos_Trans
	ELSE COMMIT TRANSACTION mapGLTipCos_Trans
END;
GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP) VALUES('PA', 'script', 'updated mapGLTipCos procedure definition', GETDATE());