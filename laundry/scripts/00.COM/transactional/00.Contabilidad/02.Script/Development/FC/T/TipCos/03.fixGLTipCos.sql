IF OBJECT_ID('dbo.fixGLTipCos') IS NOT NULL
    DROP PROCEDURE fixGLTipCos
GO

CREATE PROCEDURE dbo.fixGLTipCos (@Country nVarchar(2))
AS BEGIN
    SET NOCOUNT ON;
    BEGIN TRANSACTION fixGLTipCos_Trans

    UPDATE dbo.GLTipCos SET TipCosOracle = NULL WHERE Country = '' + @Country + '';;	

	UPDATE dbo.GLTipCos SET TipCosOracle = '1310' WHERE TipCos = 'E201' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '1110' WHERE TipCos = 'E100' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '1310' WHERE TipCos = 'E202' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '1410' WHERE TipCos = 'E301' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '1310' WHERE TipCos = 'E302' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '1110' WHERE TipCos = 'E400' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '5310' WHERE TipCos = 'E500' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '5310' WHERE TipCos = 'E501' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '5110' WHERE TipCos = 'E502' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '5210' WHERE TipCos = 'E503' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '7310' WHERE TipCos = 'E600' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '7310' WHERE TipCos = 'E601' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '7310' WHERE TipCos = 'E602' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '7310' WHERE TipCos = 'E603' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '7310' WHERE TipCos = 'E604' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '7921' WHERE TipCos = 'E610' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '7910' WHERE TipCos = 'E611' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '7940' WHERE TipCos = 'E612' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '1943' WHERE TipCos = 'E613' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '1910' WHERE TipCos = 'E614' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '7950' WHERE TipCos = 'E615' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '7830' WHERE TipCos = 'E616' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '7810' WHERE TipCos = 'E617' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '7820' WHERE TipCos = 'E618' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '5140' WHERE TipCos = 'E619' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '5340' WHERE TipCos = 'E619' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '7435' WHERE TipCos = 'E620' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '7435' WHERE TipCos = 'E621' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '1910' WHERE TipCos = 'E700' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '1943' WHERE TipCos = 'E702' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '1350' WHERE TipCos = 'E701' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '1350' WHERE TipCos = 'E701' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '1350' WHERE TipCos = 'E701' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '1150' WHERE TipCos = 'E711' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '1250' WHERE TipCos = 'E712' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '1350' WHERE TipCos = 'E713' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '2100' WHERE TipCos = 'O01' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '2100' WHERE TipCos = 'O02' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '2100' WHERE TipCos = 'O03' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '2100' WHERE TipCos = 'O04' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '1110' WHERE TipCos = 'O05' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '2100' WHERE TipCos = 'O06' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '2100' WHERE TipCos = 'O07' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '2100' WHERE TipCos = 'O08' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '2100' WHERE TipCos = 'O09' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '2200' WHERE TipCos = 'O01' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '2200' WHERE TipCos = 'O02' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '2200' WHERE TipCos = 'O03' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '2200' WHERE TipCos = 'O04' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '1110' WHERE TipCos = 'O05' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '2200' WHERE TipCos = 'O06' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '2200' WHERE TipCos = 'O07' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '2200' WHERE TipCos = 'O08' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipCos SET TipCosOracle = '2200' WHERE TipCos = 'O09' AND Country = '' + @Country + '';
	
    IF @@Error != 0 ROLLBACK TRANSACTION fixGLTipCos_Trans
    ELSE COMMIT TRANSACTION fixGLTipCos_Trans
END
GO