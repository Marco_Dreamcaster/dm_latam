IF OBJECT_ID('DBO.mapGLTipoCont') IS NOT NULL DROP PROCEDURE DBO.mapGLTipoCont
GO

CREATE PROCEDURE DBO.mapGLTipoCont(@Country nVarchar(2))
AS
BEGIN
	DECLARE @SQL NVARCHAR(MAX);
	DECLARE @sourceDataBaseName NVARCHAR(20);
	
	SET @sourceDataBaseName = 'TK' + @Country + '_LATEST';
	
	SET NOCOUNT ON
	BEGIN TRANSACTION mapGLTipoCont_Trans

	DELETE FROM DBO.GLTIPOCONT WHERE COUNTRY = '' + @Country + '';

	SET @SQL = '';
	SET @SQL = @SQL + ' INSERT INTO DBO.GLTIPOCONT (TIPO, DESCRIP, CTA, CTA1, REQEQUIPO, PRODUCTO, ';
	SET @SQL = @SQL + '     CTADOLAR, CTA1DOLAR, REQCONTRATO, PRESUPUESTO, OT, CTACOSTO, REQSERVICIO, CTAEURO, ';
	SET @SQL = @SQL + '     CTA1EURO, TIPOCONTPRESUP, RSP, COUNTRY) ';

	SET @SQL = @SQL + ' SELECT TIPO, DESCRIP, CTA, CTA1, REQEQUIPO, PRODUCTO, ';
	SET @SQL = @SQL + '     CTADOLAR, CTA1DOLAR, REQCONTRATO, PRESUPUESTO, OT, CTACOSTO, REQSERVICIO, CTAEURO, ';
	SET @SQL = @SQL + '     CTA1EURO, TIPOCONTPRESUP, RSP, ''' + @Country + '''';
	SET @SQL = @SQL + ' FROM ' + @sourceDataBaseName + '.dbo.TIPOCONT ';
	SET @SQL = @SQL + ' WHERE NOT EXISTS ( ';
	SET @SQL = @SQL + '     SELECT A.TIPO ';
	SET @SQL = @SQL + '     FROM DBO.GLTIPOCONT A ';
	SET @SQL = @SQL + '     WHERE A.TIPO = ' + @sourceDataBaseName + '.dbo.TIPOCONT.TIPO ';
	SET @SQL = @SQL + '     AND A.COUNTRY = ''' + @Country + '''';
	SET @SQL = @SQL + ' )';
	EXEC (@SQL);

    SET @SQL = '';
    SET @SQL = @SQL + ' DELETE FROM DBO.GLTIPOCONT ';
    SET @SQL = @SQL + ' WHERE NOT EXISTS ( ';
    SET @SQL = @SQL + '     SELECT A.TIPO ';
    SET @SQL = @SQL + '     FROM ' + @sourceDataBaseName + '.dbo.TIPOCONT A ';
    SET @SQL = @SQL + '     WHERE DBO.GLTIPOCONT.TIPO = A.TIPO ';
    SET @SQL = @SQL + ' )';
    SET @SQL = @SQL + ' AND COUNTRY = ''' + @Country + '''';
    EXEC (@SQL) ;

    EXEC dbo.fixGLTipoCont @Country;

	IF @@ERROR !=0 ROLLBACK TRANSACTION mapGLTipoCont_Trans
	ELSE COMMIT TRANSACTION mapGLTipoCont_Trans
END;
GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('PA', 'script', 'updated mapGLTipoCont procedure definition', GETDATE());