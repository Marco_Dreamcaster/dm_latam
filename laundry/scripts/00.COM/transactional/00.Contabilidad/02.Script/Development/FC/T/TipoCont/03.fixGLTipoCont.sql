IF OBJECT_ID('dbo.fixGLTipoCont') IS NOT NULL
    DROP PROCEDURE dbo.fixGLTipoCont
GO

CREATE PROCEDURE dbo.fixGLTipoCont (@Country nVarchar(2))
AS BEGIN
    SET NOCOUNT ON;
    BEGIN TRANSACTION fixGLTipoCont_Trans

	UPDATE dbo.GLTipoCont SET TipoOracle = NULL WHERE Country = '' + @Country + '';

	UPDATE dbo.GLTipoCont SET TipoOracle = '10' WHERE Tipo = '01' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipoCont SET TipoOracle = '20' WHERE Tipo = '10' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipoCont SET TipoOracle = '40' WHERE Tipo = '02' AND Country = '' + @Country + '';
	UPDATE dbo.GLTipoCont SET TipoOracle = '50' WHERE Tipo = '04' AND Country = '' + @Country + '';

    IF @@Error != 0 ROLLBACK TRANSACTION fixGLTipoCont_Trans
    ELSE COMMIT TRANSACTION fixGLTipoCont_Trans
END
GO