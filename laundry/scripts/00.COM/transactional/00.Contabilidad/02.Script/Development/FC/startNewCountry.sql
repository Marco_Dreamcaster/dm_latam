IF OBJECT_ID('dbo.startNewCountry') IS NOT NULL
   DROP PROCEDURE dbo.startNewCountry
GO

CREATE PROCEDURE dbo.startNewCountry(@Country NVARCHAR(2))
AS BEGIN
    SET NOCOUNT ON;
    BEGIN TRANSACTION startNewCountry_Trans;
SELECT * FROM GLConfigEstandar
    -- Setup
    DELETE FROM dbo.GLConfigEstandar WHERE Country = '' + @Country + '';

    INSERT INTO dbo.GLConfigEstandar (Country, Branch, Org_Name, Company, cutoffDate)
    VALUES ('' + @Country + '', '000000', '' + @Country + ' - THYSSENKRUPP ELEVADORES S.A.', '000000', '31-12-2018');

    EXEC dbo.mapGLLocales @Country;

    -- Tablas
    EXEC dbo.mapGLCuentas @Country;
    EXEC dbo.mapGLTipCos @Country;
    EXEC dbo.mapGLTipoCont @Country;
    EXEC dbo.mapGLDiarios @Country;
    EXEC dbo.mapGLCostoLocal @Country;
    EXEC dbo.mapGLMonedas @Country;

    -- Params
    EXEC dbo.mapGLCostCenter @Country;
    EXEC dbo.mapGLCostCenterFixed @Country;
    EXEC dbo.mapGLLineOfBusiness @Country;
    EXEC dbo.mapGLLineOfBusinessFixed @Country;
    EXEC dbo.mapGLProductLine @Country;
    EXEC dbo.mapGLProductLineFixed @Country;
    EXEC dbo.mapGLIntercompany @Country;
    EXEC dbo.mapGLExclusionCuentas @Country;
    EXEC dbo.mapGLCheckRules @Country;

    IF @@Error != 0 ROLLBACK TRANSACTION startNewCountry_Trans
    ELSE COMMIT TRANSACTION startNewCountry_Trans
END
GO