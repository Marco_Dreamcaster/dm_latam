IF OBJECT_ID('DBO.GLCONTABILIDAD') IS NOT NULL
    DROP TABLE DBO.GLCONTABILIDAD
GO

CREATE TABLE DBO.GLCONTABILIDAD (
    FLEXFIELD_SEGMENTS varchar(150),
    ENTERED_DR float,
    ENTERED_CR float,
    NET_LOCAL float,
    ACCOUNTING_DATE Date,
    ACCOUNT_ORACLE varchar(8),
    ACCOUNT_SIGLA varchar(8),
    REFERENCE1 varchar(100),
    REFERENCE2 varchar(240),
    REFERENCE4 varchar(100),
    REFERENCE5 varchar(240),
    REFERENCE10 varchar(240),
    CURRENCY_CODE varchar(15),
    ORG_NAME varchar(60),
    DIARIO varchar(2),
    MONEDA varchar(1),
    TASA float,
    ENTERED_DR_TC float,
    ENTERED_CR_TC float,
    NET_TC float,
    PERIODO Varchar(4),
    MES Varchar(12),
    TYPEGENERATION CHAR(1),
    MONEDALANC CHAR(1)
)
GO