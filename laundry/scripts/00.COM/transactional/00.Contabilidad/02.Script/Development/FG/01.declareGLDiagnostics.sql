IF OBJECT_ID('DBO.GLDiagnosticsB') IS NOT NULL
    DROP TABLE DBO.GLDiagnosticsB
GO

CREATE TABLE DBO.GLDiagnosticsB (
    [ID] Numeric(5) NOT NULL IDENTITY,
    CTA varchar(8) NOT NULL,
    SALDOANT Numeric(15,2) NULL,
    DEBE Numeric(15,2) NULL,
    HABER Numeric(15,2) NULL,
    SALDO Numeric(15,2) NULL,
    LAUNDRY Numeric(15,2) NULL,
    DIF Numeric(15,2) NULL,
    COUNTRY Varchar(2) NULL
)
GO

IF OBJECT_ID('DBO.GLDiagnosticsR') IS NOT NULL
    DROP TABLE DBO.GLDiagnosticsR
GO

CREATE TABLE DBO.GLDiagnosticsR (
    [ID] Numeric(5) NOT NULL IDENTITY,
    CTA varchar(8) NOT NULL,
    SALDOANT Numeric(15,2) NULL,
    DEBE Numeric(15,2) NULL,
    HABER Numeric(15,2) NULL,
    SALDO Numeric(15,2) NULL,
    LAUNDRY Numeric(15,2) NULL,
    DIF Numeric(15,2) NULL,
    COUNTRY Varchar(2) NULL
)
GO

IF OBJECT_ID('DBO.GLDiagnosticsH') IS NOT NULL
    DROP TABLE DBO.GLDiagnosticsH
GO

CREATE TABLE DBO.GLDiagnosticsH (
    [ID] Numeric(5) NOT NULL IDENTITY,
    CTA varchar(8) NOT NULL,
    SALDOANT Numeric(15,2) NULL,
    DEBE Numeric(15,2) NULL,
    HABER Numeric(15,2) NULL,
    SALDO Numeric(15,2) NULL,
    LAUNDRY Numeric(15,2) NULL,
    DIF Numeric(15,2) NULL,
    COUNTRY Varchar(2) NULL
)
GO