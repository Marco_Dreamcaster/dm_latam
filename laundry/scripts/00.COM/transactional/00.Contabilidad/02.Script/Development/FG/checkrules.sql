UPDATE DBO.GLCONTABILIDAD SET
    FLEXFIELD_SEGMENTS =
       (CASE WHEN
          ISNULL((  SELECT [ID]
                    FROM DBO.GLCHECKRULES
                    WHERE COUNTRY = @Country
                    AND CTAORACLE = SUBSTRING(FLEXFIELD_SEGMENTS, 8, 8)
                    AND LOB = SUBSTRING(FLEXFIELD_SEGMENTS, 22, 2)), '0') = '0' THEN FLEXFIELD_SEGMENTS
        ELSE

          SUBSTRING(FLEXFIELD_SEGMENTS,1, 16) + (   SELECT CC
                                                    FROM DBO.GLCHECKRULES
                                                    WHERE COUNTRY = @Country
                                                    AND CTAORACLE = SUBSTRING(FLEXFIELD_SEGMENTS, 8, 8)
                                                    AND LOB = SUBSTRING(FLEXFIELD_SEGMENTS, 22, 2)) + SUBSTRING(FLEXFIELD_SEGMENTS, 21, 41)
        END)
;