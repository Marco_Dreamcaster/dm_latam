DECLARE @TypeGeneration nVarchar(1);
DECLARE @Processamento datetime;

DECLARE @MontoDebe Numeric(15,2);
DECLARE @MontoHaber Numeric(15,2);
DECLARE @Saldo Numeric(15,2);
DECLARE @RegApurados Numeric(10);

DECLARE @Country nVarchar(2);
DECLARE @FechaCorte Date;
DECLARE @Fecha_Hist_Result Date;
DECLARE @Account1 nVarchar(8);
DECLARE @Account2 nVarchar(8);

DECLARE @Year int;
DECLARE @Month int;
DECLARE @YearOld int;
DECLARE @MonthOld int;

DECLARE @AMOUNTSIGLA NUMERIC(15,2);
DECLARE @AMOUNTORACLE NUMERIC(15,2);
DECLARE @AMOUNTDIFF NUMERIC(15,2);
DECLARE @CTAOFF INT;

DECLARE @snapshotDate DATE;