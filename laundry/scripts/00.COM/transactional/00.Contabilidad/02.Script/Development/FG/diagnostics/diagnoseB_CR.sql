:setvar country CR
:setvar cutoffDate 30-04-2019

:setvar TypeGeneration B

:setvar Account1 1
:setvar Account2 29999999

SET ANSI_WARNINGS OFF
GO

SET NOCOUNT ON;

:r "..\declare.sql"
:r "..\default.sql"

DECLARE @cutoffDate DATE;
SET @cutoffDate=CONVERT(date,'$(cutoffDate)',103)
SET @snapshotDate=dbo.udf_EncodeSnapshotDate('$(country)')

PRINT 'country = $(country)' + CHAR(13)+CHAR(10);
PRINT 'cutoffDate = $(cutoffDate)' +  CHAR(13)+CHAR(10);
PRINT 'snapshotDate = ' + CONVERT(varchar, @snapshotDate, 103) + CHAR(13)+CHAR(10);


PRINT 'DIAGNOSTICS FOR BALANCE ON ACC_SIGLA' + CHAR(13)+CHAR(10);
PRINT '12201095' + CHAR(13)+CHAR(10);

SELECT a.ACCOUNT_SIGLA, a.ACCOUNT_ORACLE AS CTA_ORACLE, a.PERIODO AS AC_ANO, a.MES AS AC_MES, a.DIARIO, a.MONEDA, a.CURRENCY_CODE, a.ENTERED_DR, a.ENTERED_CR
FROM DBO.GLCONTABILIDAD a
WHERE TypeGeneration =  'B' AND
a.ACCOUNT_SIGLA = '12201095'
ORDER BY a.ACCOUNT_SIGLA ASC, PERIODO DESC, MES DESC;



