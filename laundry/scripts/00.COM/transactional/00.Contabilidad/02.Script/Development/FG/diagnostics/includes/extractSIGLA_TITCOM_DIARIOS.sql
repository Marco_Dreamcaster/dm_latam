SET ANSI_WARNINGS OFF
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#TIT_COM_DIARIOS_BY_RANGE') IS NOT NULL DROP TABLE #TIT_COM_DIARIOS_BY_RANGE


SELECT
	NULL AS FLEXFIELD_SEGMENTS,
	A.OE AS OE,
	A.LOCAL AS LOCAL,
	A.TIPCOS AS TIPCOS,
	A.TIPOCONT AS TIPOCONT,
	A.DEBE AS ENTERED_DR,
	A.HABER AS ENTERED_CR,
    (A.DEBE-A.HABER) AS NET_LOCAL,
	@cutoffDate AS ACCOUNTING_DATE,
	NULL AS ACCOUNT_ORACLE,
	A.CTA AS ACCOUNT_SIGLA,
	NULL AS REFERENCE1,
	NULL AS REFERENCE2,
	NULL AS REFERENCE4,
	NULL AS REFERENCE5,
	NULL AS REFERENCE10,
	NULL AS CURRENCY_CODE,
	NULL AS ORG_NAME,
	C.DIARIO AS DIARIO,
	NULL AS MONEDA,
	NULL AS TASA,
	NULL AS ENTERED_DR_TC,
	NULL AS ENTERED_CR_TC,
	NULL AS NET_TC,
	B.PERIODO AS PERIODO,
	B.MES AS MES,
	'$(TypeGeneration)' AS TYPEGENERATION,
	A.MONEDA AS MONEDALANC

INTO #TIT_COM_DIARIOS_BY_RANGE
-- SIGLA
FROM TK$(country)_LATEST.DBO.COM A
INNER JOIN TK$(country)_LATEST.DBO.TITCOM B ON (B.IDCOMP = A.IDCOMP)
INNER JOIN TK$(country)_LATEST.DBO.DIARIOS C ON (C.DIARIO = B.DIARIO)

WHERE A.GLOSA <> 'A N U L A D O'
AND NOT (A.DEBE = 0 AND A.HABER = 0)
AND NOT B.MES IN (0, 13)
AND A.CTA BETWEEN $(Account1) AND $(Account2)

;