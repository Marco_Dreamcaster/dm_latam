--:setvar cutoffDate 31-12-2019
--:setvar country CO

DELETE FROM DBO.GLCONTABILIDAD;

:r ".\declare.sql" -- declaracao de variaveis
:r ".\balance.sql" -- arquivo de balan�o
:r ".\result.sql" -- arquivo de resultado

SELECT *
FROM DBO.GLCONTABILIDAD
WHERE CAST(CONVERT(NVARCHAR, '01/' + CAST(MES AS NVARCHAR(2)) + '/' + CAST(PERIODO AS NVARCHAR(4)) , 103) AS DATE)
    <= '' + CONVERT(VARCHAR,  @FechaCorte, 103) + ''
AND TYPEGENERATION = 'B'

UNION ALL

SELECT *
FROM DBO.GLCONTABILIDAD
WHERE CAST(CONVERT(NVARCHAR, '01/' + CAST(MES AS NVARCHAR(2)) + '/' + CAST(PERIODO AS NVARCHAR(4)) , 103) AS DATE)
    BETWEEN DATEADD(DAY, 1, ('' + CONVERT(VARCHAR, @Fecha_Hist_Result, 103) + '')) AND '' + CONVERT(VARCHAR,  @FechaCorte, 103) + ''
AND TYPEGENERATION = 'R'


