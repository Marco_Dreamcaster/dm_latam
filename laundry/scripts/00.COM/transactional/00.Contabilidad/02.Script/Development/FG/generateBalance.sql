--:setvar cutoffDate 30-04-2019
--:setvar country CL

:setvar TypeGeneration B

:setvar Account1 1
:setvar Account2 29999999

:r ".\declare.sql"
:r ".\generateDefaultSIGLAQuery.sql"
:r ".\calculateNetLocal.sql"
:r ".\excludeAccounts.sql"
:r ".\applyFallbackRules.sql"
:r ".\checkRules.sql"
:r ".\generateMayorGeneral.sql"

:r ".\generateBalanceFile.sql"
