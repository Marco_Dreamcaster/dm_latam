--:setvar cutoffDate 30-04-2019
--:setvar country CL

:setvar TypeGeneration R

:setvar Account1 3
:setvar Account2 99999999

:r ".\declare.sql"
:r ".\generateDefaultSIGLAQuery.sql"
:r ".\calculateNetLocal.sql"
:r ".\excludeAccounts.sql"
:r ".\applyFallbackRules.sql"
:r ".\checkRules.sql"
:r ".\generateMayorGeneral.sql"

:r ".\generateResultFile.sql"





