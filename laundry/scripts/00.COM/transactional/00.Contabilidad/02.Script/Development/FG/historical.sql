--:setvar cutoffDate 31-01-2019
--:setvar country CO

:setvar TypeGeneration H

:setvar Account1 3
:setvar Account2 99999999

:r ".\default.sql"
:r ".\adequacy.sql"
:r ".\accountexclusion.sql"
:r ".\fixedrules.sql"
:r ".\checkrules.sql"

SELECT *
FROM DBO.GLCONTABILIDAD
WHERE CAST(CONVERT(NVARCHAR, '01/' + CAST(MES AS NVARCHAR(2)) + '/' + CAST(PERIODO AS NVARCHAR(4)) , 103) AS DATE)
    <= '' + CONVERT(VARCHAR,  @Fecha_Hist_Result, 103) + ''
ORDER BY ACCOUNT_SIGLA;