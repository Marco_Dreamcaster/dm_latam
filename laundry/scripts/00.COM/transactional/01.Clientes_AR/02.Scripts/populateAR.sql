IF OBJECT_ID('dbo.populateAR') IS NOT NULL DROP PROCEDURE dbo.populateAR
go

CREATE PROCEDURE dbo.populateAR(@FechaCorte Date, @country nVarchar(2))
AS
BEGIN
IF @country='CO' EXEC dbo.populateAR_CO @FechaCorte
--ELSE IF @country='MX' EXEC dbo.populateAR_MX @FechaCorte
ELSE RAISERROR (10000, -- Message id.  
				10, -- Severity,  
				1, -- State,  
				N'unconfigured country')



END;
go
INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)  VALUES('ZZ', 'script', 'updated populateAR switcher procedure definition', GETDATE());