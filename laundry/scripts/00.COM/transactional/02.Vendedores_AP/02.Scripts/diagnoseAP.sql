--:setvar country HN
--:setvar cutoffDate 30-04-2019
--:setvar timestamp 20190331

SET ANSI_WARNINGS OFF
SET NOCOUNT ON;

DECLARE @startAt DATETIME
SET @startAt = GETDATE()

DECLARE @cutoffDate DATE;
SET @cutoffDate=CONVERT(date,'$(cutoffDate)',103)

DECLARE @snapshotDate DATE;
SET @snapshotDate=dbo.udf_EncodeSnapshotDate('$(country)')

DECLARE @extractDatetime DATE;
SET @extractDatetime=GETDATE()


PRINT 'country = $(country)' + CHAR(13)+CHAR(10);
PRINT 'cutoffDate = $(cutoffDate)' +  CHAR(13)+CHAR(10);
PRINT 'snapshotDate = ' + CONVERT(varchar, @snapshotDate, 103) + CHAR(13)+CHAR(10);

PRINT CHAR(13)+CHAR(10);
PRINT CHAR(13)+CHAR(10) + '---------------------------- SIGLA MONEDAS on $(country)---------------------' + CHAR(13)+CHAR(10);

SELECT * FROM TK$(country)_LATEST.dbo.MONEDAS

DECLARE @invoicesCount INT;

:r "includes\extractSIGLAAntiguidades.sql"

PRINT CHAR(13)+CHAR(10);
PRINT CHAR(13)+CHAR(10) + '---------------------------- check SIGLA Antiguidades on TKE_$(country)_FD_04_$(timestamp)_AP_SIGLA_Antiguidades.csv for more details ---------------------' + CHAR(13)+CHAR(10);
PRINT CHAR(13)+CHAR(10);

SELECT @invoicesCount = COUNT(*) FROM #RAW_AP
PRINT CHAR(13)+CHAR(10)+ 'SIGLA Antiguidades count = ' + CAST(@invoicesCount AS VARCHAR) + CHAR(13)+CHAR(10);

PRINT CHAR(13)+CHAR(10) + '---------------------------- INVOICE_CURRENCY - SUM OF AMOUNTS ---------------------' + CHAR(13)+CHAR(10);

SELECT DISTINCT INVOICE_CURR, SUM(AMOUNT) AS SUM_OF_AMOUNT FROM #RAW_AP GROUP BY INVOICE_CURR ORDER BY INVOICE_CURR


PRINT CHAR(13)+CHAR(10) + '---------------------------- DISTINCT TYPE, INVOICE_CURRENCY COUNT ---------------------' + CHAR(13)+CHAR(10);

SELECT DISTINCT TYPE, INVOICE_CURR, COUNT(*) AS INVOICE_COUNT FROM #RAW_AP GROUP BY TYPE, INVOICE_CURR



PRINT CHAR(13)+CHAR(10) + '---------------------------- DISTINCT TYPE, INVOICE_CURRENCY - SUM OF AMOUNTS ---------------------' + CHAR(13)+CHAR(10);

SELECT DISTINCT TYPE, INVOICE_CURR, SUM(AMOUNT) AS SUM_OF_AMOUNT FROM #RAW_AP GROUP BY TYPE, INVOICE_CURR ORDER BY TYPE, INVOICE_CURR

PRINT CHAR(13)+CHAR(10) + '---------------------------- DISTINCT TIPODOC ---------------------' + CHAR(13)+CHAR(10);

SELECT DISTINCT
TIPDOC,
TIPDOC_DESCRIPTION, 
TYPE
FROM #RAW_AP b


PRINT CHAR(13)+CHAR(10) + '---------------------------- DISTINCT TYPE COUNT ---------------------' + CHAR(13)+CHAR(10);

SELECT DISTINCT TYPE, COUNT(*) AS INVOICE_COUNT FROM #RAW_AP GROUP BY TYPE

PRINT CHAR(13)+CHAR(10) + '---------------------------- INVOICE_NUM WITH MORE THAN ONE INVOICE (TO BE GROUPED) ---------------------' + CHAR(13)+CHAR(10);

SELECT 
CAST(REPLACE(REPLACE(a.INVOICE_NUM,'.',''),' ','') AS VARCHAR(MAX)) AS INVOICE_NUM,
CAST(a.ID_PROVEEDOR AS VARCHAR(MAX)) AS ID_PROVEEDOR,
COUNT(*) AS SIGLA_INVOICE_COUNT
FROM #RAW_AP a
GROUP BY CAST(REPLACE(REPLACE(a.INVOICE_NUM,'.',''),' ','') AS VARCHAR(MAX)), a.ID_PROVEEDOR
HAVING COUNT(*)>1
ORDER BY SIGLA_INVOICE_COUNT DESC

:r "includes\crossRefVendors.sql"


PRINT CHAR(13)+CHAR(10);
PRINT CHAR(13)+CHAR(10) + '---------------------------- VENDORS WITH INVOICES WHOSE VENDOR_SITE_CODE_ALT COULD NOT BE CROSS-REFERENCED (maybe missing alias or wrongly dismissed?)---------------------' + CHAR(13)+CHAR(10);
PRINT CHAR(13)+CHAR(10);

SELECT * 
FROM #ENCODED_VENDOR_INVOICES
WHERE VENDOR_SITE_CODE_ALT IS NULL

PRINT CHAR(13)+CHAR(10);
PRINT CHAR(13)+CHAR(10) + '---------------------------- COUNT OF CROSS-REFERENCED VENDOR_SITE_CODE_ALT (REQUIRE ATTENTION)---------------------' + CHAR(13)+CHAR(10);
PRINT CHAR(13)+CHAR(10);

SELECT ORACLE_VENDOR_REF, COUNT(*) AS ENCODED_VENDOR_COUNT
FROM #ENCODED_VENDOR_INVOICES
WHERE VENDOR_SITE_CODE_ALT IS NOT NULL
GROUP BY ORACLE_VENDOR_REF
HAVING COUNT(*)>1

:r "includes\mergeInvoices.sql"

-- BENCHMARKING

INSERT INTO COM_AUDIT_LOGS
(COUNTRY,
USER_ID,
TYPE,
EVENT,
SNAPSHOT_DATE,
CUTOFF_DATE,
STAMP,
COUNT,
TARGET)
VALUES(
'$(country)',
'script',
'AP_DIAGNOSE',
'script took ' + CAST(DATEDIFF(second, @startAt, GETDATE()) AS VARCHAR(MAX))+'s',
@snapshotDate,
@cutoffDate,
GETDATE(),
CAST(DATEDIFF(second, @startAt, GETDATE()) AS VARCHAR(MAX)),
0)


