--:setvar country HN
--:setvar cutoffDate 31-03-2019

SET ANSI_WARNINGS OFF
SET NOCOUNT ON;


DECLARE @cutoffDate DATE;
SET @cutoffDate=CONVERT(date,'$(cutoffDate)',103)

DECLARE @extractDatetime DATE;
SET @extractDatetime=GETDATE()

DECLARE @snapshotDate DATE;
SET @snapshotDate=dbo.udf_EncodeSnapshotDate('$(country)')

:r "includes\extractSIGLAAntiguidades.sql"

:r "includes\crossRefVendors.sql"

:r "includes\mergeInvoices.sql"


SELECT
MAX(a.OPERATING_UNIT) AS OPERATING_UNIT,
MAX(a.TIPDOC) AS TIPDOC,
MAX(a.TIPDOC_DESCRIPTION) AS TIPDOC_DESCRIPTION,
MAX(a.TYPE) AS TYPE,
MAX(dbo.udf_EncodeVendorReference('$(country)', a.ID_PROVEEDOR)) AS VENDOR_NUMBER,
MAX(a.ID_PROVEEDOR) AS ID_PROVEEDOR,
MAX(a.CODIGO_LOOKUP) AS CODIGO_LOOKUP,
MAX(a.NOMBRE_PROVEEDOR) AS TRADE_PARTNER,
MAX(a.SUPPLIER_SITE_NAME) AS SUPPLIER_SITE_NAME,
MAX(convert(varchar(25), a.EMISION, 103)) AS INVOICE_EMI_DATE,
MAX(convert(varchar(25), a.VENCIM, 103)) AS INVOICE_DUE_DATE,
a.DIFF_EMI_VEN as DIFF_EMI_VEN,
CASE 
WHEN (a.DIFF_EMI_VEN>=0 AND a.DIFF_EMI_VEN<10) THEN 'IMMEDIATE' 
WHEN (a.DIFF_EMI_VEN>=10 AND a.DIFF_EMI_VEN<20) THEN 'NET10' 
WHEN (a.DIFF_EMI_VEN>=20 AND a.DIFF_EMI_VEN<30) THEN 'NET20' 
WHEN (a.DIFF_EMI_VEN>=30) THEN 'NET30' 
ELSE 'IMMEDIATE' END AS TERMS,
MAX(CAST(REPLACE(REPLACE(a.INVOICE_NUM,'.',''),' ','') AS VARCHAR(MAX))) AS INVOICE_NUM,
MAX(CAST(a.MONEDA AS VARCHAR(MAX))) AS MONEDA,
MAX(CAST(a.INVOICE_CURR AS VARCHAR(MAX))) AS INVOICE_CURR,
convert(varchar,CAST(SUM(a.AMOUNT) AS MONEY)) AS INVOICE_AMOUNT,
CAST(SUM(a.AMOUNT) AS MONEY) AS RAW_AMOUNT,
MAX(CAST(a.DEFAULT_DISTRIBUTION_ACCOUNT AS VARCHAR(MAX))) AS DEFAULT_DISTRIBUTION_ACCOUNT,
MAX(CAST(a.DESCRIPTION AS VARCHAR(MAX))) AS DESCRIPTION,
MAX(CAST(a.RATE_TYPE AS VARCHAR(MAX))) AS RATE_TYPE,
convert(varchar,CAST(MAX(EXCHANGE_RATE) AS DECIMAL(10,4))) AS EXCHANGE_RATE
FROM #RAW_AP a
WHERE 
(a.TYPE='PREPAYMENT')
GROUP BY
OPERATING_UNIT,
TIPDOC,
TIPDOC_DESCRIPTION,
TYPE,
ID_PROVEEDOR,
NOMBRE_PROVEEDOR,
SUPPLIER_SITE_NAME,
EMISION,
INVOICE_NUM,
MONEDA,
INVOICE_CURR,
DEFAULT_DISTRIBUTION_ACCOUNT,
DESCRIPTION,
RATE_TYPE,
EXCHANGE_RATE,
DIFF_EMI_VEN
ORDER BY TYPE, INVOICE_CURR, RAW_AMOUNT DESC


