--:setvar country UY
--:setvar cutoffDate 30-04-2019
--:setvar timestamp 20190318

SET ANSI_WARNINGS OFF
GO

SET NOCOUNT ON;

DECLARE @cutoffDate DATE;
SET @cutoffDate=CONVERT(date,'$(cutoffDate)',103)

DECLARE @snapshotDate DATE;
SET @snapshotDate=dbo.udf_EncodeSnapshotDate('$(country)')

DECLARE @itemCount INT;

PRINT 'country = $(country)' + CHAR(13)+CHAR(10);
PRINT 'cutoffDate = $(cutoffDate)' +  CHAR(13)+CHAR(10);
PRINT 'snapshotDate = ' + CONVERT(varchar, @snapshotDate, 103) + CHAR(13)+CHAR(10);
PRINT 'timestamp = $(timestamp)';

PRINT CHAR(13)+CHAR(10) + '---------------- SIGLA ALMACENES ----------------------'+CHAR(13)+CHAR(10);

:r "includes\listAlmacen.sql"

SELECT @itemCount=COUNT(*) FROM #SIGLA_ALMACEN
PRINT CHAR(13)+CHAR(10) + 'COUNT(*) FROM #SIGLA_ALMACEN = ' +  CAST(@itemCount AS varchar(max)) + CHAR(13)+CHAR(10);

PRINT '@see records on file TKE_$(country)_CD_01_$(timestamp)_listAlmacenes.txt' + CHAR(13)+CHAR(10);


PRINT CHAR(13)+CHAR(10) + '---------------- SIGLA ALMACENES MAPPINGS ----------------------'+CHAR(13)+CHAR(10);

:r "includes\listConfigAlmacen.sql"

SELECT @itemCount=COUNT(*) FROM #CONFIG_ALMACEN
PRINT CHAR(13)+CHAR(10) + 'COUNT(*) FROM #CONFIG_ALMACEN = ' +  CAST(@itemCount AS varchar(max)) + CHAR(13)+CHAR(10);

PRINT '@see records on file TKE_$(country)_CD_01_$(timestamp)_listConfigAlmacen.txt' + CHAR(13)+CHAR(10);

:r "includes\extractReconciliationRawData.sql"

PRINT CHAR(13)+CHAR(10) + '---------------- RECONCILIATION_RAW_DATA ----------------------'+CHAR(13)+CHAR(10);

SELECT @itemCount=COUNT(*) FROM #RAW_RECONCILIATION
PRINT CHAR(13)+CHAR(10) + 'COUNT(*) FROM #RAW_RECONCILIATION = ' +  CAST(@itemCount AS varchar(max)) + CHAR(13)+CHAR(10);

PRINT '@see records on file TKE_$(country)_CD_01_$(timestamp)_listReconciliationRawData.txt' + CHAR(13)+CHAR(10);

DECLARE @amountSIGLA float;

SELECT 
@amountSIGLA=SUM(SALDO*COSTO)
FROM #RAW_RECONCILIATION a 

DECLARE @localCurrency varchar(max);
SELECT @localCurrency=a.LocalCurrency FROM GLConfigEstandar a	WHERE country='$(country)'

DECLARE @dollarRate float;
SELECT @dollarRate=a.Tasa FROM GLMONEDAS a WHERE a.Country='$(country)' AND a.Sigla='USD' 

DECLARE @dollarAmountSIGLA float;
SET @dollarAmountSIGLA = @amountSIGLA / @dollarRate;


PRINT CHAR(13)+CHAR(10) + '---------------- SUM (SALDO*COSTO) FROM RAW_RECONCILIATION ----------------------'+CHAR(13)+CHAR(10);

PRINT CHAR(13)+CHAR(10)+'(A) AMOUNT ON SIGLA = ' + cast(cast( @amountSIGLA as decimal(25,2)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)
PRINT CHAR(13)+CHAR(10)+'USD Exchange rate (used on GL) $1 = ' + cast(cast( @dollarRate as decimal(25,4)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)
PRINT CHAR(13)+CHAR(10)+'(A) DOLLAR AMOUNT ON SIGLA = $' + cast(cast( @dollarAmountSIGLA as decimal(25,2)) AS VARCHAR(MAX)) + CHAR(13)+CHAR(10)


INSERT INTO COM_AUDIT_LOGS(COUNTRY, USER_ID, TYPE, EVENT, SNAPSHOT_DATE, CUTOFF_DATE, STAMP, COUNT)
VALUES(
'$(country)',
'script',
'INV_SIGLA_AMT',
cast(cast( @amountSIGLA as decimal(25,2)) AS VARCHAR(MAX)),
@snapshotDate,
@cutoffDate,
GETDATE(),
@itemCount)

PRINT CHAR(13)+CHAR(10) + '---------------- (SIGLA_REPORT) SUM OF SALDO*COSTO PER ALMACEN AND SUB_INV_ORG_CODE (in ' + @localCurrency + ') ----------------------'+CHAR(13)+CHAR(10);

SELECT 
a.IDALMACEN,
b.subInvOrgCode,
SUM(a.SALDO*a.COSTO) AS TRANSACTION_TOTAL_ALMACEN
FROM #RAW_RECONCILIATION a 
INNER JOIN #CONFIG_ALMACEN b
ON (a.IDALMACEN=b.idAlmacen)
GROUP BY a.IDALMACEN,b.subInvOrgCode

PRINT CHAR(13)+CHAR(10) + '---------------- (SIGLA_REPORT) SUM OF SALDO*COSTO PER SUB_INV_ORG_CODE (in ' + @localCurrency + ') ----------------------'+CHAR(13)+CHAR(10);

SELECT 
b.subInvOrgCode,
SUM(a.SALDO*a.COSTO) AS TRANSACTION_TOTAL_ALMACEN
FROM #RAW_RECONCILIATION a 
INNER JOIN #CONFIG_ALMACEN b
ON (a.IDALMACEN=b.idAlmacen)
GROUP BY b.subInvOrgCode


PRINT CHAR(13)+CHAR(10) + '---------------- SUM OF TRANSACTION_TOTAL PER SUB_INV_ORG_CODE (in ' + @localCurrency + ') ----------------------'+CHAR(13)+CHAR(10);

SELECT 
c.SUB_INV_DISTRIBUTION_ACCOUNT,
SUM(a.SALDO*a.COSTO) AS TRANSACTION_TOTAL_SUB_INV_ORG_CODE
FROM #RAW_RECONCILIATION a
INNER JOIN #CONFIG_ALMACEN b
ON (a.IDALMACEN=b.idAlmacen)
INNER JOIN GLLocales c
ON (b.subInvOrgCode COLLATE DATABASE_DEFAULT = c.SUB_INV_ORG_CODE COLLATE DATABASE_DEFAULT)
WHERE
c.COUNTRY='$(country)'
GROUP BY
c.SUB_INV_DISTRIBUTION_ACCOUNT

IF OBJECT_ID('tempdb..#SIGLA_INVALID_LOCATORS') IS NOT NULL DROP TABLE #SIGLA_INVALID_LOCATORS

SELECT a.PRODUCTO,
a.IDALMACEN,
b.subInvOrgCode,
a.UBICACION
INTO #SIGLA_INVALID_LOCATORS
FROM #RAW_RECONCILIATION a
INNER JOIN #CONFIG_ALMACEN b
ON (a.IDALMACEN=b.idAlmacen)
WHERE UBICACION = @defaultLocatorName

PRINT CHAR(13)+CHAR(10);
PRINT CHAR(13)+CHAR(10) + '---------------- ITEMS ON RAW_RECONCILIATION WITH INVALID LOCATOR (using defaultLocatorName)----------------------'+CHAR(13)+CHAR(10);

PRINT CHAR(13)+CHAR(10) + 'defaultLocatorName for $(country) = ' +  @defaultLocatorName + CHAR(13)+CHAR(10);

SELECT @itemCount=COUNT(*) FROM #SIGLA_INVALID_LOCATORS
PRINT CHAR(13)+CHAR(10) + 'COUNT(*) FROM #SIGLA_INVALID_LOCATORS = ' +  CAST(@itemCount AS varchar(max)) + CHAR(13)+CHAR(10);

PRINT '@see records on file TKE_$(country)_CD_01_$(timestamp)_listItemsWithInvalidLocators.txt' + CHAR(13)+CHAR(10);

PRINT CHAR(13)+CHAR(10);
PRINT CHAR(13)+CHAR(10) + '---------------- CROSS REFERENCE WITH ITEMS ON HEADER ----------------------'+CHAR(13)+CHAR(10);

:r "includes\listSIGLAStockCrossRef.sql"

DECLARE @amountCrossRef float;

SELECT 
@amountCrossRef = SUM(a.TRANSACTION_QUANTITY*TRANSACTION_COST) 
FROM #SIGLA_STOCK_CROSS_REF a

PRINT CHAR(13)+CHAR(10)+'(A) AMOUNT ON SIGLA (RAW_RECONCILIATION) = ' + cast(cast( @amountSIGLA as decimal(25,2)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)
PRINT CHAR(13)+CHAR(10)+'(A1) AMOUNT ON #SIGLA_STOCK_CROSS_REF = ' + cast(cast( @amountCrossRef as decimal(25,2)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)

DECLARE @differenceAfterCrossRef float;
SET @differenceAfterCrossRef = @amountCrossRef - @amountSIGLA;


PRINT CHAR(13)+CHAR(10)+'(A) - (A1) (should be zero) = ' + cast(cast( @differenceAfterCrossRef as decimal(25,2)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10) + '---------------- (ORACLE_REPORT - RAW_RECONCILIATION throug crossRef) SUM OF TRANSACTION_QUANTITY*TRANSACTION_QUANTITY PER SUB_INV_ORG_CODE (in ' + @localCurrency + ') ----------------------'+CHAR(13)+CHAR(10);

IF OBJECT_ID('tempdb..#RAW_TOTAL_CROSSREF') IS NOT NULL DROP TABLE #RAW_TOTAL_CROSSREF

SELECT 
c.SUB_INV_DISTRIBUTION_ACCOUNT,
SUM(a.TRANSACTION_QUANTITY*TRANSACTION_COST) AS TRANSACTION_TOTAL_RAW_DISTRIBUTION_ACCOUNT
INTO #RAW_TOTAL_CROSSREF
FROM #SIGLA_STOCK_CROSS_REF a
INNER JOIN #CONFIG_ALMACEN b
ON (a.IDALMACEN=b.idAlmacen)
INNER JOIN GLLocales c
ON (b.subInvOrgCode COLLATE DATABASE_DEFAULT = c.SUB_INV_ORG_CODE COLLATE DATABASE_DEFAULT)
WHERE
c.COUNTRY='$(country)'
GROUP BY
c.SUB_INV_DISTRIBUTION_ACCOUNT

SELECT * FROM #RAW_TOTAL_CROSSREF ORDER BY SUB_INV_DISTRIBUTION_ACCOUNT

PRINT CHAR(13)+CHAR(10) + '---------------- ITEMS ON RAW_RECONCILIATION THAT ARE NOT ON HEADER ----------------------'+CHAR(13)+CHAR(10);
PRINT CHAR(13)+CHAR(10)

PRINT '@see records on file TKE_$(country)_CD_01_$(timestamp)_problem_listItemTransNotOnHeader.txt' + CHAR(13)+CHAR(10);

:r "..\..\..\..\master\03.Items\02.development\01.SQLCMD\includes\generateItemHeader.sql"

DECLARE @amountNotOnHeader float;

SELECT @amountNotOnHeader=SUM(TRANSACTION_QUANTITY*TRANSACTION_COST) FROM #SIGLA_STOCK_CROSS_REF a
WHERE
ITEM_NUMBER IS NULL OR
ITEM_NUMBER NOT IN (
SELECT itemNumber FROM #ITEM_RAW_HEADER)

PRINT CHAR(13)+CHAR(10) + '---------------- IMPACT OF DIRTY ITEMS ON AMOUNT ----------------------'+CHAR(13)+CHAR(10);

DECLARE @amountSIGLA float;

SELECT 
@amountSIGLA=SUM(SALDO*COSTO)
FROM #RAW_RECONCILIATION a 

DECLARE @localCurrency varchar(max);
SELECT @localCurrency=a.LocalCurrency FROM GLConfigEstandar a	WHERE country='$(country)'


PRINT CHAR(13)+CHAR(10)+'(A) AMOUNT ON SIGLA (RAW_RECONCILIATION) = ' + cast(cast( @amountSIGLA as decimal(25,2)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10)+'(B) AMOUNT NOT ON HEADER = ' + cast(cast( @amountNotOnHeader as decimal(25,2)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)
PRINT CHAR(13)+CHAR(10)+'(B/A) PERCENTAGE OF SIGLA AMOUNT = ' + cast(cast( @amountNotOnHeader/@amountSIGLA*100 as decimal(25,4)) AS VARCHAR(MAX)) + '%' + CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10) + '---------------- MAKE SURE THAT ITEMS NOT ON HEADER ARE RECOMMENDED FOR MIGRATION (RFM) ----------------------'+CHAR(13)+CHAR(10);

DECLARE @itemCount INT;

SELECT @itemCount=COUNT(*) FROM O_PRE_ITEM_HEADER a WHERE a.RECOMMENDED=1 AND a.CLEANSED=0 AND COUNTRY='CO'

PRINT CHAR(13)+CHAR(10) + '$(country) RFM items (before update) = ' +  CAST(@itemCount AS varchar(max)) + CHAR(13)+CHAR(10);

SELECT @itemCount=COUNT(ORIG_ITEM_NUMBER) FROM #SIGLA_STOCK_CROSS_REF a WHERE ITEM_NUMBER IS NULL OR
ITEM_NUMBER NOT IN (
SELECT itemNumber FROM #ITEM_RAW_HEADER)


PRINT CHAR(13)+CHAR(10) + 'COUNT(NOT ON HEADER) FROM #SIGLA_STOCK_CROSS_REF = ' +  CAST(@itemCount AS varchar(max)) + CHAR(13)+CHAR(10);
PRINT CHAR(13)+CHAR(10) + 'make sure all of these ' +  CAST(@itemCount AS varchar(max)) + ' items are marked with ranking=5 and RFM=true ' + CHAR(13)+CHAR(10);

UPDATE O_PRE_ITEM_HEADER SET RANKING=1, RECOMMENDED=0 WHERE COUNTRY='$(country)' AND CLEANSED=0 AND RECOMMENDED=1

UPDATE O_PRE_ITEM_HEADER SET RANKING=5, RECOMMENDED=1
WHERE ORIG_ITEM_NUMBER IN (SELECT ORIG_ITEM_NUMBER FROM #SIGLA_STOCK_CROSS_REF)

SELECT @itemCount=COUNT(*) FROM O_PRE_ITEM_HEADER a WHERE a.RECOMMENDED=1 AND a.CLEANSED=0 AND COUNTRY='$(country)'
PRINT CHAR(13)+CHAR(10) + '$(country) RFM items  (after update) = ' +  CAST(@itemCount AS varchar(max)) + CHAR(13)+CHAR(10);

PRINT CHAR(13)+CHAR(10) + '---------------- IMPACT OF MISSING HEADER ----------------------'+CHAR(13)+CHAR(10);

PRINT CHAR(13)+CHAR(10)+'(A) AMOUNT ON SIGLA (RAW_RECONCILIATION) = ' + cast(cast( @amountSIGLA as decimal(25,2)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)
PRINT CHAR(13)+CHAR(10)+'(B) AMOUNT NOT ON HEADER = ' + cast(cast( @amountNotOnHeader as decimal(25,2)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)

DECLARE @amountOnHeader float;

SELECT 
@amountOnHeader = SUM(a.TRANSACTION_QUANTITY*a.TRANSACTION_COST) 
FROM #SIGLA_STOCK_CROSS_REF a
WHERE
ITEM_NUMBER IS NOT NULL AND
ITEM_NUMBER IN (
SELECT itemNumber FROM #ITEM_RAW_HEADER)

PRINT CHAR(13)+CHAR(10)+'(C) AMOUNT ON HEADER = ' + cast(cast( @amountOnHeader as decimal(25,2)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10)+'(A - C) DIFFERENCE = ' + cast(cast( @amountSIGLA - @amountOnHeader as decimal(25,2)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)

PRINT '(A - C) / A DIFFERENCE PERCENTAGE = ' + cast(cast( (@amountSIGLA - @amountOnHeader)/@amountSIGLA*100 as decimal(25,4)) AS VARCHAR(MAX)) + ' %' + CHAR(13)+CHAR(10)

PRINT '(A - C - B) (should be zero) = ' + cast(cast( (@amountSIGLA - @amountOnHeader - @amountNotOnHeader)  as decimal(25,2)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10)
PRINT CHAR(13)+CHAR(10) + '---------------- CALCULATING LIST PRICE UNIT BASED ON ITEM_NUMBER GROUPING ----------------------'+CHAR(13)+CHAR(10);
PRINT '---------------- AVERAGE PRICE WEIGHTS BASED ON ITEM ASSOCIATIONS  ----------------------'+CHAR(13)+CHAR(10);

:r "includes\listInventoryTrans.sql"

PRINT CHAR(13)+CHAR(10) + '---------------- (ITEM TRANS) with values less than $1 ----------------------'+CHAR(13)+CHAR(10);

SELECT
*
FROM #INVENTORY_TRANS
WHERE 
TRANSACTION_COST < 1
ORDER BY
TRANSACTION_COST ASC


PRINT '@see records on file TKE_$(country)_CD_01_$(timestamp)_ItemTrans.txt   (the same flat file sent to Sha)' + CHAR(13)+CHAR(10);


DECLARE @amountWeighted float;

SELECT 
@amountWeighted = IsNull(SUM(a.TRANSACTION_QUANTITY*a.TRANSACTION_COST),0)
FROM #INVENTORY_TRANS a

SELECT @itemCount=COUNT(*) FROM #INVENTORY_TRANS

DECLARE @snapshotDate DATE;
SET @snapshotDate=dbo.udf_EncodeSnapshotDate('$(country)')

DECLARE @cutoffDate DATE;
SET @cutoffDate=CONVERT(date,'$(cutoffDate)',103)

INSERT INTO COM_AUDIT_LOGS(COUNTRY, USER_ID, TYPE, EVENT, SNAPSHOT_DATE, CUTOFF_DATE, STAMP, COUNT)
VALUES(
'$(country)',
'script',
'INV_ORACLE_AMT',
cast(cast( @amountWeighted as decimal(25,2)) AS VARCHAR(MAX)),
@snapshotDate,
@cutoffDate,
GETDATE(),
@itemCount)

PRINT CHAR(13)+CHAR(10) + '---------------- (ITEM TRANS) SUM OF TRANSACTION_QUANTITY*TRANSACTION_QUANTITY PER DISTRIBUTION_ACCOUNT (in ' + @localCurrency + ') ----------------------'+CHAR(13)+CHAR(10);

DECLARE @dollarRate float;
SELECT @dollarRate=a.Tasa FROM GLMONEDAS a WHERE a.Country='$(country)' AND a.Sigla='USD' 


IF OBJECT_ID('tempdb..#WEIGHTED_PRICE_DIFFERENCE') IS NOT NULL DROP TABLE #WEIGHTED_PRICE_DIFFERENCE

SELECT 
a.DISTRIBUTION_ACCOUNT,
b.TRANSACTION_TOTAL_RAW_DISTRIBUTION_ACCOUNT,
SUM(a.TRANSACTION_QUANTITY*TRANSACTION_COST) AS TRANSACTION_TOTAL_WEIGHTED_PRICES,
CAST(b.TRANSACTION_TOTAL_RAW_DISTRIBUTION_ACCOUNT - SUM(a.TRANSACTION_QUANTITY*TRANSACTION_COST) as decimal(25,2)) AS DIFF,
CAST((b.TRANSACTION_TOTAL_RAW_DISTRIBUTION_ACCOUNT - SUM(a.TRANSACTION_QUANTITY*TRANSACTION_COST))/@dollarRate as decimal(25,2)) AS DIFF_DOLLAR
INTO #WEIGHTED_PRICE_DIFFERENCE
FROM #INVENTORY_TRANS a
INNER JOIN #RAW_TOTAL_CROSSREF b
ON (a.DISTRIBUTION_ACCOUNT=b.SUB_INV_DISTRIBUTION_ACCOUNT)
GROUP BY
a.DISTRIBUTION_ACCOUNT,b.TRANSACTION_TOTAL_RAW_DISTRIBUTION_ACCOUNT


SELECT * FROM #WEIGHTED_PRICE_DIFFERENCE ORDER BY DISTRIBUTION_ACCOUNT

PRINT CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10) + '---------------- SUM OF DIFFERENCES SHOULD BE ZERO (or close to zero) (A - D) ----------------------'+CHAR(13)+CHAR(10);

SELECT SUM(DIFF) AS SUM_OF_DIFF FROM #WEIGHTED_PRICE_DIFFERENCE

PRINT CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10) + '---------------- IMPACT OF WEIGHTED PRICES ON AMOUNT ----------------------'+CHAR(13)+CHAR(10);

PRINT CHAR(13)+CHAR(10)+'(C) AMOUNT ON HEADER = ' + cast(cast( @amountOnHeader as decimal(25,2)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10)+'(D) AMOUNT WEIGHTED PRICE ON HEADER = ' + cast(cast( @amountWeighted as decimal(25,2)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)

PRINT '(C - D) (should be close to zero) = ' + cast(cast( (@amountOnHeader - @amountWeighted)  as decimal(25,2)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10) + '---------------- SUMMARY ----------------------'+CHAR(13)+CHAR(10);

DECLARE @dollarAmountSIGLA float;
SET @dollarAmountSIGLA = @amountSIGLA / @dollarRate;

PRINT CHAR(13)+CHAR(10)+'(A) AMOUNT ON SIGLA (RAW_RECONCILIATION) = ' + cast(cast( @amountSIGLA as decimal(25,2)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10)+'(D) AMOUNT AFTER PURGING ITEMS NOT ON HEADER, AND APPLYING PRICE WEIGHTS = ' + cast(cast( @amountWeighted as decimal(25,2)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10)+'(A - D) DIFFERENCE = ' + cast(cast( ( @amountSIGLA - @amountWeighted) as decimal(25,2)) AS VARCHAR(MAX)) + ' ' + @localCurrency + ' = $' + cast(cast( (( @amountSIGLA - @amountWeighted) / @dollarRate) as decimal(25,2)) AS VARCHAR(MAX)) + CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10)+'USD Exchange rate (same as used on GL) $1 = ' + cast(cast( @dollarRate as decimal(25,4)) AS VARCHAR(MAX)) + ' ' + @localCurrency + CHAR(13)+CHAR(10)

PRINT CHAR(13)+CHAR(10)+'(A - D) / A = DIFFERENCE PERCENTAGE = ' + cast(cast( (@amountSIGLA - @amountWeighted)/@amountSIGLA*100 as decimal(25,4)) AS VARCHAR(MAX)) + ' %' + CHAR(13)+CHAR(10);



