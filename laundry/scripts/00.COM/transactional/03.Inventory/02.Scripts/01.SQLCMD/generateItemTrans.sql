--:setvar country AR
--:setvar cutoffDate 28-02-2019
--:setvar timestamp 20193101

SET ANSI_WARNINGS OFF
GO

SET NOCOUNT ON;

:r "includes\listAlmacen.sql"

:r "includes\listConfigAlmacen.sql"

:r "includes\extractReconciliationRawData.sql"

:r "includes\listSIGLAStockCrossRef.sql"

:r "..\..\..\..\master\03.Items\02.development\01.SQLCMD\includes\generateItemHeader.sql"

--SELECT * FROM #ITEM_RAW_HEADER ORDER BY itemNumber

:r "includes\listInventoryTrans.sql"

SELECT * FROM #INVENTORY_TRANS ORDER BY ITEM_NUMBER

--SELECT SUM(TRANSACTION_QUANTITY*TRANSACTION_COST) FROM #INVENTORY_TRANS

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('$(country)', 'script', 'SQLCMD execution finished for generateItemTrans.sql for $(country)', GETDATE());
