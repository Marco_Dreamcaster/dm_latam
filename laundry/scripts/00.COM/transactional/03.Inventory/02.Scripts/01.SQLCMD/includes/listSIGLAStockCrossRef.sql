SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#SIGLA_STOCK_CROSS_REF') IS NOT NULL DROP TABLE #SIGLA_STOCK_CROSS_REF

SELECT
a.IDALMACEN												AS IDALMACEN,
RTRIM(LTRIM('$(country)' + a.PRODUCTO))					AS ORIG_ITEM_NUMBER,
a.PRODUCTO												AS SIGLA_PRODUCTO,
a.SALDO													AS TRANSACTION_QUANTITY,
a.COSTO													AS TRANSACTION_COST,
a.UBICACION												AS LOCATOR_NAME,
0														AS DISMISSED,
0														AS MERGED_ID,
0														AS LOCAL_VALIDATED,
0														AS CROSSREF_VALIDATED,
b.subInvOrgCode											AS ORGANIZATION_CODE,
c.SUB_INV_DISTRIBUTION_ACCOUNT							AS DISTRIBUTION_ACCOUNT
INTO #SIGLA_STOCK_CROSS_REF
FROM #RAW_RECONCILIATION a
INNER JOIN #CONFIG_ALMACEN b
ON (a.IDALMACEN=b.idAlmacen)
INNER JOIN GLLocales c
ON (b.subInvOrgCode COLLATE DATABASE_DEFAULT = c.SUB_INV_ORG_CODE COLLATE DATABASE_DEFAULT)

ALTER TABLE #SIGLA_STOCK_CROSS_REF ADD ITEM_NUMBER varchar(255);

MERGE INTO #SIGLA_STOCK_CROSS_REF T
   USING O_STG_ITEM_CROSS_REF_ALT S 
      ON (T.ORIG_ITEM_NUMBER = S.ORIG_ITEM_NUMBER AND S.ORGANIZATION_CODE='L$(country)')
WHEN MATCHED THEN
   UPDATE 
      SET T.ITEM_NUMBER = S.ITEM_NUMBER;

MERGE INTO #SIGLA_STOCK_CROSS_REF T
   USING O_STG_ITEM_HEADER S 
      ON (T.ORIG_ITEM_NUMBER = S.ORIG_ITEM_NUMBER AND S.ORGANIZATION_CODE='L$(country)')
WHEN MATCHED THEN
   UPDATE 
      SET T.DISMISSED = S.DISMISSED, T.MERGED_ID=ISNULL(S.MERGED_ID,-1),
	  T.LOCAL_VALIDATED = S.LOCAL_VALIDATED, T.CROSSREF_VALIDATED=S.CROSSREF_VALIDATED;


/**
SELECT * FROM #SIGLA_STOCK_CROSS_REF ORDER BY SIGLA_PRODUCTO
	   
SELECT SIGLA_PRODUCTO FROM #SIGLA_STOCK_CROSS_REF WHERE SIGLA_PRODUCTO NOT IN (SELECT PRODUCTO FROM #RAW_RECONCILIATION)

SELECT * FROM #RAW_RECONCILIATION WHERE PRODUCTO NOT IN (SELECT SIGLA_PRODUCTO FROM #SIGLA_STOCK_CROSS_REF)
**/

/**
FOR DEBUG USE ON INFAMOUS

La instrucci�n MERGE intent� UPDATE o DELETE en la misma fila m�s de una vez.
Esto sucede cuando una fila de destino coincide con m�s de una fila de origen. 
Una instrucci�n MERGE no puede UPDATE/DELETE la misma fila de la tabla de destino varias veces.
Refine la cl�usula ON para asegurarse de que la fila de destino coincide s�lo con una fila de origen, o utilice la cl�usula GROUP BY para agrupar las filas de origen.


ORIG_ITEM_NUMBER	(No column name)
SVX08.012.128	2  (registro com problema)
SVX08.012.191	1
SVX08.012.228	1

SELECT ORIG_ITEM_NUMBER,COUNT(*) FROM O_STG_ITEM_HEADER S WHERE S.COUNTRY='$(country)' GROUP BY ORIG_ITEM_NUMBER ORDER BY COUNT(*) DESC



remover 1 dos SVX08.012.128 (e TODAS suas depend�ncias)

id	ORIG_ITEM_NUMBER
23913	SVX08.012.128
23914	SVX08.012.128

SELECT * FROM O_STG_ITEM_HEADER WHERE ORIG_ITEM_NUMBER LIKE '%X08.012.128%' AND COUNTRY='SV'
SELECT * FROM O_STG_ITEM_CATEGORY WHERE ORIG_ITEM_NUMBER LIKE '%X08.012.128%' AND COUNTRY='SV'
SELECT * FROM O_STG_ITEM_MFG_PART_NUM WHERE ORIG_ITEM_NUMBER LIKE '%X08.012.128%' AND COUNTRY='SV'
SELECT * FROM O_STG_ITEM_TRANS_DEF_LOC WHERE ORIG_ITEM_NUMBER LIKE '%X08.012.128%' AND COUNTRY='SV'
SELECT * FROM O_STG_ITEM_TRANS_DEF_SUB_INV WHERE ORIG_ITEM_NUMBER LIKE '%X08.012.128%' AND COUNTRY='SV'

PROSSEGUIR COM MUITO CUIDADO PARA N�O REMOVER REGISTROS LIMPOS

SEMPRE FAZER BACKUP ANTES!! 

DELETE FROM O_STG_ITEM_HEADER WHERE ID=23914
DELETE FROM O_STG_ITEM_CATEGORY WHERE ID=12182
DELETE FROM O_STG_ITEM_MFG_PART_NUM WHERE ID=22389
DELETE FROM O_STG_ITEM_TRANS_DEF_LOC WHERE ID IN (48367)
DELETE FROM O_STG_ITEM_TRANS_DEF_SUB_INV WHERE ID IN (56335)


**/
