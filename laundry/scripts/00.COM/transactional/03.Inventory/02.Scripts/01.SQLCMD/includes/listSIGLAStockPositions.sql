--DEPRECATED

SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#SIGLA_STOCK_POS') IS NOT NULL DROP TABLE #SIGLA_STOCK_POS

SELECT DISTINCT
'$(country)'																as COUNTRY,
RTRIM(LTRIM('$(country)' + b.PRODUCTO))										as ORIG_ITEM_NUMBER,
a.IdAlmacen																	as ID_ALMACEN,
max(ltrim(rtrim(e.UBICACION))) 												as LOCATOR_NAME,
round(sum(a.SALDO),4)														as TRANSACTION_QUANTITY,
round(sum(a.COSTO * a.SALDO) / (SUM(a.saldo)) ,4)							as TRANSACTION_COST,
round(sum(a.COSTO * a.SALDO) ,4)											as TRANSACTION_TOTAL
INTO #SIGLA_STOCK_POS
from TK$(country)_LATEST..STOCK as a
inner join TK$(country)_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
inner join TK$(country)_LATEST..UBICAPRODALM as e on b.PRODUCTO=e.PRODUCTO and a.IdAlmacen=e.IDALMACEN
inner join TK$(country)_LATEST..ALMACEN as f on a.IdAlmacen=f.IdAlmacen
WHERE
A.PERIODO=DATEPART(year, CONVERT(date,'$(cutoffDate)',103))
AND A.MES=DATEPART(month,CONVERT(date,'$(cutoffDate)',103))                     -- watch out for the base month
AND a.SALDO > 0
group by RTRIM(LTRIM('$(country)' + b.PRODUCTO)), a.IdAlmacen
