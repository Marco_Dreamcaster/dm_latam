--:setvar country CO
--:setvar cutoffDate 31-01-2019

:r "includes\listAlmacen.sql"

:r "includes\listConfigAlmacen.sql"

:r "includes\extractReconciliationRawData.sql"

:r "includes\listSIGLAStockCrossRef.sql"

SELECT * FROM #SIGLA_STOCK_CROSS_REF a WHERE ITEM_NUMBER IS NULL