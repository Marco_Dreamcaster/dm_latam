--:setvar country CO
--:setvar cutoffDate 31-01-2018

:r "includes\listAlmacen.sql"

:r "includes\listConfigAlmacen.sql"

:r "includes\extractReconciliationRawData.sql"

:r "includes\listSIGLAStockCrossRef.sql"

DELETE FROM #SIGLA_STOCK_CROSS_REF WHERE ITEM_NUMBER IS NULL

SELECT * FROM #SIGLA_STOCK_CROSS_REF a WHERE (MERGED_ID=-1 AND DISMISSED=1) AND NOT (LOCAL_VALIDATED=0 OR CROSSREF_VALIDATED=0) AND NOT(ITEM_NUMBER IS NULL)


