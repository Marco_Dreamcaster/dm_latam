--:setvar country PY
--:setvar cutoffDate 31-03-2019
--:setvar timestamp 20190318

:r "includes\listAlmacen.sql"

:r "includes\listConfigAlmacen.sql"

:r "includes\extractReconciliationRawData.sql"

:r "includes\listSIGLAStockCrossRef.sql"

:r "..\..\..\..\master\03.Items\02.development\01.SQLCMD\includes\generateItemHeader.sql"

IF OBJECT_ID('tempdb..#ITEMS_MERGED') IS NOT NULL DROP TABLE #ITEMS_MERGED

SELECT origItemNumber, itemNumber, mergedId INTO #ITEMS_MERGED FROM #ITEM_RAW_HEADER WHERE mergedId IS NOT NULL;

DECLARE @itemCount INT;

SELECT @itemCount=COUNT(*) FROM #ITEMS_MERGED
PRINT CHAR(13)+CHAR(10) + 'COUNT(*) FROM #ITEMS_MERGED = ' +  CAST(@itemCount AS varchar(max)) + CHAR(13)+CHAR(10);


SELECT @itemCount=COUNT(*) FROM #ITEM_RAW_HEADER
PRINT CHAR(13)+CHAR(10) + 'COUNT(*) FROM #ITEM_HEADER = ' +  CAST(@itemCount AS varchar(max)) + CHAR(13)+CHAR(10);

SELECT * FROM #ITEMS_MERGED
