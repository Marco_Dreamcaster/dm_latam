--:setvar country CO
--:setvar cutoffDate 31-12-2018

:r "includes\listAlmacen.sql"

:r "includes\listConfigAlmacen.sql"

:r "includes\extractReconciliationRawData.sql"

:r "includes\listSIGLAStockCrossRef.sql"

SELECT a.PRODUCTO,
a.IDALMACEN,
b.subInvOrgCode,
a.UBICACION
FROM #RAW_RECONCILIATION a
INNER JOIN #CONFIG_ALMACEN b
ON (a.IDALMACEN=b.idAlmacen)
WHERE UBICACION = @defaultLocatorName


