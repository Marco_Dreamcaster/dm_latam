--:setvar country MX
--:setvar cutoffDate 31-03-2019
--:setvar timestamp 20190318

UPDATE O_STG_ITEM_HEADER SET CALCULATED_QUANTITY=0 WHERE CALCULATED_QUANTITY IS NULL AND COUNTRY='$(country)'
UPDATE O_STG_ITEM_HEADER SET PRICE_MANUAL_OVERRIDE=0 WHERE PRICE_MANUAL_OVERRIDE IS NULL AND COUNTRY='$(country)'

:r "includes\listAlmacen.sql"

:r "includes\listConfigAlmacen.sql"

:r "includes\extractReconciliationRawData.sql"

:r "includes\listSIGLAStockCrossRef.sql"

:r "..\..\..\..\master\03.Items\02.development\01.SQLCMD\includes\generateItemHeader.sql"

IF OBJECT_ID('tempdb..#ITEMS_ZERO_STOCK') IS NOT NULL DROP TABLE #ITEMS_ZERO_STOCK

SELECT origItemNumber, itemNumber, listPricePerUnit, priceManualOverride,  quantity
INTO #ITEMS_ZERO_STOCK
FROM #ITEM_RAW_HEADER a
WHERE
listPricePerUnit IS NULL
OR quantity=0

DECLARE @itemCount INT;

SELECT @itemCount=COUNT(*) FROM #ITEMS_ZERO_STOCK
PRINT CHAR(13)+CHAR(10) + 'COUNT(*) FROM #ITEMS_ZERO_STOCK = ' +  CAST(@itemCount AS varchar(max)) + CHAR(13)+CHAR(10);

SELECT @itemCount=COUNT(*) FROM #ITEM_RAW_HEADER
PRINT CHAR(13)+CHAR(10) + 'COUNT(*) FROM #ITEM_HEADER = ' +  CAST(@itemCount AS varchar(max)) + CHAR(13)+CHAR(10);

SELECT * FROM #ITEMS_ZERO_STOCK ORDER BY priceManualOverride DESC, itemNumber ASC, origItemNumber ASC
