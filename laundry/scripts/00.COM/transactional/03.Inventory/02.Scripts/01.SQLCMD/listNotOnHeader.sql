--:setvar country PY
--:setvar cutoffDate 31-03-2019
--:setvar timestamp 20190318

:r "includes\listAlmacen.sql"

:r "includes\listConfigAlmacen.sql"

:r "includes\extractReconciliationRawData.sql"

:r "includes\listSIGLAStockCrossRef.sql"

:r "..\..\..\..\master\03.Items\02.development\01.SQLCMD\includes\generateItemHeader.sql"

IF OBJECT_ID('tempdb..#ITEMS_NOT_ON_HEADER') IS NOT NULL DROP TABLE #ITEMS_NOT_ON_HEADER

SELECT ORIG_ITEM_NUMBER, IDALMACEN, ITEM_NUMBER, TRANSACTION_QUANTITY, TRANSACTION_COST 
INTO #ITEMS_NOT_ON_HEADER
FROM #SIGLA_STOCK_CROSS_REF a
WHERE
ITEM_NUMBER IS NULL OR
ITEM_NUMBER NOT IN (
SELECT itemNumber FROM #ITEM_RAW_HEADER)


DECLARE @itemCount INT;

SELECT @itemCount=COUNT(*) FROM #ITEMS_NOT_ON_HEADER
PRINT CHAR(13)+CHAR(10) + 'COUNT(*) FROM #ITEMS_NOT_ON_HEADER = ' +  CAST(@itemCount AS varchar(max)) + CHAR(13)+CHAR(10);

SELECT * FROM #ITEMS_NOT_ON_HEADER  ORDER BY ITEM_NUMBER


DECLARE @amountCrossRef float;
SELECT @amountCrossRef=SUM(TRANSACTION_QUANTITY*TRANSACTION_COST) FROM #SIGLA_STOCK_CROSS_REF a

PRINT CHAR(13)+CHAR(10) + '@amountCrossRef = ' +  CAST(@amountCrossRef AS varchar(max)) + CHAR(13)+CHAR(10);


DECLARE @amountOnHeader float;

SELECT @amountOnHeader=SUM(TRANSACTION_QUANTITY*TRANSACTION_COST) FROM #SIGLA_STOCK_CROSS_REF a
WHERE
ITEM_NUMBER IS NOT NULL AND
ITEM_NUMBER IN (
SELECT itemNumber FROM #ITEM_RAW_HEADER)

PRINT CHAR(13)+CHAR(10) + '@amountOnHeader = ' +  CAST(@amountOnHeader AS varchar(max)) + CHAR(13)+CHAR(10);

PRINT CHAR(13)+CHAR(10) + 'ITEMS ON HEADER' + CHAR(13)+CHAR(10);

SELECT ORIG_ITEM_NUMBER, ITEM_NUMBER FROM #SIGLA_STOCK_CROSS_REF a
WHERE
ITEM_NUMBER IS NOT NULL AND
ITEM_NUMBER IN (
SELECT itemNumber FROM #ITEM_RAW_HEADER)


DECLARE @amountNotOnHeader float;

SELECT @amountNotOnHeader=SUM(TRANSACTION_QUANTITY*TRANSACTION_COST) FROM #SIGLA_STOCK_CROSS_REF a
WHERE
ITEM_NUMBER IS NULL OR
ITEM_NUMBER NOT IN (
SELECT itemNumber FROM #ITEM_RAW_HEADER)

PRINT CHAR(13)+CHAR(10) + '@amountNotOnHeader = ' +  CAST(@amountNotOnHeader AS varchar(max)) + CHAR(13)+CHAR(10);

PRINT CHAR(13)+CHAR(10) + '@amountNotOnHeader + @amountOnHeader = ' +  CAST((@amountNotOnHeader + @amountOnHeader) AS varchar(max)) + CHAR(13)+CHAR(10);

PRINT CHAR(13)+CHAR(10) + '@diff = ' +  CAST((@amountCrossRef - (@amountNotOnHeader + @amountOnHeader)) AS varchar(max)) + CHAR(13)+CHAR(10);


