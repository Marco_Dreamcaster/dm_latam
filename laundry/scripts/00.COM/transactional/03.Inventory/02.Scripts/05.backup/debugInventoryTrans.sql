DECLARE @cutoffDate DATE;
SET @cutoffDate=CONVERT(date,'31-10-2018',103);

SELECT
dbo.udf_EncodeItemReference('CO',a.PRODUCTO) AS ITEM_NUMBER,
a.IdAlmacen AS idAlmacen,
a.SALDO AS SALDO,
a.COSTO AS COSTO
FROM
TKCO_LATEST..STOCK as a
inner join TKCO_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
WHERE
A.PERIODO=DATEPART(year, @cutoffDate)
and a.IdAlmacen IN (SELECT DISTINCT(CAST(a.idAlmacen AS INTEGER)) FROM GLLocales a WHERE COUNTRY='CO' AND a.idAlmacen IS NOT NULL AND LTRIM(RTRIM(a.idAlmacen))<>'' )
AND A.MES=DATEPART(month, @cutoffDate)
and a.SALDO > 0
and a.PRODUCTO = '3Z.0598.CX.1'

SELECT * FROM TKCO_LATEST..ALMACEN WHERE idAlmacen=5




/**
SELECT DISTINCT
max(ltrim(rtrim(d.ITEM_NUMBER)))											as Item_Number,
max(g.SUB_INV_ORG_CODE)														as Organization_Code,
'DEP01'																		as Subinventory_Name,
round(sum(a.SALDO),4)														as Transaction_Quantity,
'31-10-2018'																as Transaction_Date,
'DATA MIGRATION $(targetCountryCode)'										as Transaction_Reference,
max(ltrim(rtrim(e.UBICACION))) 												as Locator_Name,
round(sum(a.COSTO * a.SALDO) / (SUM(a.saldo)) ,4)                           as Transaction_Cost,
''																			as Serial_Number_From,
''																			as Serial_Number_To	,
MAX(g.SUB_INV_DISTRIBUTION_ACCOUNT)											as Distribution_Account,
'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.'						as Operating_Unit_Name
from TK$(targetCountryCode)_LATEST..STOCK as a
inner join TK$(targetCountryCode)_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
inner join DBO.O_STG_ITEM_CROSS_REF_ALT as c on ltrim(rtrim(b.PRODUCTO))=ltrim(rtrim(c.CROSS_REFERENCE))
inner join DBO.O_STG_ITEM_HEADER as d on c.ORIG_ITEM_NUMBER=d.ORIG_ITEM_NUMBER
inner join TK$(targetCountryCode)_LATEST..UBICAPRODALM as e on b.PRODUCTO=e.PRODUCTO and a.IdAlmacen=e.IDALMACEN
inner join TK$(targetCountryCode)_LATEST..ALMACEN as f on a.IdAlmacen=f.IdAlmacen
inner join DBO.GLLocales as g on a.IdAlmacen=g.idAlmacen
WHERE
A.PERIODO=DATEPART(year, @cutoffDate)
and a.IdAlmacen IN (SELECT DISTINCT(CAST(a.idAlmacen AS INTEGER)) FROM GLLocales a WHERE COUNTRY='$(targetCountryCode)' AND a.idAlmacen IS NOT NULL AND LTRIM(RTRIM(a.idAlmacen))<>'' )
AND A.MES=DATEPART(month, @cutoffDate)
and a.SALDO > 0
and d.COUNTRY='$(targetCountryCode)'
**/