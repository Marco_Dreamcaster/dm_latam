IF OBJECT_ID('DBO.generateItemTransForCO_ALT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateItemTransForCO_ALT;
GO

CREATE PROCEDURE dbo.generateItemTransForCO_ALT
(
@cutoffDate Date
)
AS
BEGIN

	SET NOCOUNT ON;
	BEGIN TRANSACTION

	-- 1. get RAW data from SIGLA

	IF OBJECT_ID('tempdb..##O_STG_ITEM_TRANS_RAW_DATA') IS NOT NULL DROP TABLE ##O_STG_ITEM_TRANS_RAW_DATA;

	SELECT DISTINCT 
	a.PRODUCTO																	as Producto,
	a.idAlmacen																	as idAlmacen,
	MAX(a.SALDO)																as Saldo,
	MAX(a.COSTO)																as Costo,
	MAX(e.Ubicacion)															as Ubicacion,
	'CO'																		as COUNTRY
	INTO ##O_STG_ITEM_TRANS_RAW_DATA
	from TKCO_LATEST..STOCK as a
	inner join TKCO_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
	inner join TKCO_LATEST..ALMACEN as f on a.IdAlmacen=f.IdAlmacen
	inner join TKCO_LATEST..UBICAPRODALM as e on b.PRODUCTO=e.PRODUCTO and a.IdAlmacen=f.IdAlmacen
	WHERE
	A.PERIODO=DATEPART(year, @cutoffDate)
	and a.IdAlmacen IN (SELECT DISTINCT(CAST(a.idAlmacen AS INTEGER)) FROM GLLocales a WHERE COUNTRY='CO' AND a.idAlmacen IS NOT NULL AND LTRIM(RTRIM(a.idAlmacen))<>'' )
	AND A.MES=DATEPART(month, @cutoffDate)
	and a.SALDO > 0
	GROUP BY a.PRODUCTO, a.idAlmacen

	-- 2. crossReference to account for items ALIASES
	--

	IF OBJECT_ID('tempdb..##O_STG_ITEM_TRANS_ALIASED') IS NOT NULL DROP TABLE ##O_STG_ITEM_TRANS_ALIASED;

	SELECT DISTINCT
	b.Producto																	as Producto,
	c.ITEM_NUMBER																as itemNumber,
	b.idAlmacen																	as idAlmacen,
	b.SALDO																		as Saldo,
	b.COSTO																		as Costo,
	(b.SALDO * b.COSTO)															as DollarAmount,
	b.Ubicacion																	as Ubicacion,
	'CO'																		as COUNTRY
	INTO
	##O_STG_ITEM_TRANS_ALIASED
	FROM
	##O_STG_ITEM_TRANS_RAW_DATA b
	inner join DBO.O_STG_ITEM_CROSS_REF_ALT as c on ltrim(rtrim(b.PRODUCTO))=ltrim(rtrim(c.CROSS_REFERENCE))
	inner join DBO.O_STG_ITEM_HEADER as d on c.ORIG_ITEM_NUMBER=d.ORIG_ITEM_NUMBER
	WHERE
	b.COUNTRY='CO'
	

	-- 3. obtain dollarAmount per ITEM_NUMBER
	--

	IF OBJECT_ID('tempdb..##O_STG_ITEM_TRANS_DOLLAR_AMOUNT') IS NOT NULL DROP TABLE ##O_STG_ITEM_TRANS_DOLLAR_AMOUNT;

	SELECT
	b.itemNumber																as itemNumber,
	SUM(b.DollarAmount)															as totalAmount,
	SUM(b.SALDO)																as Saldo,
	'CO'																		as COUNTRY
	INTO
	##O_STG_ITEM_TRANS_DOLLAR_AMOUNT
	FROM
	##O_STG_ITEM_TRANS_ALIASED b
	WHERE
	b.COUNTRY='CO'
	GROUP BY
	b.itemNumber

	-- 4. update averaged prices to O_STG_ITEM_HEADER based on ITEM_NUMBER
	--

	MERGE INTO O_STG_ITEM_HEADER T
	USING ##O_STG_ITEM_TRANS_DOLLAR_AMOUNT S 
      ON T.ITEM_NUMBER=S.itemNumber
         AND S.COUNTRY = 'CO'
	WHEN MATCHED THEN
		UPDATE 
			SET
			LIST_PRICE_PER_UNIT = ROUND(S.totalAmount/S.Saldo,4);

	-- 5. finally project data into ITEM_TRANS IDL format
	--
	IF OBJECT_ID('tempdb..##O_STG_ITEM_TRANS') IS NOT NULL DROP TABLE ##O_STG_ITEM_TRANS;


	SELECT DISTINCT
		RTRIM(LTRIM(a.itemNumber))											as Item_Number,
		g.SUB_INV_ORG_CODE													as Organization_Code,
		g.SUB_INV_ACCOUNT													as Subinventory_Name,
		round(a.SALDO,4)													as Transaction_Quantity,
		CONVERT(VARCHAR(8), @cutoffDate, 5)									as Transaction_Date,
		'DATA MIGRATION CO'													as Transaction_Reference,
		d.LOCATOR_NAME		 												as Locator_Name,
		b.LIST_PRICE_PER_UNIT												as Transaction_Cost,
		''																	as Serial_Number_From,
		''																	as Serial_Number_To	,
		g.SUB_INV_DISTRIBUTION_ACCOUNT										as Distribution_Account,
		'CO - THYSSENKRUPP ELEVADORES S.A.'									as Operating_Unit_Name
	INTO 
	##O_STG_ITEM_TRANS
	FROM
		##O_STG_ITEM_TRANS_ALIASED a
		inner join DBO.O_STG_ITEM_HEADER as b on a.itemNumber=b.ITEM_NUMBER
		inner join DBO.GLLocales as g on g.IdAlmacen=a.idAlmacen
		LEFT join O_STG_ITEM_TRANS_DEF_LOC d on (d.ITEM_NUMBER=RTRIM(LTRIM(a.itemNumber)) AND d.ORGANIZATION_CODE=g.SUB_INV_ORG_CODE)
	WHERE
		a.IdAlmacen IN (SELECT DISTINCT(CAST(a.idAlmacen AS INTEGER)) FROM GLLocales a WHERE COUNTRY='CO' AND a.idAlmacen IS NOT NULL AND LTRIM(RTRIM(a.idAlmacen))<>'' )
	ORDER BY
		RTRIM(LTRIM(a.itemNumber)), g.SUB_INV_ORG_CODE

	IF @@ERROR != 0
	ROLLBACK TRANSACTION
	ELSE
	COMMIT TRANSACTION

	SELECT * FROM ##O_STG_ITEM_TRANS

	RETURN

END

GO



INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('CO', 'script', 'updated generateItemTransForCO_ALT procedure definition', GETDATE());

