:setvar targetCountryCode CO
--:setvar targetCountryCode MX

IF OBJECT_ID('fixRoundedCentsProjectAgreementFor$(targetCountryCode)', 'P') IS NOT NULL
	DROP PROCEDURE dbo.fixRoundedCentsProjectAgreementFor$(targetCountryCode);
GO

CREATE PROCEDURE dbo.fixRoundedCentsProjectAgreementFor$(targetCountryCode)
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

DECLARE @agreement_Number VARCHAR(25)
DECLARE @diff FLOAT

DECLARE ROUNDED_CENTS_FIX CURSOR FOR
SELECT
a.Agreement_Number,
ROUND(MAX(b.Amount)-SUM(a.Amount),3) as diff
FROM O_STG_PROJECT_AGREEMENT_FUNDING a
inner join O_STG_PROJECT_AGREEMENT_HEADER b ON a.Agreement_Number=b.Agreement_Number
GROUP BY a.Agreement_Number
HAVING ABS(ROUND(MAX(b.Amount)-SUM(a.Amount),3))>0
ORDER BY diff DESC

OPEN ROUNDED_CENTS_FIX;

FETCH NEXT FROM ROUNDED_CENTS_FIX
INTO @agreement_Number, @diff;

WHILE @@FETCH_STATUS = 0
BEGIN
DECLARE @taskNumber VARCHAR(25)

SELECT TOP 1 @taskNumber=a.Task_Number FROM O_STG_PROJECT_AGREEMENT_FUNDING a
WHERE a.Agreement_Number=@agreement_Number

UPDATE O_STG_PROJECT_AGREEMENT_FUNDING SET Amount=Amount+@diff
WHERE Task_Number=@taskNumber AND Agreement_Number=@agreement_Number

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('$(targetCountryCode)', 'debug', 'updated with diff ' + CAST(@diff AS VARCHAR(max)), GETDATE());

FETCH NEXT FROM ROUNDED_CENTS_FIX
INTO @agreement_Number, @diff;
END;

CLOSE ROUNDED_CENTS_FIX;

DEALLOCATE ROUNDED_CENTS_FIX;

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

END

GO


INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('$(targetCountryCode)', 'script', 'updated fixRoundedCentsProjectAgreementFor$(targetCountryCode) procedure definition', GETDATE());