:setvar targetCountryCode CO
--:setvar targetCountryCode MX

IF OBJECT_ID('generateProjectAgreementFundingFor$(targetCountryCode)', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateProjectAgreementFundingFor$(targetCountryCode);
GO

CREATE PROCEDURE dbo.generateProjectAgreementFundingFor$(targetCountryCode)
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

DELETE FROM O_STG_PROJECT_AGREEMENT_FUNDING WHERE COUNTRY='$(targetCountryCode)'

INSERT INTO O_STG_PROJECT_AGREEMENT_FUNDING
EXEC dbo.populateProjectAgreementFundingFor$(targetCountryCode)

EXEC dbo.fixRoundedCentsProjectAgreementFor$(targetCountryCode)

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

SELECT
Agreement_Number,
Project_Name,
Task_Number,
Date_Allocated,
Amount,
Category,
Operating_Unit_Name
FROM O_STG_PROJECT_AGREEMENT_FUNDING
WHERE COUNTRY='$(targetCountryCode)'
ORDER BY Agreement_Number

RETURN
END


GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('$(targetCountryCode)', 'script', 'updated generateProjectAgreementFundingFor$(targetCountryCode) procedure definition', GETDATE());