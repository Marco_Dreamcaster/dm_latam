:setvar targetCountryCode CO


IF OBJECT_ID('populateProjectAgreementFundingFor$(targetCountryCode)', 'P') IS NOT NULL
DROP PROCEDURE dbo.populateProjectAgreementFundingFor$(targetCountryCode);
GO

CREATE PROCEDURE dbo.populateProjectAgreementFundingFor$(targetCountryCode)
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

IF OBJECT_ID('tempdb..#tmpAgreementFunding') IS NOT NULL DROP TABLE #tmpAgreementFunding

select distinct
dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) as Agreement_Number,
e.PROJECT_NAME as Project_Name,
LEFT('$(targetCountryCode)_' + CAST (c.IdEquipo AS VARCHAR(max)),20) as Task_Number,
convert(varchar(10),a.REGISTRO,105) as Date_Allocated,
round((g.haber-g.debe) / f.cantequipos,2) as Amount,
'ORIGINAL' as Category,
'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME,
'$(targetCountryCode)' as [COUNTRY],
'loaded from script' as [OBSERVATION],
e.SIGLA_PRIMARY_KEY as [SIGLA_PRIMARY_KEY],
GETDATE() AS LAST_UPDATED
into #tmpAgreementFunding
from TK$(targetCountryCode)_LATEST..contrato as a
LEFT join TK$(targetCountryCode)_LATEST..DETCONT as b on a.CONTRATO=b.CONTRATO
LEFT join TK$(targetCountryCode)_LATEST..EQUIPOS as c on b.IdEquipo=c.IdEquipo
inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) = e.SIGLA_PRIMARY_KEY
inner join (select COUNT(idequipo) as cantequipos,contrato from TK$(targetCountryCode)_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
inner join TK$(targetCountryCode)_LATEST..COM as g on a.contrato = g.CONTRATO
inner join TK$(targetCountryCode)_LATEST..TITCOM as i on g.IdComp=i.IdComp
where
g.CTA in ('31101001','31101002','31101003','31101004','31101005','31101006','31101007','31101008','31101030','31102','31102001','31102002','31102003','31102004','31102006','31102007','31102008')
--and a.contrato in (select [COD OBRA] from contratosPOC )
and g.fecha <= convert(datetime, '31-10-2018', 105)
union all

select
dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) as Agreement_Number,
c.PROJECT_NAME as Project_Name,
LEFT('$(targetCountryCode)_' + CAST (e.IdEquipo AS VARCHAR(max)),20) as Task_Number,
convert(varchar(10),a.REGISTRO,105) as Date_Allocated,
round(((b.[TOT CONTRATO])+b.[Valor de manto Gratuito]) / f.cantequipos ,2) as Amount,
'ORIGINAL' as Category,
'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME,
'$(targetCountryCode)' as [COUNTRY],
'loaded from script' as [OBSERVATION],
c.SIGLA_PRIMARY_KEY as [SIGLA_PRIMARY_KEY],
GETDATE() AS LAST_UPDATED
from  TKPA_LATEST..CONTRATO as a
inner join contratosPOC as b on a.CONTRATO=b.[COD OBRA]
inner join O_STG_PROJECT_HEADER as c on dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) = c.SIGLA_PRIMARY_KEY
inner join TKPA_LATEST..DETCONT as d on a.CONTRATO=d.CONTRATO
inner join TKPA_LATEST..EQUIPOS as e on d.IdEquipo=e.IdEquipo
inner join (select COUNT(idequipo) as cantequipos,contrato from TKPA_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
where a.CONTRATO in (select [COD OBRA] from contratosPOC)

select
max(Agreement_Number) as Agreement_Number,
Project_Name,
Task_Number,
max(Date_Allocated) as Date_Allocated,
sum(Amount)as Amount,
max(Category) as Category,
max(OU_NAME) as OU_NAME,
max(COUNTRY) as [COUNTRY],
max(OBSERVATION) as [OBSERVATION],
max(SIGLA_PRIMARY_KEY) as [SIGLA_PRIMARY_KEY],
max(LAST_UPDATED) as LAST_UPDATED
from #tmpAgreementFunding
where Amount > 0
group by Project_Name,Task_Number
order by Project_Name,Task_Number


IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

RETURN
END


GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('$(targetCountryCode)', 'script', 'updated populateProjectAgreementFundingFor$(targetCountryCode) procedure definition', GETDATE());