:setvar targetCountryCode CO
--:setvar targetCountryCode MX

IF OBJECT_ID('populateProjectAgreementHeaderFor$(targetCountryCode)', 'P') IS NOT NULL
DROP PROCEDURE dbo.populateProjectAgreementHeaderFor$(targetCountryCode);
Go

CREATE PROCEDURE dbo.populateProjectAgreementHeaderFor$(targetCountryCode)
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

IF OBJECT_ID('tempdb..#tmpAgreement') IS NOT NULL DROP TABLE #tmpAgreement
--- sigla procedure
select
e.CUSTOMER_NUMBER as Customer_Number,
dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) as Agreement_Number,
'CONSTRUCTION' as Agreement_Type,
round(g.haber-g.debe,2)as Amount,
'$(targetCountryCode)P' as Currency_code,
'Y' as Revenue_Limit_Flag,
'Y' as Invoice_Limit_Flag,
'$(targetCountryCode)DM001' as Administrator_Employee_Reference,
e.PROJECT_NAME as Description,
'' as Date_Expiration,
case WHEN ORGANIZATION_CODE = '501001' then  'NET30'
	when ORGANIZATION_CODE = '501030' then 'NET10'
	ELSE 'TERMS SIN MAPEO'  END  as Terms,
'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME,
'$(targetCountryCode)' as [COUNTRY],
'loaded from script' as [OBSERVATION],
e.SIGLA_PRIMARY_KEY as [SIGLA_PRIMARY_KEY],
GETDATE() AS LAST_UPDATED
into #tmpAgreement
from TK$(targetCountryCode)_LATEST..CONTRATO as a
left join TK$(targetCountryCode)_LATEST..PAGOS as b on a.IDPAGO=b.IdPago
inner join (select TOP 1 VENTA from TK$(targetCountryCode)_LATEST..CAMBIOS where month(FECHA)= MONTH(dateadd(mm,-1,getdate())) order by FECHA desc) as c on 1=1
inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) = e.SIGLA_PRIMARY_KEY
inner join TK$(targetCountryCode)_LATEST..COM as g on a.contrato = g.CONTRATO
where
--a.contrato in (select [COD OBRA] from contratosPOC)  // removing reference from contratosPOC
e.LOCAL_VALIDATED=1
and e.CROSSREF_VALIDATED=1
and g.CTA in ('31101001','31101002','31101003','31101004','31101005','31101006','31101007','31101008','31101030','31102','31102001','31102002','31102003','31102004','31102006','31102007','31102008')
and g.fecha <= convert(datetime, '31-10-2018', 105)

--- full procedure

union all
select
'$(targetCountryCode)' + ltrim(rtrim(a.CLIENTE))+'-CC' as Customer_Number,
dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) as Agreement_Number,
'CONSTRUCTION' as Agreement_Type,
round(ROUND(d.[TOT CONTRATO],2)+round(d.[Valor de manto Gratuito],2),2)as Amount,
'USD' as Currency_code,
'Y' as Revenue_Limit_Flag,
'Y' as Invoice_Limit_Flag,
'CODM01' as Administrator_Employee_Reference,
e.PROJECT_NAME as Description,
'' as Date_Expiration	,
case WHEN ORGANIZATION_CODE = '501001' then  'NET30'
	when ORGANIZATION_CODE = '501030' then 'NET10' END  as Terms,
'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME,
'$(targetCountryCode)' as [COUNTRY],
'loaded from script' as [OBSERVATION],
e.SIGLA_PRIMARY_KEY as [SIGLA_PRIMARY_KEY],
GETDATE() AS LAST_UPDATED
from TK$(targetCountryCode)_LATEST..CONTRATO as a
left join TK$(targetCountryCode)_LATEST..PAGOS as b on a.IDPAGO=b.IdPago
inner join contratosPOC as d on a.CONTRATO=d.[COD OBRA]
inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) = e.SIGLA_PRIMARY_KEY
where
a.contrato in (select [COD OBRA] from contratosPOC)

---group fix
select
max(Customer_Number) as Customer_Number,
max(Agreement_Number)as Agreement_Number,
max(Agreement_Type) as Agreement_Type,
sum(Amount) as Amount,
max(Currency_code) as Currency_code,
max(Revenue_Limit_Flag) as Revenue_Limit_Flag,
max(Invoice_Limit_Flag) as Invoice_Limit_Flag,
max(Administrator_Employee_Reference) as Administrator_Employee_Reference,
Description,
max(Date_Expiration) as Date_Expiration,
max(Terms) as Terms,
max(OU_NAME) as OU_NAME,
max(COUNTRY) as [COUNTRY],
max(OBSERVATION) as [OBSERVATION],
max(SIGLA_PRIMARY_KEY) as [SIGLA_PRIMARY_KEY],
max(LAST_UPDATED) as LAST_UPDATED
from #tmpAgreement
group by Description
order by Description

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

RETURN
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('$(targetCountryCode)', 'script', 'updated populateProjectAgreementHeaderFor$(targetCountryCode) procedure definition', GETDATE());