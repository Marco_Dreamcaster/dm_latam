IF OBJECT_ID('O_STG_PROJECT_BUDGET_HEADER') IS NOT NULL DROP TABLE O_STG_PROJECT_BUDGET_HEADER

CREATE TABLE O_STG_PROJECT_BUDGET_HEADER(
	Project_Name VARCHAR(30),
	Budget_Type VARCHAR(30),
	Version_Name VARCHAR(30),
	Description VARCHAR(255),
	Entry_Method VARCHAR(30),
	Resource_List_Name VARCHAR(60),
	Version_Date VARCHAR(10),
	Operating_Unit_Name VARCHAR(240),
	[COUNTRY] [nvarchar](2) NOT NULL,
	[OBSERVATION] [nvarchar](3000) NULL,
	[SIGLA_PRIMARY_KEY] [nvarchar](60) NULL,
	[LAST_UPDATED] [datetime2](6) NOT NULL
)

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated O_STG_PROJECT_BUDGET_HEADER table definition', GETDATE());
