:setvar targetCountryCode CO
--:setvar targetCountryCode MX

IF OBJECT_ID('DBO.generateProjectBudgetHeaderFor$(targetCountryCode)', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateProjectBudgetHeaderFor$(targetCountryCode);

GO
CREATE PROCEDURE dbo.generateProjectBudgetHeaderFor$(targetCountryCode)
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

EXEC dbo.create_O_STG_PROJECT_costoLocalFor$(targetCountryCode)
EXEC dbo.create_O_STG_PROJECT_conceptoimputacionFor$(targetCountryCode)

select
e.PROJECT_NAME as Project_Name,
'Approved Cost Budget' as Budget_Type,
'ORIGINAL' as Version_Name,
'Original Budget' as Description,
'COST-BY EXP TYPE' as Entry_Method,
'$(targetCountryCode)-COSTO DE CONSTRUCCION' as Resource_List_Name,
convert(varchar(10),a.REGISTRO,105) as Version_Date,
'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME
from TK$(targetCountryCode)_LATEST..contrato as a
inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) = e.SIGLA_PRIMARY_KEY
where
a.CONTRATO in (select [COD OBRA] from contratosPOC)
union all
select
e.PROJECT_NAME as Project_Name,
'Approved Revenue Budget' as Budget_Type,
'ORIGINAL' as Version_Name,
'Original Budget' as Description,
'REVENUE-TOP TASKS' as Entry_Method,
'$(targetCountryCode)-INGRESOS DE CONSTRUCCION' as Resource_List_Name,
convert(varchar(10),a.registro,105) as Version_Date,
'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME
from TK$(targetCountryCode)_LATEST..contrato as a
inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) = e.SIGLA_PRIMARY_KEY
WHERE
e.[COUNTRY]='$(targetCountryCode)' AND
e.[TEMPLATE_NAME]<>'SERV TEMPLATE $(targetCountryCode)' AND
e.[DISMISSED]=0 AND
e.[LOCAL_VALIDATED]=1 AND
e.[CROSSREF_VALIDATED]=1

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

RETURN
END

GO
INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('$(targetCountryCode)', 'script', 'updated generateProjectBudgetHeaderForPA procedure definition', GETDATE());