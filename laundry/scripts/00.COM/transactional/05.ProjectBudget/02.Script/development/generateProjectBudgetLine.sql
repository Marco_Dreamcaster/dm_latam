:setvar targetCountryCode CO
--:setvar targetCountryCode MX

IF OBJECT_ID('DBO.generateProjectBudgetLineFor$(targetCountryCode)', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateProjectBudgetLineFor$(targetCountryCode);

GO
CREATE PROCEDURE dbo.generateProjectBudgetLineFor$(targetCountryCode)
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

EXEC dbo.create_O_STG_PROJECT_conceptoimputacionFor$(targetCountryCode)
EXEC dbo.create_O_STG_PROJECT_costoLocalFor$(targetCountryCode)
EXEC dbo.create_O_STG_PROJECT_tipcosFor$(targetCountryCode)

select
a.CONTRATO,
e.PROJECT_NAME as Project_Name,
case
when isnull(j.CLASIFICACION,h.CLASIFICACION)='LABOR' then LEFT('$(targetCountryCode)_' + CAST (c.IdEquipo AS VARCHAR(max)),20)  +'.01'+'.01'
when isnull(j.CLASIFICACION,h.CLASIFICACION)='MATERIAL' then LEFT('$(targetCountryCode)_' + CAST (c.IdEquipo AS VARCHAR(max)),20)  +'.02'
when isnull(j.CLASIFICACION,h.CLASIFICACION)='MISCELLANEOUS' then LEFT('$(targetCountryCode)_' + CAST (c.IdEquipo AS VARCHAR(max)),20)  +'.03'
when isnull(j.CLASIFICACION,h.CLASIFICACION)='SUBCONTRACT' then LEFT('$(targetCountryCode)_' + CAST (c.IdEquipo AS VARCHAR(max)),20)  +'.03'
end	as Task_Number,
'$(targetCountryCode)-COSTO DE CONSTRUCCION' as Resource_List,
isnull(j.expenditure,h.expenditure) as Resource_List_Member,
--g.CTA,g.TIPCOS,g.CONIMPUTACION,i.FECHA,i.DIARIO,i.CONTROL,i.GLOSA,i.MES,i.PERIODO, i.FECHAREG,
round((g.DEBE-g.haber) / f.cantequipos,2) as Raw_Cost,
round((g.DEBE-g.haber) / f.cantequipos,2) as Burdened_Cost,
'' as Revenue,
case
when isnull(j.CLASIFICACION,h.CLASIFICACION) ='LABOR' then round(((g.DEBE-g.haber) / 11) / f.cantequipos, 2) else ''
end as Quantity,
'' as NIM_DURATION,
'ORIGINAL' as Version_name,
convert(varchar(10),g.fecha, 105) as Version_date,
'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME
into #tmpBudgetLine
from TK$(targetCountryCode)_LATEST..contrato as a
LEFT join TK$(targetCountryCode)_LATEST..DETCONT as b on a.CONTRATO=b.CONTRATO
LEFT join TK$(targetCountryCode)_LATEST..EQUIPOS as c on b.IdEquipo=c.IdEquipo
inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) = e.SIGLA_PRIMARY_KEY
inner join (select COUNT(idequipo) as cantequipos,contrato from TK$(targetCountryCode)_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
inner join TK$(targetCountryCode)_LATEST..COM as g on a.contrato = g.CONTRATO
inner join TK$(targetCountryCode)_LATEST..TITCOM as i on g.IdComp=i.IdComp
inner join ##O_STG_PROJECT_tipcos as h on g.TIPCOS=h.TIPCOS
left join ##O_STG_PROJECT_conceptoimputacion as j on g.CONIMPUTACION=j.conimputacion
where
--a.CONTRATO='001-10-14' and
--a.CONTRATO='585-115' and
g.CTA in ('41101001','41101002','41104001','41104002','41107001','41107002','41107003','41107004','41107005','41107006')
and a.contrato in
(select [COD OBRA] from contratosPOC )
and g.fecha <= convert(datetime, '31-10-2018', 105)
and g.TIPCOS <> 'O05'
union all
/* Inicio Excel POC */
/* MATERIAL IMPORTADO-$(targetCountryCode)	= Costo Materiales + Costo Mat Adicionales*/
select 	distinct
a.CONTRATO,
c.PROJECT_NAME as Project_Name,
LEFT('$(targetCountryCode)_' + CAST (e.IdEquipo AS VARCHAR(max)),20)  +'.02'
as Task_Number,
'$(targetCountryCode)-COSTO DE CONSTRUCCION' as Resource_List,
'MATERIAL IMPORTADO-$(targetCountryCode)' as Resource_List_Member,
round((b.[Costo Materiales]+[Costo Mat Adicionales]) / f.cantequipos ,2) as Raw_Cost,
round((b.[Costo Materiales]+[Costo Mat Adicionales]) / f.cantequipos ,2) as Burdened_Cost,
'' as Revenue,
'' as Quantity,
'' as NIM_DURATION,
'ORIGINAL' as Version_name,
convert(varchar(10),a.registro, 105) as Version_date,
'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME
from  TK$(targetCountryCode)_LATEST..CONTRATO as a
inner join contratosPOC as b on a.CONTRATO=b.[COD OBRA]
inner join O_STG_PROJECT_HEADER as c on dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) = c.SIGLA_PRIMARY_KEY
inner join TK$(targetCountryCode)_LATEST..DETCONT as d on a.CONTRATO=d.CONTRATO
inner join TK$(targetCountryCode)_LATEST..EQUIPOS as e on d.IdEquipo=e.IdEquipo
inner join (select COUNT(idequipo) as cantequipos,contrato from TK$(targetCountryCode)_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
where a.CONTRATO in (select [COD OBRA] from contratosPOC)
--and a.CONTRATO='585-115'
union all
/* - FLETE DE MATERIALES-$(targetCountryCode) = Flete*/
select 	distinct
a.CONTRATO,
c.PROJECT_NAME as Project_Name,
LEFT('$(targetCountryCode)_' + CAST (e.IdEquipo AS VARCHAR(max)),20)  +'.03'
as Task_Number,
'$(targetCountryCode)-COSTO DE CONSTRUCCION' as Resource_List,
'FLETE DE MATERIALES-$(targetCountryCode)' as Resource_List_Member,
round((b.Flete) / f.cantequipos ,2) as Raw_Cost,
round((b.Flete) / f.cantequipos ,2) as Burdened_Cost,
'' as Revenue,
'' as Quantity,
'' as NIM_DURATION,
'ORIGINAL' as Version_name,
convert(varchar(10),a.registro, 105) as Version_date,
'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME
from  TK$(targetCountryCode)_LATEST..CONTRATO as a
inner join contratosPOC as b on a.CONTRATO=b.[COD OBRA]
inner join O_STG_PROJECT_HEADER as c on dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) = c.SIGLA_PRIMARY_KEY
inner join TK$(targetCountryCode)_LATEST..DETCONT as d on a.CONTRATO=d.CONTRATO
inner join TK$(targetCountryCode)_LATEST..EQUIPOS as e on d.IdEquipo=e.IdEquipo
inner join (select COUNT(idequipo) as cantequipos,contrato from TK$(targetCountryCode)_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
where a.CONTRATO in (select [COD OBRA] from contratosPOC)
--and a.CONTRATO='585-115'
union all
/* NACIONALIZACION-$(targetCountryCode) = Importacion*/
select 	distinct
a.CONTRATO,
c.PROJECT_NAME as Project_Name,
LEFT('$(targetCountryCode)_' + CAST (e.IdEquipo AS VARCHAR(max)),20)  +'.03'
as Task_Number,
'$(targetCountryCode)-COSTO DE CONSTRUCCION' as Resource_List,
'NACIONALIZACION-$(targetCountryCode)' as Resource_List_Member,
round((b.Importacion) / f.cantequipos ,2) as Raw_Cost,
round((b.Importacion) / f.cantequipos ,2) as Burdened_Cost,
'' as Revenue,
'' as Quantity,
'' as NIM_DURATION,
'ORIGINAL' as Version_name,
convert(varchar(10),a.registro, 105) as Version_date,
'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME
from  TK$(targetCountryCode)_LATEST..CONTRATO as a
inner join contratosPOC as b on a.CONTRATO=b.[COD OBRA]
inner join O_STG_PROJECT_HEADER as c on dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) = c.SIGLA_PRIMARY_KEY
inner join TK$(targetCountryCode)_LATEST..DETCONT as d on a.CONTRATO=d.CONTRATO
inner join TK$(targetCountryCode)_LATEST..EQUIPOS as e on d.IdEquipo=e.IdEquipo
inner join (select COUNT(idequipo) as cantequipos,contrato from TK$(targetCountryCode)_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
where a.CONTRATO in (select [COD OBRA] from contratosPOC)
--and a.CONTRATO='001-10-14'
--and a.CONTRATO='585-115'
union all
/* HORAS REGULARES-$(targetCountryCode) = Costo Mano de Obra Instalacion + Costo MdeO Adicional*/
select 	distinct
a.CONTRATO,
c.PROJECT_NAME as Project_Name,
LEFT('$(targetCountryCode)_' + CAST (e.IdEquipo AS VARCHAR(max)),20)  +'.01'+'.01'
as Task_Number,
'$(targetCountryCode)-COSTO DE CONSTRUCCION' as Resource_List,
'HORAS REGULARES-$(targetCountryCode)' as Resource_List_Member,
round(((b.[Costo Mano de Obra Instalacion]+b.[Costo MdeO Adicional])) / f.cantequipos ,2) as Raw_Cost,
round(((b.[Costo Mano de Obra Instalacion]+b.[Costo MdeO Adicional])) / f.cantequipos ,2) as Burdened_Cost,
'' as Revenue,
round(((b.[Costo Mano de Obra Instalacion]+b.[Costo MdeO Adicional]) / 11) / f.cantequipos ,2) as Quantity,
'' as NIM_DURATION,
'ORIGINAL' as Version_name,
convert(varchar(10),a.registro, 105) as Version_date,
'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME
from  TK$(targetCountryCode)_LATEST..CONTRATO as a
inner join contratosPOC as b on a.CONTRATO=b.[COD OBRA]
inner join O_STG_PROJECT_HEADER as c on dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) = c.SIGLA_PRIMARY_KEY
inner join TK$(targetCountryCode)_LATEST..DETCONT as d on a.CONTRATO=d.CONTRATO
inner join TK$(targetCountryCode)_LATEST..EQUIPOS as e on d.IdEquipo=e.IdEquipo
inner join (select COUNT(idequipo) as cantequipos,contrato from TK$(targetCountryCode)_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
where a.CONTRATO in (select [COD OBRA] from contratosPOC)
--and a.CONTRATO='001-10-14'
--and a.CONTRATO='585-115'
union all
/* ADICIONALES INSTALACION-$(targetCountryCode) = Locales y varios + Loc y varios Adicionales + Estructura Montaje*/
select 	distinct
a.CONTRATO,
c.PROJECT_NAME as Project_Name,
LEFT('$(targetCountryCode)_' + CAST (e.IdEquipo AS VARCHAR(max)),20)  +'.03'
as Task_Number,
'$(targetCountryCode)-COSTO DE CONSTRUCCION' as Resource_List,
'ADICIONALES DE INSTALACION-$(targetCountryCode)' as Resource_List_Member,
round((b.[Locales y varios]+b.[Loc y varios Adicionales]+b.[Estructura Montaje]) / f.cantequipos ,2) as Raw_Cost,
round((b.[Locales y varios]+b.[Loc y varios Adicionales]+b.[Estructura Montaje]) / f.cantequipos ,2) as Burdened_Cost,
'' as Revenue,
'' as Quantity,
'' as NIM_DURATION,
'ORIGINAL' as Version_name,
convert(varchar(10),a.registro, 105) as Version_date,
'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME
from  TK$(targetCountryCode)_LATEST..CONTRATO as a
inner join contratosPOC as b on a.CONTRATO=b.[COD OBRA]
inner join O_STG_PROJECT_HEADER as c on dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) = c.SIGLA_PRIMARY_KEY
inner join TK$(targetCountryCode)_LATEST..DETCONT as d on a.CONTRATO=d.CONTRATO
inner join TK$(targetCountryCode)_LATEST..EQUIPOS as e on d.IdEquipo=e.IdEquipo
inner join (select COUNT(idequipo) as cantequipos,contrato from TK$(targetCountryCode)_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
where a.CONTRATO in (select [COD OBRA] from contratosPOC)
--and a.CONTRATO='001-10-14'
--and a.CONTRATO='585-115'
union all
/*Cuentas 3110*/
select
a.CONTRATO,
e.PROJECT_NAME as Project_Name,
LEFT('$(targetCountryCode)_' + CAST (c.IdEquipo AS VARCHAR(max)),20)  AS TASK_NUMBER,
'$(targetCountryCode)-INGRESOS DE CONSTRUCCION' as Resource_List,
'CONSTRUCTION REVENUE' as Resource_List_Member,
'' as Raw_Cost,
'' as Burdened_Cost,
round((g.haber-g.debe) / f.cantequipos,2) as Revenue,
'' as Quantity,
'' as NIM_DURATION,
'ORIGINAL' as Version_name,
convert(varchar(10),g.fecha, 105) as Version_date,
'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME
--into #tmp
from TK$(targetCountryCode)_LATEST..contrato as a
LEFT join TK$(targetCountryCode)_LATEST..DETCONT as b on a.CONTRATO=b.CONTRATO
LEFT join TK$(targetCountryCode)_LATEST..EQUIPOS as c on b.IdEquipo=c.IdEquipo
inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) = e.SIGLA_PRIMARY_KEY
inner join (select COUNT(idequipo) as cantequipos,contrato from TK$(targetCountryCode)_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
inner join TK$(targetCountryCode)_LATEST..COM as g on a.contrato = g.CONTRATO
inner join TK$(targetCountryCode)_LATEST..TITCOM as i on g.IdComp=i.IdComp
where
--a.CONTRATO='001-10-14' and
-- a.CONTRATO='585-115' and
g.CTA in ('31101001','31101002','31101003','31101004','31101005','31101006','31101007','31101008','31101030','31102','31102001','31102002','31102003','31102004','31102006','31102007','31102008')and
a.contrato in
(select [COD OBRA] from contratosPOC )
and g.fecha <= convert(datetime, '31-10-2018', 105)
/*Excel POC Ingresos*/
union all
select 	distinct
a.CONTRATO,
c.PROJECT_NAME as Project_Name,
LEFT('$(targetCountryCode)_' + CAST (e.IdEquipo AS VARCHAR(max)),20)  AS TASK_NUMBER,
'$(targetCountryCode)-INGRESOS DE CONSTRUCCION' as Resource_List,
'CONSTRUCTION REVENUE' as Resource_List_Member,
'' as Raw_Cost,
'' as Burdened_Cost,
round((b.[TOT CONTRATO]) / f.cantequipos ,2) as Revenue,
'' as Quantity,
'' as NIM_DURATION,
'ORIGINAL' as Version_name,
convert(varchar(10),a.registro, 105) as Version_date,
'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME
from  TK$(targetCountryCode)_LATEST..CONTRATO as a
inner join contratosPOC as b on a.CONTRATO=b.[COD OBRA]
inner join O_STG_PROJECT_HEADER as c on dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) = c.SIGLA_PRIMARY_KEY
inner join TK$(targetCountryCode)_LATEST..DETCONT as d on a.CONTRATO=d.CONTRATO
inner join TK$(targetCountryCode)_LATEST..EQUIPOS as e on d.IdEquipo=e.IdEquipo
inner join (select COUNT(idequipo) as cantequipos,contrato from TK$(targetCountryCode)_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
where a.CONTRATO in (select [COD OBRA] from contratosPOC)
union all
select 	distinct
a.CONTRATO,
c.PROJECT_NAME as Project_Name,
LEFT('$(targetCountryCode)_' + CAST (e.IdEquipo AS VARCHAR(max)),20)  AS TASK_NUMBER,
'$(targetCountryCode)-INGRESOS DE CONSTRUCCION' as Resource_List,
'NIM' as Resource_List_Member,
'' as Raw_Cost,
'' as Burdened_Cost,
round((b.[Valor de manto Gratuito]) / f.cantequipos ,2) as Revenue,
'' as Quantity,
'' as NIM_DURATION,
'ORIGINAL' as Version_name,
convert(varchar(10),a.registro, 105) as Version_date,
'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME
from  TK$(targetCountryCode)_LATEST..CONTRATO as a
inner join contratosPOC as b on a.CONTRATO=b.[COD OBRA]
inner join O_STG_PROJECT_HEADER as c on dbo.udf_EncodeProjectReference('$(targetCountryCode)', a.CONTRATO) = c.SIGLA_PRIMARY_KEY
inner join TK$(targetCountryCode)_LATEST..DETCONT as d on a.CONTRATO=d.CONTRATO
inner join TK$(targetCountryCode)_LATEST..EQUIPOS as e on d.IdEquipo=e.IdEquipo
inner join (select COUNT(idequipo) as cantequipos,contrato from TK$(targetCountryCode)_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
where a.CONTRATO in (select [COD OBRA] from contratosPOC)
--and a.CONTRATO='001-10-14'
select
contrato,
max(Project_Name) Project_Name,
Task_Number,
max(Resource_List) Resource_List,
Resource_List_Member,
sum(Raw_Cost) Raw_Cost,
sum(Burdened_Cost) Burdened_Cost,
sum(Revenue) Revenue,
sum(Quantity) Quantity,
max(NIM_DURATION) NIM_DURATION,
max(Version_name) Version_name,
max(Version_date) Version_date,
max(OU_NAME)OU_NAME
into #tmpBudgetLine2
from #tmpBudgetLine
--where CONTRATO=
group by CONTRATO, Task_Number,Resource_List_Member
/*INICIO UPDATE*/
UPDATE #tmpBudgetLine2 SET QUANTITY = ROUND(Raw_Cost/11,2) from #tmpBudgetLine2 where Resource_List_Member = 'HORAS REGULARES-$(targetCountryCode)'
/*FIN UPDATE*/
select
Project_Name,
Task_Number,
Resource_List,
Resource_List_Member,
--Raw_Cost,
case when cast(Raw_Cost as varchar (30)) = '0' then '' else cast(Raw_Cost as varchar (30)) end as Raw_Cost,
--Burdened_Cost,
case when cast(Burdened_Cost as varchar (30)) = '0' then '' else cast(Burdened_Cost as varchar (30)) end as Burdened_Cost,
case when cast(Revenue as varchar (30)) = '0' then '' else ltrim(str(Revenue,30,2)) end as Revenue,
--Quantity,
case when cast(Quantity as varchar (30)) = '0' then '' else cast(Quantity as varchar (30)) end as Quantity,
--NIM_DURATION,
case when cast(NIM_DURATION as varchar (30)) = '0' then '' else cast(NIM_DURATION as varchar (30)) end as NIM_DURATION,
Version_name,
Version_date,
OU_NAME
from #tmpBudgetLine2
where
contrato in
(select [COD OBRA] from contratosPOC )
and (Raw_Cost <> 0 and Resource_List='$(targetCountryCode)-COSTO DE CONSTRUCCION') or (Revenue <> 0 and Resource_List='$(targetCountryCode)-INGRESOS DE CONSTRUCCION')
order by Resource_List,project_name,task_number

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

RETURN
END

GO
INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('$(targetCountryCode)', 'script', 'updated generateProjectBudgetLineFor$(targetCountryCode) procedure definition', GETDATE());