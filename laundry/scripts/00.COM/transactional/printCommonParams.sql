DECLARE @cutoffDate DATE;
SET @cutoffDate=CONVERT(date,'$(cutoffDate)',103)

DECLARE @snapshotDate DATE;
SET @snapshotDate=dbo.udf_EncodeSnapshotDate('$(country)')

PRINT 'country = $(country)' + CHAR(13)+CHAR(10);
PRINT 'cutoffDate = $(cutoffDate)' +  CHAR(13)+CHAR(10);
PRINT 'snapshotDate = ' + CONVERT(varchar, @snapshotDate, 103) + CHAR(13)+CHAR(10);
