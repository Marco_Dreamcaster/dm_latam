-- 1. set up country variables
--
:setvar targetCountryCode PA

:setvar targetDatabaseName "DM_LATAM"           -- PRODUCTION
--:setvar targetDatabaseName "DM_LATAM_DEV"		-- DEV

:setvar sourceDatabaseName "TKPA_LATEST"
:setvar fiscalClassCode "TKE_LATAM_PA_CONTRIBUTOR"
:setvar timezoneName America/Panama
:setvar orgName (PANAMA)


-- 2. include common procedures
--

:r "../../../00.COM/master/01.Customers/load_CustomersData.sql"

EXEC $(procMain)$(targetCountryCode)

UPDATE O_PRE_CUSTOMER_HEADER
SET RECOMMENDED=0
WHERE COUNTRY='$(targetCountryCode)';



-- Customers from mantenimento
--
UPDATE O_PRE_CUSTOMER_HEADER
SET RECOMMENDED=1
WHERE ORIG_SYSTEM_CUSTOMER_REF COLLATE DATABASE_DEFAULT IN
(
select

DISTINCT '$(targetCountryCode)' + RTRIM(LTRIM(CAST(a.CLIENTE AS varchar(30)))) + '-CC' AS ORIG_SYSTEM_CUSTOMER_REF

from $(sourceDatabaseName).dbo.manten as a

      inner join $(sourceDatabaseName).dbo.titrenovac as b on a.contrato = b.contrato and b.RENOVACION=(select top 1 RENOVACION from $(sourceDatabaseName).dbo.TITRENOVAC where CONTRATO=a.CONTRATO order by RENOVACION desc)

      left join $(sourceDatabaseName).dbo.USUARIOS as c on a.USERVENDEDOR=c.USUARIO

      inner join $(sourceDatabaseName).dbo.DETTABAUX as d on a.CLASIFICACION=d.Id and d.TABLA=6

      left join (select MAX(fecha) as fecha, contrato, max(ltrim(rtrim(glosa))) as glosa from $(sourceDatabaseName).dbo.ObservacionesContrato group by contrato ) as e on a.CONTRATO=e.contrato

      inner join $(sourceDatabaseName).dbo.PAGOS as f on a.IDPAGO=f.IdPago

      left join (select max(Fcancel) as FCancel,codcont from $(sourceDatabaseName).dbo.CabCancelContMant group by codcont) as h     on a.CONTRATO=h.CodCont

where h.FCancel is null

)


-- Customers from obras en curso
--

UPDATE O_PRE_CUSTOMER_HEADER
SET RECOMMENDED=1
WHERE ORIG_SYSTEM_CUSTOMER_REF COLLATE DATABASE_DEFAULT IN
(
select
DISTINCT '$(targetCountryCode)' + RTRIM(LTRIM(CAST(contrato.CLIENTE AS varchar(30)))) + '-CC' AS ORIG_SYSTEM_CUSTOMER_REF

from $(sourceDatabaseName).dbo.CONTRATO

where

contrato in

(

'001-10-14',

'01/10/14-a',

'564-4-15',

'584-1115',

'585-115',

'587-1115',

'610-0716',

'612-0716',

'616-0816',

'617-0816',

'702-1016',

'703-1016',

'704-1016',

'712-0217',

'714-0417',

'716-517',

'719-0617',

'721-0717',

'722-0817',

'726-1117',

'75-1016',

'CT244-12',

'CT-246-12',

'CT-247-12',

'CT-248-12',

'CT-274-13A',

'CT-299-13',

'CT-301-13',

'CT-442-14',

'CT580-1015',

'L-0603',

'L-0607',

'L-0732',

'L-0767',

'L-0792',

'L-0922',

'L-0926',

'L-0927',

'L-0938',

'L-0950',

'L-0965',

'L-0967',

'L-0970',

'L-0979',

'L-0991',

'L-0992',

'L-1001',

'L-10017',

'L-1004',

'L-1006',

'L-1016',

'L-1020',

'L-1022',

'L-1026',

'L-1041',

'L-1042 ',

'L-1044',

'L-1053',

'L-1070',

'L-1072',

'L-1073',

'L-1075',

'L-1076',

'L-1088',

'L-1089',

'L-1093',

'L-1094',

'L-1095',

'L-1107',

'L-1112',

'L-1116',

'L-1151',

'L-1161',

'L-1162',

'L-1165',

'L-1169',

'L-1182',

'L-1183',

'L-1184',

'L-1187',

'L-1188',

'L-1192',

'L-1200',

'L-1201',

'L-1202',

'L-1208',

'L-1210',

'L-1213',

'L-1218',

'L-1219',

'L-1220',

'L-1228',

'L-1240',

'L-1248',

'L-1249',

'L-1253',

'L-1256',

'L-1260',

'L-1265',

'L-1266',

'L-1272',

'L-1273',

'L-1279',

'L-0887',

'M741-0218',

'MO593-0216',

'N-709-1216',

'N-720-0717',

'N-727-1217',

'N729-1217',

'N730-1217',

'N-731-1217',

'N732-0118',

'N740-0118',

'N-742-0318',

'N-743-0318',

'N-744-0318',

'ON601-0516',

'ON-604-071',

'ON-607-071'

)

)

SELECT COUNT(*) AS CUSTOMER_COUNT, RECOMMENDED, COUNTRY FROM $(targetDatabaseName).dbo.O_PRE_CUSTOMER_HEADER GROUP BY COUNTRY, RECOMMENDED


UPDATE O_PRE_CUSTOMER_HEADER SET PAYMENT_METHOD_NAME='PA - BAC MANUAL - USD' WHERE COUNTRY='PA'
UPDATE O_PRE_CUSTOMER_ADDRESS SET PAYMENT_METHOD_NAME='PA - BAC MANUAL - USD' WHERE COUNTRY='PA'

