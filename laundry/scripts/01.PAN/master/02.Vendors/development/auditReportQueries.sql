EXEC dbo.generateVendorHeaderAuditByCountry 'PA'
 
/**

SELECT
	a.SEGMENT1 AS SIGLA_REF,
	a.CLEANSED AS CLEANSED,
	a.RECOMMENDED AS RFM,
	b.DISMISSED AS DISMISSED,
	b.CROSSREF_VALIDATED AS VALIDATED,
	a.VENDOR_NAME AS VENDOR_NAME_SIGLA,
	b.VENDOR_NAME AS VENDOR_NAME_ORACLE,
	b.*
FROM O_PRE_VENDOR_HEADER a
	LEFT JOIN O_STG_VENDOR_HEADER b ON a.SEGMENT1=b.SEGMENT1
WHERE
	a.COUNTRY='PA'
ORDER BY
	a.CLEANSED DESC,
	a.RECOMMENDED DESC,
	b.DISMISSED ASC,
	b.CROSSREF_VALIDATED DESC,
	a.SEGMENT1 ASC

SELECT
	a.SEGMENT1 AS SIGLA_REF,
	a.CLEANSED AS CLEANSED,
	a.RECOMMENDED AS RFM,
	b.DISMISSED AS DISMISSED,
	b.CROSSREF_VALIDATED AS VALIDATED,
	a.VENDOR_NAME AS VENDOR_NAME_SIGLA,
	b.VENDOR_NAME AS VENDOR_NAME_ORACLE,
	c.*
FROM O_PRE_VENDOR_HEADER a
	LEFT JOIN O_STG_VENDOR_HEADER b ON a.SEGMENT1=b.SEGMENT1
	LEFT JOIN O_STG_VENDOR_SITE c ON a.SEGMENT1=c.VENDOR_NUMBER
WHERE
	a.COUNTRY='PA'
ORDER BY
	a.CLEANSED DESC,
	a.RECOMMENDED DESC,
	b.DISMISSED ASC,
	b.CROSSREF_VALIDATED DESC,
	a.SEGMENT1 ASC

SELECT
	a.SEGMENT1,
	a.RECOMMENDED AS RFM,
	a.CLEANSED,
	b.DISMISSED,
	a.VENDOR_NAME AS VENDOR_NAME_SIGLA,
	b.VENDOR_NAME AS VENDOR_NAME_ORACLE,
	c.*
FROM O_PRE_VENDOR_HEADER a
	LEFT JOIN O_STG_VENDOR_HEADER b ON a.SEGMENT1=b.SEGMENT1
	LEFT JOIN O_STG_VENDOR_SITE_CONTACT c ON a.SEGMENT1=c.VENDOR_NUMBER
WHERE
	a.COUNTRY='PA'
ORDER BY
	a.CLEANSED DESC,
	a.RECOMMENDED DESC,
	b.DISMISSED ASC,
	b.CROSSREF_VALIDATED DESC,
	a.SEGMENT1 ASC

SELECT
	a.SEGMENT1,
	a.RECOMMENDED AS RFM,
	a.CLEANSED,
	b.DISMISSED,
	a.VENDOR_NAME AS VENDOR_NAME_SIGLA,
	b.VENDOR_NAME AS VENDOR_NAME_ORACLE,
	c.*
FROM O_PRE_VENDOR_HEADER a
	LEFT JOIN O_STG_VENDOR_HEADER b ON a.SEGMENT1=b.SEGMENT1
	LEFT JOIN O_STG_VENDOR_BANK_ACCOUNT c ON a.SEGMENT1=c.VENDOR_NUMBER
WHERE
	a.COUNTRY='PA'
ORDER BY
	a.CLEANSED DESC,
	a.RECOMMENDED DESC,
	b.DISMISSED ASC,
	b.CROSSREF_VALIDATED DESC,
	a.SEGMENT1 ASC

**/