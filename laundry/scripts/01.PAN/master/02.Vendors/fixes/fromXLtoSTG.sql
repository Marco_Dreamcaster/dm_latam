DELETE FROM  O_STG_VENDOR_HEADER WHERE COUNTRY='PA';

INSERT INTO O_STG_VENDOR_HEADER
(
      [ALIASES]
      ,[RECOMMENDED]
      ,[LOCAL_VALIDATED]
      ,[CROSSREF_VALIDATED]
      ,[ACTION]
      ,[CLEANSED]
      ,[COUNTRY]
      ,[OBSERVATION]
      ,[DISMISSED]

      ,[VENDOR_NAME]
      ,[SEGMENT1]
      ,[VENDOR_NAME_ALT]
      ,[VENDOR_TYPE_LOOKUP_CODE]
      ,[_8ID]
      ,[VAT_REGISTRATION_NUM]
      ,[NUM_1099]
      ,[FEDERAL_REPORTABLE_FLAG]
      ,[TYPE_1099]
      ,[STATE_REPORTABLE_FLAG]
      ,[ALLOW_AWT_FLAG]
      ,[MATCH_OPTION_LEVEL]
      ,[PAYMENT_METHOD_LOOKUP_CODE]
      ,[GLOBAL_FLAG]
      ,[SENSITIVE_VENDOR]
      ,[GLOBAL_ATTRIBUTE1]
      ,[GLOBAL_ATTRIBUTE8]
      ,[GLOBAL_ATTRIBUTE9]
      ,[GLOBAL_ATTRIBUTE10]
      ,[GLOBAL_ATTRIBUTE12]
      ,[GLOBAL_ATTRIBUTE15]
      ,[GLOBAL_ATTRIBUTE16]
      ,[ATTRIBUTE_CATEGORY]
      ,[ATTRIBUTE1]
      ,[ATTRIBUTE2]
      ,[ATTRIBUTE3]
      ,[ATTRIBUTE4]
      ,[ATTRIBUTE5]
      ,[ATTRIBUTE6]
      ,[ATTRIBUTE7]
      ,[ATTRIBUTE8]
      ,[ATTRIBUTE9]
      ,[ATTRIBUTE10]

      ,[ORG_NAME]
)
SELECT
	[Column39] AS ALIASES
	, 1 AS [RECOMMENDED]
	, 0 AS [LOCAL_VALIDATED]
	, 0 AS [CROSSREF_VALIDATED]
	, '' AS [ACTION]
	, 1 AS CLEANSED
	, 'PA' AS COUNTRY
	, '' AS [OBSERVATION]
	, 0 AS DISMISSED

    ,[VENDOR_NAME]
    ,[SEGMENT1]
    ,[VENDOR_NAME_ALT]
    ,[VENDOR_TYPE_LOOKUP_CODE]
    ,[8ID]
    ,[VAT_REGISTRATION_NUM]
    ,[NUM_1099]
	,CASE [FEDERAL_REPORTABLE_FLAG]  
		WHEN 'Y' THEN 1  
		WHEN 'N' THEN 0
		ELSE 0
	END
    ,[TYPE_1099]
	,CASE [STATE_REPORTABLE_FLAG]  
		WHEN 'Y' THEN 1  
		WHEN 'N' THEN 0
		ELSE 0
	END
	,CASE [ALLOW_AWT_FLAG]  
		WHEN 'Y' THEN 1  
		WHEN 'N' THEN 0
		ELSE 0
	END
    ,[MATCH_OPTION_LEVEL]
    ,[PAYMENT_METHOD_LOOKUP_CODE]
	,CASE [GLOBAL_FLAG]  
		WHEN 'Y' THEN 1  
		WHEN 'N' THEN 0
		ELSE 0
	END
	,CASE [SENSITIVE_VENDOR]  
		WHEN 'Y' THEN 1  
		WHEN 'N' THEN 0
		ELSE 0
	END
    ,[GLOBAL_ATTRIBUTE1]
    ,[GLOBAL_ATTRIBUTE8]
    ,[GLOBAL_ATTRIBUTE9]
    ,[GLOBAL_ATTRIBUTE10]
    ,[GLOBAL_ATTRIBUTE12]
    ,[GLOBAL_ATTRIBUTE15]
    ,[GLOBAL_ATTRIBUTE16]
    ,[ATTRIBUTE_CATEGORY]
    ,[ATTRIBUTE1]
    ,[ATTRIBUTE2]
    ,[ATTRIBUTE3]
    ,[ATTRIBUTE4]
    ,[ATTRIBUTE5]
    ,[ATTRIBUTE6]
    ,[ATTRIBUTE7]
    ,[ATTRIBUTE8]
    ,[ATTRIBUTE9]
    ,[ATTRIBUTE10]

	,'THYSSENKRUPP ELEVADORES S.A. (PANAMA)'
	
  FROM DM_LATAM.dbo.O_XL_PA_VENDOR_HEADER

DELETE FROM  O_STG_VENDOR_SITE WHERE COUNTRY='PA';

INSERT INTO O_STG_VENDOR_SITE
(
      [RECOMMENDED]
      ,[LOCAL_VALIDATED]
      ,[CROSSREF_VALIDATED]
      ,[ACTION]
      ,[CLEANSED]
      ,[COUNTRY]
      ,[OBSERVATION]
      ,[DISMISSED]

      ,[VENDOR_NUMBER]
      ,[VENDOR_SITE_CODE]
      ,[VENDOR_SITE_CODE_ALT]
      ,[ADDRESS_LINE1]
      ,[ADDRESS_LINE2]
      ,[ADDRESS_LINE3]
      ,[ADDRESS_LINE4]
      ,[ADDRESS_LINE_ALT]
      ,[CITY]
      ,[COUNTY]
      ,[STATE]
      ,[PROVINCE]
      ,[ZIP]
      ,[ADDRESS_COUNTRY]
      ,[AREA_CODE]
      ,[PHONE]
      ,[FAX_AREA_CODE]
      ,[FAX]
      ,[PAYMENT_METHOD_LOOKUP_CODE]
      ,[VAT_CODE]
      ,[ACCTS_PAY_ACCOUNT]
      ,[PREPAY_ACCOUNT]
      ,[PAY_GROUP_LOOKUP_CODE]
      ,[INVOICE_CURRENCY_CODE]
      ,[LANGUAGE]
      ,[TERMS_NAME]
      ,[EMAIL_ADDRESS]
      ,[PURCHASING_SITE_FLAG]
      ,[PAY_SITE_FLAG]
      ,[VAT_REGISTRATION_NUM]
      ,[SHIP_TO_LOCATION_CODE]
      ,[PRIMARY_PAY_SITE_FLAG]
      ,[GAPLESS_INV_NUM_FLAG]
      ,[OFFSET_TAX_FLAG]
      ,[ALWAYS_TAKE_DISC_FLAG]
      ,[EXCLUDE_FREIGHT_FROM_DISCOUNT]
      ,[GLOBAL_ATTRIBUTE17]
      ,[GLOBAL_ATTRIBUTE18]
      ,[GLOBAL_ATTRIBUTE19]
      ,[GLOBAL_ATTRIBUTE20]
      ,[ATTRIBUTE_CATEGORY]
      ,[ATTRIBUTE1]
      ,[ATTRIBUTE2]
      ,[ATTRIBUTE3]
      ,[ATTRIBUTE4]
      ,[ATTRIBUTE5]
      ,[ATTRIBUTE6]
      ,[ATTRIBUTE7]
      ,[ATTRIBUTE8]
      ,[ATTRIBUTE9]
      ,[ATTRIBUTE10]
      ,[ORG_NAME]
)
SELECT 
	  1 AS [RECOMMENDED]
	  , 0 AS [LOCAL_VALIDATED]
	  , 0 AS [CROSSREF_VALIDATED]
	  , '' AS [ACTION]
	  , 1 AS CLEANSED
	  , 'PA' AS COUNTRY
	  , '' AS [OBSERVATION]
	  , 0 AS DISMISSED
      ,[VENDOR_NUMBER]
      ,[VENDOR_SITE_CODE]
      ,[VENDOR_SITE_CODE_ALT]
      ,[ADDRESS_LINE1]
      ,[ADDRESS_LINE2]
      ,[ADDRESS_LINE3]
      ,[ADDRESS_LINE4]
      ,[ADDRESS_LINE_ALT]
      ,[CITY]
      ,[COUNTY]
      ,[STATE]
      ,[PROVINCE]
      ,[ZIP]
      ,[COUNTRY] AS ADDRESS_COUNTRY
      ,[AREA_CODE]
      ,[PHONE]
      ,[FAX_AREA_CODE]
      ,[FAX]
      ,[PAYMENT_METHOD_LOOKUP_CODE]
      ,[VAT_CODE]
      ,[ACCTS_PAY_ACCOUNT]
      ,[PREPAY_ACCOUNT]
      ,[PAY_GROUP_LOOKUP_CODE]
      ,[INVOICE_CURRENCY_CODE]
      ,[LANGUAGE]
      ,[TERMS_NAME]
      ,[EMAIL_ADDRESS]
	  ,CASE [PURCHASING_SITE_FLAG]  
		  WHEN 'Y' THEN 1  
		  WHEN 'N' THEN 0
		  ELSE 0
    	END
	  ,CASE [PAY_SITE_FLAG]  
		  WHEN 'Y' THEN 1  
		  WHEN 'N' THEN 0
		  ELSE 0
    	END
      ,[VAT_REGISTRATION_NUM]
      ,[SHIP_TO_LOCATION_CODE]
	  ,CASE [PRIMARY_PAY_SITE_FLAG]  
		  WHEN 'Y' THEN 1  
		  WHEN 'N' THEN 0
		  ELSE 0
    	END
	  ,CASE [GAPLESS_INV_NUM_FLAG]  
		  WHEN 'Y' THEN 1  
		  WHEN 'N' THEN 0
		  ELSE 0
    	END
      ,[OFFSET_TAX_FLAG]
      ,[ALWAYS_TAKE_DISC_FLAG]
      ,[EXCLUDE_FREIGHT_FROM_DISCOUNT]
      ,[GLOBAL_ATTRIBUTE17]
      ,[GLOBAL_ATTRIBUTE18]
      ,[GLOBAL_ATTRIBUTE19]
      ,[GLOBAL_ATTRIBUTE20]
      ,[ATTRIBUTE_CATEGORY]
      ,[ATTRIBUTE1]
      ,[ATTRIBUTE2]
      ,[ATTRIBUTE3]
      ,[ATTRIBUTE4]
      ,[ATTRIBUTE5]
      ,[ATTRIBUTE6]
      ,[ATTRIBUTE7]
      ,[ATTRIBUTE8]
      ,[ATTRIBUTE9]
      ,[ATTRIBUTE10]
      ,[ORG_NAME]

  FROM DM_LATAM.dbo.O_XL_PA_VENDOR_SITE

DELETE FROM  O_STG_VENDOR_SITE_CONTACT WHERE COUNTRY='PA';

INSERT INTO O_STG_VENDOR_SITE_CONTACT
(
      [RECOMMENDED]
      ,[LOCAL_VALIDATED]
      ,[CROSSREF_VALIDATED]
      ,[ACTION]
      ,[CLEANSED]
      ,[COUNTRY]
      ,[OBSERVATION]
      ,[DISMISSED]
      ,[VENDOR_NUMBER]
      ,[VENDOR_SITE_CODE]
      ,[PREFIX]
      ,[FIRST_NAME]
      ,[MIDDLE_NAME]
      ,[LAST_NAME]
      ,[DEPARTMENT]
      ,[PHONE_AREA_CODE]
      ,[PHONE_NUMBER]
      ,[FAX_AREA_CODE]
      ,[FAX_NUMBER]
      ,[EMAIL_ADDRESS]
      ,[ORG_NAME]
)
SELECT 
	  1 AS [RECOMMENDED]
	  , 0 AS [LOCAL_VALIDATED]
	  , 0 AS [CROSSREF_VALIDATED]
	  , '' AS [ACTION]
	  , 1 AS CLEANSED
	  , 'PA' AS COUNTRY
	  , '' AS [OBSERVATION]
	  , 0 AS DISMISSED
      ,[VENDOR_NUMBER]
      ,[VENDOR_SITE_CODE]
      ,[PREFIX]
      ,[FIRST_NAME]
      ,[MIDDLE_NAME]
      ,[LAST_NAME]
      ,[DEPARTMENT]
      ,[PHONE_AREA_CODE]
      ,[PHONE_NUMBER]
      ,[FAX_AREA_CODE]
      ,[FAX_NUMBER]
      ,[EMAIL_ADDRESS]
      ,[ORG_NAME]
  FROM [DM_LATAM].[dbo].[O_XL_PA_VENDOR_SITE_CONTACT]


DELETE FROM  O_STG_VENDOR_BANK_ACCOUNT WHERE COUNTRY='PA';

INSERT INTO O_STG_VENDOR_BANK_ACCOUNT
(
      [RECOMMENDED]
      ,[LOCAL_VALIDATED]
      ,[CROSSREF_VALIDATED]
      ,[ACTION]
      ,[CLEANSED]
      ,[COUNTRY]
      ,[OBSERVATION]
      ,[DISMISSED]
      ,[VENDOR_NUMBER]
      ,[BANK_NAME]
      ,[BRANCH_NAME]
      ,[BANK_NUMBER]
      ,[BRANCH_NUMBER]
      ,[BANK_HOME_COUNTRY]
      ,[TAX_PAYER_ID]
      ,[BANK_ACCOUNT_NAME]
      ,[BANK_ACCOUNT_NUM]
      ,[CURRENCY_CODE]
      ,[START_DATE]
      ,[END_DATE]
      ,[ORG_NAME]
)
SELECT 
	  1 AS [RECOMMENDED]
	  , 0 AS [LOCAL_VALIDATED]
	  , 0 AS [CROSSREF_VALIDATED]
	  , '' AS [ACTION]
	  , 1 AS CLEANSED
	  , 'PA' AS COUNTRY
	  , '' AS [OBSERVATION]
	  , 0 AS DISMISSED
      ,[VENDOR_NUMBER]
      ,[BANK_NAME]
      ,[BRANCH_NAME]
      ,[BANK_NUMBER]
      ,[BRANCH_NUMBER]
      ,[BANK_HOME_COUNTRY]
      ,[TAX_PAYER_ID]
      ,[BANK_ACCOUNT_NAME]
      ,[BANK_ACCOUNT_NUM]
      ,[CURRENCY_CODE]
      ,ISNULL([START_DATE], '01-01-2001')
      ,[END_DATE]
      ,[ORG_NAME]
FROM O_XL_PA_VENDOR_BANK_ACCOUNT