-- 1. set up country variables	
--
:setvar targetCountryCode PA

:setvar targetDatabaseName "DM_LATAM"           -- PRODUCTION
--:setvar targetDatabaseName "DM_LATAM_DEV"		-- DEV

:setvar sourceDatabaseName "TKPA_LATEST"
:setvar timezoneName Am�rica/Panam�
:setvar orgName (PANAMA)

:r "../../../00.COM/master/02.Vendors/load_VendorsData.sql"

GO


SELECT COUNT(*) AS VENDORS_COUNT, RECOMMENDED, COUNTRY FROM $(targetDatabaseName).dbo.O_PRE_VENDOR_HEADER GROUP BY RECOMMENDED, COUNTRY
