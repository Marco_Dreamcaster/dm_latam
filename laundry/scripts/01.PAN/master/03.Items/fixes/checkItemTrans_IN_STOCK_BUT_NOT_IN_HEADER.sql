DECLARE @cutoffDate DATE
SET @cutoffDate = '2018-09-30'

-- this should be empty 
-- if not, then
-- some items are recommended, but not cleansed
-- some items are dismissed (grouped)

select distinct LEFT(RTRIM(LTRIM('PA' + a.PRODUCTO)),30) AS ORIG_ITEM_NUMBER
from TKPA_LATEST.dbo.STOCK as a
		inner join TKPA_LATEST.dbo.PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO  
WHERE
	a.PERIODO=DATEPART(year, @cutoffDate)
	and a.IdAlmacen=1
	AND A.MES=DATEPART(month, @cutoffDate)
	and a.SALDO > 0
AND LEFT(RTRIM(LTRIM('PA' + a.PRODUCTO)),30) NOT IN
(
SELECT b.ORIG_ITEM_NUMBER
FROM DBO.[O_STG_ITEM_HEADER] b
WHERE
b.[COUNTRY]='PA' AND
b.[DISMISSED]=0 AND
b.[LOCAL_VALIDATED]=1 AND
b.[CROSSREF_VALIDATED]=1
)

-- this should be empty if there are no recommendations left to be cleansed
-- some items should be dismissed
select distinct LEFT(RTRIM(LTRIM('PA' + a.PRODUCTO)),30) AS ORIG_ITEM_NUMBER
from TKPA_LATEST.dbo.STOCK as a
		inner join TKPA_LATEST.dbo.PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO  
WHERE
	a.PERIODO=DATEPART(year, @cutoffDate)
	and a.IdAlmacen=1
	AND A.MES=DATEPART(month, @cutoffDate)
	and a.SALDO > 0
AND LEFT(RTRIM(LTRIM('PA' + a.PRODUCTO)),30) NOT IN
(
SELECT b.ORIG_ITEM_NUMBER
FROM DBO.[O_STG_ITEM_HEADER] b
WHERE
b.[COUNTRY]='PA'
)


-- no items should exist on STOCK but not on preSTG

select distinct LEFT(RTRIM(LTRIM('PA' + a.PRODUCTO)),30) AS ORIG_ITEM_NUMBER
from TKPA_LATEST.dbo.STOCK as a
		inner join TKPA_LATEST.dbo.PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO  
WHERE
	a.PERIODO=DATEPART(year, @cutoffDate)
	and a.IdAlmacen=1
	AND A.MES=DATEPART(month, @cutoffDate)
	and a.SALDO > 0
AND LEFT(RTRIM(LTRIM('PA' + a.PRODUCTO)),30) NOT IN
(
SELECT b.ORIG_ITEM_NUMBER
FROM DBO.[O_PRE_ITEM_HEADER] b
WHERE
b.[COUNTRY]='PA'
)