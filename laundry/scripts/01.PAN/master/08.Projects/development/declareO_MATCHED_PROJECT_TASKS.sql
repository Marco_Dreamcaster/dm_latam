IF OBJECT_ID('O_MATCHED_PROJECT_TASKS', 'V') IS NOT NULL
DROP VIEW dbo.O_MATCHED_PROJECT_TASKS
GO

CREATE VIEW dbo.O_MATCHED_PROJECT_TASKS AS
SELECT 
a.PROJECT_NAME,
a.TASK_NUMBER,
b.SIGLA_PRIMARY_KEY
FROM O_STG_PROJECT_TASKS a
INNER JOIN O_PRE_PROJECT_TASKS b
ON (a.SIGLA_PRIMARY_KEY=b.SIGLA_PRIMARY_KEY AND a.TASK_NUMBER = b.TASK_NUMBER)

GO
INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated O_MATCHED_PROJECT_TASKS view definition', GETDATE());
GO