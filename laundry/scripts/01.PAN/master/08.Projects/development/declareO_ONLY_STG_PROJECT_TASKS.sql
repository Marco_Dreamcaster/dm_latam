IF OBJECT_ID('O_ONLY_STG_PROJECT_TASKS', 'V') IS NOT NULL
DROP VIEW dbo.O_ONLY_STG_PROJECT_TASKS
GO

CREATE VIEW dbo.O_ONLY_STG_PROJECT_TASKS AS
SELECT
a.PROJECT_NAME,a.TASK_NUMBER,a.COUNTRY
FROM O_STG_PROJECT_TASKS a
WHERE 
NOT EXISTS (
SELECT * FROM O_MATCHED_PROJECT_TASKS c WHERE c.SIGLA_PRIMARY_KEY = a.SIGLA_PRIMARY_KEY AND TASK_NUMBER = a.TASK_NUMBER
)

GO
INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated O_ONLY_STG_PROJECT_TASKS view definition', GETDATE());
GO