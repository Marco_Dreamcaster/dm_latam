IF OBJECT_ID('DBO.O_STG_CUSTOMERS_AR') IS NOT NULL DROP TABLE O_STG_CUSTOMERS_AR;
GO

CREATE TABLE DBO.O_STG_CUSTOMERS_AR(

    -- Fields --
	Transaction_Number Varchar(20) Not Null,
	Description Varchar(250) Not Null,
	Transaction_Date Varchar(11) Not Null,
	Quantity Varchar(240) Null,
	Unit_Price Varchar(30) Null,
	Bill_To_Customer_Number_Reference Varchar(240) Not Null,
	Bill_To_Address_Reference Varchar(240) Not Null,
	Ship_To_Address_Reference Varchar(240) Null,
	Primary_Sales_Rep_Number Varchar(30) Not Null,
	Currency_Code Varchar(15) Not Null,
	Amount Varchar(30) Not Null,
    Payment_Terms Varchar(60) Not Null,
	Batch_Source_Name Varchar(30) Not Null,
	Tax_Rate_Code Varchar(60) Not Null,
	Accounting_Date Varchar(11) Not Null,
	Line_Type Varchar(60) Not Null,
	Transaction_Type Varchar(60) Not Null,
	Exchange_Rate_Type Varchar(60) Not Null,
	Conversion_Rate Varchar(60) Not Null,
	Invoice_Line_Number Varchar(150) Not Null,
	Project_Name Varchar(150) Null,
	Printer_Fiscal_Number Varchar(150) Null,
	Contract_Number Varchar(150) Null,
	Link_To_Line_Attribute1 Varchar(150) Not Null,
	Link_To_Line_Attribute2 Varchar(150) Not Null,
	Link_To_Line_Attribute3 Varchar(150) Not Null,
	Link_To_Line_Attribute4 Varchar(150) Not Null,
	Link_To_Line_Attribute5 Varchar(150) Not Null,
	Operating_Unit_Name Varchar(150) Not Null,

    -- Default --
	[COUNTRY] [nvarchar](2) NOT NULL,
	[OBSERVATION] [nvarchar](3000) NULL,
	[LAST_UPDATED] [datetime2](6) NOT NULL
);
GO