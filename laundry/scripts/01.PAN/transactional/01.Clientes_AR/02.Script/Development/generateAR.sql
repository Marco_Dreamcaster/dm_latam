IF OBJECT_ID('DBO.GenerateAR') IS NOT NULL DROP PROCEDURE DBO.GenerateAR
go
CREATE PROCEDURE DBO.GenerateAR(@FechaCorte DATE, @Country NVARCHAR(2))
AS
BEGIN
	 DECLARE @SQL nVarchar(MAX);
	 DECLARE @sourceDataBaseName nVarchar(20);

	 SET @sourceDataBaseName = 'TK' + @Country + '_LATEST';

	 SET NOCOUNT ON;
	 BEGIN TRANSACTION;

	 DELETE FROM DBO.O_STG_CUSTOMERS_AR WHERE COUNTRY = @Country;

	 EXEC DBO.populateAR @FechaCorte, @Country;

	 INSERT INTO DBO.O_STG_CUSTOMERS_AR

		  (Transaction_Number, Description, Transaction_Date, Quantity, Unit_Price,
		  Bill_To_Customer_Number_Reference, Bill_To_Address_Reference, Ship_To_Address_Reference,
		  Primary_Sales_Rep_Number, Currency_Code, Amount, Payment_Terms, Batch_Source_Name,
		  Tax_Rate_Code, Accounting_Date, Line_Type, Transaction_Type, Exchange_Rate_Type,
		  Conversion_Rate, Invoice_Line_Number, Project_Name, Printer_Fiscal_Number,
		  Contract_Number, Link_To_Line_Attribute1, Link_To_Line_Attribute2, Link_To_Line_Attribute3,
		  Link_To_Line_Attribute4, Link_To_Line_Attribute5, Operating_Unit_Name,
		  COUNTRY, OBSERVATION, LAST_UPDATED)

	 SELECT

		  Transaction_Number, Description, Transaction_Date, Quantity, Unit_Price,
		  Bill_To_Customer_Number_Reference, Bill_To_Address_Reference, Ship_To_Address_Reference,
		  Primary_Sales_Rep_Number, Currency_Code, Amount, Payment_Terms, Batch_Source_Name,
		  Tax_Rate_Code, Accounting_Date, Line_Type, Transaction_Type, Exchange_Rate_Type,
		  Conversion_Rate, Invoice_Line_Number, Project_Name, Printer_Fiscal_Number,
		  Contract_Number, Link_To_Line_Attribute1, Link_To_Line_Attribute2, Link_To_Line_Attribute3,
		  Link_To_Line_Attribute4, Link_To_Line_Attribute5, Operating_Unit_Name,
		  COUNTRY, OBSERVATION, LAST_UPDATED

	 FROM ##TMPARINVOICE

-- FLATFILE --

	 SELECT

		  Transaction_Number, UPPER(Description), Transaction_Date, Quantity, Unit_Price,
		  Bill_To_Customer_Number_Reference, Bill_To_Address_Reference, Ship_To_Address_Reference,
		  Primary_Sales_Rep_Number, Currency_Code, Amount, Payment_Terms, Batch_Source_Name,
		  Tax_Rate_Code, Accounting_Date, Line_Type, Transaction_Type, Exchange_Rate_Type,
		  Conversion_Rate, Invoice_Line_Number, Project_Name, Printer_Fiscal_Number,
		  Contract_Number, Link_To_Line_Attribute1, Link_To_Line_Attribute2, Link_To_Line_Attribute3,
		  Link_To_Line_Attribute4, Link_To_Line_Attribute5, Operating_Unit_Name

	 FROM DBO.O_STG_CUSTOMERS_AR
	 WHERE COUNTRY = @COUNTRY
	 ORDER BY TRANSACTION_NUMBER

	 IF @@ERROR != 0 ROLLBACK TRANSACTION
	 ELSE COMMIT TRANSACTION

	 RETURN
END;
go

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('ZZ', 'script', 'updated generateAR procedure definition', GETDATE());