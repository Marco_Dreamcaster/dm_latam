IF OBJECT_ID('DBO.generateItemTransForPA', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateItemTransForPA;
GO

CREATE PROCEDURE dbo.generateItemTransForPA
(
@cutoffDate Date
)
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

EXECUTE dbo.fixListPricePerUnitPA @cutoffDate;

SELECT DISTINCT
max(ltrim(rtrim(d.ITEM_NUMBER)))											as Item_Number,
'CPA'																		as Organization_Code,
case when max(isnull(f.DESCRIP,'')) = 'ALMACEN PRINCIPAL' THEN 'DEP01'
else 'DEPCAL'
END																			as Subinventory_Name,
round(sum(a.SALDO),4)														as Transaction_Quantity,
'31-10-2018'																as Transaction_Date,
'DATA MIGRATION PA'															as Transaction_Reference,
max(ltrim(rtrim(e.UBICACION))) 												as Locator_Name,
round(sum(a.COSTO * a.SALDO) / (SUM(a.saldo)) ,4)                           as Transaction_Cost,
''																			as Serial_Number_From,
''																			as Serial_Number_To	,
'501020.12601002.0000.00.00.000000.000000.000000.000000.274705'				as Distribution_Account,
'PA - THYSSENKRUPP ELEVADORES S.A.'											as Operating_Unit_Name
from TKPA_LATEST..STOCK as a
inner join TKPA_LATEST..PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO
inner join DBO.O_STG_ITEM_CROSS_REF_ALT as c on ltrim(rtrim(b.PRODUCTO))=ltrim(rtrim(c.CROSS_REFERENCE))
inner join DBO.O_STG_ITEM_HEADER as d on c.ORIG_ITEM_NUMBER=d.ORIG_ITEM_NUMBER
inner join TKPA_LATEST..UBICAPRODALM as e on b.PRODUCTO=e.PRODUCTO and a.IdAlmacen=e.IDALMACEN
inner join TKPA_LATEST..ALMACEN as f on a.IdAlmacen=f.IdAlmacen
WHERE
A.PERIODO=DATEPART(year, @cutoffDate)
and a.IdAlmacen=1
AND A.MES=DATEPART(month, @cutoffDate)
and a.SALDO > 0
and d.COUNTRY='PA'
group by c.ITEM_NUMBER

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('PA', 'script', 'updated generateItemTransForPA procedure definition', GETDATE());
