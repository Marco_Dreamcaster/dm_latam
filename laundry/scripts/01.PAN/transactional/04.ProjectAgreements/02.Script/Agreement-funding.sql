--drop table #tmpAgreementFunding
select distinct
	replace(replace('PA_' + ltrim(rtrim(a.CONTRATO)),'-','_'),'/','_') as Agreement_Number,
	e.PROJECT_NAME as Project_Name,
	--UPPER(LEFT(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(c.OE)),' ','_'),'-','_'),'�','a'),'�','e'),'�','i'),'�','o'),'�','u'),'�','n'),19)) Task_Number,
	LEFT('PA_' + CAST (c.IdEquipo AS VARCHAR(max)),20) as Task_Number,
	convert(varchar(10),a.REGISTRO,105) as Date_Allocated,
	round((g.haber-g.debe) / f.cantequipos,4) as Amount,		
	'ORIGINAL' as Category,
	'PA - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME 
into #tmpAgreementFunding
from contrato as a
	LEFT join DETCONT as b on a.CONTRATO=b.CONTRATO
	LEFT join EQUIPOS as c on b.IdEquipo=c.IdEquipo
	inner join DM_LATAM..O_STG_PROJECT_HEADER as e on 'PA_'+replace(replace(a.CONTRATO,'-','_'),' ','_') = e.SIGLA_PRIMARY_KEY
	inner join (select COUNT(idequipo) as cantequipos,contrato from DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
	inner join COM as g on a.contrato = g.CONTRATO
	inner join TITCOM as i on g.IdComp=i.IdComp
	where
g.CTA in ('31101001','31101002','31101003','31101004','31101005','31101006','31101007','31101008','31101030','31102','31102001','31102002','31102003','31102004','31102006','31102007','31102008')
and a.contrato in 
(select [COD OBRA] from DM_LATAM..contratosPOC )
and g.fecha <= '31-07-2018'
--and a.CONTRATO='001-10-14'
union all
select 
	replace(replace('PA_' + ltrim(rtrim(a.CONTRATO)),'-','_'),'/','_') as Agreement_Number,
	c.PROJECT_NAME as Project_Name,
	--UPPER(LEFT(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(e.OE)),' ','_'),'-','_'),'�','a'),'�','e'),'�','i'),'�','o'),'�','u'),'�','n'),19)) Task_Number,
	LEFT('PA_' + CAST (e.IdEquipo AS VARCHAR(max)),20) as Task_Number,
	convert(varchar(10),a.REGISTRO,105) as Date_Allocated,
	round(((b.[TOT CONTRATO])+b.[Valor de manto Gratuito]) / f.cantequipos ,4) as Amount,
	'ORIGINAL' as Category,
	'PA - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME 
from  CONTRATO as a
	inner join DM_LATAM..contratosPOC as b on a.CONTRATO=b.[COD OBRA]
	inner join DM_LATAM..O_STG_PROJECT_HEADER as c on 'PA_'+replace(replace(a.CONTRATO,'-','_'),' ','_') = c.SIGLA_PRIMARY_KEY
	inner join DETCONT as d on a.CONTRATO=d.CONTRATO
	inner join EQUIPOS as e on d.IdEquipo=e.IdEquipo
	inner join (select COUNT(idequipo) as cantequipos,contrato from DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
where a.CONTRATO in (select [COD OBRA] from DM_LATAM..contratosPOC)
--and a.CONTRATO='001-10-14' 

select 
	max(Agreement_Number) as Agreement_Number,'|' as '|',
	Project_Name,'|' as '|',
	Task_Number,'|' as '|',
	max(Date_Allocated) as Date_Allocated,'|' as '|',
	sum(Amount)as Amount,'|' as '|',
	max(Category) as Category,'|' as '|',
	max(OU_NAME) as OU_NAME
from #tmpAgreementFunding
where Amount > 0
group by Project_Name,Task_Number
order by Project_Name,Task_Number
