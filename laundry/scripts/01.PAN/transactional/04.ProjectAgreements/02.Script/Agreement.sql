-- drop table #tmpAgreement 
select 
	'PA' + ltrim(rtrim(a.CLIENTE))+'-CC' as Customer_Number,
	replace(replace('PA_' + ltrim(rtrim(a.CONTRATO)),'-','_'),'/','_') as Agreement_Number,
	'CONSTRUCTION' as Agreement_Type,
	round(g.haber-g.debe,4)as Amount,
	'USD' as Currency_code,
	'Y' as Revenue_Limit_Flag,
	'Y' as Invoice_Limit_Flag,
	'PA0' as Administrator_Employee_Reference,
	e.PROJECT_NAME as Description,
	'' as Date_Expiration	,
	case
		when ltrim(rtrim(b.IdPago)) = 1 then 'IMMEDIATE'
		when ltrim(rtrim(b.IdPago)) = 2 then '30 Days Net'
		when ltrim(rtrim(b.IdPago)) = 3 then 'DAY 05'
		else '30 Days Net'
	end as Terms,	
	'PA - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME
into #tmpAgreement
from CONTRATO as a
		left join PAGOS as b on a.IDPAGO=b.IdPago
		inner join (select TOP 1 VENTA from CAMBIOS where month(FECHA)= MONTH(dateadd(mm,-1,getdate())) order by FECHA desc) as c on 1=1
		inner join DM_LATAM..contratosPOC as d on a.CONTRATO=d.[COD OBRA]
		inner join DM_LATAM..O_STG_PROJECT_HEADER as e on 'PA_'+replace(replace(a.CONTRATO,'-','_'),' ','_') = e.SIGLA_PRIMARY_KEY
		inner join COM as g on a.contrato = g.CONTRATO
where
a.contrato in (select [COD OBRA] from DM_LATAM..contratosPOC)
and g.CTA in ('31101001','31101002','31101003','31101004','31101005','31101006','31101007','31101008','31101030','31102','31102001','31102002','31102003','31102004','31102006','31102007','31102008')
and g.fecha <= '31-07-2018'
union all
select 
	'PA' + ltrim(rtrim(a.CLIENTE))+'-CC' as Customer_Number,
	replace(replace('PA_' + ltrim(rtrim(a.CONTRATO)),'-','_'),'/','_') as Agreement_Number,
	'CONSTRUCTION' as Agreement_Type,
	round(ROUND(d.[TOT CONTRATO],4)+round(d.[Valor de manto Gratuito],4),4)as Amount,
	'USD' as Currency_code,
	'Y' as Revenue_Limit_Flag,
	'Y' as Invoice_Limit_Flag,
	'PA0' as Administrator_Employee_Reference,
	--UPPER(LEFT('PA' + '-' + RTRIM(LTRIM(CAST(a.CONTRATO AS VARCHAR))) + '-' + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(a.OBRA)),'�','a'),'�','e'),'�','i'),'�','o'),'�','u'),'�','n'),' ','_'), 30)) as Description,
	e.PROJECT_NAME as Description,
	'' as Date_Expiration	,
	case
		when ltrim(rtrim(b.IdPago)) = 1 then 'IMMEDIATE'
		when ltrim(rtrim(b.IdPago)) = 2 then '30 Days Net'
		when ltrim(rtrim(b.IdPago)) = 3 then 'DAY 05'
		else '30 Days Net'
	end as Terms,	
	'PA - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME
	from CONTRATO as a
		left join PAGOS as b on a.IDPAGO=b.IdPago
		inner join DM_LATAM..contratosPOC as d on a.CONTRATO=d.[COD OBRA]
		inner join DM_LATAM..O_STG_PROJECT_HEADER as e on 'PA_'+replace(replace(a.CONTRATO,'-','_'),' ','_') = e.SIGLA_PRIMARY_KEY		
where
a.contrato in (select [COD OBRA] from DM_LATAM..contratosPOC)
--order by a.CONTRATO 

select 
	max(Customer_Number) as Customer_Number,'|' as '|',
	max(Agreement_Number)as Agreement_Number,'|' as '|',
	max(Agreement_Type) as Agreement_Type,'|' as '|',
	sum(Amount) as Amount,'|' as '|',
	max(Currency_code) as Currency_code,'|' as '|',
	max(Revenue_Limit_Flag) as Revenue_Limit_Flag,'|' as '|',
	max(Invoice_Limit_Flag) as Invoice_Limit_Flag,'|' as '|',
	max(Administrator_Employee_Reference) as Administrator_Employee_Reference,'|' as '|',
	Description,'|' as '|',
	max(Date_Expiration) as Date_Expiration,'|' as '|',
	max(Terms) as Terms,'|' as '|',
	max(OU_NAME) as OU_NAME
from #tmpAgreement
where Amount > 0
group by Description
order by Description


