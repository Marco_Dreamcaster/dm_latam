select COUNT([COD OBRA]) from contratosPOC

EXEC dbo.generateProjectAgreementHeaderForPA
EXEC dbo.generateProjectAgreementFundingForPA



/*
select 
	UPPER(replace(replace('PA_' + ltrim(rtrim(a.CONTRATO)),'-','_'),'/','_')) as Agreement_Number,
	c.PROJECT_NAME as Project_Name,
	LEFT('PA_' + CAST (e.IdEquipo AS VARCHAR(max)),20) as Task_Number,
	convert(varchar(10),a.REGISTRO,105) as Date_Allocated,
	round(((b.[TOT CONTRATO])+b.[Valor de manto Gratuito]) / f.cantequipos ,2) as Amount,
	'ORIGINAL' as Category,
	'PA - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME,
	'PA' as [COUNTRY],
	'loaded from script' as [OBSERVATION],
	c.SIGLA_PRIMARY_KEY as [SIGLA_PRIMARY_KEY]
from  TKPA_LATEST..CONTRATO as a
	inner join contratosPOC as b on a.CONTRATO=b.[COD OBRA]
	inner join O_STG_PROJECT_HEADER as c on UPPER('PA_'+replace(replace(RTRIM(LTRIM(a.CONTRATO)),'-','_'),' ','_')) = c.SIGLA_PRIMARY_KEY
	inner join TKPA_LATEST..DETCONT as d on a.CONTRATO=d.CONTRATO
	inner join TKPA_LATEST..EQUIPOS as e on d.IdEquipo=e.IdEquipo
	inner join (select COUNT(idequipo) as cantequipos,contrato from TKPA_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
WHERE
c.COUNTRY='PA' AND
c.TEMPLATE_NAME<>'SERV TEMPLATE PA' AND
c.DISMISSED=0 AND
c.PM_PROJECT_REFERENCE='PA-01/10/14-A' AND
a.CONTRATO in (select [COD OBRA] from contratosPOC)


EXEC dbo.generateProjectAgreementHeaderForPA

SELECT c.SIGLA_PRIMARY_KEY FROM O_STG_PROJECT_HEADER as c 
WHERE
c.COUNTRY='PA' AND
c.TEMPLATE_NAME<>'SERV TEMPLATE PA' AND
c.DISMISSED=0 AND
c.SIGLA_PRIMARY_KEY NOT IN (
SELECT a.Agreement_Number
FROM O_STG_PROJECT_AGREEMENT_HEADER a
)

SELECT a.Agreement_Number
FROM O_STG_PROJECT_AGREEMENT_HEADER a
WHERE
a.Agreement_Number NOT IN (
SELECT c.SIGLA_PRIMARY_KEY FROM O_STG_PROJECT_HEADER as c 
WHERE
c.COUNTRY='PA' AND
c.TEMPLATE_NAME<>'SERV TEMPLATE PA' AND
c.DISMISSED=0
)

SELECT dbo.udf_EncodeProjectReference('PA', a.[COD OBRA])
FROM contratosPOC a
WHERE
dbo.udf_EncodeProjectReference('PA', a.[COD OBRA]) NOT IN (
SELECT c.SIGLA_PRIMARY_KEY FROM O_STG_PROJECT_HEADER as c 
WHERE
c.COUNTRY='PA' AND
c.TEMPLATE_NAME<>'SERV TEMPLATE PA' AND
c.DISMISSED=0
)

SELECT c.SIGLA_PRIMARY_KEY FROM O_STG_PROJECT_HEADER as c 
WHERE
c.COUNTRY='PA' AND
c.TEMPLATE_NAME<>'SERV TEMPLATE PA' AND
c.DISMISSED=0 AND
c.SIGLA_PRIMARY_KEY NOT IN (
SELECT dbo.udf_EncodeProjectReference('PA', a.[COD OBRA])
FROM contratosPOC a
)


SELECT 
SUM(Amount) as SummedFundingAmount,
Agreement_Number
FROM O_STG_PROJECT_AGREEMENT_FUNDING
GROUP BY Agreement_Number




SELECT a.Amount,* FROM O_STG_PROJECT_AGREEMENT_HEADER a WHERE a.Agreement_Number='PA_CT580_1015'

SELECT DISTINCT(a.Task_Number), a.Amount FROM O_STG_PROJECT_AGREEMENT_FUNDING a WHERE a.Agreement_Number='PA_CT580_1015'
AND a.TASK_NUMBER NOT IN(
SELECT b.TASK_NUMBER
FROM O_STG_PROJECT_HEADER a
INNER JOIN O_STG_PROJECT_TASKS b ON a.PROJECT_NAME=b.PROJECT_NAME
WHERE a.SIGLA_PRIMARY_KEY='PA_CT580_1015'
)

*/





/** checking Agreement Funding for PA_CT580_1015 **/

IF OBJECT_ID('tempdb..#tmpAgreementFunding') IS NOT NULL DROP TABLE #tmpAgreementFunding

select distinct
	dbo.udf_EncodeProjectReference('PA', a.CONTRATO) as Agreement_Number,
	e.PROJECT_NAME as Project_Name,
	LEFT('PA_' + CAST (c.IdEquipo AS VARCHAR(max)),20) as Task_Number,
	convert(varchar(10),a.REGISTRO,105) as Date_Allocated,
	round((g.haber-g.debe) / f.cantequipos,2) as Amount,		
	'ORIGINAL' as Category,
	'PA - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME,
	'PA' as [COUNTRY],
	'loaded from COM' as [OBSERVATION],
	e.SIGLA_PRIMARY_KEY as [SIGLA_PRIMARY_KEY],
	GETDATE() AS LAST_UPDATED
into #tmpAgreementFunding
from TKPA_LATEST..contrato as a
	LEFT join TKPA_LATEST..DETCONT as b on a.CONTRATO=b.CONTRATO
	LEFT join TKPA_LATEST..EQUIPOS as c on b.IdEquipo=c.IdEquipo
	inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = e.SIGLA_PRIMARY_KEY
	inner join (select COUNT(idequipo) as cantequipos,contrato from TKPA_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
	inner join TKPA_LATEST..COM as g on a.contrato = g.CONTRATO
	inner join TKPA_LATEST..TITCOM as i on g.IdComp=i.IdComp
	where
g.CTA in ('31101001','31101002','31101003','31101004','31101005','31101006','31101007','31101008','31101030','31102','31102001','31102002','31102003','31102004','31102006','31102007','31102008')
and dbo.udf_EncodeProjectReference('PA', a.CONTRATO)='PA_CT580_1015'
and a.contrato in 
(select [COD OBRA] from contratosPOC )
and g.fecha <= convert(datetime, '31-08-2018', 105)
union all
select 
	dbo.udf_EncodeProjectReference('PA', a.CONTRATO) as Agreement_Number,
	c.PROJECT_NAME as Project_Name,
	LEFT('PA_' + CAST (e.IdEquipo AS VARCHAR(max)),20) as Task_Number,
	convert(varchar(10),a.REGISTRO,105) as Date_Allocated,
	round(((b.[TOT CONTRATO])+b.[Valor de manto Gratuito]) / f.cantequipos ,2) as Amount,
	'ORIGINAL' as Category,
	'PA - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME,
	'PA' as [COUNTRY],
	'loaded from SPREADSHEET' as [OBSERVATION],
	c.SIGLA_PRIMARY_KEY as [SIGLA_PRIMARY_KEY],
	GETDATE() AS LAST_UPDATED
from  TKPA_LATEST..CONTRATO as a
	inner join contratosPOC as b on a.CONTRATO=b.[COD OBRA]
	inner join O_STG_PROJECT_HEADER as c on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = c.SIGLA_PRIMARY_KEY
	inner join TKPA_LATEST..DETCONT as d on a.CONTRATO=d.CONTRATO
	inner join TKPA_LATEST..EQUIPOS as e on d.IdEquipo=e.IdEquipo
	inner join (select COUNT(idequipo) as cantequipos,contrato from TKPA_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
where a.CONTRATO in (select [COD OBRA] from contratosPOC)
and dbo.udf_EncodeProjectReference('PA', a.CONTRATO)='PA_CT580_1015'


/** checking by source **/
select 
	max(Agreement_Number) as Agreement_Number,
	Project_Name,
	Task_Number,
	max(Date_Allocated) as Date_Allocated,
	sum(Amount)as Amount,
	max(Category) as Category,
	max(OU_NAME) as OU_NAME,
	max(COUNTRY) as [COUNTRY],
	OBSERVATION as [OBSERVATION],
	max(SIGLA_PRIMARY_KEY) as [SIGLA_PRIMARY_KEY],
	max(LAST_UPDATED) as LAST_UPDATED
from #tmpAgreementFunding
where Amount > 0
group by Project_Name,Task_Number,OBSERVATION
order by Project_Name,Task_Number,OBSERVATION

/** checking by source **/
select 
	max(Agreement_Number) as Agreement_Number,
	Project_Name,
	Task_Number,
	max(Date_Allocated) as Date_Allocated,
	sum(Amount)as Amount,
	max(Category) as Category,
	max(OU_NAME) as OU_NAME,
	max(COUNTRY) as [COUNTRY],
	--OBSERVATION as [OBSERVATION],
	max(SIGLA_PRIMARY_KEY) as [SIGLA_PRIMARY_KEY],
	max(LAST_UPDATED) as LAST_UPDATED
from #tmpAgreementFunding
where Amount > 0
group by Project_Name,Task_Number--,OBSERVATION
order by Project_Name,Task_Number--,OBSERVATION




IF OBJECT_ID('tempdb..#tmpAgreementDebug') IS NOT NULL DROP TABLE #tmpAgreementDebug

select 
	'PA' + ltrim(rtrim(a.CLIENTE))+'-CC' as Customer_Number,
	dbo.udf_EncodeProjectReference('PA', a.CONTRATO) as Agreement_Number,
	'CONSTRUCTION' as Agreement_Type,
	round(g.haber-g.debe,2)as Amount,
	'USD' as Currency_code,
	'Y' as Revenue_Limit_Flag,
	'Y' as Invoice_Limit_Flag,
	'PADM01' as Administrator_Employee_Reference,
	e.PROJECT_NAME as Description,
	'' as Date_Expiration,
	case
		when ltrim(rtrim(b.IdPago)) = 1 then 'IMMEDIATE'
		when ltrim(rtrim(b.IdPago)) = 2 then '30 Days Net'
		when ltrim(rtrim(b.IdPago)) = 3 then 'DAY 05'
		else '30 Days Net'
	end as Terms,	
	'PA - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME,
	'PA' as [COUNTRY],
	'loaded from script' as [OBSERVATION],
	e.SIGLA_PRIMARY_KEY as [SIGLA_PRIMARY_KEY],
	GETDATE() AS LAST_UPDATED
into #tmpAgreementDebug
from TKPA_LATEST..CONTRATO as a
		left join TKPA_LATEST..PAGOS as b on a.IDPAGO=b.IdPago
		inner join (select TOP 1 VENTA from TKPA_LATEST..CAMBIOS where month(FECHA)= MONTH(dateadd(mm,-1,getdate())) order by FECHA desc) as c on 1=1
		inner join contratosPOC as d on a.CONTRATO=d.[COD OBRA]
		inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = e.SIGLA_PRIMARY_KEY
		inner join TKPA_LATEST..COM as g on a.contrato = g.CONTRATO
where
a.contrato in (select [COD OBRA] from contratosPOC)
and g.CTA in ('31101001','31101002','31101003','31101004','31101005','31101006','31101007','31101008','31101030','31102','31102001','31102002','31102003','31102004','31102006','31102007','31102008')
and g.fecha <= convert(datetime, '31-08-2018', 105)
and dbo.udf_EncodeProjectReference('PA', a.CONTRATO)='PA_CT580_1015'
union all
select 
	'PA' + ltrim(rtrim(a.CLIENTE))+'-CC' as Customer_Number,
	dbo.udf_EncodeProjectReference('PA', a.CONTRATO) as Agreement_Number,
	'CONSTRUCTION' as Agreement_Type,
	round(ROUND(d.[TOT CONTRATO],2)+round(d.[Valor de manto Gratuito],2),2)as Amount,
	'USD' as Currency_code,
	'Y' as Revenue_Limit_Flag,
	'Y' as Invoice_Limit_Flag,
	'PADM01' as Administrator_Employee_Reference,
	e.PROJECT_NAME as Description,
	'' as Date_Expiration	,
	case
		when ltrim(rtrim(b.IdPago)) = 1 then 'IMMEDIATE'
		when ltrim(rtrim(b.IdPago)) = 2 then '30 Days Net'
		when ltrim(rtrim(b.IdPago)) = 3 then 'DAY 05'
		else '30 Days Net'
	end as Terms,	
	'PA - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME,
	'PA' as [COUNTRY],
	'loaded from script' as [OBSERVATION],
	e.SIGLA_PRIMARY_KEY as [SIGLA_PRIMARY_KEY],
	GETDATE() AS LAST_UPDATED
	from TKPA_LATEST..CONTRATO as a
		left join TKPA_LATEST..PAGOS as b on a.IDPAGO=b.IdPago
		inner join contratosPOC as d on a.CONTRATO=d.[COD OBRA]
		inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = e.SIGLA_PRIMARY_KEY		
where
a.contrato in (select [COD OBRA] from contratosPOC)
and dbo.udf_EncodeProjectReference('PA', a.CONTRATO)='PA_CT580_1015'

SELECT SUM(Amount) FROM #tmpAgreementDebug GROUP BY Agreement_Number



SELECT DISTINCT a.CONTRATO,
round(g.haber-g.debe,2) as Amount
from TKPA_LATEST..contrato as a
	LEFT join TKPA_LATEST..DETCONT as b on a.CONTRATO=b.CONTRATO
	LEFT join TKPA_LATEST..EQUIPOS as c on b.IdEquipo=c.IdEquipo
	inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = e.SIGLA_PRIMARY_KEY
	inner join (select COUNT(idequipo) as cantequipos,contrato from TKPA_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
	inner join TKPA_LATEST..COM as g on a.contrato = g.CONTRATO
	inner join TKPA_LATEST..TITCOM as i on g.IdComp=i.IdComp
	where
g.CTA in ('31101001','31101002','31101003','31101004','31101005','31101006','31101007','31101008','31101030','31102','31102001','31102002','31102003','31102004','31102006','31102007','31102008')
and a.contrato in 
(select [COD OBRA] from contratosPOC )
and g.fecha <= convert(datetime, '31-08-2018', 105)
and a.CONTRATO='CT580-1015'
GROUP BY a.CONTRATO

SELECT DISTINCT a.CONTRATO
from TKPA_LATEST..CONTRATO as a
		left join TKPA_LATEST..PAGOS as b on a.IDPAGO=b.IdPago
		inner join (select TOP 1 VENTA from TKPA_LATEST..CAMBIOS where month(FECHA)= MONTH(dateadd(mm,-1,getdate())) order by FECHA desc) as c on 1=1
		inner join contratosPOC as d on a.CONTRATO=d.[COD OBRA]
		inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = e.SIGLA_PRIMARY_KEY
		inner join TKPA_LATEST..COM as g on a.contrato = g.CONTRATO
where
a.contrato in (select [COD OBRA] from contratosPOC)
and g.CTA in ('31101001','31101002','31101003','31101004','31101005','31101006','31101007','31101008','31101030','31102','31102001','31102002','31102003','31102004','31102006','31102007','31102008')
and g.fecha <= convert(datetime, '31-08-2018', 105)
and a.CONTRATO='CT580-1015'




EXEC dbo.fixRoundedCentsProjectAgreementForPA



SELECT
a.Agreement_Number,
COUNT(b.Amount) as NoEquipos,
max(b.Amount) as TotalFundingAmmount,
SUM(a.Amount) as SummedFundingAmount,
ROUND(MAX(b.Amount)-SUM(a.Amount),3) as diff
FROM O_STG_PROJECT_AGREEMENT_FUNDING a
inner join O_STG_PROJECT_AGREEMENT_HEADER b ON a.Agreement_Number=b.Agreement_Number
GROUP BY a.Agreement_Number
ORDER BY diff DESC