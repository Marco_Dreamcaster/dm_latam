IF OBJECT_ID('dbo.udf_EncodeProjectReference', 'FN') IS NOT NULL
DROP FUNCTION dbo.udf_EncodeProjectReference;
GO

CREATE FUNCTION dbo.udf_EncodeProjectReference ( @country VARCHAR(2), @contrato VARCHAR(10) )
RETURNS VARCHAR(25)
AS
BEGIN

DECLARE @pmProjectReference VARCHAR(25)

SET @pmProjectReference = UPPER(@country+'_'+replace(replace(RTRIM(LTRIM(@contrato)),'-','_'),' ','_'));

RETURN @pmProjectReference
END
GO

IF OBJECT_ID('DBO.fixSIGLAForProjectAgreementsPA', 'P') IS NOT NULL
	DROP PROCEDURE dbo.fixSIGLAForProjectAgreementsPA;
GO

CREATE PROCEDURE dbo.fixSIGLAForProjectAgreementsPA
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

update TKPA_LATEST..COM set CONTRATO='001-10-14' where contrato='001-1014'
update TKPA_LATEST..COM set CONTRATO='N729-1217' where contrato='N29-1217'

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

RETURN
END
GO


IF OBJECT_ID('populateProjectAgreementHeaderForPA', 'P') IS NOT NULL
	DROP PROCEDURE dbo.populateProjectAgreementHeaderForPA;
GO

CREATE PROCEDURE dbo.populateProjectAgreementHeaderForPA
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

EXEC dbo.fixSIGLAForProjectAgreementsPA

IF OBJECT_ID('tempdb..#tmpAgreement') IS NOT NULL DROP TABLE #tmpAgreement

select 
	'PA' + ltrim(rtrim(a.CLIENTE))+'-CC' as Customer_Number,
	dbo.udf_EncodeProjectReference('PA', a.CONTRATO) as Agreement_Number,
	'CONSTRUCTION' as Agreement_Type,
	round(g.haber-g.debe,2)as Amount,
	'USD' as Currency_code,
	'Y' as Revenue_Limit_Flag,
	'Y' as Invoice_Limit_Flag,
	'PADM01' as Administrator_Employee_Reference,
	e.PROJECT_NAME as Description,
	'' as Date_Expiration,
	case
		when ltrim(rtrim(b.IdPago)) = 1 then 'IMMEDIATE'
		when ltrim(rtrim(b.IdPago)) = 2 then '30 Days Net'
		when ltrim(rtrim(b.IdPago)) = 3 then 'DAY 05'
		else '30 Days Net'
	end as Terms,	
	'PA - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME,
	'PA' as [COUNTRY],
	'loaded from script' as [OBSERVATION],
	e.SIGLA_PRIMARY_KEY as [SIGLA_PRIMARY_KEY],
	GETDATE() AS LAST_UPDATED
into #tmpAgreement
from TKPA_LATEST..CONTRATO as a
		left join TKPA_LATEST..PAGOS as b on a.IDPAGO=b.IdPago
		inner join (select TOP 1 VENTA from TKPA_LATEST..CAMBIOS where month(FECHA)= MONTH(dateadd(mm,-1,getdate())) order by FECHA desc) as c on 1=1
		inner join contratosPOC as d on a.CONTRATO=d.[COD OBRA]
		inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = e.SIGLA_PRIMARY_KEY
		inner join TKPA_LATEST..COM as g on a.contrato = g.CONTRATO
where
a.contrato in (select [COD OBRA] from contratosPOC)
and g.CTA in ('31101001','31101002','31101003','31101004','31101005','31101006','31101007','31101008','31101030','31102','31102001','31102002','31102003','31102004','31102006','31102007','31102008')
and g.fecha <= convert(datetime, '31-08-2018', 105)
--and a.CONTRATO='CT580-1015'
union all
select 
	'PA' + ltrim(rtrim(a.CLIENTE))+'-CC' as Customer_Number,
	dbo.udf_EncodeProjectReference('PA', a.CONTRATO) as Agreement_Number,
	'CONSTRUCTION' as Agreement_Type,
	round(ROUND(d.[TOT CONTRATO],2)+round(d.[Valor de manto Gratuito],2),2)as Amount,
	'USD' as Currency_code,
	'Y' as Revenue_Limit_Flag,
	'Y' as Invoice_Limit_Flag,
	'PADM01' as Administrator_Employee_Reference,
	e.PROJECT_NAME as Description,
	'' as Date_Expiration	,
	case
		when ltrim(rtrim(b.IdPago)) = 1 then 'IMMEDIATE'
		when ltrim(rtrim(b.IdPago)) = 2 then '30 Days Net'
		when ltrim(rtrim(b.IdPago)) = 3 then 'DAY 05'
		else '30 Days Net'
	end as Terms,
	'PA - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME,
	'PA' as [COUNTRY],
	'loaded from script' as [OBSERVATION],
	e.SIGLA_PRIMARY_KEY as [SIGLA_PRIMARY_KEY],
	GETDATE() AS LAST_UPDATED
	--into #tmpAgreement
	from TKPA_LATEST..CONTRATO as a
		left join TKPA_LATEST..PAGOS as b on a.IDPAGO=b.IdPago
		inner join contratosPOC as d on a.CONTRATO=d.[COD OBRA]
		inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = e.SIGLA_PRIMARY_KEY		
where
a.contrato in (select [COD OBRA] from contratosPOC)
--and a.CONTRATO='CT580-1015'

select 
	max(Customer_Number) as Customer_Number,
	max(Agreement_Number)as Agreement_Number,
	max(Agreement_Type) as Agreement_Type,
	sum(Amount) as Amount,
	max(Currency_code) as Currency_code,
	max(Revenue_Limit_Flag) as Revenue_Limit_Flag,
	max(Invoice_Limit_Flag) as Invoice_Limit_Flag,
	max(Administrator_Employee_Reference) as Administrator_Employee_Reference,
	Description,
	max(Date_Expiration) as Date_Expiration,
	max(Terms) as Terms,
	max(OU_NAME) as OU_NAME,
	max(COUNTRY) as [COUNTRY],
	max(OBSERVATION) as [OBSERVATION],
	max(SIGLA_PRIMARY_KEY) as [SIGLA_PRIMARY_KEY],
	max(LAST_UPDATED) as LAST_UPDATED
from #tmpAgreement
--where Amount > 0
group by Description
order by Description

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

RETURN
END

GO

IF OBJECT_ID('populateProjectAgreementFundingForPA', 'P') IS NOT NULL
	DROP PROCEDURE dbo.populateProjectAgreementFundingForPA;
GO

CREATE PROCEDURE dbo.populateProjectAgreementFundingForPA
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

IF OBJECT_ID('tempdb..#tmpAgreementFunding') IS NOT NULL DROP TABLE #tmpAgreementFunding

select distinct
	dbo.udf_EncodeProjectReference('PA', a.CONTRATO) as Agreement_Number,
	e.PROJECT_NAME as Project_Name,
	LEFT('PA_' + CAST (c.IdEquipo AS VARCHAR(max)),20) as Task_Number,
	convert(varchar(10),a.REGISTRO,105) as Date_Allocated,
	round((g.haber-g.debe) / f.cantequipos,2) as Amount,		
	'ORIGINAL' as Category,
	'PA - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME,
	'PA' as [COUNTRY],
	'loaded from script' as [OBSERVATION],
	e.SIGLA_PRIMARY_KEY as [SIGLA_PRIMARY_KEY],
	GETDATE() AS LAST_UPDATED
into #tmpAgreementFunding
from TKPA_LATEST..contrato as a
	LEFT join TKPA_LATEST..DETCONT as b on a.CONTRATO=b.CONTRATO
	LEFT join TKPA_LATEST..EQUIPOS as c on b.IdEquipo=c.IdEquipo
	inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = e.SIGLA_PRIMARY_KEY
	inner join (select COUNT(idequipo) as cantequipos,contrato from TKPA_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
	inner join TKPA_LATEST..COM as g on a.contrato = g.CONTRATO
	inner join TKPA_LATEST..TITCOM as i on g.IdComp=i.IdComp
	where
g.CTA in ('31101001','31101002','31101003','31101004','31101005','31101006','31101007','31101008','31101030','31102','31102001','31102002','31102003','31102004','31102006','31102007','31102008')
and a.contrato in 
(select [COD OBRA] from contratosPOC )
and g.fecha <= convert(datetime, '31-08-2018', 105)
--and a.CONTRATO='CT580-1015'
union all

select 
	dbo.udf_EncodeProjectReference('PA', a.CONTRATO) as Agreement_Number,
	c.PROJECT_NAME as Project_Name,
	LEFT('PA_' + CAST (e.IdEquipo AS VARCHAR(max)),20) as Task_Number,
	convert(varchar(10),a.REGISTRO,105) as Date_Allocated,
	round(((b.[TOT CONTRATO])+b.[Valor de manto Gratuito]) / f.cantequipos ,2) as Amount,
	'ORIGINAL' as Category,
	'PA - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME,
	'PA' as [COUNTRY],
	'loaded from script' as [OBSERVATION],
	c.SIGLA_PRIMARY_KEY as [SIGLA_PRIMARY_KEY],
	GETDATE() AS LAST_UPDATED
from  TKPA_LATEST..CONTRATO as a
	inner join contratosPOC as b on a.CONTRATO=b.[COD OBRA]
	inner join O_STG_PROJECT_HEADER as c on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = c.SIGLA_PRIMARY_KEY
	inner join TKPA_LATEST..DETCONT as d on a.CONTRATO=d.CONTRATO
	inner join TKPA_LATEST..EQUIPOS as e on d.IdEquipo=e.IdEquipo
	inner join (select COUNT(idequipo) as cantequipos,contrato from TKPA_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
where a.CONTRATO in (select [COD OBRA] from contratosPOC)
--and a.CONTRATO='CT580-1015'

select 
	max(Agreement_Number) as Agreement_Number,
	Project_Name,
	Task_Number,
	max(Date_Allocated) as Date_Allocated,
	sum(Amount)as Amount,
	max(Category) as Category,
	max(OU_NAME) as OU_NAME,
	max(COUNTRY) as [COUNTRY],
	max(OBSERVATION) as [OBSERVATION],
	max(SIGLA_PRIMARY_KEY) as [SIGLA_PRIMARY_KEY],
	max(LAST_UPDATED) as LAST_UPDATED
from #tmpAgreementFunding
where Amount > 0
group by Project_Name,Task_Number
order by Project_Name,Task_Number


IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

RETURN
END

GO

IF OBJECT_ID('fixRoundedCentsProjectAgreementForPA', 'P') IS NOT NULL
	DROP PROCEDURE dbo.fixRoundedCentsProjectAgreementForPA;
GO

CREATE PROCEDURE dbo.fixRoundedCentsProjectAgreementForPA
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

DECLARE @agreement_Number VARCHAR(25)
DECLARE @diff FLOAT

DECLARE ROUNDED_CENTS_FIX CURSOR FOR   
SELECT
a.Agreement_Number,
ROUND(MAX(b.Amount)-SUM(a.Amount),3) as diff
FROM O_STG_PROJECT_AGREEMENT_FUNDING a
inner join O_STG_PROJECT_AGREEMENT_HEADER b ON a.Agreement_Number=b.Agreement_Number
GROUP BY a.Agreement_Number
HAVING ABS(ROUND(MAX(b.Amount)-SUM(a.Amount),3))>0
ORDER BY diff DESC


OPEN ROUNDED_CENTS_FIX;

FETCH NEXT FROM ROUNDED_CENTS_FIX   
INTO @agreement_Number, @diff;

WHILE @@FETCH_STATUS = 0
BEGIN
	DECLARE @taskNumber VARCHAR(25)

	SELECT TOP 1 @taskNumber=a.Task_Number FROM O_STG_PROJECT_AGREEMENT_FUNDING a
	WHERE a.Agreement_Number=@agreement_Number

	UPDATE O_STG_PROJECT_AGREEMENT_FUNDING SET Amount=Amount+@diff
	WHERE Task_Number=@taskNumber AND Agreement_Number=@agreement_Number


	FETCH NEXT FROM ROUNDED_CENTS_FIX   
	INTO @agreement_Number, @diff;
END;

CLOSE ROUNDED_CENTS_FIX;

DEALLOCATE ROUNDED_CENTS_FIX;

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

END

GO




IF OBJECT_ID('generateProjectAgreementHeaderForPA', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateProjectAgreementHeaderForPA;
GO

CREATE PROCEDURE dbo.generateProjectAgreementHeaderForPA
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

DELETE FROM O_STG_PROJECT_AGREEMENT_HEADER WHERE COUNTRY='PA'

INSERT INTO O_STG_PROJECT_AGREEMENT_HEADER
EXEC dbo.populateProjectAgreementHeaderForPA

SELECT 
	Customer_Number
	,Agreement_Number
	,[Agreement_Type]
    ,[Amount]
    ,[CurrencyCode]
    ,[RevenueLimitFlag]
    ,[InvoiceLimitFlag]
    ,[Administrator_Employee_Ref]
    ,[Description]
    ,[DateExpiration]
    ,[Terms]
    ,[Operating_Unit_Name]

FROM O_STG_PROJECT_AGREEMENT_HEADER
WHERE COUNTRY='PA'

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

RETURN
END

GO

IF OBJECT_ID('generateProjectAgreementFundingForPA', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateProjectAgreementFundingForPA;
GO

CREATE PROCEDURE dbo.generateProjectAgreementFundingForPA
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

DELETE FROM O_STG_PROJECT_AGREEMENT_FUNDING WHERE COUNTRY='PA'

INSERT INTO O_STG_PROJECT_AGREEMENT_FUNDING
EXEC dbo.populateProjectAgreementFundingForPA

EXEC dbo.fixRoundedCentsProjectAgreementForPA

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

SELECT 
Agreement_Number,
Project_Name,
Task_Number,
Date_Allocated,
Amount,
Category,
Operating_Unit_Name
FROM O_STG_PROJECT_AGREEMENT_FUNDING

RETURN
END

GO


EXEC dbo.generateProjectAgreementHeaderForPA

EXEC dbo.generateProjectAgreementFundingForPA









