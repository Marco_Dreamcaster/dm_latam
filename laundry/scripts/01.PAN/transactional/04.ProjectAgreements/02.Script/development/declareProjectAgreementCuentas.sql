IF OBJECT_ID('DBO.ProjectAgreementCuentas') IS NOT NULL DROP TABLE DBO.ProjectAgreementCuentas
GO

CREATE TABLE DBO.ProjectAgreementCuentas (
    Account_Sigla CHAR(8),
    Country Char(2)
)
GO