IF OBJECT_ID('generateProjectAgreementFundingForPA', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateProjectAgreementFundingForPA;
GO

CREATE PROCEDURE dbo.generateProjectAgreementFundingForPA
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

DELETE FROM O_STG_PROJECT_AGREEMENT_FUNDING WHERE COUNTRY='PA'

INSERT INTO O_STG_PROJECT_AGREEMENT_FUNDING
EXEC dbo.populateProjectAgreementFundingForPA

EXEC dbo.fixRoundedCentsProjectAgreementForPA

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

SELECT
Agreement_Number,
Project_Name,
Task_Number,
Date_Allocated,
Amount,
Category,
Operating_Unit_Name
FROM O_STG_PROJECT_AGREEMENT_FUNDING
ORDER BY Agreement_Number

RETURN
END


GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('PA', 'script', 'updated generateProjectAgreementFundingForPA procedure definition', GETDATE());