IF OBJECT_ID('dbo.udf_GetMinTransactionalDate', 'FN') IS NOT NULL
	DROP FUNCTION dbo.udf_GetMinTransactionalDate;
GO

CREATE FUNCTION dbo.udf_GetMinTransactionalDate ( @encodedProjectReference VARCHAR(25) )
RETURNS DATE
AS
BEGIN

DECLARE @minTransactionalDate DATE

SELECT @minTransactionalDate = MIN(CANDI_DATE)
FROM (

-- start date from SIGLA
SELECT
a.START_DATE AS CANDI_DATE,
a.SIGLA_PRIMARY_KEY AS PROJECT_REF
FROM
O_PRE_PROJECT_HEADER a
WHERE a.SIGLA_PRIMARY_KEY = @encodedProjectReference

UNION

-- start date from TASKS
SELECT
b.START_DATE AS CANDI_DATE,
a.SIGLA_PRIMARY_KEY AS PROJECT_REF
FROM
O_PRE_PROJECT_HEADER a INNER JOIN
O_PRE_PROJECT_TASKS b ON (a.PROJECT_NAME=b.PROJECT_NAME)
WHERE a.SIGLA_PRIMARY_KEY = @encodedProjectReference

UNION

-- completion date from Revenue Invoice
SELECT
CONVERT(date,a.Completion_Date,103) AS CANDI_DATE,
a.SIGLA_PRIMARY_KEY AS PROJECT_REF
FROM O_STG_PROJECT_REVENUE_INVOICE a
WHERE a.SIGLA_PRIMARY_KEY = @encodedProjectReference

UNION

-- allocated funding date
SELECT
CONVERT(date,a.Date_Allocated,103) AS CANDI_DATE,
a.Agreement_Number AS PROJECT_REF
FROM O_STG_PROJECT_AGREEMENT_FUNDING a
WHERE a.Agreement_Number = @encodedProjectReference

) AS TRANS
GROUP BY TRANS.PROJECT_REF


RETURN @minTransactionalDate

END
GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('PA', 'script', 'updated udf_GetMinTransactionalDate procedure definition', GETDATE());
