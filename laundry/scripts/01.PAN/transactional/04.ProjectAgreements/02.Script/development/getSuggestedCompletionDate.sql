IF OBJECT_ID('dbo.udf_GetSuggestedCompletionDate', 'FN') IS NOT NULL
	DROP FUNCTION dbo.udf_GetSuggestedCompletionDate;
GO

CREATE FUNCTION dbo.udf_GetSuggestedCompletionDate ( @encodedProjectReference VARCHAR(25) )
RETURNS DATE
AS
BEGIN

DECLARE @suggestedCompletionDate DATE

SET @suggestedCompletionDate = DATEADD(year,10, dbo.udf_GetMinTransactionalDate(@encodedProjectReference))

RETURN @suggestedCompletionDate

END

GO
INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('PA', 'script', 'updated udf_GetSuggestedCompletionDate procedure definition', GETDATE());
