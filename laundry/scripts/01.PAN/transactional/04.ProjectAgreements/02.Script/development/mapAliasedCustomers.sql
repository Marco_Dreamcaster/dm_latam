SELECT
b.ORACLE_CUSTOMER_REF as Customer_Number
,Agreement_Number
,[Agreement_Type]
,[Amount]
,[CurrencyCode]
,[RevenueLimitFlag]
,[InvoiceLimitFlag]
,[Administrator_Employee_Ref]
,[Description]
,[DateExpiration]
,[Terms]
,[Operating_Unit_Name]

FROM O_STG_PROJECT_AGREEMENT_HEADER a
INNER JOIN O_STG_CUSTOMER_CROSS_REF_EXT b ON (b.ORIG_SYSTEM_CUSTOMER_REF=a.Customer_Number)
ORDER BY Agreement_Number


