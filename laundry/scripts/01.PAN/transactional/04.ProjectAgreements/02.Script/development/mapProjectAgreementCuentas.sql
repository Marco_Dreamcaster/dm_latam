IF OBJECT_ID('DBO.mapProjectAgreementCuentas') IS NOT NULL DROP PROCEDURE DBO.mapProjectAgreementCuentas
GO

CREATE PROCEDURE DBO.mapProjectAgreementCuentas(@Country nVarchar(2))
AS BEGIN
    SET NOCOUNT ON;
    BEGIN TRANSACTION mapProjectAgreementCuentas_Trans;

    DELETE FROM DBO.mapProjectAgreementCuentas_Trans WHERE COUNTRY = @Country;

    INSERT INTO DBO.mapProjectAgreementCuentas_Trans (Account_Sigla, Country) VALUES ('31101001', @Country);
    INSERT INTO DBO.mapProjectAgreementCuentas_Trans (Account_Sigla, Country) VALUES ('31101002', @Country);
    INSERT INTO DBO.mapProjectAgreementCuentas_Trans (Account_Sigla, Country) VALUES ('31101003', @Country);
    INSERT INTO DBO.mapProjectAgreementCuentas_Trans (Account_Sigla, Country) VALUES ('31101004', @Country);
    INSERT INTO DBO.mapProjectAgreementCuentas_Trans (Account_Sigla, Country) VALUES ('31101005', @Country);
    INSERT INTO DBO.mapProjectAgreementCuentas_Trans (Account_Sigla, Country) VALUES ('31101006', @Country);
    INSERT INTO DBO.mapProjectAgreementCuentas_Trans (Account_Sigla, Country) VALUES ('31101007', @Country);
    INSERT INTO DBO.mapProjectAgreementCuentas_Trans (Account_Sigla, Country) VALUES ('31101008', @Country);
    INSERT INTO DBO.mapProjectAgreementCuentas_Trans (Account_Sigla, Country) VALUES ('31101030', @Country);
    INSERT INTO DBO.mapProjectAgreementCuentas_Trans (Account_Sigla, Country) VALUES ('31102', @Country);
    INSERT INTO DBO.mapProjectAgreementCuentas_Trans (Account_Sigla, Country) VALUES ('31102001', @Country);
    INSERT INTO DBO.mapProjectAgreementCuentas_Trans (Account_Sigla, Country) VALUES ('31102002', @Country);
    INSERT INTO DBO.mapProjectAgreementCuentas_Trans (Account_Sigla, Country) VALUES ('31102003', @Country);
    INSERT INTO DBO.mapProjectAgreementCuentas_Trans (Account_Sigla, Country) VALUES ('31102004', @Country);
    INSERT INTO DBO.mapProjectAgreementCuentas_Trans (Account_Sigla, Country) VALUES ('31102006', @Country);
    INSERT INTO DBO.mapProjectAgreementCuentas_Trans (Account_Sigla, Country) VALUES ('31102007', @Country);
    INSERT INTO DBO.mapProjectAgreementCuentas_Trans (Account_Sigla, Country) VALUES ('31102008', @Country);





    IF @@ERROR != 0 ROLLBACK TRANSACTION mapProjectAgreementCuentas_Trans
    ELSE COMMIT TRANSACTION mapProjectAgreementCuentas_Trans
END
GO
