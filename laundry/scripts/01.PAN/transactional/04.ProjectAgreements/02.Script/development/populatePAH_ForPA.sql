
IF OBJECT_ID('dbo.populatePAH_ForPA', 'P') IS NOT NULL
DROP PROCEDURE DBO.populatePAH_ForPA;
GO

CREATE PROCEDURE DBO.populatePAH_ForPA
AS
BEGIN

	SET NOCOUNT ON;
	BEGIN TRANSACTION populatePAH_ForPA_Trans

	UPDATE TKPA_LATEST..COM set CONTRATO='001-10-14' where contrato='001-1014';
	UPDATE TKPA_LATEST..COM set CONTRATO='N729-1217' where contrato='N29-1217';
	
	DELETE FROM DBO.O_STG_PROJECT_AGREEMENT_HEADER WHERE COUNTRY = 'PA';
	
	EXEC DBO.populatePAH_Sigla;
	EXEC DBO.populatePAH_Full;


    select
max(Customer_Number) as Customer_Number,
max(Agreement_Number)as Agreement_Number,
max(Agreement_Type) as Agreement_Type,
sum(Amount) as Amount,
max(Currency_code) as Currency_code,
max(Revenue_Limit_Flag) as Revenue_Limit_Flag,
max(Invoice_Limit_Flag) as Invoice_Limit_Flag,
max(Administrator_Employee_Reference) as Administrator_Employee_Reference,
Description,
max(Date_Expiration) as Date_Expiration,
max(Terms) as Terms,
max(OU_NAME) as OU_NAME,
max(COUNTRY) as [COUNTRY],
max(OBSERVATION) as [OBSERVATION],
max(SIGLA_PRIMARY_KEY) as [SIGLA_PRIMARY_KEY],
max(LAST_UPDATED) as LAST_UPDATED
into ##tempPAH
from  DBO.O_STG_PROJECT_AGREEMENT_HEADER
group by Description
order by Description

	DELETE FROM DBO.O_STG_PROJECT_AGREEMENT_HEADER WHERE COUNTRY = 'PA';


	INSERT INTO DBO.O_STG_PROJECT_AGREEMENT_HEADER (CUSTOMER_NUMBER, AGREEMENT_NUMBER, AGREEMENT_TYPE, AMOUNT,
		CURRENCYCODE, REVENUELIMITFLAG, INVOICELIMITFLAG, ADMINISTRATOR_EMPLOYEE_REF, DESCRIPTION, DATEEXPIRATION,
		TERMS, OPERATING_UNIT_NAME, COUNTRY, OBSERVATION, SIGLA_PRIMARY_KEY, LAST_UPDATED

	)
 select
Customer_Number,
 Agreement_Number,
Agreement_Type,
 Amount,
Currency_code,
Revenue_Limit_Flag,
Invoice_Limit_Flag,
Administrator_Employee_Reference,
Description,
 Date_Expiration,
Terms,
OU_NAME,
[COUNTRY],
[OBSERVATION],
[SIGLA_PRIMARY_KEY],
LAST_UPDAT
from
##tempPAH



	IF @@ERROR != 0 ROLLBACK TRANSACTION populatePAH_ForPA_Trans
	ELSE COMMIT TRANSACTION populatePAH_ForPA_Trans
END
GO

INSERT INTO DBO.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP) VALUES('PA', 'SCRIPT', 'UPDATED PopulateProjectAgreementHeaderForPA PROCEDURE DEFINITION', GETDATE());
	