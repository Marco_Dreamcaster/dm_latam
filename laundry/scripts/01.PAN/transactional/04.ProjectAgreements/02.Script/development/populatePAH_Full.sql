IF OBJECT_ID('dbo.populatePAH_Full', 'P') IS NOT NULL DROP PROCEDURE DBO.populatePAH_Full
GO

CREATE PROCEDURE DBO.populatePAH_Full
AS BEGIN
	SET NOCOUNT ON
	BEGIN TRANSACTION populatePAH_Full_Trans

    INSERT INTO DBO.O_STG_PROJECT_AGREEMENT_HEADER (CUSTOMER_NUMBER, AGREEMENT_NUMBER, AGREEMENT_TYPE, AMOUNT,
		CURRENCYCODE, REVENUELIMITFLAG, INVOICELIMITFLAG, ADMINISTRATOR_EMPLOYEE_REF, DESCRIPTION, DATEEXPIRATION,
		TERMS, OPERATING_UNIT_NAME, COUNTRY, OBSERVATION, SIGLA_PRIMARY_KEY, LAST_UPDATED
	)
select
'PA' + ltrim(rtrim(a.CLIENTE))+'-CC' as Customer_Number,
dbo.udf_EncodeProjectReference('PA', a.CONTRATO) as Agreement_Number,
'CONSTRUCTION' as Agreement_Type,
round(ROUND(d.[TOT CONTRATO],2)+round(d.[Valor de manto Gratuito],2),2)as Amount,
'USD' as Currency_code,
'Y' as Revenue_Limit_Flag,
'Y' as Invoice_Limit_Flag,
'PADM01' as Administrator_Employee_Reference,
e.PROJECT_NAME as Description,
'' as Date_Expiration	,
case
when ltrim(rtrim(b.IdPago)) = 1 then 'IMMEDIATE'
when ltrim(rtrim(b.IdPago)) = 2 then '30 Days Net'
when ltrim(rtrim(b.IdPago)) = 3 then 'DAY 05'
else '30 Days Net'
end as Terms,
'PA - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME,
'PA' as [COUNTRY],
'loaded from script' as [OBSERVATION],
e.SIGLA_PRIMARY_KEY as [SIGLA_PRIMARY_KEY],
GETDATE() AS LAST_UPDATED
from TKPA_LATEST..CONTRATO as a
left join TKPA_LATEST..PAGOS as b on a.IDPAGO=b.IdPago
inner join contratosPOC as d on a.CONTRATO=d.[COD OBRA]
inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = e.SIGLA_PRIMARY_KEY
where
a.contrato in (select [COD OBRA] from contratosPOC)

    IF @@ERROR != 0 ROLLBACK TRANSACTION populatePAH_Full_Trans
    ELSE COMMIT TRANSACTION populatePAH_Full_Trans

END
GO
