IF OBJECT_ID('dbo.populatePAH_Sigla', 'P') IS NOT NULL DROP PROCEDURE DBO.populatePAH_Sigla
GO

CREATE PROCEDURE DBO.populatePAH_Sigla
AS BEGIN
	SET NOCOUNT ON
	BEGIN TRANSACTION populatePAH_Sigla_Trans

	INSERT INTO DBO.O_STG_PROJECT_AGREEMENT_HEADER (CUSTOMER_NUMBER, AGREEMENT_NUMBER, AGREEMENT_TYPE, AMOUNT,
		CURRENCYCODE, REVENUELIMITFLAG, INVOICELIMITFLAG, ADMINISTRATOR_EMPLOYEE_REF, DESCRIPTION, DATEEXPIRATION,
		TERMS, OPERATING_UNIT_NAME, COUNTRY, OBSERVATION, SIGLA_PRIMARY_KEY, LAST_UPDATED
	)

select
'PA' + ltrim(rtrim(a.CLIENTE))+'-CC' as Customer_Number,
dbo.udf_EncodeProjectReference('PA', a.CONTRATO) as Agreement_Number,
'CONSTRUCTION' as Agreement_Type,
round(g.haber-g.debe,2)as Amount,
'USD' as Currency_code,
'Y' as Revenue_Limit_Flag,
'Y' as Invoice_Limit_Flag,
'PADM01' as Administrator_Employee_Reference,
e.PROJECT_NAME as Description,
'' as Date_Expiration,
case
when ltrim(rtrim(b.IdPago)) = 1 then 'IMMEDIATE'
when ltrim(rtrim(b.IdPago)) = 2 then '30 Days Net'
when ltrim(rtrim(b.IdPago)) = 3 then 'DAY 05'
else '30 Days Net'
end as Terms,
'PA - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME,
'PA' as [COUNTRY],
'loaded from script' as [OBSERVATION],
e.SIGLA_PRIMARY_KEY as [SIGLA_PRIMARY_KEY],
GETDATE() AS LAST_UPDATED

from TKPA_LATEST..CONTRATO as a
left join TKPA_LATEST..PAGOS as b on a.IDPAGO=b.IdPago
inner join (select TOP 1 VENTA from TKPA_LATEST..CAMBIOS where month(FECHA)= MONTH(dateadd(mm,-1,getdate())) order by FECHA desc) as c on 1=1
inner join contratosPOC as d on a.CONTRATO=d.[COD OBRA]
inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = e.SIGLA_PRIMARY_KEY
inner join TKPA_LATEST..COM as g on a.contrato = g.CONTRATO
where
a.contrato in (select [COD OBRA] from contratosPOC)
and g.CTA in ('31101001','31101002','31101003','31101004','31101005','31101006','31101007','31101008','31101030','31102','31102001','31102002','31102003','31102004','31102006','31102007','31102008')
and g.fecha <= convert(datetime, '30-09-2018', 105)

	IF @@ERROR != 0 ROLLBACK TRANSACTION populatePAH_Sigla_Trans
	ELSE COMMIT TRANSACTION populatePAH_Sigla_Trans
END
GO
INSERT INTO DBO.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('PA', 'SCRIPT', 'UPDATED populatePAH_Sigla PROCEDURE DEFINITION', GETDATE());
