IF OBJECT_ID('DBO.generateProjectBudgetHeaderForPA', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateProjectBudgetHeaderForPA;

GO
CREATE PROCEDURE dbo.generateProjectBudgetHeaderForPA
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

EXEC dbo.create_O_STG_PROJECT_costoLocalForPA
EXEC dbo.create_O_STG_PROJECT_conceptoimputacionForPA

select
e.PROJECT_NAME as Project_Name,
'Approved Cost Budget' as Budget_Type,
'ORIGINAL' as Version_Name,
'Original Budget' as Description,
'COST-BY EXP TYPE' as Entry_Method,
'PA-COSTO DE CONSTRUCCION' as Resource_List_Name,
convert(varchar(10),a.REGISTRO,105) as Version_Date,
'PA - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME
from TKPA_LATEST..contrato as a
inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = e.SIGLA_PRIMARY_KEY
where
a.CONTRATO in (select [COD OBRA] from contratosPOC)
union all
select
e.PROJECT_NAME as Project_Name,
'Approved Revenue Budget' as Budget_Type,
'ORIGINAL' as Version_Name,
'Original Budget' as Description,
'REVENUE-TOP TASKS' as Entry_Method,
'PA-INGRESOS DE CONSTRUCCION' as Resource_List_Name,
convert(varchar(10),a.registro,105) as Version_Date,
'PA - THYSSENKRUPP ELEVADORES S.A.' as OU_NAME
from TKPA_LATEST..contrato as a
inner join O_STG_PROJECT_HEADER as e on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = e.SIGLA_PRIMARY_KEY
where
a.CONTRATO in (select [COD OBRA] from contratosPOC)

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

RETURN
END

GO
INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('PA', 'script', 'updated generateProjectBudgetHeaderForPA procedure definition', GETDATE());