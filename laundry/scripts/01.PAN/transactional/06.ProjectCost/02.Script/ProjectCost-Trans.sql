--drop table #tmpCost
 --select * from #tmpCost
select distinct 
	a.contrato,
	'DATA MIGRATION PA' as Transaction_Source,
	g.id as Original_Transaction_Reference,
	e.project_name as project_name,
	case 
		when isnull(j.CLASIFICATION,h.CLASIFICATION)='LABOR' then LEFT('PA_' + CAST (c.IdEquipo AS VARCHAR(max)),20)  +'.01'+'.01'
		when isnull(j.CLASIFICATION,h.CLASIFICATION)='MATERIAL' then LEFT('PA_' + CAST (c.IdEquipo AS VARCHAR(max)),20) +'.02'
		when isnull(j.CLASIFICATION,h.CLASIFICATION)='MISCELLANEOUS' then LEFT('PA_' + CAST (c.IdEquipo AS VARCHAR(max)),20)  +'.03'
		when isnull(j.CLASIFICATION,h.CLASIFICATION)='SUBCONTRACT' then LEFT('PA_' + CAST (c.IdEquipo AS VARCHAR(max)),20)  +'.03'
	end as Task_Number,
	case when convert(varchar(10),a.FECHA,112) 
			between convert(varchar(10),b.FCHMONTAJE,112) and convert(varchar(10),b.FCHENTREGA,112) 
		then convert(varchar(10),a.FECHA,105) 
		else convert(varchar(10),b.FCHENTREGA,105)
	end as Expenditure_Item_Date,
	isnull(j.expenditure, h.expenditure) as Expenditure_Type,
	'GASTOS GENERALES-PA' as Burden_Cost_Expenditure_Type,
	'CIUDAD DE PANAMA' as Organization_Name,
	'PA0' as Employee_Number,
	case 
		when isnull(j.CLASIFICATION,h.CLASIFICATION) ='LABOR' then round(((g.DEBE-g.haber) / 11) / f.cantequipos, 2) else '1' 
	end as Quantity,
	((g.DEBE-g.HABER) / f.cantequipos) as Raw_Cost,
	left(ltrim(rtrim(g.GLOSA)),240) as Expenditure_Comment,
	((g.DEBE-g.HABER) / f.cantequipos) as Burdened_Cost,
	convert(varchar(10),g.FECHA,105) as Accounting_Date,
	'501001.51102095.2100.10.10.000000.000000.000000.000000.274705' as Credit_Account_Code_Combination,
	'501001.51102095.2100.10.10.000000.000000.000000.000000.274705' as Debit_Account_Code_Combination,
	'PA - THYSSENKRUPP ELEVADORES S.A.' as Operating_Unit_Name
into #tmpCost	
from contrato as a
	LEFT join DETCONT as b on a.CONTRATO=b.CONTRATO
	LEFT join EQUIPOS as c on b.IdEquipo=c.IdEquipo
	inner join DM_LATAM..O_STG_PROJECT_HEADER as e on 'PA_'+replace(replace(a.CONTRATO,'-','_'),' ','_') = e.SIGLA_PRIMARY_KEY
	inner join (select COUNT(idequipo) as cantequipos,contrato from DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
	inner join COM as g on a.contrato = g.CONTRATO
	inner join TITCOM as i on g.IdComp=i.IdComp
	inner join newtipcos as h on g.TIPCOS=h.TIPCOS
	left join newconceptoimputacion as j on g.CONIMPUTACION=j.conimputacion
where
--a.CONTRATO='001-10-14' and
g.CTA in ('41101001','41101002','41104001','41104002','41107001','41107002','41107003','41107004','41107005','41107006')
and a.contrato in (select [COD OBRA] from DM_LATAM..contratosPOC )
and g.fecha <= '31-07-2018'
and g.TIPCOS <> 'O05' 

select 
	max(Transaction_Source) as Transaction_Source,'|' as '|',
	max(Original_Transaction_Reference) as Original_Transaction_Reference,'|' as '|',
	Project_Name,'|' as '|',
	Task_Number,'|' as '|',
	max(Expenditure_Item_Date) as Expenditure_Item_Date,'|' as '|',
	Expenditure_Type,'|' as '|',
	max(Burden_Cost_Expenditure_Type) as Burden_Cost_Expenditure_Type ,'|' as '|',
	max(Organization_Name) as Organization_Name,'|' as '|',
	max(Employee_Number) as Employee_Number,'|' as '|',
	sum(Quantity) as Quantity ,'|' as '|',
	sum(Raw_Cost) as Raw_Cost ,'|' as '|',
	max(Expenditure_Comment) as Expenditure_Comment,'|' as '|',
	sum(Burdened_Cost) as Burdened_Cost,'|' as '|',
	max(Accounting_Date) as Accounting_Date,'|' as '|',
	max(Credit_Account_Code_Combination) as Credit_Account_Code_Combination,'|' as '|',
	max(Debit_Account_Code_Combination) as Debit_Account_Code_Combination,'|' as '|',
	max(Operating_Unit_Name) as Operating_Unit_Name
from #tmpCost
--where CONTRATO='01/10/14-a'
group by Project_Name,Task_Number,Expenditure_Type
order by project_name,Task_Number,Expenditure_Type
