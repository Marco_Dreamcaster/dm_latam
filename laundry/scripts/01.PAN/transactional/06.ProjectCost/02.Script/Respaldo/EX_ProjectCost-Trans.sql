--drop table #tmpCost
select --distinct a.CONTRATO
	max(j.conimputacion) as conimputacion,
	'DATA MIGRATION PA' as Transaction_Source,
	max(c.id) as Original_Transaction_Reference,
	--LEFT('PA' + '-' + RTRIM(LTRIM(CAST(A.CONTRATO AS VARCHAR))) + '-' + MAX(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(A.OBRA)),'�','a'),'�','e'),'�','i'),'�','o'),'�','u')), 30) as Project_Name,'|' as '|',
	--UPPER(LEFT('PA' + '-' + max(RTRIM(LTRIM(CAST(A.CONTRATO AS VARCHAR)))) + '-' + max(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(A.OBRA)),'�','a'),'�','e'),'�','i'),'�','o'),'�','u'),'�','n')), 30)) as Project_Name,
	max(o.project_name) as project_name,
	case 
	when j.CLASIFICACION='LABOR' then UPPER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LEFT(max(RTRIM(LTRIM(i.OE))),19),' ','_'),'-','_'),'�','a'),'�','e'),'�','i'),'�','o'),'�','u'),'�','n')) +'.01'+'.01'
	when j.CLASIFICACION='MATERIAL' then UPPER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LEFT(max(RTRIM(LTRIM(i.OE))),19),' ','_'),'-','_'),'�','a'),'�','e'),'�','i'),'�','o'),'�','u'),'�','n'))+'.02'
	when j.CLASIFICACION='MISCELLANEOUS' then UPPER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LEFT(max(RTRIM(LTRIM(i.OE))),19),' ','_'),'-','_'),'�','a'),'�','e'),'�','i'),'�','o'),'�','u'),'�','n')) +'.03'
	when j.CLASIFICACION='SUBCONTRACT' then UPPER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LEFT(max(RTRIM(LTRIM(i.OE))),19),' ','_'),'-','_'),'�','a'),'�','e'),'�','i'),'�','o'),'�','u'),'�','n')) +'.03'
	end as Task_Number,
	--max(c.FECHA),max(i.FCHMONTAJE),max(i.FCHENTREGA),
	/*inicio cambio de fecha*/
	--max(convert(varchar(10),c.FECHA,105)) as Expenditure_Item_Date,'|' as '|',
	case when convert(varchar(10),max(c.FECHA),112) 
			between convert(varchar(10),max(i.FCHMONTAJE),112) and convert(varchar(10),max(i.FCHENTREGA),112) 
		then convert(varchar(10),max(c.FECHA),105) 
		else convert(varchar(10),max(i.FCHENTREGA),105)
	end as Expenditure_Item_Date,
	/*Fin cambio de fecha*/
	j.expenditure as Expenditure_Type,
	'GASTOS GENERALES-PA' as Burden_Cost_Expenditure_Type,
	'CIUDAD DE PANAMA' as Organization_Name,
	'PA0' as Employee_Number,
	'1' as Quantity,
	sum(c.DEBE-c.HABER) as Raw_Cost,
	MAX(left(ltrim(rtrim(c.GLOSA)),240)) as Expenditure_Comment,
	sum(c.DEBE-c.HABER) as Burdened_Cost,
	max(convert(varchar(10),c.FECHA,105)) as Accounting_Date,
	--max(k.localoracle)+'.'+max(l.ctaoracle)+'.'+max(m.tipcosoracle)+'.'+max(n.tipooracle)+'.'+'00.000000.000000.000000.000000.274705' as Credit_Account_Code_Combination,'|' as '|',
	--max(k.localoracle)+'.'+max(l.ctaoracle)+'.'+max(m.tipcosoracle)+'.'+max(n.tipooracle)+'.'+'00.000000.000000.000000.000000.274705' as Debit_Account_Code_Combination,'|' as '|',
	--max(k.localoracle)+'.51102095.2100.10.10.000000.000000.000000.000000.274705' as Credit_Account_Code_Combination,'|' as '|',
	--max(k.localoracle)+'.51102095.2100.10.10.000000.000000.000000.000000.274705' as Debit_Account_Code_Combination,'|' as '|',
	'501001.51102095.2100.10.10.000000.000000.000000.000000.274705' as Credit_Account_Code_Combination,
	'501001.51102095.2100.10.10.000000.000000.000000.000000.274705' as Debit_Account_Code_Combination,
	'PA - THYSSENKRUPP ELEVADORES S.A.' as Operating_Unit_Name
into #tmpCost
from contrato as a
	left join com as c on a.CONTRATO = c.CONTRATO and a.TIPOCONT in ('01','10')
	left join TIPCOS as d on c.TIPCOS=d.TIPCOS
	left join CONCEPTOIMPUTACION as e on c.CONIMPUTACION=e.CONIMPUTACION
	left join TITCOM as f on c.IdComp=f.IdComp
	left join DIARIOS as g on f.DIARIO=g.DIARIO
	left join CUENTAS as h on  c.cta=h.CTA
	inner join (select b.oe,a.contrato,b.IdEquipo,a.FCHENTREGA,a.FCHMONTAJE from detcont as a inner join EQUIPOS as b on a.IdEquipo=b.IdEquipo) as i
		on a.CONTRATO=i.CONTRATO and c.OE = i.OE	
	inner join NEWconceptoimputacion as j on c.conimputacion=j.conimputacion --and j.conimputacion <> '501'
	left join newlocales as k on a.LOCAL=k.local
	left join newCUENTAS as l on c.CTA=l.cta
	left join newTIPCOS as m on c.TIPCOS=m.tipcos
	left join NEWtipocont as n on a.TIPOCONT=n.tipo
	inner join DM_LATAM..O_STG_PROJECT_HEADER as o on 'PA_'+replace(replace(a.CONTRATO,'-','_'),' ','_') = o.SIGLA_PRIMARY_KEY
where c.CTA like '%12102%' and
--j.conimputacion <> '501' and
a.CONTRATO in
(
/*AGRUPADO*/
select 
--f.cta, f.DESCRIP,b.DESCRIP,c.CONIMPUTACION,* 
distinct a.CONTRATO
from com as a
	left join TIPCOS as b on a.TIPCOS=b.TIPCOS
	left join CONCEPTOIMPUTACION as c on a.CONIMPUTACION=c.CONIMPUTACION
	left join TITCOM as d on a.IdComp=d.IdComp
	left join DIARIOS as e on d.DIARIO=e.DIARIO
	left join CUENTAS as f on  a.cta=f.CTA
	where --TIPOCONT in ('01','10') and 
	CONTRATO <> ''
and a.CTA like '%12102%'
group by a.contrato
having sum (debe-haber)<> 0 and not sum (debe-haber) between -200 and 200
--------------------------------
and a.contrato in 
( select contrato from contratosRFM))
and c.OE <> ''  --and a.contrato='714-0417'
group by a.CONTRATO,i.idequipo,c.CTA,j.clasificacion,j.expenditure
having sum(c.DEBE-c.HABER) not between -0.45 and 0.45 and sum(c.DEBE-c.HABER) <> 0
order by a.CONTRATO,i.idequipo,j.CLASIFICACION

--'|' as '|',
select 
	Transaction_Source,'|' as '|',
	Original_Transaction_Reference,'|' as '|',
	Project_Name,'|' as '|',
	Task_Number,'|' as '|',
	Expenditure_Item_Date,'|' as '|',
	Expenditure_Type,'|' as '|',
	Burden_Cost_Expenditure_Type,'|' as '|',
	Organization_Name,'|' as '|',
	Employee_Number,'|' as '|',
	Quantity,'|' as '|',
	Raw_Cost,'|' as '|',
	Expenditure_Comment,'|' as '|',
	Burdened_Cost,'|' as '|',
	Accounting_Date,'|' as '|',
	Credit_Account_Code_Combination,'|' as '|',
	Debit_Account_Code_Combination,'|' as '|',
	Operating_Unit_Name
from #tmpCost	
where conimputacion <> '501'