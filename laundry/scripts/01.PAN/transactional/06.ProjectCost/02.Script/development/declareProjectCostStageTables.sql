IF OBJECT_ID('O_STG_PROJECT_COSTS') IS NOT NULL DROP TABLE O_STG_PROJECT_COSTS

CREATE TABLE O_STG_PROJECT_COSTS(
	Transaction_Source VARCHAR(30),
	Original_Transaction_Reference VARCHAR(30),
	Project_Name VARCHAR(30),
	Task_Number VARCHAR(25),
	Expenditure_Item_Date varchar(10),
	Expenditure_Type VARCHAR(30),
	Burden_Cost_Expenditure_Type VARCHAR(30),
	Organization_Name VARCHAR(240),
	Employee_Number VARCHAR(30),
	Quantity FLOAT ,
	Raw_Cost  FLOAT ,
	Expenditure_Comment VARCHAR(240),
	Burdened_Cost FLOAT,
	Accounting_Date varchar(10),
	Credit_Account_Code_Combination VARCHAR(150),
	Debit_Account_Code_Combination VARCHAR(150),
	Operating_Unit_Name VARCHAR(240),
	[COUNTRY] [nvarchar](2) NOT NULL,
	[OBSERVATION] [nvarchar](3000) NULL,
	[SIGLA_PRIMARY_KEY] [nvarchar](60) NULL,
	[LAST_UPDATED] [datetime2](6) NOT NULL
)

GO
INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('PA', 'script', 'updated O_STG_PROJECT_COSTS table definition', GETDATE());
