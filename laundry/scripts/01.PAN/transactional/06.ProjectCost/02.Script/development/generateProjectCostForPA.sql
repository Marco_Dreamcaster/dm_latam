IF OBJECT_ID('DBO.generateProjectCostForPA', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateProjectCostForPA;

GO

CREATE PROCEDURE dbo.generateProjectCostForPA
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

EXEC dbo.createO_STG_PROJECT_conceptoimputacionForPA
EXEC dbo.createO_STG_PROJECT_tipcosForPA

IF OBJECT_ID('tempdb..#tmpCost') IS NOT NULL DROP TABLE #tmpCost

select distinct
a.contrato,
'DATA MIGRATION PA' as Transaction_Source,
g.id as Original_Transaction_Reference,
e.project_name as project_name,
case
when isnull(j.CLASIFICACION,h.CLASIFICACION)='LABOR' then LEFT('PA_' + CAST (c.IdEquipo AS VARCHAR(max)),20)  +'.01'+'.01'
when isnull(j.CLASIFICACION,h.CLASIFICACION)='MATERIAL' then LEFT('PA_' + CAST (c.IdEquipo AS VARCHAR(max)),20) +'.02'
when isnull(j.CLASIFICACION,h.CLASIFICACION)='MISCELLANEOUS' then LEFT('PA_' + CAST (c.IdEquipo AS VARCHAR(max)),20)  +'.03'
when isnull(j.CLASIFICACION,h.CLASIFICACION)='SUBCONTRACT' then LEFT('PA_' + CAST (c.IdEquipo AS VARCHAR(max)),20)  +'.03'
end as Task_Number,
convert(varchar(10),g.FECHA,105) as EXPENDITURE_Item_Date,
isnull(j.EXPENDITURE, h.EXPENDITURE) as EXPENDITURE_Type,
'GASTOS GENERALES-PA' as Burden_Cost_EXPENDITURE_Type,
'CIUDAD DE PANAMA' as Organization_Name,
'PADM01' as Employee_Number,
case
when isnull(j.CLASIFICACION,h.CLASIFICACION) ='LABOR' then round(((g.DEBE-g.haber) / 11) / f.cantequipos, 2) else '1'
end as Quantity,
((g.DEBE-g.HABER) / f.cantequipos) as Raw_Cost,
UPPER(REPLACE(REPLACE(left(ltrim(rtrim(g.GLOSA)),240), CHAR(13), ''), CHAR(10), '')) COLLATE sql_latin1_general_cp1251_ci_as as EXPENDITURE_Comment,
((g.DEBE-g.HABER) / f.cantequipos) as Burdened_Cost,
convert(varchar(10),g.FECHA,105) as Accounting_Date,
'501001.51102095.2100.10.10.000000.000000.000000.000000.274705' as Credit_Account_Code_Combination,
'501001.51102095.2100.10.10.000000.000000.000000.000000.274705' as Debit_Account_Code_Combination,
'PA - THYSSENKRUPP ELEVADORES S.A.' as Operating_Unit_Name
into #tmpCost
from TKPA_LATEST.dbo.contrato as a
LEFT join TKPA_LATEST.dbo.DETCONT as b on a.CONTRATO=b.CONTRATO
LEFT join TKPA_LATEST.dbo.EQUIPOS as c on b.IdEquipo=c.IdEquipo
inner join O_STG_PROJECT_HEADER as e on REPLACE(REPLACE(LEFT(RTRIM(LTRIM('PA_' + CAST(a.CONTRATO AS VARCHAR))), 16),' ','_'),'-','_') = e.SIGLA_PRIMARY_KEY
inner join O_STG_PROJECT_TASKS as pt on (LEFT('PA_' + CAST (c.IdEquipo AS VARCHAR(max)),20) = pt.TASK_NUMBER AND e.PROJECT_NAME=pt.PROJECT_NAME)
inner join (select COUNT(idequipo) as cantequipos,contrato from TKPA_LATEST.dbo.DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
inner join TKPA_LATEST.dbo.COM as g on a.contrato = g.CONTRATO
inner join TKPA_LATEST.dbo.TITCOM as i on g.IdComp=i.IdComp
inner join ##O_STG_PROJECT_tipcos as h on g.TIPCOS=h.TIPCOS
left join ##O_STG_PROJECT_conceptoimputacion as j on g.CONIMPUTACION=j.conimputacion
where
g.CTA in ('41101001','41101002','41104001','41104002','41107001','41107002','41107003','41107004','41107005','41107006')
and e.DISMISSED=0
and e.CROSSREF_VALIDATED=1
and e.LOCAL_VALIDATED=1
and e.COUNTRY='PA'
--and a.contrato in (select [COD OBRA] from contratosPOC )
and g.fecha <= convert(datetime,'30-09-2018',103)
and g.TIPCOS <> 'O05'

select
max(Transaction_Source) as Transaction_Source,
max(Original_Transaction_Reference) as Original_Transaction_Reference,
Project_Name,
Task_Number,
max(Expenditure_Item_Date) as Expenditure_Item_Date,
Expenditure_Type,
max(Burden_Cost_Expenditure_Type) as Burden_Cost_Expenditure_Type ,
max(Organization_Name) as Organization_Name,
max(Employee_Number) as Employee_Number,
sum(Quantity) as Quantity ,
sum(Raw_Cost) as Raw_Cost ,
max(Expenditure_Comment) as Expenditure_Comment,
sum(Burdened_Cost) as Burdened_Cost,
max(Accounting_Date) as Accounting_Date,
max(Credit_Account_Code_Combination) as Credit_Account_Code_Combination,
max(Debit_Account_Code_Combination) as Debit_Account_Code_Combination,
max(Operating_Unit_Name) as Operating_Unit_Name
from #tmpCost
--where CONTRATO='01/10/14-a'
group by Project_Name,Task_Number,Expenditure_Type
order by project_name,Task_Number,Expenditure_Type

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

RETURN
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('PA', 'script', 'updated generateProjectCostForPA procedure definition', GETDATE());