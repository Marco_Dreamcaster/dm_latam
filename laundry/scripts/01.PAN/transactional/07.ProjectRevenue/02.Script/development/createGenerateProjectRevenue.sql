IF OBJECT_ID('dbo.udf_EncodeProjectTask', 'FN') IS NOT NULL
	DROP FUNCTION dbo.udf_EncodeProjectTask;

GO

CREATE FUNCTION dbo.udf_EncodeProjectTask ( @country VARCHAR(2), @idEquipo VARCHAR(20) )
RETURNS VARCHAR(25)
AS
BEGIN

DECLARE @projectTask VARCHAR(25)

SET @projectTask = UPPER(LEFT(@country+'_' + CAST (@idEquipo AS VARCHAR(max)),20));

RETURN @projectTask
END

GO

IF OBJECT_ID('DBO.populateProjectRevenueForPA', 'P') IS NOT NULL
	DROP PROCEDURE dbo.populateProjectRevenueForPA;

GO

CREATE PROCEDURE dbo.populateProjectRevenueForPA
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

DECLARE @lastUpdated DATE

SET @lastUpdated=GETDATE()

select
round((sum(b.haber-b.debe) / max(f.cantequipos)),2) as Revenue_Amount,
round((sum(b.haber-b.debe) / max(f.cantequipos)),2) as Invoice_Bill_Amount,
c.project_name as Project_Name,
dbo.udf_EncodeProjectTask('PA', e.IdEquipo)  as Top_Task_Number,
convert(varchar(10),max(b.FECHA),105)  as Completion_Date,
'DATA MIGRATION' as Description,
'PA - THYSSENKRUPP ELEVADORES S.A.' as Operating_Unit_Name,
'PA' as [COUNTRY],
'loaded from script' as [OBSERVATION],
c.SIGLA_PRIMARY_KEY as [SIGLA_PRIMARY_KEY],
@lastUpdated AS LAST_UPDATED

from TKPA_LATEST..contrato as a
inner join TKPA_LATEST..com as b on a.CONTRATO = b.CONTRATO and b.TIPOCONT in ('01','10')
inner join O_STG_PROJECT_HEADER as c on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = c.SIGLA_PRIMARY_KEY
inner join TKPA_LATEST..DETCONT as d on a.CONTRATO=d.CONTRATO
inner join TKPA_LATEST..EQUIPOS as e on d.IdEquipo=e.IdEquipo
inner join (select COUNT(idequipo) as cantequipos,contrato from TKPA_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
where
b.CTA in ('31101001','31101002','31101003','31101004','31101005','31101006','31101007','31101008','31101030','31102','31102001','31102002','31102003','31102004','31102006','31102007','31102008')
and b.CONTRATO in (select [COD OBRA] from contratosPOC )
and b.fecha <= convert(datetime, '31-08-2018', 105)
and b.TIPCOS <> 'O05'
group by c.PROJECT_NAME,e.IdEquipo, COUNTRY, OBSERVATION, SIGLA_PRIMARY_KEY, LAST_UPDATED
having round((sum(b.haber-b.debe) / max(f.cantequipos)),2) >0
order by c.PROJECT_NAME

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

RETURN
END

GO


IF OBJECT_ID('DBO.generateProjectRevenueForPA', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateProjectRevenueForPA;

GO

CREATE PROCEDURE dbo.generateProjectRevenueForPA
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

DELETE FROM O_STG_PROJECT_REVENUE_INVOICE WHERE COUNTRY='PA'

INSERT INTO O_STG_PROJECT_REVENUE_INVOICE
EXEC dbo.populateProjectRevenueForPA

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION


SELECT 
	Revenue_Amount,
	Invoice_Bill_Amount,
	Project_Name,
	Top_Task_Number,
	Completion_Date,
	Descriptions,
	Operating_Unit_Name
FROM O_STG_PROJECT_REVENUE_INVOICE
WHERE COUNTRY='PA'



RETURN
END

GO

EXEC dbo.generateProjectRevenueForPA