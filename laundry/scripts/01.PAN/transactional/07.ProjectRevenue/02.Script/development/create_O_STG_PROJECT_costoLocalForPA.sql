IF OBJECT_ID('DBO.create_O_STG_PROJECT_costoLocalForPA', 'P') IS NOT NULL
DROP PROCEDURE dbo.create_O_STG_PROJECT_costoLocalForPA;
GO

CREATE PROCEDURE dbo.create_O_STG_PROJECT_costoLocalForPA
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

IF OBJECT_ID('tempdb..##O_STG_PROJECT_costoLocal') IS NOT NULL DROP TABLE ##O_STG_PROJECT_costoLocal

select * INTO ##O_STG_PROJECT_costoLocal from TKPA_LATEST..costolocal
alter table ##O_STG_PROJECT_costoLocal add CLASIFICACION varchar(100)
alter table ##O_STG_PROJECT_costoLocal add EXPENDITURE varchar(100)

update ##O_STG_PROJECT_costoLocal set clasificacion='' where id='01'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='0101'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='0102'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='0103'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='0104'
update ##O_STG_PROJECT_costoLocal set clasificacion='' where id='02'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0201'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0202'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0203'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0204'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0205'
update ##O_STG_PROJECT_costoLocal set clasificacion='' where id='03'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0301'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0302'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0303'
update ##O_STG_PROJECT_costoLocal set clasificacion='' where id='04'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0401'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0402'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0403'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0404'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0405'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0406'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0407'
update ##O_STG_PROJECT_costoLocal set clasificacion='SUBCONTRACT' where id='0408'
update ##O_STG_PROJECT_costoLocal set clasificacion='SUBCONTRACT' where id='0409'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0410'
update ##O_STG_PROJECT_costoLocal set clasificacion='' where id='05'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0501'
update ##O_STG_PROJECT_costoLocal set clasificacion='LABOR' where id='0502'
update ##O_STG_PROJECT_costoLocal set clasificacion='LABOR' where id='0503'
update ##O_STG_PROJECT_costoLocal set clasificacion='LABOR' where id='0504'
update ##O_STG_PROJECT_costoLocal set clasificacion='LABOR' where id='0505'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='0506'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='0507'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='0508'
update ##O_STG_PROJECT_costoLocal set clasificacion='SUBCONTRACT' where id='0509'
update ##O_STG_PROJECT_costoLocal set clasificacion='SUBCONTRACT' where id='0510'
update ##O_STG_PROJECT_costoLocal set clasificacion='LABOR' where id='0511'
update ##O_STG_PROJECT_costoLocal set clasificacion='LABOR' where id='0512'
update ##O_STG_PROJECT_costoLocal set clasificacion='LABOR' where id='0513'
update ##O_STG_PROJECT_costoLocal set clasificacion='LABOR' where id='0514'
update ##O_STG_PROJECT_costoLocal set clasificacion='LABOR' where id='0515'
update ##O_STG_PROJECT_costoLocal set clasificacion='LABOR' where id='0516'
update ##O_STG_PROJECT_costoLocal set clasificacion='LABOR' where id='0517'
update ##O_STG_PROJECT_costoLocal set clasificacion='LABOR' where id='0518'
update ##O_STG_PROJECT_costoLocal set clasificacion='LABOR' where id='0519'
update ##O_STG_PROJECT_costoLocal set clasificacion='' where id='06'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0601'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0602'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0603'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0604'
update ##O_STG_PROJECT_costoLocal set clasificacion='' where id='07'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0701'
update ##O_STG_PROJECT_costoLocal set clasificacion='LABOR' where id='0702'
update ##O_STG_PROJECT_costoLocal set clasificacion='' where id='08'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0801'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0802'
update ##O_STG_PROJECT_costoLocal set clasificacion='' where id='09'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0901'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0902'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='0903'
update ##O_STG_PROJECT_costoLocal set clasificacion='' where id='10'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1001'
update ##O_STG_PROJECT_costoLocal set clasificacion='' where id='11'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1101'
update ##O_STG_PROJECT_costoLocal set clasificacion='' where id='12'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1201'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1202'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1203'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1204'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1205'
update ##O_STG_PROJECT_costoLocal set clasificacion='' where id='13'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1301'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1302'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1303'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1304'
update ##O_STG_PROJECT_costoLocal set clasificacion='' where id='14'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='1401'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='1402'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1403'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1404'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='1405'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='1406'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='1407'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='1408'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='1409'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='1410'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='1411'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='1412'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1413'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='1414'
update ##O_STG_PROJECT_costoLocal set clasificacion='LABOR' where id='1415'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1416'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='1417'
update ##O_STG_PROJECT_costoLocal set clasificacion='SUBCONTRACT' where id='1418'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='1419'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='1420'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1421'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1422'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1423'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1424'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='1425'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1426'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1427'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1428'
update ##O_STG_PROJECT_costoLocal set clasificacion='' where id='15'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1501'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1502'
update ##O_STG_PROJECT_costoLocal set clasificacion='' where id='16'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1601'
update ##O_STG_PROJECT_costoLocal set clasificacion='' where id='17'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1701'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1702'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='1703'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='1901'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='2001'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='2002'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='2003'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='2004'
update ##O_STG_PROJECT_costoLocal set clasificacion='MATERIAL' where id='2005'
update ##O_STG_PROJECT_costoLocal set clasificacion='MISCELLANEOUS' where id='2101'

update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIAL IMPORTADO-PA' where id='0101'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIAL IMPORTADO-PA' where id='0102'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIAL IMPORTADO-PA' where id='0103'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIAL IMPORTADO-PA' where id='0104'
update ##O_STG_PROJECT_costoLocal set Expenditure='NACIONALIZACION-PA' where id='0201'
update ##O_STG_PROJECT_costoLocal set Expenditure='FLETE DE MATERIALES-PA' where id='0202'
update ##O_STG_PROJECT_costoLocal set Expenditure='FLETE DE MATERIALES-PA' where id='0203'
update ##O_STG_PROJECT_costoLocal set Expenditure='FLETE DE MATERIALES-PA' where id='0204'
update ##O_STG_PROJECT_costoLocal set Expenditure='ADICIONALES DE INSTALACION-PA' where id='0205'
update ##O_STG_PROJECT_costoLocal set Expenditure='DESCARGUE MATERIALES-PA' where id='0301'
update ##O_STG_PROJECT_costoLocal set Expenditure='DESCARGUE MATERIALES-PA' where id='0302'
update ##O_STG_PROJECT_costoLocal set Expenditure='DESCARGUE MATERIALES-PA' where id='0303'
update ##O_STG_PROJECT_costoLocal set Expenditure='GASTOS DE VIAJE-PA' where id='0401'
update ##O_STG_PROJECT_costoLocal set Expenditure='GASTOS DE VIAJE-PA' where id='0402'
update ##O_STG_PROJECT_costoLocal set Expenditure='GASTOS DE VIAJE-PA' where id='0403'
update ##O_STG_PROJECT_costoLocal set Expenditure='GASTOS DE VIAJE-PA' where id='0404'
update ##O_STG_PROJECT_costoLocal set Expenditure='GASTOS DE VIAJE-PA' where id='0405'
update ##O_STG_PROJECT_costoLocal set Expenditure='GASTOS DE VIAJE-PA' where id='0406'
update ##O_STG_PROJECT_costoLocal set Expenditure='GASTOS DE VIAJE-PA' where id='0407'
update ##O_STG_PROJECT_costoLocal set Expenditure='SUBCONTRATO-PA' where id='0408'
update ##O_STG_PROJECT_costoLocal set Expenditure='SUBCONTRATO-PA' where id='0409'
update ##O_STG_PROJECT_costoLocal set Expenditure='GASTOS DE VIAJE-PA' where id='0410'
update ##O_STG_PROJECT_costoLocal set Expenditure='ADICIONALES DE INSTALACION-PA' where id='0501'
update ##O_STG_PROJECT_costoLocal set Expenditure='HORAS REGULARES-PA' where id='0502'
update ##O_STG_PROJECT_costoLocal set Expenditure='HORAS REGULARES-PA' where id='0503'
update ##O_STG_PROJECT_costoLocal set Expenditure='HORAS REGULARES-PA' where id='0504'
update ##O_STG_PROJECT_costoLocal set Expenditure='HORAS REGULARES-PA' where id='0505'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='0506'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='0507'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='0508'
update ##O_STG_PROJECT_costoLocal set Expenditure='SUBCONTRATO-PA' where id='0509'
update ##O_STG_PROJECT_costoLocal set Expenditure='SUBCONTRATO-PA' where id='0510'
update ##O_STG_PROJECT_costoLocal set Expenditure='HORAS REGULARES-PA' where id='0511'
update ##O_STG_PROJECT_costoLocal set Expenditure='HORAS REGULARES-PA' where id='0512'
update ##O_STG_PROJECT_costoLocal set Expenditure='HORAS REGULARES-PA' where id='0513'
update ##O_STG_PROJECT_costoLocal set Expenditure='HORAS REGULARES-PA' where id='0514'
update ##O_STG_PROJECT_costoLocal set Expenditure='HORAS REGULARES-PA' where id='0515'
update ##O_STG_PROJECT_costoLocal set Expenditure='HORAS REGULARES-PA' where id='0516'
update ##O_STG_PROJECT_costoLocal set Expenditure='HORAS REGULARES-PA' where id='0517'
update ##O_STG_PROJECT_costoLocal set Expenditure='HORAS REGULARES-PA' where id='0518'
update ##O_STG_PROJECT_costoLocal set Expenditure='HORAS REGULARES-PA' where id='0519'
update ##O_STG_PROJECT_costoLocal set Expenditure='NACIONALIZACION-PA' where id='0601'
update ##O_STG_PROJECT_costoLocal set Expenditure='NACIONALIZACION-PA' where id='0602'
update ##O_STG_PROJECT_costoLocal set Expenditure='NACIONALIZACION-PA' where id='0603'
update ##O_STG_PROJECT_costoLocal set Expenditure='NACIONALIZACION-PA' where id='0604'
update ##O_STG_PROJECT_costoLocal set Expenditure='MANTENIMIENTO GRATUITO-PA' where id='0701'
update ##O_STG_PROJECT_costoLocal set Expenditure='HORAS REGULARES-PA' where id='0702'
update ##O_STG_PROJECT_costoLocal set Expenditure='ADICIONALES DE INSTALACION-PA' where id='0801'
update ##O_STG_PROJECT_costoLocal set Expenditure='ADICIONALES DE INSTALACION-PA' where id='0802'
update ##O_STG_PROJECT_costoLocal set Expenditure='SEGURO-PA' where id='0901'
update ##O_STG_PROJECT_costoLocal set Expenditure='SEGURO-PA' where id='0902'
update ##O_STG_PROJECT_costoLocal set Expenditure='SEGURO-PA' where id='0903'
update ##O_STG_PROJECT_costoLocal set Expenditure='IMPREVISTOS-PA' where id='1001'
update ##O_STG_PROJECT_costoLocal set Expenditure='ADICIONALES DE INSTALACION-PA' where id='1101'
update ##O_STG_PROJECT_costoLocal set Expenditure='HERRAMIENTAS-PA' where id='1201'
update ##O_STG_PROJECT_costoLocal set Expenditure='HERRAMIENTAS-PA' where id='1202'
update ##O_STG_PROJECT_costoLocal set Expenditure='HERRAMIENTAS-PA' where id='1203'
update ##O_STG_PROJECT_costoLocal set Expenditure='HERRAMIENTAS-PA' where id='1204'
update ##O_STG_PROJECT_costoLocal set Expenditure='HERRAMIENTAS-PA' where id='1205'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES DE DECORACION-PA' where id='1301'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES DE DECORACION-PA' where id='1302'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES DE DECORACION-PA' where id='1303'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES DE DECORACION-PA' where id='1304'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='1401'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='1402'
update ##O_STG_PROJECT_costoLocal set Expenditure='OBRAS CIVILES-PA' where id='1403'
update ##O_STG_PROJECT_costoLocal set Expenditure='ADICIONALES DE INSTALACION-PA' where id='1404'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='1405'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='1406'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='1407'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='1408'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='1409'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='1410'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='1411'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='1412'
update ##O_STG_PROJECT_costoLocal set Expenditure='ADICIONALES DE INSTALACION-PA' where id='1413'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='1414'
update ##O_STG_PROJECT_costoLocal set Expenditure='CAPACITACION LABORAL-PA' where id='1415'
update ##O_STG_PROJECT_costoLocal set Expenditure='SEGURO-PA' where id='1416'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES DE DECORACION-PA' where id='1417'
update ##O_STG_PROJECT_costoLocal set Expenditure='SUBCONTRATO-PA' where id='1418'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='1419'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='1420'
update ##O_STG_PROJECT_costoLocal set Expenditure='ADICIONALES DE INSTALACION-PA' where id='1421'
update ##O_STG_PROJECT_costoLocal set Expenditure='ADICIONALES DE INSTALACION-PA' where id='1422'
update ##O_STG_PROJECT_costoLocal set Expenditure='ADICIONALES DE INSTALACION-PA' where id='1423'
update ##O_STG_PROJECT_costoLocal set Expenditure='ADICIONALES DE INSTALACION-PA' where id='1424'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='1425'
update ##O_STG_PROJECT_costoLocal set Expenditure='ADICIONALES DE INSTALACION-PA' where id='1426'
update ##O_STG_PROJECT_costoLocal set Expenditure='ADICIONALES DE INSTALACION-PA' where id='1427'
update ##O_STG_PROJECT_costoLocal set Expenditure='ADICIONALES DE INSTALACION-PA' where id='1428'
update ##O_STG_PROJECT_costoLocal set Expenditure='No va en los custos de Obra! ' where id='1501'
update ##O_STG_PROJECT_costoLocal set Expenditure='ADICIONALES DE INSTALACION-PA' where id='1502'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='1601'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='1701'
update ##O_STG_PROJECT_costoLocal set Expenditure='ADICIONALES DE INSTALACION-PA' where id='1702'
update ##O_STG_PROJECT_costoLocal set Expenditure='No va en los custos de Obra! ' where id='1703'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='1901'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='2001'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='2002'
update ##O_STG_PROJECT_costoLocal set Expenditure='ADICIONALES DE INSTALACION-PA' where id='2003'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES DE DECORACION-PA' where id='2004'
update ##O_STG_PROJECT_costoLocal set Expenditure='MATERIALES LOCALES-PA' where id='2005'
update ##O_STG_PROJECT_costoLocal set Expenditure='FLETE DE MATERIALES-PA' where id='2101'

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

RETURN
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('PA', 'script', 'updated create_O_STG_PROJECT_costoLocalForPA auxiliary mapping table definition', GETDATE());