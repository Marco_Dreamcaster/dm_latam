IF OBJECT_ID('DBO.generateProjectRevenueForPA', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateProjectRevenueForPA;

GO

CREATE PROCEDURE dbo.generateProjectRevenueForPA
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

DELETE FROM O_STG_PROJECT_REVENUE_INVOICE WHERE COUNTRY='PA'

INSERT INTO O_STG_PROJECT_REVENUE_INVOICE
EXEC dbo.populateProjectRevenueForPA

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

SELECT
Revenue_Amount,
Invoice_Bill_Amount,
Project_Name,
Top_Task_Number,
Completion_Date,
Descriptions,
Operating_Unit_Name
FROM O_STG_PROJECT_REVENUE_INVOICE
WHERE COUNTRY='PA'
ORDER BY Project_Name

RETURN
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('PA', 'script', 'updated generateProjectRevenueForPA procedure definition', GETDATE());