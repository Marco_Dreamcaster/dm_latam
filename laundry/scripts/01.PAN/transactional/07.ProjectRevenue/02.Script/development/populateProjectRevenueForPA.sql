IF OBJECT_ID('DBO.populateProjectRevenueForPA', 'P') IS NOT NULL
DROP PROCEDURE dbo.populateProjectRevenueForPA;
GO

if object_id('Tempdb..#PR') IS NOT NULL DROP TABLE #PR
GO

if object_id('Tempdb..#GA') IS NOT NULL DROP TABLE #GA
GO

if object_id('Tempdb..#GB') IS NOT NULL DROP TABLE #GB
GO

CREATE PROCEDURE dbo.populateProjectRevenueForPA
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

DECLARE @lastUpdated DATETIME
SET @lastUpdated=GETDATE()

/**********************************************************************************************/
select
round((sum(b.haber-b.debe) / max(f.cantequipos)),2)			as Revenue_Amount,

c.project_name												as Project_Name,
dbo.udf_EncodeProjectTask('PA', e.IdEquipo)					as Top_Task_Number,
convert(varchar(10),max(b.FECHA),105)						as Completion_Date,
'DATA MIGRATION'											as Description,
'PA - THYSSENKRUPP ELEVADORES S.A.'							as Operating_Unit_Name,
'PA'														as [COUNTRY],
'loaded from script'										as [OBSERVATION],
c.SIGLA_PRIMARY_KEY											as [SIGLA_PRIMARY_KEY],
@lastUpdated												AS LAST_UPDATED
into #GA
from	
	TKPA_LATEST..contrato			as a
	inner join TKPA_LATEST..com		as b on a.CONTRATO = b.CONTRATO and b.TIPOCONT in ('01','10')
	inner join O_STG_PROJECT_HEADER as c on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = c.SIGLA_PRIMARY_KEY
	inner join TKPA_LATEST..DETCONT as d on a.CONTRATO=d.CONTRATO
	inner join TKPA_LATEST..EQUIPOS as e on d.IdEquipo=e.IdEquipo
	inner join (select COUNT(idequipo) as cantequipos,contrato from TKPA_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
where
b.CTA in ('31101001','31101002','31101003','31101004','31101005','31101006','31101007','31101008','31101030','31102','31102001','31102002','31102003','31102004','31102006','31102007','31102008')
--and b.CONTRATO in (select [COD OBRA] from contratosPOC )
and b.fecha <=convert(datetime, '30-09-2018', 105)
and b.TIPCOS <> 'O05'
group by c.PROJECT_NAME,e.IdEquipo, COUNTRY, OBSERVATION, SIGLA_PRIMARY_KEY, LAST_UPDATED
having round((sum(b.haber-b.debe) / max(f.cantequipos)),2) >0
order by c.PROJECT_NAME
;
--277 Rows


/**********************************************************************************************/
select
round((sum(b.haber-b.debe) / max(f.cantequipos)),2)			as Invoice_Bill_Amount,

c.project_name												as Project_Name,
dbo.udf_EncodeProjectTask('PA', e.IdEquipo)					as Top_Task_Number,
convert(varchar(10),max(b.FECHA),105)						as Completion_Date,
'DATA MIGRATION'											as Description,
'PA - THYSSENKRUPP ELEVADORES S.A.'							as Operating_Unit_Name,
'PA'														as [COUNTRY],
'loaded from script'										as [OBSERVATION],
c.SIGLA_PRIMARY_KEY											as [SIGLA_PRIMARY_KEY],
@lastUpdated												AS LAST_UPDATED
into #GB
from TKPA_LATEST..contrato		as a
inner join TKPA_LATEST..com		as b on a.CONTRATO = b.CONTRATO and b.TIPOCONT in ('01','10')
inner join O_STG_PROJECT_HEADER as c on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = c.SIGLA_PRIMARY_KEY
inner join TKPA_LATEST..DETCONT as d on a.CONTRATO=d.CONTRATO
inner join TKPA_LATEST..EQUIPOS as e on d.IdEquipo=e.IdEquipo
inner join (select COUNT(idequipo) as cantequipos,contrato from TKPA_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
where
b.CTA in ('23801095')
--and b.CONTRATO in (select [COD OBRA] from contratosPOC )
and b.fecha <=convert(datetime, '30-09-2018', 105)
and b.TIPCOS <> 'O05'
group by c.PROJECT_NAME,e.IdEquipo, COUNTRY, OBSERVATION, SIGLA_PRIMARY_KEY, LAST_UPDATED
--having round((sum(b.haber-b.debe) / max(f.cantequipos)),2) >0 --La agregacion arroja resultados negativos para esas cuentas 
order by c.PROJECT_NAME
;
--414 Rows


/***************************************RESULTS************************************************/
/*select
	GA.Revenue_Amount,
	GB.Invoice_Bill_Amount,
	GA.Project_Name,
	GA.Top_Task_Number,
	GA.Completion_Date,
	GA.Description,
	GA.Operating_Unit_Name,
	GA.COUNTRY,
	GA.OBSERVATION,
	GA.SIGLA_PRIMARY_KEY,
	GA.LAST_UPDATED
From 
	#GA GA
	inner join #GB GB
		on 
			GA.SIGLA_PRIMARY_KEY	= GB.SIGLA_PRIMARY_KEY
	  --and GA.Completion_Date		= GB.Completion_Date
		and GA.Top_Task_Number		= GB.Top_Task_Number
		and GA.Project_Name			= GB.Project_Name
*/

SELECT SUM(REVENUE_AMOUNT) AS REVENUE_AMOUNT,
	SUM(INVOICE_BILL_AMOUNT) AS INVOICE_BILL_AMOUNT,
	PROJECT_NAME,
	TOP_TASK_NUMBER,
	MAX(COMPLETION_DATE) AS COMPLETION_DATE,
	[DESCRIPTION],
	OPERATING_UNIT_NAME,
	COUNTRY,
	OBSERVATION,
	SIGLA_PRIMARY_KEY, MAX(ISNULL(REVENUE_COMPLETION_DATE,0)) AS REVENUE_COMPLETION_DATE,
	MAX(ISNULL(BILLING_COMPLETION_DATE,0)) AS  BILLING_COMPLETION_DATE

INTO #PR
FROM (
	SELECT
		GA.REVENUE_AMOUNT,
		0 AS INVOICE_BILL_AMOUNT,
		GA.PROJECT_NAME,
		GA.TOP_TASK_NUMBER,
		GA.COMPLETION_DATE,
		GA.DESCRIPTION,
		GA.OPERATING_UNIT_NAME,
		GA.COUNTRY,
		GA.OBSERVATION,
		GA.SIGLA_PRIMARY_KEY,
		GA.COMPLETION_DATE AS REVENUE_COMPLETION_DATE,
		NULL AS BILLING_COMPLETION_DATE

	FROM #GA GA

	UNION

	SELECT
		0 AS REVENUE_AMOUNT,
		GB.INVOICE_BILL_AMOUNT,
		GB.PROJECT_NAME,
		GB.TOP_TASK_NUMBER,
		GB.COMPLETION_DATE,
		GB.DESCRIPTION,
		GB.OPERATING_UNIT_NAME,
		GB.COUNTRY,
		GB.OBSERVATION,
		GB.SIGLA_PRIMARY_KEY,NULL AS REVENUE_COMPLETION_DATE,
		GB.COMPLETION_DATE AS BILLING_COMPLETION_DATE

	FROM #GB GB

) TBLAUX

GROUP BY PROJECT_NAME,
	TOP_TASK_NUMBER,
	[DESCRIPTION],
	OPERATING_UNIT_NAME,
	COUNTRY,
	OBSERVATION,
	SIGLA_PRIMARY_KEY
;
SELECT
    REVENUE_AMOUNT,
	INVOICE_BILL_AMOUNT,
	PROJECT_NAME,
	TOP_TASK_NUMBER,
	COMPLETION_DATE,
	[DESCRIPTION],
	OPERATING_UNIT_NAME,
	COUNTRY,
	OBSERVATION,
	SIGLA_PRIMARY_KEY,
	REVENUE_COMPLETION_DATE,
	BILLING_COMPLETION_DATE
FROM #PR
;
IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

RETURN
END
GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('PA', 'script', 'updated populateProjectRevenueForPA procedure definition', GETDATE());