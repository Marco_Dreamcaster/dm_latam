if object_id('Tempdb..#GB') IS NOT NULL DROP TABLE #GB
GO

if object_id('Tempdb..#GA') IS NOT NULL DROP TABLE #GA
GO

 --/ max(f.cantequipos)),2)
select
round((sum(b.haber-b.debe)/ max(f.cantequipos)),2)		as Revenue_Amount,

c.project_name												as Project_Name,
dbo.udf_EncodeProjectTask('PA', e.IdEquipo)					as Top_Task_Number,
convert(varchar(10),max(b.FECHA),105)						as Completion_Date,
'DATA MIGRATION'											as Description,
'PA - THYSSENKRUPP ELEVADORES S.A.'							as Operating_Unit_Name,
'PA'														as [COUNTRY],
'loaded from script'										as [OBSERVATION],
c.SIGLA_PRIMARY_KEY											as [SIGLA_PRIMARY_KEY]
into #GA
from
	TKPA_LATEST..contrato			as a
	inner join TKPA_LATEST..com		as b on a.CONTRATO = b.CONTRATO and b.TIPOCONT in ('01','10')
	inner join O_STG_PROJECT_HEADER as c on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = c.SIGLA_PRIMARY_KEY
	inner join TKPA_LATEST..DETCONT as d on a.CONTRATO=d.CONTRATO
	inner join TKPA_LATEST..EQUIPOS as e on d.IdEquipo=e.IdEquipo
	inner join (select COUNT(idequipo) as cantequipos,contrato from TKPA_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
where
b.CTA in ('31101001','31101002','31101003','31101004','31101005','31101006','31101007','31101008','31101030','31102','31102001','31102002','31102003','31102004','31102006','31102007','31102008')
--and b.CONTRATO in (select [COD OBRA] from contratosPOC )
and b.fecha <=convert(datetime, '30-09-2018', 105)
and b.TIPCOS <> 'O05'
group by c.PROJECT_NAME,e.IdEquipo, COUNTRY, OBSERVATION, SIGLA_PRIMARY_KEY, LAST_UPDATED
having round((sum(b.haber-b.debe) / max(f.cantequipos)),2) >0
order by c.PROJECT_NAME
--277 Rows


/**********************************************************************************************/
select
round((sum(b.haber-b.debe) / max(f.cantequipos)),2)			as Invoice_Bill_Amount,

c.project_name												as Project_Name,
dbo.udf_EncodeProjectTask('PA', e.IdEquipo)					as Top_Task_Number,
convert(varchar(10),max(b.FECHA),105)						as Completion_Date,
'DATA MIGRATION'											as Description,
'PA - THYSSENKRUPP ELEVADORES S.A.'							as Operating_Unit_Name,
'PA'														as [COUNTRY],
'loaded from script'										as [OBSERVATION],
c.SIGLA_PRIMARY_KEY											as [SIGLA_PRIMARY_KEY]
into #GB
from TKPA_LATEST..contrato		as a
inner join TKPA_LATEST..com		as b on a.CONTRATO = b.CONTRATO and b.TIPOCONT in ('01','10')
inner join O_STG_PROJECT_HEADER as c on dbo.udf_EncodeProjectReference('PA', a.CONTRATO) = c.SIGLA_PRIMARY_KEY
inner join TKPA_LATEST..DETCONT as d on a.CONTRATO=d.CONTRATO
inner join TKPA_LATEST..EQUIPOS as e on d.IdEquipo=e.IdEquipo
inner join (select COUNT(idequipo) as cantequipos,contrato from TKPA_LATEST..DETCONT group by contrato) f on a.CONTRATO=f.CONTRATO
where
b.CTA in ('23801095')
and b.CONTRATO in (select [COD OBRA] from contratosPOC )
and b.fecha <=convert(datetime, '30-09-2018', 105)
and b.TIPCOS <> 'O05'
group by c.PROJECT_NAME,e.IdEquipo, COUNTRY, OBSERVATION, SIGLA_PRIMARY_KEY, LAST_UPDATED
--having round((sum(b.haber-b.debe) / max(f.cantequipos)),2) >0 --La agregacion arroja resultados negativos para esas cuentas
order by c.PROJECT_NAME
--414 Rows


/***************************************RESULTS************************************************/

if object_id('Tempdb..#PR') IS NOT NULL DROP TABLE #PR
GO

SELECT SUM(REVENUE_AMOUNT) AS REVENUE_AMOUNT,
	SUM(INVOICE_BILL_AMOUNT) AS INVOICE_BILL_AMOUNT,
	PROJECT_NAME,
	TOP_TASK_NUMBER,
	MAX(COMPLETION_DATE) AS COMPLETION_DATE,
	[DESCRIPTION],
	OPERATING_UNIT_NAME,
	COUNTRY,
	OBSERVATION,
	SIGLA_PRIMARY_KEY, MAX(ISNULL(REVENUE_COMPLETION_DATE,0)) AS REVENUE_COMPLETION_DATE,
	MAX(ISNULL(BILLING_COMPLETION_DATE,0)) AS  BILLING_COMPLETION_DATE

INTO #PR
FROM (
	SELECT
		GA.REVENUE_AMOUNT,
		0 AS INVOICE_BILL_AMOUNT,
		GA.PROJECT_NAME,
		GA.TOP_TASK_NUMBER,
		GA.COMPLETION_DATE,
		GA.DESCRIPTION,
		GA.OPERATING_UNIT_NAME,
		GA.COUNTRY,
		GA.OBSERVATION,
		GA.SIGLA_PRIMARY_KEY,
		GA.COMPLETION_DATE AS REVENUE_COMPLETION_DATE,
		NULL AS BILLING_COMPLETION_DATE

	FROM #GA GA

	UNION

	SELECT
		0 AS REVENUE_AMOUNT,
		GB.INVOICE_BILL_AMOUNT,
		GB.PROJECT_NAME,
		GB.TOP_TASK_NUMBER,
		GB.COMPLETION_DATE,
		GB.DESCRIPTION,
		GB.OPERATING_UNIT_NAME,
		GB.COUNTRY,
		GB.OBSERVATION,
		GB.SIGLA_PRIMARY_KEY,NULL AS REVENUE_COMPLETION_DATE,
		GB.COMPLETION_DATE AS BILLING_COMPLETION_DATE

	FROM #GB GB

) TBLAUX

GROUP BY PROJECT_NAME,
	TOP_TASK_NUMBER,
	[DESCRIPTION],
	OPERATING_UNIT_NAME,
	COUNTRY,
	OBSERVATION,
	SIGLA_PRIMARY_KEY

SELECT * FROM #GA GA WHERE SIGLA_PRIMARY_KEY = 'PA_CT_299_13'
SELECT * FROM #GB GB WHERE SIGLA_PRIMARY_KEY = 'PA_CT_299_13'
SELECT * FROM #PR PR WHERE SIGLA_PRIMARY_KEY = 'PA_CT_299_13'
SELECT  SUM(REVENUE_AMOUNT), SUM(INVOICE_BILL_AMOUNT) from #pr
SELECT SUM(REVENUE_AMOUNT), SUM(INVOICE_BILL_AMOUNT), TOP_TASK_NUMBER
FROM #PR
WHERE SIGLA_PRIMARY_KEY = 'PA_001_10_14'
GROUP BY TOP_TASK_NUMBER
-- GB = BILLING
-- GA = REVENUE

select * from #pr


-- diferença encontrada na soma destes projectos
--Conta Overbilling	Projetos Limpos
--CT-0307-13	CT-0307-13
--CT-998-13	CT-998-13
--CT-999-13	CT-999-13
--L-0001	L-0001
--L-0673	L-0673
--L-0764	L-0764
--L-0855	L-0855
--L-0862	L-0862
--L-0867	L-0867
--L-0897	L-0897
--L-1015	L-1015*****
--L-1178	L-1178
--L-1238	#N/D
--L-1379	#N/D
--L-1391	#N/D
--N 724-1017	#N/D
--N-752-0718	#N/D
 --22687994,44 valor sem estes contratos

-- GB = BILLING - 25892827,56
-- GA = REVENUE - 16266286,89

-- 12201095

--- 21110817 revenue