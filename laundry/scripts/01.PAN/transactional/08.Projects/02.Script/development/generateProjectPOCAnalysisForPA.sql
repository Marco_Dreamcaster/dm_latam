IF OBJECT_ID('DBO.generateProjectPOCAnalysisForPA', 'P') IS NOT NULL
	DROP PROCEDURE dbo.generateProjectPOCAnalysisForPA;

GO

CREATE PROCEDURE dbo.generateProjectPOCAnalysisForPA
AS
BEGIN

SET NOCOUNT ON;
BEGIN TRANSACTION

IF @@ERROR != 0
ROLLBACK TRANSACTION
ELSE
COMMIT TRANSACTION

SELECT 
[ Pais]
,[ ON/ MOD]
,[COD OBRA]
,[Nombre Obra]
,[ CIF/CCM]
,[Fecha proy Cierre CONTRATO]
,[Contratado]
,[Adendas/ Change Orders]
,[TOT CONTRATO]
,[Valor de manto Gratuito]
,[Costo Materiales]
,[Costo Mat Adicionales]
,[Flete]
,[Importacion]
,[Costo Materiales1]
,[Costo Mano de Obra Instalacion]
,[Costo MdeO Adicional]
,[Costo Mano de Obra]
,[Locales y varios]
,[Loc y varios Adicionales]
,[Locales y varios1]
,[Estructura Montaje]
,[TOTAL COSTOS]
,[Margen]
,[% GM]
,[Comision Venta]
FROM [contratosPOC] a
WHERE 
a.[ Pais]='Panam�' OR 
a.[ Pais]='LARO PA'

RETURN
END

GO

INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP)
VALUES('PA', 'script', 'updated generateProjectPOCAnalysisForPA procedure definition', GETDATE());