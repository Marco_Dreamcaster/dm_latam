-- 1. set up country variables
--
:setvar targetCountryCode CO

:setvar targetDatabaseName "DM_LATAM"           -- PRODUCTION
--:setvar targetDatabaseName "DM_LATAM_DEV"		-- DEV
:setvar sourceDatabaseName "TKCO_LATEST"

:setvar pmRef "CO327"
:setvar smRef "CO327"
:setvar salesRef "CO32"
:setvar dummyRef "CODM001"  -- parametro adicional no ConfigEstandar

:setvar orgName (COLOMBIA)

:r "../../../00.COM/master/08.Projects/load_ProjectsData.sql"
GO






