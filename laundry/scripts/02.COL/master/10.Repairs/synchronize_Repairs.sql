-- this is the script for loading from TKCO_LATEST
-- it should be run everytime the snapshot gets synchronized 

--DECLARE @cutoffDate DATE;SET @cutoffDate=CONVERT(date,'31-10-2018',103);

DECLARE @cutoffDate DATE;SET @cutoffDate=GETDATE();

EXEC dbo.loadRepairsMasterData_CO @cutoffDate

UPDATE O_PRE_PROJECT_HEADER
SET RECOMMENDED = 0 
WHERE COUNTRY='CO'
AND TEMPLATE_NAME='CC QR TEMPLATE CO'

UPDATE O_PRE_PROJECT_HEADER
SET RECOMMENDED = 1 
WHERE COUNTRY='CO'
AND TEMPLATE_NAME='CC QR TEMPLATE CO'
AND START_DATE >= DATEADD(year,-1, GETDATE())


