DECLARE @testDate DATE;SET @testDate=CONVERT(date,'31-10-2018',103);EXEC dbo.GenerateAR @testDate, 'CO';

DECLARE @testDate DATE;SET @testDate=CONVERT(date,'31-10-2018',103);EXEC dbo.populateAR @testDate, 'CO';




SELECT

Transaction_Number, Description, Transaction_Date, Quantity, Unit_Price,
ENCODED_CUSTOMER, Bill_To_Customer_Number_Reference, Bill_To_Address_Reference, Ship_To_Address_Reference,
Primary_Sales_Rep_Number, Currency_Code, Amount, Payment_Terms, Batch_Source_Name,
Tax_Rate_Code, Accounting_Date, Line_Type, Transaction_Type, Exchange_Rate_Type,
Conversion_Rate, Invoice_Line_Number, Project_Name, Printer_Fiscal_Number,
Contract_Number, Link_To_Line_Attribute1, Link_To_Line_Attribute2, Link_To_Line_Attribute3,
Link_To_Line_Attribute4, Link_To_Line_Attribute5, Operating_Unit_Name,
COUNTRY, OBSERVATION, LAST_UPDATED

FROM ##TMPARINVOICE

SELECT * FROM O_STG_CUSTOMER_CROSS_REF_ALT a WHERE a.ORIG_SYSTEM_CUSTOMER_REF=dbo.udf_EncodeCustomerReference('CO', '800221870')

EXECUTE dbo.fixMultipleCustomers 'CO';

EXEC dbo.generateCustomerCrossRefByCountry 'CO'
