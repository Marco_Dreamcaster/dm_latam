-- 1. set up country variables
--
:setvar targetCountryCode MX

:setvar targetDatabaseName "DM_LATAM"           -- PRODUCTION
--:setvar targetDatabaseName "DM_LATAM_DEV"		-- DEV
:setvar sourceDatabaseName "TKMX_LATEST"

:setvar pmRef "MX51"
:setvar smRef "MX0"
:setvar salesRef "MX21"
:setvar dummyRef "MXDM001"

:setvar orgName (MEXICO)

:r "../../../00.COM/master/08.Projects/load_ProjectsData.sql"
GO






