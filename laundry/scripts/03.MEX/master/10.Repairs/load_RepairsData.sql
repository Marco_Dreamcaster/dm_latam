-- this script is used for defining the loading procedures
-- for actual synchronization try associated script synchronize_Repairs.sql

-- 1. set up country variables
--
:setvar targetCountryCode MX

:setvar targetDatabase "DM_LATAM"           -- PRODUCTION
--:setvar targetDatabase "DM_LATAM_DEV"		-- DEV

:setvar pmRef 99999999
:setvar smRef 99999999
:setvar dummyRef MXDM001

:setvar sourceDatabase "TKMX_LATEST"

:r "..\..\..\00.COM\master\10.Repairs\02.development\populateRCPContratadas.sql"
:r "..\..\..\00.COM\master\10.Repairs\02.development\generateRCPContratadas.sql"


:r "..\..\..\00.COM\master\10.Repairs\load_RepairsData.sql"