-- 1. set up country variables
--
:setvar targetCountryCode SV

:setvar targetDatabaseName "DM_LATAM"           -- PRODUCTION
--:setvar targetDatabaseName "DM_LATAM_DEV"		-- DEV
:setvar sourceDatabaseName "TKSV_LATEST"

:setvar pmRef "SV51"
:setvar smRef "SV0"
:setvar salesRef "SV21"
:setvar dummyRef "SVDM001"

:r "../../../00.COM/master/08.Projects/load_ProjectsData.sql"
GO






