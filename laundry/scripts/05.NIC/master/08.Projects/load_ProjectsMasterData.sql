-- 1. set up country variables
--
:setvar targetCountryCode NI

:setvar targetDatabaseName "DM_LATAM"           -- PRODUCTION
--:setvar targetDatabaseName "DM_LATAM_DEV"		-- DEV
:setvar sourceDatabaseName "TKNI_LATEST"

:setvar pmRef "NI51"
:setvar smRef "NI0"
:setvar salesRef "NI21"
:setvar dummyRef "NIDM001"

:r "../../../00.COM/master/08.Projects/load_ProjectsData.sql"
GO






