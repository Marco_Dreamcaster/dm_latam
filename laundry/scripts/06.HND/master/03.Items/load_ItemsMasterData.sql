:setvar targetCountryCode HN
:setvar targetDatabaseName "DM_LATAM"			-- PROD
--:setvar targetDatabaseName "DM_LATAM_DEV"		-- DEV
:setvar sourceDatabaseName "TKHN_LATEST"

:r "..\..\..\00.COM\master\03.Items\load_ItemsData.sql"

GO

USE $(targetDatabaseName)

DROP PROCEDURE dbo.buildItemTransDefSubInvRow_$(targetCountryCode);
GO

CREATE PROCEDURE dbo.buildItemTransDefSubInvRow_$(targetCountryCode)
(  
    @itemCode VARCHAR(16),
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN 

DECLARE @uniqueOrgCode VARCHAR(3)

DECLARE UNIQUE_ORG_CODE CURSOR FOR   
SELECT 'HNL'

OPEN UNIQUE_ORG_CODE;

FETCH NEXT FROM UNIQUE_ORG_CODE   
INTO @uniqueOrgCode;

WHILE @@FETCH_STATUS = 0
BEGIN
	INSERT INTO O_PRE_ITEM_TRANS_DEF_SUB_INV  WITH (TABLOCK)
	(
	COUNTRY,
	LOCAL_VALIDATED,
	CROSSREF_VALIDATED,
	CLEANSED,
	RECOMMENDED,
	LAST_UPDATED,

	ORIG_ITEM_NUMBER,
	ITEM_NUMBER,
	ORGANIZATION_CODE,
	SUBINVENTORY_NAME,
	ORG_NAME
	)
	SELECT
	TOP 1
	'$(targetCountryCode)',
	1,
	1,
	0,
	0,
	GETDATE(),

	RTRIM(LTRIM('$(targetCountryCode)' + a.PRODUCTO)) AS ORIG_ITEM_NUMBER,
	ISNULL(LEFT(UPPER(CODIGOINTERNO), 16),'FIX ME') AS ITEM_NUMBER,
	@uniqueOrgCode AS ORGANIZATION_CODE,
	'DEP01' AS SUBINVENTORY_NAME,
	'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME

	FROM $(sourceDatabaseName).dbo.PRODUCTOS as a
	left outer join  $(sourceDatabaseName).dbo.UBICAPRODALM as b on a.PRODUCTO=b.PRODUCTO
	left outer join  $(sourceDatabaseName).dbo.ALMACEN as c on b.IDALMACEN=c.IdAlmacen
	WHERE
	a.PRODUCTO = @itemCode;

	FETCH NEXT FROM UNIQUE_ORG_CODE   
		INTO @uniqueOrgCode;
END;

CLOSE UNIQUE_ORG_CODE;

DEALLOCATE UNIQUE_ORG_CODE;

END;

GO


DROP PROCEDURE dbo.buildItemTransDefLocRow_$(targetCountryCode);
GO

CREATE PROCEDURE dbo.buildItemTransDefLocRow_$(targetCountryCode)
(  
    @itemCode VARCHAR(16),
    @userStory VARCHAR(16) = ''
)  
AS  
BEGIN 

DECLARE @uniqueOrgCode VARCHAR(3)

DECLARE UNIQUE_ORG_CODE CURSOR FOR   
SELECT 'HNL'

OPEN UNIQUE_ORG_CODE;

FETCH NEXT FROM UNIQUE_ORG_CODE   
INTO @uniqueOrgCode;

WHILE @@FETCH_STATUS = 0
BEGIN

	INSERT INTO O_PRE_ITEM_TRANS_DEF_LOC  WITH (TABLOCK)
	(
	COUNTRY,
	LOCAL_VALIDATED,
	CROSSREF_VALIDATED,
	CLEANSED,
	RECOMMENDED,
	LAST_UPDATED,

	ORIG_ITEM_NUMBER,
	ITEM_NUMBER,
	ORGANIZATION_CODE,
	SUBINVENTORY_NAME,
	LOCATOR_NAME,
	ORG_NAME
	)
	SELECT
	TOP 1
	'$(targetCountryCode)',
	1,
	1,
	0,
	0,
	GETDATE(),

	RTRIM(LTRIM('$(targetCountryCode)' + a.PRODUCTO)) AS ORIG_ITEM_NUMBER,
	ISNULL(LEFT(UPPER(CODIGOINTERNO), 16),'FIX ME') AS ITEM_NUMBER,
	@uniqueOrgCode AS ORGANIZATION_CODE,
	'DEP01' AS SUBINVENTORY_NAME,
	ISNULL(b.UBICACION, 'UNKNOWN') AS LOCATOR_NAME,
	'$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME

	FROM $(sourceDatabaseName).dbo.PRODUCTOS as a
	left outer join  $(sourceDatabaseName).dbo.UBICAPRODALM as b on a.PRODUCTO=b.PRODUCTO
	left outer join  $(sourceDatabaseName).dbo.ALMACEN as c on b.IDALMACEN=c.IdAlmacen
	WHERE
	a.PRODUCTO  = @itemCode;

	FETCH NEXT FROM UNIQUE_ORG_CODE   
		INTO @uniqueOrgCode;
END;

CLOSE UNIQUE_ORG_CODE;

DEALLOCATE UNIQUE_ORG_CODE;

END;

GO

IF OBJECT_ID('tempdb.dbo.#NONZEROSTOCK', 'U') IS NOT NULL
	DROP TABLE dbo.#NONZEROSTOCK;

IF OBJECT_ID('$(targetDatabaseName).dbo.addRecommendationsFor$(targetCountryCode)', 'P') IS NOT NULL
	DROP PROCEDURE dbo.addRecommendationsFor$(targetCountryCode);
GO

CREATE PROCEDURE dbo.addRecommendationsFor$(targetCountryCode)
AS
BEGIN

	DELETE FROM COM_AUDIT_LOGS WHERE EVENT LIKE '%Items Master Data partial recommendation%' AND COUNTRY='$(targetCountryCode)'

	UPDATE O_PRE_ITEM_HEADER SET RECOMMENDED=0 WHERE COUNTRY='$(targetCountryCode)'
	UPDATE O_PRE_ITEM_HEADER SET RANKING=0 WHERE COUNTRY='$(targetCountryCode)'

	CREATE TABLE #NONZEROSTOCK ( [Value] VARCHAR(255) COLLATE Modern_Spanish_CI_AS );

	INSERT INTO #NONZEROSTOCK
	select distinct RTRIM(LTRIM('$(targetCountryCode)' + a.PRODUCTO)) AS ORIG_ITEM_NUMBER
	from $(sourceDatabaseName).dbo.STOCK as a
		  inner join $(sourceDatabaseName).dbo.PRODUCTOS as b on a.PRODUCTO=b.PRODUCTO  
	WHERE
		  a.PERIODO=YEAR(GETDATE())
		  AND a.MES=MONTH(GETDATE())
		  and a.SALDO > 0
		  and isnull(b.BLOQUEADO,'N') <> 'S'

	-- add weight 5 for SALDO > 0 criteria
	--
	UPDATE O_PRE_ITEM_HEADER SET RANKING=RANKING + 5 WHERE ORIG_ITEM_NUMBER IN (SELECT DISTINCT * FROM #NONZEROSTOCK); 

	-- add weight 1 for ITEMS starting with 'L'
	--
	UPDATE O_PRE_ITEM_HEADER SET RANKING=RANKING + 1 WHERE COUNTRY='$(targetCountryCode)' AND ORIG_ITEM_NUMBER LIKE '$(targetCountryCode)L%'


	DECLARE @orig_item_number VARCHAR(16)
	DECLARE @codigo_interno VARCHAR(16)

	DECLARE @logThreshold int = 100
	DECLARE @count int = 0
	DECLARE @total int = 0

	SELECT @total=COUNT(ORIG_ITEM_NUMBER) FROM O_PRE_ITEM_HEADER WHERE COUNTRY='$(targetCountryCode)';

	DECLARE RANKING_CURSOR CURSOR FOR   
	SELECT ORIG_ITEM_NUMBER, ITEM_NUMBER
	FROM O_PRE_ITEM_HEADER WHERE COUNTRY = '$(targetCountryCode)';

	OPEN RANKING_CURSOR;

	FETCH NEXT FROM RANKING_CURSOR   
	INTO @orig_item_number, @codigo_interno;


	WHILE @@FETCH_STATUS = 0
	BEGIN

		IF EXISTS(SELECT TOP 1 * FROM O_STG_ITEM_HEADER ih WHERE ih.ORIG_ITEM_NUMBER LIKE '%'+ RTRIM(LTRIM(SUBSTRING(@orig_item_number,3,14))) +'%')
  		  BEGIN
			-- add weight 1 for "same ORIG_ITEM_NUMBER"

			UPDATE O_PRE_ITEM_HEADER SET RANKING=RANKING+1 WHERE ORIG_ITEM_NUMBER = @orig_item_number; 
		  END	


		IF (RTRIM(LTRIM(@codigo_interno))!=' ') 
		  BEGIN
			-- add weight 1 for "same CODIGO_INTERNO"

			IF EXISTS(SELECT TOP 1 * FROM O_STG_ITEM_HEADER ih WHERE ih.OBSERVATION LIKE '%'+ @codigo_interno +'%')
			 BEGIN
			   UPDATE O_PRE_ITEM_HEADER SET RANKING=RANKING+1 WHERE ORIG_ITEM_NUMBER = @orig_item_number; 
			 END	

		  END

		IF (@count%@logThreshold = 0)
			BEGIN
				PRINT CAST(@count AS VARCHAR(max))+'/'+CAST(@total AS VARCHAR(max));

				INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,SNAPSHOT_DATE)
				VALUES('$(targetCountryCode)', 'script', 'Items Master Data partial recommendation ' + CAST(@count AS VARCHAR(max))+'/'+CAST(@total AS VARCHAR(max)), GETDATE(), @count, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));
			END
		
		SET @count = @count + 1

		FETCH NEXT FROM RANKING_CURSOR   
		INTO @orig_item_number, @codigo_interno;
	END;

	CLOSE RANKING_CURSOR;

	DEALLOCATE RANKING_CURSOR;

	UPDATE O_PRE_ITEM_HEADER SET RECOMMENDED=1 WHERE RANKING>=5 AND COUNTRY='$(targetCountryCode)';

	SELECT @count=COUNT(*) FROM O_PRE_ITEM_HEADER WHERE RECOMMENDED=1 AND COUNTRY='$(targetCountryCode)'

	DELETE FROM COM_AUDIT_LOGS WHERE EVENT LIKE '%Items Master Data partial recommendation%' AND COUNTRY='$(targetCountryCode)'

	INSERT INTO dbo.COM_AUDIT_LOGS(COUNTRY, USER_ID,EVENT, STAMP,COUNT,SNAPSHOT_DATE)
	VALUES('$(targetCountryCode)', 'script', 'Items Master Data added recommendations from $(sourceDatabaseName) @ ' + dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'), GETDATE(), @count, dbo.udf_EncodeSnapshotDate('$(targetCountryCode)'));

END;
GO

