-- 1. set up country variables
--
:setvar targetCountryCode HN

:setvar targetDatabaseName "DM_LATAM"           -- PRODUCTION
--:setvar targetDatabaseName "DM_LATAM_DEV"		-- DEV
:setvar sourceDatabaseName "TKHN_LATEST"

:setvar pmRef "HN51"
:setvar smRef "HN0"
:setvar salesRef "HN21"
:setvar dummyRef "HNDM001"

:r "../../../00.COM/master/08.Projects/load_ProjectsData.sql"
GO






