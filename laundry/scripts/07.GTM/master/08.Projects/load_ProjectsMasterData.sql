-- 1. set up country variables
--
:setvar targetCountryCode GT

:setvar targetDatabaseName "DM_LATAM"           -- PRODUCTION
--:setvar targetDatabaseName "DM_LATAM_DEV"		-- DEV
:setvar sourceDatabaseName "TKGT_LATEST"

:setvar pmRef "GT51"
:setvar smRef "GT0"
:setvar salesRef "GT21"
:setvar dummyRef "GTDM001"

:r "../../../00.COM/master/08.Projects/load_ProjectsData.sql"
GO






