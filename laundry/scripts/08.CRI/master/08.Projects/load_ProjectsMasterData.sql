-- 1. set up country variables
--
:setvar targetCountryCode CR

:setvar targetDatabaseName "DM_LATAM"           -- PRODUCTION
--:setvar targetDatabaseName "DM_LATAM_DEV"		-- DEV
:setvar sourceDatabaseName "TKCR_LATEST"

:setvar pmRef "CR51"
:setvar smRef "CR0"
:setvar salesRef "CR21"
:setvar dummyRef "CRDM001"

:r "../../../00.COM/master/08.Projects/load_ProjectsData.sql"
GO






