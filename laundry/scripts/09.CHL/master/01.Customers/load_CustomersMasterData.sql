-- 1. set up country variables
--
:setvar targetCountryCode CL

:setvar targetDatabaseName "DM_LATAM"           -- PRODUCTION
--:setvar targetDatabaseName "DM_LATAM_DEV"		-- DEV

:setvar sourceDatabaseName "TKCL_LATEST"
:setvar timezoneName America/Santiago

-- 2. include common procedures
--

:r "..\..\..\00.COM\master\01.Customers\load_CustomersData.sql"


--IF OBJECT_ID('$(targetDatabaseName).dbo.buildCustomerAddressRow_$(targetCountryCode)', 'P') IS NOT NULL
DROP PROCEDURE dbo.buildCustomerAddressRow_$(targetCountryCode);
GO

CREATE PROCEDURE dbo.buildCustomerAddressRow_$(targetCountryCode)
(
    @itemCode VARCHAR(16),
    @userStory VARCHAR(16) = ''
)
AS
BEGIN
  INSERT INTO O_PRE_CUSTOMER_ADDRESS WITH (TABLOCK)
    (
    LAST_UPDATED,
    COUNTRY,
    LOCAL_VALIDATED,
    CROSSREF_VALIDATED,
    CLEANSED,
    RECOMMENDED,

    CUSTOMER_NAME,
    ORIG_SYSTEM_CUSTOMER_REF,
    ORIG_SYSTEM_ADRESS_REFERENCE,
    SITE_USE_CODE,
    PRIMARY_SITE_USE_FLAG,
    LOCATION,
    ADDRESS_LINE1,
    CITY,
    ADDRESS_COUNTRY,
    PROVINCE,
    CUSTOMER_PROFILE,
    TIME_ZONE,
    LANGUAGE,
    ORG_NAME
	--,GDF_ADDRESS_ATTRIBUTE8
  )
  SELECT
    GETDATE(),
    '$(targetCountryCode)',
    1,
    1,
    0,
    0,

    UPPER(c.NOMBRE) AS CUSTOMER_NAME,
    '$(targetCountryCode)' + RTRIM(LTRIM(CAST(c.CLIENTE AS varchar(30)))) + '-CC' AS ORIG_SYSTEM_CUSTOMER_REF,
    '$(targetCountryCode)' + RTRIM(LTRIM(CAST(c.CLIENTE AS varchar(30)))) + '-CA' AS ORIG_SYSTEM_ADRESS_REFERENCE,
    'ALL',
    1,
    UPPER(LEFT(c.DIRECCION,40)),
    UPPER(LEFT(c.DIRECCION,40)),
    cc.DESCRIP,
    '$(targetCountryCode)',
    d.DESCRIP,
    'DEFAULT',
    '$(timezoneName)',
    'ESA',
    '$(targetCountryCode) - THYSSENKRUPP ELEVADORES S.A.' AS ORG_NAME
	--,TblAux.Descripcion

  FROM $(sourceDatabaseName).dbo.Clientes c
  
  -- Nota: NO EXISTE LA TABLA CliTipoContrib EN CHILE
  --LEFT JOIN (
  --  SELECT TipoContribuyente.Tipo,
  --    TipoContribuyente.Descripcion,
  --    CliTipoContrib.Cliente
  --  FROM $(sourceDatabaseName).dbo.CliTipoContrib AS CliTipoContrib
  --  INNER JOIN $(sourceDatabaseName).dbo.TipoContribuyente AS TipoContribuyente ON (CliTipoContrib.TipoContrib = TipoContribuyente.Tipo)
  --) TblAux ON (TblAux.Cliente = C.Cliente)

  LEFT JOIN $(sourceDatabaseName).dbo.CIUDADES cc ON (c.IdCiudad = cc.IdCiudad)
  LEFT JOIN $(sourceDatabaseName).dbo.DISTRITOS d ON (c.IdDistrito = d.IdDistrito)
  WHERE c.Cliente = @itemCode
END;
GO

