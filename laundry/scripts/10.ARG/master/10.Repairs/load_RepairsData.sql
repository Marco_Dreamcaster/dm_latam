-- this script is used for defining the loading procedures
-- for actual synchronization try associated script synchronize_Repairs.sql

-- 1. set up country variables
--
:setvar targetCountryCode CO

:setvar targetDatabase "DM_LATAM"           -- PRODUCTION
--:setvar targetDatabase "DM_LATAM_DEV"		-- DEV

:setvar pmRef 10270035
:setvar smRef 10375792
:setvar dummyRef CODM001

:setvar sourceDatabase "TKCO_LATEST"


:r "..\..\..\00.COM\master\10.Repairs\02.development\populateRCPContratadas.sql"
:r "..\..\..\00.COM\master\10.Repairs\02.development\generateRCPContratadas.sql"

:r "..\..\..\00.COM\master\10.Repairs\load_RepairsData.sql"

