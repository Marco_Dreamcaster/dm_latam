<?php

/*
 * **********************************************************************************
 * CLASSE AUTOLOAD
 * 
 * Programa responsável por executar ao autoload das classes PHP do sistema.
 * 
 * @copyright (C) FSA Tecnologia  
 * Todos os direitos desde sistema são protegidos pela lei de 
 * direitos autorais, a cópia sem autoriazação de qualquer parte 
 * deste sistema é proibida.
 * Desenvolvido por Fabiano Santos, Setembro 2016, SP/Brasil
 * 
 * **********************************************************************************
 */


/*
  AUTO LOAD CLASSES
 */

spl_autoload_register(function ($class) {

    global $currentModule;

    // 'Controller' é case sensitive, não alterar !!
    if (strpos($class, 'Controller') > -1) {
        
        if (file_exists('modules/' . $currentModule . '/controller/' . $class . '.class.php')) {
            require_once 'modules/' . $currentModule . '/controller/' . $class . '.class.php';
        } else  {
            header("Location: " . BASE . "p404.html");
        }
        
    } elseif (file_exists('modules/' . $currentModule . '/model/' . $class . '.class.php')) {
        require_once 'modules/' . $currentModule . '/model/' . $class . '.class.php';
            
    } elseif (file_exists('modules/' . $currentModule . '/helpers/' . $class . '.class.php')) {
        require_once 'modules/' . $currentModule . '/helpers/' . $class . '.class.php';
        
    } elseif (file_exists('core/' . $class . '.class.php')) {
        require_once 'core/' . $class . '.class.php';
            
    } 
    
});


/*
  END CLASS
 */

