<?php
/*
 * **********************************************************************************
 * Arquivo de configuração
 * 
 * Este arquivo contém as configurações gerais do sistema, definição das constantes
 * e load de configurações auxiliares.
 * 
 * @copyright (C) FSA Tecnologia  
 * Todos os direitos desde sistema são protegidos pela lei de 
 * direitos autorais, a cópia sem autoriazação de qualquer parte 
 * deste sistema é proibida.
 * Desenvolvido por Fabiano Santos, Setembro 2016, SP/Brasil
 * 
 * **********************************************************************************
 */

/*
 * Carrega o arquivo de definição do ambiente de operação do sistema
 */
    require 'environment.inc.php';
    
/*
 * Configurações de acesso ao Banco de dados e endereço de base do sistema.
 * 
 */
   if(ENVIRONMENT == 'Development') {            // Ambiente de Desenvolvimento
        
        define('SIS_DB_HOST', 'localhost');         // Endereco do banco de dados
        define('SIS_DB_USER', 'root');           // Login de acesso ao banco de dados
        define('SIS_DB_PASS', 'FBNsts@1980');  // Senha de acesso ao  banco de dados
        define('SIS_DB_DBSA', 'PFF');               // Nome do banco de dados
        define('SIS_DB_PORT', '3306');              // Porta do banco de dados

        define('BASE', 'http://localhost/laundry/startup/');
    
        
   } else {                                         // Ambiente de Produção
       
        define('SIS_DB_HOST', 'collaborate-in.com');         // Endereco do banco de dados
        define('SIS_DB_USER', 'colaborate_sa');           // Login de acesso ao banco de dados
        define('SIS_DB_PASS', 'FBNsts@1980');  // Senha de acesso ao  banco de dados
        define('SIS_DB_DBSA', 'colaborate_db1');               // Nome do banco de dados
        define('SIS_DB_PORT', '3306');              // Porta do banco de dados

        define('BASE', 'http://collaborate-in.com/startup/');
        
    }

  /*
 * Carrega o arquivo de definição das configurações do cliente
 */
    require 'customer.inc.php';
    
 /*
 * Carrega o arquivo de Autoload das classes PHP
 */
    require 'autoload.inc.php';
    
 /*
 * Carrega o arquivo de Funções do sistema
 */

    require('core/functions/functions.inc.php');

    

/*
 * ****************** End of file  ***************************************************  
 */
    
    





