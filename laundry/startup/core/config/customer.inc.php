<?php

/*
 * **********************************************************************************
 * Arquivo de configuração do cliente
 * 
 * Este arquivo contém as configurações gerais do cliente para o sistema, 
 * definição das constantes utilizadas nas páginas e dados de redes sociais.
 * 
 * 
 * @copyright (C) FSA Tecnologia  
 * Todos os direitos desde sistema são protegidos pela lei de 
 * direitos autorais, a cópia sem autoriazação de qualquer parte 
 * deste sistema é proibida.
 * Desenvolvido por Fabiano Santos, Setembro 2016, SP/Brasil
 * 
 * **********************************************************************************
 */

/**********************************************************************************
 * SITE CONFIG
 **********************************************************************************
 */

define('SITE_URL', 		'www.dbtraining.com.br');	// Endereço do site
define('SITE_NAME', 	'dbtraining');				//Nome do site do cliente
define('SHORT_NAME', 	'dbtraining Consultoria e Treinamentos');
define('SITE_SUBNAME', 	'dbtraining - Treinamentos em Bancos de dados'); //Nome do site do cliente
define('SITE_COPYRIGHT','© dbtraining 2016');
define('SITE_DESC', 	'Somos especialitas em treinamentos de Bancos de dados, BI e Data, Warehousing.'); //Descrição do site do cliente
define('SITE_FONT_NAME','Open Sans'); 				//Definição da Tipografia padrão do site (https://www.google.com/fonts)
define('SITE_FONT_WHIGHT','300,400,600,700,800');  //Definição da Tipografia padrão do site (https://www.google.com/fonts)



/**********************************************************************************
 * DADOS DO SEU CLIENTE/DONO DO SITE
 **********************************************************************************
 */

define('SITE_ADDR_NAME', 'Contato - dbtraining'); 					//Nome de remetente
define('SITE_ADDR_RS', 'dbtraining Consultoria e Treinamentos'); 	//Razão Social
define('SITE_ADDR_EMAIL', 'contato@dbtraining.com.br'); 			//E-mail de contato
define('SITE_ADDR_SITE', 'www.dbtraining.com.br'); 					//URL descrita
define('SITE_ADDR_CNPJ', '00.000.000/0000-00'); 					//CNPJ da empresa
define('SITE_ADDR_IE', '000/0000000'); 								//Inscrição estadual da empresa
define('SITE_ADDR_PHONE_A', '(11) 1234-5678'); 						//Telefone 1
define('SITE_ADDR_PHONE_B', '(11) 1234-5678'); 						//Telefone 2
define('SITE_ADDR_ADDR', '*** ENDEREÇO ****'); 						//ENDEREÇO: rua, número (complemento)
define('SITE_ADDR_CITY', ' **** CIDADE ***'); 						//ENDEREÇO: cidade
define('SITE_ADDR_DISTRICT', ' *** bairro ****'); 					//ENDEREÇO: bairro
define('SITE_ADDR_UF', ' *** ESTADO ***'); 							//ENDEREÇO: UF do estado
define('SITE_ADDR_ZIP', ' 12345-123'); 								//ENDEREÇO: CEP
define('SITE_ADDR_COUNTRY', ' Brasil'); 							//ENDEREÇO: País



/**********************************************************************************
 * CONFIGURAÇÕES DAS REDES SOCIAIS
 **********************************************************************************
 */

define('SITE_SOCIAL_NAME', 'dbtraining');

/*
 * Google
 */
define('SITE_SOCIAL_GOOGLE', false);
define('SITE_SOCIAL_GOOGLE_AUTHOR', ''); //https://plus.google.com/????? (???? ID DO USUÁRIO)
define('SITE_SOCIAL_GOOGLE_PAGE', 'https://plus.google.com/????'); //https://plus.google.com/???? (**ID DA PÁGINA)

/*
 * Facebook
 */
define('SITE_SOCIAL_FB', false);
define('SITE_SOCIAL_FB_APP', null); 								//Opcional APP do facebook
define('SITE_SOCIAL_FB_AUTHOR', ''); 								//https://www.facebook.com/?????
define('SITE_SOCIAL_FB_PAGE', 'https://www.facebook.com/?????'); 	//https://www.facebook.com/?????

/*
 * Twitter
 */
define('SITE_SOCIAL_TWITTER', '#'); //https://www.twitter.com/?????

/*
 * LinkedIn
 */
define('SITE_SOCIAL_LINKEDIN', '#'); //https://www.snapchat.com/add/?????

/*
 * YouTube
 */
define('SITE_SOCIAL_YOUTUBE', '#'); //https://www.youtube.com/user/?????

/*
 * Instagram
 */
define('SITE_SOCIAL_INSTAGRAM', '#'); //https://www.instagram.com/?????

/*
 * Snapchat
 */
define('SITE_SOCIAL_SNAPCHAT', '#'); //https://www.snapchat.com/add/?????


/**********************************************************************************
 *DADOS DO DESENVOLVEDOR DO SISTEMA
 **********************************************************************************
 */

define('AGENCY_CONTACT', 	'Fabiano Santos'); 				//Nome do contato
define('AGENCY_EMAIL', 		'Contato@dbtraining.com.br'); 	//E-mail de contato
define('AGENCY_PHONE', 		'(11)1234-5678'); 				//Telefone de contato
define('AGENCY_URL', 		'www.dbtraining.com.br'); 		//URL completa do seu site/portfolio
define('AGENCY_NAME', 		'dbtraining'); 					//Nome da sua agência web
define('AGENCY_ADDR', 		'*** ENDEREÇO ***'); 			//Endereço da sua agência web (RUA, NÚMERO)
define('AGENCY_CITY', 		'*** CIDADE ***');  			//Endereço da sua agência web (CIDADE)
define('AGENCY_UF', 		'*** ESTADO ***');  			//Endereço da sua agência web (UF DO ESTADO)
define('AGENCY_ZIP', 		'12345-678');  					//Endereço da sua agência web (CEP)
define('AGENCY_COUNTRY', 	'Brasil');  					//Endereço da sua agência web (PAÍS)

    
 /*********************************************************************************
 *CONFIGURAÇÕES DA LOJA
 **********************************************************************************
 */   

/*
 * Moeda
 */
define('CURRENCY', 'BRL'); 	//Nome do contato

/*
 * ****************** End of file  ************************************************  
 */
    
    





