<?php
/*
 * **********************************************************************************
 * Arquivo de configuração do ambiente
 * 
 * Este arquivo contém as configuração do ambiente de operação do sistema.
 *
 * SET:
 *
 *  ENVIRONMENT = "Desenvolvimento" para ambiente de desenvolvimento, Localhost.
 *  ENVIRONMENT = "Produção" para ambiente de Produção, Online.
 *
 *
 * @copyright (C) FSA Tecnologia  
 * Todos os direitos desde sistema são protegidos pela lei de 
 * direitos autorais, a cópia sem autoriazação de qualquer parte 
 * deste sistema é proibida.
 * Desenvolvido por Fabiano Santos, Setembro 2016, SP/Brasil
 * 
 * **********************************************************************************
 */

/*
 * Define o ambiente de operação do sistema
 */

	define("ENVIRONMENT", "Development");

    
?>