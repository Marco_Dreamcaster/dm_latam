<?php

/*
 * **********************************************************************************
 * Classe de conexão com o Banco de dados
 * **********************************************************************************
 *
 * Esta classe estabele a conexão com o banco de dados com o pattern singleton.
 *
 * Retorna um objeto tipo PDO!
 *
 * fabiano.santos@dbtraining.com.br
 *
 * **********************************************************************************
 */

class conn {
    
    /*****************************************************************
     * Define as variáveis da Classe de conexão 
     * com o Banco de dados.
     *
     * Recebe os valores das constantes definidas no arquivo de
     * configuração do sistema
     */
    
        private static $Host = SIS_DB_HOST;
        private static $User = SIS_DB_USER;
        private static $Pass = SIS_DB_PASS;
        private static $Dbsa = SIS_DB_DBSA;
        private static $Port = SIS_DB_PORT;

        /** @var PDO */
        private static $Connect = null;

    /*****************************************************************
     * Conecta com o banco de dados com o pattern singleton.
     * Retorna um objeto PDO!
     */

        private static function Conectar() {
            try {
                if (self::$Connect == null):
                    $dsn = 'mysql:host=' . self::$Host . ';dbname=' . self::$Dbsa. ';port=' . self::$Port;
                    $options = [ PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8', PDO::MYSQL_ATTR_LOCAL_INFILE =>true];
                    self::$Connect = new PDO($dsn, self::$User, self::$Pass, $options);
                    self::$Connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                endif;
            } catch (PDOException $e) {
                echo $e->getCode();
                echo $e->getMessage();
                echo $e->getFile();
                echo $e->getLine();
                die;
            }
            return self::$Connect;
        }

    /*****************************************************************
    *Retorna um objeto PDO Singleton Pattern. 
    */
        public static function getConn() {
            return self::Conectar();
        }

    /*******************************************************************
     * Construtor do tipo protegido previne que uma nova instância da
     * Classe seja criada atravês do operador `new` de fora dessa classe.
     */
        private function __construct() {
        }

    /*****************************************************************
     * Método clone do tipo privado previne a clonagem dessa instância
     * da classe
     *
     * @return void
     */
        private function __clone() {
        }

    /*****************************************************************
     * Método unserialize do tipo privado para previnir desserialização
     * da instância dessa classe.
     *
     * @return void
     */
        private function __wakeup() {
        }

}
