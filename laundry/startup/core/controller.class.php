<?php

/*
 * **********************************************************************************
 * Classe controller do sistema
 * **********************************************************************************
 *
 * Esta Classe é Reponsável por carregar o template, povoar e exibir a view, 
 * povoar e incluir arquivos PHP no sistem.
 *
 * Arquitetura MVC!
 *
 * @copyright (C) FSA Tecnologia  
 * Todos os direitos desde sistema são protegidos pela lei de 
 * direitos autorais, a cópia sem autoriazação de qualquer parte 
 * deste sistema é proibida.
 * Desenvolvido por Fabiano Santos, Setembro 2016, SP/Brasil
 *
 * fabiano.santos@dbtraining.com.br
 *
 * **********************************************************************************
 */ 


class controller {

    /**
     * <b>Carrega o método loadView:</b>
     */
    public function loadView($viewName, $viewData = array()) {
        global $currentModule;
         extract($viewData);
        include 'modules/' . $currentModule . '/views/' . $viewName . '.php';
    }

    /**
     * <b>Carrega o método loadTemplate :</b>
     */
    public function loadTemplate($viewName, $viewData = array()) {
        global $currentModule;
        include 'modules/' . $currentModule . '/templates/template.tpl.php';
    }

    /**
     * <b>>Carrega o método loadViewinTemplate:</b>
     */
    public function loadViewInTemplate($viewName, $viewData = array()) {
        global $currentModule;
        extract($viewData);
        include 'modules/' . $currentModule . '/views/' . $viewName . '.php';
    }

} 






