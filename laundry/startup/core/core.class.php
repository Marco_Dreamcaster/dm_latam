<?php

/*
 * **********************************************************************************
 * Classe core do sistema
 * **********************************************************************************
 *
 * Esta classe identifica as ações do usuário e do sistema e 
 * define qual controller fica responsável pelo processamento da requisição
 *
 * Esta Core foi desenvolvida para controle de sistemas com 02 ou mais módulos
 * de funcionalidades distintas. Ex Portal com modulo de administração (CMS).
 *                                  Portal com área restrita para clientes.
 *
 * URL >
 *      Modulo 1 > Controlers 1
 *      Modulo 2 > Controlers 2
 *
 * @copyright (C) FSA Tecnologia  
 * Todos os direitos desde sistema são protegidos pela lei de 
 * direitos autorais, a cópia sem autoriazação de qualquer parte 
 * deste sistema é proibida.
 * Desenvolvido por Fabiano Santos, Setembro 2016, SP/Brasil
 *
 * fabiano.santos@dbtraining.com.br
 *
 * **********************************************************************************
 * Alterações:
 *
 *  21/10/2016 16:00 - Validação do método antes da execução para prevenir erro na chamada via url.
 *
 * **********************************************************************************
 */

class core {
    /*****************************************************************
     * Método de excução das funções do sistema
     */
    public function run() {
        // Carrega a variável global que contém o módulo em execução
        global $currentModule;
        
        $url = '/'. (isset($_GET['q'])?$_GET['q']:'' );

        $params = array();

        if (!empty($url) && $url != '/') {

            $url = explode('/', $url);
            array_shift($url);
            $currentModule = $url[0];
            array_shift($url);

            if (isset($url[0])) {
                $currentController = $url[0] . 'Controller';
                array_shift($url);
            } else {
                $currentController = 'homeController';
            }

            if (isset($url[0])) {
                $currentAction = $url[0];
                array_shift($url);
            } else {
                $currentAction = 'index';
            }

            if (count($url) > 0) {
                $params = $url;
            }
        } else {

            /* Se a URL estiver vazia define os valores 
            * padrão para execução.
            */
            $currentModule = 'app';
            $currentController = 'homeController';
            $currentAction = 'index';
        } 
        
        // DEBUG ###############################
        //var_dump($currentModule);
        //var_dump($currentController);
        //var_dump($currentAction);
        //######################################
        
        // Carrega a classe de controle do sistema.
        require_once 'core/controller.class.php';
        
        // Inicia a classe de controle e passa a ação e parametros para execução.
        $c = new $currentController();
        // call_user_func_array(array($c, $currentAction), $params);  // Alterado em 21/10/2016 16:00
        // Verifica se o método chamado existe na Classe antes de executar.    
        if (method_exists($c, $currentAction) && is_callable(array($c, $currentAction))){
            call_user_func_array(array($c, $currentAction), $params);
        }else {
            // Se o método não existir na classe redireciona para a página de erro.
            header("Location: " . BASE . "p404.html");
        }
    }
}
