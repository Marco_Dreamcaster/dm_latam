<?php

/*
 * **********************************************************************************
 * Arquivo de funções auxiliares para o sistema
 * **********************************************************************************
 *
 * Este arquivo contém funções auxiliares para execução de tarefas, calculos e
 * tratamento de dados
 * 
 *
 * **********************************************************************************
 */

/*
 * Descreve nivel de usuário
 */

function getUserLevel($Level = null) {
    $UserLevel = [
        1 => 'Usuário',
        9 => 'Administrador (adm)',
        99 => 'Super Admin (adm)'
    ];

    if (!empty($Level)):
        return $UserLevel[$Level];
    else:
        return $UserLevel;
    endif;
}

/*
 * Funcao de tratamento de Strings
 */

function after($this, $inthat) {
    if (!is_bool(strpos($inthat, $this))):
        return substr($inthat, strpos($inthat, $this) + strlen($this));
    endif;
}

function after_last($this, $inthat) {
    if (!is_bool(strrevpos($inthat, $this))):
        return substr($inthat, strrevpos($inthat, $this) + strlen($this));
    endif;
}

function before($this, $inthat) {
    return substr($inthat, 0, strpos($inthat, $this));
}

function before_last($this, $inthat) {
    return substr($inthat, 0, strrevpos($inthat, $this));
}

function between($this, $that, $inthat) {
    return before($that, after($this, $inthat));
}

function between_last($this, $that, $inthat) {
    return after_last($this, before_last($that, $inthat));
}

function date_converter($_date = null) {
    $format = '/^([0-9]{2}).([0-9]{2}).([0-9]{4})$/';
    if ($_date != null && preg_match($format, $_date, $partes)) {
        return $partes[3].$partes[2].$partes[1];
    }
    return false;
}

/*
 * Funcao para manipulacao de Arrays  ***********************
 */

function array_implode_with_keys($array) {
    $return = '';
    if (count($array) > 0) {
        foreach ($array as $key => $value) {
            $return .= $key . '¦' . $value . ' && ';
        }
        $return = substr($return, 0, strlen($return) - 4);
    }
    return $return;
}

function array_explode_with_keys($string) {
    $return = array();
    $pieces = explode(' && ', $string);
    foreach ($pieces as $piece) {
        $keyval = explode('¦', $piece);
        if (count($keyval) > 1) {
            $return[$keyval[0]] = $keyval[1];
        } else {
            $return[$keyval[0]] = '';
        }
    }
    return $return;
}

/*
 * ***************************************************** 
 * Funcao para apresentar numeros de forma compactada. 
 * Ex.  "1.750.000"  retorna " 1.75 M "
 */

function NiceNumber($n) {
    $n = (0 + str_replace(",", "", $n));
    if (!is_numeric($n))
        return false;
    if ($n > 1000000000000)
        return round(($n / 1000000000000), 1) . 'Tri';
    else if ($n > 1000000000)
        return round(($n / 1000000000), 1) . ' Bil';
    else if ($n > 1000000)
        return round(($n / 1000000), 2) . ' M';
    else if ($n > 1000)
        return round(($n / 1000), 1) . ' K';
    return number_format($n);
}

/*
 * ***************************************************** 
 * Funcao para verificar elementos vazios em um Array 
 * Retorna string
 * 
 */

function verify_empty_fields($arr) {
    
    $empty_fields = FALSE;
    foreach ($arr as $key => $value) {
        if ($value == '') {
            $empty_fields[] = $key;
        }
    }
    return $empty_fields ;
}



function tirarAcentos($string){
    return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
}