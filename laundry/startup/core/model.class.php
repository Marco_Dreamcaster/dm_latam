<?php

/*
 * **********************************************************************************
 * Classe Model 
 * **********************************************************************************
 * 
 * Esta classe é reponsável por instanciar uma conexão com o Banco de dados.
 * Retorna um objeto PDO.
 * 
 * @copyright (C) FSA Tecnologia  
 * Todos os direitos desde sistema são protegidos pela lei de 
 * direitos autorais, a cópia sem autoriazação de qualquer parte 
 * deste sistema é proibida.
 * Desenvolvido por Fabiano Santos, Setembro 2016, SP/Brasil
 *
 * fabiano.santos@dbtraining.com.br
 *
 * **********************************************************************************
 */

class model {

    protected $db;
    
    public function __construct() {
        $this->db = conn::getConn();
    }

}
