<?php

/*
 * Copyright (C) FSA Tecnologia  - All Rights Reserved
 * Unauthorized copying of this file, is strictly prohibited
 * Proprietary and confidential
 * Written by Fabiano Santos, September 2016
 */

//$path = '/home/storage/c/c7/64/dbtraining1/tmp';
//ini_set('session.save_path', $path);
//session_save_path($path);
session_start();


if (isset($_SESSION['user_login_time']) && (time() - $_SESSION['user_login_time'] > 1800)) {
    // última atividade foi mais de 60 minutos atrás
    session_unset();     // unset $_SESSION  
    session_destroy();   // destroindo session data 
}
$_SESSION['user_login_time'] = time(); // update da ultima atividade
global $currentModule;
require 'core/config/config.inc.php';
$core = new core();
$core->run();
?>