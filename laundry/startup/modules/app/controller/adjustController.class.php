<?php

/*
 * **********************************************************************************
 * HomeController.class  
 * 
 * @copyright (C) FSA Tecnologia  - All Rights Reserved
 * Unauthorized copying of this file, is strictly prohibited
 * Proprietary and confidential
 * Written by Fabiano Santos, September 2016, SP/Brazil
 * 
 * **********************************************************************************
 */

class adjustController extends Controller {


    /* <b>index:</b>
    *  Método responsavel por carregar a View de login no modulo Portal.
    */
    public function index() {
    	ini_set('max_execution_time', 8000);
    	$addr = new adjustAddr();
    	$results = $addr->getAddr();
    	foreach ( $results as $a){
            sleep(1);
			$address =  ltrim(rtrim($a['logradouro'])).",".ltrim(rtrim($a['endereco'])).",". ltrim(rtrim($a['numero']))."-".ltrim(rtrim($a['Bairro']))."-".ltrim(rtrim($a['cidade']))."-".ltrim(rtrim($a['uf']));
    		$gmaps = new gmaps();
    		$dados = $gmaps->geoLocal($address);
    		var_dump($dados);
    		if(count($dados)==1){
                $b['unidade'] = $a['unidade'];
    			$b['elevador']=$a['elevador'];
    			$b['endereco_formatado'] = $dados[0]['formatted_address'];
    			$b['lat'] = $dados[0]['lat'];
    			$b['lng'] = $dados[0]['lng'];
    			echo $b['endereco_formatado']."</br>";
    		}else{
                $b['unidade'] =$a['unidade'];
    			$b['elevador']=$a['elevador'];
    			$b['endereco_formatado'] = "0";
    			$b['lat'] = "0";
    			$b['lng'] = "0";
    			echo $b['endereco_formatado']."</br>";
    		}
    		$up =  $addr->editAddr($b);
    	}
    }
}
