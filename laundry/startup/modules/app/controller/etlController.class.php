<?php

/*
 * **********************************************************************************
 * HomeController.class  
 * 
 * @copyright (C) FSA Tecnologia  - All Rights Reserved
 * Unauthorized copying of this file, is strictly prohibited
 * Proprietary and confidential
 * Written by Fabiano Santos, September 2016, SP/Brazil
 * 
 * **********************************************************************************
 */

class etlController extends model {

    /* <b>index:</b>
    *  Método responsavel por carregar a View de login no modulo Portal.
    */
    public function carga($fileName,$table) {
        echo "Iniciando carga da tabela $table !!</br>";
        $p1 = $this->proc($fileName,$table);
        echo $p1.' Registros inseridos</br>';
        echo "Carga terminada !!</br>";
        die;
    }

    private function proc($fileName,$table) {
        // Limpa tabela de Stage
        //$sql = $this->db->prepare("TRUNCATE TABLE $table");
        //$sql->execute();
        //Abre o arquivo para importacao.
        $file = 'upload/'.$fileName.'.csv';
        $sql = $this->db->prepare('
            LOAD DATA LOCAL INFILE "'.$file.'"
            INTO TABLE '.$table.'
            FIELDS TERMINATED by \';\'
            LINES TERMINATED BY \'\n\'
            IGNORE 1 LINES');
        $sql->execute();
        return $sql->rowCount();
        var_dump($sql);
    }
}

