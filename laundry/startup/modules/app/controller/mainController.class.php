<?php

/*
 * **********************************************************************************
 * Classe mainController 
 * **********************************************************************************
 *
 * Esta Classe é Reponsável pelo controle das requisicoes
 *
 * Classe de controle no Padrao MVC
 *
 * Desenvolvido por Fabiano Santos, Setembro 2016, SP/Brasil
 * fabiano.dos.stos@gmail.com
 *
 * **********************************************************************************
 */ 


// Verifica se a Constante BASE foi definida no Autoload e previne acesso direto a Classe.
defined('BASE') OR exit;

// Extende a Classe Controller para implementacao dos metodos de Templates
class mainController extends Controller {

    /* <b>__construct:</b>
    *  Método __construct Verifica se existe uma session de usuario ativa na 
    *  contrucao da classe.
    *  Recebe True ou False do Metodo getLoginSession() da Class Model users.
    *  Se existir redireciona para a pagina de Dashboard senao devolve para o Login
    */

	public function __construct() {
        
	}

    /* <b>index:</b>
    *  Método responsavel por carregar a View Dashboard no modulo.
    */
    public function index() {
        
    }

   

//  ########################################################################
//  for panama
//  ########################################################################    


    /* <b>getBuildings:</b>
    *  Método responsavel pela requisicao de selecao dos Edificios.
    */
    public function getBuildings(){
        $model = new buildings();
        $buildings = $model->getBuildings();
        echo json_encode($buildings);
    }

    /* <b>updateBuilding:</b>
    *  Método responsavel pela requisicao de update dos Edificios.
    */
    public function updateBuilding(){

        $post_data = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        $post_data = array_map('strip_tags', $post_data);
        $post_data = array_map('addslashes', $post_data);

        //Adiciona o campo Cleansed no array
        $post_data += [ "Cleansed" => 'Y' ];

        //Define a tabela
        $table  = "buildings";

        $model   = new buildings();
        $result = $model->updateBuilding($table,$post_data);
        echo json_encode($result);

        //$fields = verify_empty_fields($post_data);

        /*
        if ($fields) {
            $jSON = [
                'type' => "warning",
                'title'=> "Atención",
                'text' => "Por favor complete el formulario : " . implode(", ", $fields),
                'redirect' => FALSE
            ];
            echo json_encode($jSON);
        }else{

            $table  = "buildings";
            $maps   = new maps();
            $result = $maps->updateBuilding($table,$post_data);
            echo json_encode($result);

        }*/
    }


}
