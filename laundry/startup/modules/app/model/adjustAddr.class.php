<?php

/**
 * <b>xxxxxx.class:</b>
 * Classe responsável por executar o processamento de dados referente ao cadastro 
 * e manutencao de Alunos no banco de dados!
 * @copyright (c) 2016, Fabiano Santos
 */
class adjustAddr extends model{

    public function getAddr() {

        $sql = $this->db->prepare("SELECT 
                                        elevador,
                                        unidade,
                                        posto,
                                        zona,
                                        setor,
                                        endereco,
                                        numero,
                                        Bairro,
                                        cidade,
                                        uf,
                                        coordenadas
                                    FROM
                                        equipamentos
                                    WHERE
                                        coordenadas = 0 and  filial = '5006'
                                    ORDER BY filial,zona,setor
                                    LIMIT 2500");
        $sql->execute();

        if($sql->rowCount() > 0) {
            $reg = $sql->fetchAll(PDO::FETCH_ASSOC);
        }else{
         $reg = 0;
     }

     return $reg;
 }


 public function editAddr(array $data) {

    try{
        $sql = $this->db->prepare("UPDATE equipamentos 
                                        SET 
                                            endereco_formatado= :endereco_formatado,
                                            lat               = :lat, 
                                            lng               = :lng,
                                            coordenadas       = 1
                                        WHERE 
                                            elevador = :elevador and unidade = :unidade");


        $sql->execute($data);

        if($sql->rowCount() > 0) {

            $data = 1;

        }else{

            $data = -1;
        }

    } catch (exception $e) {        
        $data = $e;
    };

    return $data;
}

}


