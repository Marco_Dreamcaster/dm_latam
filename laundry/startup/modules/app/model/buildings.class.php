<?php

/**
 * <b>maps.class:</b>
 * Classe responsável por executar o processamento das informacoes no banco de dados.
 * Classe Model no Padrao MVC
 * @copyright (c) 2016, Fabiano Santos
 */
class buildings extends model{

    public function getBuildings() {
        $sql = $this->db->prepare("SELECT * FROM buildings order by revision desc,Address");
        $sql->execute();
        if($sql->rowCount() > 0) {
            $reg = $sql->fetchAll(PDO::FETCH_ASSOC);
        }else{
         $reg = 0;
     }
     return $reg;
 }

 public function updateBuilding($table, array $data) {

    try {
        $sql = $this->db->prepare("
            UPDATE {$table} 
            SET 
            building_name=:building_name, 
            Address=:Address,
            BH_SiteContactRole=:BH_SiteContactRole, 
            BH_SiteContact=:BH_SiteContact,
            BH_Email=:BH_Email,
            BH_Phone_1=:BH_Phone_1,
            reference=:reference,
            route=:route,
            formatted_address=:formatted_address,
            lat=:lat,
            lng=:lng,
            Cleansed= :Cleansed,
            revision=:revision,
            country=:country,
            administrative_area_level_1=:administrative_area_level_1,
            administrative_area_level_2=:administrative_area_level_2,
            locality=:locality 
            WHERE 
            building_no=:building_no");
        $sql->execute($data);

        if($sql->rowCount() > 0) {
            $msg = [
                'type' => "success",
                'title'=> "Success",
                'text' => "Dados Atualizados!",
                'redirect' => FALSE
            ];
            return $msg;
        } else {
            $msg = [
                'type' => "warning",
                'title'=> "Ops!",
                'text' => "Ocorreu um problema!",
                'redirect' => FALSE
            ];
            return $msg;
        }

    } catch (Exception $e) {
        $msg = [
            'type' => 'error',
            'title'=> "Ops!",
            'text' => $e,
            'redirect' => FALSE
        ];
        return $msg;
    }
}

public function create($table, array $data) {

    try {
        $fields = implode(",", array_keys($dados));
        $values = ":" . implode(", :", array_keys($dados));

        $sql = "INSERT INTO {$table} ({$fields}) VALUES ({$values})";
        $statement = $this->db->prepare($sql);
        $statement->execute($data);

        $lastId = $this->db->lastInsertId();

        if ($lastId) {
            $msg = [
                'type' => "success",
                'text' => "Dados Inseridos!",
                'redirect' => FALSE
            ];
            return $msg;
        } else {
            $msg = [
                'type' => "alert",
                'text' => "Ocorreu um problema!",
                'redirect' => FALSE
            ];
            return $msg;
        }

    } catch (Exception $e) {
        $msg = [
            'type' => 'error',
            'text' => $e,
            'redirect' => FALSE
        ];
        return $msg;
    }
}


/*
    public function create($tabela, array $dados){
        $campos = implode(", ", array_keys($dados));
        $values = implode(", ", array_values($dados));
        $totalInterrogacoes = count($dados);
        $interrogacoes = str_repeat('?,', $totalInterrogacoes);
        $interrogacoes = substr($interrogacoes, 0, -1); // remove a última virgula
        $sql = "INSERT INTO $tabela FIELDS($campos) VALUES($interrogacoes)";
        echo $sql;
    }
*/

}


