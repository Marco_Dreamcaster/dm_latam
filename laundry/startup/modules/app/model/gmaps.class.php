<?php

/*
 * **********************************************************************************
 * HomeController.class  
 * 
 * @copyright (C) FSA Tecnologia  - All Rights Reserved
 * Unauthorized copying of this file, is strictly prohibited
 * Proprietary and confidential
 * Written by Fabiano Santos, September 2016, SP/Brazil
 * 
 * **********************************************************************************
 */

class gmaps{

  private $mapsKey;

  function __construct($key = null) {
    if (!is_null($key)) {
      $this->mapsKey = "AIzaSyC-xP1jTawz7wroY2SgfdEZ9krjRikv1dg";
      //$this->mapsKey = "AIzaSyCHwk800A7vBvWgplt8rAaMmR1lEcsmHnw";

    }
  }



  function carregaUrl($url) {
    
    
    
    if (function_exists('curl_init')) {
      $cURL = curl_init($url);
      curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($cURL, CURLOPT_FOLLOWLOCATION, true);
      $resultado = curl_exec($cURL);
      curl_close($cURL);
    
    } else {
    
   
      $resultado = file_get_contents($url); 

        
    }
    
    if (!$resultado) {
      trigger_error('Não foi possível carregar o endereço: <strong>' . $url . '</strong>');
    } else {
      return $resultado;
    }
    

  }


  function geoLocal($endereco) {

 
    $addr = tirarAcentos($endereco);

    $url = "https://maps.googleapis.com/maps/api/geocode/json?key={$this->mapsKey}&address=" . urlencode($addr);
    //$resp = json_decode($this->carregaUrl($url),true);

    $resp_json = file_get_contents($url);
    $resp = json_decode($resp_json,true);

    if ($resp['status'] == 'OK'):
      $i = 0;

    foreach ($resp["results"] as $key) {

      $lati = $resp['results'][$i]['geometry']['location']['lat'];
      $longi = $resp['results'][$i]['geometry']['location']['lng'];
      $formatted_address = $resp['results'][$i]['formatted_address'];

      $jSON[] = array(
        "formatted_address" => "$formatted_address",
        "lat" => "$lati",
        "lng" => "$longi",

        );

      $i++;
    }

    return $Result = $jSON;
   
    else:
      
    return $Result = $resp['status'] ; 

    endif;


  } //end function

} //end class




