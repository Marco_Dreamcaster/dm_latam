<?php
// selenium-tests/tests/TitlePageTest.php

namespace tests; // Note the "My" namespace maps to the "tests" folder, as defined in the autoload part of `composer.json`.

use Facebook\WebDriver\WebDriverBy;
use Lmc\Steward\Test\AbstractTestCase;

class FirstTest extends AbstractTestCase
{
    public function testShouldContainSearchInput()
    {
        // Load the URL (will wait until page is loaded)
        $this->wd->get('http://10.148.1.47/laundry/'); // $this->wd holds instance of \RemoteWebDriver

        // Do some assertion
        $this->assertContains('Laundry App', $this->wd->getTitle());

        // You can use $this->log(), $this->warn() or $this->debug() with sprintf-like syntax
        $this->log('Current page "%s" has title "%s"', $this->wd->getCurrentURL(), $this->wd->getTitle());

        $searchInput = $this->findById('keywords');

        // Assert title of the search input
        $this->assertEquals('form-control form-control-sm p-2', $searchInput->getAttribute('class'));
    }
}