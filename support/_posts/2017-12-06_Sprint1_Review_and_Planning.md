---
layout: post
title:  "DM LATAM Sprint 1 Review and Planning for next sprint"
date:   2017-12-06 10:00:00 +0100
categories: dm_latam
author: dreamcaster
location: Porto Alegre, Brazil
description: before we close sprint 1, let´s review and plan for the next sprint
---


# Prototype

The prototype has gone through a major refactoring, now it´s possible to configure validations, and see results on the fly.

We have the beginnings of a true validation framework written in Visual Basic.   We should do some in-depth code review, since the work has evolved

We're still missing some important features:

- it does not follow file pattern on outuput flat file
- no reports are generated
- no metrics are collected
- no dataset level validations are implemented yet
- no validation for numeric and date types
- no validation for RUC

# Vendors Template

The Vendors template is only partially implemented, so this is our focus for the last day of the sprint.

We halt development on the prototype, and focus now on the template, and on the entryset.


# Terminology Review

A prototype is what the name says, just a testing bed for development.

PROTOTYPE + SPECS = TEMPLATE

The template is a spreadsheet closely follows flat files specifications, but has no real data attached to it.   Of course, templates will need data to make sure the validations are properly configured.   But its data does not really mean anything for the businness.

TEMPLATE + REAL DATA = ENTRYSET

An entryset is a template populated with real data, obtained from the queries defined by the functional team.  Entrysets can be assigned to individual users during cleansing tasks.

ENTRYSET + CLEANSING WORK = DATASET

A dataset is the flat file resulting from the cleansing work done on the entryset.  Only records which survive validation routines will be passed through.


# Excel magic codes for line formatting

=AND($A2="",$C2="C")
=AND($A2="A",$C2="C")
=AND($A2="R",$C2="C")
=AND($C2="D")
=$C2=""

=$2:$5,$1684:$1048576,$AK$167:$XFD$1683


# SQL Server temporary tables

[temporary tables is SQL Server](https://stackoverflow.com/questions/16749045/is-it-necessary-to-use-for-creating-temp-tables-in-sql-server)
