---
layout: post
title:  "DM LATAM Sprint 2 review"
date:   2017-12-13 10:00:00 +0100
categories: dm_latam
author: dreamcaster
location: Porto Alegre, Brazil
description: review of features of Commons 0.2
---


# What's new on Commons 0.2?

Commons is the Oracle EBS VBA framework for validation and flat-file generation.

## Write code to configure

No more manual configuration.  Why?  It costs time, and gets easily corrupted.   Write code that performs the configuration instead.

It may take longer to make it work, but the process can be repeated indefinitely.

## Code on Commons modules

Keep code on Commons modules, avoid coding directly on workbook and worksheets.

Why?  Because code is rapidly evolving, and needs to be shared among the various templates.

### VBAConfigurator

The VBAConfigurator takes care of managing the VBA code template workbook VBA Project.

Click one button, and you save your code to Commons.

Click another, and you load the current release.

If you want to preserve changes on a module for a specific country or VO, remove it from the VBAConfigurator monitored components list on that template.


## Naming conventions

All workbooks have a worksheet named "Help" to give support (color codes and messages should be explained)

Data worksheets are prefixed with "Data - "
Configuration worksheets are prefixed with "Config - "

All workbooks have worksheet named "Config - Commons" to store application-level parameters (e.g. folders, constants, connection strings)

All workbooks have worksheet named "Version" to keep track of major changes

## Auto Configuration

Happens automatically before full validation is triggered

1. adds conditional formatting to table range.
2. locks fields
3. hide fields (planned)


# What is planned for version 0.3?

## Usability

The next release will have at least two modes, one for developers, another for regular users,  and buttons to switch between them

### DEV MODE

All worksheets are available for editing

No locks.

### USER MODE

Comes in two flavors: basic, and advanced

In all USER modes:

1. configuration worksheets are hidden
2. fields obey the isHidden metadata

#### Basic

Only Data, Help and Version worksheets are available.

Locks are in place, but user will be able to filter records.

Why?

We want the user to be totally safe.

#### Advanced

Only Data, Commons Config, Help and Version worksheets are available.

No locks are in place: user will be able to filter AND sort records.

Why?

We want to let users change sorting, or output destination folder, before re-locking again.


## Deployment

### VM

1. need to review administrative rights on the VM
2. install Pageant, TortoiseGit and Git

### Sharepoint

build some sort of integration with SHP US (perhaps OneDrive) (issue under Carole)

Synch a folder on the VM with SHP, configure the flat file output folder to be the same

Check if entryset workbooks can also be kept on SHP


## New metadata

### isLocked

  controls if a field is locked for editing (when in basic USER MODE)

### isHidden

  controls if a field is hidden from the user

### validations based on ORA_TYPE

  NUMERIC triggers a isNumber validation
  DATE triggers a isDate validation

  VARCHAR2 automatically sets maxLen (future max Range)

### TextHandler.convertToMM-DD-YYYY

  Post-processing function used to override date formats on the flat file, disregarding the display format on Excel
  (planned)

### constant values

  Constant values go directly to the flat file

  If the field has a constant value, it gets automatically hidden from the user (in basic USER MODE)   


### min max ranges

  should be available for NUMBERIC, DATE on S3 (or S4)

  mandatory VARCHAR2 fields mean implicitly minLen = 1


# References
[Customized Ribbon Images](http://soltechs.net/customui/imageMso01.asp?gal=1&count=no)
