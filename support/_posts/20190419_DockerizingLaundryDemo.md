# Docker Housekeeping (mantendo a casa limpa)

Pq?  Para evitar que configurações anteriores atrapalhem os testes de novas configurações.


[Pruning Docker volumes and housekeeping](https://linuxize.com/post/how-to-remove-docker-images-containers-volumes-and-networks/)

## Remove all volumes

```
PS D:\pub\laradock> docker volume ls
DRIVER              VOLUME NAME

PS D:\pub\laradock> docker system prune --volumes
WARNING! This will remove:
        - all stopped containers
        - all networks not used by at least one container
        - all volumes not used by at least one container
        - all dangling images
        - all dangling build cache
Are you sure you want to continue? [y/N] y
Total reclaimed space: 0B
PS D:\pub\laradock>
```

## Remove all stopped containers

```
PS D:\pub\laradock> docker container ls -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES

PS D:\pub\laradock> docker container prune
WARNING! This will remove all stopped containers.
Are you sure you want to continue? [y/N] y
Total reclaimed space: 0B
```

## Force docker-compose to rebuild an image

```
PS D:\pub\laradock> docker-compose build --no-cache mssql

Building mssql
Step 1/12 : FROM microsoft/mssql-server-linux
 ---> 314918ddaedf
...
Step 12/12 : CMD /bin/bash ./entrypoint.sh
 ---> Running in d2b1465a52f6
Removing intermediate container d2b1465a52f6
 ---> f5f30df62681
Successfully built f5f30df62681
Successfully tagged laradock_mssql:latest
```

## Docker MSSQL 

Using  [Microsoft MSSQL SERVER Docker image](https://hub.docker.com/_/microsoft-mssql-server)

### LAUNDRY_DEMO snapshot

Place a new DM_LATAM.bak as LAUNDRY_DEMO_LATEST.bak on pub\data\mssql\snapshots

The BAK file will be available on MSSQL container.

```
PS D:\pub> cp C:\Users\XXX\OneDrive\20190419_DM_LATAM.bak D:\pub\data\mssql\snapshots\LAUNDRY_DEMO_LATEST.bak

PS D:\pub> dir .\data\mssql\snapshots\


    Directory: D:\pub\data\mssql\snapshots


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----        4/20/2019  12:00 AM      861352448 LAUNDRY_DEMO_LATEST.bak

```

### RESTORE script

restores LAUNDRY_DEMO_LATEST.bak on pub\data\mssql\snapshots 

```
PS D:\pub\laradock>
 
 docker-compose exec mssql bash /usr/src/app/restoreLaundryDemo.sh

Processed 105072 pages for database 'LAUNDRY_DEMO', file 'DM_LATAM' on file 1.
Processed 7 pages for database 'LAUNDRY_DEMO', file 'DM_LATAM_log' on file 1.
Converting database 'LAUNDRY_DEMO' from version 661 to the current version 869.
Database 'LAUNDRY_DEMO' running the upgrade step from version 661 to version 668.
Database 'LAUNDRY_DEMO' running the upgrade step from version 668 to version 669.
...

```
Where restoreLaundryDemo.sh is just a wrapper for:

```
/opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P "<YourStrong!Passw0rd>"
 -Q 
 "RESTORE DATABASE LAUNDRY_DEMO FROM DISK = '/var/opt/mssql/snapshots/LAUNDRY_DEMO_LATEST.bak'
  WITH  MOVE 'DM_LATAM' TO '/var/opt/mssql/data/LAUNDRY_DEMO.mdf',
        MOVE 'DM_LATAM_log' TO '/var/opt/mssql/data/LAUNDRY_DEMO_log.ldf'"

```


## Changing password
Open manager and run the query:

```
sp_password NULL, '<Laundry@Dem0>', 'sa'

```

## Finding out the version of linux on php-fpm

```
PS D:\pub\laradock> docker-compose exec php-fpm bash

root@52763d5c1a00:/var/www/dm_latam.demo/laundry/scripts# cat /etc/issue*
Debian GNU/Linux 9 \n \l

Debian GNU/Linux 9

cat /etc/issue*

```
## F

