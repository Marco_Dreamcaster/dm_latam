# Project BackLog

We'll work under a Scrum-like approach with one week long sprints.  Let�s keep this simple.

Initially, each sprint could start on Thursday, and end on next Wednesday.

At the beginning of the sprint, we have a quick standup to log all objectivesfor the next week.   As the week moves on, we annotate the week�s backlog to reflect changes to the scope.

Each sprint will have its own file, the format can be decided later, but it should include:

1. dates for beginning and end of sprint (including acommodation for holidays)
2. the perceived scope at the beginning of the sprint, usually stated as user stories
3. any changes to the scope or further request on a separate section
4. Any data which might be useful during that week (links, comments, code snippets, etc...)

[Multiple users on same workbook](https://support.office.com/en-us/article/Collaborate-on-Excel-workbooks-at-the-same-time-with-co-authoring-7152aa8b-b791-414c-a3bb-3024e46fb104)
